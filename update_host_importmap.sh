#!/bin/bash

### This script provides replaces the host variables for BA and SM in importmaps.json to the provided host.
###     
###     Run command: bash update_host.sh ${HOST}
###     example: bash update_host.sh https://sample.org
### 


 
### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

### Escape special character /
 
HOST=$(sed 's/\//\\\//g' <<< $HOST_URL)
declare -A products
 
#core system components (an api call should replace it)
products["\"@ebs/ba\":"]="${HOST}\/ba\/ebs-ba.js"
products["\"@ebs/sm\":"]="${HOST}\/sm\/ebs-sm.js"
products["\"@ebs/styleguide\":"]="${HOST}\/style\/ebs-styleguide.js"
products["\"@ebs/components\":"]="${HOST}\/component\/ebs-components.js"
products["\"@ebs/cs\":"]="${HOST}\/ui\/ebs-cs.js"
products["\"@ebs/root-config\":"]="${HOST}\/ebs-root-config.js"
products["\"@ebs/layout\":"]="${HOST}\/layout\/ebs-layout.js"
products["\"@ebs/po\":"]="${HOST}\/printout\/ebs-po.js"
products["\"@ebs/chart\":"]="${HOST}\/chart\/ebs-chart.js"


length=${#products[@]}

FILE="$PWD/shared/importmaps.json"
 
while read -r line; do
for key in "${!products[@]}"; do
    
    MFE_HOST_URL=${products[${key}]}  

    if [[ "$line" == *"${key}"* ]]; then  
     HOST_STR=$(sed 's/\//\\\//g' <<< ${key})
      echo $MFE_HOST_URL
    if  sed -i -e "s/${HOST_STR}.*/${HOST_STR} \"${MFE_HOST_URL}\",/" $FILE; then
            ### Remove escape characters
            MFE_HOST_URL=$(sed 's/\\//g' <<< ${MFE_HOST_URL})        
            echo;
            echo "$GREEN ☒  Host successfully updated to ${MFE_HOST_URL} . $RESET"
        else 
            echo;
            echo "$RED ☒  Update of host failed. Kindly check for unescaped special characters. $RESET"
            exit;
        fi  
    fi 
done   

done < "$FILE"