# **Core System Core**

This Project is the core front-end of a micro-front-end architecture.
For EBS is defined as an entry front-end model where the following capabilities are implemented and included as Micro-front-end concept.

- Maintain a catalog of reusable components to be shared and used by all front-end domains aligned to the front-end architecture
- Sharing information among domains under the Cross Context concept.
- Maintaining and implement UI/UX standard sharing only one style provider under Core System control.

## Included Projects

| Project                 | Description                                                                     |
| ----------------------- | ------------------------------------------------------------------------------- |
| cs-ui                   | Contains the UI from Core System domain                                         |
| ebs-components          | Contains complex components as EbsGrid and EbsForm                              |
| ebs-layout              | Contains the application layout and handle authentication/authorization control |
| ebs-printout            | Contains UI from printout tool                                                  |
| ebs-shared-dependencies | Contains the necessary import maps to register every MFE deployed               |
| ebs-styleguide          | Contains Styles for all MFEs                                                    |
| ebs-root                | Orchestrator to handle every MFE imported                                       |

## Pattern and Technologies

This project relies heavily on single-spa and react and uses the following main packages to work:

- npm version 8 or higher installed.
- nodejs version 16 or higher installed.
- lerna version 4 or higher globally installed.

## Project Structure

Core System Core contains Core system components to handle all micro-front-end architecture and include new micro-front-end modules.

### Layers

Every package maintains the structure and code logic described on [Developer guides](https://ebsproject.atlassian.net/l/c/wZ70WpL0)

## Local Development

### Recommendations

- IDE: VSCode (recommended).
- npm 8 or higher.
- nodejs 16 or higher.
- lerna 4 or higher.

#### VSCode extensions recommended

- ebs-snippets
- Better Comments
- EsLint

### Installation

1. Clone [core system core](https://bitbucket.org/ebsproject/cs-core/src/develop/) repository.
2. In VSCode (or IDE) open the folder where the repository was cloned.

### Before to setting packages up

Every package is a React project independent. So you need to copy `.git` folder from `cs-core/.git` to every package folder `cs-core/packages/cs-ui`, `cs-core/packages/ebs-root`, etc...

> **Note: Mostly the .git folder is hidden.**

### Setting packages up

In the terminal of `cs-core/` folder.

```
lerna exec -- npm install -f
```

This command will install all node_modules for every package.

### How to run every package locally

-
-
-
-
-
-

> **Note: ebs-shared-dependencies project is used only for deployment process**

### Build

This process will start testing for all packages. If you need to build only one project, please refer the documentation for every project.

Results of build can be found in:

- `cs-core/packages/${project}/dist`

In the terminal of `cs-core/` folder.

```
lerna exec -- npm run build
```

### Testing and Coverage

This process will start unit testing for every package. If you need to test only one project, please refer the documentation for every project

In the terminal of `cs-core/` folder.

```
lerna exec -- npm run test
```

In the terminal of `cs-core/` folder.

```
lerna exec -- npm run coverage
```

Results of coverage can be found in:

- `cs-core/packages/${project}/coverage/index.html`

## Using the RBAC component
The RBAC component is based on High-Order Component approach and allow us apply the access control to users in REACT apps, this component is exposed through the ebs-layout and can be used in any Micro Front End (mfe). The RBAC needs the "User Profile API" that contains all the actions allowed to the user by product and domain.
The following code segment implement the basics of RBAC component: 
```
export const RBAC = ({ allowedAction, children }) => {
    try {
        const selectedProduct = JSON.parse(localStorage.getItem("selectedProduct"));
        const userProfile = getUserProfile();
        const domains = getContext();
        const dataActions = userProfile.permissions.applications.filter(a => a.id === domains.domainId).map(a => {
            return a.products
                .filter(a => a.name.includes(selectedProduct.name)).map(a => {
                    return a.actions.concat(a.dataActions);
                })
        })
        const result = dataActions[0];
        const access = result[0].includes(allowedAction);
        return access && children;
    }
    catch (error) {
        return (
            <div>
                <Typography
                    className=" font-ebs text-xs text-red-700"
                >
                    Not Allowed

                </Typography>
            </div>
        )
    }
};
```
### Detail of constants and props used in the RBAC component
- The selectedProduct is get it dynamically according the user interaction ("CRM", "Printout", etc).
- The userProfile and domains are get them from the context.
- The dataActions holds the actions allowed for the current user profile.
- The access holds the result of checking if the allowedAction is contained in the actions given by the user profile.
- The showComponent is a flag to load the disabled component even when the access is false, is optional. 
- The RBAC component has three props : the allowedAction ("Create", "Modify", etc), the shoComponent(boolean, optional) and the children component (any component that needs implement the access control).
- if the allowedAction is contained in the dataActions, the component returns the child component so it will be available in the UI.
- if the showComponent is passed as true and the result of the access is false, the component will be rendered but will be disabled.

### Use of RBAC component 
- To use the RBAC component we have to import from the ebs-layout and wrap the child component that we want to apply the access control and pass the action regarding this component, example:
```       
<RBAC allowedAction={"Print"} showComponent={true}> // showComponent is optional, the default value is false
    <Designer reportName={"people_contact_report"} mode={"preview"} />
</RBAC>

``` 
In the example above the RBAC component is wrapping the Designer component, the user should have the action "Print" in the userProfile=>dataActions for this specific product in order he/she can use it.
If the showComponent prop is passed with the value "true" and the user does not have the enough permissions, the component will be rendered and disabled.

## How to get the user profile using the application context

Inside of any React application import the userContext from the @ebs/layout, pass to the useContext hook and extract the userProfile object.
In case you need a token you should get it from @ebs/layout.

```
import React,{useContext} from "react";
//import { getUserProfile } from "@ebs/layout";
import {useSelector as useSelectorLayout, getTokenId} from "@ebs/layout";

const UserProfileTest = ({}) =>{

   //const userProfile = getUserProfile();  ////deprecated
   //const tokenId = localStorage.getItem("token_id"); ////deprecated

    const tokenId = getTokenId();
    const{ userProfile } = useSelectorLayout(({user})=> user);

    return (<>
    Component
    </>)
}
```

## Deploy container
