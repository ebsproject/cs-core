#!/bin/bash

### This script provides replaces the host variable in src/index.ejs to specified host.
###     
###     Run command: bash update_host.sh ${HOST}
###     example: bash update_host.sh https://sample.org
### 

### Set Colors
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
RESET=`tput sgr0`

### Escape special character / 
HOST=$(sed 's/\//\\\//g' <<< $HOST_URL)

GLOBAL_HOST_URL="${HOST}\/shared\/importmaps.json"

HOST_STR="<script type=\"systemjs-importmap\" src"
FILE="$PWD/index.html"
echo $FILE

while read -r line; do
 
        if sed -i -e "s/#{HOST_CS_SHAREDLIB}#/${GLOBAL_HOST_URL}/" $FILE; then
            echo;
            echo "$GREEN ☒  Host successfully updated to $GLOBAL_HOST_URL. $RESET"
            exit;
        else 
            echo;
            echo "$RED ☒  Update of host failed. Kindly check for unescaped special characters. $RESET"
            exit;
        fi
 
done < "$FILE"