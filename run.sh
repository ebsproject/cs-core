#!/usr/bin/env bash

(cd /usr/code;printenv | egrep 'REACT_APP_AUTH_CONFIG_AUTH_URL|REACT_APP_AUTH_CONFIG_TOKEN_URL|REACT_APP_AUTH_CONFIG_CALLBACK_URL|REACT_APP_AUTH_CONFIG_CLIENT_ID|REACT_APP_AUTH_CONFIG_CLIENT_SECRET|REACT_APP_CSAPI_URI_GRAPHQL|REACT_APP_CSAPI_URI_REST|REACT_APP_PRINTOUT_ENDPOINT|REACT_APP_MARKERDB_ENDPOINT|REACT_APP_FILEAPI_URI|REACT_APP_ACTUAL_VERSION' > .env)

echo "===> Building ..."
(cd /usr/code;npm install)

echo "===> Building ..."
(cd /usr/code;npm run build)

#echo "create a directory in nginx folder"
#mkdir /usr/share/nginx/html/

echo "copy build"
cp -avr /usr/code/dist/* /usr/share/nginx/html/

echo "run scripts"

nginx -g 'daemon off;'



