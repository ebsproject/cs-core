
import * as materialUIStyles from "@mui/material/styles";
import EbsThemeProvider from './ebs-theme-provider';
import { ThemeContextProvider } from 'catalog/theme-context';

export function EBSMaterialUIProvider(props) {
  return (
    <materialUIStyles.StyledEngineProvider
    >
      <ThemeContextProvider>
      <EbsThemeProvider children = {props.children} />
      </ThemeContextProvider>
    </materialUIStyles.StyledEngineProvider>
  )
}
