import { ThemeProvider } from '@mui/material/styles';
import { useThemeContext } from 'catalog/theme-context';
import CssBaseline from '@mui/material/CssBaseline';

const EbsThemeProvider = (props) =>{
const {theme} = useThemeContext ()
    return(
      <ThemeProvider theme={theme}>
          <CssBaseline /> 
            {props.children}
          </ThemeProvider>
    )
}
export default EbsThemeProvider;