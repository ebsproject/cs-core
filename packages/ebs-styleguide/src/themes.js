import { createTheme } from "@mui/material";

const baseStyles = {
    typography: {
        "fontFamily": `Roboto`
       },
       components: {
        MuiInputLabel: {
          styleOverrides: {
            root: {
              color: (theme) => theme.palette.mode === "dark" ? '#000000' : '#fff',
            },

            outlined: {
              '&.Mui-focused': {
                color: (theme) => theme.palette.mode === "dark" ? '#000000' : '#fff', 
              },
            },
          },
        },
        MuiButton: {
          defaultProps: {
            variant: 'contained',
          },
          styleOverrides: {
            root: {
              borderRadius: '5px',
              textTransform: 'none',
              margin:'3px'
            },
            containedPrimary: {
              backgroundColor: 'rgba(0, 150, 136, 1)',
              '&:hover': {
                backgroundColor: 'rgba(0, 150, 136, 0.8)',
              },
            },
          },
        },
        MuiTextField: {
          
            defaultProps: {
              variant: 'outlined',
            },
          styleOverrides: {
            root: {
              margin:'5px',
              borderRadius: '5px',
              '& .MuiInputBase-root': {
                borderRadius: '5px',
              
              },
            },
          },
        },
        MuiAutocomplete: {
          styleOverrides: {
            root: {
              margin:'5px',
              borderRadius: '5px',
            },
            inputRoot: {
              padding: '4px',
            },
            paper: {
              borderRadius: '5px',
            },
          },
        },
      },
}
export const lightTheme = createTheme({
...baseStyles,
  palette: {
    mode:"light",
    common: {
      black: "#000",
      white: "#fff",
      processing: {
        main: "rgba(10, 117, 199, 1)",
        contrastText: "#fff",
      },
      completed: {
        main: "rgba(6, 64, 112, 1)",
        contrastText: "#fff",
      },
    },
    background: {
      paper: "#fff",
      default: "#fff",
    },
    primary: {
      light: "rgba(82, 199, 183, 1)",
      main: "rgba(0, 150, 136, 1)",
      dark: "rgba(0, 103, 90, 1)",
      contrastText: "#fff",
    },
    secondary: {
      light: "rgba(133, 187, 92, 1)",
      main: "rgba(85, 139, 47, 1)",
      dark: "rgba(37, 93, 0, 1)",
      contrastText: "#fff",
    },
    ebs: {
      main: "rgba(111, 191, 61, 1)",
      light: "rgba(85, 139, 47, 1)",
      dark: "rgba(133, 187, 92, 1)",
      contrastText: "#fff",
    },
    error: {
      light: "rgba(255, 95, 82, 1)",
      main: "#f44336",
      dark: "rgba(142, 0, 0, 1)",
      contrastText: "#fff",
    },
    text: {
      primary: "rgba(51, 51, 51, 1)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)",
    },
    processing: {
      main: "rgba(10, 117, 199, 1)",
      contrastText: "#fff",
    }
  },
});
export const darkTheme = createTheme({
  ...baseStyles,
  palette: {
    mode: 'dark',
    common: {
      black: '#000',
      white: '#fff',
      processing: {
        main: 'rgba(10, 117, 199, 1)',
        contrastText: '#fff',
      },
      completed: {
        main: 'rgba(6, 64, 112, 1)',
        contrastText: '#fff',
      },
    },
    background: {
      paper: '#121212',
      default: '#121212',
    },
    primary: {
      light: 'rgba(82, 199, 183, 1)',
      main: 'rgba(0, 150, 136, 1)',
      dark: 'rgba(0, 103, 90, 1)',
      contrastText: '#fff',
    },
    secondary: {
      light: 'rgba(133, 187, 92, 1)',
      main: 'rgba(85, 139, 47, 1)',
      dark: 'rgba(37, 93, 0, 1)',
      contrastText: '#fff',
    },
    ebs: {
      main: 'rgba(111, 191, 61, 1)',
      light: 'rgba(85, 139, 47, 1)',
      dark: 'rgba(133, 187, 92, 1)',
      contrastText: '#fff',
    },
    error: {
      light: 'rgba(255, 95, 82, 1)',
      main: '#f44336',
      dark: 'rgba(142, 0, 0, 1)',
      contrastText: '#fff',
    },
    text: {
      primary: 'rgba(255, 255, 255, 1)',
      secondary: 'rgba(255, 255, 255, 0.7)',
      disabled: 'rgba(255, 255, 255, 0.5)',
      hint: 'rgba(255, 255, 255, 0.5)',
    },
    processing: {
      main: 'rgba(10, 117, 199, 1)',
      contrastText: '#fff',
    },
  },
});