import EbsTabsLayout from "./EbsTabsLayout";
import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("EbsTabsLayout is in the DOM", () => {
  render(
    <EbsTabsLayout tabs={[{ label: "example", component: <div>Hello</div> }]} />
  );
  expect(screen.getByTestId("EbsTabsLayoutTestId")).toBeInTheDocument();
});

test("EbsTabsLayout change tab correctly", () => {
  render(
    <EbsTabsLayout
      tabs={[
        {
          label: "tab1",
          component: <div data-testid="tabpanel1">Tab 1 displayed!</div>,
        },
        {
          label: "tab2",
          component: <div data-testid="tabpanel1">Tab 2 displayed!</div>,
        },
      ]}
    />
  );
  
  expect(screen.getByTestId("EbsTabsLayoutTestId")).toBeInTheDocument();
  // * Find tab buttons
  const [tab1, tab2] = screen.getAllByRole("tab");
  fireEvent.click(tab1);
  expect(screen.getByTestId("tabpanel1")).toHaveTextContent("Tab 1 displayed!");
  fireEvent.click(tab2);
  expect(screen.getByTestId("tabpanel1")).toHaveTextContent("Tab 2 displayed!");
  fireEvent.click(tab1);
  expect(screen.getByTestId("tabpanel1")).toHaveTextContent("Tab 1 displayed!");
});

test("EbsTabsLayout changes index correctly 0", () => {
  render(
    <EbsTabsLayout
      index={0}
      tabs={[
        {
          label: "tab1",
          component: <div data-testid="tabpanel1">Tab 1 displayed!</div>,
        },
        {
          label: "tab2",
          component: <div data-testid="tabpanel1">Tab 2 displayed!</div>,
        },
      ]}
    />
  );
  
  expect(screen.getByTestId("EbsTabsLayoutTestId")).toBeInTheDocument();
  expect(screen.getByTestId("tabpanel1")).toHaveTextContent("Tab 1 displayed!");
});

test("EbsTabsLayout changes index correctly 1", () => {
  render(
    <EbsTabsLayout
      index={1}
      tabs={[
        {
          label: "tab1",
          component: <div data-testid="tabpanel1">Tab 1 displayed!</div>,
        },
        {
          label: "tab2",
          component: <div data-testid="tabpanel1">Tab 2 displayed!</div>,
        },
      ]}
    />
  );
  
  expect(screen.getByTestId("EbsTabsLayoutTestId")).toBeInTheDocument();
  expect(screen.getByTestId("tabpanel1")).toHaveTextContent("Tab 2 displayed!");
});