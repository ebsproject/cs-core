import TabPanel from "./TabPanel";
import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("EbsTabsLayout is in the DOM", () => {
  render(
    <TabPanel value={1} index={1}>
      Hello
    </TabPanel>
  );
  expect(screen.getByTestId("tabpaneltestid")).toBeInTheDocument();
});
