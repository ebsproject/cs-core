import { Dialog, DialogContent, DialogActions, Button, Typography, DialogTitle, IconButton, Badge } from "@mui/material"
import { useDropzone } from "react-dropzone";
import { useMemo, useState, useEffect } from "react";
import { CloudUpload, Delete } from "@mui/icons-material";
const baseStyle = {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: '20px',
    borderWidth: 2,
    borderRadius: 2,
    borderColor: '#eeeeee',
    borderStyle: 'dashed',
    backgroundColor: '#fafafa',
    color: '#bdbdbd',
    outline: 'none',
    transition: 'border .24s ease-in-out',
    height: "300px"
};

const focusedStyle = {
    borderColor: '#2196f3'
};

const acceptStyle = {
    borderColor: '#00e676'
};

const rejectStyle = {
    borderColor: '#ff1744'
};

const thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 16
};

const thumb = {
    position: 'relative',
    display: 'inline-block',
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: 'border-box'
};

const thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%'
};

const img = {
    display: 'block',
    width: 'auto',
    height: '100%'
};

function getMimeTypes(accepted) {
    let mime_types = {}

    accepted.forEach(type => {

        switch (type) {
            case '.csv': mime_types = { ...mime_types, ...{ "text/csv": [".csv"] } }
                break;
            case '.pdf': mime_types = { ...mime_types, ...{ "application/pdf": [".pdf"] } }
                break;
            case '.doc': mime_types = { ...mime_types, ...{ "application/msword": [".doc"] } }
                break;
            case '.docx': mime_types = { ...mime_types, ...{ "application/vnd.openxmlformats-officedocument.wordprocessingml.document": [".docx"] } }
                break;
            case '.json': mime_types = { ...mime_types, ...{ "application/json": [".json"] } }
                break;
            case '.xls': mime_types = { ...mime_types, ...{ "application/vnd.ms-excel": [".xls"] } }
                break;
            case '.xlsx': mime_types = { ...mime_types, ...{ "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet": [".xlsx"] } }
                break;
            case '.xml': mime_types = { ...mime_types, ...{ "application/xml": [".xml"] } }
                break;
            case '.zip': mime_types = { ...mime_types, ...{ "application/zip": [".zip"] } }
                break;
            case '.7z': mime_types = { ...mime_types, ...{ "application/x-7z-compressed": [".7z"] } }
                break;
            case '.txt': mime_types = { ...mime_types, ...{ "text/plain": [".txt"] } }
                break;
            case '.rar': mime_types = { ...mime_types, ...{ "application/vnd.rar": [".rar"] } }
                break;
            case '.png': mime_types = { ...mime_types, ...{ "image/png": [".png"] } }
                break;
            case '.jpeg': mime_types = { ...mime_types, ...{ "image/jpeg": [".jpeg"] } }
                break;
            case '.jpg': mime_types = { ...mime_types, ...{ "image/jpeg": [".jpg"] } }
                break;
            case '.intertek': mime_types = { ...mime_types, ...{ "text/plain": [".intertek"] } }
                break;

        }
    })

    return mime_types;
}

const DropzoneDialog = ({
    error,
    open,
    onClose,
    onSave,
    cancelButtonText,
    submitButtonText,
    showPreviews,
    showFileNamesInPreview,
    maxFileSize, acceptedFiles }) => {

    const acceptedTypes = getMimeTypes(acceptedFiles);
    const [files, setFiles] = useState([]);
    const {
        getRootProps,
        getInputProps,
        isFocused,
        isDragAccept,
        isDragReject } = useDropzone({
            accept: acceptedTypes,
            maxSize: maxFileSize || 1048576,
            onDrop: acceptedFiles => {
                setFiles(acceptedFiles.map(file => Object.assign(file, { preview: URL.createObjectURL(file) })))
            }
        });
    const style = useMemo(() => ({
        ...baseStyle,
        ...(isFocused ? focusedStyle : {}),
        ...(isDragAccept ? acceptStyle : {}),
        ...(isDragReject ? rejectStyle : {})
    }), [
        isFocused,
        isDragAccept,
        isDragReject
    ]);

    const _files = files.map(file => (
        <li key={file.path}>
            {file.path} - {file.size} bytes
        </li>
    ));
    const handleClose = () => {
        setFiles([]);
        onClose();
    }
    const handleSubmit = () => {
        onSave(files);
    }
    const handleDelete = (fileToDelete) => {
        setFiles(files.filter(file => file !== fileToDelete));
    };
    const thumbs = files.map(file => (
        <div style={thumb} key={file.name}>
            <Badge
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                badgeContent={
                    <IconButton
                        aria-label="delete"
                        onClick={() => handleDelete(file)}
                        style={{ backgroundColor: 'white', padding: 2 }}
                    >
                        <Delete color="error" />
                    </IconButton>
                }
            >
                <div style={thumbInner}>
                    <img
                        src={file.preview}
                        style={img}
                        onLoad={() => { URL.revokeObjectURL(file.preview) }}
                    />
                </div>
            </Badge>
        </div>
    ));

    useEffect(() => {
        return () => files.forEach(file => URL.revokeObjectURL(file.preview));
    }, []);
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            maxWidth="md"
            fullWidth
        >
            <DialogTitle>
                <Typography varian="h4">Upload a file</Typography>
            </DialogTitle>
            <DialogContent>
                <section className="container">
                    <div {...getRootProps({ style })}>
                        <input {...getInputProps()} />
                        <Typography variant="h5" paragraph>Drag 'n' drop some files here, or click to select files</Typography>
                        <CloudUpload fontSize="large" />
                    </div>
                    {error && <Typography variant="h6" color={"error"}>{error}</Typography>}
                    {
                        showPreviews && (
                            <aside style={thumbsContainer}>
                                {thumbs}
                            </aside>
                        )
                    }
                    {
                        showFileNamesInPreview && (
                            <aside>
                                <ul>{_files}</ul>
                            </aside>
                        )
                    }
                </section>
            </DialogContent>
            <DialogActions>
                <Button variant="text" onClick={handleClose}>
                    <Typography className="uppercase">
                        {cancelButtonText || "Cancel"}
                    </Typography>
                </Button>
                <Button variant="text" onClick={handleSubmit} >
                    <Typography className="uppercase">
                        {submitButtonText || "Submit"}
                    </Typography>
                </Button>
            </DialogActions>
        </Dialog>
    )
}
export default DropzoneDialog;