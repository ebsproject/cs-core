import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import {
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  DialogContentText,
  Button,
  Slide,
  Typography,
  Box,
  Grid
} from "@mui/material";
import { Close } from "@mui/icons-material";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const EbsDialogAtom = React.forwardRef(
  ({ open, handleClose, title, maxWidth, children, actions }, ref) => {
    return (
      /* 
     @prop data-testid: Id to use inside EbsDialog.test.js file.
     */
      <div data-testid={"EbsDialogTestId"} ref={ref}>
        <Dialog
          onSubmit={e => {
            e.preventDefault();
            e.stopPropagation();
          }}
          open={open}
          TransitionComponent={Transition}
          onClose={handleClose}
          aria-labelledby="ebs-dialog-slide"
          aria-describedby="ebs-dialog-side-description"
          fullWidth
          maxWidth={maxWidth}
          sx={{ zIndex: 200 }}
        >
          <DialogContent>

            <DialogContentText>
              <Grid container>

                <Grid item xs={6} >
                  <Grid container justifyContent={"flex-start"}>
                    <Typography className="flex-grow font-ebs text-ebs-green-900 font-bold p-2 m-2  text-4xl">
                      {title}
                    </Typography>
                  </Grid>

                </Grid>
                <Grid item xs={6} >
                  <Grid container justifyContent={"flex-end"}>
                    <Button
                      aria-label="close"
                      variant="text"
                      onClick={handleClose}
                    >
                      <Close />
                    </Button>

                  </Grid>

                </Grid>
              </Grid>

            </DialogContentText>

            {children}

          </DialogContent>

          <DialogActions>
            {actions}
          </DialogActions>
        </Dialog>
      </div >
    );
  }
);
// Type and required properties
EbsDialogAtom.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.node,
  actions: PropTypes.node,
  maxWidth: PropTypes.oneOf(["xs", "sm", "md", "lg", "xl", false]),
  children: PropTypes.node,
};
// Default properties
EbsDialogAtom.defaultProps = {
  open: false,
  maxWidth: "md",
};

export default EbsDialogAtom;
