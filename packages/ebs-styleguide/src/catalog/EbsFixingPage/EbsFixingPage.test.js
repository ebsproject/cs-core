import EbsFixingPage from './EbsFixingPage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('EbsFixingPage is in the DOM', () => {
  render(<EbsFixingPage></EbsFixingPage>)
  expect(screen.getByTestId('EbsFixingPageTestId')).toBeInTheDocument();
})
