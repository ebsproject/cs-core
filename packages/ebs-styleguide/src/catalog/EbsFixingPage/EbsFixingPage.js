import React, { useState } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { Box, Container, Typography } from "@mui/material";
import EBSLogo from "assets/logos/EBS.png";

//MAIN FUNCTION
/**
 *@param { }: component properties
 *@param ref: reference made by React.forward
 */
const EbsFixingPageAtom = React.forwardRef(({ countdownDate }, ref) => {
  const [time, setTime] = useState({
    days: 3,
    hours: 4,
    minutes: 5,
    seconds: 1,
  });

  const interval = setInterval(() => {
    const countDownDate = countdownDate.getTime();
    // Get todays date and time
    var now = new Date().getTime();

    // Find the distance between now an the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(
      (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
    );
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
    setTime({
      days,
      hours,
      minutes,
      seconds,
    });
  }, 1000);

  return (
    /**
     *@prop data-testid: Id to use inside EbsFixingPage.test.js file.
     */
    <Container
      data-testid={"EbsFixingPageTestId"}
      ref={ref}
      className="container justify-items-center mx-full rounded-md bg-white h-screen"
    >
      <img src={EBSLogo} className="mx-auto" />
      <Typography className="capitalize text-center md:text-5xl text-ebs-brand-default font-ebs">
        This Tool will be available coming soon
      </Typography>
      <Box className="flex flex-row flex-nowrap justify-center w-full p-8">
        <Box>
          <Box className="rounded-md border-2 border-black min-w-16 p-4 mx-4">
            <Typography className="text-5xl font-ebs">{time.days}</Typography>
          </Box>
          <Typography className="capitalize text-xl font-ebs text-center">
            days
          </Typography>
        </Box>
        <Box>
          <Box className="rounded-md border-2 border-black min-w-16 p-4 mx-4">
            <Typography className="text-5xl font-ebs">{time.hours}</Typography>
          </Box>
          <Typography className="capitalize text-xl font-ebs text-center">
            hours
          </Typography>
        </Box>
        <Box>
          <Box className="rounded-md border-2 border-black min-w-16 p-4 mx-4">
            <Typography className="text-5xl font-ebs">
              {time.minutes}
            </Typography>
          </Box>
          <Typography className="capitalize text-xl font-ebs text-center">
            minutes
          </Typography>
        </Box>
        <Box>
          <Box className="rounded-md border-2 border-black min-w-16 p-4 mx-4">
            <Typography className="text-5xl font-ebs">
              {time.seconds}
            </Typography>
          </Box>
          <Typography className="capitalize text-xl font-ebs text-center">
            seconds
          </Typography>
        </Box>
      </Box>
    </Container>
  );
});
// Type and required properties
EbsFixingPageAtom.propTypes = {
  countdownDate: PropTypes.any.isRequired,
};
// Default properties
EbsFixingPageAtom.defaultProps = {
  countdownDate: new Date(),
};

export default EbsFixingPageAtom;
