import { useThemeContext } from "catalog/theme-context";
import { darkTheme, lightTheme } from "../../themes";
import FormControlLabel from '@mui/material/FormControlLabel';
import LightModeIcon from '@mui/icons-material/LightMode';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import { Switch } from "@mui/material";
import { useEffect } from "react";
import Cookies from "js-cookie";

const ThemeSelector = () =>{
    const {theme,setTheme} = useThemeContext();
    
    useEffect(()=>{
      let _theme = Cookies.get("theme");
      if(_theme){
        setTheme(_theme === "dark" ? darkTheme : lightTheme);
      }
    },[]);
    const handleThemeChange = (event) => {
        setTheme(event.target.checked ? darkTheme : lightTheme);
        Cookies.set("theme", event.target.checked ? "dark" : "light" );
      };

  return (
    <FormControlLabel
    control={
      <Switch
        checked={theme.palette.mode === 'dark'}
        onChange={handleThemeChange}
        icon={<LightModeIcon />}
        checkedIcon={<DarkModeIcon />}
      />
    }
    label=""
  />
  );
}
export default ThemeSelector;