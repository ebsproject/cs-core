const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const DotenvWebpackPlugin = require("dotenv-webpack");
const CopyPlugin = require("copy-webpack-plugin");
const path = require('path');
module.exports = (webpackConfigEnv, argv) => {
  const orgName = "ebs";
  const defaultConfig = singleSpaDefaults({
    orgName: orgName,
    projectName: "root-config",
    webpackConfigEnv,
    argv,
    disableHtmlGeneration: true,
  });

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    plugins: [
      new HtmlWebpackPlugin({
        inject: false,
        favicon: "./src/assets/images/favicon.ico",
        template: "src/index.ejs",
        templateParameters: {
          isLocal: webpackConfigEnv && webpackConfigEnv.isLocal,
          orgName,
        },
      }),
      new DotenvWebpackPlugin(),
      new CopyPlugin({
        patterns: [
          {
            from: "src/*.json",
            to({ context, absoluteFilename }) {
              return Promise.resolve("[name][ext]");
            },
          },
        ],
      }),
    ],
  });
};
