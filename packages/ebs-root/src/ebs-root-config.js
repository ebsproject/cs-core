import { registerApplication, start } from "single-spa";
import {
  constructApplications,
  constructRoutes,
  constructLayoutEngine,
} from "single-spa-layout";
import microfrontendLayout from "./microfrontend-layout.html";
import loader from "./loader.html";
const data = {
  loaders: {
    pageLoader: loader,
  },
};

const routes = constructRoutes(microfrontendLayout, data);

const applications = constructApplications({
  routes,
  loadApp({ name }) {
    return System.import(name);
  },
});

registerApplication({
  name: "@ebs/messaging",
  app: () => System.import("@ebs/messaging"),
  activeWhen: () => false,
});

const layoutEngine = constructLayoutEngine({ routes, applications });

applications.forEach(registerApplication);

function load(url) {
  return new Promise(async function (resolve, reject) {
    // do async thing
    const res = await fetch(url);
    // resolve
    resolve(res.json()); // see note below!
  });
}

const importMapUrl = document.getElementById("importmaps").getAttribute("src");

export const mfeInfo = load(importMapUrl);

export const graphqlUri = process.env.REACT_APP_CSAPI_URI_GRAPHQL;
export const restUri = process.env.REACT_APP_CSAPI_URI_REST;
export const printoutUri = process.env.REACT_APP_PRINTOUT_ENDPOINT;
export const authUrl = process.env.REACT_APP_AUTH_CONFIG_AUTH_URL;
export const authTokenUrl = process.env.REACT_APP_AUTH_CONFIG_TOKEN_URL;
export const clientId = process.env.REACT_APP_AUTH_CONFIG_CLIENT_ID;
export const clientSecret = process.env.REACT_APP_AUTH_CONFIG_CLIENT_SECRET;
export const callbackUrl = process.env.REACT_APP_AUTH_CONFIG_CALLBACK_URL;
export const appVersion = process.env.REACT_APP_ACTUAL_VERSION;
export const fileAPIUrl = process.env.REACT_APP_FILEAPI_URI;
export const markerDbUrl = process.env.REACT_APP_MARKERDB_ENDPOINT;
export const cbGraphqlUri = process.env.REACT_APP_CBAPI_URI_GRAPHQL;
export const authConfigLogoutUri = process.env.REACT_APP_AUTH_CONFIG_LOGOUT_URI;

Promise.all([System.import("@ebs/styleguide")]).then(([styleguide]) => {
  layoutEngine.activate();
  start();
});
