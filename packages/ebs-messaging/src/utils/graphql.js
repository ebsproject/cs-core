import { gql } from "@apollo/client/core";

 export const FIND_CONTACT_LIST =`
 query(
    $page: PageInput,
    $filters:[FilterInput],
    $sort:[SortInput]
    ){
    findUserList(
      page:$page,
      filters:$filters,
      sort:$sort) {
        totalPages
        totalElements
          content{
                id
                userName

                contact{
                  id
                  person{
                    givenName
                    familyName
                  }
                  jobLogs{
                    id
                    message
                    status
                    jobWorkflow{
                      id
                    }
                    contacts{
                      id
                    }
                }
                }    
              }        
    }
  
  }`;

export const FIND_USER_LIST=`
query{
  findUserList{
      content{
          id
          userName
          contact{
              id
              person{
                  givenName
                  familyName
              }
          }
      }
  }
}`;
 export const CREATE_MESSAGE =``;
 export const UPDATE_MESSAGE=`
mutation($jobLog:JobLogInput!){
     modifyJobLog(jobLog:$jobLog)
              {
              id
              }
}

`;
export const CREATE_JOB_LOG=`
mutation($jobLog:JobLogInput!){
     createJobLog(jobLog:$jobLog)
              {
              id
              }
}

`;

 export const FIND_MESSAGES =`
 query(
  $page: PageInput,
  $filters:[FilterInput],
  $sort:[SortInput]
  ){
  findJobLogList(
       page:$page,
    filters:$filters,
    sort:$sort) {
      totalPages
      totalElements
        content{
            recordId
              id
              jobTrackId
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
            }        
  }

}
 
 `;

 export const FIND_USER =`
 query(
    $filters:[FilterInput],
    $sort:[SortInput]
    ){
    findUserList(
      filters:$filters,
      sort:$sort) {
        totalPages
        totalElements
          content{
                id
                userName
                contact{
                  id
                  person{
                    givenName
                    familyName
                  }
                  jobLogs{
                    id
                    message
                    status
                    jobWorkflow{
                      id
                    }
                    contacts{
                      id
                    }
                }
                }    
              }        
    }
  
  }`;
