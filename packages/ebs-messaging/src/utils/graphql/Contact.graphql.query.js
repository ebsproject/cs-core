
import { gql } from "@apollo/client/core";

export const GET_JOB_LOG = gql`
query(
  $filters:[FilterInput],
  $sort:[SortInput]
  ){
  findJobLogList(
    filters:$filters,
    sort:$sort) {
      totalPages
      totalElements
        content{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
            }        
  }

}
  
`;
export const GET_JOB_LOG_SUBSCRIPTION = gql`
subscription(
  $filters:[FilterInput],
  $sort:[SortInput]
  ){
  findJobLogListSubscription(
    filters:$filters,
    sort:$sort) {
      totalPages
      totalElements
        content{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
            }        
  }

}
  
`;
export const JOB_LOG_SUBSCRIPTION = gql`
subscription{
  jobLog{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
                 
  }

}
  
`;

export const CREATE_JOB_LOG = gql`
  mutation CREATE_JOB_LOG($jobLog: JobLogInput!) {
    createJobLog(jobLog: $jobLog) {
      id
      
    }
  }
`;
export const MODIFY_JOB_LOG = gql`
  mutation MODIFY_JOB_LOG($jobLog: JobLogInput!) {
    modifyJobLog(jobLog: $jobLog) {
      id
      
    }
  }
`;


export const FIND_USERS_LIST =gql`
query(
   $filters:[FilterInput],
   $sort:[SortInput]
   ){
   findUserList(
     filters:$filters,
     sort:$sort) {
       totalPages
       totalElements
         content{
               id
               userName
               contact{
                 id
                 person{
                   givenName
                   familyName
                 }
                 jobLogs{
                   id
                   message
                   status
                   jobWorkflow{
                     id
                   }
                   contacts{
                     id
                   }
               }
               }    
             }        
   }
 
 }`;
