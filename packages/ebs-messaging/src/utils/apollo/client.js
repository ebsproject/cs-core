import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  from
} from '@apollo/client/core';
import { split } from '@apollo/client/core';
import { getMainDefinition } from '@apollo/client/utilities';
import { createClient } from 'graphql-ws';
export const CreateApolloClient = (token, endpoint) =>{

var ws_uri_endpoint = endpoint;
ws_uri_endpoint = ws_uri_endpoint.replace('graphql', 'subscriptions');
ws_uri_endpoint.includes("http") ? ws_uri_endpoint = ws_uri_endpoint.replace('http','ws') :  ws_uri_endpoint = ws_uri_endpoint.replace('https','wss');


const wsLink = createClient({
  url:ws_uri_endpoint,
})

const httpLink = new HttpLink({
        uri: endpoint
      });
    
      const authLink = new ApolloLink((operation, forward) => {
        operation.setContext(({ headers }) => ({
          headers: {
            ...headers,
            authorization: `Bearer ${token}`,
          },
        }));
        return forward(operation);
      });


const link = split(
({ query }) => {
  const { kind, operation } = getMainDefinition(query);
  return (
    kind === 'OperationDefinition' &&
    operation === 'subscription'
  );
},
wsLink,
from([authLink, httpLink])
);

return  new ApolloClient({
link:link,
cache: new InMemoryCache(),
fetchPolicy: "no-cache",
});
}
