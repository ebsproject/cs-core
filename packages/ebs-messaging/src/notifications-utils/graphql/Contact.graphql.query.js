
import { gql } from "@apollo/client/core";

export const GET_JOB_LOG = gql`
query(
  $filters:[FilterInput],
  $sort:[SortInput]
  ){
  findJobLogList(
    filters:$filters,
    sort:$sort) {
      totalPages
      totalElements
        content{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
            }        
  }

}
  
`;
export const GET_JOB_LOG_SUBSCRIPTION = gql`
subscription(
  $filters:[FilterInput],
  $sort:[SortInput]
  ){
  findJobLogListSubscription(
    filters:$filters,
    sort:$sort) {
      totalPages
      totalElements
        content{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
            }        
  }

}
  
`;
export const JOB_LOG_SUBSCRIPTION = gql`
subscription{
  jobLog{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }     
                 
  }

}
  
`;
