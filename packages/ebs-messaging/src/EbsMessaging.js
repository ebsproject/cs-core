import template from "./views/index.html";
import NoContactSelected from "./views/NoContactSelected.html"
import StartMessage from "./views/StartMessage.html"
import Loading from "./views/loading.html"
import styles from "./views/style.css";
import axios from 'axios';
import { FIND_CONTACT_LIST, FIND_USER, FIND_MESSAGES, UPDATE_MESSAGE } from "./utils/graphql";
import {jwtDecode} from "jwt-decode";
import moment from "moment";
import { CreateApolloClient } from "./utils/apollo/client";
import bell_icon from "/images/bell-icon.png";
import white_bell from "/images/bell-white.png";
import ebs from "/images/EBS.svg";
import message_icon from "/images/message_icon.png";
import airplane from "/images/white-paper-airplane-icon.png"
import _contact_img from "/images/contact-removebg-preview.png";
import envelope_icon from "/images/envelope.png"
import { MODIFY_JOB_LOG, CREATE_JOB_LOG, FIND_USERS_LIST } from "./utils/graphql/Contact.graphql.query";
/*-------------------NOTIFICATION IMPORTS WEB COMPONENT---------------------------*/

import not_template from "./notifications-views/index.html";
import main from "./notifications-views/main_head.html";
import noNotifications from "./notifications-views/no-notifications.html";
import { QUERY, MUTATION_MODIFY, FIND_USER as FIND_USER_NOT} from "./notifications-utils/graphql";
// import  {ApolloQueryMixin}  from '@apollo-elements/mixins/apollo-query-mixin.js';
import { JOB_LOG_SUBSCRIPTION } from "./notifications-utils/graphql/Contact.graphql.query";
 import{ CreateApolloClient as NotificationCreateApolloClient } from "./notifications-utils/apollo/client";
 import{ CreateApolloClient as MessagingCreateApolloClient } from "./notifications-utils/apollo/client";
 import { LitElement, html, css } from 'lit';
class EbsShipment extends LitElement {
  static styles = css`
     :host {
       display: block;
     }
     .modal {
       display: none;
       position: fixed;
       z-index: 1;
       left: 0;
       top: 0;
       width: 100%;
       height: 100%;
       overflow: auto;
       background-color: rgb(0, 0, 0);
       background-color: rgba(0, 0, 0, 0.4);
       padding-top: 60px;
     }
     .modal-content {
       background-color: #fefefe;
       margin: 5% auto;
       padding: 20px;
       border: 1px solid #888;
       width: 80%;
       color: black;
     }
     .close {
       color: #aaa;
       float: right;
       font-size: 28px;
       font-weight: bold;
     }
     .close:hover,
     .close:focus {
       color: black;
       text-decoration: none;
       cursor: pointer;
     }
    button {
      background-color: #6200ea;
      color: white;
      border: none;
      padding: 10px 20px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      margin: 10px 2px;
      cursor: pointer;
      border-radius: 8px;
      transition: background-color 0.3s, transform 0.3s;
    }
    button:hover {
      background-color: #3700b3;
      transform: scale(1.05);
    }
    button:focus {
      outline: none;
    }
               table {
      width: 100%;
      border-collapse: collapse;
      margin-top: 20px;
    }
    th, td {
      border: 1px solid #ddd;
      padding: 8px;
      text-align: left;
    }
    th {
      background-color: #f2f2f2;
    }
    tr:nth-child(even) {
      background-color: #f9f9f9;
    }
    tr:hover {
      background-color: #f1f1f1;
    }
   `;

  static properties = {
    open: { type: Boolean, reflect: true },
    items: { type: Array },
    endPoint: { type: String },
    tokenId: { type: String },
    userName: { type: String }
  };

  constructor() {
    super();
    this.open = false;
    this.items = [1, 2, 3];
    this.tokenId="";
    this.userName="";
    this.endPoint="";
  }

  toggleModal() {
    this.open = !this.open;
  }
  addItem() {
    let count = this.items.length
    this.items = [...this.items, count + 1];
    console.log(this.items)

  }

  render() {
    return html`
       <button @click="${this.toggleModal}">Create Shipment</button>
       <div class="modal" style="display: ${this.open ? 'block' : 'none'};">
         <div class="modal-content">
           <span class="close" @click="${this.toggleModal}">&times;</span>
           <slot></slot>
           <h2>Create a Shipment based on Occurrences selection</h2>
           <h3>UserName: ${this.userName}</h3>
            <h3>Token ID: ${this.tokenId}</h3>
             <h3>Endpoint: ${this.endPoint}</h3>
            <table>
            <tr>
              <th>Occurencce</th>
              <th>ID</th>
              <th>Institution</th>
              <th>Origin</th>
            </tr>
            ${this.items.map(item => html`<tr>
              <td>Occurrence-${item}</td>
              <td>${item}</td>
              <td>CIMMYT-${item}</td>
              <td>Philippines</td>
            </tr>`
    )}
          </table>
          <button @click="${this.addItem}">Add Occurrence to Shipment</button>
         </div>
       </div>
     `;
  }
}
customElements.define('ebs-shipment', EbsShipment);

class LeafletMap extends LitElement {

  static styles = css`
    #map {
      height: 400px;
      width: 100%;
      border: 2px solid black;
    }
  `;

  static properties = {
    lat: { type: Number },
    lng: { type: Number },
    zoom: { type: Number },
    type: { type: String },
    geojsonData: { type: String }
  }

  constructor(){
    super();;
    this.geojsonData = "";
    this.map = null;
    this.drawnItems = null;
    this.layers = null;
    this.drawControl = null;
  }

  firstUpdated() {
    this.map = L.map(this.shadowRoot.getElementById('map')).setView([this.lat, this.lng], this.zoom);
    L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
      attribution: "© OpenStreetMap contributors",
    }).addTo(this.map);

    this.drawnItems = new L.FeatureGroup();
    this.map.addLayer(this.drawnItems);

    this.updateGeoJSON();
    
      const drawControl = new L.Control.Draw({
        position: "topright",
        edit: {
          featureGroup: this.drawnItems,
        },
        draw: {
          polygon: true,
          polyline: false,
          rectangle: false,
          circle: false,
          marker: true,
          circlemarker: false,
        },
      });

      this.type ? null : this.map.addControl(drawControl);
  
    this.map.on(L.Draw.Event.CREATED, (e) => {
      const { layer } = e;
      this.drawnItems.addLayer(layer);
    });

      this.map.on("draw:created", (e) => {
        this._dispatchGeoJSONUpdate();
      });
  
      this.map.on("draw:edited", (e) => {
        this._dispatchGeoJSONUpdate();
      });
  
      this.map.on("draw:deleted", (e) => {
        this._dispatchGeoJSONUpdate();
      });
    
  }

  updated(changedProperties) {
    if (changedProperties.has('lat') || changedProperties.has('lng')) {
      this.map.setView([this.lat, this.lng], this.zoom);
    }
    if (changedProperties.has('geojsonData')) {
      this.updateGeoJSON();
    }
  }

  updateGeoJSON() {
    if (this.geojsonData !== "") {
      const geojsonData = JSON.parse(this.geojsonData);
      this.drawnItems.clearLayers(); // Limpia las capas anteriores
      L.geoJSON(geojsonData, {
        onEachFeature: (feature, layer) => {
          this.drawnItems.addLayer(layer);
        },
      }).addTo(this.map);
    }
  }

  _dispatchGeoJSONUpdate() {
    const updatedGeoJSON = this.drawnItems.toGeoJSON();
    this.dispatchEvent(new CustomEvent('geojson-updated', {
      detail: updatedGeoJSON,
      bubbles: true,
      composed: true
    }));
  }

  render() {
    return html`
     <link rel="stylesheet" href="https://unpkg.com/leaflet/dist/leaflet.css" />
     <link rel="stylesheet" href="https://unpkg.com/leaflet-draw/dist/leaflet.draw.css" />
    <div id="map"></div>`;
  }
}

customElements.define("ebs-map", LeafletMap)


/*-------------------MESSAGING WEB COMPONENT---------------------------*/
class EbsMessaging extends HTMLElement {
  constructor() {
    super();
    this.Token = "";
    this.end_point = "";
    this.contactSelected = null;
    this.messagesList = null;
    this.userId = 0;
    this.contactId = 0;
    this.direct_message_array = null;
    this.JobId = 0;
    this.message_object = null;
    this.userInfo = null;
    this.initial_new_messages = 0;
    this.new_messages_notifications = 0;
    this.hide_notifications = false;
    this.recordId= null;
    this.familyName ="";
    this.givenName="";
    this.messaging_client = null;
  }
  disconnectedCallback(){
    if(this.messaging_client)
    this.messaging_client.unsubscribe();
  }
  async connectedCallback() {
    this.Token = this.getAttribute("authorization_token");
    this.end_point = this.getAttribute("endPoint");
    this.jsonObj = this.getAttribute("data");
    this.data = JSON.parse(this.jsonObj);

    this.client = MessagingCreateApolloClient(this.Token, this.end_point);

    this.userInfo = this.DecodeToken(this.Token);
    let _contacts = await this.getContactListByUserName(this.userInfo.userName.toLowerCase()); // Getting all the contacts in EBS
    this.familyName =  _contacts[0].contact.person.familyName;
    this.givenName = _contacts[0].contact.person.givenName;
    this.userId = _contacts[0].id;
    this.contactId = _contacts[0].contact.id;

    let shadowRoot = this.attachShadow({ mode: 'open' }); // Added the styles  
    let styleTag = document.createElement('style');
    styleTag.innerHTML = styles;
    shadowRoot.innerHTML += template;
    shadowRoot.appendChild(styleTag);

    if (this.data) { //set the styles for record level messaging
      this.recordId = this.data.recordId;
      let message_button = shadowRoot.querySelector("#message-button");
      message_button.style.display =`none`;
      let float = shadowRoot.querySelector(".float");
      float.style.position = `relative`;
      float.style.bottom = `0px`;
      float.style.left = `0px`;
      let img_float = shadowRoot.querySelector(".img-float");

      img_float.style.cssText = `
      z-index: 999;
      position:relative;
      width:20px;
      height:20px
       `;
      let sidepanel = shadowRoot.querySelector("#sidepanel");
      sidepanel.style.cssText = `display:none`;
      let frame = shadowRoot.querySelector("#frame");
      frame.style.cssText = this.data.styles ? this.data.styles : `
      position : relative;
      display : block;
      width : auto;
      height :75vh;
      bottom: 0px;
      left:0px;
      box-shadow: 10px 5px 5px rgb(255, 255, 255);
      `;
      let content = shadowRoot.querySelector("#content");
      content.style.width = `100%`;
      content.style.borderRadius = `10px`;
      this.messages_list = shadowRoot.querySelector("#messages_list");
      this.setMessageListByContact(shadowRoot, this.userInfo);
    }
    let _image = shadowRoot.querySelector(".img-float");
    _image.src= message_icon;

    this.messagesList = await this.getMessagesFromGraphql(this.userInfo.userName);
    this.messages_list = shadowRoot.querySelector("#messages_list");

    let contact_profile = shadowRoot.querySelector(".contact-profile"); //Added the contact profile
    contact_profile.innerHTML = `<p style="font-family:'Raleway'; padding-left:50px;">${this.data ? this.data.name :"EBS Direct Message"}</p>
                                  `;

    let profile = shadowRoot.querySelector("#profile"); //Added the user profile

    profile.innerHTML += `
                                <div class="wrap">
                                <img id="profile-img" 
                                src= ${_contact_img} 
                                class="online" alt="" />
                                <p>${this.familyName}, ${this.givenName} (${this.userInfo.userName})</p>
                                </div>
                                 `;

    let send_button_image = shadowRoot.querySelector("#send_button");
    send_button_image.innerHTML = `
                                      <img style="height: 20px;height: 20px;"
                                      src=${airplane}
                                      alt="Send"/>
                                     `;
                                     this.setMessageListByContact(shadowRoot, this.userInfo);
    this.createContactList(shadowRoot);
    //#region Handle the event listener
    this.AddEventListenerToElements(shadowRoot);
    // Using a subscriptions to fetch more messages

    this.messaging_client = this.client.subscribe({ query: JOB_LOG_SUBSCRIPTION }).subscribe({
      next: (data) => {
        this.setMessageListByContact(shadowRoot, this.userInfo);
        this.createContactList(shadowRoot)
      },
    });
    //#endregion
  }
  postMessageFunction(shadowRoot) {
    let messages = shadowRoot.querySelector("#messages");
    let input_message = shadowRoot.querySelector("#input_message");
    let message = input_message.value;
    if (message || message !== "") {
      if (this.direct_message_array === null) { this.direct_message_array = [] }
      let date = moment.utc().format();
      this.direct_message_array.filter(item => item.sentBy !== this.contactId).map(item => { item.status = "read" })
      this.direct_message_array.push({
        sentBy: this.contactId,
        content: message,
        sent_timestamp: date,
        status: "unread",
        sentByName: this.userInfo.fullName,
      })
      this.postNewMessagesGraphql()
      this.messages_list.innerHTML += `
                                    <li class="replies">
                                    <img src= ${_contact_img} alt="" />
                                    <p>${message}</p>
                                    </li>
                                    `;

      messages.scroll({ top: messages.scrollHeight, behavior: "auto" });

    }
    input_message.value = null;
  }
  async postNewMessagesGraphql() {
    let selected_contacts_ids = this.data ? 
     [Number(this.contactId) ]:
     [Number(this.contactId), Number( this.contactSelected.contactId)]
    

    let JobLogInput = {
      id: this.JobId === 0 ? 0 : this.JobId,
      recordId : this.data ? this.data.recordId : null,
      jobWorkflowId: 2,
      status: "unread",
      message: this.JobId === 0 ? {
        message: {
          direct_messaging: this.direct_message_array,
          title: "Direct Message",
          language: "en",
          description: `${this.userInfo.fullName} has started a direct chat with you. EBS`,
          translation_id: 1
        }
      } : this.message_object.message,
      tenantId: 1,
      startTime: "",
      contactIds: selected_contacts_ids

    }

    try {
      await this.client.mutate({
        mutation: this.JobId === 0 ? CREATE_JOB_LOG : MODIFY_JOB_LOG,
        variables: { jobLog: JobLogInput },
      });

      if(this.data){
      
      }

    } catch (error) {
      this.messages_list.innerHTML += `
  <p style ="font-family:'Raleway';font-size: 9pt; color: red">"Your message cannot be delivered at this moment.</p>
  <p style ="font-family:'Raleway';font-size: 9pt; color: red">${error.message}</p>
    `;
    }

  }


  async setMessageListByContact(shadowRoot, userInfo) {
    let todayHeader = true;
    let olderHeader = true;
    let countTodayHeader = 0;
    let time = null;

    if (!this.contactSelected && !this.data) {
      this.messages_list.innerHTML = NoContactSelected;
      return;
    }
    this.messagesList = await this.getMessagesFromGraphql(userInfo.userName);
    this.direct_message_array = null;
    //  
    if (this.messagesList.findJobLogList.content.length > 0) {
      let result = this.messagesList.findJobLogList.content.filter(item => item.contacts.map(i => { return i.id }).includes(this.data ? this.data.recordId : this.contactSelected.contactId));
      if(this.data){
        result = this.messagesList.findJobLogList.content;
      }
      if (result.length > 0) {
        this.message_object = result[0];//
        this.JobId = result[0].id;

        this.direct_message_array = result[0].message.message.direct_messaging;
        let current_messages_number = this.direct_message_array.length;

        if (this.initial_new_messages !== current_messages_number) {

          this.initial_new_messages = current_messages_number;
          this.messages_list.innerHTML = "";
          this.direct_message_array.forEach((direct_message, index) => {

            let string_date = ""
            time = this.Calculate_Received_Time(direct_message.sent_timestamp);
            if (time.isToday) {
              if (countTodayHeader === 0) {
                ++countTodayHeader
              } else {
                todayHeader = false;
              }
            } else {
              string_date = moment.utc(direct_message.sent_timestamp).local().format('MMMM Do YYYY');
              if (index > 0 && index < this.direct_message_array.length) {

                let next_date = moment.utc(this.direct_message_array[index - 1].sent_timestamp).local().format('MMMM Do YYYY');
                next_date === string_date ? olderHeader = false : olderHeader = true;
              }
            }

            let dateTime = moment.utc(direct_message.sent_timestamp).local().format('MMMM Do YYYY, h:mm:ss a');

            let isSameUser = true;

            if (index > 0) {
              let currentUser = Array.isArray(direct_message.sentByName) ? direct_message.sentByName.join() : direct_message.sentByName ;
              let pastUser =  Array.isArray(this.direct_message_array[index - 1].sentByName) ? this.direct_message_array[index - 1].sentByName.join() : this.direct_message_array[index - 1].sentByName ;
              if (pastUser === currentUser) {
                isSameUser = false;
              }
            }

           this.messages_list.innerHTML += `
           ${time.isToday && todayHeader ? ' <div style ="text-align:center;"><h3 style ="color:gray; font-family:"Raleway": font-size:10pt;">Today</h3></div>' : ''}
           ${!time.isToday && olderHeader ? `<div style ="text-align:center;"><h3 style ="color:gray; font-family:"Raleway": font-size:10pt;">${string_date}</h3></div>` : ''} 
           ${this.data && isSameUser ? 
           `<p style='font-size:8pt; padding-bottom:0px; padding-top:20px'> ${direct_message.sentByName ? direct_message.sentByName + " wrote:" : "" }</p>`
           :``}

            <li ${direct_message.sentBy === this.contactId ? 'class="replies"' : 'class="sent"'}>
   
            <img src=${_contact_img} alt="" /> 
                                  
            <p>${direct_message.content}
            <span style="float:right; font-size:8pt; padding-top : 15px;padding-left : 15px">${dateTime.split(",")[1]}  ${direct_message.status === 'read' ?  "✅" : ""}   </span>                     
            </p>                         
            </li> 
            `;

          });

          let messages = shadowRoot.querySelector("#messages");
          messages.scroll({ top: messages.scrollHeight, behavior: "auto" });

        }
        else {

          return;
        }
      }
      else {
        this.setStartMessage(shadowRoot);
      }

    }
    else {
      this.setStartMessage(shadowRoot);
    }

  }

  DecodeToken(token) {
    let decoded_token = jwtDecode(token);
    let userinfo = {
      userName: (decoded_token['http://wso2.org/claims/emailaddress'] != undefined) ? decoded_token['http://wso2.org/claims/emailaddress'] : decoded_token['email'],
      fullName: (decoded_token['http://wso2.org/claims/fullName'] != undefined) ? decoded_token['http://wso2.org/claims/fullName'] : decoded_token['name'],
      displayName: (decoded_token['http://wso2.org/claims/displayName'] != undefined) ? decoded_token['http://wso2.org/claims/displayName'] : decoded_token['name']
    };
    return userinfo;
  }

  setStartMessage(shadowRoot) {
    this.JobId = 0;
    if(!this.data){
      this.messages_list.innerHTML = StartMessage;
      let contact_selected_name = shadowRoot.querySelector("#contact_selected_name");
      contact_selected_name.innerHTML = `<h3>${this.contactSelected.contactName}</h3>`;
    }else{
      this.messages_list.innerHTML = `<h3 style="font-family:'Raleway';">No one has posted messages yet.</h3>`
    }
  }

  //#region Utils
  async getMessagesFromGraphql(userName) {

    let response = await this.GraphqlCall(userName, true);
    return response.data;

  }
  async createContactList(shadowRoot, filter) {
    let contacts_list = shadowRoot.querySelector("#contacts_list");//Added the contact list to the window chat
    //contacts_list.innerHTML = Loading;
    contacts_list.innerHTML = ``;
    if(!filter) return;
    //let _contacts = filter === undefined ? await this.getUserList() : await this.getUserList(filter);// Getting all the contacts in EBS
    let _contacts =  await this.getUserList(filter);
    let contacts = this.sortContactsByMessages(_contacts.filter(contact => contact.userName !== this.userInfo.userName.toLowerCase()));
    contacts_list.innerHTML = "";
    let number_messages = 0;
    if (contacts.length > 0) {
      contacts.forEach(contact => {
        if (contact.new_messages > 0) {
          number_messages = contact.new_messages;
          if (!this.hide_notifications) {
            // let message_button = shadowRoot.getElementById("message-button");
            // message_button.className = "float shake";
            // setTimeout(() => { message_button.className = "float"; }, 1000);
            // let notificationSound =
            // new Audio('https://firebasestorage.googleapis.com/v0/b/ebs-project-21967.appspot.com/o/notification-tone-swift-gesture.mp3?alt=media&token=6799f788-858d-46a2-b168-24e5dc42ca6f');
            // notificationSound.play();
            this.new_messages_notifications = this.new_messages_notifications + contact.new_messages;
            shadowRoot.getElementById('new_message_notification').innerHTML = this.new_messages_notifications > 0 ? this.new_messages_notifications : "";
          }

        } else {
          number_messages = 0;
        }
        let objectString = JSON.stringify(contact.contact.jobLogs)

        let objectValue = `{"contactObject":${objectString.replace(/[&\/\\#+$~%'a*?<>]/g, ' ')},"contactName":"${contact.contact.person.familyName + ", " + contact.contact.person.givenName}","contactId": ${contact.contact.id},"userId":${contact.id}}`
        contacts_list.innerHTML += `
                                    <li class="contact" style="cursor:pointer;">
                                          <div class="wrap" id="contact_container">
                                            <img style="width:20px; height: 20px;" src=${_contact_img} alt="" />
                                           ${number_messages > 0 ? `<div class='number1' id='total_number_${contact.contact.id}'>${number_messages}</div>` : ''}
                                            <div class="meta">    
                                            <input hidden type="radio" name="contact_radio" class="chkBoxes" id="contacts_selected_${contact.contact.id}"
                                            style="float:right; cursor: pointer;"
                                              value='${objectValue}'/>
                                              <label for="contacts_selected_${contact.contact.id}">
                                              <p style="cursor:pointer; font-size:9pt; font-family:'Raleway';font-weight:600;">${contact.contact.person.familyName}, ${contact.contact.person.givenName} (${contact.userName})
                                              </p>
                                              </label>
                                             
                                            </div>  

                                                                        
                                          </div>
                                         
                                    </li>                          
                                    ` ;
      });
    };
    this.new_messages_notifications = 0
    let contacts_selected = shadowRoot.querySelectorAll(".chkBoxes");

    contacts_selected.forEach(contact => {
      let selected_contact;

      contact.addEventListener("change", (event) => {
        let _contact = JSON.parse(event.target.value);
        let elementId = `#total_number_${_contact.contactId}`;
        let elementBadge = shadowRoot.querySelector(elementId);
        if (elementBadge) { elementBadge.style.cssText = `display : none;` };
        contacts_selected.forEach(element => {
          element.setAttribute("hidden", true)
        });

        contact.removeAttribute("hidden");
        selected_contact = event.target.value;
        let contact_object = JSON.parse(selected_contact);

        this.setContactToWindowChat(contact_object, shadowRoot);

      });

    });
  }
  sortContactsByMessages(contacts) {
    let contactList = []
    contacts.forEach(contact => {
      if (contact.contact.jobLogs.length === 0) {
        contactList.push(Object.assign(contact, { new_messages: 0 }))
      }
      else {
        let messages_count = contact.contact.jobLogs?.filter(a => a.jobWorkflow?.id === "2" && a.contacts?.map(a => { return a.id }).includes(this.contactId));
        if (messages_count.length > 0) {
          let only_sent_to_me_messages = messages_count[0].message.message.direct_messaging
          let number = only_sent_to_me_messages.map(item => { return { id: item.sentBy, status: item?.status } }).filter(item => item.id !== this.contactId && item?.status === "unread")
          contactList.push(Object.assign(contact, { new_messages: number.length }))
        } else {
          contactList.push(Object.assign(contact, { new_messages: 0 }))
        }
      }

    });
    return contactList.sort((a, b) => b.new_messages - a.new_messages);
  }

  async setContactToWindowChat(contact, shadowRoot) {

    await this.updateMessagesToRead(contact);

    this.direct_message_array = null;
    this.initial_new_messages = 0;
    this.contactSelected = contact;
    let contact_profile = shadowRoot.querySelector(".contact-profile"); //Added the the New contact profile window
    contact_profile.innerHTML = `                         
                            <p style ="font-size:10pt; margin:10px">${contact.contactName}</p>
                            `;

    this.setMessageListByContact(shadowRoot, this.userInfo);
  }
  async getContactListByUserName(userName) {
    let result = null
    result = await this.client.query({
      query: FIND_USERS_LIST,
      variables: {
        filters: [{ col: "userName", mod: "EQ", val: userName }],
      }
    });

    return result.data.findUserList.content;
  }

  async getUserList(filter) {
    let contacts = await this.GraphqlCall(filter);
    if (filter)
      return contacts.data.findUserList.content.filter(item => item.contact.jobLogs !== null);
    else 
      return contacts.data.findUserList.content.filter(item => item.contact.jobLogs !== null && item.contact.jobLogs.length > 0);
  }
  /**Graphql Call */
  async GraphqlCall(filter, isMessages) {
    let query = FIND_CONTACT_LIST;
    let disjunctionFilters = false;
    let filter_array = [];
    let sortArray = [];
    if (filter) {

      if (isMessages) {
        disjunctionFilters = true;
        this.data ? filter_array.push({ col: "recordId", mod: "EQ", val: this.data.recordId }):
        filter_array.push({ col: "contacts.users.userName", mod: "EQ", val: filter.toLowerCase() })

        filter_array.push({ col: "jobWorkflow.jobType.id", mod: "EQ", val: "3" });
        query = FIND_MESSAGES;
        sortArray.push({ col: "startTime", mod: "DES" });
      }
      else {
        filter_array.push({ col: "contact.person.givenName", mod: "LK", val: filter });
        filter_array.push({ col: "contact.contactTypes.id", mod: "EQ", val: "9" });
        query = FIND_USER;
        sortArray.push({ col: "userName", mod: "ASC" });
      }
    }
    else {
      sortArray.push({ col: "contact.person.familyName", mod: "ASC" });
      filter_array.push({ col: "contact.contactTypes.id", mod: "EQ", val: "9" });
    }
    const headers = {
      "content-type": "application/json",
      "Authorization": `Bearer ${this.Token}`
    };

    const graphqlQuery = {
      "query": query,
      "variables": {
        filters: filter_array,
        sort: sortArray,
        page: { number: 1, size: 1000 },
        disjunctionFilters: disjunctionFilters,
      }
    };

    const response = await axios.request({
      url: this.end_point,
      method: 'post',
      headers: headers,
      data: graphqlQuery
    });

    return response.data;
  }
  Calculate_Received_Time(startTime) {
    let isToday = false;
    let now = moment(new Date());
    let duration = moment.duration(now.diff(startTime));
    let hours = duration.asHours();
    if (hours < 24) { isToday = true }
    return { response: "Older", isToday: isToday };
  }
  async updateMessagesToRead(contact) {

    let list = await this.getMessagesFromGraphql(this.userInfo.userName);
    let result = list.findJobLogList.content.filter(item => item.contacts.map(i => { return i.id }).includes(contact.contactId));
    if (result.length > 0) {

      let jobId = result[0].id;
      let temporalArray = result[0];
      let total_unread = result[0].message.message.direct_messaging.filter(item => item.sentBy !== this.contactId && item.status === "unread");
      if (total_unread.length === 0) return;
      result[0].message.message.direct_messaging.filter(item => item.sentBy !== this.contactId).map(item => { item.status = "read" })
      temporalArray.message.message.direct_messaging = result[0].message.message.direct_messaging//.filter(item => item.sentBy !== this.contactId)

      const headers = {
        "content-type": "application/json",
        "Authorization": `Bearer ${this.Token}`
      };

      let selected_contacts_ids = [Number(this.contactId), Number(contact.contactId)];

      let jobLogObject = {
        id: jobId,
        message: temporalArray.message,
        jobWorkflowId: 2,
        tenantId: 1,
        contactIds: selected_contacts_ids
      };
      const graphqlQuery = {
        "query": UPDATE_MESSAGE,
        "variables": {
          jobLog: jobLogObject
        }
      };
      try {
        await axios.request({
          url: this.end_point,
          method: 'post',
          headers: headers,
          data: graphqlQuery
        });
      } catch (error) {
        console.log(error)

      }

    }
  }
  AddEventListenerToElements(shadowRoot) {
    let search_contact = shadowRoot.querySelector("#search_contact");
    let send_button = shadowRoot.querySelector("#send_button");
    let message_button = shadowRoot.querySelector("#message-button");
    let input_message = shadowRoot.querySelector("#input_message");

    search_contact.addEventListener('keyup', async (event) => {
      let filter = event.target.value;
      if (filter !== '') {
        this.createContactList(shadowRoot, filter);

      } else {
        this.createContactList(shadowRoot);
      }

    })
    send_button.addEventListener('click', () => {
      if (this.contactSelected) {
        let elementId = `#total_number_${this.contactSelected.contactId}`;
        let element = shadowRoot.querySelector(elementId);
        if (element) { element.style.cssText = `display : none;` }

        this.postMessageFunction(shadowRoot);
      } else {
        if(this.data){
          this.postMessageFunction(shadowRoot);
          return;
        }
        let text_message = shadowRoot.getElementById("input_message");

        let send_button = shadowRoot.getElementById("send_button");
        send_button.className = "shake";
        text_message.innerText = "Please select a contact first"
        text_message.style.cssText = `color: red`
        setTimeout(() => { send_button.className = ""; text_message.innerText = ""; text_message.style.cssText = `color: black` }, 1000);

      }

    });

    input_message.addEventListener('keypress', (event) => {
      if (event.code === 'Enter') {
        if (this.contactSelected) {
          let elementId = `#total_number_${this.contactSelected.contactId}`;
          let element = shadowRoot.querySelector(elementId);
          if (element) { element.style.cssText = `display : none;` }
          this.postMessageFunction(shadowRoot);
        } else {
          let text_message = shadowRoot.getElementById("input_message");
          let send_button = shadowRoot.getElementById("send_button");
          send_button.className = "shake";
          text_message.innerText = "Please select a contact first";
          text_message.style.cssText = `color: red`;
          setTimeout(() => { send_button.className = ""; text_message.innerText = ""; text_message.style.cssText = `color: black` }, 1000);
        }

      }
    })

    message_button.addEventListener('click', () => {

      let box = shadowRoot.querySelector("#frame");
      if (box.style.display === '' || box.style.display === 'none') {
        this.hide_notifications = true;
        box.style.display = 'block';
        this.new_messages_notifications = 0;
        shadowRoot.getElementById('new_message_notification').innerHTML = "";

      }
      else {
        box.style.display = 'none';
        this.hide_notifications = false;
      }


    });

  }


  //#endregion Utils
}
window.customElements.define("ebs-messaging", EbsMessaging);

 
/**Class Element declaration */
/*-------------------NOTIFICATION WEB COMPONENT---------------------------*/
class EbsNotification extends  HTMLElement {
  constructor() {
    super();
    this.totalAlerts = 0;
    this.position = "center";
    this.switchPosition = false;
    this.notifications = 0;
    this.onlyRead = false;
    this.markAllFlag = false;
    this.intervalId = "";
    this.activate_sound = true;
    this.filters = [];
    this.endPoint = "";
    this.Token = "";
    this.headers = {};
    this.initial_counter = 0;
    this.data_persist = null;
    this.reload_when_only_read = false;
    this._frame = "";
    this.intervalId = null;
    this.showBox = false;
    this.notification_client = null;

  }
  disconnectedCallback(){
    if(this.notification_client)
    this.notification_client.unsubscribe();
  }
  /**Connected callback is executed when the html element is mounted, it is async because here we call the graphql API */
  async connectedCallback() {
    let shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.innerHTML = not_template;
    let _image = shadowRoot.querySelector("#icon-bell");
    _image.src= white_bell;
//
    /**Getting the attributes passed to the ebs-notification Web Component from the application that can be Core Breeding or Core System */
    //this.contactId = this.getAttribute("contactId");
    this.endPoint = this.getAttribute("endPoint");
    this.position = this.getAttribute("position");
    this.Token = this.getAttribute("authorization_token");
    this.contactId =  await this.decodeToken(this.Token);

        
    this.client = NotificationCreateApolloClient(this.Token,this.endPoint);

    /**Setting the position of the Web Component */
    let position = this.CalculatePosition(this.position);
    let box = shadowRoot.querySelector(".box");

    box.style.cssText = `left : ${position}px;`;
    let main_header = shadowRoot.querySelector("#main_header")
    main_header.innerHTML = main;

    let slider = shadowRoot.querySelector("#slider_read");
    // let slider_notification = shadowRoot.querySelector("#slider_notifications");
    let notBtn = shadowRoot.querySelector(".notBtn");
    notBtn.addEventListener('mouseover',()=>{
      let total = shadowRoot.querySelector("#total_number");
      total.innerHTML="";
    })

    slider.addEventListener('change', async (e) => {
      this.activate_sound = !this.activate_sound ;
      this.onlyRead = !this.onlyRead;
      this.switchPosition = !this.switchPosition;
      !this.reload_when_only_read ? this.reload_when_only_read = false : this.reload_when_only_read = true;
      if(!this.switchPosition){this.reload_when_only_read = true}
      this.getNotifications(shadowRoot);
    });

   slider.checked = this.switchPosition;

   this.getNotifications(shadowRoot);
   this.notification_client = this.client.subscribe({ query: JOB_LOG_SUBSCRIPTION }).subscribe({
    next:(data)=>{
      this.getNotifications(shadowRoot);
    },
  });

  }
  async getNotifications(shadowRoot){              
       let arrayFilterData = await this.GraphqlCall(this.contactId);
       let data = arrayFilterData.data?.findJobLogList?.content;
       this.data_persist = data;
       let totalElements = arrayFilterData.data?.findJobLogList?.totalElements;
       let data_for_notifications = arrayFilterData.data?.findJobLogList.content.filter(item => item.jobWorkflow.jobType.id === "1");
       let list = shadowRoot.querySelector("#list-cont");
       if (totalElements === 0 || data_for_notifications.length === 0  ) {

           list.innerHTML = noNotifications;
           let _image_not = shadowRoot.querySelector("#image_not");
           _image_not.src = ebs;
   
         }else{
           if(this.initial_counter !== totalElements || this.reload_when_only_read){
                   this.initial_counter = totalElements;
                   this.updateNotifications(shadowRoot,data);
                   this.updateList(data,shadowRoot);
                   this.setEventListenerForComponents(shadowRoot);
                   this.reload_when_only_read = false             
           }

         } 
   }

 updateList(data, shadowRoot){
    let list = shadowRoot.querySelector("#list-cont");
    list.innerHTML = "";
    let todayHeader = true;
    let olderHeader = true;
    let countTodayHeader = 0;
    let countOlderHeader = 0


    if(data.filter(item => item.status === "submitted" && item.jobWorkflow.jobType.id === "1").length  === 0 && this.onlyRead){
  
        list.innerHTML = noNotifications;
        return;
    }

    /**Adding the alerts, messages or request, for every occurrence this segment will be added */
    data.filter(item => item.jobWorkflow.jobType.id === "1").forEach(item => {

      //Added the DateTime Transformations from UTC to Local DateTime

      let date_to_transform = new Date(item.startTime); 
      let local_date = new Date(date_to_transform.getTime() - date_to_transform.getTimezoneOffset() * 60000);
      let date_time = moment.utc(item.startTime).local().format('MMMM Do YYYY, hh:mm:ss a');
      let message_type = item.jobWorkflow.jobType.id;
      let workflowId = item.jobWorkflow.id;
      let objectJSON = `{"itemId":"${item.id}","workflowId":"${workflowId}"}`;
      let time = this.Calculate_Received_Time(local_date);
      /**Calculate the Today or Older header in the messages */
      if (time.isToday) {
        if (countTodayHeader === 0) {
          ++countTodayHeader
        } else {
          todayHeader = false;
        }
      } else {
        if (countOlderHeader === 0) {
          ++countOlderHeader;
        } else {
          olderHeader = false;
        }
      }
      /**Inserting the messages */
      list.innerHTML += `
                        <div class = "sec">
                        <div class = "profCont">
                        ${time.isToday && todayHeader ? '<div><h3 style ="color:gray; font-family:"Raleway": font-size:10pt;">Today</h3></div>' : ''}
                        ${!time.isToday && olderHeader ? '<div><h3 style ="color:gray; font-family:"Raleway": font-size:10pt;">Older</h3></div>' : ''}                   
                      <img class = "profile" alt="No image" src =${message_type === "1" ?
                           bell_icon
                        : envelope_icon}>
                        </div>
                       
                        <div style ="float:right;font-size:7pt;color:gray;font-family:'Raleway'; padding-left:70px; padding-right:10px">
                            <ul>
                                  <li class="time" style="padding:2px;float:right;"> ${time.response}
                                  </li>
                                  <br/>
                                  <br/>
                                  <li style="padding:2px;float:right;">
                                    <input type="checkbox" id='read' value=${objectJSON}  ${item.status === "submitted" ? 'checked' : 'disabled'}/>
                                  </li>
                                  <br/>
                                  <br/>
                                  <li style="padding:2px; float:right;">
                                  <a id="goToActionLink" href="#" style ="font-family:'Raleway';cursor:pointer;font-family:'Raleway'; font-size:8pt;">
                                  
                                  </a>
                                  </li>
                          </ul>
                          </div>
                        <div class = "txt" ${item.status === "submitted" ? 'style="font-weight: 900;" ' : ""}>
                         ${item.message.message.title}
                        </div>
                        <div class = "sub txt" style="color:gray; font-size:8pt;" >${item.message.message.description}</div>

                        <div class = "sub txt" style="color:gray; font-size:8pt;" >${date_time}</div>
                    </div> `;
    });
    /**Once the alerts, messages or requests are added in the Web Component set the events for the controls located in index.html, main_head.html and NotFound.html */

 
}
setEventListenerForComponents(shadowRoot){
  // let go_to_action_link = shadowRoot.querySelectorAll("#goToActionLink");
  // go_to_action_link.forEach(element => {
  //   element.addEventListener('click', (e) => {
  //     let ebs_messaging = document.querySelector("ebs-messaging").shadowRoot;
  //     this._frame = ebs_messaging.querySelector("#frame");
  //      this._frame.style.display = 'block';
  //     })
  // });
    let elements = shadowRoot.querySelectorAll('#read');// radio button to show the status unread=blank or read=green

    let checkboxes = [];

    let checkbox_alert = shadowRoot.querySelectorAll("#check_type_Alert");//checkbox for filtering Alert, Message, Email
    checkboxes.push(checkbox_alert);

    let checkbox_action = shadowRoot.querySelectorAll("#check_type_Action");
    checkboxes.push(checkbox_action);

    let checkbox_message = shadowRoot.querySelectorAll("#check_type_Email");
    checkboxes.push(checkbox_message);

    let clear_filter_button = shadowRoot.querySelector("#filter_clear");

    let mark_all_button = shadowRoot.querySelector("#mark_all_btn");//mark all as read button 

    let apply_btn = shadowRoot.querySelector("#apply_btn");// apply filtering button

    /** Persist the filter combination in the list */
    if (this.filters.length > 0) {
      checkboxes.forEach(e => {
        if (this.filters.includes(e[0].value)) {
          let elementId = `#check_type_${e[0].value}`;
          let checkBox = shadowRoot.querySelector(elementId);
          checkBox.checked = true;
        }

      })
    }


    /**Events*/
    clear_filter_button.addEventListener('click', () => {
      this.filters = [];
    })

    apply_btn.addEventListener("click", () => {
      checkboxes.forEach(e => {
        if (e[0].checked) {
          if (!this.filters.includes(e[0].value)) {
            this.filters.push(e[0].value);
          }
        }
      });
      this.Apply_filters(shadowRoot);
    });

    mark_all_button.addEventListener("click", () => {
       this.markAllFlag = !this.markAllFlag;
     this.MarkAllAsRead(shadowRoot,this.data_persist);
    })

    elements.forEach(element => {
      element.addEventListener('click', (e) => {
        let item = e.target.value;
        this.MarkAsRead(shadowRoot, item);

      })

    });

   
}
  updateNotifications(shadowRoot, data){
    
    let total = shadowRoot.querySelector("#total_number");
    /**Calculating the number of notifications, this number will appear in the badge, also added the shaking effect to the icon bell */
    let new_notifications = data.filter(item => item.status === "submitted" && item.jobWorkflow.jobType.id === "1").length;
    if (this.notifications !== new_notifications) {
     // let total_new_notifications = new_notifications - this.notifications; // used in desktop notifications
      this.notifications = new_notifications;
      if (this.activate_sound && this.notifications > 0) {
        // let notificationSound =
        //   new Audio('https://firebasestorage.googleapis.com/v0/b/ebs-project-21967.appspot.com/o/notification-tone-swift-gesture.mp3?alt=media&token=6799f788-858d-46a2-b168-24e5dc42ca6f');
        // notificationSound.play();
        let icon_bell = shadowRoot.getElementById("icon-bell");
        icon_bell.className = "shake";
        setTimeout(() => { icon_bell.className = ""; }, 1000);
      }

     // this.ShowNotification(total_new_notifications); // hide or show the desktop notifications
    }
    if (this.notifications < 1) {
      total.innerHTML = "";
    }
    if (new_notifications > 0) {
      total.innerHTML = new_notifications < 10 ? new_notifications : '9+'
    }
  }
async decodeToken(token){
  let token_decoded = jwtDecode(token);
  let userName = (token_decoded['http://wso2.org/claims/emailaddress'] != undefined) ? token_decoded['http://wso2.org/claims/emailaddress'] : token_decoded['email'];
  const  contactId = await this.getUserInfo(userName.toLowerCase())
  return contactId;
}

  async getUserInfo(userName) {
    this.headers = {
      "content-type": "application/json",
      "Authorization": `Bearer ${this.Token}`
    };

    let disjunctionFilters = false;
    let filtersArray = [{col:"userName", val:userName, mod:"EQ"}];

    const graphqlQuery = {
      "query": FIND_USER_NOT,
      "variables": {
        filters: filtersArray,
        disjunctionFilters: disjunctionFilters,
      }
    };

    const response = await axios.request({
      url: this.endPoint,
      method: 'post',
      headers: this.headers,
      data: graphqlQuery
    });

    if (response.data.errors) {
      return 0;
    }
    else {
      if (response.data.data.findUserList.totalElements > 0)
        return response.data.data.findUserList.content[0].contact.id;
      else
        return 0;
    }

  }
  /**Graphql Call */
  async GraphqlCall(contactId) {
    this.headers = {
      "content-type": "application/json",
      "Authorization": `Bearer ${this.Token}`
    };

    let filters = this.filters;
    let disjunctionFilters = false;
    let filtersArray = [];
    let sortArray = [];
    if (filters.length > 0) {
      disjunctionFilters = true;
      filters.map(filter => {
        filtersArray.push({ col: "jobWorkflow.jobType.name", val: filter, mod: "EQ" })
      })
    }
    filtersArray.push({ col: "contacts.id", val: contactId, mod: "EQ" });

    if (this.onlyRead) {
      filtersArray.push({ col: "status", val: "submitted", mod: "EQ" })
    }

    sortArray.push({ col: "startTime", mod: "DES" });

    const graphqlQuery = {
      "query": QUERY,
      "variables": {
        filters: filtersArray,
        sort: sortArray,
        disjunctionFilters: disjunctionFilters,
      },
    };

    const response = await axios.request({
      url: this.endPoint,
      method: 'post',
      headers: this.headers,
      data: graphqlQuery
    });
    return response.data;
  }
  CalculatePosition(position) {
    let positionValue = -450;
    switch (position) {
      case 'center': return positionValue = positionValue * 1;
      case 'left': return positionValue = positionValue * 2;
      case 'right': return positionValue = positionValue * 0;
      default: return positionValue = -450;
    }

  }
  Apply_filters(shadowRoot) {
    if (this.filters.length > 0) {
    //  this.fetchData(this.contactId, shadowRoot);
    } else {
      console.log("No filters to apply")
    }

  }

  attributeChangedCallback(field, oldVal, newVal) {
  }
  static get observedAttributes() {
    return ['position']
  }

  Calculate_Received_Time(startTime) {

    let start = startTime;
    let now = Date.now();
    let difference = now - start;

    let formatted_response = ""

    let differencesInMinutes = difference / 60000;
    let isToday = (differencesInMinutes / 60) < 24 ? true : false;

    if (differencesInMinutes > 60 && (differencesInMinutes / 60) < 24) {

      formatted_response = `${Math.round(differencesInMinutes / 60)} hours ago.`;

    }
    else if (differencesInMinutes < 60) {

      formatted_response = `${Math.round(differencesInMinutes)} minutes ago.`;

    }
    else {
      if (Math.round((differencesInMinutes / 60) / 24) > 2) {
        formatted_response = (new Date(startTime)).toLocaleDateString('en-US');
      }
      else {
        formatted_response = `${Math.round((differencesInMinutes / 60) / 24)} days ago.`;
      }

    }

    return { response: formatted_response, isToday: isToday };
  }


  /**This function throw a notification on the computer  */
  ShowNotification(total_new_notifications) {

    let options = {
      image: ebs
    }
    if (Notification.permission === "granted") {
      // If it's okay let's create a notification
      const notification = new Notification(`You got a ${total_new_notifications} notifications`, options);
    }
    else if (Notification.permission !== "denied") {
      Notification.requestPermission().then(function (permission) {
        // If the user accepts, let's create a notification
        if (permission === "granted") {
          const notification = new Notification(`You got ${total_new_notifications} notifications`, options);

        }
      });
    }
  }

  MarkAllAsRead(shadowRoot,data) { //ToDo fix this method
    let data_filtered = data.filter(item=> item.status ==="submitted" && item.jobWorkflow.jobType.id === "1" && item.contacts.map(item=> item.id).includes(this.contactId))
    data_filtered.forEach(async (item)=>{
      let objectJSON = `{"itemId":"${item.id}","workflowId":"${item.jobWorkflow.id}"}`;

     await this.MarkAsRead(shadowRoot,objectJSON)

    })


    //ToDo send to backend mutation to update the read status for all items
    this.initial_counter = 0;
    shadowRoot.getElementById('total_number').innerHTML = this.initial_counter;

   // shadowRoot.getElementById("total_number").innerHTML = "";
    let elements = shadowRoot.querySelectorAll('#read');
    elements.forEach(element=>{
      element.checked = false;
      element.disabled = true;  
    })
  }
  async MarkAsRead(shadowRoot, item) {

   await this.update(shadowRoot, --this.notifications, item);

  }

  async update(shadowRoot, total, item) {

    if (total > 0) {
      shadowRoot.getElementById('total_number').innerHTML = total;
    }
 
   await this.UpdateStatus(item);

  }

  async UpdateStatus(item) {
    let objectParse = JSON.parse(item);
    let jobLogObject = {
      id: objectParse.itemId,
      tenantId: 1,
      jobWorkflowId: objectParse.workflowId,
      status: "read",
      contactIds:[this.contactId]

    };

    const graphqlQuery = {
      "query": MUTATION_MODIFY,
      "variables": {
        jobLog: jobLogObject
      }
    };

    const response = await axios.request({
      url: this.endPoint,
      method: 'post',
      headers: this.headers,
      data: graphqlQuery
    });
    return response;


  }

}


window.customElements.define("ebs-notification", EbsNotification);