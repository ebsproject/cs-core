import ShipmentTool from './ShipmentTool';
import React, {Suspense} from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ShipmentTool is in the DOM', () => {
  render(<Suspense fallback={<div>Loading...</div>}><ShipmentTool></ShipmentTool> </Suspense>)
  expect(screen.getByTestId('ShipmentToolTestId')).toBeInTheDocument();
})
