import React, { lazy, Suspense, memo } from "react";

const ShipmentTool = lazy(() =>
  System.import("@ebs/sh").then((module) => ({ default: module.App }))
);
/*
  @param { }: page props,
*/
function ShipmentToolView({}) {
  /*
  @prop data-testid: Id to use inside shipmenttool.test.js file.
 */
  return (
<div data-testid="ShipmentToolTestId">
<ShipmentTool />
</div>

  );
}
// Type and required properties
ShipmentToolView.propTypes = {};
// Default properties
ShipmentToolView.defaultProps = {};

export default memo(ShipmentToolView);
