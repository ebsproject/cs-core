import React, { lazy, Suspense, memo } from "react";

const MarkerDb = lazy(() =>
  System.import("@ebs/md").then((module) => ({ default: module.App }))
);

function MarkerDbView({ }) {
  return (
    <div data-testid="MarkerDbTestId">
      <MarkerDb />
    </div>

  );
}
// Type and required properties
MarkerDbView.propTypes = {};
// Default properties
MarkerDbView.defaultProps = {};

export default memo(MarkerDbView);
