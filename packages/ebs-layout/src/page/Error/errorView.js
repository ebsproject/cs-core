import React from 'react';
import { Core, Styled, Icons } from "@ebs/styleguide";
const { Container, Typography, Button, Box, Grid } = Core;
const { _styled: styled } = Styled;
import { useNavigate } from 'react-router-dom';

const primaryColor = '#00695C';
const accentColor = '#D32F2F';

const StyledContainer = styled(Container)({
  textAlign: 'center',
  paddingTop: '100px',
  color: primaryColor,
});
const StyledBox = styled(Box)({
  display: " flex flex-column",
  justifyContent: "center",
  alignItems: "center",
  margin: '20px 0',
  backgroundColor: '#F1F8E9',
  padding: '20px',
  borderRadius: '10px',
});
const StyledButton = styled(Button)({
  marginTop: '20px',
  backgroundColor: accentColor,
  color: '#ffffff',
  '&:hover': {
    backgroundColor: '#B71C1C',
  },
});

const ErrorSVG = () => (
  <svg width="150" height="150" xmlns="http://www.w3.org/2000/svg" fill={accentColor}>
    <circle cx="60" cy="60" r="60" />
    <line x1="60" y1="20" x2="60" y2="70" stroke="#ffffff" strokeWidth="10" strokeLinecap="round" />
    <circle cx="60" cy="90" r="7" fill="#ffffff" />
  </svg>
);

const ErrorView = ({ error }) => {
  console.error(error)
  return (
    <Grid container direction={"column"}>
      <StyledContainer>
        <StyledBox >
          <Grid container direction="row" sx={{
            justifyContent: "center",
            alignItems: "center",
          }} >
              <ErrorSVG />
          </Grid>
          <Typography variant="h4" component="h1" gutterBottom>
            Oops! Something went wrong.
          </Typography>
          <Typography variant="body1" gutterBottom>
            {error?.message}
          </Typography>
        </StyledBox>
        <StyledButton variant="contained" onClick={() => window.location.href = '/'}>
          Go Back Home
        </StyledButton>
      </StyledContainer>
    </Grid>
  );
}

export default ErrorView;

