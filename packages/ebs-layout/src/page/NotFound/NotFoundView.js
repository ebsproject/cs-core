import React from 'react';
import {Core, Styled} from "@ebs/styleguide";
const { Container, Typography, Button, Box, Grid } = Core;
const { _styled : styled } = Styled;
import { useNavigate } from 'react-router-dom';

const primaryColor = 'rgb(0,150,136)';

const StyledContainer = styled(Container)({
  textAlign: 'center',
  paddingTop: '100px',
});

const StyledBox = styled(Box)({
  backgroundColor: primaryColor,
  color: '#ffffff',
  padding: '20px',
  borderRadius: '10px',
  marginBottom: '20px',
});

const StyledButton = styled(Button)({
  marginTop: '20px',
  backgroundColor: primaryColor,
  color: '#ffffff',
  '&:hover': {
    backgroundColor: 'rgba(0,150,136, 0.8)',
  },
});

function NotFound() {
  const navigate = useNavigate();
  const handleGoHome = () => {
    navigate('/');
  };

  return (
    <Grid container direction="column">
    <StyledContainer>
      <StyledBox>
        <Typography variant="h1" component="h1" gutterBottom>
          404
        </Typography>
        <Typography variant="h4" component="h2" gutterBottom>
          Oops! Page Not Found
        </Typography>
        <Typography variant="body1" gutterBottom>
          Sorry, the page you're looking for doesn't exist.
        </Typography>
      </StyledBox>
      <StyledButton variant="contained" onClick={handleGoHome}>
        Go Back Home
      </StyledButton>
    </StyledContainer>
    </Grid>

  );
}

export default NotFound;
