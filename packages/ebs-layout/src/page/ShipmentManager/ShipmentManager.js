import React, { lazy, Suspense, memo } from "react";

const ShipmentManager = lazy(() =>
  System.import("@ebs/shm").then((module) => ({ default: module.App }))
);
/*
  @param { }: page props,
*/
function ShipmentManagerView({}) {
  /*
  @prop data-testid: Id to use inside shipmentManager.test.js file.
 */
  return (
<div data-testid="ShipmentManagerTestId">
<ShipmentManager />
</div>

  );
}
// Type and required properties
ShipmentManagerView.propTypes = {};
// Default properties
ShipmentManagerView.defaultProps = {};

export default memo(ShipmentManagerView);
