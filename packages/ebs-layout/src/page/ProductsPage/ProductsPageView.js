import React, { useEffect, useState } from "react";
import { Core } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
const {
  Typography,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Box,
  CardActionArea,
} = Core;
import { FormattedMessage } from "react-intl";
import EbsLogo from "assets/images/logos/EBS.png";
import { useSelector, useDispatch, useStore } from "react-redux";
import FavoriteDomain from "components/atoms/FavoriteDomain";
import Progress from "components/atoms/Progress";
import { getContext } from "utils/other/CrossMFE/CrossContext";
import Cookies from "js-cookie";

const ProductsPageView = React.forwardRef(({ children, ...rest }, ref) => {
  const { domainInfo } = useSelector(({ tenant }) => tenant);
  const { userProfile, userInfo } = useSelector(({ user }) => user);
  const navigate = useNavigate();
  const [product, setProducts] = useState([]);
  const [currentProducts, setCurrentProducts] = useState([]);
  let _userInfo = Object.assign({}, userInfo);
  let _domainInfo = Object.assign({}, domainInfo);

  useEffect(() => {
    let _userProfile = Object.assign({}, userProfile);
    if (_userProfile) {
      let domains = getContext();
      const productsArray = _userProfile?.permissions?.applications
        ?.filter((a) => a.id === domains.domainId)
        .map((a) => {
          return a.products;
        });
      const productsList = productsArray.map((product) =>
        product.filter((item) => item.menu_order > 0)
      );
      if (productsList.length > 0) {
        setProducts(
          productsList.map((product) =>
            product.filter(
              (item) => item.menu_order > 0 && item.showMenu === true
            )
          )[0]
        );
        setCurrentProducts(_userProfile.preference.favorites.products);
      }
    }
  }, [userProfile]);

  const handleClickProduct = (product) => {
    Cookies.set(
      "productSelected",
      JSON.stringify({
        id: product.id,
        name: product.name,
        description: product.description,
      })
    );
    setTimeout(() => navigate(`/${_domainInfo.prefix}/${product.path}`), 300);
  };
  if (!_domainInfo || !userProfile || !product) return <Progress />;

  useEffect(() => {
    if (currentProducts.length > 0) {
      currentProducts.map((FavoriteItem) => {
        const item = product

          .filter((a) => a.id === FavoriteItem.id)
          .map((a) => a);
        if (_domainInfo.prefix === FavoriteItem.domain) {
          Cookies.set(
            "productSelected",
            JSON.stringify({
              id: item[0].id,
              name: item[0].name,
              description: item[0].description,
            })
          );
          navigate(`/${_domainInfo.prefix}/${FavoriteItem.prefix}`);
        }
      });
    }
  }, [currentProducts]);

  return (
    <div className="grid-cols-1 gap-14 sm:gap-16 ">
      <Card>
        <CardMedia sx={{ left: "45%", position: "relative" }}>
          <div>
            <img src={EbsLogo} style={{ height: "110px" }} />
          </div>
        </CardMedia>
        <CardHeader
          title={
            <Typography
              variant="h5"
              color="inherit"
              paragraph={true}
              className="font-ebs text-ebs-green-900 text-center font-semibold"
            >
              <FormattedMessage
                id="tnt.comp.comsoon.welcome"
                defaultMessage={`Select one of the following products`}
              />
            </Typography>
          }
        />
        <Box className="grid grid-cols-3 justify-center justify-items-center">
          {product
            .sort(function (a, b) {
              return a.menu_order - b.menu_order;
            })
            .map((product, index) => (
              <CardActionArea
                key={index}
                className="w-4/5 h-auto transition duration-200 ease-in-out transform hover:-translate-y-1 rounded sm:rounded-fully py-2 px-4 md:rounded-lg"
                onClick={() => handleClickProduct(product)}
              >
                <Card className="rounded-sm m-8 hover:w-auto h-5/6">
                  <CardContent>
                    <FavoriteDomain
                      value={product.name}
                      data={product}
                      userProfile={userProfile}
                      userInfo={_userInfo}
                      type="products"
                      prefix={_domainInfo.prefix}
                      nameDomain={_domainInfo.name}
                    />
                    <div className="grid grid-cols-3 gap-4 justify-items-center items-center h-full w-full">
                      <Typography
                        variant="h5"
                        className="font-ebs text-ebs-black-900 text-center col-span-3 font-extrabold text-3xl"
                      >
                        {product.name}
                      </Typography>

                      <Typography
                        variant="h6"
                        className="font-ebs text-ebs-black-900 text-center col-span-3 font-semibold mt-10 mb-5"
                      >
                        {product.description}
                      </Typography>
                    </div>
                  </CardContent>
                </Card>
                <br />
              </CardActionArea>
            ))}
          {/* </FormGroup>
          </FormControl> */}
        </Box>
      </Card>
    </div>
  );
});
export default ProductsPageView;
