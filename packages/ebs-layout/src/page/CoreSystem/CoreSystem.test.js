import CoreSystem from './CoreSystem';
import React,{Suspense} from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('CoreSystem is in the DOM', () => {
  render(<Suspense fallback={<div>Loading...</div>}> <CoreSystem></CoreSystem></Suspense>)
  expect(screen.getByTestId('CoreSystemTestId')).toBeInTheDocument();
})
