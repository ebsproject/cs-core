import React, { lazy, Suspense, memo } from "react";

const CoreSystem = lazy(() =>
  System.import("@ebs/cs").then((module) => ({ default: module.App }))
);

function CoreSystemView({}) {
  return (
    <div data-testid="CoreSystemTestId">
      <CoreSystem />
    </div>

  );
}

export default memo(CoreSystemView);
