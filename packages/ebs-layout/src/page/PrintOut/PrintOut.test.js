import PrintOut from './PrintOut';
import React,{Suspense} from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('PrintOut is in the DOM', () => {
  render(<Suspense fallback={<div>Loading...</div>}><PrintOut></PrintOut></Suspense>)
  expect(screen.getByTestId('PrintOutTestId')).toBeInTheDocument();
})
