import React, { lazy, Suspense, memo } from "react";

const PrintOut = lazy(() =>
  System.import("@ebs/po").then((module) => ({ default: module.App }))
);

function PrintOutView({ }) {

  return (
    <div data-testid="PrintOutTestId">
      <PrintOut />
    </div>

  );
}

export default memo(PrintOutView)