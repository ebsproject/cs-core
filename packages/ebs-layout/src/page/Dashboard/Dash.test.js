import Dash from "./DashView";
import React,{Suspense} from "react";
import { render, cleanup, fireEvent,screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWrapper";

afterEach(cleanup);

test('Dashboard is in the DOM', () => {
  render(<Suspense fallback={<div>Loading...</div>} ><Wrapper > <Dash></Dash></Wrapper> </Suspense>)
  expect(screen.getByTestId('dashTestId')).toBeInTheDocument();
})
