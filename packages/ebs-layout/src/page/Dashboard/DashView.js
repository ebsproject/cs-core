import React, { useEffect, memo, useState } from "react";
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch, useStore } from "react-redux";
import { Core, Icons } from "@ebs/styleguide";
import {
  CardList as TenantList,
  Info as TenantInfo,
  InstanceList as TenantInstance,
} from "components/molecule/Tenant";
import Progress from "components/atoms/Progress";
import Products from "components/molecule/ProductCardList";
import { setUserData, setUserDomains } from "store/ducks/user";
import EbsLogo from "assets/images/logos/EBS.png";
import { useNavigate } from "react-router-dom";
import { setTenantContext } from "store/ducks/tenant";
import { showMessage } from "store/ducks/message";
import Cookies from "js-cookie";
const {Box, Typography, Card, CardContent, CardHeader, CardMedia } = Core;

function DashView() {
  const { userState } = useSelector(({ auth }) => auth);
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { userProfile, userInfo, domainList } = useSelector(({ user }) => user);
  const [currentDomain, setCurrentDomain] = useState(null);
  const [dataProduct, setDataProduct] = useState([])
  const navigate = useNavigate();

  useEffect(() => {
    if (!userProfile) {
      setUserData(userState)(dispatch, getState);
    } else {
      const domains = userProfile.permissions.applications.map(a => a.domain);
      const favoriteDomain = userProfile.preference.favorites.domain
      setUserDomains(domains)(dispatch);
      setCurrentDomain(favoriteDomain);
      const _dataProduct = userProfile.preference.favorites.products;
      setDataProduct(_dataProduct)
    }
  }, []);

  useEffect(() => {
    if (currentDomain && currentDomain.length > 0) {
      if (currentDomain[0].mfe) {
        setTenantContext({
          domainId: Number(currentDomain[0].domainId),
        })(dispatch, getState);
        Cookies.remove("filters");
        navigate(`/${currentDomain[0].prefix}`);
      } else {
        currentDomain[0].context
          ? window.open(
            currentDomain[0].context,
            "__blank"
          )
          : dispatch(
            showMessage({
              message: "The component does not have URL",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
      }
    }
  }, [currentDomain])

  if (!userInfo || !userProfile || !domainList) return <Progress />;
  const { person: personInfo } = userInfo.contact;

  return (
    <div
      className="flex flex-row flex-wrap justify-center text-center"
      data-testid={"dashboardTestId"}
    >
      <Box>
          <CardHeader
            title={
              <Typography
                variant="h4"
                color="inherit"
                className="text-center font-semibold"
              >
                <FormattedMessage
                  id="tnt.comp.comsoon.welcome"
                  defaultMessage={`Welcome ${personInfo.familyName}, ${personInfo.givenName} to Enterprise Breeding System (EBS)`}
                />
              </Typography>
            }
          />
          <div style={{ display:'flex', justifyContent:'center' }}>
          <CardMedia>
            <img src={EbsLogo} style={{ height: "180px" }} />
          </CardMedia>
          </div>
          {/* <CardContent className="grid grid-cols-1 w-full flex-wrap flex-col justify-center justify-items-center">
            <TenantList />
          </CardContent> */}
          <CardContent>
          <TenantInfo />
          <Products domains={domainList} userProfile={userInfo} userInfo={userProfile} dataProduct={dataProduct} />
          </CardContent>
        </Box>
        {/* <Card
          className="hidden">
          <CardContent>
            <TenantInstance />
          </CardContent>
        </Card> */}

    </div>
  );
}

export default memo(DashView);
