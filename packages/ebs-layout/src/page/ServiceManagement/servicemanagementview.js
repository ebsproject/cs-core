import React, { lazy, memo } from "react";

const ServiceManagement = lazy(() =>
  System.import("@ebs/sm").then((module) => ({ default: module.App }))
);

function ServiceManagementView({}) {
  return (
      <ServiceManagement />
  );
}

export default memo(ServiceManagementView);
