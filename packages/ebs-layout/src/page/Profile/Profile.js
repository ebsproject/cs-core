import React, { memo, Fragment, useState, useEffect } from "react";
import { FormattedMessage } from "react-intl";
import { Core, EbsTabsLayout, Styles } from "@ebs/styleguide";
import { setUserData } from "store/ducks/user";
import { useSelector, useStore, useDispatch } from "react-redux";
import Progress from "components/atoms/Progress";
import { modifyUserPreference } from "store/ducks/user";
import FavoriteList from "components/molecule/FavoriteList";
import MemberOfList from "components/molecule/MemberOfList";
import PermissionsList from "components/molecule/PermissionsList";
import RoleList from "components/molecule/RoleList";
const { Box, Typography, Avatar } = Core;

// const { Favorite, Delete, Star } = Icons;

const classes = {
boxHeigth: {
  height: "55em !important",
  background: "#ffffff !important",
  borderStyle: "solid !important",
  borderRadius: "0.5rem !important",
  borderWidth: "1px",
  borderColor: "#E2DAD9",
}
}

const ProfileView = React.forwardRef(({}, ref) => {
  const { userInfo, userProfile } = useSelector(({ user }) => user);


  if (!userInfo || !userProfile) return <Progress></Progress>;
  const { person: personInfo } = userInfo.contact;
  return (
    <div>
      <Box className="grid grid-cols-3 gap-4 rounded-lg shadow-2xl divide-x-2 justify-center bg-gray-50">
        <Box className="justify-self-center">
          <Typography className="font-ebs text-ebs-green-default text-4xl p-6 font-bold text-center pt-80">
            <FormattedMessage
              id="Profile"
              defaultMessage="Profile"
            ></FormattedMessage>
          </Typography>
          <Avatar className="mx-6 w-40 h-40 object-center ml-36"></Avatar>
          <Typography className="font-ebs text-2xl p-6 text-center pt-20">
            <FormattedMessage
              id="name"
              defaultMessage={"Name: "}
            ></FormattedMessage>
            <FormattedMessage
              id="name"
              defaultMessage={`${personInfo?.givenName} ${personInfo?.familyName}`}
            ></FormattedMessage>
          </Typography>
          <Typography className="font-ebs text-2xl p-6 text-center">
            <FormattedMessage
              id="jobTitle"
              defaultMessage={"Job Title: "}
            ></FormattedMessage>
            <FormattedMessage
              id="jobTitleValue"
              defaultMessage={personInfo.jobTitle ? personInfo.jobTitle : "Unknown"}
            ></FormattedMessage>
          </Typography>
          <Typography className="font-ebs text-2xl p-6 text-center pb-80">
            <FormattedMessage
              id="account"
              defaultMessage={"Account: "}
            ></FormattedMessage>
            <FormattedMessage
              id="accountValue"
              defaultMessage={userProfile.account}
            ></FormattedMessage>
          </Typography>
        </Box>
        <Box className="col-span-2 bg-gray-100">
          <EbsTabsLayout
            data-testid={"TenantManagementViewTestId"}
            orientation={"horizontal"}
            tabs={[
              {
                label: <FormattedMessage id="none" defaultMessage="Member OF" />,
                component: (
                  <Box sx={classes.boxHeigth}>
                    <Typography className="font-ebs text-ebs-green-default text-3xl p-6 font-bold">
                      <FormattedMessage
                        id="Member OF"
                        defaultMessage="Member OF"
                      ></FormattedMessage>
                    </Typography>
                    <MemberOfList userProfile={userProfile} />
                  </Box>
                ),
              },
              {
                label: <FormattedMessage id="none" defaultMessage="Permissions" />,
                component: (
                  <Box sx={classes.boxHeigth}>
                    <Typography className="font-ebs text-ebs-green-default text-3xl p-6 font-bold">
                      <FormattedMessage
                        id="Permission"
                        defaultMessage="Permissions"
                      ></FormattedMessage>
                    </Typography>
                    <PermissionsList userProfile={userProfile} />
                  </Box>
                ),
              },
              {
                label: (
                  <FormattedMessage id="none" defaultMessage="Favorites" />
                ),
                component: (
                  <Box sx={classes.boxHeigth}>
                    <Typography className="font-ebs text-ebs-green-default text-3xl p-6 font-bold">
                      <FormattedMessage
                        id="Favorite"
                        defaultMessage="Favorites"
                      ></FormattedMessage>
                    </Typography>
                    <FavoriteList />
                  </Box>
                ),
              },
              {
                label: (
                  <FormattedMessage id="none" defaultMessage="Roles" />
                ),
                component: (
                  <Box sx={classes.boxHeigth}>
                    <Typography className="font-ebs text-ebs-green-default text-3xl p-6 font-bold">
                      <FormattedMessage
                        id="Favorite"
                        defaultMessage="Roles"
                      ></FormattedMessage>
                    </Typography>
                    <RoleList userProfile={userProfile} />
                  </Box>
                ),
              },
            ]}
          />
        </Box>
      </Box>
    </div>
  );
});

ProfileView.propTypes = {};

ProfileView.defaultProps = {};
export default ProfileView;
