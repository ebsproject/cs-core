  import BaseUI from './BaseUI';
import React,{Suspense} from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('BaseUI is in the DOM', () => {
  render(<Suspense fallback={<div>Loading...</div>}> <BaseUI></BaseUI> </Suspense>)
  expect(screen.getByTestId('BaseUITestId')).toBeInTheDocument();
})
