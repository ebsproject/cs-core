import React, { lazy, Suspense, memo } from "react";

const BreedingAnalytics = lazy(() =>
  System.import("@ebs/ba").then((module) => ({ default: module.App }))
);

function BreedingAnalyticsView({}) {
  return (
  //  <Suspense fallback={<div>Loading...</div>}>
      <BreedingAnalytics />
  //  </Suspense>
  );
}
export default memo(BreedingAnalyticsView);
