import Login from "./LoginView";
import React , {Suspense}from "react";
import { render, cleanup, fireEvent,screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWrapper";

afterEach(cleanup);

test('Login is in the DOM', () => {
  render(<Suspense fallback={<div>Loading...</div>} ><Wrapper > <Login></Login></Wrapper> </Suspense>)
  expect(screen.getByTestId('loginTestId')).toBeInTheDocument();
})
