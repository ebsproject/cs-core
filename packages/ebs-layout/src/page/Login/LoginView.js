import React, { memo } from "react";
import { authUrl, clientId, callbackUrl } from "@ebs/root-config";
import { Core, Styles } from "@ebs/styleguide";
import UserMessage from "components/atoms/Message";
import Logo from "assets/images/logos/EBS_Vertical_2.png";
import Domains from "assets/images/branding/ebs_domains.png";
const { AppBar, Grid, Toolbar, List, ListItem, Button, Typography } = Core;
const {useTheme} = Styles;

function LoginView({ }) {
  const theme = useTheme();
  function handleLogin() {
    let url = `${authUrl}?client_id=${clientId}&scope=openid&`;
    const redirectUrl = `redirect_uri=${callbackUrl}&response_type=code`;
    url += redirectUrl;
    window.location.href = url;
  }

  return (
    <div className={`relative ${theme.palette.mode === "dark" ? "bg-white":""}`} data-testid={"loginTestId"} >
      <div
        className={`h-screen ${theme.palette.mode === "dark" ? "bg-black":"bg-ebs-green-default"} `}
        style={{
          clipPath: "polygon(82% 0, 100% 0, 100% 100%, 70% 100%)",
        }}
      />
      <div className="absolute top-0 left-0 w-full">
        <UserMessage />
        <AppBar
          color="transparent"
          elevation={0}
          className={"static h-16 w-full"}
        >
          <Toolbar className="flex justify-between items-center">
            <div>
              <img className={"m-8"} width="180" src={Logo} alt="logo" />
            </div>
            <nav >
              <List className="flex justify-items-center" sx={{left:"70%"}}>
                <ListItem className="bg-transparent">
                  <Button  variant="text"  href="https://ebsproject.atlassian.net/wiki/spaces/EUG/overview">
                    <Typography color="black" className="text-ebs text-2xl uppercase">Products</Typography>
                  </Button>
                </ListItem>
                <ListItem className="bg-transparent">
                  <Button variant="text" href="https://ebs.excellenceinbreeding.org/">
                    <Typography color="black"  className="text-ebs text-2xl uppercase" noWrap>
                      About us
                    </Typography>
                  </Button>
                </ListItem>
                <ListItem className="bg-transparent">
                  <Button variant="text" href="https://ebsproject.atlassian.net/servicedesk/customer/portal/2">
                    <Typography color="black" className="text-ebs text-2xl uppercase">Contact</Typography>
                  </Button>
                </ListItem>
              </List>
            
            </nav>
            <Grid container justifyContent='flex-end'>
            <Button
             className=" w-1/5"
              color="ebs"
              aria-label="Login"
              onClick={handleLogin}
            >
             <Typography className="text-ebs text-2xl uppercase">Login</Typography>
            </Button>
            </Grid>

          </Toolbar>
          <div       sx={{background:"yellow"}} className="w-2/3 p-10">
            <div >
              <Typography
                color="primary"
                variant="h3"
                className=" text-teal-900 text"
              >
                Enterprise Breeding System
              </Typography>
            </div>
            <div >
              <Typography
              variant={"title"}
                align="justify"
                paragraph
                className={`w-2/3 sm:pt-10 text-xl ${theme.palette.mode === "dark" ? "text-black":""}`}
              >
                The Enterprise Breeding System (EBS) is an open-source breeding
                informatics software being developed for crop breeding programs
                serving resource-poor farmers in Africa, Asia and Latin America.
              </Typography>
            </div>
            <div>
              <Typography
                variant={"title"}
                paragraph
                align="justify"
                className={`w-2/3 sm:pt-10 text-xl  ${theme.palette.mode === "dark" ? "text-black":""}`}
              >
                The EBS connects, merges and builds upon existing breeding
                software and data solutions to offer a single powerful tool, so
                that breeders can focus on using data to create better varieties,
                faster.
              </Typography>
            </div>
            <div>
              <div className="mx-auto flex justify-center mt-6 md:mt-10">
                <img
                  className="w-224 h-224 md:h-224 md:w-224 lg:w-320 lg:h-320"
                  src={Domains}
                  alt="logo"
                />
              </div>
            </div>

          </div>


        </AppBar>
      </div>
    </div>
  );
}

export default memo(LoginView);
