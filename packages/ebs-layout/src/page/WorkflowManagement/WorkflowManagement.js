import React, { lazy, memo } from "react";

const WorkflowManagement = lazy(() =>
  System.import("@ebs/workflow").then((module) => ({ default: module.App }))
);
/*
  @param { }: page props,
*/
function Workflow() {
  return (
    <div data-testid="WorkflowManagementTestId">
      <WorkflowManagement />
    </div>
  );
}
// Type and required properties
Workflow.propTypes = {};


export default memo(Workflow);
