import React, { useEffect } from "react";
import { useDispatch, useSelector, useStore } from "react-redux";
import { useNavigate, Navigate, useSearchParams } from "react-router-dom";
import queryString from "query-string";
import EBSSplashScreen from "components/atoms/EBSSplashScreen";
import { getAuthToken } from "store/ducks/auth";
import { setUserData } from "store/ducks/user";
import { useLayoutContext } from "layout/layout-context";

export default function CallbackView() {
  const {setUserState} = useLayoutContext();
  const [searchParams] = useSearchParams();
  const { isSignIn, error, userState } = useSelector(({ auth }) => auth);
  const dispatch = useDispatch();
  const { getState } = useStore();
  useEffect(() => {
    // * obtain Authentication code
    const code = searchParams.get("code");
    if (code) {
      // * Obtain id token
      getAuthToken(code)(dispatch, getState, setUserState);
    } 
  }, []);

  if (isSignIn) {
    return <Navigate to="/dashboard" />;
  } else if (error) {
    return <Navigate to="/login" />;
  } else {
    return <EBSSplashScreen />;
  }
}
