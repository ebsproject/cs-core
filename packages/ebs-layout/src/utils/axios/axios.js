import axios from "axios";
import { restUri, printoutUri, graphqlUri } from "@ebs/root-config";
import { configure } from "axios-hooks";
import LRU from "lru-cache";
import {getTokenId} from "utils/other/CrossMFE/CrossContext"

export const client = axios.create({
  baseURL: restUri,
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

client.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});

const cache = new LRU({ max: 10 });
configure({ client, cache });

export const printoutClient = axios.create({
  baseURL: printoutUri,
  headers: {
    "Content-Type": "application/json",
  },
});

printoutClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});

export const clientRest = axios.create({
  baseURL: graphqlUri.replace("/graphql", ""),
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

clientRest.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});
