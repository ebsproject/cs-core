import Cookies from "js-cookie";
import {
  graphqlUri,
  restUri,
  printoutUri,
  appVersion,
  fileAPIUrl,
  markerDbUrl,
  cbGraphqlUri,
  authConfigLogoutUri,
  clientId,
  authUrl,
} from "@ebs/root-config";

export const getDomainContext = (domainPrefix) => {
  return JSON.parse(Cookies.get("domainContexts"))[domainPrefix];
};

export const getContext = () => {
  return JSON.parse(Cookies.get("auth")).context;
};

export const getAuthState = () => {
  return JSON.parse(Cookies.get("auth")).userState;
};

export const getTokenId = () => {
  return JSON.parse(Cookies.get("auth")).idToken;
};

export const getCoreSystemContext = () => {
  const coreSystemContext = {};
  Object.assign(coreSystemContext, {
    graphqlUri,
    restUri,
    printoutUri,
    appVersion,
    fileAPIUrl,
    markerDbUrl,
    cbGraphqlUri,
    authConfigLogoutUri,
    clientId,
    authUrl,
  });
  return coreSystemContext;
};

export const getGlobalFilters = () => {
  const globalFilters = {};
  // console.log(Cookies.get("filters"));
  const dataCookie = Cookies.get("filters");
  if (dataCookie === undefined) {
    return globalFilters;
  } else {
    Object.assign(globalFilters, JSON.parse(dataCookie));
    return globalFilters;
  }
};
