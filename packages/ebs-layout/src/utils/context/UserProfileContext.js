import React, { createContext, useState, useEffect, useContext } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { clientRest } from 'utils/axios';
import { FIND_USER } from './query';
import { getTokenId } from "../other/CrossMFE/CrossContext";
import axios from 'axios';
import { graphqlUri } from "@ebs/root-config";
const userContext = createContext();
import { setUserProfile as setUserP } from 'store/ducks/user';
import { useLayoutContext } from 'layout/layout-context'

export const UserContextProvider = ({ children }) => {
  const { userState } = useSelector(({ auth }) => auth);
  const [userProfile, _setUserProfile] = useState(null);
  const { setUserProfile, userProfile: user } = useLayoutContext();
  const dispatch = useDispatch()

  useEffect(() => {
    if (userState) {
      const fetchData = async () => {
        try {
          const userName = (userState['http://wso2.org/claims/emailaddress'] != undefined) ? userState['http://wso2.org/claims/emailaddress'].toLowerCase() : userState['email'].toLowerCase();
          const headers = {
            "content-type": "application/json",
            "Authorization": `Bearer ${getTokenId()}`
          };
          const graphql = {
            "query": FIND_USER,
            "variables": {
              page: { number: 1, size: 10 },
              filters: [{ col: "userName", mod: "EQ", val: userName }],
            },
          };
          const { data } = await axios.request({
            url: graphqlUri,
            method: 'post',
            headers: headers,
            data: graphql
          });
          const response = await clientRest.get(`/user/${Number(data.data.findUserList.content[0].id)}`)
          _setUserProfile(response.data);
          setUserProfile(response.data)
          dispatch(setUserP(response.data));
          localStorage.setItem("user_profile", JSON.stringify(response.data))
          localStorage.setItem("user_permissions", JSON.stringify(response.data.permissions))
          localStorage.setItem("id_token", getTokenId())
        } catch (error) {
          console.error('Error fetching user profile:', error);
        }
      };
      fetchData();
    }
  }, [userState]);
  return (
    <userContext.Provider value={{ userProfile, setUserProfile }}>
      {children}
    </userContext.Provider>
  );
};
export default userContext;
export function useUserContext() {
  return useContext(userContext)
}