import axios from "axios";
import Cookies from "js-cookie";
import { FIND_USER } from "./query";
import jwtDecode from "jwt-decode";
import { graphqlUri } from "@ebs/root-config";
import { clientRest } from 'utils/axios';

export const getUserProfile = () => {
  return JSON.parse(localStorage.getItem("user_profile"));

  // const tokenId = JSON.parse(Cookies.get("auth")).idToken;
  // const userState = jwtDecode(tokenId);

  // try {
  //   const userName = (userState['http://wso2.org/claims/emailaddress'] != undefined) ? userState['http://wso2.org/claims/emailaddress'].toLowerCase() : userState['email'].toLowerCase();
  //   const headers = {
  //     "content-type": "application/json",
  //     "Authorization": `Bearer ${tokenId}}`
  //   };
  //   const graphql = {
  //     "query": FIND_USER,
  //     "variables": {
  //       page: { number: 1, size: 10 },
  //       filters: [{ col: "userName", mod: "EQ", val: userName }],
  //     },
  //   };
  //   const { data } = await axios.request({
  //     url: graphqlUri,
  //     method: 'post',
  //     headers: headers,
  //     data: graphql
  //   })

  //   const result = await clientRest.get(`/user/${Number(data.data.findUserList.content[0].id)}`)
  //   return result.data;

  // } catch (error) {
  //   console.error(error)
  // }

};