export const FIND_USER=`
query(
   $page: PageInput,
   $filters:[FilterInput],
   $sort:[SortInput]
   ){
   findUserList(
     page:$page,
     filters:$filters,
     sort:$sort) {
       totalPages
       totalElements
         content{
               id
               userName    
             }        
   }
 
 }`;