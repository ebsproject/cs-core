import { gql } from "@apollo/client";

export const FIND_WORKFLOW_LIST = gql`
  query findWorkflowList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findWorkflowList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        description
        sortNo
        showMenu
        navigationName
        securityDefinition
        product{
              name
              id
              path
              description
              menuOrder
              hasWorkflow
              help
              icon      
    }
      }
    }
  }
`;
