import gql from "graphql-tag";

// * QUERIES

export const GET_ROLE_BY_ID = gql`
  query findUser($id: ID!) {
    findRole(id: $id) {
      id
      name
      productfunctions {
        id
        description
        product {
          id
          name
          domain {
            id
            name
          }
        }
      }
    }
  }
`;

// export const GET_USER_BY_ID = gql`
//   query findUser($id: ID!) {
//     findUser(id: $id) {
//       id
//       userName
//       person {
//         givenName
//         familyName
//         jobTitle
//         email
//       }
//       tenants {
//         id
//         name
//       }
//       roles {
//         id
//         name
//         tenant
//         description
//       }
//       functionalunits {
//         id
//         name
//         tenant
//       }
//     }
//   }
// `;
// * Find user list
export const FIND_USER_LIST = gql`
  # Write your query or mutation here
  query findUserList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findUserList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        externalId
        userName
        isActive
        isAdmin
        tenants {
          id
          name
          instances {
            id
            name
            domaininstances {
              domain {
                name
                info
                icon
              }
              context
              sgContext
            }
          }
        }
        contact {
          id
          contactInfos {
            id
            value
            default
            contactInfoType {
              id
              name
            }
          }
          person {
            familyName
            additionalName
            givenName
            jobTitle
            knowsAbout
          }
        }
      }
    }
  }
`;

export const GET_USERS_BY_TENANT = gql`
  query findTenant($id: ID!) {
    findTenant(id: $id) {
      id
      name
      users {
        id
        userName
        contact {
          id
          contactInfos {
            id
            value
            isDefault
            contactInfoType {
              id
              name
            }
          }
          person {
            familyName
            additionalName
            givenName
            gender
            jobTitle
            knowsAbout
          }
        }
      }
    }
  }
`;

export const FIND_CONTACT_LIST = gql`
  query findContactList($number: Int!, $size: Int!) {
    findContactList(page: { number: $number, size: $size }) {
      content {
        id
        contactInfos {
          id
          value
          isDefault
          contactInfoType {
            id
            name
          }
        }
        person {
          givenName
          additionalName
          jobTitle
          knowsAbout
        }
        addresses {
          location
          region
          zipCode
          streetAddress
          country {
            id
            name
          }
        }
      }
    }
  }
`;

// CREATE
export const CREATE_PERSON = gql`
  mutation createPerson($type: PersonInput!) {
    createPerson(PersonTo: $type) {
      id
    }
  }
`;
export const CREATE_USER = gql`
  mutation createUser($type: UserInput!) {
    createUser(UserTo: $type) {
      id
    }
  }
`;
// MODIFY
export const MODIFY_PERSON = gql`
  mutation modifyPerson($type: PersonInput!) {
    modifyPerson(PersonTo: $type) {
      id
    }
  }
`;

// DELETE
export const DELETE_USER = gql`
  mutation deleteUser($id: Int!) {
    deleteUser(iduser: $id)
  }
`;

export const DELETE_PERSON = gql`
  mutation deletePerson($id: Int!) {
    deletePerson(idperson: $id)
  }
`;
// * MODIFY PREFERENCE USER
export const MODIFY_PREFERENCE_USER = gql`
  mutation modifyUser($user: UserInput!) {
    modifyUser(user: $user) {
      id
      externalId
      userName
      isActive
      isAdmin
      tenants {
        id
        name
        instances {
          id
          name
          domaininstances {
            domain {
              name
              info
              icon
            }
            context
            sgContext
          }
        }
      }
      contact {
        id
        contactInfos {
          id
          value
          default
          contactInfoType {
            id
            name
          }
        }
        person {
          familyName
          additionalName
          givenName
          jobTitle
          knowsAbout
        }
      }
    }
  }
`;
