import { gql } from "@apollo/client";
// TODO: QUERIES
/**
 * @graph FIND_CONTACT_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_LIST = gql`
  query FIND_CONTACT_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        purpose {
          id
          name
        }
        category {
          id
          name
        }
        institution {
          commonName
          legalName
          principalContact {
            person {
              givenName
              familyName
            }
          }
        }
        addresses {
          id
          country {
            id
            name
          }
          purposes {
            id
            name
            category {
              name
            }
          }
          location
          region
          zipCode
          streetAddress

        }
        contactTypes {
          id
          name
          category {
            id
            name
          }
        }
        contactInfos {
          id
          value
          default
          contactInfoType {
            id
            name
          }
        }
        person {
          familyName
          givenName
          additionalName
          jobTitle
          gender
        }
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT_INFO_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_INFO_TYPE_LIST = gql`
  query FIND_CONTACT_INFO_TYPE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactInfoTypeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
      }
    }
  }
`;
// Find tenant list to create or modify contact and users tenant relationship
export const FIND_TENANT_LIST = gql`
  query{findTenantList{
    content{
      id
      name
    }
  }
}
`;
/**
 * @graph FIND_CONTACT_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_TYPE_LIST = gql`
  query FIND_CONTACT_TYPE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactTypeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        category {
          id
          name
        }
      }
    }
  }
`;
/**
 * @graph FIND_PURPOSE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_PURPOSE_LIST = gql`
  query FIND_PURPOSE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findPurposeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        category {
          name
        }
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT_INFO_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
 export const FIND_HIERARCHY_LIST = gql`
 query FIND_HIERARCHY_LIST(
   $page: PageInput
   $sort: [SortInput]
   $filters: [FilterInput]
   $disjunctionFilters: Boolean = false
 ) {
  findHierarchyList(
     page: $page
     sort: $sort
     filters: $filters
     disjunctionFilters: $disjunctionFilters
   ) {
    content {
      contact {
        id
        person {
          givenName
          familyName
        }
      }
      institution {
        id
        institution {
          legalName
          commonName
        }
      }
    }
   }
 }
`;
/**
 * @graph FIND_CONTACT
 * @param id: contact id
 */
export const FIND_CONTACT = gql`
  query FIND_CONTACT($id: ID!) {
    findContact(id: $id) {
      id
      category {
        name
        id
      }
      addresses {
        id
        country {
          id
          name
        }
        purpose {
          id
          name
          category {
            name
          }
        }
        location
        region
        zipCode
        streetAddress

      }
      contactTypes {
        id
        name
        category {
          id
          name
        }
      }
      contactInfos {
        id
        value
        default
        contactInfoType {
          id
          name
        }
      }
      person {
        familyName
        givenName
        additionalName
        gender
        institutions {
          commonName
          legalName
        }
      }
      institution {
        commonName
        legalName
        contacts {
          id
          email
          person {
            givenName
            familyName
          }
        }
        principalContact {
          id
          email
          person {
            givenName
          }
        }
      }
      email
      purpose {
        id
        name
      }
    }
  }
`;
/**
 * @graph FIND_COUNTRY_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_COUNTRY_LIST = gql`
  query FIND_COUNTRY_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findCountryList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
      }
    }
  }
`;
// TODO: CREATE
/**
 * @graph CREATE_CONTACT
 * @param contact: {
 * * id: Int!,
 * * category:
 * * ContactTypeCategory!,
 * * contactTypeIds: [Int!],
 * * addresses: [AddressesInput!],
 * * ContactInfos: [ContactInfoInput!],
 * * person: PersonInput,
 * * tenants:[Int!],
 * * institution: InstitutionInput
 * }
 */
export const CREATE_CONTACT = gql`
  mutation CREATE_CONTACT($contact: ContactInput!) {
    createContact(contact: $contact) {
      id
    }
  }
`;
/**
 * @param {HierarchyInput!} hierarchy
 */
export const CREATE_HIERARCHY = gql`
  mutation CREATE_HIERARCHY($hierarchy: HierarchyInput!) {
    createHierarchy(hierarchy: $hierarchy) {
      contact {
        id
      }
    }
  }
`;
/**
 * @param {ContactInfoInput!} contactInfo
 */
export const CREATE_CONTACT_INFO = gql`
  mutation CREATE_CONTACT_INFO($contactInfo: ContactInfoInput!) {
    createContactInfo(contactInfo: $contactInfo) {
      id
    }
  }
`;
/**
 * @graphql CREATE_CONTACT_INFO_TYPE
 * @param {ContactInfoTypeInput} contactInfoType
 */
export const CREATE_CONTACT_INFO_TYPE = gql`
  mutation CREATE_CONTACT_INFO_TYPE($contactInfoType: ContactInfoTypeInput!) {
    createContactInfoType(contactInfoType: $contactInfoType) {
      id
    }
  }
`;
// TODO: MODIFY
/**
  * @graph MODIFY_CONTACT
  * @param contact: { 
  *
  }
*/
export const MODIFY_CONTACT = gql`
  mutation MODIFY_CONTACT($contact: ContactInput!) {
    modifyContact(contact: $contact) {
      id
    }
  }
`;
/**
 * @param {ContactInfoInput!} contactInfo
 */
export const MODIFY_CONTACT_INFO = gql`
  mutation MODIFY_CONTACT_INFO($contactInfo: ContactInfoInput!) {
    modifyContactInfo(contactInfo: $contactInfo) {
      id
    }
  }
`;
// TODO: DELETE
/**
@graph DELETE_CONTACT
@param contactId: Contact id
*/
export const DELETE_CONTACT = gql`
  mutation DELETE_CONTACT($contactId: Int!) {
    deleteContact(contactId: $contactId)
  }
`;
/**
@graph DELETE_CONTACT_INFO
@param contactId: Contact Info id
*/
export const DELETE_CONTACT_INFO = gql`
  mutation REMOVE_CONTACT_INFO($contactInfoId: Int!) {
    removeContactInfo(contactInfoId: $contactInfoId)
  }
`;
/**
 * @graphql REMOVE_ADDRESS
 * @param {Int} id
 */
export const REMOVE_ADDRESS = gql`
  mutation REMOVE_ADDRESS($id: Int) {
    removeAddress(addressId: $id)
  }
`;

/**
 * @graphql DELETE_HIERARCHY
 * @variable {Int} contactId
 * @variable {Int} institutionId
 */
 export const DELETE_HIERARCHY = gql`
 mutation DELETE_HIERARCHY($contactId: Int!, $institutionId: Int!) {
   deleteHierarchy(contactId: $contactId, institutionId: $institutionId)
 }
`;
