import gql from "graphql-tag";

export const FIND_FILTER = gql`
  query findFilter($id: ID!) {
    findFilter(id: $id) {
        id
        apiUrl
        body
        description
        hierarchyDesign{
            id
            name
        }
        tooltip
        isGraphService
        paramPath
        isSystem
    }
  }
`;
// * FIND_PRODUCT_LIST
export const FIND_FILTER_LIST = gql`
  query findFilterList(  
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findFilterList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        apiUrl
        body
        description
        hierarchyDesign{
            id
            name
        }
        tooltip
        isGraphService
        paramPath
        isSystem
      }
    }
  }
`;
// * Create domain instance
export const CREATE_FILTER = gql`
  mutation createFilter($type: FilterInput!) {
    createFilter(FilterTo: $type) {
      id
    }
  }
`;