import fetch from "cross-fetch";
import { getTokenId } from "utils/other/CrossMFE/CrossContext";
import { graphqlUri } from "@ebs/root-config";
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from "@apollo/client";

const httpLink = new HttpLink({
  uri: graphqlUri,
  fetch,
});

const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      authorization: `Bearer ${getTokenId()}`,
    },
  }));
  return forward(operation);
});

export const client = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});
