import { FIND_CONTACT } from "utils/apollo/gql/crm";

export const mocks = [
    {
      request: {
        query: FIND_CONTACT,
        variables: {
          id: 1,
        },
      },
      result: {
        data: {
          findContact: {
            contactTypes: [
              {
                id: 9,
                name: "EBS member",
                category: "Person",
              },
            ],
            addresses: [
              {
                id: "2",
                location: "Loc",
                region: "R",
                zipCode: "9999",
                streetAddress: "Mexico-Veracruz, El Batan Km. 45, 56237 Mex.",
                country: {
                  id: 137,
                  name: "Mexico",
                },
              },
            ],
            contactInfos: [
              {
                id: 76,
                value: "e.briones@cimmyt.org",
                contactInfoType: {
                  id: 2,
                },
              },
              {
                id: 77,
                value: "(123) 123-1231",
                contactInfoType: {
                  id: 1,
                },
              },
            ],
          },
        },
      },
    },
  ];