import PropTypes from "prop-types";
import { getContext } from "utils/other/CrossMFE/CrossContext";
import { Core } from "@ebs/styleguide";
import { useSelector } from "react-redux";
import React, { useEffect, useState } from "react";
import { useLayoutContext } from "layout/layout-context";
import Cookies from "js-cookie";

const { Typography } = Core;


export const RBAC = (
  { allowedAction, children, showComponent, domain, product },
  ref
) => {
  const [productSelected, setProductSelected] = useState(null);
  useEffect(()=>{
    const _ps = Cookies.get("productSelected") && JSON.parse( Cookies.get("productSelected") );
    setProductSelected(_ps)
  },[]);
  const { userProfile } = useLayoutContext();
  if (
    allowedAction === "Chat_View" ||
    allowedAction === "Notification_View"
  ) {
    return children;
  }
  if (!userProfile || !productSelected) return <></>;
  try {
    const domains = getContext();
    const selectedProduct = productSelected;
    const dataActions = userProfile?.permissions?.applications
      ?.filter((a) => a.id === domains.domainId)
      .map((a) => {
        return a?.products
          .filter((a) => a?.name?.includes(product || selectedProduct?.name))
          .map((a) => {
            return a?.actions.concat(a?.dataActions);
          });
      });
    if (dataActions && dataActions.length > 0) {
      const result = dataActions[0];
      let finalActions = [];
      let actions = result[0]?.map((obj) => Object.values(obj || []));
      for (let i = 0; i < actions?.length; i++) {
        for (let j = 0; j < actions[i][0]?.length; j++) {
          finalActions.push(actions[i][0][j]);
        }
      }
      const access = finalActions?.includes(allowedAction);
      if (
        !access &&
        selectedProduct?.name === "Printout" &&
        (allowedAction === "Export_Backend" ||
          allowedAction === "Import_Backend")
      ) {
        return (
          <div>
            <Typography className="font-ebs text-xs text-red-700 px-5">
              {allowedAction === "Export_Backend"
                ? "No privileges to export"
                : "No privileges to import"}
            </Typography>
          </div>
        );
      }
      if (showComponent && !access) {
        children = React.Children.map(children, (child) => (
          <div
            style={{ opacity: 0.5, pointerEvents: "none", background: "#CCC" }}
          >
            {child}
          </div>
        ));
        return children;
      }
      return access && children;
    }
  } catch (error) {
    console.log(error);
    children = React.Children.map(children, (child) => (
      <div style={{ opacity: 0.5, pointerEvents: "none", background: "#CCC" }}>
        {child}
      </div>
    ));
    return children;
  }
};
RBAC.propTypes = {
  allowedActions: PropTypes.string,
  children: PropTypes.element,
};
