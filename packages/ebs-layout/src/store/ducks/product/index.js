import { printoutClient } from "utils/axios";

/*
 Initial state and properties
 */
export const initialState = {
  productSelected: null,
  templateList: null,
};
/*
 Action types
 */
export const SET_PRODUCT_SELECTED = "[product] SET_PRODUCT_SELECTED";
export const SET_LIST_TEMPLATE = "[templeate] SET_LIST_TEMPLATE";
/*
 Arrow function for change state
 */
export const setProductSelected = (payload) => ({
  type: SET_PRODUCT_SELECTED,
  payload,
});

export const setTempleteProduct = (payload) => ({
  type: SET_LIST_TEMPLATE,
  payload,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_PRODUCT_SELECTED:
      return {
        ...state,
        productSelected: payload,
      };
    case SET_LIST_TEMPLATE:
      return {
        ...state,
        templateList: payload,
      };
    default:
      return state;
  }
}

export const findListTemplate = (idProduct) => async (dispatch) => {
  try {
    const { data } = await printoutClient.get(
      `/api/Template/product/` + idProduct
    );
    dispatch(setTempleteProduct(data.result.data))
  } catch (error) {
    console.log(error);
  }
};
