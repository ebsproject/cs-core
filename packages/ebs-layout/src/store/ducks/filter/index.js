import client from "utils/apollo";
import { showMessage } from "store/ducks/message";
import Cookies from "js-cookie";
import { getContext } from "utils/other/CrossMFE/CrossContext";
import { FIND_FILTER, FIND_FILTER_LIST } from "utils/apollo/gql/filter";
/*
 Here goes initial state and properties to be changed
 */
export const initialState = {
  filterListToApply: {},
  filterGridToApply: [],
  filterList: null,
  loading: false,
  error: null,
  tenantContext: (Cookies.get("auth") &&
    JSON.parse(Cookies.get("auth")).context) || {
    tenantId: 0,
    instanceId: 0,
    domainId: 0,
  },
};
/*
 Action types
 */
const GET_FILTERS = "[FILTER] GET_FILTERS";
const SET_FILTERS = "[FILTER] SET_FILTERS";
const FILTER_ERROR = "[FILTER] FILTER_ERROR";
const SET_FILTERS_TO_APPLY = "[FILTER] SET_FILTERS_TO_APPLY";
const SET_FILTERS_GRID_TO_APPLY = "[FILTER] SET_FILTERS_GRID_TO_APPLY";

/*
 Arrow function for change state
 */
const getFilters = () => ({
  type: GET_FILTERS,
});
const setFilters = (filterList) => ({
  type: SET_FILTERS,
  payload: filterList,
});
const filterFailure = (error) => ({
  type: FILTER_ERROR,
  payload: error,
});
const setFilterListToApply = (filterListToApply) => ({
  type: SET_FILTERS_TO_APPLY,
  payload: filterListToApply,
});
const setFilterGridToApply = (filterGridToApply) => ({
  type: SET_FILTERS_GRID_TO_APPLY,
  payload: filterGridToApply,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case GET_FILTERS:
      return {
        ...state,
        loading: true,
      };
    case SET_FILTERS_TO_APPLY:
      return {
        ...state,
        loading: false,
        filterListToApply: payload,
      };
      case SET_FILTERS_GRID_TO_APPLY:
      return {
        ...state,
        loading: false,
        filterGridToApply: payload,
      };
    case SET_FILTERS:
      return {
        ...state,
        loading: false,
        filterList: payload,
      };
    case FILTER_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}

export const findFilterList = (domainId) => async (dispatch, getState) => {
  dispatch(getFilters());
  client
    .query({
      query: FIND_FILTER_LIST,
      variables: {
        filters: [
          {
            col: "description",
            mod: "EQ",
            val: "Program",
          },
        ],
      },
    })
    .then(({ data }) => {
      dispatch(setFilters(data.findFilterList.content));
    })
    .catch(({ message }) => {
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(filterFailure(message));
    });
};

export const filterListToApply_f = (filterArray) => async (dispatch) => {
  dispatch(setFilterListToApply(filterArray));
};

export const filterGridToApply = (filterArray) => async (dispatch) => {
  dispatch(setFilterGridToApply(filterArray));
};
