import client from "utils/apollo";
import { showMessage } from "store/ducks/message";
import Cookies from "js-cookie";
import {
  FIND_TENANT,
  FIND_INSTANCE,
  FIND_DOMAIN,
  FIND_PRODUCT_LIST,
  FIND_PROGRAM_LIST,
} from "utils/apollo/gql/tenant";
/*
 Here goes initial state and properties to be changed
 */
export const initialState = {
  tenantInfo: null,
  instanceInfo: null,
  domainInfo: null,
  productList: null,
  programList: null,
  loading: false,
  error: null,
  tenantContext: (Cookies.get("auth") &&
    JSON.parse(Cookies.get("auth")).context) || {
    tenantId: 1,
    instanceId: 1,
    domainId: 0,
  },
};
/*
 Action types
 */
const GET_PRODUCTS = "[TENANT] GET_PRODUCTS";
const SET_PRODUCTS = "[TENANT] SET_PRODUCTS";
const GET_PROGRAMS = "[TENANT] GET_PROGRAMS";
const SET_PROGRAMS = "[TENANT] SET_PROGRAMS";
const GET_INSTANCE = "[TENANT] GET_INSTANCE";
const SET_INSTANCE = "[TENANT] SET_INSTANCE";
const GET_TENANT = "[TENANT] GET_TENANT";
const SET_TENANT = "[TENANT] SET_TENANT";
const GET_DOMAIN = "[TENANT] GET_DOMAIN";
const SET_DOMAIN = "[TENANT] SET_DOMAIN";
const TENANT_ERROR = "[TENANT] TENANT_ERROR";
const CHANGE_TENANT_CONTEXT = "[TENANT] CHANGE_TENANT_CONTEXT";

/*
 Arrow function for change state
 */
const getProducts = () => ({
  type: GET_PRODUCTS,
});
const setProducts = (productList) => ({
  type: SET_PRODUCTS,
  payload: productList,
});
const getPrograms = () => ({
  type: GET_PROGRAMS,
});
const setPrograms = (programList) => ({
  type: SET_PROGRAMS,
  payload: programList,
});
const getInstance = () => ({
  type: GET_INSTANCE,
});
const setInstance = (instanceInfo) => ({
  type: SET_INSTANCE,
  payload: instanceInfo,
});
const getTenant = () => ({
  type: GET_TENANT,
});
const setTenant = (tenantInfo) => ({
  type: SET_TENANT,
  payload: tenantInfo,
});
const tenantFailure = (error) => ({
  type: TENANT_ERROR,
  payload: error,
});
const getDomain = () => ({
  type: GET_DOMAIN,
});
const setDomain = (domainInfo) => ({
  type: SET_DOMAIN,
  payload: domainInfo,
});
const setContext = (newContext) => ({
  type: CHANGE_TENANT_CONTEXT,
  payload: newContext,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case GET_PRODUCTS:
      return {
        ...state,
        loading: true,
      };
    case SET_PRODUCTS:
      return {
        ...state,
        loading: false,
        productList: payload,
      };
    case GET_PROGRAMS:
      return {
        ...state,
        loading: true,
      };
    case SET_PROGRAMS:
      return {
        ...state,
        loading: false,
        programList: payload,
      };
    case GET_INSTANCE:
      return {
        ...state,
        loading: true,
      };
    case SET_INSTANCE:
      return {
        ...state,
        loading: false,
        instanceInfo: payload,
      };
    case GET_TENANT:
      return {
        ...state,
        loading: true,
      };
    case SET_TENANT:
      return {
        ...state,
        loading: false,
        tenantInfo: payload,
      };
      case  CHANGE_TENANT_CONTEXT:
        return {
          ...state,
          tenantContext: payload,
        };
    case GET_DOMAIN:
      return {
        ...state,
        loading: true,
      };
    case SET_DOMAIN:
      return {
        ...state,
        loading: false,
        domainInfo: payload,
      };
    case TENANT_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}
export const setTenantContext = ({ tenantId, instanceId, domainId }) => (dispatch, getState) => {

  let authState = Cookies.get("auth") && JSON.parse(Cookies.get("auth"));

  let { tenantContext } = getState().tenant;

  let newTenantContext = Object.assign({},tenantContext );
  if (tenantId) {
    newTenantContext = { ...newTenantContext, tenantId };
    authState = { ...authState, context: newTenantContext };
    Cookies.set("auth", JSON.stringify(authState), { sameSite: "Lax" });
    dispatch(setContext(newTenantContext));
  }

  if (instanceId) {
    newTenantContext = { ...newTenantContext, instanceId };
    authState = { ...authState, context: newTenantContext };
    Cookies.set("auth", JSON.stringify(authState), { sameSite: "Lax" });
    dispatch(setContext(newTenantContext));
  }
  if (domainId) {
    newTenantContext = { ...newTenantContext, domainId };
    authState = { ...authState, context: newTenantContext };
    Cookies.set("auth", JSON.stringify(authState), { sameSite: "Lax" });
    dispatch(setContext(newTenantContext));
  }
};

export const findTenant = (id) => async (dispatch, getState) => {
  dispatch(getTenant());
  client
    .query({
      query: FIND_TENANT,
      variables: {
        id: id,
      },
    })
    .then(({ data }) => {
      dispatch(setTenant(data.findTenant));
    })
    .catch(({ message }) => {
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(tenantFailure(message));
    });
};

export const findInstance = (id) => async (dispatch, getState) => {
  if(id > 0){
  dispatch(getInstance());
  client
    .query({
      query: FIND_INSTANCE,
      variables: {
        id: id,
      },
    })
    .then(({ data }) => {
      let domainContext = new Object();
      data.findInstance.domaininstances.map((domain, key) => {
        domainContext[domain.domain.prefix] = {
          context: domain.context,
          sgContext: domain.sgContext,
        };
      });
      Cookies.set("domainContexts", JSON.stringify(domainContext), {
        sameSite: "Lax",
      });
      dispatch(setInstance(data.findInstance));
    })
  }
    // .catch(({ message }) => {
    //   dispatch(
    //     showMessage({
    //       message: "no paso",
    //       variant: "error",
    //       anchorOrigin: {
    //         vertical: "top",
    //         horizontal: "right",
    //       },
    //     })
    //   );
    //   dispatch(tenantFailure(message));
    // });
};

export const findDomain = (id) => async (dispatch, getState) => {
  dispatch(getDomain());
  client
    .query({
      query: FIND_DOMAIN,
      variables: {
        id: id,
      },
    })
    .then(({ data }) => {
      dispatch(setDomain(data.findDomain));
    })
    .catch(({ message }) => {
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(tenantFailure(message));
    });
};

export const findProductList = (domainId) => async (dispatch, getState) => {
  dispatch(getProducts());
  client
    .query({
      query: FIND_PRODUCT_LIST,
      variables: {
        page: { number: 1, size: 100 },
        filters: [{ mod: "EQ", col: "domain.id", val: domainId }],
      },
    })
    .then(({ data }) => {
      dispatch(setProducts(data.findProductList.content));
    })
    .catch(({ message }) => {
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(tenantFailure(message));
    });
};

export const findProgramList = (tenantId) => async (dispatch, getState) => {
  dispatch(getPrograms());
  client
    .query({
      query: FIND_PROGRAM_LIST
    })
    .then(({ data }) => {
      dispatch(setPrograms(data.findProgramList.content));
    })
    .catch(({ message }) => {
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(tenantFailure(message));
    });
};
