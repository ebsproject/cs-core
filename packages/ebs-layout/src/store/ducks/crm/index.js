import { client } from "utils/apollo";
import { showMessage } from "store/ducks/message";
import _ from "lodash";
import {
  CREATE_CONTACT,
  MODIFY_CONTACT,
  DELETE_CONTACT,
  REMOVE_ADDRESS,
  DELETE_CONTACT_INFO,
  CREATE_CONTACT_INFO,
  MODIFY_CONTACT_INFO,
  CREATE_HIERARCHY,
  CREATE_CONTACT_INFO_TYPE,
} from "utils/apollo/gql/crm";
/*
 Initial state and properties
 */
export const initialState = {
  error: null,
  loading: false,
  success: false,
  contactInfoSuccess: false,
};
/*
 Action types
 */
const ERROR = "[CRM] ERROR";
const MUTATION_CREATE_CONTACT_START = "[CRM] MUTATION_CREATE_CONTACT_START";
const MUTATION_CREATE_CONTACT_SUCCESS = "[CRM] MUTATION_CREATE_CONTACT_SUCCESS";
const MUTATION_MODIFY_CONTACT_START = "[CRM] MUTATION_MODIFY_CONTACT_START";
const MUTATION_MODIFY_CONTACT_SUCCESS = "[CRM] MUTATION_MODIFY_CONTACT_SUCCESS";
const MUTATION_DELETE_CONTACT_START = "[CRM] MUTATION_DELETE_CONTACT_START";
const MUTATION_DELETE_CONTACT_SUCCESS = "[CRM] MUTATION_DELETE_CONTACT_SUCCESS";
const MUTATION_CREATE_HIERARCHY_START = "[CRM] MUTATION_CREATE_HIERARCHY_START";
const MUTATION_CREATE_HIERARCHY_SUCCESS =
  "[CRM] MUTATION_CREATE_HIERARCHY_SUCCESS";
const MUTATION_ASSIGN_NEW_CONTACT_START =
  "[CRM] MUTATION_ASSIGN_NEW_CONTACT_START";
const MUTATION_ASSIGN_NEW_CONTACT_SUCCESS =
  "[CRM] MUTATION_ASSIGN_NEW_CONTACT_SUCCESS";
const MUTATION_CREATE_CONTACT_INFO_START =
  "[CRM] MUTATION_CREATE_CONTACT_INFO_START";
const MUTATION_CREATE_CONTACT_INFO_SUCCESS =
  "[CRM] MUTATION_CREATE_CONTACT_INFO_SUCCESS";
const MUTATION_MODIFY_CONTACT_INFO_START =
  "[CRM] MUTATION_MODIFY_CONTACT_INFO_START";
const MUTATION_MODIFY_CONTACT_INFO_SUCCESS =
  "[CRM] MUTATION_MODIFY_CONTACT_INFO_SUCCESS";
const MUTATION_DELETE_CONTACT_INFO_START =
  "[CRM] MUTATION_DELETE_CONTACT_INFO_START";
const MUTATION_DELETE_CONTACT_INFO_SUCCESS =
  "[CRM] MUTATION_DELETE_CONTACT_INFO_SUCCESS";
export const MUTATION_CREATE_INFO_TYPE_START =
  "[CRM] MUTATION_CREATE_INFO_TYPE_START";
export const MUTATION_CREATE_INFO_TYPE_SUCCESS =
  "[CRM] MUTATION_CREATE_INFO_TYPE_SUCCESS";
const CLEAN_CONTACT_SUCCESS = "[CRM] CLEAN_CONTACT_SUCCESS";
const CLEAN_CONTACT_INFO_SUCCESS = "[CRM] CLEAN_CONTACT_INFO_SUCCESS";
/*
 Arrow function for change state
 */
const process = (type) => ({
  type,
});
const setError = (payload) => ({
  type: ERROR,
  payload,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case MUTATION_CREATE_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_MODIFY_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_DELETE_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_HIERARCHY_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_HIERARCHY_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_DELETE_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_ASSIGN_NEW_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_ASSIGN_NEW_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_CREATE_CONTACT_INFO_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: true, loading: false };
    case MUTATION_MODIFY_CONTACT_INFO_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: true, loading: false };
    case MUTATION_DELETE_CONTACT_INFO_START:
      return { ...state, loading: true };
    case MUTATION_DELETE_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: true, loading: false };
    case MUTATION_CREATE_INFO_TYPE_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_INFO_TYPE_SUCCESS:
      return { ...state, loading: false, contactInfoSuccess: true };
    case CLEAN_CONTACT_SUCCESS:
      return { ...state, success: false };
    case CLEAN_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: false };
    case ERROR:
      return { ...state, loading: false, error: payload };
    default:
      return state;
  }
}
/**
 * @param {String} type
 * @param {Object} data
 * @returns void
 */
export const assignNewContact =
  ({ type, data }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_ASSIGN_NEW_CONTACT_START));
      // * Contact Infos
    /*  let contactInfos = new Array();
      data.phones.map((item, index) =>
        contactInfos.push({
          id: item.id || 0,
          value: item.value,
          default: index === 0 ? true : false,
          contactInfoTypeId: 1,
        })
      );*/
      // * Contact Input
      let contactInput = new Object({
        id: 0,
        contactTypeIds: data.contactType.map((item) => item.id),
        addresses: data.addresses.map((item) => ({
        //  default: item.default || false,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.country.id,
          purpose: { id: item.purpose.id },
          id: 0,
        })),
       // contactInfos: contactInfos,
        person: {
          familyName: data.familyName,
          givenName: data.givenName,
          additionalName: data.additionalName,
          gender: data.gender.name,
        },
        institution: null,
        email: data.officialEmail,
        purpose: { id: data.purpose.id },
      });
      // * Category
      switch (type) {
        case "PERSON":
          Object.assign(contactInput, {
            category: { id: 1, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        case "INSTITUTION":
          Object.assign(contactInput, {
            category: { id: 2, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        default:
          break;
      }
      const { errors, data: contactData } = await client.mutate({
        mutation: CREATE_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      const { errors: Assign } = await client.mutate({
        mutation: CREATE_HIERARCHY,
        variables: {
          hierarchy: {
            contact: {
              id: contactData.createContact.id,
              category: { id: 1 },
              purpose: { id: 1 },
            },
            institution: {
              id: Number(data.institutionId),
              category: { id: 1 },
              purpose: { id: 1 },
            },
            principalContact: false,
          },
        },
      });
      if (Assign) {
        throw new Error(`${Assign[0].message}`);
      }
      dispatch(process(MUTATION_ASSIGN_NEW_CONTACT_SUCCESS));
      dispatch(
        showMessage({
          message: "Contact created and assigned successfully",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAN_CONTACT_SUCCESS));
    }
  };
/**
 * @param {String} type
 * @param {Object} data
 * @returns void
 */
export const createContact =
  ({ type, data }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_CREATE_CONTACT_START));
      // * Contact Infos
      /*let contactInfos = new Array();
      data.phones.map((item, index) =>
        contactInfos.push({
          id: item.id || 0,
          value: item.value,
          default: index === 0 ? true : false,
          contactInfoTypeId: 1,
        })
      );*/
      // * Contact Input
      let contactInput = new Object({
        id: 0,
        contactTypeIds: data.contactType.map((item) => item.id),
        addresses: data.addresses.map((item) => ({
          //default: item.default || false,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.country.id,
          purpose: { id: item.purpose.id },
          tenants: {id: item.tenants.id},
          id: 0,
        })),
      //  contactInfos: contactInfos,
        person:
          (type === "PERSON" && {
            familyName: data.familyName,
            givenName: data.givenName,
            additionalName: data.additionalName,
            gender: data.gender.name,
          }) ||
          null,
        institution:
          (type === "INSTITUTION" && {
            commonName: data.commonName,
            legalName: data.legalName,
          }) ||
          null,
        email: data.officialEmail,
        purpose: { id: data.purpose.id },
      });
      // * Category
      switch (type) {
        case "PERSON":
          Object.assign(contactInput, {
            category: { id: 1, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        case "INSTITUTION":
          Object.assign(contactInput, {
            category: { id: 2, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        default:
          break;
      }
      const { errors } = await client.mutate({
        mutation: CREATE_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(process(MUTATION_CREATE_CONTACT_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully created",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAN_CONTACT_SUCCESS));
    }
  };
/**
 * @param {Number/String} id
 * @returns void
 */
export const removeAddress =
  ({ id }) =>
  async (dispatch) => {
    try {
      const { errors } = await client.mutate({
        mutation: REMOVE_ADDRESS,
        variables: { id: Number(id) },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
  };
/**
 * @param {String} type
 * @param {Object} data
 * @returns void
 */
export const modifyContact =
  ({ type, data }) =>
  async (dispatch) => {
    try {
      // * Contact Infos
     /* let contactInfos = new Array();
      data.phones.map((item, index) =>
        contactInfos.push({
          id: item.id || 0,
          value: item.value,
          default: index === 0 ? true : false,
          contactInfoTypeId: 1,
        })
      );*/
      // * Contact Input
      let contactInput = new Object({
        id: data.id,
        contactTypeIds: data.contactType.map((item) => item.id),
        addresses: data.addresses.map((item) => ({
        //  default: item.default || false,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.country.id,
          purpose: { id: item.purpose.id },
          id: Number(item.id) || 0,
        })),
        //contactInfos: contactInfos,
        person:
          (type === "PERSON" && {
            familyName: data.familyName,
            givenName: data.givenName,
            additionalName: data.additionalName,
            gender: data.gender.name,
          }) ||
          null,
        institution:
          (type === "INSTITUTION" && {
            commonName: data.commonName,
            legalName: data.legalName,
          }) ||
          null,
        email: data.officialEmail,
        purpose: { id: data.purpose.id },
      });
      // * Category
      switch (type) {
        case "PERSON":
          Object.assign(contactInput, {
            category: { id: 1, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        case "INSTITUTION":
          Object.assign(contactInput, {
            category: { id: 2, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        default:
          break;
      }
      dispatch(process(MUTATION_MODIFY_CONTACT_START));
      const { errors } = await client.mutate({
        mutation: MODIFY_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(process(MUTATION_MODIFY_CONTACT_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully updated information",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAN_CONTACT_SUCCESS));
    }
  };
/**
 * @param {Number} contactId
 * @returns void
 */
export const deleteContact = (contactId) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_DELETE_CONTACT_START));
    const { errors } = await client.mutate({
      mutation: DELETE_CONTACT,
      variables: { contactId: contactId },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_DELETE_CONTACT_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully deleted",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};
/**
 * @param {HierarchyInput} hierarchy
 * @returns void
 */
export const createHierarchy = (hierarchy) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_CREATE_HIERARCHY_START));
    const { errors } = await client.mutate({
      mutation: CREATE_HIERARCHY,
      variables: { hierarchy },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_HIERARCHY_SUCCESS));
    dispatch(
      showMessage({
        message: "Contact Assigned",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};
/**
 * @param {Object} contactInfo
 * @returns void
 */
const createContactInfo = (contactInfo) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_CREATE_CONTACT_INFO_START));
    delete contactInfo.undefined;
    Object.assign(contactInfo, {
      contactInfoTypeId: contactInfo.contactInfoType.id,
    });
    delete contactInfo.contactInfoType;
    const { errors } = client.mutate({
      mutation: CREATE_CONTACT_INFO,
      variables: { contactInfo },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_CONTACT_INFO_SUCCESS));
    dispatch(
      showMessage({
        message: "Additional Information Added",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
  }
};
/**
 * @param {Object} contactInfo
 * @returns void
 */
const modifyContactInfo = (contactInfo) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_MODIFY_CONTACT_INFO_START));
    delete contactInfo.undefined;
    Object.assign(contactInfo, {
      contactInfoTypeId: contactInfo.contactInfoType.id,
    });
    delete contactInfo.contactInfoType;
    const { errors } = client.mutate({
      mutation: MODIFY_CONTACT_INFO,
      variables: { contactInfo },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_CONTACT_INFO_SUCCESS));
    dispatch(
      showMessage({
        message: "Additional Information Modified",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
  }
};
/**
 * @param {Int} contactInfoId
 * @returns void
 */
export const deleteContactInfo = (contactInfoId) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_DELETE_CONTACT_INFO_START));
    const { errors } = await client.mutate({
      mutation: DELETE_CONTACT_INFO,
      variables: { contactInfoId },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_DELETE_CONTACT_INFO_SUCCESS));
    dispatch(
      showMessage({
        message: "Additional Information Deleted",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
  }
};
/**
 * @param {String} type
 * @param {String} method
 * @param {Object} data
 * @returns void
 */
export const mutationContact =
  ({ type, method, data, addresses }) =>
  async (dispatch) => {
    switch (method) {
      case "POST":
        createContact({ type, data })(dispatch);
        break;
      case "PUT":
        modifyContact({ type, data, addresses })(dispatch);
        break;
      case "ASSIGN":
        assignNewContact({ type, data })(dispatch);
        break;
      default:
        break;
    }
  };
/**
 * @param {String} method
 * @param {Object} data
 * @returns void
 */
export const mutationContactInfo =
  ({ method, data }) =>
  async (dispatch) => {
    switch (method) {
      case "POST":
        createContactInfo(data)(dispatch);
        break;
      case "PUT":
        modifyContactInfo(data)(dispatch);
        break;
      default:
        break;
    }
  };
/**
 * @param {Object} data
 * @returns void
 */
export const createInfoType =
  ({ data }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_CREATE_INFO_TYPE_START));
      const { errors } = await client.mutate({
        mutation: CREATE_CONTACT_INFO_TYPE,
        variables: {
          contactInfoType: data,
        },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(process(MUTATION_CREATE_INFO_TYPE_SUCCESS));
      dispatch(
        showMessage({
          message: "New Info Type Created!",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(setError(error));
    } finally {
      dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
    }
  };
