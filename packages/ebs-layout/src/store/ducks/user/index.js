import { client } from "utils/apollo/apollo";
import { clientRest } from "utils/axios";
import { FIND_USER_LIST, MODIFY_PREFERENCE_USER } from "utils/apollo/gql/user";
import { showMessage } from "store/ducks/message";
import Cookies from "js-cookie";
import { signOut } from "store/ducks/auth";
// constants
const initialState = {
  loading: false,
  userInfo: null,
  userProfile: null,
  error: null,
  domainList: null,
  tokenId: "",
};

const USER_GET_USER = "[USER] GET_USER";
const USER_SET_USER = "[USER] SET_USER";
const MODIFY_PREFERENCE_USER_START = "[USER] MODIFY_PREFERENCE_USER_START";
const MODIFY_PREFERENCE_USER_SUCCESS = "[USER] MODIFY_PREFERENCE_USER_SUCCESS";
const USER_SET_USER_PROFILE = "[USER] SET_USER_PROFILE";
const USER_ERROR = "[USER] ERROR";
const USER_SET_DOMAINS = "[USER] USER_SET_DOMAINS";
const USER_SET_TOKEN = "[USER] USER_SET_TOKEN";

const process = (type) => ({
  type,
});

const getUser = () => ({
  type: USER_GET_USER,
});

export const setUser = (userInfo) => ({
  type: USER_SET_USER,
  payload: userInfo,
});
export const setToken = (tokenId) => ({
  type: USER_SET_TOKEN,
  payload: tokenId,
});

export const setUserProfile = (userProfile) => ({
  type: USER_SET_USER_PROFILE,
  payload: userProfile,
});

const userError = (message) => ({
  type: USER_ERROR,
  payload: message,
});

const setDomains = (domains) => ({
  type: USER_SET_DOMAINS,
  payload: domains,
});

// reducer
export default function userReducer(state = initialState, { type, payload }) {
  switch (type) {
    case USER_GET_USER:
      return {
        ...state,
        loading: true,
      };
    case USER_SET_USER:
      return {
        ...state,
        userInfo: payload,
        loading: false,
      };
    case USER_SET_DOMAINS:
      return {
        ...state,
        domainList: payload,
      };
    case USER_SET_TOKEN:
      return {
        ...state,
        tokenId: payload,
      };
    case USER_SET_USER_PROFILE:
      return {
        ...state,
        userProfile: payload,
        loading: false,
      };
    case MODIFY_PREFERENCE_USER_START:
      return { ...state, loading: true };
    case MODIFY_PREFERENCE_USER_SUCCESS:
      return { ...state, success: true, loading: false };
    case USER_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}
/**
 * @param {Number} userId
 * @returns void
 */
const getUserProfile =
  ({ userId }) =>
  async (dispatch) => {
    try {
      const { data } = await clientRest.get(`/user/${userId}`);
      const { permissions, ...rest } = data;
      dispatch(setUserProfile(data));
    } catch (error) {
      userError(error);
    }
  };

export const setUserDomains = (domains) => async (dispatch) => {
  dispatch(setDomains(domains));
};
/**
 * @param {Object} userState
 * @returns void
 */
export const setUserData =
  (userState) => async (dispatch, getState, setUserInfo) => {
    const userName =
      userState["http://wso2.org/claims/emailaddress"] != undefined
        ? userState["http://wso2.org/claims/emailaddress"].toLowerCase()
        : userState["email"].toLowerCase();
    dispatch(getUser());
    client
      .query({
        query: FIND_USER_LIST,
        variables: {
          page: { number: 1, size: 10 },
          filters: [{ col: "userName", mod: "EQ", val: userName }],
        },
        fetchPolicy: "no-cache",
      })
      .then(async ({ data }) => {
        if (data === null || data.findUserList.content.length === 0) {
          dispatch(userError("No user found"));
          signOut()(dispatch, getState);
          dispatch(
            showMessage({
              message:
                "Your attempt to log in has failed. Please contact your administrator.",
              variant: "error",
              autoHideDuration: 10000,
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        } else if (data.findUserList.content[0].isActive === false) {
          dispatch(userError("User is not activated"));
          signOut()(dispatch, getState);
          dispatch(
            showMessage({
              message: "User is not activated, contact the administrator.",
              variant: "error",
              autoHideDuration: 10000,
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        } else {
          // * Set user id in auth State
          if (setUserInfo) setUserInfo(data.findUserList.content[0]);
          dispatch(setUser(data.findUserList.content[0]));
          await getUserProfile({ userId: data.findUserList.content[0].id })(
            dispatch
          );
          let newCookie = JSON.parse(Cookies.get("auth"));
          newCookie.userState["userId"] = data.findUserList.content[0].id;
          newCookie.userState["externalId"] =
            data.findUserList.content[0].externalId;
          Cookies.set("auth", JSON.stringify(newCookie), { sameSite: "Lax" });
        }
      })
      .catch((error) => {
        // dispatch(userError(error));
        const typeError = userError(error);

        if (typeError?.payload?.networkError?.statusCode === 401) {
          dispatch(
            showMessage({
              message:
                "Your attempt to log in has failed. Please contact your administrator.",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          signOut()(dispatch);
        } else {
          dispatch(
            showMessage({
              message:
                "We are having issues connecting to an internal service. Please wait a bit and try again or contact your administrator.",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          signOut()(dispatch);
        }
      });
  };

export const modifyUserPreference = (user) => async (dispatch) => {
  try {
    dispatch(process(MODIFY_PREFERENCE_USER_START));
    const { data, errors } = await client.mutate({
      mutation: MODIFY_PREFERENCE_USER,
      variables: { user },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MODIFY_PREFERENCE_USER_SUCCESS));
    dispatch(
      showMessage({
        message: "Saved and applied configuration",
        autoHideDuration: 5000,
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    await getUserProfile({ userId: data.modifyUser.id })(dispatch);
  } catch (error) {
    console.log(error);
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "Error Action",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
