import {
  authTokenUrl,
  clientId,
  clientSecret,
  callbackUrl,
} from "@ebs/root-config";
import axios from "axios";
import { authorizationCode, refreshToken } from "axios-oauth-client";
import { showMessage } from "store/ducks/message";
import { jwtDecode } from "jwt-decode";
import { setToken, setUserData } from "store/ducks/user";
import { getCoreSystemContext } from "utils/other/CrossMFE/CrossContext";

import Cookies from "js-cookie";

const initialState = {
  loading: false,
  error: null,
  isSignIn: Cookies.get("auth") ? true : false,
  userState:
    (Cookies.get("auth") && JSON.parse(Cookies.get("auth")).userState) || null,
  idToken:
    (Cookies.get("auth") && JSON.parse(Cookies.get("auth")).idToken) || null,
  expireAt:
    (Cookies.get("auth") &&
      new Date(
        JSON.parse(Cookies.get("auth")).expireAt || new Date().toISOString()
      ).toISOString()) ||
    null,
  refreshIdToken:
    (Cookies.get("auth") && JSON.parse(Cookies.get("auth"))?.refreshIdToken) ||
    null,
};

// constants
const SING_IN_LOADING = "[AUTH] SING_IN_LOADING";
const SING_IN_ERROR = "[AUTH] SING_IN_ERROR";
const SIGN_IN_SUCCESS = "[AUTH] SIGN_IN_SUCCESS";
const REFRESH_TOKEN_SUCCESS = "[AUTH] REFRESH_TOKEN_SUCCESS";
const REFRESH_TOKEN_ERROR = "[AUTH] REFRESH_TOKEN_ERROR";
const SIGN_OUT = "[AUTH] SIGN_OUT";

// Arrow functions
export const signIn = () => ({
  type: SING_IN_LOADING,
});

export const signInSuccess = (userState) => ({
  type: SIGN_IN_SUCCESS,
  payload: userState,
});

export const signInError = (message) => ({
  type: SING_IN_ERROR,
  payload: message,
});

const updateToken = (tokenData) => ({
  type: REFRESH_TOKEN_SUCCESS,
  payload: tokenData,
});

const refreshIdTokenError = (message) => ({
  type: REFRESH_TOKEN_ERROR,
  payload: message,
});

const signSessionOut = () => ({
  type: SIGN_OUT,
});

// reducer
export default function authReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SING_IN_LOADING:
      return {
        ...state,
        loading: true,
      };
    case SING_IN_ERROR:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    case SIGN_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        isSignIn: true,
        error: null,
        ...payload,
      };
    case REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        idToken: payload.idToken,
        expireAt: payload.expireAt,
        refreshIdToken: payload.refreshIdToken,
        error: null,
      };
    case REFRESH_TOKEN_ERROR:
      return {
        ...state,
        idToken: null,
        expireAt: null,
        refreshIdToken: null,
        isSignIn: false,
        error: payload,
      };
    case SIGN_OUT:
      return {
        isSignIn: false,
      };
    default:
      return state;
  }
}

export const signOut = () => (dispatch) => {
  // const { authConfigLogoutUri, clientId, authUrl } = getCoreSystemContext();
  // const cognitoEndPoint = authUrl.split("/oauth2/authorize")[0];
  // const urlCognito = `${cognitoEndPoint}/logout?client_id=${clientId}&logout_uri=${authConfigLogoutUri}`;
  sessionStorage.clear();
  localStorage.clear();
  const allCookies = Cookies.get();
  for (const cookie in allCookies) {
    Cookies.remove(cookie);
    // window.location.href = urlCognito;
    dispatch(signSessionOut());
  }
  // setTimeout(function () {
  //   dispatch(signSessionOut());
  // }, 1500);
};

export const signOutByInactivity = () => (dispatch, getState) => {
  dispatch(signOut());
  dispatch(
    showMessage({
      message: "Your session was closed by inactivity",
      variant: "warning",
      anchorOrigin: {
        vertical: "top",
        horizontal: "right",
      },
    })
  );
};

export const getAuthToken =
  (authCode) => async (dispatch, getState, setUserState) => {
    const Client = authorizationCode(
      axios.create(),
      authTokenUrl,
      clientId,
      clientSecret,
      callbackUrl
    );
    Client(authCode)
      .then(({ expires_in, id_token, refresh_token }) => {
        const userState = jwtDecode(id_token);
        let tokenExpire = new Date(
          Date.now() + expires_in * 1000
        ).toISOString();
        Cookies.remove("auth");
        Cookies.set(
          "auth",
          JSON.stringify({
            refreshIdToken: refresh_token,
            idToken: id_token,
            expireAt: tokenExpire,
            userState: {
              "http://wso2.org/claims/emailaddress":
                userState["http://wso2.org/claims/emailaddress"] != undefined
                  ? userState["http://wso2.org/claims/emailaddress"]
                  : userState["email"],
            },
            context: {
              tenantId: 1,
              instanceId: 1,
              domainId: null,
            },
          }),
          { sameSite: "Lax" }
        );
        // * Save User Profile to redux
        dispatch(setToken(id_token));
        if (setUserState) setUserState(userState);
        setUserData(userState)(dispatch, getState);
        dispatch(
          signInSuccess({
            userState: userState,
            idToken: id_token,
            expireAt: tokenExpire,
            refreshIdToken: refresh_token,
          })
        );
      })
      .catch(({ message }) => {
        dispatch(signInError(message));
        dispatch(
          showMessage({
            message: message,
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "center",
            },
          })
        );
      });
  };

export const refreshTokenFunction =
  (_refreshToken) => async (dispatch, getState) => {
    const Client = refreshToken(
      axios.create(),
      authTokenUrl,
      clientId,
      clientSecret
    );
    Client(_refreshToken)
      .then((resp) => {
        const { expires_in, id_token, access_token } = resp;
        let tokenExpire = new Date(
          Date.now() + expires_in * 1000
        ).toISOString();
        const prevCookie = JSON.parse(Cookies.get("auth"));
        let _userState = prevCookie.userState;
        let _context = prevCookie.context;
        Cookies.set(
          "auth",
          JSON.stringify({
            refreshIdToken: _refreshToken,
            idToken: id_token,
            expireAt: tokenExpire,
            userState: _userState,
            context: _context,
          }),
          { sameSite: "Lax" }
        );
        dispatch(setToken(id_token));
        localStorage.setItem("id_token", id_token);
        dispatch(
          updateToken({
            idToken: id_token,
            expireAt: tokenExpire,
            refreshIdToken: _refreshToken,
          })
        );
      })
      .catch(({ message }) => {
        signOut()(dispatch, getState);
        dispatch(refreshIdTokenError(message));
        dispatch(
          showMessage({
            message: `Internal Server Error: Something went wrong with the authentication process. Please try logging in again, and if this persists, please contact your administrator.`,
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      });
  };
