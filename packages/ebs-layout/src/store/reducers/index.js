import { combineReducers } from "redux";
import tenant from "store/ducks/tenant";
import auth from "store/ducks/auth";
import user from "store/ducks/user";
import message from "store/ducks/message";
import filter from "store/ducks/filter"
import product from "store/ducks/product"

export default combineReducers({
  auth,
  tenant,
  user,
  message,
  filter,
  product
});
