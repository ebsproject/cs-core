import React, { memo } from "react";
import {
  BrowserRouter as Router, Routes, Navigate,
  Route,
  useLocation,

} from "react-router-dom";
import { useSelector } from "react-redux";
import { Login } from "page/Login";
import { Dashboard } from "page/Dashboard";
import BreedingAnalytics from "page/BreedingAnalytics";
import BaseUI from "page/BaseUI";
import Profile from "page/Profile";
import ServiceManagement from "page/ServiceManagement";
import CoreSystem from "page/CoreSystem";
import PrintOut from "page/PrintOut";
import ShipmentTool from "page/ShipmentTool";
import ShipmentManager from "page/ShipmentManager";
import { Callback } from "page/Callback";
import MainLayout from "layout/MainLayout";

import { NotFound } from "page/NotFound";
import { PrivateRoute } from "auth/PrivateRoute";
import Workflow from "page/WorkflowManagement/WorkflowManagement";
import {
  BASE_PATH,
  BASE_UI,
  BREEDING_ANALYTICS,
  CALLBACK,
  CORE_SYSTEM,
  DASHBOARD,
  ERROR_404,
  LOGIN,
  SERVICE_MANAGEMENT,
  PRINT_OUT,
  SHIPMENT_TOOL,
  SHIPMENT_MANAGER,
  PROFILE,
  MARKER_DB,
  WORKFLOW,
} from "./paths";
import MarkerDb from "page/MarkerDb";
import ErrorView from "page/Error/errorView";

const MainRoutes = () => {
  const { isSignIn } = useSelector(({ auth }) => auth);

  return (
    <Router>
      <Routes>
        <Route path={LOGIN} element={<Login />} />
        <Route path={CALLBACK} element={<Callback />} />
        <Route path={BASE_PATH} element={isSignIn ? <Navigate to={DASHBOARD} replace /> : <Navigate to={LOGIN} replace />} />
        <Route
          path={DASHBOARD}
          element={
            <PrivateRoute
              element={() => <MainLayout type="blank" component={Dashboard} />}
            />
          } />
        <Route
          path={CORE_SYSTEM}
          element={
            <PrivateRoute
              element={() => <MainLayout type="main" component={CoreSystem} />}
            />
          } />
        <Route
          path={SERVICE_MANAGEMENT}
          element={
            <PrivateRoute
              element={() => <MainLayout type="main" component={ServiceManagement} />}
            />
          } />
        <Route
          path={WORKFLOW}
          element={
            <PrivateRoute
              element={() => <MainLayout type="main" component={Workflow} />}
            />
          } />
        <Route
          path={MARKER_DB}
          element={
            <PrivateRoute
              element={() => <MainLayout type="main" component={MarkerDb} />}
            />
          } />
        <Route
          path={SHIPMENT_TOOL}
          element={
            <PrivateRoute
              element={() => <MainLayout type="main" component={ShipmentTool} />}
            />
          } />
        <Route
          path={PRINT_OUT}
          element={
            <PrivateRoute
              element={() => <MainLayout type="main" component={PrintOut} />}
            />
          } />

        <Route
          path={PROFILE}
          element={
            <PrivateRoute
              element={() => <MainLayout type="blank" component={Profile} />}
            />
          }
        />
        <Route
          path={"/error-page"}
          element={<ErrorView error={"There was an error."} />}
        />
        <Route
          path={"*"}
          element={<NotFound />}
        />

      </Routes>
    </Router>
  );
};

export default memo(MainRoutes);
