import React, { useEffect, useState, memo } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import { Core, Styles, Icons, Styled } from "@ebs/styleguide";
import {
  getContext,
  getCoreSystemContext,
} from "utils/other/CrossMFE/CrossContext";
import "@ebs/messaging";
const { graphqlUri } = getCoreSystemContext();
const { useTheme } = Styles;
const {
  Drawer,
  List,
  CssBaseline,
  Typography,
  IconButton,
  ListItem,
  Dialog,
  Button,
  DialogTitle,
  DialogContent,
  DialogActions,
  ListItemText,
  Box,
} = Core;
const { ChevronLeft, ChevronRight } = Icons;
import { FormattedMessage } from "react-intl";
import UserMessage from "components/atoms/Message";
import Footer from "components/molecule/Footer";
import {
  findProductList,
  // findTenant,
  findProgramList,
} from "store/ducks/tenant";
import { useDispatch, useSelector, useStore } from "react-redux";
import { setUserData } from "store/ducks/user";
import { RBAC } from "utils/RBAC/RBAC";
// import { setTenantContext } from "store/ducks/tenant";
import FavoriteList from "components/molecule/FavoriteList";
import  SubMenu  from "./SubMenu";
import { useLayoutContext } from "layout/layout-context";
// import { setProductSelected } from "store/ducks/product";
import { getTokenId } from "utils/other/CrossMFE/CrossContext";
const { styled } = Styled;
import ListSkeleton from "./list-skeleton";
import Cookies from "js-cookie";

const Main = styled('main')(({ theme }) => ({
  flexGrow: 1,
  overflowY: 'auto',
  height: `calc(100vh)`,
  boxSizing: 'border-box',
}));
const DrawerHeader = styled('div')(({ theme }) => ({
  ...theme.mixins.toolbar,
}));
const MainLayout = ({ type, children, ...rest }) => {

  const theme = useTheme();
  const drawerWidth = 280;
  const classes = {
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: "100%",//`calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 2,
    },
    hide: {
      display: "none",
    },
    actions: {
      flexGrow: 1,
      position: "relative",
      borderRadius: theme.shape.borderRadius,
      marginRight: theme.spacing(2),
      marginLeft: 0,
      width: "100%",
      [theme.breakpoints.up("md")]: {
        marginLeft: theme.spacing(3),
        width: "auto",
        display: "block",
      },
    },
    drawer: {
      marginTop: '70px',
      width: drawerWidth,
      flexShrink: 0,
      whiteSpace: "nowrap"
    },

    drawerOpen: {
      marginTop: '70px',
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerClose: {
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      overflowX: "hidden",
      width: theme.spacing(7) + 1,
      [theme.breakpoints.up("sm")]: {
        width: theme.spacing(9) + 1,
      },
    },
    toolbar: {
      padding: 5,
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: theme.spacing(0, 1),
      // necessary for content to be below app bar
      ...theme.mixins.toolbar,
    },
    logo: {
      marginLeft: 0,
      width: 250,
      height: 92,
    },
    content: {
      flexGrow: 1,
      overflow: "hidden",
      padding: theme.spacing(2),
    },
    footer: {
      position: "fixed",
      bottom: 0,
    },
  }
  const { userProfile, setFilterGridToApply, favoriteOpen, setFavoriteOpen, drawerOpen : open, setDrawerOpen } = useLayoutContext();
  const tokenID = getTokenId();
 // const [openFavorite, setOpenFavorite] = React.useState(false);
  const dispatch = useDispatch();
  const { getState } = useStore();
 // const [open, setOpen] = useState(true);
  const {
    productList,
    domainInfo,
    tenantContext,
    instanceInfo,
    tenantInfo,
    programList,
  } = useSelector(({ tenant }) => tenant);
  const { userState } = useSelector(({ auth }) => auth);
  const { userInfo } = useSelector(({ user }) => user);
  const navigate = useNavigate();
  const [openFilter, setOpenFilter] = useState(false);
  const location = useLocation();
  const [products, setProducts] = useState([]);

  useEffect(() => {
    if (userProfile) {
      let domains = getContext();
      const productsArray = userProfile?.permissions?.applications
        .filter((a) => a.id === domains.domainId)
        .map((a) => {
          return a.products;
        });
      const productsList = productsArray.map((product) =>
        product.filter((item) => item.menu_order > 0)
      );
      if (productsList.length > 0) {
        setProducts(
          productsList[0].map((a) => (a.showMenu === true ? a.name : null))
        );
      }
    }
  }, [
     userProfile
  ]);

  useEffect(() => {
    !userInfo && setUserData(userState)(dispatch, getState);
  }, [
    // userInfo,
    // domainInfo,
    // tenantContext.domainId,
    // instanceInfo,
    // tenantContext.instanceId,
  ]);

  // useEffect(() => {
  //   !tenantInfo && findTenant(tenantContext.tenantId)(dispatch, getState);
  // }, [tenantInfo, tenantContext.tenantId]);
  // useEffect(() => {
  //   if (tenantInfo) {
  //     setTenantContext({ instanceId: Number(tenantInfo.instances[0].id) })(
  //       dispatch,
  //       getState
  //     );
  //   }
  // }, [tenantInfo])

  useEffect(() => {
    findProductList(tenantContext.domainId)(dispatch, getState);
  }, [ tenantContext.domainId]);

  useEffect(() => {
    findProgramList(1)(dispatch, getState);
  }, []);

  // const handleDrawerOpen = () => {
  //   if (openFilter) setOpenFilter(false);
  //   setOpen(true);
  // };

  // const handleGlobalFilterOpen = () => {
  //   if (open) setDrawerOpen(false);
  //   setOpenFilter(!openFilter);
  // };


  const handleDrawerClose = () => {
    setDrawerOpen(!open);
  };
  const handleClick = (path, item) => {
    setFilterGridToApply([])
    Cookies.set("productSelected", JSON.stringify({ id: item.id, name: item.name, description: item.description }));
    switch (item.path) {
      case 'designer': navigate(`/wf/${path}`);
        break;
      case 'marker': navigate(`/md/${path}`);
        break;
      default: navigate(`/${domainInfo.prefix}/${path}`);
        break;
    }
  };

  const handleClickOpenFavorite = () => {
    setFavoriteOpen(true);
  };

  const handleCloseFavorite = () => {
    setFavoriteOpen(!favoriteOpen);
  };
if(!userProfile) return null;
  return (
    <>
      {
        type === 'main' && <Dialog
          onClose={handleCloseFavorite}
          open={favoriteOpen}
          fullWidth={true}
        >
          <DialogTitle id="favorites" onClose={handleCloseFavorite}>
            <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
              <FormattedMessage
                id="favoritesList"
                defaultMessage="Favorites List"
              ></FormattedMessage>
            </Typography>
          </DialogTitle>
          <DialogContent dividers>
            <FavoriteList />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseFavorite} color="primary">
              Close
            </Button>
          </DialogActions>
        </Dialog>
      }

      {
        type === 'main' && <RBAC allowedAction={"Chat_View"}>
          <ebs-messaging
            authorization_token={tokenID}
            endPoint={graphqlUri}
          ></ebs-messaging>
        </RBAC>
      }
      <div className="flex">
        <CssBaseline />
        {/* <EbsAppBar classes= {classes} open={open} handleDrawerOpen={handleDrawerOpen} handleClickOpenFavorite={handleClickOpenFavorite} handleGlobalFilterOpen={handleGlobalFilterOpen} /> */}
        {type === 'main' && (
          <>
            {open && (
              <Drawer
                variant="permanent"
                sx={open ? { ...classes.drawer, ...classes.drawerOpen } : { ...classes.drawer, ...classes.drawerClose }}
                classes={{
                  paper: open ?
                    classes.drawerOpen :
                    classes.drawerClose
                }}
              >
                {
                  !domainInfo ? (<ListSkeleton drawerWidth={drawerWidth} />) : (
                    <Box sx={{ width: drawerWidth}}  >
                      <List sx={{ marginTop: "64px" }}>
                        {productList
                          ?.slice()
                          .filter((item) => products.includes(item.name))
                          .sort(function (a, b) {
                            return a.menuOrder - b.menuOrder;
                          })
                          .map((item, index) => (
                            <div key={index}>
                              {item.hasWorkflow ? (
                                <div key={item.id}>
                                  <SubMenu
                                    location={location}
                                    domainInfo={domainInfo}
                                    item={item}
                                  />
                                </div>
                              ) : domainInfo && (
                                <ListItem
                                  button
                                  key={item.id}
                                  onClick={() => handleClick(item.path, item)}
                                  className={
                                    location.pathname ===
                                      `/${domainInfo.prefix}/${item.path}`
                                      ? "text-ebs-green-default font-extrabold bg-slate-100 w-96"
                                      : "hover:-translate-x-1 rounded w-96"
                                  }
                                >
                                  <ListItemText
                                    primary={
                                      <Typography
                                        variant="h6"
                                        className={
                                          location.pathname ===
                                            `/${domainInfo.prefix}/${item.path}`
                                            ? "text-ebs-green-default font-ebs font-extrabold bg-slate-100 w-96"
                                            : "hover:-translate-x-1 rounded w-96 font-ebs font-extrabold"
                                        }
                                      >
                                        {`${item.name}`}
                                      </Typography>
                                    }
                                  />
                                </ListItem>
                              )}
                            </div>
                          ))}
                      </List>
                    </Box>
                  )
                }
                <div className="absolute bottom-5 right-0">
                  <IconButton color="primary" onClick={handleDrawerClose}>
                    {open ? <ChevronLeft /> : <ChevronRight />}
                  </IconButton>
                </div>
                {open ? (
                  <div className={classes.footer}>
                    {" "}
                    <Footer />
                  </div>
                ) : (
                  ""
                )}
              </Drawer>
            )}

            {openFilter && (
              <Drawer
                variant="permanent"
                sx={openFilter ? { ...classes.drawer, ...classes.drawerOpen } : { ...classes.drawer, ...classes.drawerClose }}
                classes={{
                  paper: openFilter ?
                    classes.drawerOpen :
                    classes.drawerClose

                }}
              >
                <div className="absolute top-20 left-0">
                  <div>
                    {/* <EbsGlobalFilter
                      setOpenFilter={setOpenFilter}
                      programList={programList}
                      objectProperties={objectProperties}
                      userProfile={userProfile}
                    /> */}
                  </div>
                </div>
                <div className="absolute bottom-5 right-0">
                  <IconButton
                    color="primary"
                    onClick={() => setOpenFilter(false)}
                  >
                    {openFilter ? <ChevronLeft /> : <ChevronRight />}
                  </IconButton>
                </div>
              </Drawer>
            )}
          </>
        )

        }

        <Main>
          <DrawerHeader />
          <Box sx={classes.toolbar}>
            <UserMessage />
            {children}
          </Box>
        </Main>
      </div>
    </>
  );
}
export default memo(MainLayout);
