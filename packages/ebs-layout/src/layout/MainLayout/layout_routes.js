import React, { Suspense, useEffect, useState } from "react";
import MainLayout from "./layout";
import { ApolloProvider } from "@apollo/client";
import client from "utils/apollo";
import { useLocation } from "react-router-dom";
import { useSelector, useDispatch, useStore } from "react-redux";
import { setTenantContext } from "store/ducks/tenant";
import ProductsPageView from "page/ProductsPage/ProductsPageView"
import EBSSplashScreen from "components/atoms/EBSSplashScreen";
import { Core } from "@ebs/styleguide";
const { Box } = Core;
import { findInstance, findDomain } from "store/ducks/tenant";

const LayoutRoutes = ({ type, component: Component, ...rest }) => {
  const [openProductView, setOpenProductView] = useState(true);
  const location = useLocation();
  const { getState } = useStore();
  const dispatch = useDispatch();
  const { instanceInfo, domainInfo } = useSelector(({ tenant }) => tenant);

  useEffect(() => {
    !instanceInfo && findInstance(1)(dispatch, getState);
  }, []);

  useEffect(() => {
    if (instanceInfo) {
      const item = instanceInfo.domaininstances.find(
        (item) => item.domain.prefix === location.pathname.split("/")[1]
      );
      if(item) {
        setTenantContext({
          domainId: Number(item.domain.id),
        })(dispatch, getState);
        findDomain(Number(item.domain.id))(dispatch, getState);
      }
    }
  }, [location, instanceInfo]);
  useEffect(() => {
    if (type === 'main') {
      if (domainInfo) {
        let prefix = "/" + domainInfo.prefix;
        prefix !== location.pathname ? setOpenProductView(false) : setOpenProductView(true)
      }
      else {
        if (location.pathname !== 'dashboard')
            setOpenProductView(false);
      }
    } else setOpenProductView(false);
  }, [domainInfo, location])

  return (
    <ApolloProvider client={client}>
      <MainLayout type={type}>
      {openProductView ?
          (
            <ProductsPageView />
          ) :
          (
            <Suspense fallback={<EBSSplashScreen />}>
              <Box sx={{ width: '100%', height: '100vh', overflow: 'hidden', marginTop:"25px" }}>
                <Box sx={{ height: '100%', overflow: 'auto', maxWidth: '100%' }}>
                      <Component {...rest} />
                </Box>
              </Box>
            </Suspense>

          )
        }
      </MainLayout>
    </ApolloProvider>
  );
};

export default LayoutRoutes;
