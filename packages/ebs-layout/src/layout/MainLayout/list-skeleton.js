import React from 'react';
import { Core, Lab } from "@ebs/styleguide";
const { Grid, Box, List, ListItem, ListItemText } = Core;
const { Skeleton } = Lab;

const ListSkeleton = ({ drawerWidth }) => {
    return (
        <Grid container spacing={1}>
            <Box sx={{ width: drawerWidth, marginTop: "64px" }}  >
                <List>
                    {Array.from(new Array(8)).map((_, index) => (
                        <ListItem key={index}>
                            <ListItemText primary={<Skeleton width="100%" height="40px" />} />
                        </ListItem>
                    ))}
                </List>
            </Box>
        </Grid>
    );
};

export default ListSkeleton;
