import { Core } from "@ebs/styleguide";
const { ListItemButton, ListItemText, Typography } = Core;
import { memo, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useQuery } from "@apollo/client";
import { FIND_WORKFLOW_LIST } from "utils/apollo/gql/workflow";
import { useDispatch, useSelector } from "react-redux";
import { checkPermissions, setPermissions } from "./functions";
import Cookies from "js-cookie";
import client from "utils/apollo";

  const SubMenu = ({ location, item }) => {
  const { userProfile } = useSelector(({ user }) => user);
  const [userPermissions, setUserPermissions] = useState(null);
  const [indexSelected, setIndexSelected] = useState(-1);
  const navigate = useNavigate();
  const [services, setServices] = useState(null)

    useEffect(() => {
      if (userProfile) setUserPermissions(setPermissions(userProfile));
    }, [userProfile]);
    useEffect(() => {
      const fetchData = async () => {
        const { data } = await client.query({
          query: FIND_WORKFLOW_LIST,
          variables: {
            filters: [{ col: "showMenu", val: true, mod: "EQ" }],
          },
        })
        let _services=  [...data.findWorkflowList.content];
        setServices(_services.sort((a, b) => { return Number(a.sortNo) - Number(b.sortNo);}));
      }
      fetchData()
    }, []);
  const handleClick = (item, index) => {
    setIndexSelected(index);
    Cookies.set("productSelected", JSON.stringify({id:item.product.id, name: item.product.name, description: item.product.description }))
    navigate(`/wf/request?workflowID=${item.id}`,
      {replace:true, state: { name: item.name, id: item.id, description: item.description },
    });
  };
  if (!services || !userProfile) return null;
  return (
    <>
      {services.map((item, index) => {
          return (
            userPermissions &&
            checkPermissions(userPermissions, {...item.securityDefinition}) && (
              <div key={index}>
                <ListItemButton onClick={() => handleClick(item, index)}>
                  <ListItemText primary={
                    <Typography
                      className={
                        index === indexSelected &&
                          location.pathname === "/wf/request"
                          ? "text-ebs-green-default  font-ebs font-extrabold bg-slate-100 w-96"
                          : "hover:-translate-x-1 rounded w-96  font-ebs font-extrabold"
                      }
                      variant="h6"
                    >
                      {`${item.navigationName}`}
                    </Typography>
                  } />
                </ListItemButton>
              </div>
            )
          );
        })}
    </>
  );
};
export default memo(SubMenu);