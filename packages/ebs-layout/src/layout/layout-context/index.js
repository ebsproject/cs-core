import { createContext, useState, useContext } from 'react';

const LayoutContext = createContext();

export function LayoutContextProvider({ children }) {

  const [filterListToApply, setFilterListToApply] = useState({});
  const [filterGridToApply, setFilterGridToApply] = useState([]);
  const [userProfile, setUserProfile] = useState(null);
  const [userInfo, setUserInfo] = useState(null);
  const [userState, setUserState] = useState(null);
  const [drawerOpen, setDrawerOpen] = useState(true);
  const [favoriteOpen, setFavoriteOpen] = useState(false);

  return (
    <LayoutContext.Provider value={
      {
        filterListToApply, setFilterListToApply,
        filterGridToApply, setFilterGridToApply,
        userProfile, setUserProfile,
        userInfo, setUserInfo,
        userState, setUserState,
        drawerOpen, setDrawerOpen,
        favoriteOpen, setFavoriteOpen,
      }
    }>
      {children}
    </LayoutContext.Provider>
  );
};

export function useLayoutContext() {
  return useContext(LayoutContext)
}