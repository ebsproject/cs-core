import React, { useEffect } from "react";
import clsx from "clsx";
import PropTypes from "prop-types";
import { Core, Styles, Icons } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import UserGadgets from "components/molecule/UserGadgets";

const { AppBar, Container, Toolbar, Typography, Grid } = Core;

const LayoutOrganism = React.forwardRef((props, ref) => {
  // Properties of the organism

  return (
    <div >
      <AppBar
        position="static"
      >
        <Toolbar>
          <Grid container direction="row">
            <Grid item xs={6}>
              <Grid
                flexDirection="row"
                justifyContent="flex-start"
                container >
                <Typography
                  variant="h6"
                  noWrap
                  component="a"
                  href="#"
                  sx={{
                    mr: 1,
                    display: { xs: 'none', md: 'flex' },
                    textDecoration: 'none',
                  }}
                >
                  <FormattedMessage id="cs.dashboard" defaultMessage="EBS Portal" />
                </Typography>
              </Grid>

            </Grid>
            <Grid item xs={6}>
              <Grid container
                spacing={2}
                flexDirection="row"
                justifyContent="flex-end">
                <UserGadgets />
              </Grid>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <main>
        <div/>
        {props.children}
      </main>
    </div>
  );
});
// Type and required properties
LayoutOrganism.propTypes = {
  children: PropTypes.node,
};
// Default properties
LayoutOrganism.defaultProps = {
  children: null,
};

export default LayoutOrganism;
