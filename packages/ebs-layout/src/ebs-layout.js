import React, { useContext} from "react";
import ReactDOM from "react-dom/client";
import singleSpaReact from "single-spa-react";
import Root from "./root.component";
import "./styles/index.css";
import ProductDescription from "./components/atoms/ProductDescription";
import ProgressLayout from "components/atoms/Progress";
import { default as UserContextProvider } from "utils/context/UserProfileContext";


const lifecycles = singleSpaReact({
  renderType: "createRoot",
  React,
  ReactDOM,
  rootComponent: Root,
  errorBoundary(err, info, props) {
    // Customize the root error boundary for your microfrontend here.
    return null;
  },
});

export {
  getDomainContext,
  getContext,
  getAuthState,
  getTokenId,
  getCoreSystemContext,
  getGlobalFilters,
} from "utils/other/CrossMFE/CrossContext";
export { RBAC } from "utils/RBAC/RBAC";
export { getUserProfile } from "utils/context/getUserProfile";
export const { bootstrap, mount, unmount } = lifecycles;
export const ProductDescriptionTitle = ProductDescription;
export const Progress = ProgressLayout;
export const userContext = UserContextProvider;
export { useLayoutContext } from "layout/layout-context";
export function useUserContext() {
  return useContext(UserContextProvider)
}
export { default as  SecureRoute} from "components/SecureRoute";

