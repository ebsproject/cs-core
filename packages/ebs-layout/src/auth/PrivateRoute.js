import React, { useEffect } from "react";
import { Route, useNavigate, Navigate, useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { refreshToken, refreshTokenFunction, signOutByInactivity } from "store/ducks/auth";

export const PrivateRoute = ({element: Element, ...rest}) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isSignIn, expireAt, refreshIdToken } = useSelector(
    ({ auth }) => auth
  );
  useEffect(() => {
    !isSignIn && navigate("/login");
  }, [isSignIn]);

  useEffect(() => {
    if(expireAt){
      const refreshMilliseconds = new Date(expireAt) - Date.now();
      const timer = setTimeout(() => {
        ( new Date(expireAt) - Date.now() < refreshMilliseconds) &&
          dispatch(refreshTokenFunction(refreshIdToken));
      }, refreshMilliseconds);
      return () => clearTimeout(timer);
    }
  }, [expireAt]);
 if(isSignIn)return <Element {...rest} />
};
