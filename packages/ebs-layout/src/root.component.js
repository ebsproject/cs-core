import React, { Suspense, useState, useEffect } from "react";
import { Provider, useSelector } from "react-redux";
import { ErrorBoundary } from "react-error-boundary";
import { IntlProvider } from "react-intl";
import Routes from "routes/Routes";
import ErrorFallback from "error/ErrorFallback";
import { EBSMaterialUIProvider, Core } from "@ebs/styleguide";
const { Paper } = Core;
//duck store
import store from "./store";
import { persister } from "./store";
import SessionTimeout from "components/SessionTimeout";
import { UserContextProvider } from "utils/context/UserProfileContext";
import { LayoutContextProvider } from "layout/layout-context";
import { PersistGate } from 'redux-persist/integration/react';
import EbsAppBar from "components/molecule/AppBar/ebs-appbar";

export default function Root(props) {
  const [state, setState] = useState({
    locale: localStorage.getItem("locale") || "en",
    message: localStorage.getItem("language"),
  });
  return (
    <EBSMaterialUIProvider>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persister}>
          <IntlProvider
            locale={state.locale}
            messages={state.message}
            defaultLocale={"en"}
          >
            <ErrorBoundary FallbackComponent={ErrorFallback}>
              <SessionTimeout />
              <LayoutContextProvider>
                <UserContextProvider>
                  <EbsAppBar />
                  <Routes />
                </UserContextProvider>
              </LayoutContextProvider>
            </ErrorBoundary>
          </IntlProvider>
        </PersistGate>
      </Provider>
    </EBSMaterialUIProvider>
  );
}
