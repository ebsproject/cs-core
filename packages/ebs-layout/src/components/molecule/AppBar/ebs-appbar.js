import React, { memo } from "react";
import { Core, Icons, Styles } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import UserGadgets from "../UserGadgets";
import EbsImg from "assets/images/logos/EBS.png";
import { useSelector } from "react-redux";
const { useTheme } = Styles;
import { useLayoutContext } from "layout/layout-context";
const { AppBar, Toolbar, Typography, Grid, Icon, IconButton, Tooltip } = Core;
const { Dashboard, Menu, Star } = Icons

const EbsAppBar = () => {
  const { isSignIn } = useSelector(({ auth }) => auth);
  const { drawerOpen : open, setDrawerOpen, setFavoriteOpen, favoriteOpen } = useLayoutContext();
  const theme = useTheme();
  const drawerWidth = 280;
  const classes = {
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: "100%",//`calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["width", "margin"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 2,
    },
    hide: {
      display: "none",
    },

  }
  const {
    domainInfo,
  } = useSelector(({ tenant }) => tenant);
  const handleClickOpenFavorite = () => { setFavoriteOpen(!favoriteOpen) }
  const handleDrawerOpen = () => { setDrawerOpen(!open) }
if(!isSignIn || window.location.pathname === "/login") return null;
  return (
    <AppBar
    position="fixed"
    sx={open ? { ...classes.appBar, ...classes.appBarShift } : { ...classes.appBar }}
    >
      <Toolbar>
        <Grid container direction="row">
          <Grid item xs={6}>
            <Grid container
              direction="row"
              justifyContent="flex-start"
              alignItems="flex-start">
              <Grid item className="flex flex-row p-1 items-center gap-4">
                {
                  window.location.pathname !== '/dashboard' && <>
                    {!open && <IconButton
                      color="inherit"
                      aria-label="open drawer"
                      onClick={handleDrawerOpen}
                      sx={open ? { ...classes.menuButton, ...classes.hide } : classes.menuButton}
                    >
                      <Menu />
                    </IconButton>}
                    <Icon color="inherit" aria-label="open drawer" edge="start">
                      <div>
                        <img src={EbsImg} style={{ height: "70%", width: "70%" }} />
                      </div>
                    </Icon>
                    <Typography
                      variant="h6"
                      noWrap
                      className="font-ebs font-extrabold"
                    >
                      {domainInfo?.name || ""}
                    </Typography>
                  </>
                }
                {
                  window.location.pathname === '/dashboard' && (
                    <>
                      <Typography
                        variant="h6" noWrap className="font-ebs font-extrabold -mr-32" color="inherit">
                        <FormattedMessage id="cs.dashboard" defaultMessage="EBS Portal" />
                      </Typography>
                    </>
                  )
                }
                {
                  window.location.pathname !== '/dashboard' &&
                  <div >
                    <div className="flex flex-nowrap">
                      <Tooltip title="Go back dashboard">
                        <IconButton color="inherit" edge="start" href="/dashboard">
                          <Dashboard />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Favorites List">
                        <IconButton
                          color="inherit"
                          onClick={handleClickOpenFavorite}
                        >
                          <Star />
                        </IconButton>
                      </Tooltip>
                      {/* <div className="flex flex-nowrap rounded-xl p-2">
                        <Tooltip title="filter">
                          <IconButton
                            color="inherit"
                            onClick={handleGlobalFilterOpen}
                          >
                            <img src={Filter} />
                          </IconButton>
                        </Tooltip>
                      </div> */}
                    </div>
                  </div>
                }
              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={6}>
            <Grid container
              direction="row"
              justifyContent="flex-end"
              alignItems="flex-end">
              <Grid item>
                <UserGadgets />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};
export default memo(EbsAppBar);
