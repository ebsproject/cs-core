import React, { memo, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import PropTypes from "prop-types";
import { Core, Icons, ThemeSelector } from "@ebs/styleguide";
import { RBAC } from "utils/RBAC/RBAC";
import { getCoreSystemContext } from "utils/other/CrossMFE/CrossContext";
import AvatarProfile from "components/atoms/AvatarProfile";
import DomainMenus from "components/molecule/MenuDomainList";
import { FormattedMessage } from "react-intl";
import Uri from "components/atoms/Uris";
import { getTokenId } from "utils/other/CrossMFE/CrossContext";
const { IconButton, Popover, Tooltip, Typography } = Core;
const { Help, Apps } = Icons;
import "@ebs/messaging";
import { setUserDomains } from "store/ducks/user";

const UserGadgets = () => {
  const tokenID = getTokenId();
  const { graphqlUri, printoutUri } = getCoreSystemContext();
  const { userInfo, userProfile, domainList } = useSelector(({ user }) => user);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [openPop, setOpenPop] = React.useState(false);
  const [placement, setPlacement] = React.useState();
  const [anchorElHelp, setAnchorElHelp] = React.useState(null);
  const [openPopHelp, setOpenPopHelp] = React.useState(false);
  const [placementHelp, setPlacementHelp] = React.useState();
  const [anchorElApi, setAnchorElApi] = React.useState(null);
  const [openPopApi, setOpenPopApi] = React.useState(false);
  const [placementApi, setPlacementApi] = React.useState();
  const dispatch = useDispatch();

  useEffect(() => {
    if (userProfile && !domainList) {
      setUserDomains(
        userProfile?.permissions?.applications?.map((a) => {
          return a.domain;
        })
      )(dispatch);
    }
  }, []);
  const handleClickPop = (newPlacement) => (event) => {
    setAnchorEl(event.currentTarget);
    setOpenPop((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
  };
  const handleClickHelp = (newPlacement) => (event) => {
    setAnchorElHelp(event.currentTarget);
    setOpenPopHelp((prev) => placementHelp !== newPlacement || !prev);
    setPlacementHelp(newPlacement);
  };
  const handleClickApi = (newPlacement) => (event) => {
    setAnchorElApi(event.currentTarget);
    setOpenPopApi((prev) => placementApi !== newPlacement || !prev);
    setPlacementApi(newPlacement);
  };
  const handleClose = () => {
    setOpenPop(false);
  };
  const handleCloseHelp = () => {
    setOpenPopHelp(false);
  };
  const handleCloseApi = () => {
    setOpenPopApi(false);
  };
  if (!RBAC) return null;
  return (
    <div className="flex flex-row p-1 items-center gap-4">
      <ThemeSelector />
      <RBAC allowedAction={"Notification_View"}>
        <div className="-mr-5">
          <ebs-notification
            position="center"
            endPoint={graphqlUri}
            contactId={userInfo?.contact?.id}
            authorization_token={tokenID}
          ></ebs-notification>
        </div>
      </RBAC>
      <Tooltip
        arrow
        placement="top"
        title={
          <Typography className="font-ebs">
            <FormattedMessage id={"none"} defaultMessage={"Help"} />
          </Typography>
        }
      >
        <IconButton
          aria-label="show 17 new notifications"
          color="inherit"
          edge="end"
          onClick={handleClickHelp("bottom-end")}
        >
          <Help />
        </IconButton>
      </Tooltip>
      <Tooltip
        arrow
        placement="top"
        title={
          <Typography className="font-ebs">
            <FormattedMessage id={"none"} defaultMessage={"Shortcuts"} />
          </Typography>
        }
      >
        <div className="mr-2">
          <IconButton
            aria-label=""
            color="inherit"
            edge="end"
            onClick={handleClickPop("bottom-end")}
          >
            <Apps />
          </IconButton>
        </div>
      </Tooltip>
      <AvatarProfile />

      <Popover
        open={openPop}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <DomainMenus
          userProfile={userProfile}
          domains={domainList}
          handleClose={handleClose}
        />
      </Popover>

      <Popover
        open={openPopHelp}
        anchorEl={anchorElHelp}
        onClose={handleCloseHelp}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <a
          href="https://ebsproject.atlassian.net/servicedesk/customer/portal/2"
          target="_blank"
        >
          <Typography className="font-ebs font-extrabold ml-3 mt-5 mb-1 mr-5 text-base">
            <FormattedMessage id="none" defaultMessage={"EBS Support Desk"} />
          </Typography>
        </a>
        <IconButton
          disableRipple={true}
          color="inherit"
          edge="end"
          onClick={handleClickApi("bottom-end")}
        >
          <Typography className="font-ebs font-extrabold ml-3 mt-1 mb-1 mr-8 text-base">
            <FormattedMessage id="none" defaultMessage={"API"} />
          </Typography>
        </IconButton>
      </Popover>
      <Popover
        open={openPopApi}
        anchorEl={anchorElApi}
        onClose={handleCloseApi}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "right",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        <Uri />
      </Popover>
    </div>
  );
};
// Type and required properties
UserGadgets.propTypes = {
  children: PropTypes.node,
};
// Default properties


export default memo(UserGadgets);
