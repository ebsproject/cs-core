import React, { useEffect } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND ATOMS TO USE
import { useSelector, useDispatch, useStore } from "react-redux";
import { findTenant } from "store/ducks/tenant";
import { FormattedMessage } from "react-intl";
const { Grid, Typography } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const TenantInfoMolecule = React.forwardRef(({ }, ref) => {
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { tenantInfo, tenantContext } = useSelector(({ tenant }) => tenant);
  const { userInfo } = useSelector(({ user }) => user);
  // * Set tenantInfo
  useEffect(() => {
    tenantContext.tenantId &&
      findTenant(tenantContext.tenantId)(dispatch, getState);
  }, [tenantInfo]);

  return (
    <div>
      {tenantInfo && <div>
        <Grid container sx={{ textAlign: "center" }}>
          <Grid item xs={12}>
            <Typography variant="h5" className="font-ebs font-bold">
              {`${userInfo?.userName} is part of:`}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" className="font-ebs">
              <FormattedMessage id="none" defaultMessage={"Customer Name" + `: ${tenantInfo?.customer.name}`} />
            </Typography>

          </Grid>
          <Grid item xs={12}>
            <Typography variant="h6" className="font-ebs">
              <FormattedMessage id="none" defaultMessage={"Organization Name" + `: ${tenantInfo?.organization.name}`} />
            </Typography>
          </Grid>
        </Grid>
      </div>}
    </div>
  );
});
// Type and required properties
TenantInfoMolecule.propTypes = {};
// Default properties
TenantInfoMolecule.defaultProps = {};

export default TenantInfoMolecule;
