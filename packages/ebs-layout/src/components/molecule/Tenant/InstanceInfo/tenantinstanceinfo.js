import React, { Fragment, useEffect } from "react";
import { useSelector, useDispatch, useStore } from "react-redux";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Icons } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { setTenantContext } from "store/ducks/tenant";
import Progress from "components/atoms/Progress";
const { Visibility } = Icons;
const {
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  IconButton,
  Tooltip,
} = Core;

const TenantInstanceInfoMolecule = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { loading, error, tenantInfo } = useSelector(({ tenant }) => tenant);
  // * There is only one instance
  // useEffect(() => {
  //   tenantInfo &&
  //     tenantInfo.instances.length === 1 &&
  //     setTenantContext({ instanceId: Number(tenantInfo.instances[0].id) })(
  //       dispatch,
  //       getState
  //     );
  // }, [tenantInfo]);

  const handleInstanceClick = (e) => {
    e.preventDefault();
    // setTenantContext({ instanceId: Number(e.currentTarget.id) })(
    //   dispatch,
    //   getState
    // );
  };

  if (loading) {
    return <Progress />;
  }
  if (error || !tenantInfo) {
    return <Fragment />;
  }

  return (
    <TableContainer className="flex-auto rounded-sm">
      <Table aria-label="simple-table">
        <TableHead>
          <TableRow>
            <TableCell
              align="center"
              colSpan={4}
              className="bg-ebs-green-default"
            >
              <Typography variant="h6" className="ebs text-white">
                <FormattedMessage
                  id="tnt.comp.comsoon.welcome"
                  defaultMessage="This tenant selected has differents  EBS Instances deployed in your institution"
                />
              </Typography>
            </TableCell>
          </TableRow>
          <TableRow>
            <TableCell>
              <Typography className="ebs text-xs">
                <FormattedMessage id="none" defaultMessage="EBS Instance" />
              </Typography>
            </TableCell>
            <TableCell>
              <Typography className="ebs text-xs">
                <FormattedMessage id="none" defaultMessage="Server" />
              </Typography>
            </TableCell>
            <TableCell align="center">
              <Typography className="ebs text-xs">
                <FormattedMessage id="none" defaultMessage="Port" />
              </Typography>
            </TableCell>
            <TableCell align="center">
              <Typography className="ebs text-xs">
                <FormattedMessage id="none" defaultMessage="Actions" />
              </Typography>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tenantInfo.instances
            .slice()
            .sort(function (a, b) {
              return a.id - b.id;
            })
            .map((row) => (
              <TableRow key={row.id}>
                <TableCell>
                  <Typography className="ebs text-xs">{row.name}</Typography>
                </TableCell>
                <TableCell>
                  <Typography className="ebs text-xs">{row.server}</Typography>
                </TableCell>
                <TableCell>
                  <Typography className="ebs text-xs">{row.port}</Typography>
                </TableCell>
                <TableCell align="center">
                  <Tooltip
                    title={
                      <Typography className="ebs text-xs">
                        <FormattedMessage
                          id="tnt.tenant.select"
                          defaultMessage="View the Products"
                        />
                      </Typography>
                    }
                  >
                    <IconButton
                      size="small"
                      color="primary"
                      id={row.id}
                      onClick={handleInstanceClick}
                    >
                      <Visibility />
                    </IconButton>
                  </Tooltip>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
});
// Type and required properties
TenantInstanceInfoMolecule.propTypes = {};
// Default properties
TenantInstanceInfoMolecule.defaultProps = {};

export default TenantInstanceInfoMolecule;
