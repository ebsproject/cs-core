import React, { useEffect, Fragment } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
import CardTenant from "components/atoms/CardTenant";
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch, useStore } from "react-redux";
import { setTenantContext } from "store/ducks/tenant";
import Progress from "components/atoms/Progress";
import { useNavigate } from "react-router-dom";
const { Grid, Typography, Button } = Core;

const TenantListOrganism = React.forwardRef(({ }, ref) => {
  const dispatch = useDispatch();
  const { getState } = useStore();
  const { userInfo } = useSelector(({ user }) => user);
  const navigate = useNavigate();
  //setTenantContext({ tenantId: Number(1) })(dispatch, getState);
  const handleCardClick = (e) => {
    e.preventDefault();
    // setTenantContext({ tenantId: Number(e.currentTarget.id) })(
    //   dispatch,
    //   getState
    // );
    navigate("/");
  };

  // ? There is only one tenant
  // useEffect(() => {

  //     userInfo.tenants.length === 1 &&
  //     setTenantContext({ tenantId: Number(userInfo.tenants[0].id) })(
  //       dispatch,
  //       getState
  //     );


  // }, []);

  //* There isn't userInfo
  if (!userInfo) {
    return <Progress />;
  } else if (userInfo.tenants.length === 1) {
    return <Fragment />;
  } else {
    return (
      <div className="justify-center">
        <Typography variant="h4" className="font-ebs text-center p-4">
          <FormattedMessage
            id="tnt.comp.comsoon.welcome"
            defaultMessage="Select an organization"
          />
        </Typography>
        <div className="justify-center justify-items-center">
          <Grid
            className="flex flex-nowrap gap-5"
          >
            {userInfo.tenants
              .slice()
              .sort(function (a, b) {
                return a.id - b.id;
              })
              .map((tenant, index) => (
                <Grid key={index} className="justify-center items-center ">
                  <CardTenant key={index} ref={ref} tenantId={Number(tenant.id)}>
                    <div>
                      <Button
                        size="small"
                        color="primary"
                        id={tenant.id}
                        onClick={(e) => handleCardClick(e)}
                      >
                        <Typography className="font-ebs">
                          <FormattedMessage
                            id="tnt.tenant.select"
                            defaultMessage="Select"
                          />
                        </Typography>

                      </Button>
                    </div>
                  </CardTenant>
                </Grid>
              ))}
          </Grid>
        </div>
      </div>
    );
  }
});
// Type and required properties
TenantListOrganism.propTypes = {
  tenants: PropTypes.array,
  children: PropTypes.node,
};
// Default properties
TenantListOrganism.defaultProps = {
  tenants: [],
  children: null,
};

export default TenantListOrganism;
