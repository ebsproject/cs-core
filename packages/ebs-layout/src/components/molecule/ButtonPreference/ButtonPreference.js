import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Lab } from "@ebs/styleguide";
const { Button, Snackbar } = Core;
const { Alert } = Lab;
import { setUserData, modifyUserPreference, setUser } from "store/ducks/user";
import { useSelector, useStore, useDispatch } from "react-redux";
import Progress from "components/atoms/Progress";
import { useQuery } from "@apollo/client";
import { FIND_USER_LIST } from "utils/apollo/gql/user";
import { client } from "utils/apollo/apollo";
import { useLayoutContext } from "layout/layout-context";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ButtonPreferenceMolecule = React.forwardRef(({ columnsGrid }, ref) => {
  const { userProfile, userInfo, userState } = useLayoutContext()
  const [stateMessage, setStateMessage] = React.useState({
    open: false,
    vertical: "top",
    horizontal: "right",
  });
  let _userProfile = Object.assign({}, userProfile);

  const { vertical, horizontal, open } = stateMessage;

  const dispatch = useDispatch();
  
  const [state, setState] = useState(false);
  const { getState } = useStore();
  useEffect(() => {
    if (state) {
      setUserData(userState)(dispatch, getState);
    }
    return () => setState(false);
  }, [state]);

  if (!_userProfile || !userInfo)
    return (
      <div>
        <Progress />
      </div>
    );

  const handleClose = () => {
    setStateMessage({ ...stateMessage, open: false });
  };
  const savePreferenceColumns = () => {
    if (!columnsGrid.idGrid) {
      setStateMessage({
        open: true,
        vertical: "top",
        horizontal: "right",
      });
      console.error(
        "The grid needs a necessary identification, to be able to save the preference columns in the database, assign the property to the corresponding grid, example: id: value. Warning: the id value must be unique in each grid, there cannot be two grids with the same value in the id property."
      );
    } else {
      let preferenceObject = _userProfile.preference;
      if (_userProfile.preference.favorites.columns.length === 0) {
        preferenceObject.favorites.columns.push(columnsGrid);
        modifyUserPreference({
          id: Number(userInfo.id),
          userName: userInfo.userName,
          active: userInfo.isActive,
          admin: userInfo.isAdmin,
          contactId: userInfo.contact.id,
          preference: preferenceObject,
        })(dispatch);
        setState(true);
      } else {
        const idGridPreference = columnsGrid.idGrid;
        const dataIndexPreference =
          preferenceObject.favorites.columns.findIndex(
            (element) => element.idGrid === idGridPreference
          );
        if (dataIndexPreference >= 0) {
          preferenceObject.favorites.columns[dataIndexPreference] = columnsGrid;
          modifyUserPreference({
            id: Number(userInfo.id),
            userName: userInfo.userName,
            active: userInfo.isActive,
            admin: userInfo.isAdmin,
            contactId: userInfo.contact.id,
            preference: preferenceObject,
          })(dispatch);
          setState(true);
        } else {
          preferenceObject.favorites.columns.push(columnsGrid);
          modifyUserPreference({
            id: Number(userInfo.id),
            userName: userInfo.userName,
            active: userInfo.isActive,
            admin: userInfo.isAdmin,
            contactId: userInfo.contact.id,
            preference: preferenceObject,
          })(dispatch);
          setState(true);
        }
      }
    }
  };

  return (
    <div style={{ paddingLeft: 93 + "%", marginTop: 10 + "px" }}>
      <Button
        onClick={() => savePreferenceColumns()}
        className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white ml-3"
      >
        Save
      </Button>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        anchorOrigin={{ vertical, horizontal }}
      >
        <Alert
          severity="error"
          variant="filled"
          onClose={handleClose}
          sx={{ width: "100%" }}
        >
          The grid needs a necessary identification, to be able to save the
          preference columns in the database, assign the property to the
          corresponding grid, example: id: value. Warning: the id value must be
          unique in each grid, there cannot be two grids with the same value in
          the id property.
        </Alert>
      </Snackbar>
    </div>
  );
});
// Type and required properties
ButtonPreferenceMolecule.propTypes = {};
// Default properties
ButtonPreferenceMolecule.defaultProps = {};

export default ButtonPreferenceMolecule;
