import React from 'react'
import { createRoot } from 'react-dom/client'
// Component to be Test
import VersionView from './versionview'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  createRoot(<VersionView></VersionView>, div)
})
// Props to send component to be rendered
const props = {
  properyName: 'Value',
}
test('Render correctly', () => {
  const { getByTestId } = render(<VersionView {...props}></VersionView>)
  expect(getByTestId('VersionViewTestId')).toBeInTheDocument()
})
