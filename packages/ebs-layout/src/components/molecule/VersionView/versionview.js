import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import { Core } from "@ebs/styleguide"
const { List, ListItem, ListItemText, ListItemAvatar, Avatar } = Core
import { mfeInfo } from "@ebs/root-config";
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const VersionViewMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule

  const { domains } = props;
  const [data, setData] = useState(null)
  useEffect(() => {
    mfeInfo.then(result => {
      setData(result.imports)
    })
  }, [])

  let arrModules = domains.map(moduleObject)

  function moduleObject(value, index, array) {
    return {
      id: `@ebs/${value.domain.prefix}`,
      module: value.domain.name
    }
  }


  if (data == null) return <p>Loading ImportMaps Information</p>

  const importMaps = data;


  return (
    <List dense={true}>
      {
        arrModules?.map((item) => {
          const arrMfe = importMaps[item?.id]?.split('/');
          const numSegments = Array.isArray(arrMfe) ? arrMfe.length : 0;
          const version = numSegments === 7 ? arrMfe[5] : numSegments === 0 ? "Unknown" : "Dev";
          return (
            <ListItem key={item.id}>
              <ListItemText primary={item.module} secondary={version} />
            </ListItem>
          )
        })
      }
    </List>
  )
})


export default VersionViewMolecule
