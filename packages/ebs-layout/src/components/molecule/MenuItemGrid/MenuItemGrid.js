import React from "react";
import { Core, Lab } from "@ebs/styleguide";
const { TextField, Typography, MenuItem } = Core;
import { useSelector, useStore, useDispatch } from "react-redux";
import { modifyUserPreference } from "store/ducks/user";

const MenuItemGridMolecule = React.forwardRef(
  ({ pageSize, setPageSize, gotoPage, id }, ref) => {
    const { userProfile, userInfo } = useSelector(({ user }) => user);
    let _userProfile = Object.assign({}, userProfile);
    let dataGrid=[] 
     _userProfile.preference.favorites.columns.map(item => dataGrid = [...dataGrid,item]);
    const filterPreference = dataGrid.filter((item) => item.idGrid === id);
    const dispatch = useDispatch();

    const handleChangeRowsPerPage = (event) => {
      if (filterPreference.length !== 0) {
        if (dataGrid.length !== 0) {
          for (let i = 0; i < dataGrid.length; i++) {
            if (dataGrid[i].idGrid === id) {
              if (dataGrid[i].pagination) {
                dataGrid[i].pagination = event.target.value;
                modifyUserPreference({
                  id: Number(userInfo.id),
                  userName: userInfo.userName,
                  active: userInfo.isActive,
                  admin: userInfo.isAdmin,
                  contactId: userInfo.contact.id,
                  preference:  _userProfile.preference,
                })(dispatch);
              } else {
                dataGrid[i] = {
                  pagination: event.target.value,
                  ...dataGrid[i],
                };
                modifyUserPreference({
                  id: Number(userInfo.id),
                  userName: userInfo.userName,
                  active: userInfo.isActive,
                  admin: userInfo.isAdmin,
                  contactId: userInfo.contact.id,
                  preference:  _userProfile.preference,
                })(dispatch);
              }
            }
          }
        } else {
          dataGrid.push({
            idGrid: id,
            pagination: event.target.value,
          });
          modifyUserPreference({
            id: Number(userInfo.id),
            userName: userInfo.userName,
            active: userInfo.isActive,
            admin: userInfo.isAdmin,
            contactId: userInfo.contact.id,
            preference:  _userProfile.preference,
          })(dispatch);
        }
      } else {
        dataGrid.push({
          idGrid: id,
          pagination: event.target.value,
        });
        modifyUserPreference({
          id: Number(userInfo.id),
          userName: userInfo.userName,
          active: userInfo.isActive,
          admin: userInfo.isAdmin,
          contactId: userInfo.contact.id,
          preference:  _userProfile.preference,
        })(dispatch);
      }

      setPageSize(parseInt(event.target.value, 10));
      gotoPage(0);
    };

    const pageSizeFuction = () => {
      const valuePagination = filterPreference.filter(
        (item) => item.pagination
      );
      if (valuePagination.length > 0) {
        return valuePagination[0].pagination;
      } else {
        return pageSize;
      }
    };
    return (
      <div>
        <TextField
          className="place-self-center font-ebs px-3"
          select
          variant="outlined"
          size="small"
          value={pageSizeFuction()}
          onChange={handleChangeRowsPerPage}
        >
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={50}>50</MenuItem>
          <MenuItem value={100}>100</MenuItem>
        </TextField>
      </div>
    );
  }
);

MenuItemGridMolecule.propTypes = {};

MenuItemGridMolecule.defaultProps = {};

export default MenuItemGridMolecule;
