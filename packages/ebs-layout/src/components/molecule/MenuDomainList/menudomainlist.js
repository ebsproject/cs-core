import React, { Fragment } from "react";
import { Core } from "@ebs/styleguide";
import { useSelector, useDispatch, useStore } from "react-redux";
//import { useNavigate } from "react-router-dom";
// CORE COMPONENTS AND ATOMS TO USE
import { showMessage } from "store/ducks/message";
import { setTenantContext } from "store/ducks/tenant";
import { FormattedMessage } from "react-intl";
import EbsLogo from "assets/images/logos/EBS_Vertical_2.png";
import Cookies from "js-cookie";
//MAIN FUNCTION
const { ListItem, ListItemText,Box,Typography } = Core;

// Import all Icons
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});


const MenuDomainListMolecule = React.forwardRef(({userProfile,domains, handleClose}, ref) => {
  //redux
  
  const dispatch = useDispatch();
  const { getState } = useStore();
 // const navigate = useNavigate();
  const { instanceInfo, tenantContext } = useSelector(({ tenant }) => tenant);
  
  //events
  const handleClickDomain = (item) => {
    if (item.mfe) {
      setTenantContext({ domainId: Number(item.domain.id) })(
        dispatch,
        getState
      );
      if(userProfile.preference.filters?.length > 0){
       const filters = userProfile.preference.filters.filter(a => a.default && a.domainId === Number(item.domain.id))
       if(filters.length > 0)           
            {Cookies.remove("filters");}
            else{
              Cookies.remove("filters");
            }
      } 
      else{
       Cookies.remove("filters");
      }
      window.location.href = `/${item.domain.prefix}`;   
      handleClose();
    } else {
      item.context
        ? window.open(item.context, "__blank")
        : dispatch(
            showMessage({
              message: "The component does not have URL",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          handleClose();
    }
  };

  return (
    <Box>
      <Box className="ml-5">
      <div>
      <img src={EbsLogo} style={{ height: "70%", width: "70%" }}/>
      </div>
          
            
      </Box>
      <a href="/dashboard">

        <Typography

          className="font-ebs font-extrabold ml-3 mt-2 mb-5 text-blue-600 text-sm">
          <FormattedMessage id="none" defaultMessage={"Go to EBS Portal --->"} />
        </Typography>
      </a>

<Typography className="font-ebs font-extrabold ml-2 mt-5">
  <FormattedMessage id="none" defaultMessage={"Domains Shortcuts"}/>
</Typography>
   
    <Fragment>
      {instanceInfo?.domaininstances
        .slice().filter(item => domains.includes(item.domain.name))
        .sort(function (a, b) {
          return a.name - b.name;
        })
        .filter(function (item) {
          return item.domain.id !== tenantContext.domainId;
        })
        .map((r) => (
          <ListItem button key={r.id} onClick={(event) => handleClickDomain(r)}>
            <ListItemText primary={r.domain.name} className="font-ebs text-gray-600 -mt-2 -mb-2"></ListItemText>
          </ListItem>
        ))}
    </Fragment>
    </Box>
  );
});
// Type and required properties
MenuDomainListMolecule.propTypes = {};
// Default properties
MenuDomainListMolecule.defaultProps = {};

export default MenuDomainListMolecule;
