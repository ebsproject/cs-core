import React from "react";
import { Core } from "@ebs/styleguide";
const { Box, Typography, FormattedMessage, List, ListItem, ListItemText } =
  Core;

const MemberOfListMolecule = React.forwardRef(({ userProfile }, ref) => {
  const memberOfPrograms = userProfile.permissions.memberOf.programs;
  const memberOfUnit = userProfile.permissions.memberOf.units;

  return (
    <Box className="ml-10">
      <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
        {" "}
        Programs:
      </Typography>
      <List>
        {memberOfPrograms.map((program, index) => (
          <ListItem key={index} id={index}>
            <ListItemText primary={program.name} />
          </ListItem>
        ))}
      </List>
      <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
        {" "}
        Units:
      </Typography>
      <List>
        {memberOfUnit.map((program, index) => (
          <ListItem key={index} id={index}>
            <ListItemText primary={program.name} />
          </ListItem>
        ))}
      </List>
    </Box>
  );
});
export default MemberOfListMolecule;
