import React, { Fragment, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Core, Icons } from "@ebs/styleguide";
import { findInstance } from "store/ducks/tenant";
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector, useStore } from "react-redux";
import { setTenantContext } from "store/ducks/tenant";
import Progress from "components/atoms/Progress";
import { showMessage } from "store/ducks/message";
import FavoriteDomain from "components/atoms/FavoriteDomain";
import Cookies from "js-cookie";
import { findDomain } from "store/ducks/tenant";
const {
  Grid,
  Typography,
  Card,
  CardContent,
  CardMedia,
  CardActionArea,
  Box,
  FormControl,
  FormGroup,
} = Core;

// Import all Icons
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});

const ProductListMolecule = React.forwardRef(
  ({ domains, userProfile, userInfo, dataProduct }, ref) => {
    const [domainNoVisible, setDomainNoVisible] = useState([]);
    const [domainList, setDomainList] = useState([]);


    const dispatch = useDispatch();
    const { getState } = useStore();
    const { loading, error, instanceInfo, tenantContext, tenantInfo } = useSelector(
      ({ tenant }) => tenant
    );

    const navigate = useNavigate();
    useEffect(() => {
      findInstance(1)(dispatch, getState);
    }, []);
    useEffect(() => {
      if (instanceInfo) {
        const _domainNoVisible = userInfo.permissions.applications.filter(
          (item) => item.showMenu === false
        );
        setDomainNoVisible(_domainNoVisible)

        const _domainList = instanceInfo.domaininstances.filter(
          (item) => Number(item.domain.id) !== Number(domainNoVisible?.[0]?.id)
        );
        setDomainList(_domainList)
      }

    }, [instanceInfo]);

    const handleClickDomain = (item) => {
      if (item.mfe) {
        findDomain(Number(item.domain.id))(
          dispatch,
          getState
        );
        setTenantContext({ domainId: Number(item.domain.id) })(
          dispatch,
          getState
        );
        if (userInfo.preference.filters?.length > 0) {
          const filters = userInfo.preference.filters.filter(
            (a) => a.default && a.domainId === Number(item.domain.id)
          );
          if (filters.length > 0) {
            Cookies.remove("filters");
          } else {
            Cookies.remove("filters");
          }
        } else {
          Cookies.remove("filters");
        }
        navigate(`/${item.domain.prefix}`);
      } else {
        item.context
          ? window.open(item.context, "__blank")
          : dispatch(
            showMessage({
              message: "The component does not have URL",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
      }
    };

    if (!instanceInfo) return null;
    return (
      <>
        <Grid container direction="row">
          <Grid item xs={12}>
            <Typography
              variant="h4"
              className="font-ebs text-ebs-green-default text-center place-self-center py-8 font-bold"
            >
              <FormattedMessage
                id="tnt.comp.comsoon.welcome"
                //defaultMessage={`EBS Services for ${instanceInfo.name} Instance`}
                defaultMessage={`Select one of the following tools`}
              />
            </Typography>
          </Grid>
        </Grid>
        <Grid container direction={"row"}>
          {domainList
            .filter((item) => domains.includes(item.domain.name))
            .slice()
            .sort(function (a, b) {
              return a.id - b.id;
            })
            .map((domain) => (
              <Grid item xs={6} md={3}
                key={domain.id}
                className=" transition duration-200 ease-in-out transform hover:-translate-y-1 scale-105 rounded sm:rounded-fully py-4 px-4 md:rounded-lg cursor-pointer"
              >
                <Card key={domain.id} className=" rounded-sm hover:shadow-sm  h-full">
                  <div className="text-right mt-3">
                    <FavoriteDomain
                      value={domain.domain.name}
                      data={instanceInfo.domaininstances}
                      userProfile={userProfile}
                      type="domains"
                      dataProduct={dataProduct}
                    />
                  </div>
                  <div style={{ display:'flex', justifyContent:'center' }}>
                  <CardMedia
                    className="w-1/2 h-1/2"
                    title={domain.domain.info}
                    onClick={() => handleClickDomain(domain)}
                  >
                    <img src={icons[`./${domain.domain.icon}`]} />
                  </CardMedia>
                  </div>
                  <CardContent>
                    <Grid  container direction={"column"}>
                      <Grid item xs={12}
                        onClick={() => handleClickDomain(domain)}
                      >
                        <Typography
                          variant="h4"
                          className="font-ebs font-black text-4xl "
                        >
                          {domain.domain.name}
                        </Typography>
                      </Grid>

                      <Grid
                        item
                        xs={12}

                        onClick={() => handleClickDomain(domain)}
                      >
                        <Typography
                          variant="body1"
                          className="font-ebs font-bold"
                        >
                          {domain.domain.info}
                        </Typography>
                      </Grid>
                    </Grid>
                  </CardContent>
                </Card>
              </Grid>
            ))}
        </Grid>
      </>


    );
  }
);

export default ProductListMolecule;
