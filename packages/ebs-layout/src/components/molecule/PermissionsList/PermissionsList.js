import React from "react";
import { Core, Icons } from "@ebs/styleguide";
const { ExpandLess, ExpandMore } = Icons;
const { Box, Typography, ListItem, ListItemText, List, Collapse } = Core;
const PermissionsListMolecule = React.forwardRef(({ userProfile }, ref) => {
  const [open, setOpen] = React.useState(true);

  // const handleClick = () => {
  //   setOpen(!open);
  // };

  return (
    <Box className="ml-10">
      <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
        {" "}
        Domains:
      </Typography>
      <List>
        {userProfile.permissions?.applications?.map((item, index) => (
          <ListItem key={index} id={index}>
            <ListItemText primary={item.domain} />
          </ListItem>
        ))}
      </List>

      <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
        Products by Domain:
      </Typography>
      {userProfile.permissions?.applications?.map((item, index) => {
        return (
          <List key={index} id={index}>
            <ListItem>
              <ListItemText
                className="font-ebs text-ebs-green-default text-xl p-1 uppercase font-bold"
                primary={item.domain}
              />
              {open}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <List component="div" disablePadding>
                <ListItem>
                  {item.products?.map((p, index) => (
                    <ListItemText primary={p.name} key={index} id={index} />
                  ))}
                </ListItem>
              </List>
            </Collapse>
          </List>
        );
      })}
    </Box>
  );
});
export default PermissionsListMolecule;
