import FavoriteList from './FavoriteList';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FavoriteList is in the DOM', () => {
  render(<FavoriteList></FavoriteList>)
  expect(screen.getByTestId('FavoriteListTestId')).toBeInTheDocument();
})
