import React, { useState, useEffect } from "react";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { useSelector, useStore, useDispatch } from "react-redux";
import { setUserData, modifyUserPreference } from "store/ducks/user";
import Progress from "components/atoms/Progress";
const {
  Box,
  Typography,
  Avatar,
  ListItem,
  ListItemText,
  ListItemAvatar,
  ListItemSecondaryAction,
  IconButton,
  ListItemIcon,
  List,
  ListSubheader,
  Tooltip,
} = Core;
const { Favorite, Delete, Star } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FavoriteListMolecule = React.forwardRef(({}, ref) => {
  const [state, setState] = useState(false);
  const { userProfile, userInfo } = useSelector(({ user }) => user);
  const { userState } = useSelector(({ auth }) => auth);
  const { getState } = useStore();
  const dispatch = useDispatch();
  useEffect(() => {
    if (state) {
      setUserData(userState)(dispatch, getState);
    }
    return () => setState(false);
  }, [state]);
  if (!userProfile || !userInfo)
    return (
      <div>
        <Progress />
      </div>
    );

  let preferenceObject = Object.assign({}, userProfile.preference);
  let _userInfo = Object.assign({}, userInfo);
  const deleteFavoriteColumn = (index) => {
    const newColumns = preferenceObject.favorites.columns.filter(
      (_, i) => i !== index
    );

    const newPreferenceObject = {
      ...preferenceObject,
      favorites: {
        ...preferenceObject.favorites,
        columns: newColumns,
      },
    };
    modifyUserPreference({
      id: Number(_userInfo.id),
      userName: _userInfo.userName,
      active: _userInfo.isActive,
      contactId: _userInfo.contact.id,
      preference: newPreferenceObject,
    })(dispatch);
    setState(true);
  };
  const deleteFavoriteDomain = (index) => {
    const newColumns = preferenceObject.favorites.domain.filter(
      (_, i) => i !== index
    );

    const newPreferenceObject = {
      ...preferenceObject,
      favorites: {
        ...preferenceObject.favorites,
        domain: newColumns,
      },
    };
    modifyUserPreference({
      id: Number(_userInfo.id),
      userName: _userInfo.userName,
      active: _userInfo.isActive,
      contactId: _userInfo.contact.id,
      preference: newPreferenceObject,
    })(dispatch);
    setState(true);
  };
  const deleteFavoriteProduct = (index) => {
    const newColumns = preferenceObject.favorites.products.filter(
      (_, i) => i !== index
    );

    const newPreferenceObject = {
      ...preferenceObject,
      favorites: {
        ...preferenceObject.favorites,
        products: newColumns,
      },
    };
    modifyUserPreference({
      id: Number(_userInfo.id),
      userName: _userInfo.userName,
      active: _userInfo.isActive,
      contactId: _userInfo.contact.id,
      preference: newPreferenceObject,
    })(dispatch);
    setState(true);
  };
  return (
    <Box>
      <List
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Products"}
              ></FormattedMessage>
            </Typography>
          </ListSubheader>
        }
      >
        {preferenceObject.favorites.products.length === 0 ? (
          <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
            <FormattedMessage
              id={"none"}
              defaultMessage={"No favorites in Products"}
            ></FormattedMessage>
          </Typography>
        ) : (
          preferenceObject.favorites.products.map((item, index) => (
            <ListItem button key={item.name} id={item.id}>
              <ListItemIcon>
                <Star color="primary" />
              </ListItemIcon>
              <ListItemText primary={item.nameDomain + " ➜ " + item.name} />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="delete"
                  onClick={() => deleteFavoriteProduct(index)}
                >
                  <Delete />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))
        )}
      </List>
      <List
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Domains"}
              ></FormattedMessage>
            </Typography>
          </ListSubheader>
        }
      >
        {preferenceObject.favorites.domain.length === 0 ? (
          <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
            <FormattedMessage
              id={"none"}
              defaultMessage={"No favorites in Domains"}
            ></FormattedMessage>
          </Typography>
        ) : (
          preferenceObject.favorites.domain.map((item, index) => (
            <ListItem button key={item.name} id={item.id}>
              <ListItemIcon>
                <Star color="primary" />
              </ListItemIcon>
              <ListItemText primary={item.name} />
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="delete"
                  onClick={() => deleteFavoriteDomain(index)}
                >
                  <Delete />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))
        )}
      </List>
      <List
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Columns"}
              ></FormattedMessage>
            </Typography>
          </ListSubheader>
        }
      >
        {preferenceObject.favorites.columns.length === 0 ? (
          <Typography className="font-ebs text-ebs-green-default text-xl p-3 uppercase font-bold">
            <FormattedMessage
              id={"none"}
              defaultMessage={"No favorites in Columns"}
            ></FormattedMessage>
          </Typography>
        ) : (
          preferenceObject.favorites.columns.map((item, index) => (
            <ListItem button key={item.idGrid} id={item.id}>
              <ListItemIcon>
                <Star color="primary" />
              </ListItemIcon>
              <Tooltip
                arrow
                title={
                  <Typography className="font-ebs text-lg">
                    {item?.columns?.length > 0
                      ? item?.columns?.map((col) => {
                          return " " + col.nameColumn + " ";
                        })
                      : ""}
                  </Typography>
                }
              >
                <ListItemText primary={item.idGrid} />
              </Tooltip>
              <ListItemSecondaryAction>
                <IconButton
                  edge="end"
                  aria-label="delete"
                  onClick={() => deleteFavoriteColumn(index)}
                >
                  <Delete />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))
        )}
      </List>
    </Box>
  );
});

export default FavoriteListMolecule;
