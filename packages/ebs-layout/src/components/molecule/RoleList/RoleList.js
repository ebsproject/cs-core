import React from "react";
import { Core } from "@ebs/styleguide";
const { Box, Typography, List, ListItem, ListItemText } = Core;

const RoleListMolecule = React.forwardRef(({ userProfile }, ref) => {
  return (
    <Box className="ml-10">
      <List>
        {userProfile?.permissions?.memberOf?.roles?.map((item, index) => (
          <ListItem key={index} id={index}>
            <ListItemText primary={item} />
          </ListItem>
        ))}
      </List>
    </Box>
  );
});
export default RoleListMolecule;
