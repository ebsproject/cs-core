import React, { useState } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { Core } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { useSelector } from "react-redux";
import { getCoreSystemContext } from "@ebs/layout";
import VersionView from "components/molecule/VersionView"
const { Typography, Dialog, DialogTitle, Button, Link } = Core;

const FooterMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { tenantInfo, tenantContext, instanceInfo } = useSelector(({ tenant }) => tenant);

  const [open, setOpen] = React.useState(false);

  const handleOpen = (event) => {
    event.preventDefault();
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div
      data-testid={"FooterTestId"}
      
    >
      <Typography className="absolute bottom-0 pl-2 text-sm" variant={"title"} >
        Copyright@2021 EBS-Core System <Link href="#" onClick={handleOpen}>View Versions</Link>
      </Typography>
      <Dialog onClose={handleClose} aria-labelledby="dialog-title" open={open}>
        <DialogTitle id="dialog-title">Module Versions</DialogTitle>
        <VersionView domains={instanceInfo?.domaininstances} />
      </Dialog>
    </div>
  );
});

// Type and required properties
FooterMolecule.propTypes = {};
// Default properties
FooterMolecule.defaultProps = {};

export default FooterMolecule;
