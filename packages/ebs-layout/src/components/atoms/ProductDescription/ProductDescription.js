import React, { memo } from "react";
import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import { getContext } from "utils/other/CrossMFE/CrossContext";
import { useLayoutContext } from "layout/layout-context";
import Cookies from "js-cookie";


const ProductDescriptionAtom = React.forwardRef(({ }, ref) => {
 const productSelected = Cookies.get("productSelected") && JSON.parse(Cookies.get("productSelected"));
const {name , description } = productSelected;
  return (
    <div>
      <Typography variant="h4" className="font-ebs text-ebs-green-900 text-left-0">
        {name || "Product Name"}
      </Typography>
      <Typography variant="h6" className="font-ebs text-ebs-black-900 text-left-0">
        {description || "Product Description"}
      </Typography>

    </div>

  )


})
// Type and required properties
ProductDescriptionAtom.propTypes = {};
// Default properties
ProductDescriptionAtom.defaultProps = {};

export default memo(ProductDescriptionAtom);