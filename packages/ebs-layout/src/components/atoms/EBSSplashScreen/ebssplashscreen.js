import React from "react";
import EbsSplash from "assets/images/logos/EBS_V-W.svg";
import { Core } from "@ebs/styleguide";
const { CircularProgress } = Core;

const EBSSplashScreenAtom = React.forwardRef((props, ref) => {
  return (

    <div  className= "flex flex-col h-screen justify-center items-center bg-ebs-gray-default w-screen">
        <img className={"m-16"} width="200" src={EbsSplash} alt="logo" />
        <CircularProgress />
    </div>
  
  );
});

export default EBSSplashScreenAtom;
