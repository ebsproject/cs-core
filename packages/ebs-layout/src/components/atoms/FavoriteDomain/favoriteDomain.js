import React from "react";
import { useNavigate } from "react-router-dom";
import { Core, Icons } from "@ebs/styleguide";
import { useDispatch, useSelector, useStore } from "react-redux";
import { setTenantContext } from "store/ducks/tenant";
import { showMessage } from "store/ducks/message";
import { modifyUserPreference } from "store/ducks/user";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
// import { setProductSelected } from "store/ducks/product";
const { FormControlLabel, Checkbox, Tooltip, Typography, Button, IconButton } =
  Core;
const { StarBorder, Star } = Icons;

const favoriteDomain = React.forwardRef(
  (
    {
      value,
      data,
      userProfile,
      type,
      userInfo,
      prefix,
      dataProduct,
      nameDomain,
    },
    ref
  ) => {
    const [checked, setChecked] = React.useState(false);
    const dispatch = useDispatch();
    const { getState } = useStore();
    const navigate = useNavigate();
    const { userProfile: userProfileContext } = useSelector(({ user }) => user);
    let preferenceObject = Object.assign({}, userProfileContext.preference);
    let _userProfile = Object.assign({}, userProfile);
    if (type === "domains") {
      const handleClickDomain = (item) => {
        if (item.mfe) {
          setTenantContext({ domainId: Number(item.domain.id) })(
            dispatch,
            getState
          );
          navigate(`/${item.domain.prefix}`);
        } else {
          item.context
            ? window.open(item.context, "__blank")
            : dispatch(
                showMessage({
                  message: "The component does not have URL",
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
        }
      };

      const handleChange = (event) => {
        setChecked(event.target.checked);
        favoriteDomainChange(event.target.value);
      };

      const favoriteDomainChange = (value) => {
        if (value === value) {
          data.forEach((item) => {
            if (item.domain.name === value) {
              const newDomainFavorites = [
                ...preferenceObject.favorites.domain,
                {
                  type: "Domain",
                  name: value,
                  prefix: item.domain.prefix,
                  mfe: item.mfe,
                  domainId: item.domain.id,
                  id: item.domain.id,
                  context: item.context,
                },
              ];

              const newPreferenceObject = {
                ...preferenceObject,
                favorites: {
                  ...preferenceObject.favorites,
                  domain: newDomainFavorites,
                },
              };

              modifyUserPreference({
                id: Number(_userProfile.id),
                userName: _userProfile.userName,
                active: _userProfile.isActive,
                contactId: _userProfile.contact.id,
                preference: newPreferenceObject,
              })(dispatch);

              setTimeout(() => handleClickDomain(item), 400);
            }
          });
        } else if (value === "") {
          return null;
        }
      };

      return (
        <div>
          <Tooltip
            title={
              <Typography>
                <FormattedMessage id="none" defaultMessage="Mark as favorite" />
              </Typography>
            }
            placement="left"
          >
            <FormControlLabel
              control={
                <Checkbox
                  className="hover:bg-green-50"
                  icon={<StarBorder />}
                  checkedIcon={<Star />}
                  checked={checked}
                  onChange={handleChange}
                  value={value}
                />
              }
            />
          </Tooltip>
        </div>
      );
    } else if (type === "products") {
      const handleClickProduct = () => {
        Cookies.set(
          "productSelected",
          JSON.stringify({
            id: data.id,
            name: data.name,
            description: data.description,
          })
        );
        navigate(`/${prefix}/${data.path}`);
      };

      const handleChange = (event) => {
        setChecked(event.target.checked);
        favoriteProductChange(event.target.value);
      };

      const favoriteProductChange = (value) => {
        if (value === value) {
          if (data.name === value) {
            const newProductFavorites = [
              ...preferenceObject.favorites.products,
              {
                name: data.name,
                prefix: data.path,
                id: data.id,
                domain: prefix,
                type: "Product",
                nameDomain: nameDomain,
              },
            ];

            const newPreferenceObject = {
              ...preferenceObject,
              favorites: {
                ...preferenceObject.favorites,
                products: newProductFavorites,
              },
            };

            modifyUserPreference({
              id: Number(userInfo.id),
              userName: userInfo.userName,
              active: userInfo.isActive,
              contactId: userInfo.contact.id,
              preference: newPreferenceObject,
            })(dispatch);
            setTimeout(() => handleClickProduct(), 400);
          }
        } else if (value === "") {
          return null;
        }
      };

      return (
        <div className="text-right">
          <Tooltip
            title={
              <Typography>
                <FormattedMessage id="none" defaultMessage="Mark as favorite" />
              </Typography>
            }
            placement="left"
          >
            <FormControlLabel
              control={
                <Checkbox
                  className="hover:bg-green-50"
                  icon={<StarBorder />}
                  checkedIcon={<Star />}
                  checked={checked}
                  onChange={handleChange}
                  value={value}
                />
              }
            />
          </Tooltip>
        </div>
      );
    }
  }
);

export default favoriteDomain;
