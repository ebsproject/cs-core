import React, { Fragment } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Core, Icons } from "@ebs/styleguide";
// import { useNavigate, useLocation } from "react-router-dom";
import { signOut } from "store/ducks/auth";
import { FormattedMessage } from "react-intl";

const {
  Popover,
  Typography,
  Icon,
  ListItemText,
  Avatar,
  Chip,
  MenuItem,
  Link,
  ListItemIcon,
  Divider,
  IconButton,
} = Core;

const { AccountCircle } = Icons;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AvatarProfileAtom = React.forwardRef((props, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [open, setOpen] = React.useState(false);
  const [placement, setPlacement] = React.useState();
  // const navigate = useNavigate();
  const dispatch = useDispatch();
  // const location = useLocation()
  const { userInfo } = useSelector(({ user }) => user);

  const handleClick = (newPlacement) => (event) => {
    setAnchorEl(event.currentTarget);
    setOpen((prev) => placement !== newPlacement || !prev);
    setPlacement(newPlacement);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleLogout = () => {
    dispatch(signOut());
  };

  const handleProfile = () => {
    handleClose();
    window.location.href = `/profile`;
    //  navigate({pathname:"/profile",state:{location:location.pathname}});
  };

  if (userInfo) {
    return (
      /* 
     @prop data-testid: Id to use inside avatarprofile.test.js file.
     */
      <React.Fragment>
        {userInfo.userName ? (
          <Chip
            avatar={<Avatar alt="user photo" src={userInfo.photo} />}
            onClick={handleClick("bottom-end")}
            variant="default"
            color="primary"
            clickable
          />
        ) : (
          <Chip size="small" />
        )}
        <Popover
          open={open}
          anchorEl={anchorEl}
          onClose={handleClose}
          anchorOrigin={{
            vertical: "bottom",
            horizontal: "center",
          }}
          transformOrigin={{
            vertical: "top",
            horizontal: "center",
          }}
        >
          <MenuItem onClick={handleProfile}>
            <ListItemIcon>
              <Icon>account_circle</Icon>
            </ListItemIcon>
            <ListItemText
              primary={
                <FormattedMessage
                  id="tnt.nav.profile.lbl.profile"
                  defaultMessage="Profile"
                />
              }
            />
          </MenuItem>
          <MenuItem onClick={handleLogout}>
            <ListItemIcon>
              <Icon>exit_to_app</Icon>
            </ListItemIcon>
            <ListItemText
              primary={
                <FormattedMessage
                  id="tnt.nav.profile.lbl.logout"
                  defaultMessage="Logout"
                />
              }
            />
          </MenuItem>
        </Popover>
      </React.Fragment>
    );
  } else {
    return <Fragment />;
  }
});
// Type and required properties
AvatarProfileAtom.propTypes = {};
// Default properties
AvatarProfileAtom.defaultProps = {};

export default AvatarProfileAtom;
