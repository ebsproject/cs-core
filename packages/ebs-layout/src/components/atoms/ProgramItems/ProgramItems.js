import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Core } from "@ebs/styleguide";

const { Grid, Tooltip, Typography } = Core;

const ProgramItemsAtom = React.forwardRef(() => {
  const [value, setValue] = useState(null);
  const { userProfile } = useSelector(({ user }) => user);

  useEffect(() => {
    if (userProfile) {
      let item;
      item = userProfile.permissions?.memberOf?.programs?.map((p) => {
        return { name: p.name, id: p.id, code: p.code };
      });
      let valuesName = [];
      if (item) {
        for (let i = 0; i < item.length; i++) {
          valuesName.push(item[i].code);
        }
        setValue(valuesName);
      }
    }
  }, [userProfile]);

  return (
    <Tooltip
      arrow
      title={
        <Typography className="font-ebs text-xl">{"User program"}</Typography>
      }
    >
      <div className="flex flex-nowrap w-max">
        {value !== null &&
          value.map((item, index) => (
            <Grid key={index}>
              <span
                key={index + 0.01}
                style={{
                  backgroundColor:
                    "#" +
                    (
                      0x1000000 +
                      (item > 0 ? item : item.length) * 30965 * 0xffff
                    )
                      .toString(16)
                      .substr(1, 6),
                }}
                className="px-4 py-2 m-0.5 rounded-full text-white font-bold text-sm"
              >
                {item}
              </span>
            </Grid>
          ))}
      </div>
    </Tooltip>
  );
});
export default ProgramItemsAtom;
