import React from "react";
import { Core } from "@ebs/styleguide";
const { Grid,Tooltip,Typography } = Core;

const FilterItemsAtom = React.forwardRef(({ item }, ref) => {
    let node = Object.values(item?.filterValues)
    let valuesName = []
    for (let i = 0; i < node.length; i++) {
        for (let j = 0; j < node[i]?.length; j++) {
            valuesName.push(node[i][j].code)
        }
    }
    return (
        <Tooltip
        arrow
        title={
            <Typography className='font-ebs text-xl'>{"Current filter"}</Typography>
        }
        >
        <div className="flex flex-nowrap w-max">
            {valuesName.map((item, index) => (
                <Grid key={index} >
                    <span key={index + 0.01} style= {{backgroundColor:"#"+(0x1000000 + ( item > 0 ? item : item.length )*30965*0xffff).toString(16).substr(1,6)}}
                        className="px-4 py-2 m-0.5 rounded-full text-white font-bold text-sm">
                        {item}
                    </span>
                </Grid>
            ))}
        </div>
        </Tooltip>
    )
})
export default FilterItemsAtom