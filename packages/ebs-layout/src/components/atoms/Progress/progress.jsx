import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { LinearProgress } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ProgressAtom = React.forwardRef((props, ref) => {

  return (
    /*
     @prop data-testid: Id to use inside progress.test.js file.
     */
    <div data-testid={"ProgressTestId"}>
      <LinearProgress />
    </div>
  );
});
// Type and required properties
ProgressAtom.propTypes = {};
// Default properties

export default ProgressAtom;
