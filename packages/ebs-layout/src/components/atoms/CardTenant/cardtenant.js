import React from "react";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/client";
import { FIND_TENANT } from "utils/apollo/gql/tenant";
import { Core } from "@ebs/styleguide";
const {
  Card,
  CardContent,
  CardActions,
  CardMedia,
  Grid,
  Typography,
} = Core;


const classes = {
  root: {
    maxWidth: 345,
    width: 250,
    height: 350,
    padding:10
  },

  media: {
    maxWidth: 250,
    width: "250",
    maxHeight: 155,
    height: "155",
    padding:10
  },
}

const CardTenantAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { tenantId } = props;
  const { loading, error, data } = useQuery(FIND_TENANT, {
    variables: { id: tenantId },
  });

  if (error) throw error;
  if (loading) return null;

  const tenant = data.findTenant;

  return (
    <Card ref={ref} sx={classes.root}>
      <Typography variant="h5" className="font-ebs">
      {'Tenant Registration ID'}
      </Typography>
      <Typography className="font-ebs" variant="h6">
        {tenant.name}
      </Typography>
      <CardMedia
        sx={classes.media}
        component="img"
        image={`data:image/jpeg;base64,${tenant.customer.logo}`}
        title={tenant.name}
        height={120}
      />
      <CardContent>
        <Grid container>
          <Grid item xs={12}>
            <Typography className="font-ebs" align="left">
              {`Customer Name: ${tenant.customer.name}`}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className="font-ebs" align="left">
              {`Organization Name: ${tenant.organization.name}`}
            </Typography>
          </Grid>
          <Grid item xs={12}>
            <Typography className="font-ebs" align="left">
              {`Admin Contact: ${tenant.customer.officialEmail}`}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
      <CardActions>{props.children}</CardActions>
    </Card>
  );
});
// Type and required properties
CardTenantAtom.propTypes = {
  tenantId: PropTypes.number.isRequired,
};
// Default properties
CardTenantAtom.defaultProps = {
  tenantId: 0,
};

export default CardTenantAtom;
