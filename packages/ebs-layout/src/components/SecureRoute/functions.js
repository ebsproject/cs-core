export function getAccessResolve(userProfile) {
    let access = false;
    let domainId = userProfile.currentDomain.domainId;
    let basepath = userProfile.currentPath;
    const { location } = userProfile
    const applications = userProfile.permissions.applications;
    const currentDomainApplications = applications.find(item => item.id === domainId);
    const products = currentDomainApplications.products.filter(item => item.showMenu) || [];
    const allowedPaths = products.map(item => `${basepath}/${item.path}`)
    const { pathname } = location;

    allowedPaths.forEach(path => {
        if (pathname.includes(path)) {
            access = true
        }

    });

    return access;
}