import { useLayoutContext } from "layout/layout-context";
import { getContext } from "utils/other/CrossMFE/CrossContext";
import { getAccessResolve } from "./functions";
import AccessDenied from "./accessDenied";
import { useLocation } from "react-router-dom";
const SecureRoute = ({ children, path }) => {

    const { userProfile } = useLayoutContext();
    if (!userProfile) return null;
    const domains = getContext();
    const location = useLocation();
    const access = getAccessResolve({ ...userProfile, currentDomain: domains, currentPath: path, location } || {});

    if (!access) return <AccessDenied />
    return (
        <>
            {children}
        </>
    )
}
export default SecureRoute;