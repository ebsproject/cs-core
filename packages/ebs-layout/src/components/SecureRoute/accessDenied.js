import React from 'react';
const styles = {
    container: {
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      height: '56vh',
    },
    message: {
      fontSize: '1.5rem',
      textAlign: 'center',
      padding: '20px',
      backgroundColor: '#f8d7da',
      color: '#721c24',
      border: '1px solid #f5c6cb',
      borderRadius: '5px',
    },
  };
const AccessDenied = ({message}) => {
  return (
    <div style={styles.container}>
      <p style={styles.message}>{message ? message : "Sorry, you do not have access to this content."}</p>
    </div>
  );
};



export default AccessDenied;
