
import React from "react";
import { Core } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { useQuery } from "@apollo/client";
import { FIND_DOMAIN_LIST } from "utils/apollo/gql/tenant";
import { getCoreSystemContext } from "utils/other/CrossMFE/CrossContext";
const { Typography } = Core;

export const Uri = ({ }) => {
    const { printoutUri } = getCoreSystemContext();
    const { loading, error, data } = useQuery(FIND_DOMAIN_LIST, {
        fetchPolicy: "no-cache",
    });
    if (loading) return null;
    if (error) return `Error! ${error}`;
    const domains = data.findDomainList.content
    const uriSM = domains.filter(item => item.id === "4")[0].domaininstances[0].sgContext;
    const uriCS = domains.filter(item => item.id === "2")[0].domaininstances[0].sgContext;
    return (
        <div>
            <a href={`${uriCS}playground`} target="_blank">
                <Typography
                    className="font-ebs font-extrabold ml-3 mt-2 mb-5 mr-8 text-base">
                    <FormattedMessage id="none" defaultMessage={"CS Playground"} />
                </Typography>
            </a>
            <a href={`${uriCS}swagger-ui/index.html`} target="_blank">
                <Typography
                    className="font-ebs font-extrabold ml-3 mt-2 mb-5 mr-8  text-base">
                    <FormattedMessage id="none" defaultMessage={"CS Swagger"} />
                </Typography>
            </a>
            <a href={`${printoutUri}/swagger/index.html`} target="_blank">
                <Typography
                    className="font-ebs font-extrabold ml-3 mt-2 mb-5 mr-8  text-base">
                    <FormattedMessage id="none" defaultMessage={"CS PS Swagger"} />
                </Typography>
            </a>
            <a href={`${uriSM}playground`} target="_blank">
                <Typography
                    className="font-ebs font-extrabold ml-3 mt-2 mb-5 text-base">
                    <FormattedMessage id="none" defaultMessage={"SM Playground"} />
                </Typography>
            </a>
        </div>
    )
};
export default Uri;