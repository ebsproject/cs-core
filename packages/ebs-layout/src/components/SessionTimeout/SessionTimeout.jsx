import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { Core } from "@ebs/styleguide";
const { Typography, Button, Dialog, DialogContent, DialogActions, DialogTitle, Divider } = Core;
import { useSelector, useDispatch } from "react-redux";
import { FormattedMessage } from "react-intl";
import { refreshTokenFunction, signOutByInactivity } from "store/ducks/auth";

const SessionTimeout = () => {
    const [events, setEvents] = useState(['click', 'scroll']);
    const [second, setSecond] = useState(0);
    const [isOpen, setOpen] = useState(false);
    const { isSignIn, refreshIdToken } = useSelector(
        ({ auth }) => auth
      );
    const dispatch = useDispatch();
    let timeStamp;
    let warningInactiveInterval;

    // start inactive check
    const timeChecker = () => {
            let storedTimeStamp = sessionStorage.getItem('lastTimeStamp');
            if(!storedTimeStamp) return;
                warningInactive(storedTimeStamp);
    };
    // warning timer
    const warningInactive = (timeString) => {

        warningInactiveInterval = setInterval(() => {
            const maxTime = 16;
            const popTime = 15;

            const diff = moment.duration(moment().diff(moment(timeString)));
            const minPast = diff.minutes();
            const leftSecond = 60 - diff.seconds();
            if (minPast === popTime) {
                setSecond(leftSecond);
                setOpen(true);
            }
            if (minPast === maxTime) {
                closeSessionNow()
            }
        }, 1000);
    };
    const closeSessionNow = () => {
        clearInterval(warningInactiveInterval);
        setOpen(false);
        sessionStorage.removeItem('lastTimeStamp');
        dispatch(signOutByInactivity());
    }
    // reset interval timer
    const resetTimer = () => {
        clearInterval(warningInactiveInterval);

        if (isSignIn) {
            timeStamp = moment();
            sessionStorage.setItem('lastTimeStamp', timeStamp);
        } else {
            sessionStorage.removeItem('lastTimeStamp');
        }
        timeChecker();
        setOpen(false);
    }

    const handleClose = () => {
        setOpen(false);
        dispatch(refreshTokenFunction(refreshIdToken));
        resetTimer();
    };

    useEffect(() => {
        events.forEach((event) => {
            window.addEventListener(event, resetTimer);
        });
        timeChecker();
        return () => {
            clearInterval(warningInactiveInterval);
        };
    }, [resetTimer, events, timeChecker]);

    return (
        <div>
            <Dialog onClose={handleClose} open={isOpen} aria-label="sessionExpire">
                <DialogTitle>
                    <FormattedMessage id="none" defaultMessage="Session will expire" />
                </DialogTitle>
                <DialogContent>
                    <Typography className=' font-ebs text-lg'>
                        {`In 
                        ${second} 
                        seconds your session will expire, continue in session?`}
                    </Typography>
                    <Divider />
                    <DialogActions>
                        <Button onClick={closeSessionNow} color="secondary">
                            <FormattedMessage id="none" defaultMessage="Logout" />
                        </Button>
                        <Button onClick={handleClose} color="primary"  >
                            <FormattedMessage id="none" defaultMessage="Stay" />
                        </Button>
                    </DialogActions>
                </DialogContent>
            </Dialog>
        </div>
    )
};

export default SessionTimeout;