import React from "react";
import ErrorView from "page/Error"


export default function ErrorFallback({ error, resetErrorBoundary }) {

  return (
    <div>
      <ErrorView error = {error}/>
    </div>
  );
}
