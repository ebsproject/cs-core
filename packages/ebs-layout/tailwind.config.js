module.exports = {
  content: ["./src/**/*.html", "./src/**/*.js"],
  theme: {
    extend: {
      colors: {
        transparent: "transparent",
        current: "currentColor",
        "ebs-splash": "#121212",
        "ebs-green": {
          default: "#009688",
          900: "#00695C",
        },
        "ebs-brand": {
          default: "#009688",
          900: "#00695C",
        },
        "ebs-login": {
          default: "#6FBF3D",
          900: "#3dbf4c",
        },
        "ebs-gray": {
          default: "#535461",
        },
      },
      fontFamily: {
        ebs: ["Raleway"],
      },
    },
  },
  plugins: [],
};
