import React, { useState } from "react";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/client";
import { BrowserRouter, Route, Routes, Outlet } from "react-router-dom";
import { IntlProvider } from "react-intl";
import store from "store";
import { client } from "utils/apollo";
import Settings from "pages/Settings";
import ReportPreview from "pages/ReportPreview";
//import Connection from "components/organism/Connection";
// Routes
import { BASE_PATH, PRINTOUT, PRINT_PREVIEW } from "./routes";

function MainPrintOut() {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default function App() {
  const [locale, setLocale] = useState("en");
  const [messages, setMessages] = useState(null);

  return (
    <Provider store={store}>
      <ApolloProvider client={client}>
        <IntlProvider locale={locale} messages={messages} defaultLocale="en">
          <BrowserRouter>
            <Routes>
              <Route path={BASE_PATH} element={<MainPrintOut />}>
                <Route path={PRINTOUT} element={<Settings />} />
                <Route path={PRINT_PREVIEW} element={<ReportPreview />} />
              </Route>
            </Routes>
          </BrowserRouter>
        </IntlProvider>
      </ApolloProvider>
    </Provider>
  );
}
