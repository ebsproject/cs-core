import { client } from "utils/apollo";
import { client as printoutClient } from "utils/axios";
import {
  CREATE_PRINTOUT_TEMPLATE,
  ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
  ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
  FIND_PRINTOUT_TEMPLATE_LIST,
  FIND_PROGRAM_LIST,
  FIND_PRODUCT_LIST,
  REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
  REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
  MODIFY_PRINTOUT_TEMPLATE,
  DELETE_PRINTOUT_TEMPLATE,
} from "utils/apollo/gql/Printout";
import { showMessage } from "store/modules/message";
import { FIND_DOMAIN_LIST } from "utils/apollo/gql/tenant";
/*
 Initial state and properties
 */
export const initialState = {
  templateList: [],
  programList: [],
  productList: [],
  loading: false,
  templateProcessing: false,
  state: "NONE",
  error: null,
  domainList:null
};
/*
 Action types
 */
const GET_TEMPLATE_LIST = "[PRINTOUT]GET_TEMPLATE_LIST";
const SET_TEMPLATE_LIST = "[PRINTOUT]SET_TEMPLATE_LIST";
const GET_PROGRAM_LIST = "[PRINTOUT]GET_PROGRAM_LIST";
const SET_PROGRAM_LIST = "[PRINTOUT]SET_PROGRAM_LIST";
const GET_PRODUCT_LIST = "[PRINTOUT]GET_PRODUCT_LIST";
const SET_PRODUCT_LIST = "[PRINTOUT]SET_PRODUCT_LIST";
const SET_DOMAIN_LIST = "[PRINTOUT]SET_DOMAIN_LIST";
const BLANK = "[PRINTOUT]CREATE_REPORT_TEMPLATE_BLANK";
const COPY = "[PRINTOUT]CREATE_REPORT_TEMPLATE_COPY";
const UPDATE = "[PRINTOUT]UPDATE_REPORT_TEMPLATE";
const DELETE = "[PRINTOUT]DELETE_REPORT_TEMPLATE";
const REPORT_PROCESS_SUCCESS = "[PRINTOUT]REPORT_PROCESS_SUCCESS";
const RESET_REPORT_STATE = "[PRINTOUT]RESET_REPORT_STATE";
const SET_ERROR = "[PRINTOUT]SET_ERROR";
/*
 Arrow function for change state
 */
const getData = (type) => ({ type });
export const getDomainList = async (dispatch)=>{

  const {data } = await client.query({
    query:FIND_DOMAIN_LIST
  })

dispatch(setDomainList(data.findDomainList.content))
}
 const setDomainList = (payload ) => (
  {
  type : SET_DOMAIN_LIST,
  payload,
}

);
const setData = ({ type, payload }) => ({
  type,
  payload,
});
const reportProcess = (type) => ({ type });
const setError = (payload) => ({
  type: SET_ERROR,
  payload,
});
/**
 * Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case GET_TEMPLATE_LIST:
      return { ...state, loading: true };
    case SET_DOMAIN_LIST:
      return {
        ...state,
        domainList: payload,
      }
    case SET_TEMPLATE_LIST:
      return { ...state, loading: false, templateList: payload };
    case GET_PROGRAM_LIST:
      return { ...state, loading: true };
    case SET_PROGRAM_LIST:
      return { ...state, loading: false, programList: payload };
    case GET_PRODUCT_LIST:
      return { ...state, loading: true };
    case SET_PRODUCT_LIST:
      return { ...state, loading: false, productList: payload };
    case BLANK:
      return { ...state, templateProcessing: true };
    case COPY:
      return { ...state, templateProcessing: true };
    case UPDATE:
      return { ...state, templateProcessing: true };
    case DELETE:
      return { ...state, templateProcessing: true };
    case REPORT_PROCESS_SUCCESS:
      return { ...state, templateProcessing: false, state: "DONE" };
    case RESET_REPORT_STATE:
      return { ...state, state: "NONE" };
    case SET_ERROR:
      return { ...state, error: payload, templateProcessing: false };
    default:
      return state;
  }
}
/**
 * @param {String} filter: Optional
 */
export const getTemplateList = (filter) => async (dispatch) => {
  try {
    dispatch(getData(GET_TEMPLATE_LIST));
    const { data, errors } = await client.query({
      query: FIND_PRINTOUT_TEMPLATE_LIST,
      variables: {
        page: { number: 1, size: 500 },
        filters: filter && [{ col: "name", mod: "LK", val: filter }],
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(
      setData({
        type: SET_TEMPLATE_LIST,
        payload: data.findPrintoutTemplateList.content,
      })
    );
  } catch (error) {
    dispatch(setError(JSON.stringify(error)));
  }
};
/**
 *
 */
export const getProgramList = () => async (dispatch) => {
  try {
    dispatch(getData(GET_PROGRAM_LIST));
    const { data, errors } = await client.query({
      query: FIND_PROGRAM_LIST,
      variables: {
        page: { number: 1, size: 100 }
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(
      setData({ type: SET_PROGRAM_LIST, payload: data.findProgramList.content })
    );
  } catch (error) {
    dispatch(setError(JSON.stringify(error)));
  }
};
/**
 *
 */
export const getProductList = () => async (dispatch) => {
  try {    
    dispatch(getData(GET_PRODUCT_LIST));
    const { data, errors } = await client.query({
      query: FIND_PRODUCT_LIST,
      variables: {  page: { number: 1, size: 100 } },
    });
    if (errors) {
      throw new Error(errors[0].message);
    }
    let productList = new Set();
    data.findProductList.content.map((product) => {
      productList.add(product)
    });
    dispatch(
      setData({ type: SET_PRODUCT_LIST, payload: Array.from(productList) })
    );
  } catch (error) {
    dispatch(setError(JSON.stringify(error)));
  }
};
/**
 * @param {Object[]} programs
 * @param {Object[]} products
 * @param {String} name
 * @param {String} label
 * @param {String} zpl
 * @param {String} description
 */
const saveMetadata =
  ({ programs, products, name, zpl, description, tenants, defaultFormat, label }) =>
    async (dispatch) => {
      try {
        let programIds = new Set();
        let productIds = new Set();
        let tenantIds = [1];
        programs.map((program) => programIds.add(Number(program.id)));
        products.map((product) => productIds.add(Number(product.id)));
        const { data, errors: templateCreation } = await client.mutate({
          mutation: CREATE_PRINTOUT_TEMPLATE,
          variables: {
            printoutTemplate: {
              id: 0,
              name:label,
              zpl,
              description,
              tenantIds,
              defaultFormat,
              label,
            },
          },
        });
        if (templateCreation) {
          throw new Error(`${templateCreation[0].message}`);
        }
        const { errors: programsCreation } = await client.mutate({
          mutation: ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
          variables: {
            printoutTemplateId: data.createPrintoutTemplate.id,
            programIds: Array.from(programIds),
          },
        });
        if (programsCreation) {
          throw new Error(`${programsCreation[0].message}`);
        }
        const { errors: productsCreation } = await client.mutate({
          mutation: ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
          variables: {
            printoutTemplateId: Number(data.createPrintoutTemplate.id),
            productIds: Array.from(productIds),
          },
        });
        if (productsCreation) {
          throw new Error(`${productsCreation[0].message}`);
        }
        if (!templateCreation && !programsCreation && !productsCreation) {
          dispatch(reportProcess(REPORT_PROCESS_SUCCESS));
          dispatch(
            showMessage({
              message: "Successfully created printout template",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        }
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {
        dispatch(reportProcess(RESET_REPORT_STATE));
      }
    };
/**
 * @param {String} name
 * @param {Object} ...other
 */
const createReport =
  ({ name, label, ...other }) =>
    async (dispatch) => {
      try {
        const { status } = await printoutClient.post(
          `/api/Report/Create?name=${label}`
        );
        (status === 200 && !label.includes("sys_")) && saveMetadata({ label, ...other })(dispatch);
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString().includes("404") ? `The Report with name "${label}" already exits.` : error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {
        getTemplateList()(dispatch);
      }
    };
const deleteDataSource =
  (connectionName) =>
    async (dispatch) => {
      try {
        const { status } = await printoutClient.post(`/api/Report/DeleteDataSource?connectionName=${connectionName}`
        );
        dispatch(reportProcess(REPORT_PROCESS_SUCCESS));
        dispatch(
          showMessage({
            message: "Successfully deleted connection",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {
        dispatch(reportProcess(RESET_REPORT_STATE));
      }
    };
/**
* @param {String} connectionName
* @param {String} Uri
* @param {String} Method
* @param {Boolean} requiredAuth
* @param {Boolean} queryParam
* @param {Boolean} pathPartam
* @param {Object} ...other
*/
export const createDataSource = async (data, dispatch, account, dateCon, type,putCreatedBy,putCreatedOn) => {
  {type === 'NEW' ?
    data = {...data, CreatedBy:account, CreatedOn:dateCon}
     :
     data = {...data, CreatedBy:putCreatedBy, CreatedOn:putCreatedOn, UpdateBy:account, UpdateOn:dateCon};
    }       
      try {
        const { status } = await printoutClient
          .post(`/api/Report/AddConnection`, data);
        dispatch(reportProcess(REPORT_PROCESS_SUCCESS));
        dispatch(
          showMessage({
            message: "Successfully created connection",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {
        dispatch(reportProcess(RESET_REPORT_STATE));
      }
    };
/**
 * @param {Object} template
 * @param {String} name
 * @param {Object} ...other
 */
const copyReport =
  ({ template, name,label, ...other }) =>
    async (dispatch) => {
      try {
        const { status } = await printoutClient.post(
          `/api/Report/Copy?source=${template.name}&destination=${label}`
        );
        status === 200 && saveMetadata({ label, ...other })(dispatch);
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {
        getTemplateList()(dispatch);
      }
    };
/**
 *
 * @param {Object[]} oldProducts
 * @param {Object[]} oldPrograms
 * @param {ID} id
 * @param {Object[]} programs
 * @param {Object[]} products
 * @param {String} name
 * @param {String} description
 * @param {String} zpl
 * @returns
 */
const updateReport =
  ({
    oldProducts,
    oldPrograms,
    id,
    products,
    programs,
    tenants,
    name,
    description,
    zpl,
    uris,
    defaultFormat,
    label
  }) =>
    async (dispatch) => {
      try {
        const newProducts = new Array();
        const deleteProducts = new Array();
        const newPrograms = new Array();
        const deletePrograms = new Array();
        let tenantIds = [1];

        //tenants.map((tenant) => tenantIds.push(Number(tenant.id)));
        // * Defining products to delete and insert
        products.map((product) => {
          if (oldProducts.findIndex((e) => e.id === product.id) === -1) {
            newProducts.push(product);
          }
        });
        oldProducts.map((product) => {
          if (products.findIndex((e) => e.id === product.id) === -1) {
            deleteProducts.push(product);
          }
        });
        // * Defining programs to delete and insert
        programs.map((program) => {
          if (oldPrograms.findIndex((e) => e.id === program.id) === -1) {
            newPrograms.push(program);
          }
        });
        oldPrograms.map((program) => {
          if (programs.findIndex((e) => e.id === program.id) === -1) {
            deletePrograms.push(program);
          }
        });
        // * Updating Template Metadata
        const { errors: Template } = await client.mutate({
          mutation: MODIFY_PRINTOUT_TEMPLATE,
          variables: {
            printoutTemplate: {
              id: Number(id),
              description,
              name,
              zpl,
              tenantIds,
              defaultFormat,
              label
            },
          },
        });
        if (Template) {
          throw new Error(`${Template[0].message}`);
        }
        // * Removing old Products
        const { errors: productsRm } = await client.mutate({
          mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
          variables: {
            printoutTemplateId: Number(id),
            productIds: deleteProducts.map((item) => Number(item.id)),
          },
        });
        if (productsRm) {
          throw new Error(`${productsRm[0].message}`);
        }
        // * Removing old Programs
        const { errors: programsRm } = await client.mutate({
          mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
          variables: {
            printoutTemplateId: Number(id),
            programIds: deletePrograms.map((item) => Number(item.id)),
          },
        });
        if (programsRm) {
          throw new Error(`${programsRm[0].message}`);
        }
        // * Adding Products
        const { errors: productsAdd } = await client.mutate({
          mutation: ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS,
          variables: {
            printoutTemplateId: Number(id),
            productIds: newProducts.map((item) => Number(item.id)),
          },
        });
        if (productsAdd) {
          throw new Error(`${productsAdd[0].message}`);
        }
        // * Adding Programs
        const { errors: programsAdd } = await client.mutate({
          mutation: ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS,
          variables: {
            printoutTemplateId: Number(id),
            programIds: newPrograms.map((item) => Number(item.id)),
          },
        });
        if (programsAdd) {
          throw new Error(`${programsAdd[0].message}`);
        }
        // * Updating Data Source URI
        /*    uris.map(async (uri) => {
              try {
                const { data } = await printoutClient.put(
                  `/api/Report/Datasource?name=${name}&uri=${uri.value}`
                );
              } catch (error) {
                if (printoutClient.isAxiosError(error)) {
                  handleAxiosError(error);
                } else {
                  handleUnexpectedError(error);
                }
              }
            });*/
        dispatch(reportProcess(REPORT_PROCESS_SUCCESS));
        dispatch(
          showMessage({
            message: "Successfully updated printout template",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {
        dispatch(reportProcess(RESET_REPORT_STATE));
      }
    };

const deleteReportRow = (report) => async (dispatch) => {
  //  reportList.map(async (report) => {
  try {
    let programIds = new Array();
    let productIds = new Array();
    report.programs.map((program) => programIds.push(Number(program.id)));
    report.products.map((product) => productIds.push(Number(product.id)));
    // * Removing programs
    const { errors: Programs } = await client.mutate({
      mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
      variables: {
        printoutTemplateId: Number(report.id),
        programIds: programIds,
      },
    });
    if (Programs) {
      throw new Error(`${Programs[0].message}`);
    }
    // * Removing products
    const { errors: Products } = await client.mutate({
      mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
      variables: {
        printoutTemplateId: Number(report.id),
        productIds,
      },
    });
    if (Products) {
      throw new Error(`${Products[0].message}`);
    }
    // * Removing Template Metadata
    const { errors: Template } = await client.mutate({
      mutation: DELETE_PRINTOUT_TEMPLATE,
      variables: {
        printoutTemplateId: Number(report.id),
      },
    });
    if (Template) {
      throw new Error(`${Template[0].message}`);
    }
    const { status } = await printoutClient.delete(
      `/api/Report/Delete?name=${report.name}`
    );
    if (status === 200 && !Template && !Products && !Programs) {
      dispatch(reportProcess(REPORT_PROCESS_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully deleted printout template",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(JSON.stringify(error)));
  } finally {
    dispatch(reportProcess(RESET_REPORT_STATE));
  }
  //  });
};

/**
 * @param {Object[]} reportList
 */
const deleteReport = (reportList) => async (dispatch) => {
  reportList.map(async (report) => {
    try {
      let programIds = new Array();
      let productIds = new Array();
      report.programs.map((program) => programIds.push(Number(program.id)));
      report.products.map((product) => productIds.push(Number(product.id)));
      // * Removing programs
      const { errors: Programs } = await client.mutate({
        mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS,
        variables: {
          printoutTemplateId: Number(report.id),
          programIds: programIds,
        },
      });
      if (Programs) {
        throw new Error(`${Programs[0].message}`);
      }
      // * Removing products
      const { errors: Products } = await client.mutate({
        mutation: REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS,
        variables: {
          printoutTemplateId: Number(report.id),
          productIds,
        },
      });
      if (Products) {
        throw new Error(`${Products[0].message}`);
      }
      // * Removing Template Metadata
      const { errors: Template } = await client.mutate({
        mutation: DELETE_PRINTOUT_TEMPLATE,
        variables: {
          printoutTemplateId: Number(report.id),
        },
      });
      if (Template) {
        throw new Error(`${Template[0].message}`);
      }
      const { status } = await printoutClient.delete(
        `/api/Report/Delete?name=${report.name}`
      );
      if (status === 200 && !Template && !Products && !Programs) {
        dispatch(reportProcess(REPORT_PROCESS_SUCCESS));
        dispatch(
          showMessage({
            message: "Success",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      }
    } catch (error) {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(setError(JSON.stringify(error)));
    } finally {
      dispatch(reportProcess(RESET_REPORT_STATE));
    }
  });
};
/**
 * @param {String} type
 * @param {Object} data
 */
export const deleteReportAPI =  (connectionName) =>  (dispatch)  =>  {
  deleteDataSource(connectionName)(dispatch);
}
/**
 * @param {String} name
 * @param {Object} ...other
 */
export const printZPLCode =
  (name, printerIP, zplCode) =>
    async (dispatch) => {
      try {
        const { status } = await printoutClient.get(
          `/api/Report/PrintZPL?name=${name}&ipAddress=${printerIP}&zplCode=${zplCode}`
        );
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString().includes("404") ? `The zpl code cannot be printed.` : error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        dispatch(setError(JSON.stringify(error)));
      } finally {

      }
    };

export const mutationReport = ({ type, data }) =>
    async (dispatch) => {
      dispatch(reportProcess(type));
      switch (type) {
        case "NEW":
          await createDataSource(data,dispatch);
          break;
        case "BLANK":
          await createReport(data)(dispatch);
          break;
        case "COPY":
          await copyReport(data)(dispatch);
          break;
        case "PUT":
          await updateReport(data)(dispatch);
          break;
        case "PUT_DATASOURCE":
          await createDataSource(data,dispatch);
          break;
        case "DELETE_DATASOURCE":
          await deleteDataSource(data)(dispatch);
          break;
        case "DELETE":
          await deleteReportRow(data)(dispatch);
          break;
        default:
          break;
      }
    };
