// import { createStore, compose, applyMiddleware } from "redux";
// import {thunk} from "redux-thunk";
// import {
//   getTemplateList,
//   getProgramList,
//   getProductList,
// } from "store/modules/Printout";

// import reducers from "./reducers";
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// const store = createStore(
//   reducers,
//   composeEnhancers(applyMiddleware(thunk))
// );

// store.dispatch(getTemplateList());
// store.dispatch(getProgramList());
// store.dispatch(getProductList());

// export default store;
import { configureStore } from '@reduxjs/toolkit';
import { thunk } from 'redux-thunk';
// import { persistStore, persistReducer } from 'redux-persist';
// import sessionStorage from 'redux-persist/lib/storage/session';
import reducers from './reducers';
import {
  getTemplateList,
  getProgramList,
  getProductList,
} from "store/modules/Printout";

// const persistConfig = {
//   key: 'user',
//   whitelist: ['user', 'product'],
//   storage: sessionStorage,
// };

// const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  reducer: reducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['persist/PERSIST', 'persist/REHYDRATE'],
      },
    }).concat(thunk),
  devTools: process.env.NODE_ENV !== 'production',
});
store.dispatch(getTemplateList());
store.dispatch(getProgramList());
store.dispatch(getProductList());

// export const persistor = persistStore(store);
export default store;

