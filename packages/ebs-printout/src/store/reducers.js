import { combineReducers } from "redux";
import printout from "store/modules/Printout";
import message from "store/modules/message";

export default combineReducers({ printout, message });
