export const BASE_PATH = "/cs";
export const PRINTOUT = "settings";
export const CONNECTION = "settings/connection";
export const PRINT_PREVIEW = "settings/report-preview";