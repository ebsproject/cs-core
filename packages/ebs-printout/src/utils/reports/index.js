import { clientDownload as client } from "utils/axios";
import { showMessage } from "store/modules/message";


export const importReports = async (file) => {
  try {
    let formData = new FormData();
    formData.append("file", file)
    const { data, status } = await client.post('/api/Report/UploadReportsZip', formData)
  } catch (error) {
    console.log(error);
  }
}
export const exportReports = async (selectedTemplates) => {

  const templates = selectedTemplates.map(item => { 
                                                   return {
                                                           Id: item.id, 
                                                           Name: item.name,
                                                           Description: item.description,
                                                           Zpl: item.zpl,
                                                           Tenant: item.tenants.map(item => item.id).join(","),
                                                           ProgramIds : item.programs.map(item => item.id).join(","),
                                                           ProductIds : item.products.map(item => item.id).join(","),
                                                           ReportName : item.name,
                                                           ReportPath :`${item.name}.repx`                                                   
                                                          } });


  try {
    const { data, status } = await client.post(
      `/api/Report/DownloadReportsZip`, templates
    );
    if (status === 200) {
      const blob = new Blob([data], { type: 'multipart/form-data' });

      if (navigator.msSaveBlob) {
        // IE 10+
        navigator.msSaveBlob(blob, "Download_PrintOut_Reports.zip");
      } else {
        var link = document.createElement("a");
        if (link.download !== undefined) {
          var url = URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", "Download_PrintOut_Reports.zip");
          link.style.visibility = "hidden";
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      }
    } else {
      throw new Error(`Server Error`);
    }
  } catch (error) {
    // console.log(error)
  }
}
