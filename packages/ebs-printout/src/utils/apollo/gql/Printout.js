import { gql } from "@apollo/client";
// TODO: Queries
/**
 * @query Find Printout Template List
 */
export const FIND_PRINTOUT_TEMPLATE_LIST = gql`
  query FIND_PRINTOUT_TEMPLATE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findPrintoutTemplateList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        label
        tenants{
          id
          name
        }
      }
    }
  }
`;
/**
 * @query Find Programs List
 */
export const FIND_PROGRAM_LIST = gql`
  query FIND_PROGRAM_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findProgramList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
      }
    }
  }
`;
/**
 * @query Find Tenant
 */
export const FIND_TENANT = gql`
  query FIND_TENANT($id: ID!) {
    findTenant(id: $id) {
      products {
        id
        name
      }
    }
  }
`;

/**
 * @query Find Product List
 */
export const FIND_PRODUCT_LIST = gql`
  query findProductList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findProductList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
      }
    }
  }
`;
/**
 * TODO: Mutations Create
 */
/**
 * @query Create Printout Template
 */
export const CREATE_PRINTOUT_TEMPLATE = gql`
  mutation CREATE_PRINTOUT_TEMPLATE($printoutTemplate: PrintoutTemplateInput!) {
    createPrintoutTemplate(printoutTemplate: $printoutTemplate) {
      id
    }
  }
`;
/**
 * @query Add Template to Programs
 */
export const ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS = gql`
  mutation ADD_PRINTOUT_TEMPLATE_TO_PROGRAMS(
    $printoutTemplateId: Int!
    $programIds: [Int!]!
  ) {
    addPrintoutTemplateToPrograms(
      printoutTemplateId: $printoutTemplateId
      programIds: $programIds
    ) {
      id
    }
  }
`;
/**
 * @query Add Template to Products
 */
export const ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS = gql`
  mutation ADD_PRINTOUT_TEMPLATE_TO_PRODUCTS(
    $printoutTemplateId: Int!
    $productIds: [Int!]!
  ) {
    addPrintoutTemplateToProducts(
      printoutTemplateId: $printoutTemplateId
      productIds: $productIds
    ) {
      id
    }
  }
`;
/**
 * TODO: Mutations Update
 */
/**
 * @query Modify Printout Template
 */
export const MODIFY_PRINTOUT_TEMPLATE = gql`
  mutation MODIFY_PRINTOUT_TEMPLATE($printoutTemplate: PrintoutTemplateInput!) {
    modifyPrintoutTemplate(printoutTemplate: $printoutTemplate) {
      id
    }
  }
`;
/**
 * TODO: Mutations Delete
 */
/**
 * @query Delete Printout Template from Programs
 */
export const REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS = gql`
  mutation REMOVE_PRINTOUT_TEMPLATE_FROM_PROGRAMS(
    $printoutTemplateId: Int!
    $programIds: [Int!]!
  ) {
    removePrintoutTemplateFromPrograms(
      printoutTemplateId: $printoutTemplateId
      programIds: $programIds
    ) {
      id
    }
  }
`;
/**
 * @query Delete Printout Template from Products
 */
export const REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS = gql`
  mutation REMOVE_PRINTOUT_TEMPLATE_FROM_PRODUCTS(
    $printoutTemplateId: Int!
    $productIds: [Int!]!
  ) {
    removePrintoutTemplateFromProducts(
      printoutTemplateId: $printoutTemplateId
      productIds: $productIds
    ) {
      id
    }
  }
`;
/**
 * @query Delete Printout Template
 */
export const DELETE_PRINTOUT_TEMPLATE = gql`
  mutation DELETE_PRINTOUT_TEMPLATE($printoutTemplateId: Int!) {
    deletePrintoutTemplate(printoutTemplateId: $printoutTemplateId)
  }
`;
export const buildQuery = (entity, content) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: [SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            ${content}
        }
    }`;
};
export const buildQuerySM = (entity, content) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: SortInput, $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            ${content}
        }
    }`;
};