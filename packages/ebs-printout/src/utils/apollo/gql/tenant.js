import { gql } from "@apollo/client";
// * FIND DOMAINS LIST
export const FIND_DOMAIN_LIST = gql`
  query findDomainList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findDomainList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        domaininstances{
          sgContext
        }
      }
    }
  }
`;