import axios from "axios";
import { getTokenId, getContext, getCoreSystemContext } from "@ebs/layout";
const { printoutUri } = getCoreSystemContext();
export const client = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: printoutUri,
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

client.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});
export const clientDownload = axios.create({
  baseURL: printoutUri,
  responseType: 'arraybuffer',
  headers: {
    Accept: "application/zip",
    "Access-Control-Allow-Origin": "*",
  },
})

clientDownload.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});
