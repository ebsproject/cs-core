import { useCallback, useRef } from "react";
/**
 **Hook to handle Async functions
 * @param fn: Function to execute
 * @param delay: Milliseconds number
 */
export function useBouncer(fn, delay) {
  const ref = useRef({});
  ref.current.fn = fn;
  const bouncer = useCallback(
    (...args) => {
      // * Clear the old Timeout if one exist
      if (ref.current.timeout) {
        clearTimeout(ref.current.timeout);
      }
      ref.current.timeout = setTimeout(() => {
        ref.current.fn(...args);
        ref.current.timeout = undefined;
      }, delay);
    },
    [delay]
  );
  return bouncer;
}
