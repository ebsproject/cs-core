import React, { useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {EbsTabsLayout} from "@ebs/styleguide";
import Reports from "components/organism/Reports";
import SystemReports from "components/organism/SystemReports";
import Message from "components/atom/Message";
import {ProductDescriptionTitle} from "@ebs/layout"
import { useLocation } from "react-router-dom";
import Connections from "components/molecule/Connections";
import { getDomainList } from "store/modules/Printout";
import { useDispatch } from "react-redux";
/**
  @param { }: page props,
*/
const SettingsView = ()=> {
  /**
  @prop data-testid: Id to use inside settings.test.js file.
 */
const dispatch = useDispatch();
useEffect(()=>{
getDomainList(dispatch)
},[])
  const location = useLocation();
  return (
    <div data-testid={"SettingsTestId"}>
      <ProductDescriptionTitle />
      <Message />
      <EbsTabsLayout 
              index={location?.state?.index || 0}//TODO:: review this
              tabs={[
                {
                  label: <FormattedMessage id="none" defaultMessage="Templates" />,
                  component: <Reports />,
                },
                {
                  label: <FormattedMessage id="none" defaultMessage="Connections" />,
                  component: <Connections />,
                },
                {
                  label:<>{"System"} <br/>{"Templates"}</>,
                  component: <SystemReports />,
                },
              ]}
      />
    </div>
  );
}
// Type and required properties
SettingsView.propTypes = {};

export default  SettingsView