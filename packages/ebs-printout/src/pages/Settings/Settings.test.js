import Settings from './Settings';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Settings is in the DOM', () => {
  render(<Settings></Settings>)
  expect(screen.getByTestId('SettingsTestId')).toBeInTheDocument();
})
