import React, { useState } from "react";
import { Core, Icons } from "@ebs/styleguide";
const { Dialog, Toolbar, IconButton, DialogContent, Typography } = Core;
const { Close } = Icons;
import Designer from "components/atom/Designer/reportdesigner";
import { useHistory, useLocation } from "@ebs/layout";

const ReportPreview = () => {
  const [open, setOpen] = useState(true);
  const location = useLocation();
  const history = useHistory();
  const paramValue =
    location?.state?.rowDataId === undefined ? "" : location?.state.rowDataId;
  const paramName =
    location?.state?.parameters[0]?.name === undefined
      ? ""
      : location?.state?.parameters[0]?.name;

  const closeReportPreview = () => {
    setOpen(false);
    history.push(location.state.currentLocation);
  };
  return (
    <Dialog fullScreen open={open} onClose={closeReportPreview}>
      <div>
        <Toolbar className="bg-ebs-brand-default text-white font-ebs">
          <IconButton
            edge="start"
            color="inherit"
            onClick={closeReportPreview}
            aria-label="close"
          >
            <Close />
          </IconButton>
          <Typography variant={"title"}>
            {`Report name: ${location.state.reportName}`}
          </Typography>
        </Toolbar>
      </div>
      <DialogContent className="py-24">
        <Designer
          paramValues={{ value: paramValue, name: paramName }}
          reportName={location.state.reportName}
          mode={"preview"}
        />
      </DialogContent>
    </Dialog>
  );
};
export default ReportPreview;
