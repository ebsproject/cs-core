import React from "react";
import ReactDOM from "react-dom/client";
import singleSpaReact from "single-spa-react";
import _App from "./root.component";
import ReportDesigner from "./components/molecule/ReportDesigner"
import PODesigner from "components/atom/Designer";


const lifecycles = singleSpaReact({
  renderType: "createRoot",
  React,
  ReactDOM,
  rootComponent: _App,
  errorBoundary(err, info, props) {
    // Customize the root error boundary for your microfrontend here.
    return null;
  },
});
export const App = _App;
export const { bootstrap, mount, unmount } = lifecycles;
export const  Designer = ReportDesigner 
export const POReportDesigner = PODesigner
