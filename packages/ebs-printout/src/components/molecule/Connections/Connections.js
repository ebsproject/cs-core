import React, { memo, useState, useEffect } from "react";
import PropTypes from "prop-types";
import { client } from "utils/axios";
import { useDispatch } from "react-redux";
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
import {Core, Icons} from "@ebs/styleguide";
const { Typography, Tooltip, Box, Button }= Core;
const { PostAdd} = Icons;
import ConnectionRowAction from "components/atom/ConnectionRowAction";
import AddConnection from "components/atom/AddConnection";
import { sortAndFilterFunction } from "./functions";
import { RBAC } from "@ebs/layout";
//MAIN FUNCTION
/**
 * @param {}: component properties
 * @param ref: reference made by React.forward
 */
const ConnectionsMolecule = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const [width, setWidth] = useState(window.innerWidth);
    useEffect(()=>{
      const handleResize = () => {
        setWidth(window.innerWidth || 1300)
      };
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    },[window.innerWidth])
  const [connection, setConnection] = useState(false);
  const handleCloseDialogConn = () => {
    setConnection(false);
  };
  const handleOpenConn = () => {
    setConnection(true);
  };
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Connection name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "name",
      csvHeader: "Connection Name",
      width: 320,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Connection URL" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "uri",
      csvHeader: "Connection URL",
      width: 320,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Created by" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "createdBy",
      csvHeader: "Created by",
      width: 200,
      filter: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Created On" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "createdOn",
      csvHeader: "Created On",
      width: 150,
      filter: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Updated By" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "updateBy",
      csvHeader: "Updated By",
      width: 200,
      filter: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Updated On" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "updateOn",
      csvHeader: "Updated On",
      width: 150,
      filter: false,
    }
  ];

  /**
   * @param {Object} page
   * @param {Object[]} sort
   * @param {Object[]} filters
   * @returns Promise
   */
  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        client.get(`/api/Report/GetAllConnections`).then(({ data, errors }) => {          
          if (errors) {
            reject(errors);
          } else {
            const formattedData = sortAndFilterFunction(data, sort, filters);
            resolve({
              pages: 1,
              elements: formattedData.length,
              data: formattedData,
            });
          }
        });
      } catch (e) {
        reject(e);
      } finally {
      }
    });
  };
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <div>
        {width > 1300 && (<Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
        <AddConnection
          type={"NEW"}
          refresh={refresh}
          open={connection}
          handleClose={handleCloseDialogConn}
        />
        <Box className="flex flex-row flex-nowrap justify-center justify-items-center items-center  gap-2 px-2">
          <RBAC allowedAction={"Create_Data_Source"}>
            <Tooltip
              arrow
              placement="top"
              className="text-lg"
              title={
                <Typography className="font-ebs text-lg">
                  <FormattedMessage
                    id={"none"}
                    defaultMessage={"Add New Data Source Connection"}
                  />
                </Typography>
              }
            >
              <Button
                startIcon={<PostAdd />}
                onClick={handleOpenConn}
                className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              >
                <Typography className="font-ebs">
                  <FormattedMessage
                    id={"none"}
                    defaultMessage={"New Connection"}
                  />
                </Typography>
              </Button>
            </Tooltip>
          </RBAC>
        </Box>
        </Box>)}
        {width < 1300 && (
        <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center" }} className={"gap-2" }>
          <AddConnection
          type={"NEW"}
          refresh={refresh}
          open={connection}
          handleClose={handleCloseDialogConn}
        />
        <Box>
          <RBAC allowedAction={"Create_Data_Source"}>
            <Tooltip
              arrow
              placement="top"
              className="text-lg"
              title={
                <Typography className="font-ebs text-lg">
                  <FormattedMessage
                    id={"none"}
                    defaultMessage={"Add New Data Source Connection"}
                  />
                </Typography>
              }
            >
              <Button
                startIcon={<PostAdd />}
                onClick={handleOpenConn}
                className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              >
              </Button>
            </Tooltip>
          </RBAC>
        </Box>
        </Box>)}        
      </div>
    );
  };  
  return (
    /**
     * @prop data-testid: Id to use inside people.test.js file.
     */
    <div data-testid={"ConnectionsTestId"}>
      <EbsGrid
        id="Connections"
        data-testid={"ConnectionsTestId"}
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={ConnectionRowAction}
        csvfilename="connectionList"
        fetch={fetch}
        height="85vh"
        select="multi"
      />      
    </div>
  );
});
// Type and required properties
ConnectionsMolecule.propTypes = {};
// Default properties
ConnectionsMolecule.defaultProps = {};

export default memo(ConnectionsMolecule);
