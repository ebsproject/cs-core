import Connections from "./Connections";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("AddConnection is in the DOM", () => {
  render(<Connections></Connections>);
  expect(screen.getByTestId("AddConnectionTestId")).toBeInTheDocument();
});
