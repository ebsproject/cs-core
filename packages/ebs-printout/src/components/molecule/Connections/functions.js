export function sortAndFilterFunction(data, sortArray, filtersArray){
    let _data = [...data];
    if (sortArray.length > 0){
        sortArray.forEach(sort => {
            if (sort.mod === 'ASC'){
                _data = _data.sort((a,b) => a[sort.col].localeCompare(b[sort.col], undefined, {caseFirst: 'upper'}));
            } else{
                _data = _data.sort((a,b) => b[sort.col].localeCompare(a[sort.col], undefined, {caseFirst: 'upper'}));
            }
        });
    }
    if (filtersArray.length > 0){
        filtersArray.forEach(filter => {
            _data = _data.filter(item => item[filter.col].toLowerCase().includes(filter.val.toLowerCase()));
        })
    }
    return _data;
}