import React from 'react'
import { Core } from "@ebs/styleguide";
const { Grid } = Core;

import InputReportParam from 'components/atom/InputReportParam'


export default function ListParameters({ parameters, control, setValue, errors, ...props }) {

  return (
    <>

      {parameters?.map((param, index) =>
        <Grid item xs={12} key={`param${index}`} >
          <InputReportParam control={control} setValue={setValue} parameter={param} errors={errors} />
        </Grid>
      )}

    </>
  )

}
