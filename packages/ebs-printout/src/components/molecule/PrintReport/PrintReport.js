import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { client } from "utils/axios";
import { useForm, Controller } from "react-hook-form";
import { Core } from "@ebs/styleguide";
import { showMessage } from "store/modules/message";
import { useDispatch } from "react-redux";
const { Grid, Button, Divider, Dialog, DialogTitle, DialogActions, DialogContent, DialogContentText, FormControl, Typography, TextField,  CircularProgress } = Core;
import { FormattedMessage } from "react-intl";
//components
import PrintTest from "components/atom/PrintTest"
import ListParameters from "./ListParameters";



export default function PrintReport(props) {
  const dispatch = useDispatch();
  const [parametersList, setParameterList] = useState([]);
  const [loading, setLoading] = useState(false)
  const { control, handleSubmit, setValue, formState: { errors } } = useForm();
  const { handleClose, open, report } = props;



  const onSubmit = formData => {

    //invoke print call
    setLoading(true);
    client.get(`/api/report/print?name=${report.name.trim()}&printerName=${formData.printerName}&parameter[pageRange]=${formData.pages}&parameter[pageInterval]=${formData.interval}&${parametersList.map((param, idx) => `parameter[${param.name}]=${formData[param.name]}`).join('&')}`)
     .then(res => {
        setLoading(false);
        dispatch(showMessage({
          message: "The print order was sent successfully",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        }))
      }).catch(err => {
        setLoading(false);

        dispatch(showMessage({
          message: err.response.data.value,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        }))


      });

  }

  //get all parameters from report
  const fetchParameters = () => {
    client.get(`/api/Report/Parameters?nameReport=${report.name.trim()}`)
      .then((res) => {
        setParameterList(JSON.parse(JSON.stringify(res.data)))
      })
      .catch((err) => {

      })
  }

  useEffect(() => {
    fetchParameters();
  }, []);


  const PageComponent = () => {

    return (
      <>
        <FormControl fullWidth >
          <Controller
            name={"pages"}
            control={control}
            
            defaultValue="0"
            render={({ field }) => <TextField
              {...field}
              error={Boolean(errors["pages"])}
              label={
                <>
                  <FormattedMessage
                    id={"pages"}
                    defaultMessage={"Pages Range"}
                  />
                </>
              }
              helperText={["Default print all pages = 0; For print in ranges e.g. 1-20."]}
            />}
          />
        </FormControl>
        <FormControl fullWidth>
          <Controller
            name={"interval"}
            control={control}
            defaultValue="0"
            render={({ field }) => 
            <TextField
            type="number"
              {...field}
              error={Boolean(errors["interval"])}
              label={
                <>
                  <FormattedMessage
                    id={"interval"}
                    defaultMessage={"Pages Interval"}
                  />
                  {` *`}
                </>
              }
              helperText={["Page/item intervals"]}
            />}
          />
        </FormControl>
      </>
    )
  }

  return (
    <Dialog onClose={handleClose} aria-labelledby="dialog01" open={open} >
      <DialogTitle id="dialog01">{`Print Report ${report.name} Without Preview`}</DialogTitle>
      <Divider light />
      <form component="form" onSubmit={handleSubmit(onSubmit)}>
        <DialogContent>
          <DialogContentText>Send a printing order directly to the printer</DialogContentText>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <PrintTest control={control} setValue={setValue} errors={errors} />
            </Grid>
            <ListParameters control={control} setValue={setValue} parameters={parametersList} errors={errors} />

            <Grid item xs={12}>
              <PageComponent />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          { loading ? (<CircularProgress />):
          (<Button type="submit" color="primary" >
            Print
          </Button>)}
        </DialogActions>
      </form >
    </Dialog >
  )
}
