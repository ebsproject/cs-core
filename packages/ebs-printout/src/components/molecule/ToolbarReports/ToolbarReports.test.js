import ToolbarReports from './ToolbarReports';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ToolbarReports is in the DOM', () => {
  render(<ToolbarReports></ToolbarReports>)
  expect(screen.getByTestId('ToolbarReportsTestId')).toBeInTheDocument();
})
