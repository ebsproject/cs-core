import React, { useState, useContext,useEffect } from "react";
import PropTypes from "prop-types";
import Report from "components/atom/Report";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const {
  Box,
  Button,
  Menu,
  MenuItem,
  Tooltip,
  Typography,
} = Core;
const { PostAdd, GetApp, Publish} = Icons;
import AddConnection from "components/atom/AddConnection";
import { getContext, userContext, RBAC} from "@ebs/layout";
import ImportButton from "../ImportButton";


//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ToolbarReportsMolecule = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    const [anchorEl, setAnchorEl] = useState(null);
    const [empty, setEmpty] = useState(false);
    const [copy, setCopy] = useState(false);
    const [connection, setConnection] = useState(false);
    const {userProfile} = useContext(userContext);
    const [width, setWidth] = useState(window.innerWidth);
    useEffect(()=>{
      const handleResize = () => {
        setWidth(window.innerWidth || 1300)
      };
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    },[window.innerWidth])
    const handleClick = (e) => {
      setAnchorEl(e.currentTarget);
    };
    const handleClose = () => {
      setAnchorEl(null);
    };
    const handleOpen = (type) => {
      setAnchorEl(null);
      if (type === "BLANK") {
        setEmpty(true);
      } else {
        setCopy(true);
      }
    };
    const handleCloseDialog = (type) => {
      setAnchorEl(null);
      if (type === "BLANK") {
        setEmpty(false);
      } else {
        setCopy(false);
      }
    };
    const handleCloseDialogConn = () => {
      setAnchorEl(null);
     setConnection(false);
    };

    return (
      /**
       * @prop data-testid: Id to use inside ToolbarReports.test.js file.
       */
      <div
        component="div"
        ref={ref}
        data-testid={"ToolbarReportsTestId"}
      >
      {width > 1300 && (<Box sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
          <RBAC
            allowedAction={"Create"}
          >
            <Tooltip
              arrow
              placement="top"
              title={
                <Typography className="font-ebs text-lg">
                  <FormattedMessage id={"none"} defaultMessage={"Add New Report"} />
                </Typography>
              }
            >
            <Button
              startIcon={<PostAdd />}
              //className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              onClick={handleClick}
            >
              <Typography className="font-ebs">
                <FormattedMessage id="none" defaultMessage={"New Report"} />
              </Typography>
            </Button>
          </Tooltip>
        </RBAC>

        <ImportButton  
        selectedRows={selectedRows}
        refresh={refresh}
        />
      </Box>)}
      {width < 1300 && (
        <Box sx={{ display: "flex", flexDirection: "column", alignItems: "center" }} className={"gap-4 ml-5" }>
        <RBAC
        allowedAction={"Create"}        
        >
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Add New Report"} />
              </Typography>
            }
          >
            <Button
               startIcon={<PostAdd />}
             // className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              onClick={handleClick}
            >
            </Button>
          </Tooltip>
        </RBAC>

        <ImportButton  
        selectedRows={selectedRows}
        refresh={refresh}
        />
      </Box>)}

        <Menu
          elevation={8}
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          transformOrigin={{ vertical: "top", horizontal: "center" }}
          id={"new-report-menu"}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Create a new report"} />
              </Typography>
            }
          >


            <MenuItem onClick={() => handleOpen("BLANK")}>
              <FormattedMessage id={"none"} defaultMessage={"Empty"} />
            </MenuItem>
          </Tooltip>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Create a new report based on existing one"} />
              </Typography>
            }
          >
            <MenuItem onClick={() => handleOpen("copy")}>
              <FormattedMessage
                id={"none"}
                defaultMessage={"Report based on..."}
              />
            </MenuItem>
          </Tooltip>
        </Menu>
        <Report
          type={"BLANK"}
          refresh={refresh}
          open={empty}
          handleClose={handleCloseDialog}
        />
        <Report
          type={"COPY"}
          refresh={refresh}
          open={copy}
          handleClose={handleCloseDialog}
        />
          <AddConnection
          type={"NEW"}
          refresh={refresh}
          open={connection}
          handleClose={handleCloseDialogConn}
        />

      </div>
    );
  }
);
// Type and required properties
ToolbarReportsMolecule.propTypes = {};
// Default properties
ToolbarReportsMolecule.defaultProps = {};

export default ToolbarReportsMolecule;
