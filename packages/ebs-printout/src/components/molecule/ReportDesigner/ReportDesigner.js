import React, { useState, memo } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons } from "@ebs/styleguide";
const {
  AppBar,
  Box,
  Dialog,
  DialogContent,
  Button,
  IconButton,
  Slide,
  Toolbar,
  Tooltip,
  Typography,
} = Core;
import Designer from "components/atom/Designer";
 const { Close, SettingsApplications,PrintRounded }= Icons;
import { IntlProvider } from "react-intl";
/**
 * * Transition Effect Component
 */
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ReportDesignerMolecule = React.forwardRef(({ name,reportName,mode , classes}, ref) => {
  const report_name = mode === "preview" ?  reportName : name;
  const message = mode === "preview" ? "Print Report" : "Designer Mode"
  const classType = !classes ? "bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900" : "text-green-600"
  const [open, setOpen] = useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const [locale, setLocale] = useState("en");
  const [messages, setMessages] = useState(null);
  return (
    /**
     * @prop data-testid: Id to use inside ReportDesigner.test.js file.
     */
     <IntlProvider locale={locale} messages={messages} defaultLocale="en">
    <Box component="div" ref={ref} data-testid={"ReportDesignerTestId"} >

      <Tooltip 
      className="bg-ebs-brand-default "
      arrow
      placement="bottom"
        title={
          <Typography className="font-ebs text-lg">
            <FormattedMessage id={"none"} defaultMessage={message} />
          </Typography>
        }
      >

        <IconButton
          aria-label="permission"
          className={classType}
          onClick={handleOpen}
          size="small"
          color="primary"
        >
         {mode === "preview" ? <PrintRounded /> : <SettingsApplications />}
        </IconButton>
        

      </Tooltip>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <div>
        <Toolbar 
          sx={{bgcolor: 'rgba(0, 150, 136, 1)'}}
          className=" text-white font-ebs" >
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <Close />
            </IconButton>
            <Typography>
              <FormattedMessage id={"none"} defaultMessage={message} />
            </Typography>
          </Toolbar>
        </div>
        <DialogContent className="py-24">
          <Designer reportName={report_name} mode = {mode} />
        </DialogContent>
      </Dialog>
    </Box>
    </IntlProvider>
  );
});
// Type and required properties
ReportDesignerMolecule.propTypes = {
  name: PropTypes.string,
};
// Default properties
ReportDesignerMolecule.defaultProps = {};

export default memo(ReportDesignerMolecule);
