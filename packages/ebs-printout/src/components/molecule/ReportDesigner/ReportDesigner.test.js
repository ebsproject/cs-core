import ReportDesigner from './ReportDesigner';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ReportDesigner is in the DOM', () => {
  render(<ReportDesigner></ReportDesigner>)
  expect(screen.getByTestId('ReportDesignerTestId')).toBeInTheDocument();
})
