import React, { useState } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Box, IconButton, Tooltip, Typography } = Core;
import ReportDesigner from "components/molecule/ReportDesigner";
import Report from "components/atom/Report";
import PrintReport from "components/molecule/PrintReport";
const { Edit, PageviewRounded, DocumentScanner } = Icons;
import { userContext, getContext, RBAC } from "@ebs/layout"
import DeleteReport from "components/atom/DeleteReport";
import { FormattedMessage } from "react-intl";
import { useContext } from "react";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const RowActionsReportsMolecule = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const { name } = rowData;
    const [open, setOpen] = useState(false);
    const [openPrint, setOpenPrint] = useState(false);
    const handleClick = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const handleClickPrint = () => setOpenPrint(true);
    const handleClosePrint = () => setOpenPrint(false);
    const { userProfile } = useContext(userContext)

    return (
      /**
       * @prop data-testid: Id to use inside RowActionsReports.test.js file.
       */
      <div className="-mr-100" >
        <Box
          component="div"
          ref={ref}
          data-testid={"RowActionsReportsTestId"}
          className="-ml-4 flex flex-auto"
        >
          <RBAC
            allowedAction={"Delete"}
          >
            <DeleteReport selectedRows={rowData} refresh={refresh} />
          </RBAC>

          <RBAC
            allowedAction={"Modify"}
          >
            <Tooltip
              arrow
              placement="bottom"
              title={
                <Typography className="font-ebs text-lg">
                  <FormattedMessage id={"none"} defaultMessage={"Edit Report"} />
                </Typography>
              }
            >
              <IconButton
                onClick={handleClick}
                size="small"
                color="primary"
              >
                <Edit />
              </IconButton>
            </Tooltip>

          </RBAC>

          <RBAC
            allowedAction={"Design"}
          >
            <ReportDesigner {...rowData} classes={true} />
          </RBAC>

          <RBAC
            allowedAction={"Print"}
          >
            <ReportDesigner reportName={name} mode={"preview"} classes={true} />
          </RBAC>

          <Tooltip
            arrow
            placement="bottom"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"printDirect"} defaultMessage={"Print Report Without Preview"} />
              </Typography>
            }
          >
            <IconButton
              onClick={handleClickPrint}
              size="small"
              color="primary"
            >
              <DocumentScanner />
            </IconButton>
          </Tooltip>

        </Box>


        <Report
          {...rowData}
          open={open}
          type="PUT"
          refresh={refresh}
          handleClose={handleClose}
        />
        {openPrint === true && <PrintReport report={rowData} open={openPrint} handleClick={handleClickPrint} handleClose={handleClosePrint} />}

      </div>
    );
  }
);

// Type and required properties
RowActionsReportsMolecule.propTypes = {};
// Default properties
RowActionsReportsMolecule.defaultProps = {};

export default RowActionsReportsMolecule;
