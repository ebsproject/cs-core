import ReportsDetail from './ReportsDetail';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ReportsDetail is in the DOM', () => {
  render(<ReportsDetail></ReportsDetail>)
  expect(screen.getByTestId('ReportsDetailTestId')).toBeInTheDocument();
})
