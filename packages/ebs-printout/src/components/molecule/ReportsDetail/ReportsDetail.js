import React, { memo,useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { EbsTabsLayout,Core,Icons } from "@ebs/styleguide";
const {PrintRounded} = Icons;
const { Box,IconButton,Dialog,DialogActions,DialogContent,DialogTitle,TextField,Typography,Button } = Core;
import Products from "components/atom/Products";
import Programs from "components/atom/Programs";
import Tenants from "components/atom/Tenants";
import { printZPLCode } from "store/modules/Printout";
import { useDispatch } from "react-redux";
//MAIN FUNCTION
/**
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ReportsDetailMolecule = React.forwardRef(
  ({ products, programs, zplCode, tenants }, ref) => {
    const [open,setOpen]= useState(false);
    const [printerIP,setPrinterIP]= useState("");
    const dispatch = useDispatch();
    const handleClose =()=>{
      setOpen(false)
    }
    const setPrinterIPOnChange = (e) => {
      setPrinterIP(e.target.value);
    };
    var labelaryURL =
      "http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/" + zplCode;
      const openDialog =()=>{
        setOpen(true)
      }
      const printZPL = () =>{
        let name = "Report"
        if(printerIP!=="" || zplCode !==""){
           printZPLCode(name,printerIP,zplCode)(dispatch)
        }
        handleClose();
      }
    return (
      /** 
     @prop data-testid: Id to use inside ReportsDetail.test.js file.
     */
      <Box
        component="div"
        ref={ref}
        data-testid={"ReportsDetailTestId"}
        className="flex flex-row"
      >
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          <Typography className="font-ebs">
            <FormattedMessage
              id={"none"}
              defaultMessage={"Send zpl code to print?"}
            />
          </Typography>
        </DialogTitle>
        <DialogContent>
        <TextField
                label="Printer's IP"
                required
                name='printerIP'
                onChange={setPrinterIPOnChange}
                className='w-3/4'
              ></TextField>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id={"none"} defaultMessage={"No"} />
          </Button>
          <Button onClick={printZPL}
          disabled={printerIP.length > 10 ? false : true}
          autoFocus>
            <FormattedMessage id={"none"} defaultMessage={"Yes"} />
          </Button>
        </DialogActions>
      </Dialog>

        <EbsTabsLayout
          orientation={"horizontal"}
          tabs={[
            {
              label: <FormattedMessage id="none" defaultMessage={"Products"} />,
              component: <Products data={products} />,
            },
            {
              label: <FormattedMessage id="none" defaultMessage={"Programs"} />,
              component: <Programs data={programs} />,
            },
            {
              label: <FormattedMessage id="none" defaultMessage={"Tenants"} />,
              component: <Tenants data={tenants} />,
            },
            {
              label: (
                <FormattedMessage id="none" defaultMessage={"ZPL Preview"} />
              ),
              component: (
                <div >
                  <div className="grid justify-end">
                    {/* <Button
                      onClick={openDialog}
                      color="primary"
                    >
                      <PrintRounded />
                    </Button> */}
                  </div>

                <img
                  id="img"
                  src={labelaryURL}
                  className="w-1/5 h-1/6"
                  alt="This report does not contain zpl code"
                />

                </div>

              ),
            },
          ]}
        />
      </Box>
    );
  }
);
// Type and required properties
ReportsDetailMolecule.propTypes = {
  products: PropTypes.array.isRequired,
  programs: PropTypes.array.isRequired,
};
// Default properties
ReportsDetailMolecule.defaultProps = {};

export default memo(ReportsDetailMolecule);
