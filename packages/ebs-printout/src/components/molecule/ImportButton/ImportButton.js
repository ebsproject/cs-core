import React, {useState} from "react";
import { FormattedMessage } from "react-intl";
import { Core, DropzoneDialog, EbsDialog } from "@ebs/styleguide";
const {
    Box,
    Button,
    DialogContent,
    DialogActions,
    Menu,
    MenuItem,
    Tooltip,
    Typography,
  } = Core;
  import { exportReports, importReports } from "utils/reports";
  import {RBAC} from "@ebs/layout";


const ImportButton = React.forwardRef(({selectedRows,refresh},ref)=>{

const [anchorEl, setAnchorEl] = useState(null);
const [openWarning, setOpenWarning] = useState(false);
const [open, setOpen] = useState(false)
const handleCloseWarning = () => {
    setOpenWarning(false);
  };
const handleClick = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleClose = () => {    
    setAnchorEl(null);    
    setOpen(false);
    setOpenWarning(false);   
  };
  const import_Reports = () =>{
    setOpenWarning(true);
    setAnchorEl(null);    
  }
  const export_Reports = () =>{
     exportReports(selectedRows);
     setAnchorEl(null);
}
const onSubmit = (files) =>{
    files.forEach(async file =>{
      await  importReports(file);
      refresh();
    })
   setOpen(false);
   setOpenWarning(false);
}
const submitWarning = () => {
    setOpen(true);
    setAnchorEl(null);
};

return(
    <div>
        <Tooltip
            arrow
            placement="top"
            title={
                <Typography className="font-ebs text-lg">
                    <FormattedMessage id={"none"} defaultMessage={"Import/Export Reports"} />
                </Typography>
            }
        >
            <Button
             
                className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
                onClick={handleClick}
            >
                <Typography className="font-ebs">
                    <FormattedMessage id="none" defaultMessage={"Import/Export"} />
                </Typography>
            </Button>
        </Tooltip>
        <Menu
            elevation={8}
            getContentAnchorEl={null}
            anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
            transformOrigin={{ vertical: "top", horizontal: "center" }}
            id={"new-report-menu"}
            anchorEl={anchorEl}
            open={Boolean(anchorEl)}
            onClose={handleClose}
        >
            <RBAC allowedAction={"Import_Backend"}>
                <Tooltip
                    arrow
                    placement="top"
                    title={
                        <Typography className="font-ebs text-lg">
                            <FormattedMessage id={"none"} defaultMessage={"Import reports to this environment"} />
                        </Typography>
                    }
                >
                    <MenuItem onClick={import_Reports}>
                        <FormattedMessage id={"none"} defaultMessage={"Import Reports"} />
                    </MenuItem>
                </Tooltip>
            </RBAC>
            <RBAC allowedAction={"Export_Backend"}>
                <Tooltip
                    arrow
                    placement="top"
                    title={
                        <Typography className="font-ebs text-lg">
                            <FormattedMessage id={"none"} defaultMessage={"Export reports from this environment"} />
                        </Typography>
                    }
                >
                    <MenuItem 
                    onClick={export_Reports}
                    disabled = {selectedRows.length > 0 ? false : true}
                    >
                        <FormattedMessage
                            id={"none"}
                            defaultMessage={selectedRows.length > 0 ? "Export Reports" : "Select a template first"}
                        />
                    </MenuItem>
                </Tooltip>
            </RBAC>

        </Menu>
          <DropzoneDialog
          acceptedFiles={[".zip"]}
          cancelButtonText={"cancel"}
          submitButtonText={"submit"}
          maxFileSize={5000000}
          open={open}
          onClose={handleClose}
          onSave={onSubmit}
          showPreviews={true}
          showFileNamesInPreview={true}
        /> 
        <EbsDialog
          open={openWarning}
          handleClose={handleCloseWarning}
          maxWidth="sm"
          title={<FormattedMessage id="none" defaultMessage="Import templates" />}
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="If the template exist, they will be overwritten."
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseWarning}
              className="bg-ebs-brand-900 rounded-md text-white p-3 m-2 hover:bg-ebs-gray-default"
            >
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submitWarning}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id="none" defaultMessage="Continue" /> 
            </Button>
          </DialogActions>
        </EbsDialog>
    </div>
    
)


})
export default ImportButton