import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import {Reports} from "components/organism/Reports";
import { EbsTabsLayout,Core } from "@ebs/styleguide";
const { Box } = Core;
import Connections from "components/atom/Connections";

//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const ConnectionOrganism = React.forwardRef(({}, ref) => {

  return (
    /**
     * @prop data-testid: Id to use inside role.test.js file.
     */
    <Box ref={ref} data-testid={"RoleTestId"}>
      <EbsTabsLayout
        orientation={"horizontal"}
        tabs={[
          {
            label: <FormattedMessage id={"none"} defaultMessage="Add New Connection" />,
            component: <Connections />,
          },
        ]}
      />
    </Box>
  );
});
// Type and required properties
ConnectionOrganism.propTypes = {};
// Default properties
ConnectionOrganism.defaultProps = {};

export default ConnectionOrganism;
