import Reports from './Reports';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Reports is in the DOM', () => {
  render(<Reports></Reports>)
  expect(screen.getByTestId('ReportsTestId')).toBeInTheDocument();
})
