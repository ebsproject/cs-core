import React, { memo, useState } from "react";
import PropTypes from "prop-types";
import Toolbar from "components/molecule/ToolbarReports";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import ReportsDetail from "components/molecule/ReportsDetail";
import RowActions from "components/molecule/RowActionsReports";
import { getCoreSystemContext } from "@ebs/layout";
import Cookies from "js-cookie";
const { graphqlUri } = getCoreSystemContext();
//MAIN FUNCTION
/**
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Reports = React.forwardRef((props, ref) => {
  const cookie = Cookies.get("filters");
  let filterItem;
  if (cookie) filterItem = JSON.parse(cookie);


  const columns = [
    {
      Header: "id",
      accessor: "id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      csvHeader: "Name",
      accessor: "label",
      filter: true,
      width: 400,
      Cell: ({row}) => {return (
        <>{ row.values["label"] ?
            row.values["label"] : 
            row.values["name"]}
        </>
      )}
    },
    {
      Header:"Name Internal",
      accessor: "name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Description" />
        </Typography>
      ),
      csvHeader: "Description",
      accessor: "description",
      filter: true,
      width: 350,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="ZPL" />
        </Typography>
      ),
      csvHeader: "ZPL",
      accessor: "zpl",
      hidden: true,
      filter: true,
      width: 350,
    },
    {
      Header: "tenants.id",
      accessor: "tenants.id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Tenant" />
        </Typography>
      ),
      accessor: "tenants.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "programs.id",
      accessor: "programs.id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "programs.name",
      accessor: "programs.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "products.id",
      accessor: "products.id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "products.name",
      accessor: "products.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "products.domain.name",
      accessor: "products.domain.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "Default Format",
      accessor: "defaultFormat",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    }
  ];

  const ReportDetail = ({ rowData }) => {
    return (
      <ReportsDetail
        products={rowData.products}
        programs={rowData.programs}
        zplCode={rowData.zpl}
        tenants={rowData.tenants}
      />
    );
  };

  /** 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <EbsGrid
      id="Reports"
      toolbar={true}
      toolbaractions={Toolbar}
      columns={columns}
      entity="PrintoutTemplate"
      uri={graphqlUri}
      detailcomponent={ReportDetail}
      rowactions={RowActions}
      csvfilename="reportList"
      callstandard="graphql"
      height="85vh"
      raWidth={150}
      select="multi"
      defaultFilters={[]}
      auditColumns
    />
  );
});

// Type and required properties
Reports.propTypes = {};


export default memo(Reports);
