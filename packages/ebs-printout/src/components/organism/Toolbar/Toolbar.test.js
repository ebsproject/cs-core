import Toolbar from './Toolbar';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Toolbar is in the DOM', () => {
  render(<Toolbar></Toolbar>)
  expect(screen.getByTestId('ToolbarTestId')).toBeInTheDocument();
})
