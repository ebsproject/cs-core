import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import { FormattedMessage } from "react-intl";

const formattedDate = (value) => {
return new Date(value).toLocaleString()

  };

export const columns = [
    {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Name" />
      </Typography>
    ),
    csvHeader: "Name",
    accessor: "documentName",
    width: 600,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Created date" />
      </Typography>
    ),
    Cell: ({ value }) => {
        return <Typography variant="body1">{formattedDate(value)}</Typography>;
      },
    csvHeader: "Created Date",
    accessor: "createdDate",
    width: 600,
  },
]