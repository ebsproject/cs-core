import React , {useState, useEffect} from "react";
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
import { columns } from "./columns";
const { Typography } = Core;
import { RowActions } from "./RowActions";
import { client } from "utils/axios";

const SystemReports = React.forwardRef(()=>{

    const fetch = async ({ page, sort, filters }) => {
        return new Promise((resolve, reject) => {
          try {
            client.get(`/api/FileManager/GetSystemTemplateList`).then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                resolve({
                  pages: 1,
                  elements: data.result.data.filter(item => item.documentName !== "sys_connection").length ,
                  data: data.result.data.filter(item => item.documentName !== "sys_connection"),
                });
              }
            });
          } catch (e) {
            reject(e);
          } finally {
          }
        });
      };
    return (
       <EbsGrid
       id="SystemReports"
      columns={columns}
      rowactions={RowActions}
      csvfilename="system_templates"
      height="85vh"
      raWidth={50}
      fetch={fetch}
       >

       </EbsGrid>
    )
});
export default SystemReports;