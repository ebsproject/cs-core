import React, { useState, useEffect } from "react";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Box, IconButton, Tooltip, Typography } = Core;
import ReportDesigner from "components/molecule/ReportDesigner";
const { Edit, PageviewRounded } = Icons;
import { RBAC } from "@ebs/layout"
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
export const RowActions = React.forwardRef(
  ({ rowData }, ref) => {
    const {documentName } = rowData;

    return (
      /**
       * @prop data-testid: Id to use inside RowActionsReports.test.js file.
       */
      <div className="-mr-100" >
        <Box
          component="div"
          ref={ref}
          data-testid={"RowActionsReportsTestId"}
          className="-ml-4 flex flex-auto"
        >
          <RBAC allowedAction={"Print"}>
            <ReportDesigner reportName={documentName} mode={"preview"} classes={true} />
          </RBAC>
          {/* design-system dont exist in product-funtions, It was done to hide the option  */}
          <RBAC allowedAction={"Design-system"} >
            <ReportDesigner name={documentName} classes={true} />
          </RBAC>


       </Box>

      </div>
    );
  }
);

// Type and required properties
RowActions.propTypes = {};
// Default properties
RowActions.defaultProps = {};
