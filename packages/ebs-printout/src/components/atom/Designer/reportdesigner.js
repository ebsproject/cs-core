import React from "react";
import ko from "knockout";
import { ajaxSetup } from "@devexpress/analytics-core/analytics-utils";
import { AsyncExportApproach } from "devexpress-reporting/scopes/reporting-viewer-settings";
import { ActionId } from "devexpress-reporting/dx-reportdesigner";
// eslint-disable-next-line no-duplicate-imports
import "devexpress-reporting/dx-reportdesigner";
import 'devexpress-reporting/dx-webdocumentviewer';
import "./index.css";
import { getCoreSystemContext, getTokenId } from "@ebs/layout";
import Progress from 'components/atom/Progress';
const { printoutUri } = getCoreSystemContext();
//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
export default class Designer extends React.Component {
  constructor(props) {
    super(props);
    this.paramValues = this.props.paramValues
    let name = "";
    let value = "";
    let isParamsPresent = false;
    if (this.paramValues) {
      value = this.paramValues.value || 0;
      name = this.paramValues.name || "";
      isParamsPresent = true
    }
    this.reportUrl = ko.observable(isParamsPresent ? `${this.props.reportName}?${name}?${value}` : this.props.reportName);
    this.mode = this.props.mode;
    this.url = printoutUri;
    if(this.mode === "preview")
    {
      this.requestOptions = {
        host: this.url,
        invokeAction: "/DXXRDV"
      };
    }
    else
    {
      this.requestOptions = {
        host: this.url,
        getDesignerModelAction: "/DXXRD/GetReportDesignerModel",
      };

    }


    this.callbacks = {
      CustomizeMenuActions: function (s, e) {
        var newMenu = e.GetById(ActionId.NewReport);
        var newViaWizardMenu = e.GetById(ActionId.NewReportViaWizard);
        var openMenu = e.GetById(ActionId.OpenReport);
        var designReportMenu = e.GetById(ActionId.ReportWizard);
        var localizationMenu = e.GetById(ActionId.Localization);
        var saveAsMenu = e.GetById(ActionId.SaveAs);
        var exitMenu = e.GetById(ActionId.Exit);

        if (newMenu) newMenu.visible = false;
        if (newViaWizardMenu) newViaWizardMenu.visible = false;
        if (openMenu) openMenu.visible = false;
        if (designReportMenu) designReportMenu.visible = false;
        if (localizationMenu) localizationMenu.visible = false;
        if (saveAsMenu) saveAsMenu.visible = false;
        if (exitMenu) exitMenu.visible = false;
      },
    };
  }
  render() {
    return this.mode === "preview" ? (<div ref="viewer" data-bind="dxReportViewer: $data" style={{ width: "100%", height: "900px" }}><Progress /></div>) : <div ref="designer" data-bind="dxReportDesigner: $data"><Progress /></div>;
  }
  componentDidMount() {
    ajaxSetup.ajaxSettings = {
      headers: {
        "Access-Control-Allow-Headers": "*",
        Authorization: `Bearer ${getTokenId()}`,
      },
    };
    if (this.mode === "preview")
    {
      ko.applyBindings({
        reportUrl: this.reportUrl,
        requestOptions: this.requestOptions
        }, this.refs.viewer);
    }
    else
    {
    AsyncExportApproach(true);
    ko.applyBindings(
      {
        reportUrl: this.reportUrl,
        requestOptions: this.requestOptions,
        callbacks: this.callbacks,
      },
      this.refs.designer
    ) 
    }
  }
  componentWillUnmount() {
   this.mode === "preview" ? ko.cleanNode(this.refs.viewer) : ko.cleanNode(this.refs.designer);
  }
}
