import React from 'react'
import { Controller } from "react-hook-form";
import { Core } from "@ebs/styleguide";
const { FormControl, Typography, TextField } = Core;
import { FormattedMessage } from "react-intl";

export default function PrintTest({ control, setValue, errors, ...props }) {

  return (
    <>
      <FormControl fullWidth  >
        <Controller
          name={"printerName"}
          control={control}
          rules={{
            required: (
              <Typography>
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Printer Name is required"}
                />
              </Typography>
            ), maxLength: 150
          }}
          render={({ field }) => <TextField
            {...field}
            error={Boolean(errors["printerName"])}
            label={
              <>
                <FormattedMessage
                  id={"printerName"}
                  defaultMessage={"Printer Name"}
                />
                {` *`}
              </>
            }
            helperText={errors["printerName"]?.message}
          />}
        />
      </FormControl>

    </>
  )
}
