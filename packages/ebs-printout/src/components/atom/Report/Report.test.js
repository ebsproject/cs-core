import Report from "./Report";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("Report is in the DOM", () => {
  render(<Report></Report>);
  expect(screen.getByTestId("ReportTestId")).toBeInTheDocument();
});
