import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { client as printoutClient } from "utils/axios";
import { client } from "utils/apollo";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { EbsDialog, Core, Icons, Lab } from "@ebs/styleguide";
import { Controller, useForm } from "react-hook-form";
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  TextField,
  Typography,
  Checkbox,
  Chip,
} = Core;
// import { userInfo } from "@ebs/layout";
const { Autocomplete,createFilterOptions } = Lab;
const { Cancel, CheckCircle ,CheckBoxOutlineBlank,CheckBox} = Icons;
import { useSelector, useDispatch } from "react-redux";
import { mutationReport } from "store/modules/Printout";
import { FIND_PRINTOUT_TEMPLATE_LIST } from "utils/apollo/gql/Printout";
/**
 * @prop {func} handleClose
 * @prop {String} type
 * @prop {func} refresh
 * @prop {String} name: optional
 * @prop {String} description: optional
 * @prop {Array} products: optional
 * @prop {Array} programs: optional
 * @prop {String} id: optional
 * @prop {String} zpl: optional
 * @param ref: reference made by React.forward
 */
const ReportAtom = React.forwardRef(
  (
    {
      open,
      type,
      refresh,
      name,
      description,
      handleClose,
      products,
      programs,
      id,
      zpl,
      tenants,
      defaultFormat,
      label
    },
    ref
  ) => {
    const dispatch = useDispatch();
    const { programList, productList } = useSelector(
      ({ printout }) => printout
    );
    const optionsFormat = [ "pdf",
                            "docx",
                            "xls",
                            "xlsx",
                            "rtf",
                            "mht",
                            "html",
                            "txt",
                            "csv",
                            "png"];
    const [templateList, setTemplateList] = useState([]);
    //This is used to bring specific tenant data from the user Info fuction
    const dataUser = [];
    const {
      handleSubmit,
      control,
      reset,
      formState: { errors },
      setValue,
    } = useForm({
      defaultValues: {
        programs: programs,
        products: products,
        name: name,
        zpl: zpl,
        description: description,
        tenants: tenants,
        defaultFormat: defaultFormat,
        label: label ? label : name,
      },
    });
    useEffect(() => {
      if(type === "COPY"){
      try {
        client.query({
          query: FIND_PRINTOUT_TEMPLATE_LIST,
          variables: {
            page: { number: 1, size: 1000 },
          },
          fetchPolicy:"no-cache"
        }).then(({ data, errors }) => {
        let templates  = []
        data.findPrintoutTemplateList.content.forEach(item=>{
          templates.push({id:item.id, name:item.name})
        });
          printoutClient.get(`/api/FileManager/GetSystemTemplateList`).then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              let result = data.result.data.filter(item => item.documentName !== "sys_connection")
              result.forEach(item =>{
                 templates.push({id:0,name:item.documentName})
              });
              setTemplateList(templates)
            }
          });
        }).catch (e => console.log(e))
      } catch (error) {
        console.log(error)
      }
    }
    },[])

    const resetClose = ()=>{
      setSelectedOptions([])
      setSelectedOptionsProducts([])
      reset();
      handleClose(type);
      refresh();
    }
    /**
     * @returns {func}
     */
    const handleClickClose = () => {
      if(type === "BLANK"  || type === "COPY" )
      {setSelectedOptions([])
      setSelectedOptionsProducts([])}
      handleClose(type);
      reset();
    };
    /**
     * @param {Object} data
     */
    const onSubmit =async (data) => {
      await mutationReport({
        type,
        data: { ...data, id, oldProducts: products, oldPrograms: programs },
      })(dispatch);
      resetClose();
    };
    /**
     * @param {Object} e
     * @param {Object} options
     */
    const onReportChange = (e, options) => {
      e.preventDefault();
      setValue("template", options);
    };
    /**
     * @param {Object} e
     * @param {Object} options
     */
    const onTenantsChange = (e, options) => {
      e.preventDefault();
      setValue("tenants", options);
    };
    /**
     * @param {Object} option
     * @returns {String}
     */
    const optionLabel = (option) => `${option.name}`;
     /**
     * @param {Object} option
     * @returns {String}
     */
     const optionLabelFormat = (option) => `${option}`;
    /**
     * @param {Object} option
     * @returns {String}
     */
    const optionTenantLabel = (option) => `${option.name}`;
    /**
     * @param {Object} e
     * @param {String} value
     */
    /**
     * @param {Object} option
     * @param {Boolean} selected
     * @returns {Node}
     */
///Select All Programs Implementation
const [selectedOptions, setSelectedOptions] = useState(type ==="PUT" ? programs :[]);
const allSelected = programList.length === selectedOptions.length;
const handleToggleOption = selectedOptions =>
  setSelectedOptions(selectedOptions);
const handleClearOptions = () => setSelectedOptions([]);
const getOptionLabel = option => `${option.name}`;
const handleSelectAll = isSelected => {
  if (isSelected) {
    setSelectedOptions(programList);
  } else {
    handleClearOptions();
  }
};
const filter = createFilterOptions();
const handleChange = (event, selectedOptions, reason) => {
  if (reason === "selectOption" || reason === "removeOption") {
    if (selectedOptions.find(option => option.value === "select-all")) {
      handleToggleSelectAll();
      let result = [];
      result = programList.filter(el => el.value !== "select-all");
      setValue("programs", result);
    } else {
      handleToggleOption && handleToggleOption(selectedOptions);
      setValue("programs", selectedOptions);
    }
  } else if (reason === "clear") {
    handleClearOptions && handleClearOptions();
    setValue("programs", []);
  }

};
const handleToggleSelectAll = () => {
  handleSelectAll && handleSelectAll(!allSelected);
};

const optionRenderer = (props,option, { selected }) => {
  const {key, ...restProps } = props;
  const selectAllProps =
    option.value === "select-all" // To control the state of 'select-all' checkbox
      ? { checked: allSelected }
      : {};
  return (
    <li key = {key} {...restProps}>
      <Checkbox
        color="primary"
        icon={<CheckBoxOutlineBlank fontSize="small" />}
        checkedIcon={<CheckBox fontSize="small" />}
        style={{ marginRight: 8 }}
        checked={allSelected || selected}
      />
      {getOptionLabel(option)}
    </li>
  );
};
/// end Select All Program implementation

///Select All Product Implementation
const [selectedOptionsProducts, setSelectedOptionsProducts] = useState(type ==="PUT" ? products :[]);
const allSelectedProducts = productList.length === selectedOptionsProducts.length;
const handleToggleOptionProducts = selectedOptionsProducts =>
  setSelectedOptionsProducts(selectedOptionsProducts);
const handleClearOptionsProducts = () => setSelectedOptionsProducts([]);
const getOptionLabelProducts = option => `${option.name}`;
const handleSelectAllProducts = isSelected => {
  if (isSelected) {
    setSelectedOptionsProducts(productList);
  } else {
    handleClearOptionsProducts();
  }
};
const filterProducts = createFilterOptions();
const handleChangeProducts = (event, selectedOptions, reason) => {
  if (reason === "selectOption" || reason === "removeOption") {
    if (selectedOptions.find(option => option.value === "select-all")) {
      handleToggleSelectAllProducts();
      let result = [];
      result = productList.filter(el => el.value !== "select-all");
      setValue("products", result);
    } else {
      handleToggleOptionProducts && handleToggleOptionProducts(selectedOptions);
      setValue("products", selectedOptions);
    }
  } else if (reason === "clear") {
    handleClearOptionsProducts && handleClearOptionsProducts();
    setValue("products", []);
  }

};
const handleToggleSelectAllProducts = () => {
  handleSelectAllProducts && handleSelectAllProducts(!allSelectedProducts);
};

const optionRendererProducts = (props,option, { selected }) => {
  const {key, ...restProps } = props;
  const selectAllProps =
    option.value === "select-all" // To control the state of 'select-all' checkbox
      ? { checked: allSelectedProducts }
      : {};
  return (
    <li key = {key} {...restProps}>
      <Checkbox
        color="primary"
        icon={<CheckBoxOutlineBlank fontSize="small" />}
        checkedIcon={<CheckBox fontSize="small" />}
        style={{ marginRight: 8 }}
        checked={allSelectedProducts || selected}
      />
      {getOptionLabelProducts(option)}
    </li>
  );
};
/// end Select All Program implementation

const handleChangeFormat = (event, selectedOptions, reason) => {
  setValue("defaultFormat", selectedOptions);
};

   /**
     * @param {Object} params
     * @returns {node}
     */
   const renderInputFormat = (params) => (
    <TextField
      {...params}
      focused={Boolean(errors["format"])}
      error={Boolean(errors["format"])}
      label={
        <>
          <FormattedMessage id={"none"} defaultMessage={"Default print option"} />
        </>
      }
      helperText={errors["format"]?.message}
    />
  );


    /**
     * @param {Object} option
     * @param {Boolean} selected
     * @returns {Node}
     */
    const productOption = (option, { selected }) => {
      return (
        <>
          <Checkbox checked={selected} />
          {option.name}
        </>
      );
    };
     /**
     * @param {Object} option
     * @param {Boolean} selected
     * @returns {Node}
     */
      const tenantOption = (props,option, { selected }) => {
        return (
          <li {...props}>
            <Checkbox checked={selected} />
            {option.name}
          </li>
        );
      };
    /**
     * @param {Object} params
     * @returns {node}
     */
    const renderInputReports = (params) => (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Template"} />
            {` *`}{" "}
          </>
        }
        focused={Boolean(errors["template"])}
        error={Boolean(errors["template"])}
        helperText={errors["template"]?.message}
      />
    );
    /**
     * @param {Object} params
     * @returns {node}
     */
    const renderInputPrograms = (params) => (
      <TextField
        {...params}
        focused={Boolean(errors["programs"])}
        error={Boolean(errors["programs"])}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Programs"} />
            {` *`}{" "}
          </>
        }
        helperText={errors["programs"]?.message}
      />
    );
    /**
     * @param {Object} params
     * @returns {node}
     */
    const renderInputProducts = (params) => (
      <TextField
        {...params}
        focused={Boolean(errors["products"])}
        error={Boolean(errors["products"])}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Products"} />
            {` *`}{" "}
          </>
        }
        helperText={errors["products"]?.message}
      />
    );
    /**
     * @param {Object} params
     * @returns {node}
     */
    const renderInputTenants = (params) => (
      <TextField
        {...params}
        focused={Boolean(errors["tenants"])}
        error={Boolean(errors["tenants"])}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Tenants"} />
            {` *`}{" "}
          </>
        }
        helperText={errors["teants"]?.message}
      /> 
    );
    /**
     *
     */
    const getProgramSelected = (option, value) => option.id === value.id;
    const getTemplateSelected = (option, value) => option.id === value.id;
    const getProductSelected = (option, value) => option.id === value.id;
const handleClose1 =(event, reason)=>{
  if(reason === "backdropClick")
     return
  handleClickClose()
}
    return (
      /**
       * @prop data-testid: Id to use inside Report.test.js file.
       */
      <div data-testid="ReportTestId">
        <EbsDialog
          open={open}
          handleClose={handleClose1}
          title={
            <FormattedMessage
              id={"none"}
              defaultMessage={
                type === "PUT"
                  ? "Edit Printout Template"
                  : "New Report Template"
              }
            />
          }
          maxWidth={"sm"}
        >
          <form onSubmit={handleSubmit(onSubmit)}>
            <DialogContent>
              <Box className="grid grid-cols-1">
                {type === "COPY" && (
                  <Controller
                    name="template"
                    control={control}
                    render={({ field }) => (
                      <Autocomplete
                        id="template"
                        {...field}
                        data-testid="template"        
                        options={templateList || []}
                        value={field?.value}
                        onChange={onReportChange}
                        getOptionLabel={optionLabel}
                        isOptionEqualToValue={getTemplateSelected}
                        renderInput={renderInputReports}
                      />
                    )}
                    rules={{
                      required:"Please select a Template"
                    }}
                  />
                )}
                <Controller
                  name={"programs"}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      multiple
                      limitTags={5}
                      id="programs"
                      disableCloseOnSelect
                      onChange={handleChange}
                      renderOption={optionRenderer}
                      options={programList || []}
                      value={selectedOptions}
                      getOptionLabel={optionLabel}
                      renderInput={renderInputPrograms}
                      filterOptions={(options, params) => {
                        const filtered = filter(options, params);
                        return [{ name: "Select all", value: "select-all" }, ...filtered];
                      }}
                      isOptionEqualToValue={getProgramSelected}
                    />
                  )}
                  rules={{ required: "Please select one or more Program(s)"}}
                />
                <Controller
                  name={"products"}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      multiple
                      limitTags={5}
                      id="products"
                      disableCloseOnSelect
                      onChange={handleChangeProducts}
                      options={productList || []}
                      value={selectedOptionsProducts}
                      getOptionLabel={optionLabel}
                      renderOption={optionRendererProducts}
                      renderInput={renderInputProducts}
                      isOptionEqualToValue={getProductSelected}
                      filterOptions={(options, params) => {
                        const filtered = filterProducts(options, params);
                        return [{ name: "Select all", value: "select-all" }, ...filtered];
                      }}
                    />
                  )}
                  rules={{ required: "Please select one or more Product(s)"}}
                />               
                <Controller
                  name={"label"}
                  control={control}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      focused={Boolean(errors["label"])}
                      error={Boolean(errors["label"])}
                      label={
                        <>
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Name"}
                          />
                          {` *`}
                        </>
                      }
                      helperText={errors["label"]?.message}
                    />
                  )}
                  rules={{required: "Please provide a printout name"}}
                />
                <Controller
                  name={"zpl"}
                  control={control}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      focused={Boolean(errors["zpl"])}
                      error={Boolean(errors["zpl"])}
                      multiline
                      rows={5}
                      label={
                        <>
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Zpl"}
                          />
                        </>
                      }
                      helperText={errors["zpl"]?.message}
                    />
                  )}
                />
                <Controller
                  name={"description"}
                  control={control}
                  render={({ field }) => (
                    <TextField
                      {...field}
                      focused={Boolean(errors["description"])}
                      error={Boolean(errors["description"])}
                      multiline
                      rows={3}
                      label={
                        <>
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Description"}
                          />
                          {` *`}
                        </>
                      }
                      helperText={errors["description"]?.message}
                    />
                  )}
                  rules={{
                    required:  "Please provide the printout description"
                  }}
                />
                <Controller
                  name={"defaultFormat"}
                  control={control}
                  render={({ field }) => (
                    <Autocomplete
                      {...field}
                      id="defaultFormat"
                      onChange={handleChangeFormat}
                      options={optionsFormat || []}
                      getOptionLabel={optionLabelFormat}
                      renderInput={renderInputFormat}
                    />
                  )}
                  defaultValue={ "pdf" }
                />

              </Box>
            </DialogContent>
            <DialogActions>
              <Button
                onClick={handleClose1}
                startIcon={<Cancel />}
              >
                <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
              </Button>
              <Button
                type="submit"
                startIcon={<CheckCircle />}
              >
                <FormattedMessage id={"none"} defaultMessage={"Save"} />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ReportAtom.propTypes = {
  open: PropTypes.bool,
  type: PropTypes.oneOf(["BLANK", "COPY", "PUT"]).isRequired,
  refresh: PropTypes.func.isRequired,
  handleClose: PropTypes.func,
  name: PropTypes.string,
  description: PropTypes.string,
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ),
  programs: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
    })
  ),
  dataSourceUris: PropTypes.arrayOf(
    PropTypes.shape({ name: PropTypes.string, value: PropTypes.string })
  ),
  id: PropTypes.string,
  zpl: PropTypes.string,
};
// Default properties
ReportAtom.defaultProps = {
  open: false,
};

export default ReportAtom;
