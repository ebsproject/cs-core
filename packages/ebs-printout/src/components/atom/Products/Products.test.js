import Products from "./Products";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
const mockData = [
  { name: "Product 1", domain: { name: "Domain 1" } },
  { name: "Product 2", domain: { name: "Domain 2" } },
];
afterEach(cleanup);

test("Products is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <Products data={mockData}></Products>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("ProductsTestId")).toBeInTheDocument();
});
