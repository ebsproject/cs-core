import React from 'react'
import { Core } from "@ebs/styleguide";
import { Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
const { Input, TextField, FormControl, Typography } = Core;


export default function InputReportParam({ control, setValue, parameter, errors, ...props }) {



  return (
    <FormControl fullWidth>
      <Controller
        name={parameter.name}
        control={control}
        rules={{
          required: (
            <Typography>
              <FormattedMessage
                id={parameter.name}
                defaultMessage={`${parameter.name} is required`}
              />
            </Typography>
          ), maxLength: 150
        }}
        defaultValue={parameter.value}
        render={({ field }) => <TextField
          {...field}
          error={Boolean(errors[parameter.name])}
          label={
            <>
              <FormattedMessage
                id={parameter.name}
                defaultMessage={parameter.name}
              />
              {` *`}
            </>
          }
          helperText={errors[parameter.name]?.message}
        />}
      />
    </FormControl>
  )
}
