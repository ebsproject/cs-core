import Progress from "./progress";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
afterEach(cleanup);

test("Progress is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <Progress></Progress>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("ProgressTestId")).toBeInTheDocument();
});
