import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS
import {Core,Styles} from "@ebs/styleguide";
const {LinearProgress} = Core;
// STYLES
const { useTheme } = Styles


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ProgressAtom = React.forwardRef((props, ref) => {
  const theme = useTheme();
  const classes = {
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }

  return (
    /*
     @prop data-testid: Id to use inside progress.test.js file.
     */
    <div sx={classes.root} data-testid={'ProgressTestId'}>
      <LinearProgress />
    </div>
  )
})
// Type and required properties
ProgressAtom.propTypes = {
}
// Default properties
ProgressAtom.defaultProps = {
}

export default ProgressAtom
