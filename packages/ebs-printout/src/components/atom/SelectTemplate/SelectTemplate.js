import React, { useState } from 'react';
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
import { useQuery } from "@apollo/client";
import { FIND_PRINTOUT_TEMPLATE_LIST } from "utils/apollo/gql/Printout"


const {
  TextField,
  Typography,
} = Core;
const { Autocomplete } = Lab;
import { Controller } from "react-hook-form";

export default function SelectTemplate({ control, errors, setValue }) {

  const { loading, error, data } = useQuery(FIND_PRINTOUT_TEMPLATE_LIST, {
    variables:{page:{number: 1, size:300}},
    fetchPolicy: 'network-only',
  });
  const [inputValue, setInputValue] = React.useState('');


  if (loading) return null;
  if (error) return `Error! ${error}`;

  const result = data.findPrintoutTemplateList.content.map(item => {
    let newItem = {...item};
    if (item.label === null ) {
      newItem.label = item.name
    }
    return newItem;
  });



  /**
* @param {Object} params
* @returns {node}
*/
  const renderInputReports = (params) => (
    <TextField
      {...params}
      label={
        <>
          <FormattedMessage id={"none"} defaultMessage={"Link to Template"} />
          {` *`}{" "}
        </>
      }
      focused={Boolean(errors["TemplateList"])}
      error={Boolean(errors["TemplateList"])}
      helperText={errors["TemplateList"]?.message}
    />
  );

  /**
* @param {Object} e
* @param {String} value
*/
  const onInputChange = (e, value) => {
    setInputValue(value);
  };


  /**
 * @param {Object} option
 * @returns {String}
 */
  const optionLabelReports = (option) => {
    const obj = result.find(item => item.name === option.name)
    return obj ? obj.label : option.name};

  /**
* @param {Object} e
* @param {Object} options
*/
  const onReportChange = (e, options) => {
    e.preventDefault();
    setValue("TemplateList", options);
  };


  return (
    <>

      <Controller
        name={"TemplateList"}
        control={control}
        render={({ field }) => (
          <Autocomplete
            multiple
            {...field}
            isOptionEqualToValue={(option, value) => option.name === value.name}
            options={result || []}
            onChange={onReportChange}
            getOptionLabel={optionLabelReports}
            onInputChange={onInputChange}
            renderInput={renderInputReports}
          />
        )}
      />
    </>
  )
}
