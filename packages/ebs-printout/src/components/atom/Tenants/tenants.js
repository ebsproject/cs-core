import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
const { Typography,List,ListItem } = Core;

//MAIN FUNCTION
/**
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Tenants = React.forwardRef(({ data }, ref) => {


  /** 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <div className=" bg-white" data-testid={"TenantsTestId"}>
  <List>
    <ListItem>
    <Typography className="font-ebs font-bold text-lg">
{"NAME"}
</Typography>
    </ListItem>
    {data.map((item,index) =>(
<ListItem
key= {index}
>
<Typography className="font-ebs font bold">
{`${index + 1}. ${item.name}`}
</Typography>
</ListItem>

    ))}
  </List>
</div>
  );
});

// Type and required properties
Tenants.propTypes = {
  data: PropTypes.array.isRequired,
};
// Default properties
Tenants.defaultProps = {};

export default Tenants;
