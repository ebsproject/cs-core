import React, { useEffect, useState, useContext } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import fetch from "cross-fetch";
import { ApolloClient, InMemoryCache, HttpLink, ApolloLink, from } from "@apollo/client";
import { FormattedMessage } from "react-intl";
import { EbsDialog, Core, Lab, Icons } from "@ebs/styleguide";
import { showMessage } from "store/modules/message";
import { Controller, useForm, useFieldArray } from "react-hook-form";
import { buildQuery, buildQuerySM } from "utils/apollo/gql/Printout";
import { useSelector, useDispatch } from "react-redux";
import { createDataSource } from "store/modules/Printout";
import { getTokenId, getCoreSystemContext, userContext } from "@ebs/layout";
import { setDefaultValues, checkData } from "./functions";
import { ParametersComponent } from "./ParametersComponent";
const { Box, Button, DialogActions, DialogContent, Checkbox, FormControlLabel, TextField, Typography, Grid, Divider } = Core;
const { Autocomplete } = Lab;
const { Cancel, CheckCircle } = Icons;
import SelectTemplate from "components/atom/SelectTemplate";
import SelectEndpoint from "../SelectEndpoint";
import Progress from 'components/atom/Progress';

const AddConnectionAtom = React.forwardRef(({ refresh, type, rowData, open, handleClose }, ref) => {
  const { domainList } = useSelector(({ printout }) => printout);
  const dispatch = useDispatch();
  const { state } = useSelector(({ printout }) => printout);
  const [isGraph, setIsGraph] = useState(rowData?.isGraph ? true : false);
  const [endpointSelect, setEndpointSelect] = useState(false);
  const [sgContextSelected, setSGContextSelected] = useState(null);
  const [showResult, setShowResult] = useState(false);
  const { printoutUri, cbGraphqlUri } = getCoreSystemContext();
  const { userProfile } = useContext(userContext);
  const dateCon = new Date().toLocaleDateString();
  const {
    handleSubmit,
    control,
    reset,
    register,
    formState: { errors },
    setValue,
    getValues
  } = useForm({
    defaultValues: setDefaultValues(type, rowData, domainList, setSGContextSelected)
  });

  const [disable, setDisable] = React.useState(true);
  const [isHttpGet, setIsHttpGet] = React.useState(true);
  const [testingMode, setTestingMode] = React.useState(false);
  const [loading, setLoading] = React.useState(false);

  const handleClickClose = () => {

    setLoading(false);
    setDisable(true);
    handleClose(type);
    reset();
  };

  const handleClose1 = (event, reason) => {
    if (reason === "backdropClick")
      return
    handleClickClose()
  }

  const changeValuesGraph = (option, value) => {
    setValue("IsGraph", value);
    setIsGraph(value);
    setValue("Uri", "");
    setValue("Type", "custom");
    setValue('HttpMethod', value ? { id: 2, name: "POST" } : { id: 1, name: "GET" })
    setValue("endpointSelector", { id: 10, name: "Custom" })
  }

  const testConn = () => setTestingMode(true)
  const setChange = () => setDisable(true);

  const onSubmit = async (dataForm) => {
    let sg = getValues("sgContext");
    let data = checkData(dataForm);
    if (testingMode) {
      if (isGraph)
        await testConnectionGraph(data);
      else
        testConnection(data);
      return;
    }
    if (sg)
      data.Uri = data.Uri.replace(sg, "{0}");
    else
      data.Uri = data.Uri.replace(sgContextSelected, "{0}");
    if (data.Type === "csps") {
      if (printoutUri.endsWith("/"))
        data.Uri = data.Uri.replace(printoutUri, "{0}");
      else {
        let printUri = printoutUri + "/";
        data.Uri = data.Uri.replace(printUri, "{0}");
      }
    }
    if (data.Type === "cbapi") {
      data.Uri = data.Uri.replace(data.Uri, '{0}');
    }

    await createDataSource(data, dispatch, userProfile.account, dateCon, type,rowData?.createdBy, rowData?.createdOn);
    handleClickClose();
    refresh();
  };

  const testConnection = ({ HttpMethod, QueryParameters, Uri, IsDefault }) => {
    setLoading(true)
    var UriResult = "";
    var extractParams = QueryParameters.map((x => {
      if (x.Name == "" && x.Value == "" && x.DefaultValue == "") {
        return "";
      }
      else {
        return x.Name + '=' + x.DefaultValue;
      }

    }))
    if (extractParams[0] === "") {
      UriResult = Uri
    }
    else {
      var params = extractParams.join('&');
      UriResult = `${Uri}?${params}`;
    }
    if (IsDefault) {
      axios.defaults.headers.common = { 'Authorization': `Bearer ${getTokenId()}` }
    }
    else {
      axios.defaults.headers.common = {};
    }

    axios({
      'method': HttpMethod,
      'url': UriResult,
      'headers': {
        'Accept': "application/json",
      }
    }).then((response) => {
      if (response.status == 200) {
        setTestingMode(false)
        setLoading(false);
        setDisable(false);
        dispatch(
          showMessage({
            message: "Successfully tested connection",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        )
      }
      else {
        setTestingMode(false)
        setDisable(true);
        setLoading(false);
        dispatch(
          showMessage({
            message: "Check parameters, method or Uri",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        )
      }
    }).catch(err => {
      setTestingMode(false)
      setDisable(true);
      setLoading(false);
      dispatch(
        showMessage({
          message: err.message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      )
    })
  }
  const testConnectionGraph = async (data) => {
    let variables = { filters: [], page: { number: 1, size: 20 } }
    if (data.QueryParameters.length > 0) {
      data.QueryParameters.forEach(filter => {
        if (filter.Name === "page") {
          variables.page.number = Number(filter.DefaultValue);
          return;
        }
        if (filter.Name === "size") {
          variables.page.size = Number(filter.DefaultValue);
          return;
        }
        variables.filters.push({ col: filter.Name, mod: "EQ", val: filter.DefaultValue })
      })
    }
    setLoading(true)
    const httpLink = new HttpLink({
      uri: data.Uri,
      fetch,
    });
    const authLink = new ApolloLink((operation, forward) => {
      operation.setContext(({ headers }) => ({
        headers: {
          ...headers,
          authorization: `Bearer ${getTokenId()}`,
        },
      }));
      return forward(operation);
    });
    const clientTest = new ApolloClient({
      link: from([authLink, httpLink]),
      cache: new InMemoryCache({
        addTypename: false,
      }),
    });
    const QUERY = data.Type === "smapi" ? buildQuerySM(data.Entity, data.Content) : buildQuery(data.Entity, data.Content);
    try {
      const { errors, data: result } = await clientTest.query({
        query: QUERY,
        variables: variables
      })

      if (result) {
        setValue("Result", JSON.stringify(result[`find${data.Entity}List`].content, undefined, 2));
        setShowResult(true);
        setTestingMode(false)
        setLoading(false);
        setDisable(false);
        dispatch(
          showMessage({
            message: "Connection tested successfully",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        )
      }
      else {
        setTestingMode(false)
        setLoading(false);
        dispatch(
          showMessage({
            message: "Check parameters, method or Uri",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        )
      }
    } catch (error) {
      setTestingMode(false)
      setLoading(false);
      dispatch(
        showMessage({
          message: error.toString() + " Check the endpoint",
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      )
    }
  }
  /**
    * @param {Object} option
    * @returns {String}
    */
  const optionLabel = (option) => `${option.name}`;

  const getHttpMethodSelected = (option, value) => option.name === value.name;
  /**
  * @param {Object} e
  * @param {Object} options
  */
  const onHttpMethodChange = (e, options) => {
    e.preventDefault();
    setIsHttpGet(options != null && options.name === "GET" ? true : false);
    setValue("HttpMethod", options);
  };

  const renderInputHttpMethod = (params, field) => {
    const newInputProps = {};
    if (!getValues(field)) {
      Object.assign(newInputProps, { value: "" });
    }

    return (
      <TextField
        {...params}
        inputProps={{
          ...params.inputProps,
          ...newInputProps,
        }}
        focused={Boolean(errors[field])}
        error={Boolean(errors[field])}
        label={
          <Typography className="text-ebs">
            <FormattedMessage id={"none"} defaultMessage={"Http Method"} />
            {` *`}
          </Typography>
        }
        helperText={errors[field]?.message}
      />
    );
  };
  const { fields: queryParameters, append: addQueryParam, remove: removeQueryParam } = useFieldArray({ control, name: "QueryParameters" });
  const { fields: pathParameters, append: addPathParam, remove: removePathParam } = useFieldArray({ control, name: "PathParameters" });
  return (
    /**
     * @prop data-testid: Id to use inside Report.test.js file.
     */
    <div data-testid="AddConnectionTestId">
      <EbsDialog
        open={open}
        handleClose={handleClose1}
        title={
          <FormattedMessage
            id={"none"}
            defaultMessage={type === "PUT_DATASOURCE" ? "Modify Data Source Connection" : "Create Data Source Connection"}
          />
        }
        maxWidth={"sm"}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <DialogContent className="h-100 w-100" >
            <Box className="grid grid-cols-1" >
              <Grid container direction="row">
                  <Grid item xs={6}>
                    <Controller
                      name="IsGraph"
                      control={control}
                      render={({ field }) => (
                        <FormControlLabel
                          control={
                            <Checkbox
                              {...field}
                              disabled={type !== "NEW"}
                              data-testid="IsGraph"
                              className="x-6"
                              checked={field.value}
                              onChange={
                                changeValuesGraph
                              }
                            />
                          }
                          label={
                            <Typography className="font-ebs">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Mark if this connection use GraphQL"
                              />
                            </Typography>
                          }
                        />
                      )}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <SelectEndpoint
                      control={control}
                      errors={errors}
                      {...register("endpointSelector")}
                      setValue={setValue}
                      setEndpointSelect={setEndpointSelect}
                      isGraph={isGraph}
                      setSGContextSelected={setSGContextSelected}
                      domains={domainList || []}
                    />
                  </Grid>
                </Grid>
                  <Divider />
                  <Controller
                    name={"Name"}
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        disabled={type === "PUT_DATASOURCE"}
                        focused={Boolean(errors["Name"])}
                        error={Boolean(errors["Name"])}
                        label={
                          <>
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Connection Name"}
                            />
                            {` *`}
                          </>
                        }
                        helperText={errors["Name"]?.message}
                      />
                    )}
                    rules={{
                      required: (
                        <Typography>
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Please provide a connection name"}
                          />
                        </Typography>
                      ),
                    }}
                  />
                  <Controller
                    name={"Uri"}
                    control={control}
                    onChange={setChange}
                    render={({ field }) => (
                      <TextField
                        id={"Uri"}
                        {...field}
                        disabled={endpointSelect}
                        focused={Boolean(errors["Uri"])}
                        error={Boolean(errors["Uri"])}
                        helperText={errors["Uri"]?.message}
                        label={
                          <>
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Data Source Uri"}
                            />
                            {` *`}
                          </>
                        }
                      />
                    )}
                    rules={{
                      required: (
                        <Typography>
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Please provide a data source uri"}
                          />
                        </Typography>
                      ),
                    }}
                  />
                <Grid container direction="row">
                  <Grid item xs={6}>
                    <Controller
                      name="HttpMethod"
                      control={control}
                      render={({ field }) => (
                        <Autocomplete
                          {...field}
                          id={"HttpMethod"}
                          data-testid={"HttpMethod"}
                          options={[
                            { id: 1, name: "GET" },
                            { id: 2, name: "POST" },
                          ]}
                          onChange={(e, options) =>
                            onHttpMethodChange(e, options, "HttpMethod")
                          }
                          isOptionEqualToValue={getHttpMethodSelected}
                          getOptionLabel={optionLabel}
                          renderInput={(params) =>
                            renderInputHttpMethod(params, "HttpMethod")
                          }
                        />
                      )}
                      rules={{
                        required: (
                          <Typography>
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Please select an HTTP method"}
                            />
                          </Typography>
                        ),
                      }}
                    />
                  </Grid>
                  <Grid item xs={6}>
                    <SelectTemplate control={control} errors={errors} setValue={setValue} />
                  </Grid>

                  {!isGraph && (
                      <Grid item xs={6}>
                        <Controller
                          name="ConvertQueryToBody"
                          control={control}

                          render={({ field }) => (
                            <FormControlLabel
                              control={
                                <Checkbox
                                  disabled={isHttpGet}
                                  {...field}
                                  data-testid="ConvertQueryToBody"
                                  className="x-6"
                                  checked={field.value}
                                />
                              }
                              label={
                                <Typography className="font-ebs">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Queries Params as Body"
                                  />
                                </Typography>
                              }
                            />
                          )}
                        />
                      </Grid>
                    )}
                  <Grid item xs={6}>
                    <Controller
                      name="AllData"
                      control={control}
                      render={({ field }) => (
                        <FormControlLabel
                          control={
                            <Checkbox
                              {...field}
                              data-testid="AllData"
                              className="x-6"
                              checked={field.value}
                            />
                          }
                          label={
                            <Typography className="font-ebs">
                              <FormattedMessage
                                id="none"
                                defaultMessage="Include All Pages in the result (Does not work in custom APIs)"
                              />
                            </Typography>
                          }
                        />
                      )}
                    />
                  </Grid>
              </Grid>
              <ParametersComponent
                control={control}
                errors={errors}
                showResult={showResult}
                queryParameters={queryParameters}
                pathParameters={pathParameters}
                addPathParam={addPathParam}
                addQueryParam={addQueryParam}
                isGraph={isGraph}
                removePathParam={removePathParam}
                removeQueryParam={removeQueryParam}
              />
            </Box>
          </DialogContent>
          <DialogActions>
            <Box className="grid grid-cols-1">
              {loading ? <Progress /> : null}
              <Button
                type="submit"
                disabled={!disable}
                onClick={testConn}
                startIcon={<CheckCircle />}
                className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
              >
                <FormattedMessage id={"none"} defaultMessage={"Test Connection"} />
              </Button>
            </Box>
            <Controller
              name="IsDefault"
              control={control}
              render={({ field }) => (
                <FormControlLabel
                  control={
                    <Checkbox
                      {...field}
                      data-testid="IsDefault"
                      className="x-6"
                      checked={field.value}
                    />
                  }
                  label={
                    <Typography className="font-ebs">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Required Auth"
                      />
                    </Typography>
                  }
                />
              )}
            />
            <Button
              onClick={handleClickClose}
              startIcon={<Cancel />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
            </Button>
            <Button
              type="submit"
              startIcon={<CheckCircle />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Save"} />
            </Button>
          </DialogActions>
        </form>
      </EbsDialog>
    </div>
  );
}
);
// Type and required properties
AddConnectionAtom.propTypes = {
  open: PropTypes.bool,
  type: PropTypes.oneOf(["BLANK", "COPY", "PUT", "NEW", "PUT_DATASOURCE"]).isRequired,
  refresh: PropTypes.func.isRequired,
  handleClose: PropTypes.func,
  connectionName: PropTypes.string,
  Uri: PropTypes.string,


};
// Default properties
AddConnectionAtom.defaultProps = {
  open: false,
};

export default AddConnectionAtom;
