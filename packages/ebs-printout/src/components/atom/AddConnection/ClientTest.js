import React, { useEffect, useState } from 'react'
import fetch from "cross-fetch";
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from "@apollo/client";
import { getTokenId, getContext, getCoreSystemContext } from "@ebs/layout";
export const ClientTester = React.forwardRef((props, ref) => {
    const { HttpMethod, QueryParameters, Uri, IsDefault, ...rest } = props
    (dispatch)
    console.log(props);
})
const { graphqlUri } = getCoreSystemContext();
/**
 * * Core client
 */
const httpLink = new HttpLink({
  uri: graphqlUri,
  fetch,
});
/**
 * * Core client
 */
const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      authorization: `Bearer ${getTokenId()}`,
    },
  }));
  return forward(operation);
});
/**
 * * Core client
 */
export const clientTest = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});
