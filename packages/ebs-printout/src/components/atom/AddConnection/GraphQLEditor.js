import { Core } from "@ebs/styleguide";
const { Grid, TextField, Typography } = Core;
import { Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";

const GraphQLEditor = ({ control, errors, showResult }) => {
    return (
        <Grid container spacing={2}>
            <Grid item xs={12}>
                <Controller
                    name={"Content"}
                    control={control}
                    render={({ field }) => (
                        <TextField
                            // style={{backgroundColor: "#222"}}
                            inputProps={{ style: { fontFamily:"sans-serif", fontSize:"10pt" } }}
                            {...field}
                            fullWidth
                            variant="filled"
                            multiline
                            rows={12}
                            focused={Boolean(errors["Content"])}
                            error={Boolean(errors["Content"])}
                            label={
                                <>
                                    <FormattedMessage
                                        id={"none"}
                                        defaultMessage={"Query"}
                                    />
                                    {` *`}
                                </>
                            }
                            helperText={errors["Content"]?.message}
                        />
                    )}
                    rules={{
                        required: (
                            <Typography>
                                <FormattedMessage
                                    id={"none"}
                                    defaultMessage={"Please provide a GraphQL query"}
                                />
                            </Typography>
                        ),
                    }}
                />
            </Grid>
            <Grid item xs={6}>
                <Controller
                    name={"Entity"}
                    control={control}
                    render={({ field }) => (
                        <TextField
                            {...field}
                            fullWidth
                            variant="outlined"
                            focused={Boolean(errors["Entity"])}
                            error={Boolean(errors["Entity"])}
                            label={
                                <>
                                    <FormattedMessage
                                        id={"none"}
                                        defaultMessage={"Entity Name"}
                                    />
                                    {` *`}
                                </>
                            }
                            helperText={errors["Entity"]?.message}
                        />
                    )}
                    rules={{
                        required: (
                            <Typography>
                                <FormattedMessage
                                    id={"none"}
                                    defaultMessage={"Please provide a Entity"}
                                />
                            </Typography>
                        ),
                    }}
                />

            </Grid>
            {showResult && (
            <Grid item xs={12}>
            <Controller
                    name={"Result"}
                    control={control}
                    render={({ field }) => (
                        <TextField
                            {...field}
                            fullWidth
                            variant="outlined"
                            multiline
                            rows={10}
                        />
                    )}
                />

            </Grid>
            )}
        </Grid>
    )
};
export default GraphQLEditor;