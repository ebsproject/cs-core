import { Core, Icons } from "@ebs/styleguide";
import { Fragment } from "react";
import GraphQLEditor from "./GraphQLEditor";
import { Controller } from "react-hook-form";
const { Box, FormControl, InputLabel, Input, Button, Tooltip, TextField, Typography, Grid, Divider } = Core;
import { FormattedMessage } from "react-intl";
const { Add, Close } = Icons

export const ParametersComponent = ({ removePathParam, removeQueryParam, isGraph, control, errors, showResult, queryParameters, pathParameters, addQueryParam, addPathParam }) => {

  return (
    isGraph ? (
      <>
        <GraphQLEditor control={control} errors={errors} showResult={showResult} />
        <Grid container direction="row">
          <Typography className="font-ebs text-md" sx={{ mb: 2 }}>
            <FormattedMessage
              id="none"
              defaultMessage="Filters"
            />
          </Typography>
        </Grid>
        {queryParameters.map((qp, index) => (
          <>
            <Fragment key={index}>
              <Grid container direction="column">
                <Grid item xs={6}>
                  <Controller
                    name={`QueryParameters.${index}.Name`}
                    control={control}
                    render={({ field }) => (
                      <FormControl className="col-span-3" hiddenLabel>
                        <InputLabel htmlFor={`QueryParameters.${index}.Name`}>
                          <FormattedMessage
                            id="none"
                            defaultMessage="Column Name"
                          />
                          {` *`}
                        </InputLabel>
                        <Input
                          {...field}
                          aria-label={`QueryParameters.${index}.Name`}
                          name={`QueryParameters.${index}.Name`}
                          id={`QueryParameters.${index}.Name`}
                          data-testid={`QueryParameters.${index}.Name`}
                        />
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Controller
                    name={`QueryParameters.${index}.DefaultValue`}
                    control={control}
                    render={({ field }) => (
                      <FormControl className="col-span-3" hiddenLabel>
                        <InputLabel htmlFor={`QueryParameters.${index}.DefaultValue`}>
                          <FormattedMessage
                            id="none"
                            defaultMessage="Value"
                          />
                          {` *`}
                        </InputLabel>
                        <Input
                          {...field}
                          aria-label={`QueryParameters.${index}.DefaultValue`}
                          name={`QueryParameters.${index}.DefaultValue`}
                          id={`QueryParameters.${index}.DefaultValue`}
                          data-testid={`QueryParameters.${index}.DefaultValue`}
                        />
                      </FormControl>
                    )}
                  />
                </Grid>
                <Grid item xs={6}>
                  <Tooltip
                    placement="top"
                    title={
                      <Typography className="font-ebs text-md">
                        <FormattedMessage
                          id="none"
                          defaultMessage="Add another Query Parameter"
                        />
                      </Typography>
                    }
                    arrow
                  >
                    <Button
                      variant="text"
                      onClick={() => addQueryParam({ Type: "", Value: "", Name: "" })}
                      startIcon={<Add />}
                    >
                      <FormattedMessage id="none" defaultMessage="Add" />
                    </Button>
                  </Tooltip>
                </Grid>
                <Grid item xs={6}>
                  {index != 0 && (
                    <Tooltip
                      placement="top"
                      title={
                        <Typography className="font-ebs text-md">
                          <FormattedMessage
                            id="none"
                            defaultMessage="Remove Query Parameter"
                          />
                        </Typography>
                      }
                      arrow
                    >
                      <Button
                        data-testid={"remove"}
                        variant="text"
                        onClick={() => removeQueryParam(index)}
                        startIcon={<Close />}
                      >
                        <FormattedMessage
                          id="none"
                          defaultMessage="Remove"
                        />
                      </Button>
                    </Tooltip>
                  )}
                </Grid>
              </Grid>
            </Fragment>
          </>
        ))}
      </>
    ) : (
      <Box className="grid grid-cols-2">
        <Grid container direction="row" >
          <Grid item xs={6}>
            <div>
              {queryParameters.map((qp, index) => (
                <Fragment key={index}>
                  <Controller
                    name={`QueryParameters.${index}.Id`}
                    control={control}
                    render={({ field }) => (
                      <TextField
                        sx={{ display: { xl: 'none', xs: 'block' } }}
                        {...field}
                        data-testid={`QueryParameters.${index}.Id`}
                        label={"id"}
                      />
                    )}
                  />
                  <Grid container direction="row" >
                    <Grid item xs={6}>
                      <Grid container direction="column" >
                        <Grid item xs={6}>
                          <Typography className="font-ebs text-md" sx={{ mb: 2 }}>
                            <FormattedMessage
                              id="none"
                              defaultMessage="Query Parameter"
                            />
                          </Typography>
                        </Grid>
                        <Grid item xs={6}>
                          <Controller
                            name={`QueryParameters.${index}.Name`}
                            control={control}
                            render={({ field }) => (
                              <FormControl className="col-span-3" hiddenLabel>
                                <InputLabel htmlFor={`QueryParameters.${index}.Name`}>
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Name"
                                  />
                                  {` *`}
                                </InputLabel>
                                <Input
                                  {...field}
                                  aria-label={`QueryParameters.${index}.Name`}
                                  name={`QueryParameters.${index}.Name`}
                                  id={`QueryParameters.${index}.Name`}
                                  data-testid={`QueryParameters.${index}.Name`}
                                />
                              </FormControl>
                            )}
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <Controller
                            name={`QueryParameters.${index}.Value`}
                            control={control}
                            render={({ field }) => (
                              <FormControl className="col-span-3" hiddenLabel>

                                <InputLabel htmlFor={`QueryParameters.${index}.Value`}>
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Value or Expression"
                                  />
                                  {` *`}
                                </InputLabel>
                                <Input
                                  {...field}
                                  aria-label={`QueryParameters.${index}.Value`}
                                  name={`QueryParameters.${index}.Value`}
                                  id={`QueryParameters.${index}.value`}

                                  data-testid={`QueryParameters.${index}.Value`}
                                />
                              </FormControl>
                            )}
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <Controller
                            name={`QueryParameters.${index}.DefaultValue`}
                            control={control}
                            render={({ field }) => (
                              <FormControl className="col-span-3" hiddenLabel>
                                <InputLabel htmlFor={`QueryParameters.${index}.DefaultValue`}>
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Default Value"
                                  />
                                  {` *`}
                                </InputLabel>
                                <Input
                                  {...field}
                                  aria-label={`QueryParameters.${index}.DefaultValue`}
                                  name={`QueryParameters.${index}.DefaultValue`}
                                  id={`QueryParameters.${index}.DefaultValue`}
                                  data-testid={`QueryParameters.${index}.DefaultValue`}
                                />
                              </FormControl>
                            )}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={6}>
                      <Grid container direction="column" justifyContent="center" alignItems="flex-start">
                        <Grid item xs={6}>
                          <Tooltip
                            sx={{ mt: 7 }}
                            placement="top"
                            title={
                              <Typography className="font-ebs text-md">
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="Add another Query Parameter"
                                />
                              </Typography>
                            }
                            arrow
                          >
                            <Button
                              variant="text"
                              onClick={() => addQueryParam({ Type: "", Value: "", Name: "" })}
                              startIcon={<Add />}
                            >
                              <FormattedMessage id="none" defaultMessage="Add" />
                            </Button>
                          </Tooltip>
                        </Grid>
                        {index != 0 && (
                          <Grid item xs={6}>
                            <Tooltip
                              placement="top"
                              title={
                                <Typography className="font-ebs text-md">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Remove Query Parameter"
                                  />
                                </Typography>
                              }
                              arrow
                            >
                              <Button
                                data-testid={"remove"}
                                variant="text"
                                onClick={() => removeQueryParam(index)}
                                startIcon={<Close />}
                              >
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="Remove"
                                />
                              </Button>
                            </Tooltip>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  </Grid>
                </Fragment>
              ))}
            </div>
          </Grid>
          <Grid item xs={6}>
            <div>
              {pathParameters.map((pp, index) => (
                <Fragment key={index}>
                  <Controller
                    name={`PathParameters.${index}.Id`}
                    control={control}
                    render={({ field }) => (
                      <TextField
                        {...field}
                        data-testid={`PathParameters.${index}.Id`}
                        label={"id"}
                        sx={{ display: { xl: 'none', xs: 'block' } }}
                      />
                    )}
                  />
                  <Grid container direction="row" >
                    <Grid item xs={6}>
                      <Grid container direction="column" >
                        <Grid item xs={6}>
                          <Typography className="font-ebs text-md" sx={{ mb: 2 }}>
                            <FormattedMessage
                              id="none"
                              defaultMessage="Path Parameter"
                            />
                          </Typography>
                        </Grid>
                        <Grid item xs={6}>
                          <Controller
                            name={`PathParameters.${index}.Name`}
                            control={control}
                            render={({ field }) => (
                              <FormControl className="col-span-3" hiddenLabel>
                                <InputLabel htmlFor={`PathParameters.${index}.Name`}>
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Name"
                                  />
                                  {` *`}
                                </InputLabel>
                                <Input
                                  {...field}
                                  aria-label={`PathParameters.${index}.Name`}
                                  name={`PathParameters.${index}.Name`}
                                  id={`PathParameters.${index}.Name`}
                                  data-testid={`PathParameters.${index}.Name`}
                                />
                              </FormControl>
                            )}
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <Controller
                            name={`PathParameters.${index}.Value`}
                            control={control}
                            render={({ field }) => (
                              <FormControl className="col-span-3" hiddenLabel>
                                <InputLabel htmlFor={`PathParameters.${index}.Value`}>
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Value"
                                  />
                                  {` *`}
                                </InputLabel>
                                <Input
                                  {...field}
                                  aria-label={`PathParameters.${index}.Value`}
                                  name={`PathParameters.${index}.Value`}
                                  id={`PathParameters.${index}.Value`}
                                  data-testid={`PathParameters.${index}.Value`}
                                />
                              </FormControl>
                            )}
                          />
                        </Grid>
                        <Grid item xs={6}>
                          <Controller
                            name={`PathParameters.${index}.DefaultValue`}
                            control={control}
                            render={({ field }) => (
                              <FormControl className="col-span-3" hiddenLabel>
                                <InputLabel htmlFor={`PathParameters.${index}.DefaultValue`}>
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Default Value"
                                  />
                                  {` *`}
                                </InputLabel>
                                <Input
                                  {...field}
                                  aria-label={`PathParameters.${index}.DefaultValue`}
                                  name={`PathParameters.${index}.DefaultValue`}
                                  id={`PathParameters.${index}.DefaultValue`}
                                  data-testid={`PathParameters.${index}.DefaultValue`}
                                />
                              </FormControl>
                            )}
                          />
                        </Grid>
                      </Grid>
                    </Grid>
                    <Grid item xs={6}>
                      <Grid container direction="column" justifyContent="center" alignItems="flex-start">
                        <Grid item xs={6}>
                          <Tooltip
                            sx={{ mt: 7 }}
                            placement="top"
                            title={
                              <Typography className="font-ebs text-md">
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="Add another Path Parameter"
                                />
                              </Typography>
                            }
                            arrow
                          >
                            <Button
                              variant="text"
                              onClick={() => addPathParam({ Type: "", Value: "", Name: "" })}
                              startIcon={<Add />}
                            >
                              <FormattedMessage id="none" defaultMessage="Add" />
                            </Button>
                          </Tooltip>
                        </Grid>
                        {index != 0 && (
                          <Grid item xs={6}>
                            <Tooltip
                              placement="top"
                              title={
                                <Typography className="font-ebs text-md">
                                  <FormattedMessage
                                    id="none"
                                    defaultMessage="Remove Path Parameter"
                                  />
                                </Typography>
                              }
                              arrow
                            >
                              <Button
                                data-testid={"remove"}
                                variant="text"
                                onClick={() => removePathParam(index)}
                                startIcon={<Close />}
                              >
                                <FormattedMessage
                                  id="none"
                                  defaultMessage="Remove"
                                />
                              </Button>
                            </Tooltip>
                          </Grid>
                        )}
                      </Grid>
                    </Grid>
                  </Grid>
                </Fragment>
              ))}
            </div>
          </Grid>
        </Grid>
      </Box>
    )
  )
}