import "@testing-library/dom";
import { checkData } from "./functions";

describe("AddConnection functions - Check data ", () => {
  test("Check data => ", () => {
    const result = checkData({ HttpMethod: { name: "test" } });

    expect(result).toBe([]);
  });
});
