import AddConnection from "./AddConnection";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";

afterEach(cleanup);

test("AddConnection is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <AddConnection></AddConnection>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("AddConnectionTestId")).toBeInTheDocument();
});
