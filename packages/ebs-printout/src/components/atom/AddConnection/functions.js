import { getCoreSystemContext} from "@ebs/layout";
const { printoutUri,cbGraphqlUri } = getCoreSystemContext();
export function setDefaultValues(type, rowData, domains, setSGContextSelected) {
    let selectedOption = {}
    let URI = rowData?.uri;
    let sgContext = ""
    if (type === "PUT_DATASOURCE") {
        let uriType = rowData?.type;
        domains?.forEach(item => {
            item.domaininstances?.forEach(i => {
                if (i.sgContext.includes(uriType)) {
                    sgContext = i.sgContext;
                    URI = URI.replace("{0}",uriType === 'cbapi'? cbGraphqlUri : sgContext)
                    selectedOption = item;
                    return;
                }
            })
        });
        if (uriType === "custom") {
            selectedOption = { id: 10, name: "Custom" }
        }
        if (uriType === "csps") {
            selectedOption = { id: 11, name: "CSPS API" }
            URI = URI.replace("{0}", printoutUri.endsWith("/") ? printoutUri : printoutUri + "/" )
        }
    }
    return type === "PUT_DATASOURCE" ? {
        IsGraph: rowData?.isGraph,
        QueryParameters:
            rowData?.queryParameters?.length > 0 ?
                rowData.queryParameters.map(item => ({ Type: item.type, Name: item.name, Value: item.value, DefaultValue: item.defaultValue }))
                :
                [{ Id: 0, Type: "", Name: "", Value: "", DefaultValue: "" }],
        PathParameters: rowData?.pathParameters?.length > 0 ? rowData.pathParameters.map(item => ({ Type: item.type, Name: item.name, Value: item.value, DefaultValue: item.defaultValue })) : [{ Id: 0, Type: "", Name: "", Value: "", DefaultValue: "" }],
        IsDefault: rowData?.isDefault,
        Name: rowData?.name,
        TemplateList: rowData?.templateList ? rowData?.templateList : [],
        HttpMethod: rowData?.httpMethod === 0 ? { id: 1, name: "GET" } : { id: 2, name: "POST" },
        Uri: URI,
        ConvertQueryToBody: rowData?.convertQueryToBody,
        AllData: rowData?.allData,
        Content: rowData?.graphQL?.content,
        Entity: rowData?.graphQL?.entity,
        Type: rowData?.type,
        endpointSelector: selectedOption,
        sgContext:sgContext
    } : {
        QueryParameters: [{ Id: 0, Type: "", Name: "", Value: "", DefaultValue: "" }],
        PathParameters: [{ Id: 0, Type: "", Name: "", Value: "", DefaultValue: "" }],
        IsDefault: true,
        ConvertQueryToBody: false,
        HttpMethod: { id: 1, name: "GET" },
        AllData: true,
        Name: "",
        Uri: "",
        Content: "",
        Entity: "",
        IsGraph: false,
        SelectEndpoint: "",
        Type: "custom",
        TemplateList:[]
    }
}
export function checkData(data) {
    data.HttpMethod = data.HttpMethod.name;
    data.TemplateList = data.TemplateList?.map(item => ({ "Name": item.name }));
    if (data.IsGraph) {
        data.GraphQL = { "Entity": data.Entity, "Content": data.Content }
        data.QueryParameters = data.QueryParameters.filter(filter => filter.Name != '').map(item => ({ "Name": item.Name, "Value": item.Value, "Type": item.Value.includes("?") ? "Expression" : "String", "DefaultValue": item.DefaultValue }))
    } else {
        data.QueryParameters = data.QueryParameters.filter(filter => filter.Name != '').map(item => ({ "Name": item.Name, "Value": item.Value, "Type": item.Value.includes("?") ? "Expression" : "String", "DefaultValue": item.DefaultValue }))
        data.PathParameters = data.PathParameters.filter(filter => filter.Name != '').map(item => ({ "Name": item.Name, "Value": item.Value, "Type": item.Value.includes("?") ? "Expression" : "String", "DefaultValue": item.DefaultValue }))
    }
    data.PathParameters = data.PathParameters.filter(filter => filter.Name != '').map(item => ({ "Name": item.Name, "Value": item.Value, "Type": item.Value.includes("?") ? "Expression" : "String", "DefaultValue": item.DefaultValue }))

    return data;
}
