import React, { useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core, Icons} from "@ebs/styleguide";
const { IconButton, Tooltip, Typography, Box } = Core;
const { Edit, Delete } = Icons;
import AddConnection from "../AddConnection";
import DeleteConnection from "../DeleteConnection";
import { RBAC } from "@ebs/layout";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ConnectionRowActionAtom = React.forwardRef(({ rowData, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const [openDelete, setOpenDelete] = useState(false);
    const handleClick = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const handleCloseDelete = () => setOpenDelete(false);
    return (
        /**
         * @prop data-testid: Id to use inside ConnectionRowActions.test.js file.
         */
        <div className="content-center">
            <Box
                component="div"
                data-testid={"ConnectionRowActionTestId"}
                ref={ref}
                className="flex flex-nowrap"
            >
                <RBAC allowedAction={"Edit Connection"}>
                <Tooltip
                    arrow
                    placement="top"
                    title={
                        <Typography className="font-ebs text-xl">
                            <FormattedMessage id={"none"} defaultMessage={"Edit"} />
                        </Typography>
                    }
                >
                    <IconButton
                        size="small"
                        color="primary"
                        onClick={handleClick}
                    >
                        <Edit />
                    </IconButton>
                </Tooltip>
                </RBAC>
                <RBAC allowedAction={"Delete Connection"}>
                <Tooltip
                    arrow
                    placement="top"
                    title={
                        <Typography className="font-ebs text-xl">
                            <FormattedMessage id={"none"} defaultMessage={"Delete"} />
                        </Typography>
                    }
                >
                    <DeleteConnection
                        open={openDelete}
                        rowData={rowData}
                        refresh={refresh}
                        handleCloseDelete={handleCloseDelete}
                    />
                </Tooltip>
                </RBAC>

                <AddConnection
                    rowData={rowData}
                    open={open}
                    type="PUT_DATASOURCE"
                    refresh={refresh}
                    handleClose={handleClose}
                />

            </Box>
        </div>
    );
});
// Type and required properties
ConnectionRowActionAtom.propTypes = {
    rowData: PropTypes.object,
    refresh: PropTypes.func,
};
// Default properties
ConnectionRowActionAtom.defaultProps = {};

export default ConnectionRowActionAtom;
