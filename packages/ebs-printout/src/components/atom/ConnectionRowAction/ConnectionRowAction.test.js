import ConnectionRowAction from "./ConnectionRowAction";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";

afterEach(cleanup);

test("ConnectionRowAction is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <ConnectionRowAction></ConnectionRowAction>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("ConnectionRowActionTestId")).toBeInTheDocument();
});
