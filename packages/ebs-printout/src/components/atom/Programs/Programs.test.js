import Programs from "./Programs";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
const mockData = [
  { name: "Program 1" },
  { name: "Program 2" },
];
afterEach(cleanup);

test("Programs is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <Programs data={mockData}></Programs>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("ProgramsTestId")).toBeInTheDocument();
});
