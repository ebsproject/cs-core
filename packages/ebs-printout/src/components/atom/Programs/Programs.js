import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
const { Typography,List,ListItem } = Core;

//MAIN FUNCTION
/**
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const Programs = React.forwardRef(({ data }, ref) => {

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      resolve({ data: data, pages: 1, elements: data.length });
    });
  }

  /** 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <div className=" bg-white" data-testid={"ProgramsTestId"}>
  <List>
    <ListItem>
    <Typography className="font-ebs font-bold text-lg">
{"NAME"}
</Typography>
    </ListItem>
    {data.map((item,index) =>(
<ListItem
key= {index}
>
<Typography className="font-ebs font bold">
{`${index + 1}. ${item.name}`}
</Typography>
</ListItem>

    ))}
  </List>
</div>
  );
});

// Type and required properties
Programs.propTypes = {
  data: PropTypes.array.isRequired,
};
// Default properties
Programs.defaultProps = {};

export default Programs;
