import React from 'react'
import { Controller } from "react-hook-form";
import { Core, Lab } from "@ebs/styleguide";
const { Autocomplete } = Lab;
const { FormControl, TextField } = Core;
import {  getCoreSystemContext } from "@ebs/layout";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SelectEndpoint = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { control, setValue, errors, setEndpointSelect,setSGContextSelected,domains,isGraph} = props;
  const { printoutUri, cbGraphqlUri } = getCoreSystemContext();

  const onChangeEndPoint = (event, option) => {
    setValue('endpointSelector', option)
    setValue("AllData", true);
    if (option.name === "Custom") {
      setValue('Uri', "");
      setValue("AllData", false);
      setEndpointSelect(false);
      setValue("Type", "custom");
      return;
    }
    if (option.name === "CSPS API") {
      setValue('Uri', printoutUri);
      setValue("AllData", false);
      setEndpointSelect(false);
      setValue("Type", "csps");
      return;
    }
    const sgContext = option.domaininstances[0].sgContext;
    setSGContextSelected(sgContext);

    if (sgContext.includes("smapi")) setValue("Type", "smapi");
    if (sgContext.includes("csapi")) setValue("Type", "csapi");
    if (sgContext.includes("baapi")) setValue("Type", "baapi");

   const graphqlUri =  new URL ("graphql",sgContext).toString() 
    setValue('Uri', isGraph ? graphqlUri : sgContext)
    if (sgContext.includes("cbapi")){
      setValue("Type", "cbapi");
      setValue('Uri',cbGraphqlUri);
  } 
    if (!isGraph) {
      setEndpointSelect(false)
      return
    }
    if (sgContext.includes("cbapi") && isGraph) {
      setEndpointSelect(false)
    } else {
      setEndpointSelect(true)
    }

  }


  return (
    <FormControl fullWidth>
      <Controller
        name={`endpointSelector`}
        control={control}
        render={({ field }) => (
          <Autocomplete
            {...field}
            options={[...domains, { id: 10, name: "Custom" }, {id:11,name:"CSPS API"}]}
            getOptionLabel={(option) =>
              option.name
            }
            isOptionEqualToValue={(option, value) => {
              option.name === value.name
            }
            }
            renderInput={(params) => (
              <TextField
                {...params}
                error={Boolean(errors["endpointSelector"])}
                label="Choose an Endpoint"
              />

            )}
            onChange={onChangeEndPoint}
          />
        )}
        defaultValue={{ id: 10, name: "Custom" }}
      />
    </FormControl>
  )
})
// Type and required properties
SelectEndpoint.propTypes = {

}
// Default properties
SelectEndpoint.defaultProps = {

}

export default SelectEndpoint
