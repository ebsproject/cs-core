import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useDispatch, useSelector, useStore } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { EbsDialog, Core, Icons } from "@ebs/styleguide";
const {
  DialogContent,
  Typography,
  DialogActions,
  Button,
  IconButton,
  Tooltip,
  Box,
} = Core;
 const { Delete } = Icons;
import { deleteReportAPI } from "store/modules/Printout";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @prop {func} refresh
 * @param ref: reference made by React.forward
 */
const DeleteConnectionAtom = React.forwardRef(
  ({ refresh, rowData, handleCloseDelete }, ref) => {
    const { state } = useSelector(({ printout }) => printout);
    const [open, setOpen] = useState(false);
    const { getState } = useStore();
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    
      const resetClose = ()=>{
        handleCloseDelete(false);
        refresh();
      }
    const submit = async() => { 
        let connectionName = rowData.name;
        await deleteReportAPI(connectionName)(dispatch);
        resetClose();
        refresh();
    };
    return (
      /**
       * @prop data-testid: Id to use inside DeleteContact.test.js file.
       */
      <Box data-testid={"DeleteConnectionTestId"} ref={ref}>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-xl">
             
                <FormattedMessage id="none" defaultMessage="Delete Connection" />
              
            </Typography>
          }
          className="border-red-700"
        >
          <IconButton
          size="small"
            onClick={handleClickOpen}
            aria-label="delete-connection"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={

              <FormattedMessage id="none" defaultMessage="Delete Connection" />
          }
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this connection permanently?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}
              className="bg-ebs-brand-900 rounded-md text-white p-3 m-2 hover:bg-ebs-gray-default"
            >
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id="none" defaultMessage="Delete" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </Box>
    );
  }
);
// Type and required properties
DeleteConnectionAtom.propTypes = {
};
// Default properties
DeleteConnectionAtom.defaultProps = {
};

export default DeleteConnectionAtom;
