import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { useDispatch, useSelector } from "react-redux";
import {Core, EbsDialog, Icons} from "@ebs/styleguide";
const {
  IconButton,
  Button,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Tooltip,
  Typography,
} = Core;
import { mutationReport } from "store/modules/Printout";
const { Cancel, CheckCircle, Delete } = Icons;

//MAIN FUNCTION
/**
 * @prop {Array} selectedRows
 * @param ref: reference made by React.forward
 */
const DeleteReportAtom = React.forwardRef(({ selectedRows, refresh }, ref) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const { state } = useSelector(({ printout }) => printout);
  const handleClickOpen = () => setOpen(true);
  const handleClickClose = () => setOpen(false);
  const resetClose = ()=>{
    handleClickClose();
    refresh();
    
  }
  const handleDelete = async () => {
      await mutationReport({ type: "DELETE", data: selectedRows })(dispatch);
      resetClose();
  };
  return (
    /**
     * @prop data-testid: Id to use inside DeleteReport.test.js file.
     */
    <div data-testid={"DeleteReportTestId"} ref={ref} className="ml-3">
      <Tooltip
        arrow
        placement="bottom"
        title={
          <Typography className="font-ebs text-lg">
            <FormattedMessage id={"none"} defaultMessage={"Delete Report"} />
          </Typography>
        }
        className="border-red-700"
      >
    
        <IconButton
            onClick={handleClickOpen}
            aria-label="delete-contact"
            color="primary"
            size="small"
          >
            <Delete />
          </IconButton>


      </Tooltip>
      <EbsDialog
        fullWidth
        keepMounted
        maxWidth={"sm"}
        open={open}
        handleClose={handleClickClose}
      >
        <DialogTitle>
          <FormattedMessage id={"none"} defaultMessage={"Delete Printout template"} />
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
          <FormattedMessage
                id={"none"}
                defaultMessage={`You are about to permanently delete printout temple ${selectedRows.name}. Click OK to proceed.`}
              />
          </DialogContentText>
        </DialogContent>
        <DialogActions className="pr-8">
          <Button
            onClick={handleClickClose}
            startIcon={<Cancel />}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            onClick={ handleDelete }
            startIcon={<CheckCircle />}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Ok"} />
          </Button>
        </DialogActions>
      </EbsDialog>
    </div>
  );
});
// Type and required properties
DeleteReportAtom.propTypes = {
  selectedRows: PropTypes.object,
  refresh: PropTypes.func.isRequired,
};
// Default properties
DeleteReportAtom.defaultProps = {};

export default DeleteReportAtom;
