import DeleteReport from './DeleteReport';
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";

afterEach(cleanup);

test("DeleteReport is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <DeleteReport></DeleteReport>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("DeleteReportTestId")).toBeInTheDocument();
});
