import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import PrintRounded from "@mui/icons-material/PrintRounded";
import Visibility from "@mui/icons-material/Visibility";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import Delete from  "@mui/icons-material/Delete";
import Cancel from "@mui/icons-material/Cancel";
import CheckCircle from "@mui/icons-material/CheckCircle";
import {
  Autocomplete,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Button,
  ButtonGroup,
  CircularProgress,
  ClickAwayListener,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  Grow,
  IconButton,
  InputLabel,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Select,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  List,
  ListItem,
  LinearProgress
} from "@mui/material";

export const Core = {
  Autocomplete,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Button,
  ButtonGroup,
  CircularProgress,
  ClickAwayListener,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  Grow,
  IconButton,
  InputLabel,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Select,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  EbsDialog : Dialog,
  List,
  ListItem,
  LinearProgress
};

export const Lab = { Autocomplete };

export const Styles = {useTheme:()=>{ return{spacing:()=>{}}}};
export const Icons = {
  ExpandLess,
  ExpandMore,
  PrintRounded,
  Visibility,
  ArrowDropDown,
  Delete,
  Cancel,
  CheckCircle
};
export const Colors = {};
export const EBSMaterialUIProvider = ({ children }) => <>{children}</>;
const test = () => {
  return (
    <div>
      <Box></Box>
    </div>
  );
};
