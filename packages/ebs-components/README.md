a# **Ebs Components**

This project is by all domains and projects to use Core Components as EbsGrid and EbsForm.

- [Considerations](#considerations)
- [Building tools](#building-tools)
- [Local Development](#local-development)
  - [Run from code](#run-from-code)
- [EbsGrid](#ebsgrid)
  - [Characteristics](#characteristics)
  - [Preference](#preference)
  - [Implementing features](#implementing-features)
    - [Basic implementation](#basic-implementation)
    - [Custom cells](#custom-cells)
    - [Custom filters](#custom-filters)
    - [Row actions](#row-actions)
    - [Toolbar actions](#toolbar-actions)
    - [Custom select](#custom-select)
    - [Custom fetch](#custom-fetch)
    - [How to manage data, pagination, filtering and sorting with a custom fetch?](#how-to-manage-data-pagination-filtering-and-sorting-with-a-custom-fetch)
      - [Setting fetch up](#setting-fetch-up)
    - [Master detail](#master-detail)
  - [EbsGrid API](#ebsgrid-api)
    - [Columns definition](#columns-definition)
- [EbsForm](#ebsform)
  - [Implementing EbsFrom](#implementing-ebsform)
  - [Properties](#properties-ebsform)
  - [Components API](#API-ebsform)
  - [Apply validations](#validations-ebsform)
- [Message notification](#message-notification)
  - [Button](#button-message-notification)
  - [Hook](#hook-message-notification)
- [Build](#build)
- [Unit testing and coverage](#unit-testing-and-coverage)
- [Using the official dev environment](#using-the-official-dev-environment)
  - [Steps](#steps)
- [Helper](#helper)

## Considerations

<a name="considerations"></a>

- IMPORTANT: If you are are going to use local environment, please review first ebs-root documentation.
- IMPORTANT: npm version 8 or higher installed.
- IMPORTANT: node version 16 or higher installed.
- node_modules already installed.

## Building tools

<a name="building-tools"></a>

| dev environment | tool |
| --------------- | ---- |
| Windows         | npm  |
| linux           | npm  |
| \*nix           | npm  |

## Local Development

<a name="local-development"></a>

### Run from code

<a name="run-from-code"></a>

How to compile and run local project for local development.

    #npm start -- --port <port>

When log displays something like this, it means the project is running:

    webpack 5.57.1 compiled successfully in 4047 ms

This project will be deployed in `http://localhost:{port}/ebs-components.js`, so you need to replace this url into the import map from the index.ejs file of ebs-root project.

## EbsGrid

<a name="ebsgrid"></a>

This component is able to build from basic tables until master detail tables dynamically and less code. Currently, this component is able to handle EBS graphql standard and BrApi standard, but if you are handling different standard, you can populate this component with your own custom API call.

### Characteristics

<a name="characteristics"></a>

| Functionality    | Description                                                                                                                                                                      |
| ---------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Filtering        | It automatically handled by EbsGrid and apply directly in the API to database, but the developer is be able to replace it by other one.                                          |
| Sorting          | It automatically handled by EbsGrid and apply directly in the API to database, in case developer is using a custom API call, the developer is responsible to apply this feature. |
| Grouping         | User can group multiple columns, Developer is be able to disable this feature on every column.                                                                                   |
| Exporting        | By default EbsGrid exports files to CSV format.                                                                                                                                  |
| User Preferences | User is able to customize EbsGrid columns (Improving feature).                                                                                                                   |
| Columns resizing | User is able to change column visual size. By default this feature is enabled but, the developer is able to disable this feature for specific columns.                           |
| Master Detail    | Developer is be able to inject a node as detail component for every row displayed.                                                                                               |
| Row Actions      | Developer is able to inject custom buttons as actions for every row. This kind of actions can be added as a function or a react node.                                            |
| Toolbar Actions  | As row actions feature, developer is able to inject global action buttons in the tool bar. This kind of actions can be added as a function or a react node.                      |
| Custom Cell      | Developer can replace default EbsGrid cell rendered by a custom component for every cell.                                                                                        |
| Selectable       | EbsGrid handle two kind of select: single and multi but, developer is able to inject another kind of select if it's necessary to handle more business rules.                     |
| styleStatic      | Object to change the style of EBS-Grid you can see the [helper](#ebs-grid-helper) to get more support                                                                            |

### Preference

<a name="preference"></a>

This section describes the necessary properties to be able to save the Ebsgrid columns in the user Preference.

We need to obligatorily place an id in each EbsGrid that we use, otherwise we cannot save the columns in the user Profile

For example:

```
    <EbsGrid
      id="Domains"
      data-testid="DomainsTestId"
      toolbar={true}
      columns={columns}
      uri={graphqlUri}
      entity="Domain"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="List" />
        </Typography>
      }
      toolbaractions={toolbarActions}
      detailcomponent={ProductDetail}
      callstandard="graphql"
      select="multi"
      height="60vh"
    />
```

> Important: The id must be unique, it is recommended to name it according to the product or component you are working on.If there are two equal ids, we will have conflicts when saving the columns.

If the EbsGrid does not contain id, it will show an error with the following message:

    "The grid needs a necessary identification, to be able to save the preference columns in the database, assign the property to the corresponding grid, example: id: value. Warning: the id value must be unique in each grid, there cannot be two grids with the same value in the id property."

and you will not be able to save the columns.

Saving the EbsGrid columns allows the user to choose which columns he considers most important to display for him, so the next time he logs in and enters a grid, he will see the columns he chose, and also be able to modify them.

### Implementing features

<a name="implementing-features"></a>

This section describes how to implement different functionalities of EbsGrid.

> In these examples `findUserList` API is implemented.

#### Basic implementation

<a name="implementing-features"></a>

The basic way to implement EbsGrid is using EBS graphql standard.

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Typography } from "@material-ui/core";

const Users = React.forwardRef(({},ref)=>{
    const columns = [
            { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
            {
                Header:(
                    <Typography variant="h5" color="primary">
                        <FormattedMessage id="none" defaultMessage="User" />
                    </Typography>
                ),
                accessor: "userName",
                csvHeader: "User",
                width: 300,
            },
            {
                Header:(
                    <Typography variant="h5" color="primary">
                        <FormattedMessage id="none" defaultMessage="Title" />
                    </Typography>
                ),
                accessor: "contact.person.jobTitle",
                csvHeader: "Title",
                width: 400,
            }
    ];

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            csvfilename="userList"
            callstandard="graphql"
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

![basic implementation](/assets/basic.png)

#### Custom Cells

<a name="custom-cells"></a>

To implement this functionality, you must to keep in mind the grouping functionality, since it could cause discrepancies in the rendering of the data.

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Typography } from "@material-ui/core";

const Users = React.forwardRef(({},ref)=>{
    const columns = [
            { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
            {
                Header:(
                    <Typography variant="h5" color="primary">
                        <FormattedMessage id="none" defaultMessage="User" />
                    </Typography>
                ),
                Cell: ({ cell, value }) => {
                    if (cell.isAggregated > 0) {
                    return <></>;
                    } else {
                    return (
                        <Typography className="font-ebs text-ebs-green-default">
                        {value}
                        </Typography>
                    );
                    }
                },
                accessor: "userName",
                csvHeader: "User",
                width: 300,
            },
            {
                Header:(
                    <Typography variant="h5" color="primary">
                        <FormattedMessage id="none" defaultMessage="Title" />
                    </Typography>
                ),
                accessor: "contact.person.jobTitle",
                csvHeader: "Title",
                width: 400,
            }
    ];

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            csvfilename="userList"
            callstandard="graphql"
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

![custom cells](/assets/customCell.png)

#### Custom filters

<a name="custom-filters"></a>

To implement this functionality, you must to keep in mind your filter will be outside of EbsGrid internal logic so is your responsibility applying this filter correctly. More about this functionality on [react table](https://react-table.tanstack.com/docs/examples/filtering).

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Typography, TextField } from "@material-ui/core";

const Users = React.forwardRef(({},ref)=>{

    function DefaultColumnFilter({
        column: { filterValue, preFilteredRows, setFilter },
    }) {

        return (
        <TextField
        fullWidth
            value={filterValue || ""}
            onChange={(e) => {
            setFilter(e.target.value || undefined); // Set undefined to remove the filter entirely
            }}
            placeholder={`Search on this column...`}
        />
        );
    };

    const columns = [
            { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
            {
                Header:(
                    <Typography variant="h5" color="primary">
                        <FormattedMessage id="none" defaultMessage="User" />
                    </Typography>
                ),
                Cell: ({ cell, value }) => {
                    if (cell.isAggregated > 0) {
                    return <></>;
                    } else {
                    return (
                        <Typography className="font-ebs text-ebs-green-default">
                        {value}
                        </Typography>
                    );
                    }
                },
                Filter: DefaultColumnFilter,
                accessor: "userName",
                csvHeader: "User",
                width: 300,
            },
            {
                Header:(
                    <Typography variant="h5" color="primary">
                        <FormattedMessage id="none" defaultMessage="Title" />
                    </Typography>
                ),
                accessor: "contact.person.jobTitle",
                csvHeader: "Title",
                width: 400,
            }
    ];

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            csvfilename="userList"
            callstandard="graphql"
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

![filtering](/assets/filtering.png)

#### Row actions

<a name="row-actions"></a>

This implementation is able to handle a function or a new React node as parameter, the different implementations are described below.

**_As function parameter_**

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Typography, Box, IconButton } from "@material-ui/core";
import { Edit } from "@material-ui/icons";

const Users = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="User" />
            </Typography>
        ),
        accessor: "userName",
        csvHeader: "User",
        width: 300,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Full Name" />
            </Typography>
        ),
        Cell: ({ row }) => (
            <Typography variant="body1">
            {`${row.values["contact.person.givenName"]} ${row.values["contact.person.familyName"]}`}
            </Typography>
        ),
        accessor: "contact.person.givenName",
        csvHeader: "Full Name",
        width: 400,
        disableGlobalFilter: true,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Title" />
            </Typography>
        ),
        accessor: "contact.person.jobTitle",
        csvHeader: "Title",
        width: 400,
        disableGlobalFilter: true,
        },
    ];

    /*
     * @param rowData: Object with row data.
     * @param refresh: Function to refresh Grid data.
     */
    const rowActions = (rowData, refresh) => {
        return (
        <Box>
            <IconButton className="text-green-600">
                <Edit/>
            </IconButton>
        </Box>
        );
    };

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            rowactions={rowActions}
            csvfilename="userList"
            callstandard="graphql"
            raWidth={55}
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

![rowAction function](/assets/rowActionsFP.png)

**_As React Node_**

`Users.js`

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import RowActions from "./RowActions";
import { Typography, Box, IconButton } from "@material-ui/core";
import { Edit } from "@material-ui/icons";

const Users = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="User" />
            </Typography>
        ),
        accessor: "userName",
        csvHeader: "User",
        width: 300,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Full Name" />
            </Typography>
        ),
        Cell: ({ row }) => (
            <Typography variant="body1">
            {`${row.values["contact.person.givenName"]} ${row.values["contact.person.familyName"]}`}
            </Typography>
        ),
        accessor: "contact.person.givenName",
        csvHeader: "Full Name",
        width: 400,
        disableGlobalFilter: true,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Title" />
            </Typography>
        ),
        accessor: "contact.person.jobTitle",
        csvHeader: "Title",
        width: 400,
        disableGlobalFilter: true,
        },
    ];

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            rowactions={RowActions}
            csvfilename="userList"
            callstandard="graphql"
            raWidth={55}
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

`RowActions.js`

```
import React, { memo } from "react";
import { Box, IconButton } from "@material-ui/core";
import { Edit } from "@material-ui/icons";

const RowActions = React.forwardRef(({ rowData, refresh },ref)=>{

    return (
      <Box>
        <IconButton>
            <Edit/>
        </IconButton>
      </Box>
    );
});

export default RowActions;
```

![rowActionsRN](/assets/rowActionsFP.png)

#### Toolbar actions

<a name="toolbar-actions"></a>

This implementation is able to handle a function or a new React node as parameter, the different implementations are described below.

**_As function parameter_**

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Typography, Box, Button } from "@material-ui/core";
import { Edit } from "@material-ui/icons";

const Users = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="User" />
            </Typography>
        ),
        accessor: "userName",
        csvHeader: "User",
        width: 300,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Full Name" />
            </Typography>
        ),
        Cell: ({ row }) => (
            <Typography variant="body1">
            {`${row.values["contact.person.givenName"]} ${row.values["contact.person.familyName"]}`}
            </Typography>
        ),
        accessor: "contact.person.givenName",
        csvHeader: "Full Name",
        width: 400,
        disableGlobalFilter: true,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Title" />
            </Typography>
        ),
        accessor: "contact.person.jobTitle",
        csvHeader: "Title",
        width: 400,
        disableGlobalFilter: true,
        },
    ];

    /**
     * @param dataSelection: Array object with data rows selected.
     * @param refresh: Function to refresh Grid data.
     */
    const toolbarActions = (selectedRows, refresh) => {
        return (
        <Box className="ml-3">
            <Button className="text-white bg-ebs-green-default hover:bg-ebs-green-900">
                <FormattedMessage id={"none"} defaultMessage={"New"} />
            </Button>
        </Box>
        );
    };

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            toolbaractions={toolbarActions}
            csvfilename="userList"
            callstandard="graphql"
            raWidth={55}
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

![rowAction function](/assets/toolbarActions.png)
**_As React Node_**

`Users.js`

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import ToolbarActions from "./ToolbarActions";
import { Typography, Box, IconButton } from "@material-ui/core";
import { Edit } from "@material-ui/icons";

const Users = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="User" />
            </Typography>
        ),
        accessor: "userName",
        csvHeader: "User",
        width: 300,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Full Name" />
            </Typography>
        ),
        Cell: ({ row }) => (
            <Typography variant="body1">
            {`${row.values["contact.person.givenName"]} ${row.values["contact.person.familyName"]}`}
            </Typography>
        ),
        accessor: "contact.person.givenName",
        csvHeader: "Full Name",
        width: 400,
        disableGlobalFilter: true,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Title" />
            </Typography>
        ),
        accessor: "contact.person.jobTitle",
        csvHeader: "Title",
        width: 400,
        disableGlobalFilter: true,
        },
    ];

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            toolbaractions={ToolbarActions}
            csvfilename="userList"
            callstandard="graphql"
            raWidth={55}
            select="single"
            height="40vh"
        />
    );
});

export default memo(Users);
```

`ToolbarActions.js`

```
import React, { memo } from "react";
import { FormattedMessage } from "react-intl";
import { Box, Button } from "@material-ui/core";

const ToolbarActions = React.forwardRef(({ selectedRows, refresh },ref)=>{

    return (
      <Box className="ml-3">
            <Button className="text-white bg-ebs-green-default hover:bg-ebs-green-900">
                <FormattedMessage id={"none"} defaultMessage={"New"} />
            </Button>
        </Box>
    );
});

export default ToolbarActions;
```

![rowActionsRN](/assets/toolbarActions.png)

#### Custom select

<a name="custom-select"></a>

The implementation of this functionality is the developer responsibility completely and must to apply it correctly if it's necessary. More information about this implementation on [React Table](https://react-table.tanstack.com/docs/examples/row-selection)

> This implementation accepts a React Node only.

`Users.js`

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import CustomSelect from "./CustomSelect";
import { Typography } from "@material-ui/core";
import { Edit } from "@material-ui/icons";

const Users = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="User" />
            </Typography>
        ),
        accessor: "userName",
        csvHeader: "User",
        width: 300,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Full Name" />
            </Typography>
        ),
        Cell: ({ row }) => (
            <Typography variant="body1">
            {`${row.values["contact.person.givenName"]} ${row.values["contact.person.familyName"]}`}
            </Typography>
        ),
        accessor: "contact.person.givenName",
        csvHeader: "Full Name",
        width: 400,
        disableGlobalFilter: true,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Title" />
            </Typography>
        ),
        accessor: "contact.person.jobTitle",
        csvHeader: "Title",
        width: 400,
        disableGlobalFilter: true,
        },
    ];

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="User"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"Users"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            select={CustomSelect}
            csvfilename="userList"
            callstandard="graphql"
            raWidth={55}
            height="40vh"
        />
    );
});

export default memo(Users);
```

`CustomSelect.js`

```
import React, { memo } from "react";
import { Checkbox } from "@material-ui/core";

const CustomSelect = React.forwardRef(({ rows, row }, ref) => {
  const type = (select) => {
    switch (select) {
      case "multi":
        return <Checkbox {...row.getToggleRowSelectedProps()} />;
      case "single":
        if (rows.filter((row) => row.isSelected).length < 1 || row.isSelected) {
          return <Checkbox {...row.getToggleRowSelectedProps()} />;
        } else {
          return (
            <Checkbox
              checked={false}
              readOnly
              style={row.getToggleRowSelectedProps().style}
            />
          );
        }
    }
  };
  return type("single");
});

export default CustomSelect;
```

![rowActionsRN](/assets/CustomSelect.png)

#### Custom fetch

<a name="custom-fetch"></a>

This functionality is necessary when developer needs to populate EbsGrid with an API call different to EBS API standard.

> The correct pagination, filtering and sorting implementation is developer responsibility. Make sure to apply this functionalities as well.

> `findContactList` API is used to describe this example

`People.js`

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { client } from "utils/apollo";
import { FormattedMessage } from "react-intl";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";
import { Typography } from "@material-ui/core";

const People = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Family Name" />
            </Typography>
        ),
        accessor: "person.familyName",
        csvHeader: "Family Name",
        width: 500,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Given Name" />
            </Typography>
        ),
        accessor: "person.givenName",
        csvHeader: "Given Name",
        width: 500,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Additional name" />
            </Typography>
        ),
        accessor: "person.additionalName",
        csvHeader: "Additional Name",
        width: 500,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Job Title" />
            </Typography>
        ),
        accessor: "person.jobTitle",
        csvHeader: "Job Title",
        width: 500,
        },
    ];

    const fetch = async ({ page, sort, filters }) => {
        return new Promise((resolve, reject) => {
        try {
            client
            .query({
                query: FIND_CONTACT_LIST,
                variables: {
                page: page,
                sort: sort,
                filters: [
                    ...filters,
                    { col: "category.name", mod: "EQ", val: "Person" },
                ],
                disjunctionFilters: filters.length > 0 ? true : false,
                },
                fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
                if (errors) {
                reject(errors);
                } else {
                resolve({
                    pages: data.findContactList.totalPages,
                    elements: data.findContactList.totalElements,
                    data: data.findContactList.content,
                });
                }
            });
        } catch (e) {
            console.log(e);
        } finally {
            console.log("finally people");
        }
        });
    };

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"People"} />
                </Typography>
            }
            csvfilename="peopleList"
            fetch={fetch}
            height="40vh"
        />
    );
});

export default memo(People);
```

![rowActionsRN](/assets/customFetch.png)

#### How to manage data, pagination, filtering and sorting with a custom fetch?

<a name="how-to-manage-data-pagination-filtering-and-sorting-with-a-custom-fetch"></a>

**_Parameters_**

`page`: Object

- This parameter contains pagination information, The default format for this object is:

```
{
    number: Int,
    size: Int,
}
```

`sort`: Array

- This parameter contains sorting information, The default format for this parameter is:

> Every sort applied is defined by every object

```
[
    { col: String, mod: String  }
]
```

`filters`: Array

- This parameter contains filtering information, The default format for this parameter is:

> Every filter applied is defined by every object, including Global filter.

```
[
    { col: String, mod: "LK", val: String }
]
```

##### Setting fetch up

<a name="setting-fetch-up"></a>

The fetch function must to return an asynchronous Promise and include total pages number, total elements in the data base and data collection to be displayed by EbsGrid.

> This example explains how to apply a custom fetch correctly using a Graphql API

```
const fetch = ({ page, sort, filters })=> {
    return new Promise((resolve, reject) => {
        try{
            client
                .query({
                    query: YOUR_QUERY,
                    variables: {    // variables described here depends on your query definition.
                        page: page,
                        sort: sort,
                        filters: filters,
                    },
                })
                .then(({ data, errors }) => {
                    if(errors){
                        reject(errors)
                    }else{
                        resolve({   // pages, elements and data are required values.
                            pages: data.totalPages,
                            elements: data.totalElements,
                            data: data
                        });
                    }
                });
        }catch(error){
            reject(errors)
        }
    });
};
```

#### Master detail

<a name="master-detail"></a>

The developer is be able to inject a React Node inside every row of EbsGrid.

> `findDomainList` API is used to describe this example.

`Domains.js`

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import ProductList from "./ProductList";
import { FormattedMessage } from "react-intl";
import { Typography } from "@material-ui/core";

const Domains = React.forwardRef(({},ref)=>{

    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Name" />
            </Typography>
        ),
        accessor: "name",
        csvHeader: "Name",
        filter: true,
        width: 450,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Description" />
            </Typography>
        ),
        accessor: "info",
        csvHeader: "Description",
        filter: true,
        width: 550,
        },
        {
        Header: (
            <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Icon" />
            </Typography>
        ),
        accessor: "icon",
        csvHeader: "Icon",
        disableFilters: true,
        disableGlobalFilter: true,
        width: 450,
        },
    ];

    const ProductDetail = ({ rowData }) => {
        return <ProductList rowData={rowData} />;
    };

    return (
        <EbsGrid
            toolbar={true}
            columns={columns}
            entity="Domain"
            title={
                <Typography className="font-ebs text-ebs-green-default text-lg" >
                    <FormattedMessage id={"none"} defaultMessage={"List"} />
                </Typography>
            }
            uri={"http://localhost:8080/graphql"}
            csvfilename="peopleList"
            callstandard="graphql"
            detailcomponent={ProductDetail}
            height="40vh"
        />
    );
});

export default memo(Domains);
```

`ProductList.js`

```
import React, { memo } from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { FIND_DOMAIN } from "utils/apollo/gql/tenantManagement";
import { client } from "utils/apollo";
import ToolbarActions from "./ToolbarActions";
import { Typography, Box } from "@material-ui/core";

const Products = React.forwardRef(({ rowData },ref)=>{

    const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Product" />
        </Typography>
      ),
      csvHeader: "Product",
      accessor: "name",
      filter: true,
      width: 750,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Menu Order" />
        </Typography>
      ),
      csvHeader: "Menu Order",
      accessor: "menuOrder",
      filter: true,
      width: 750,
    },
    { Header: "Icon", accessor: "icon", hidden: true },
    { Header: "Path", accessor: "path", hidden: true },
    {
      Header: "Description",
      accessor: "description",
      hidden: true,
    },
    { Header: "Help", accessor: "help", hidden: true },
  ];

  async function fetch({ page, sort, filters }) {
    return new Promise((resolve, reject) => {
      client
        .query({
          query: FIND_DOMAIN,
          variables: { id: rowData.id },
          fetchPolicy: "no-cache",
        })
        .then(({ data }) => {
          const newData = data.findDomain.products
            .slice()
            .sort((a, b) => a.menuOrder - b.menuOrder);
          resolve({ data: newData, pages: 1, elements: newData.length });
        })
        .catch((error) => {
          reject(error);
        });
    });
  }

    return (
        <EbsGrid
            columns={columns}
            fetch={fetch}
            toolbar={false}
            height="20vh"
        />
    );
});

export default memo(Products);
```

![rowActionsRN](/assets/masterDetail.png)

### EbsGrid API

<a name="ebsgrid-api"></a>

| Prop            | Type        | Default               | Description                                                                                                    |
| --------------- | ----------- | --------------------- | -------------------------------------------------------------------------------------------------------------- |
| uri             | String      | ""                    | API endpoint.                                                                                                  |
| title           | Node        | <></>                 | Table title.                                                                                                   |
| select          | String/node | none                  | Selectable type.                                                                                               |
| Entity          | String      | ""                    | Entity name, in `findUserList` API `User` is the entity name.                                                  |
| columns         | Array       | none                  | Columns definition, more about this definition structure on [columns definition](#columns-definition) section. |
| raWidth         | Number      | children.length \* 55 | Actions column width.                                                                                          |
| toolbar         | Boolean     | true                  | Displays toolbar.                                                                                              |
| indexing        | Boolean     | false                 | Displays indexing column.                                                                                      |
| rowactions      | fn/node     | none                  | Actions section by row.                                                                                        |
| callstandard    | String      | none                  | `"graphql"` or `"brapi"`.                                                                                      |
| defaultFilters  | Array       | none                  | Default filters applied to every API call. This filters are only bear by graphql standard.                     |
| toolbaractions  | fn/node     | none                  | Global actions displayed on toolbar section.                                                                   |
| detailcomponent | fn          | none                  | Rendered component inside every row as a master detail functionality.                                          |
| globalFilter    | Boolean     | true                  | Displays global filtering.                                                                                     |

#### Columns definition

<a name="columns-definition"></a>

The following options are supported on any column object you can pass to columns.

`accessor`: String | Function(originalRow, rowIndex) => any

- Required

- This string/function is used to build the data model for your column.

- The data returned by an accessor should be primitive and sortable.

- If a string is passed, the column's value will be looked up on the original row via that key, eg. If your column's accessor is firstName then its value would be read from row['firstName']. You can also specify deeply nested values with accessors like info.hobbies or even address[0].street

- If a function is passed, the column's value will be looked up on the original row using this accessor function, eg. If your column's accessor is row => row.firstName, then its value would be determined by passing the row to this function and using the resulting value.

- Technically speaking, this field isn't required if you have a unique id for a column. This is used for things like expander or row selection columns. Warning: Only omit accessor if you really know what you're doing.

`id`: String

- Required if accessor is a function

- This is the unique ID for the column. It is used by reference in things like sorting, grouping, filtering etc.

- If a string accessor is used, it defaults as the column ID, but can be overridden if necessary.

`columns`: Array<Column>

- Optional

- A nested array of columns.

- If defined, the column will act as a header group. Columns can be recursively nested as much as needed.

`Header`: String | Function | React.Component => JSX

- Optional

- Defaults to () => null

- Receives the table instance and column model as props

- Must either be a string or return valid JSX

- If a function/component is passed, it will be used for formatting the header value, eg. You can use a Header function to dynamically format the header using any table or column state.

`Footer`: String | Function | React.Component => JSX

- Optional

- Defaults to () => null

- Receives the table instance and column model as props

- Must either be a string or return valid JSX

- If a function/component is passed, it will be used for formatting the footer value, eg. You can use a Footer function to dynamically format the footer using any table or column state.

`Cell`: Function | React.Component => JSX

- Optional

- Defaults to ({ value }) => String(value)

- Receives the table instance and cell model as props

- Must return valid JSX

- This function (or component) is primarily used for formatting the column value, eg. If your column accessor returns a date object, you can use a Cell function to format that date to a readable format.

`width`: Int

- Optional

- Defaults to 150

- Specifies the width for the column (when using non-table-element layouts)

`csvHeader`: String

- required for exporting functionality.

- Specifies the header for this column in CSV file exported.

`disableResizing`: Boolean

- optional

- Defaults to true

- Disable/Enable resizing functionality on this column

`sticky`: String

- optional

- Stick column on `left` or `right` position.

## EbsForm

<a name="ebsform"></a>

This component is able to build basic forms dynamically and less code.

### Implementing EbsForm

<a name="implementing-ebsform"></a>

This section describes how to implement EbsForm.

```
import React from "react";
import { Button } from "@material-ui/core";
import { EbsForm } from "@ebs/components";

const definition = (props) => {
    const { getValues, setValue, reset } = props;
    return {
      name: "myForm",
      components: [
        {
          sizes: [12, 6, 4, 3, 2],
          component: "TextField",
          name: "TextField",
          inputProps: {
            label: "textfield",
          },
          rules: { required: "It's required" },
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "CheckBox",
          name: "checkbox",
          title: "Check Box Group",
          options: [{ label: "Hello" }, { label: "World" }],
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "DatePicker",
          name: "datepicker",
          inputProps: {
            label: "DatePicker",
          },
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "Radio",
          name: "radiogroup",
          label: "Radio Group",
          row: false,
          options: [
            { label: "Uno", value: 1, disabled: true },
            { label: "Dos", value: 2, color: "primary" },
          ],
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "Select",
          name: "select",
          options: users,
          inputProps: {
            label: "Hello",
            multiple: true,
            disabled: false,
          },
          defaultValue: [{ label: "Uno", value: 1 }],
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "Switch",
          name: "switch",
          label: "Switch",
          defaultValue: true,
          inputProps: { disabled: true, color: "primary" },
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "Button",
          name: "button",
          label: "Button",
          buttonProps: {
            color: "primary",
            variant: "outlined",
          },
        },
        {
          sizes: [12, 6, 4, 3, 2],
          component: "File",
          name: "file",
          label: "Choose a file...",
          customProps: {
            button: {
              color: "primary",
              variant: "outlined",
              size: "small",
            },
            input: {
                acceptedFiles: [".csv"],
                cancelButtonText: "cancel",
                submitButtonText: "submit",
                maxFileSize: 5000000,
                showPreviews: true,
                showFileNamesInPreview: true,
              },
        },
      ],
    };
  };

const onSubmit = (data) => {
  console.log(data);
};

const Demo = (props) => {

  return (
    <div>
      <EbsForm definition={definition} onSubmit={onSubmit} actionPlacement="top">
        <Button type="submit" variant="contained" color="primary">
          Submit
        </Button>
      </EbsForm>
    </div>
  );
};
```

### Properties

<a name="properties-ebsform"></a>

| Name            | Type           | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| --------------- | -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| definition      | _func_         | Function to defines the Form structure Signature: function({ _getValues_: `func`, _setValue_: `func`, _reset_: `func` }) => `object`. `getValues:` This function allows you to dynamically get the values of the form. `setValue:` This function allows you to dynamically set the value of a component. `reset:` Reset the fields' values and errors, you have the freedom to only reset specific component values. You can pass values as an optional argument to reset your form to the assigned default values |
| onSubmit        | _func_         | Function to handle form data                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| children\*      | _node_         | Nodes to show under the form                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| actionPlacement | _top_/_bottom_ | Placement to display form actions                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

### getValues: _(payload?: string | string[]) => Object_

1. getValues(): Read all form values.
2. getValues('test'): Read an individual field value by name.
3. getValues(['test', 'test1']): Read multiple fields by name.

### setValues: _(name: string, value: any, config?: Object) => void_

1. setValue('name', 'value'): Set a component value.
2. You can also set the shouldValidate parameter to true in order to trigger a field validation.
   - setValue('name', 'value', { shouldValidate: true })

### reset: _(values?: Record<string, any>, omitResetState?: Record<string, boolean>) => void_

When invoking reset({ value }) without supply defaultValues, the form will replace defaultValues with shallow clone value object which you have supplied

## Components API

<a name="API-ebsform"></a>

### Helper Attributes

1. arrow: _bool_ => If true, adds an arrow to the helper.
2. classes: _object_ => Override or extend the styles applied to the component.
3. placement: _'bottom-end'| 'bottom-start'| 'bottom'| 'left-end'| 'left-start'| 'left'| 'right-end'| 'right-start'| 'right'| 'top-end'| 'top-start'| 'top'_
4. title: _node_ => label to show

### Button

| Name        | Type     | Default                                    | Description                                                                                                                                                 |
| ----------- | -------- | ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes       | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden. |
| name        | `String` |                                            | Defines the component name.                                                                                                                                 |
| buttonProps | `Object` |                                            | Attributes applied to the element.                                                                                                                          |
| helper      | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                           |
| label       | `Node`   |                                            | Component or text to show as label.                                                                                                                         |

#### buttonProps Attributes

- classes: Override or extend the styles applied to the component
  color: 'default'| 'inherit'| 'primary'| 'secondary'
- disabled: bool
- endIcon: node
- fullWidth: bool
- size: 'large'| 'medium'| 'small'
- startIcon: node

### CheckBox

| Name       | Type     | Default                                    | Description                                                                                                                                                      |
| ---------- | -------- | ------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes      | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden.      |
| name       | `String` |                                            | Defines the component name.                                                                                                                                      |
| checkProps | `Object` |                                            | Attributes applied to all checkbox.                                                                                                                              |
| helper     | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                                |
| title      | `Node`   |                                            | Component or text to show on Top as label.                                                                                                                       |
| options    | `Array`  | _[{}]_                                     | CheckBox options, it can be only one or multiple. Signature: `{{ label: "Hello", defaultValue: false },{ label: <p>World</p>}}`                                  |
| onChange   | `Func`   |                                            | `function(event: object) => void` _event_: The event source of the callback. You can pull out the new checked state by accessing event.target.checked (boolean). |

#### checkProps Attributes

- classes: Override or extend the styles applied to the component
- color: 'default'| 'primary'| 'secondary'
- disabled: bool
- icon: node
- size: 'medium'| 'small'

### DatePicker

| Name         | Type     | Default                                    | Description                                                                                                                                                 |
| ------------ | -------- | ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes        | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden. |
| Name         | `String` |                                            | Defines the component name.                                                                                                                                 |
| inputProps   | `Object` |                                            | [Attributes](https://material-ui-pickers.dev/api/KeyboardDatePicker) applied to the element.                                                                |
| helper       | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                           |
| defaultValue | `Date`   | _new Date()_                               | Picker defaultValue                                                                                                                                         |
| onChange     | `Func`   |                                            | `function(event: object) => void` _event:_ The event source of the callback. You can pull out the date selected.                                            |
| rules        | `Object` |                                            | Rules to validate element. (see Validation section)                                                                                                         |

### Select

| Name         | Type     | Default                                    | Description                                                                                                                                                                                    |
| ------------ | -------- | ------------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes        | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden.                                    |
| name         | `String` |                                            | Defines the component name.                                                                                                                                                                    |
| inputProps   | `Object` |                                            | Attributes applied to the element.                                                                                                                                                             |
| helper       | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                                                              |
| defaultValue | `Array`  | _[{}]_                                     | Value(s) selected by default Signature: `[{ label: "One", value: 1 }]`                                                                                                                         |
| onChange     | `Func`   |                                            | `function(event: object) => void` _event:_ The event source of the callback. You can pull out the option selected.                                                                             |
| rules        | `Object` |                                            | Rules to validate element. (see Validation section)                                                                                                                                            |
| options      | `Array`  |                                            | As minimum structure for each object you must to send label and value. Signature: `[{ label: "One", value: 1, color: '#00B8D9', isFixed: true },{ label: "Two", value: 2, isDisabled: true }]` |

#### inputProps Attributes

- className: Override or extend the styles applied to the component.
  This component is based on [Autocomplete](https://v4.mui.com/api/autocomplete/#autocomplete-api) component from @material-ui/lab, if you want to override some properties, please visit the official documentation.

### RadioGroup

| Name         | Type     | Default                                    | Description                                                                                                                                                  |
| ------------ | -------- | ------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| sizes        | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden.  |
| Name         | `String` |                                            | Defines the component name.                                                                                                                                  |
| helper       | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                            |
| label        | `Node`   |                                            | Component or text to show on Top as label.                                                                                                                   |
| options      | `Array`  | _[{}]_                                     | Radio options, it can be only one or multiple. Signature: `{{ label: "One", value: 1, disabled: true },{ label: <p>World</p>, value: 2, color: "primary" }}` |
| row          | `Bool`   | _false_                                    | Defines the flex-direction style property. It is applied for all screen sizes.                                                                               |
| defaultValue | `Object` |                                            | Radio selected by default Signaure: `{ label: "Hello", value:1 }`                                                                                            |
| rules        | `Object` |                                            | Rules to validate element. (see Validation section)                                                                                                          |

### Switch

| Name         | Type     | Default                                    | Description                                                                                                                                                                                                                               |
| ------------ | -------- | ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes        | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden.                                                                               |
| Name         | `String` |                                            | Defines the component name.                                                                                                                                                                                                               |
| inputProps   | `Object` |                                            | Attributes applied to the element.                                                                                                                                                                                                        |
| helper       | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                                                                                                         |
| defaultValue | `bool`   | _false_                                    | If true, the component is checked.                                                                                                                                                                                                        |
| onChange     | `Func`   |                                            | `function(event: object) => void` _event:_ The event source of the callback. You can pull out the new value by accessing event.target.value (string). You can pull out the new checked state by accessing event.target.checked (boolean). |
| label        | `Node`   |                                            | Component or text to show on Top as label.                                                                                                                                                                                                |
| rules        | `Object` |                                            | Rules to validate element. (see Validation section)                                                                                                                                                                                       |

#### inputProps Attributes

- classes: Override or extend the styles applied to the component
- color: 'default'| 'primary'| 'secondary'
- disabled: bool
- size: 'medium'| 'small'

### TextField

| Name         | Type     | Default                                    | Description                                                                                                                                                 |
| ------------ | -------- | ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes        | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden. |
| Name         | `String` |                                            | Defines the component name.                                                                                                                                 |
| inputProps   | `Object` |                                            | Attributes applied to the element.                                                                                                                          |
| helper       | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                           |
| defaultValue | `Node`   | _""_                                       | Component or text to show as defaultValue.                                                                                                                  |
| rules        | `Object` |                                            | Rules to validate element. (see Validation section)                                                                                                         |

#### inputProps Attributes

- classes: Override or extend the styles applied to the component
- color: 'primary'| 'secondary'
- disabled: bool
- label: node
- multiline: bool
- fullWidth: bool
- size: 'medium'| 'small'
- rows: int => Number of rows to display when multiline option is set to true.

### File

| Name        | Type     | Default                                    | Definition                                                                                                                                                  |
| ----------- | -------- | ------------------------------------------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------- |
| sizes       | `Array`  | _["auto", "auto", "auto", "auto", "auto"]_ | Defines the number of grids the component is going to use. It's applied for the ["xs","sm","md","lg","xl"] breakpoints and wider screens if not overridden. |
| name        | `String` |                                            | Defines the component name.                                                                                                                                 |
| customProps | `Object` |                                            | Attributes applied to the element.                                                                                                                          |
| helper      | `Object` |                                            | Attributes applied to the helper element. (see Helper Attributes)                                                                                           |
| label       | `Node`   |                                            | Component or text to show as label.                                                                                                                         |
| rules       | `Object` |                                            | Rules to validate the element. (see Validation section)                                                                                                     |

#### customProps Attributes

- button: `object`
  - classes: Override or extend the styles applied to the component
  - color: 'default'| 'inherit'| 'primary'| 'secondary'
  - disabled: bool
  - endIcon: node
  - fullWidth: bool
  - size: 'large'| 'medium'| 'small'
  - startIcon: node
- input: `object`
  - acceptedFiles: {['image/*']}
  - cancelButtonText: {"cancel"}
  - submitButtonText: {"submit"}
  - maxFileSize: {5000000}
  - showPreviews: {true}
  - showFileNamesInPreview: {true}

## Apply Validations

<a name="validations-ebsform"></a>

List of validation rules supported:

- required
- validate

### Example

```
{
          sizes: [12, 6, 4, 3, 2],
          component: "TextField",
          name: "TextField",
          inputProps: {
            label: "textfield",
            disabled: false,
            rows: 5,
            size: "medium",
            variant: "outlined",
          },
          helper: { title: "ayuda", placement: "right", arrow: true },
          defaultValue: "Hola mundo",
          rules: {
            validate: (value) => {
              return value == "Hello world";
            },
            required: "It's required",
          },
        },
```

## Message notification

Component to use a message notification, this component is split into elements: button and hooks.

### Button message notification

Can create a simple button to send a notification when the user click on it.

```
import { ButtonWorkflow } from '@ebs/components';

<ButtonWorkflow
    className={'button'}
    id={'button_workflow'}
    jobWorkflowId={1}
    label={'Button example'}
    onFinish={({ result }) => {
        console.log('Result button:', result);
      }}
    recordId={11}
    style={{}}
/>

```

The essential props:

- jobWorkflowId : Identifier in the message notification flow (see the complete catalog of workflows for the button)
- recordId : Identifier of the application or registration that needs to be notified
  Optionals props:
- className : Class name of the component
- id : Identifier on DOM
- label : text to display on the button
- onFinish : function that receive a result object, this function run when the server response the request of notification.
  The result object contain:
  `{ status: { success: false | true, message: string }, data: object }`
- style : Object of style

### Hook message notification

The hook is the functionality the button but it don't contain any component.

```
import { useWorkflow } from '@ebs/components';

 const { sendNotification } = useWorkflow({
    jobWorkflowId: 1,
  })


  const handleClick = () => {
    const result = sendNotification({ recordId: 11, });
    console.log('Result:', result);
  }
```

The hook need to build with the jobWorkflowId, it returned a function called sendNotification, this function can report any number of recordIds.

sendNotification returned a result object with this structure.

`{ status: { success: false | true, message: string }, data: object }`

## Build

<a name="build"></a>

    #npm run build

The build folder can be found in `cs-core/packages/ebs-components/dist`

## Unit testing and coverage

<a name="unit-testing-and-coverage"></a>

This command will run all unit test

    #npm run test

This command will run coverage report

    #npm run coverage

The coverage report can be found in `cs-core/packages/ebs-components/coverage/index.html`

## Using the official dev environment

<a name="using-the-official-dev-environment"></a>

If you are going to use an Official Dev Environment, follow these steps.

### Steps:

<a name="steps"></a>

- Activate devtools, in the browser console:

  #localStorage.setItem("devtools",true);

- After refresh page, you should see the import maps icon (bottom right corner).
- Open import maps and look for `@ebs/components` module.
- Click and replace import map with your local URL.
- Apply override and refresh page.

> After apply override and refresh the page, the import maps icon should be red color

## Helper

To get more information about props or other parameters can use the helper:

```
import { useHelper } from '@ebs/components';

const helper = useHelper();
console.log('Helper:', helper);
```

With this object you can see a quick and simple example of every component on EBS-Component

### EBS-Grid helper

You can see the basic object to inject into EBS-Grid component.

- styleStatic:

> This object contains all the style attributes that you can change in EBS-Grid
>
> `styleStatic: { columnStyle: { fontSize: 14, }, rowStyle: { fontSize: 12, }, },`
