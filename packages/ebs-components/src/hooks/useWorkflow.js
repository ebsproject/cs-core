import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { getWorkflowData_jopType } from 'utils/service/coreService';
import { getCoreSystemContext } from '@ebs/layout';
import { getTokenId,userContext } from '@ebs/layout';

/*
 *
 */
export const useWorkflow = () => {
  // console.log('Log : ', getCoreSystemContext());
  const { graphqlUri, restUri } = getCoreSystemContext();
  const rootUrlServiceWorkflow =
    restUri.slice(-1) === '/' ? `${restUri}`.toString() : `${restUri}/`.toString();
  const rootUrlServiceCoreApi =
    graphqlUri.slice(-1) === '/'
      ? `${graphqlUri}`.toString()
      : `${graphqlUri}/`.toString();
  const { userProfile } = useContext(userContext);
  // TODO: To view data an inject to local machine
  // const rootUrlServiceWorkflow = `https://10.193.177.172:8290`.toString();
  // console.log('url', rootUrlServiceCoreApi, rootUrlServiceWorkflow);
  const [config, setConfig] = useState({
    submitterId: 0,
    products: [],
  });

  useEffect(() => {
    const getRequest = async ({
      isMounted,
      rootUrlServiceCoreApi,
      rootUrlServiceWorkflow,
      submitterId,
    }) => {
      // TODO: Need this values from database
      const result = await getWorkflowData_jopType({
        jopTypeId: 1,
        uri: rootUrlServiceCoreApi,
      });
      if (isMounted && result.status.success)
        setConfig((prev) => ({
          ...prev,
          products: result.data,
          submitterId: parseInt(submitterId),
          urlServiceWorkflow: `${rootUrlServiceWorkflow}`,
          urlServiceCoreApi: rootUrlServiceCoreApi,
        }));
    };
    let isMounted = true;

    getRequest({
      isMounted,
      rootUrlServiceCoreApi,
      rootUrlServiceWorkflow,
      submitterId: userProfile?.dbId,
    });
    return () => {
      isMounted = false;
    };
  }, []);

  const sendNotification = ({ recordId, jobWorkflowId, otherParameters }) => {
    // console.log(config)
    const { submitterId, urlServiceWorkflow, urlServiceCoreApi } = config;
    const sender = {
      rootUlr: urlServiceWorkflow,
      uri: 'notification/v2/send', //TODO: Only for version 1 uri: 'notify/store_message',
      payload: {
        submitter_id: submitterId,
        record_id: recordId,
        job_workflow_id: jobWorkflowId,
        other_parameters: otherParameters,
      },
    };
    let result = {
      status: {
        success: false,
        message: '',
      },
      data: {},
    };
    try {
      send(sender)
        .then((response) => {
          result.status.success = response.status;
          result.status.message = response.message;
        })
        .catch((error) => {
          result.status.success = false;
          result.status.message = error.toString();
        });
    } catch (error) {
      result.status.success = false;
      result.status.message = error.toString();
    }

    return result;
  };
  sendNotification.prototype = {
    recordId: PropTypes.number.isRequired,
    jobWorkflowId: PropTypes.number.isRequired,
    otherParameters: PropTypes.object,
  };

  const send = async ({ rootUlr, payload, uri }) => {
    console.log('Axios:', { payload, domain: rootUlr, uri });
    const instance = axios.create({
      baseURL: rootUlr,
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${getTokenId()}`,
        'Access-Control-Allow-Origin': '*',
        // 'Accept-Encoding': 'gzip, deflate, br',
      },
    });
    return await instance.post(uri, payload);
  };

  return {
    productList: config.products,
    sendNotification,
  };
};

useWorkflow.propTypes = {
  jobWorkflowId: PropTypes.number.isRequired,
  rootUrlServiceWorkflow: PropTypes.string,
  rootUrlServiceCoreApi: PropTypes.string,
  submitterId: PropTypes.number.isRequired,
};

// export default useWorkflow
// export useWorkflow
