
export const useHelper = (props) => {
  const ebsGrid = {
    styleStatic: {  
      columnStyle: {
        fontSize: 14,
      }, 
      rowStyle: {
        fontSize: 12,
      },
    },
  }
  
  return {
    ebsGrid,
  }
}