import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND MOLECULES TO USE
import { Core, Lab } from '@ebs/styleguide';
import MenuItemGrid from 'components/Molecules/MenuItemGrid';
const { TextField, Typography } = Core;
const { Pagination } = Lab;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PaginationOrganism = React.forwardRef(
  (
    {
      id,
      pageIndex,
      pageSize,
      setPageSize,
      gotoPage,
      totalElements,
      pageCount,
      counter,
      disabledFiedPagination,
      resetPage,
      setResetPage,
    },
    ref,
  ) => {
    const handleChangePage = (event, newPage) => {
      gotoPage(newPage - 1);
    };
    useEffect(() => {
      if (resetPage) gotoPage(0);
      return () => setResetPage(false);
    }, [resetPage]);

    return (
      /**
       * @prop data-testid: Id to use inside pagination.test.js file.
       */
      <div
        data-testid={'PaginationTestId'}
        ref={ref}
        className='grid grid-col-1 gap-6 p-4'
      >
        <div className='flex flex-row flex-nowrap gap-4'>
          <Pagination
            boundaryCount={1}
            siblingCount={1}
            count={pageCount}
            onChange={handleChangePage}
            page={pageIndex + 1}
            showFirstButton={true}
            showLastButton={true}
          />
          {disabledFiedPagination === true ? null : (
            <div className='flex flex-row flex-nowrap gap-4'>
              <Typography className='font-ebs place-self-center'>
                Go to page:{' '}
              </Typography>
              <TextField
                component='span'
                size='small'
                variant='outlined'
                className='font-ebs px-3'
                type='number'
                defaultValue={pageIndex + 1}
                onChange={(e) => {
                  const page = e.target.value ? Number(e.target.value) - 1 : 0;
                  gotoPage(page);
                }}
              />
            </div>
          )}
          <div className='flex-grow'></div>
          <div className='flex flex-row flex-nowrap gap-4'>
            <Typography className='font-ebs place-self-center px-6'>{`Showing ${counter[pageIndex]?.from}-${counter[pageIndex]?.to} of ${totalElements} items`}</Typography>
            <Typography className='place-self-center font-ebs px-3'>Show</Typography>
            <MenuItemGrid
              id={id}
              pageSize={pageSize}
              setPageSize={setPageSize}
              gotoPage={gotoPage}
            />
          </div>
        </div>
      </div>
    );
  },
);
// Type and required properties
PaginationOrganism.propTypes = {
  pageIndex: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  setPageSize: PropTypes.func.isRequired,
  gotoPage: PropTypes.func.isRequired,
};
// Default properties
PaginationOrganism.defaultProps = {};

export default PaginationOrganism;
