import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
// GLOBALIZATION COMPONENT
// CORE COMPONENTS AND MOLECULES TO USE
import { Styles, Lab, Core } from '@ebs/styleguide';
const { useTheme } = Styles;
const { Autocomplete } = Lab;
const { TextField, Checkbox } = Core;
import { getCoreSystemContext, getTokenId } from '@ebs/layout';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const FiedFilterOrganism = React.forwardRef(
  (
    { componentsFilter, filterArray, setfilterArray, state, array, setArray },
    ref,
  ) => {
    const theme = useTheme();
    const { graphqlUri } = getCoreSystemContext();

 const _options =  componentsFilter.map((item) =>
      item.options.map((element) => (element.filter = item.id)),
    );

    const dataSubmit = (e, options) => {
      if(!options) return;
      if (options.filter !== 'product') {
        const typeFilter = options?.filter;
        let col = typeFilter === 'roles' ? 'roles.id' : 'tenants.organization.id';
        const idOptions = options?.id;
        if (
          filterArray.filter((item) => item.col === col && item.val === idOptions)
            .length > 0
        )
          return;
        switch (typeFilter) {
          case 'roles':
            setfilterArray((filterArray) => [
              ...filterArray,
              { col: 'roles.id', mod: 'EQ', val: idOptions },
            ]);
            setArray((array) => [...array, options.filter]);
            break;
          case 'organizations':
            setfilterArray((filterArray) => [
              ...filterArray,
              { col: 'tenants.organization.id', mod: 'EQ', val: idOptions },
            ]);
            setArray((array) => [...array, options.filter]);
            break;
          case 'contactType':
            setfilterArray((filterArray) => [
              ...filterArray,
              {
                col: 'contactTypes.id',
                mod: 'EQ',
                val: idOptions,
              },
            ]);
            setArray((array) => [...array, options.filter]);
            break;
          default:
            [];
        }
      } else {
        setArray((array) => [...array, options.filter]);
        const client = axios.create({
          baseURL: graphqlUri.replace('graphql', ''),
          withCredentials: false,
          headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${getTokenId()}`,
            'Access-Control-Allow-Origin': '*',
            // 'Accept-Encoding': 'gzip, deflate, br',
          },
        });
        client
          .get('/product/' + options.id + '/members')
          .then((result) => {
            const filter = result.data.members.map((item) => ({
              col: 'id',
              mod: 'EQ',
              val: item,
            }));
            setfilterArray(filter);
          })
          .catch((e) => {
            console.log(e);
          });
      }
    };
    if(!_options) return null;

      const optionSelected = (option, value) => option.name
      const renderOption = (props, option, { selected }) => (
        <li {...props}>
          {option.name}
        </li>
      );
      
    return (
      <>
        {
          componentsFilter.map((item, index) => (
            <Autocomplete
            key={item.id}
            id="disabled-options-demo"
            disabled={array.includes(item.id)}
            isOptionEqualToValue ={optionSelected}
            getOptionLabel={(option) => option.name || ''}
            options={item.options ?? []}
            renderOption={renderOption}
            onChange={(e, options) => dataSubmit(e, options)}
            sx={{ width: 150 }}
            renderInput={(params) => <TextField variant='outlined' {...params} label={item.nameFilter} />}
          />
          ))
        }
      </>
    );
  },
);
// Type and required properties
FiedFilterOrganism.propTypes = {};
// Default properties
FiedFilterOrganism.defaultProps = {};

export default FiedFilterOrganism;
