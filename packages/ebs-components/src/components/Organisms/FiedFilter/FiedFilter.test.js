import FiedFilter from './FiedFilter';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FiedFilter is in the DOM', () => {
  render(<FiedFilter></FiedFilter>)
  expect(screen.getByTestId('FiedFilterTestId')).toBeInTheDocument();
})
