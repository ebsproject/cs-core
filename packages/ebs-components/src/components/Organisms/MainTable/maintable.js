import React, { forwardRef, useContext, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import {
  useTable,
  useSortBy,
  useGroupBy,
  useFilters,
  useExpanded,
  useRowSelect,
  usePagination,
  useBlockLayout,
  useGlobalFilter,
  useResizeColumns,
} from 'react-table';
import { useSticky } from 'react-table-sticky';
import Select from 'components/Atoms/Select';
import Progress from 'components/Atoms/Progress';
import Toolbar from 'components/Molecules/Toolbar';
import Pagination from 'components/Organisms/Pagination';
import DefaultFilter from 'components/Atoms/DefaultFilter';
import MenuColumn from 'components/Molecules/MenuColumn';
import { Styles } from './styles';
import { Core, Icons, Styles as EbsStyles } from '@ebs/styleguide';
const { Box, Divider, Typography, IconButton, Paper } = Core;
import InfoMessage from 'components/Atoms/InfoMessage';
// * Icons
const { ExpandLess, ExpandMore, KeyboardArrowUp, KeyboardArrowDown } = Icons;
import {
  useSelector as useSelectorLayout,
  getContext,
  userContext,
  useLayoutContext,
} from '@ebs/layout';
const { useTheme } = EbsStyles;

// * MAIN FUNCTION

/**
 @prop columns columns to build headers
 @prop data data to display
 @prop title Table title
 @prop loading Loading flag
 @prop pageCount: controlledPageCount controlled pagination
 @prop height Height table
 @prop indexing Indexing flag to build indexing column
 @prop rowActions Component to display on every row as actions
 @prop toolbarActions Component to display on top table as global actions
 @prop select Select flag and kind of select to apply (it can be a custom component setup by develop)
 @prop renderRowSubComponent Component to display on every row as Master detail component
 @param ref reference made by React.forward
*/
const MainTableOrganism = forwardRef(
  (
    {
      resetPage,
      setResetPage,
      defaultFilters,
      columns,
      counter,
      csvFileName,
      data,
      displayGlobalFilter,
      entity,
      errorMessage,
      fetchData,
      GetCSVData,
      height,
      id,
      indexing,
      loading,
      pageCount: controlledPageCount,
      raWidth,
      renderRowSubComponent,
      rowActions,
      select,
      styleStatic,
      title,
      toolbar,
      toolbarActions,
      totalElements,
      uri,
      disabledViewConfiguration,
      disabledViewDownloadCSV,
      disabledViewPrintOunt,
      disabledHeadTable,
      color,
      disabledPagination,
      disabledFiedPagination,
      disabledDetails,
    },
    ref,
  ) => {
    const theme = useTheme();
    const { filterListToApply, filterGridToApply, userProfile } = useLayoutContext();
    const [refresh, setRefresh] = useState(0);
    const [downloadCSV, setDownloadCSV] = useState(false);
    const [selectedRows, setSelectedRows] = useState([]);
    const refreshTable = () => setRefresh((refresh) => refresh + 1);
    const { columnStyle, rowStyle } = styleStatic;
    let _userProfile = Object.assign({}, userProfile);
    const domainId = getContext()?.domainId;

    // * Setting up default column filters
    const defaultColumn = useMemo(
      () => ({
        // * Let's set up our default Filter UI
        Filter: (column) => DefaultFilter(column, setResetPage, refresh),
      }),
      [],
    );
    // * Defining hidden columns

    const hiddenColumns = [];
    columns.map((header) => {
      if (header.columns) {
        header.columns.map((column) => {
          column.hidden
            ? hiddenColumns.push(column.accessor)
            : hiddenColumns.push(column.id);
        });
      } else {
        header.hidden
          ? hiddenColumns.push(header.accessor)
          : hiddenColumns.push(header.id);
      }
    });
    // ? row Actions ?
    !rowActions && hiddenColumns.push('rowActions');
    // ? indexing ?
    !indexing && hiddenColumns.push('indexing');
    // ? Selectable ?
    !select && hiddenColumns.push('selection');
    // ? render row sub component ?
    !renderRowSubComponent && hiddenColumns.push('details');

    let _columns = _userProfile.preference.favorites.columns.map((item) => item);
    let objectColumns = _columns.filter((item) => item.idGrid === id);

    if (objectColumns.length >= 1) {
      for (let i = 0; i < objectColumns.length; i++) {
        const obj = objectColumns[i];
        obj['columns'] &&
          obj.columns.map((item) => {
            hiddenColumns.push(item.nameColumn);
          });
      }
    }

    // * Setting up React-Table Hooks
    const {
      getTableProps,
      getTableBodyProps,
      headerGroups,
      prepareRow,
      rows,
      page,
      gotoPage,
      setPageSize,
      // visibleColumns,
      selectedFlatRows,
      allColumns,
      getToggleHideAllColumnsProps,
      // * Get the state from the instance
      state: { pageIndex, pageSize, sortBy, filters, globalFilter },
      setAllFilters,
      setGlobalFilter,
    } = useTable(
      {
        columns,
        data,
        initialState: {
          pageSize:
            id === 'Connections' || id === 'Reports'
              ? 100
              : objectColumns.find((item) => item.pagination) !== undefined
                ? objectColumns.find((item) => item.pagination)['pagination']
                : 10,
          pageIndex: 0,
          // * Defining hidden columns by default
          hiddenColumns: hiddenColumns,
        },
        // * Pass our hoisted table state
        manualPagination: true, // * Tell the usePagination
        manualSortBy: true,
        autoResetPage: false,
        autoResetSortBy: false,
        manualFilters: true,
        manualGlobalFilter: true,
        // * hook that we'll handle our own data fetching
        // * This means we'll also have to provide our own
        // * pageCount.
        pageCount: controlledPageCount,
        defaultColumn,
      },
      useFilters,
      useGlobalFilter,
      useGroupBy,
      useSortBy,
      useExpanded,
      usePagination,
      useBlockLayout,
      useResizeColumns,
      useRowSelect,
      useSticky,
      (hooks) => {
        hooks.visibleColumns.push((columns) => [
          // * Indexing column
          {
            id: 'indexing',
            Cell: ({ row }) => (
              <Typography variant='button'>{row.index + 1}</Typography>
            ),
            width: 35,
            sticky: 'left',
            disableResizing: true,
          },
          {
            id: 'selection',
            // * The header can use the table's getToggleAllRowsSelectedProps method
            // * to render a checkbox
            Header: ({ getToggleAllRowsSelectedProps }) => {
              if (select) {
                switch (select) {
                  case 'multi':
                    return (
                      <Select
                        //onClick={() => addAllItems(data)}
                        {...getToggleAllRowsSelectedProps()}
                      />
                    );
                  default:
                    return <div />;
                }
              } else {
                return <div />;
              }
            },
            // * The cell can use the individual row's getToggleRowSelectedProps method
            // * to the render a checkbox
            Cell: ({ row }) => {
              if (select) {
                switch (select) {
                  case 'multi':
                    return <Select {...row.getToggleRowSelectedProps()} />;
                  case 'single':
                    if (
                      rows.filter((row) => row.isSelected).length < 1 ||
                      row.isSelected
                    ) {
                      return <Select {...row.getToggleRowSelectedProps()} />;
                    } else {
                      return (
                        <Select
                          readOnly
                          style={row.getToggleRowSelectedProps().style}
                        />
                      );
                    }
                  default:
                    // * Node to handle a custom select
                    return select.render({ rows, row });
                }
              } else {
                return String('');
              }
            },
            width: 55,
            sticky: 'left',
            disableResizing: true,
            hidden: select ? false : true,
          },
          {
            // * Build our expander column to display detail component
            id: 'details', // * Make sure it has an ID
            Cell: ({ row }) => {
              // * Use Cell to render an expander for each row.
              // * We can use the getToggleRowExpandedProps prop-getter
              // * to build the expander.
              if (!row.isGrouped) {
                return id === 'PlantingArea' ? (
                  <div></div>
                ) : row?.original?.geospatialObjectType === 'planting area' ||
                  row?.original?.facilityClass === 'container' ? (
                  <div></div>
                ) : disabledDetails === true ? (
                  <div></div>
                ) : (
                  <span {...row.getToggleRowExpandedProps()}>
                    <IconButton>
                      {row.isExpanded ? <KeyboardArrowUp /> : <KeyboardArrowDown />}
                    </IconButton>
                  </span>
                );
              } else {
                return <React.Fragment />;
              }
            },
            width: 50,
            sticky: 'left',
            disableResizing: true,
            hidden: renderRowSubComponent ? false : true,
          },
          {
            id: 'rowActions',
            Header: (
              <Typography variant='h5' color='primary'>
                {id === 'Places' || id === 'Facilities' ? 'Actions' : null}
              </Typography>
            ),
            Cell: ({ row }) => {
              if (!row.isGrouped) {
                if (rowActions) {
                  if (typeof rowActions === 'object') {
                    return rowActions.render({
                      rowData: row.original,
                      refresh: refreshTable,
                    });
                  } else {
                    return rowActions(row.original, refreshTable);
                  }
                } else {
                  return String('');
                }
              } else {
                return <React.Fragment />;
              }
            },
            width: raWidth || getWidth(rowActions),
            sticky: 'left',
            disableResizing: true,
            hidden: rowActions ? false : true,
          },
          ...columns,
        ]);
      },
    );

    function getWidth(rowActions) {
      if (rowActions) {
        if (typeof rowActions === 'object') {
          return rowActions.render.length * 55;
        } else {
          const value =
            rowActions({}, () => {})?.type?.render?.length ||
            rowActions({}, () => {})?.props?.children?.length;
          return value * 55;
        }
      } else {
        return 10;
      }
    }
    // * Listen for changes in pagination, sorting or filtering and use the state to fetch our new data
    useEffect(() => {
      let defaultFiltersData = [];
      let idsArray = ['Users', 'People', 'tableMarker.ebsGrid'];

      switch (domainId) {
        case 2:
          if (idsArray.includes(id)) {
            if (Object.entries(filterListToApply).length > 0) {
              filterListToApply.filterValues.Tenant?.map((item) => {
                defaultFiltersData.push({
                  col: 'tenants.id',
                  mod: 'EQ',
                  val: item.id,
                });
              });
            }
            if (id === 'People') {
              defaultFiltersData.push({
                col: 'category.name',
                mod: 'EQ',
                val: 'Person',
              });
            }
            if (filterGridToApply.length > 0) {
              filterGridToApply.forEach((element) => {
                defaultFiltersData.push(element);
              });
            }
          }

          break;
        case 4:
          if (idsArray.includes(id)) {
            if (filterGridToApply.length > 0) {
              filterGridToApply.forEach((element) => {
                defaultFiltersData.push(element);
              });
              break;
            }
          }
      }

      fetchData({
        pageIndex,
        pageSize,
        sortBy,
        filters,
        globalFilter,
        counter,
        defaultFiltersData,
        filterGridToApply,
        defaultFilters,
      });
    }, [
      sortBy,
      fetchData,
      pageIndex,
      pageSize,
      filters,
      globalFilter,
      refresh,
      filterListToApply,
      filterGridToApply,
    ]);
    useEffect(() => {
      if (downloadCSV) {
        if (selectedFlatRows.length > 0) {
          selectedFlatRows.map((item) => {
            if (item?.isGrouped) {
              item.subRows.map((row) => {
                selectedRows.push(row.original);
              });
            } else {
              selectedRows.push(item.original);
            }
          });
        }
        GetCSVData(
          { filters, globalFilter, counter, totalElements, defaultFilters },
          selectedRows,
          setSelectedRows,
        );
      }
      return () => {
        setDownloadCSV(false);
      };
    }, [downloadCSV]);
    return (
      /* 
     @prop data-testid: Id to use inside maintable.test.js file.
     */
      <div data-testid={'MainTableTestId'}>
        {/*
         // * Toolbar and Titles
         */}
        <Toolbar
          id={id}
          setDownloadCSV={setDownloadCSV}
          data={data}
          title={title}
          columns={columns}
          toolbar={toolbar}
          allColumns={allColumns}
          displayGlobalFilter={displayGlobalFilter}
          refreshTable={refreshTable}
          toolbarActions={toolbarActions}
          csvFileName={csvFileName}
          globalFilter={globalFilter}
          setGlobalFilter={setGlobalFilter}
          selectedFlatRows={selectedFlatRows}
          getToggleHideAllColumnsProps={getToggleHideAllColumnsProps}
          uri={uri}
          entity={entity}
          disabledViewConfiguration={disabledViewConfiguration}
          disabledViewDownloadCSV={disabledViewDownloadCSV}
          disabledViewPrintOunt={disabledViewPrintOunt}
          objectColumns={objectColumns}
          setAllFilters={setAllFilters}
        />
        {/* // * React Table UI */}
        <InfoMessage errorMessage={errorMessage} />
        <Styles>
          <Paper>
            <div
              {...getTableProps()}
              className={
                color === 'secondary'
                  ? 'tableSe sticky'
                  : color === 'terciary'
                    ? 'tableTe sticky'
                    : 'table sticky'
              }
            >
              {disabledHeadTable === true ? null : (
                <div className='header'>
                  {headerGroups.map((headerGroup) => {
                    const { key, ...restHeaderGroupProps } =
                      headerGroup.getHeaderGroupProps();
                    return (
                      <div key={key} {...restHeaderGroupProps} className='tr'>
                        {headerGroup.headers.map((column) => {
                          const { key, ...restHeaderProps } =
                            column.getHeaderProps();
                          return (
                            <div
                              key={key}
                              {...restHeaderProps}
                              className={
                                theme.palette.mode === 'dark' ? 'thDark' : 'th'
                              }
                            >
                              {/* // * HEADERS AND SORTING, */}
                              <Box
                                display='flex'
                                flexWrap='nowrap'
                                flexDirection='row'
                                alignContent='center'
                              >
                                <Box flexGrow={1} fontSize={columnStyle.fontSize}>
                                  {column.render('Header')}
                                </Box>
                                {column.disabledMenu === false
                                  ? null
                                  : (column.canGroupBy || column.canSort) && (
                                      <Box>
                                        <MenuColumn column={column} />
                                      </Box>
                                    )}
                              </Box>
                              {!column.headers && <Divider />}
                              <Box>
                                {/* // * Render the columns filter UI */}
                                {column.canFilter && column.render('Filter')}
                              </Box>
                              {/* // * Use column.getResizerProps to hook up the events correctly */}
                              {column.canResize && (
                                <div
                                  {...column.getResizerProps()}
                                  className={`resizer ${
                                    column.isResizing ? 'isResizing' : ''
                                  }`}
                                />
                              )}
                            </div>
                          );
                        })}
                      </div>
                    );
                  })}
                </div>
              )}
              <div {...getTableBodyProps()} className='body'>
                {page.map((row) => {
                  prepareRow(row);
                  const { key, ...restRowProps } = row.getRowProps();
                  return (
                    <React.Fragment key={key}>
                      <div {...restRowProps} className='tr'>
                        {row.cells.map((cell) => {
                          const { key, ...restCellProps } = cell.getCellProps();
                          return (
                            <div
                              key={key}
                              {...restCellProps}
                              className={
                                theme.palette.mode === 'dark' ? 'tdDark' : 'td'
                              }
                            >
                              <Box
                                display='flex'
                                flexWrap='nowrap'
                                flexDirection='row'
                                alignContent='center'
                                alignItems='center'
                              >
                                {/* // * If it's a grouped cell, add an expander and row count*/}
                                {cell.isGrouped ? (
                                  <React.Fragment>
                                    <Box>
                                      <IconButton
                                        {...row.getToggleRowExpandedProps()}
                                      >
                                        {row.isExpanded ? (
                                          <ExpandLess />
                                        ) : (
                                          <ExpandMore />
                                        )}
                                      </IconButton>
                                    </Box>
                                    <Box fontSize={rowStyle.fontSize}>
                                      {/* <Typography component='span' variant='body1'> */}
                                      {cell.render('Cell')} ({row.subRows.length})
                                      {/* </Typography> */}
                                    </Box>
                                  </React.Fragment>
                                ) : cell.isAggregated ? (
                                  <Box fontSize={rowStyle.fontSize}>
                                    {/* // * If the cell is aggregated, use the Aggregated
                                  // * renderer for cell */}
                                    {/* <Typography component='span' variant='body1'> */}
                                    {cell.render('Aggregated')}
                                    {/* </Typography> */}
                                  </Box>
                                ) : cell.isPlaceholder ? null : (
                                  <Box fontSize={rowStyle.fontSize}>
                                    {/* <Typography component='span' variant='body1'> */}
                                    {/* // * For cells with repeated values, render null
                                    // * Otherwise, just render the regular cell */}
                                    {cell.render('Cell')}
                                    {/* </Typography> */}
                                  </Box>
                                )}
                              </Box>
                            </div>
                          );
                        })}
                      </div>
                      {/*
                      // * If the row is in an expanded state, render a row with a
                      // * column that fills the entire length of the table.
                    */}
                      {!row.isGrouped && renderRowSubComponent && row.isExpanded && (
                        <React.Fragment>
                          {/*
                            // * Inside it, call our renderRowSubComponent function. In reality,
                            // * you could pass whatever you want as props to
                            // * a component like this, including the entire
                            // * table instance. But for this example, we'll just
                            // * pass the row
                          */}
                          {renderRowSubComponent({ rowData: row.original })}
                        </React.Fragment>
                      )}
                    </React.Fragment>
                  );
                })}
                <div className='tr'>
                  {loading && (
                    // * Use our custom loading state to show a loading indicator
                    <div
                      className={theme.palette.mode === 'dark' ? 'tdDark' : 'td'}
                      colSpan='10000'
                    >
                      <Progress />
                    </div>
                  )}
                  {data.length < 1 && (
                    // * Use our custom loading state to show a loading indicator
                    <div
                      className={theme.palette.mode === 'dark' ? 'tdDark' : 'td'}
                      colSpan='10000'
                    >
                      <Typography className='font-ebs text-xl'>
                        No results found
                      </Typography>
                    </div>
                  )}
                </div>
              </div>
            </div>
            {/* // * Pagination:
               // * Pagination can be built however you'd like. 
              // * This is just a Material UI implementation: */}
            <div
              className={
                theme.palette.mode === 'dark' ? 'paginationDark' : 'pagination'
              }
            >
              {disabledPagination === true ? null : (
                <Pagination
                  id={id}
                  setResetPage={setResetPage}
                  resetPage={resetPage}
                  pageIndex={pageIndex}
                  pageSize={pageSize}
                  setPageSize={setPageSize}
                  pageCount={controlledPageCount}
                  gotoPage={gotoPage}
                  totalElements={totalElements}
                  counter={counter}
                  disabledFiedPagination={disabledFiedPagination}
                />
              )}
            </div>
          </Paper>
        </Styles>
      </div>
    );
  },
);
// * Type and required properties
MainTableOrganism.propTypes = {
  columns: PropTypes.array.isRequired,
  controlledPageCount: PropTypes.number,
  data: PropTypes.array,
  displayGlobalFilter: PropTypes.bool,
  errorMessage: PropTypes.any,
  fetchData: PropTypes.func.isRequired,
  height: PropTypes.any,
  indexing: PropTypes.bool,
  loading: PropTypes.bool.isRequired,
  raWidth: PropTypes.number,
  renderRowSubComponent: PropTypes.func,
  rowActions: PropTypes.func,
  select: PropTypes.node,
  styleStatic: PropTypes.shape({
    columnStyle: PropTypes.shape({
      fontSize: PropTypes.number,
    }),
    rowStyle: PropTypes.shape({
      fontSize: PropTypes.number,
    }),
  }),
  title: PropTypes.node,
  toolbar: PropTypes.bool,
  disabledHeadTable: PropTypes.bool,
  toolbarActions: PropTypes.any,
};
// * Default properties
MainTableOrganism.defaultProps = {
  styleStatic: { columnStyle: { fontSize: 14 }, rowStyle: { fontSize: 12 } },
};

export default MainTableOrganism;
