import styled from 'styled-components';
export const Styles = styled.div`
  overflow: auto;

  .pagination{
     background-color: #fff!important;;
  }
  .paginationDark{
     background-color: #1e1e1e!important;;
  }

  .paginationSe{
     background-color: #fff!important;;
  }

  .paginationTe{
    background-color: #fff!important;;
  }

  .tableSe {
    border: 1px solid #ddd;
     background-color: #fff!important;;

    .tr {
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }

    .th,
    .tdSe {
      padding: 5px;
      border-bottom: 1px solid #ddd;
      ${'' /* border-right: 1px solid #ddd; */}
      background-color: #E9E9E9!important;
      overflow: hidden;

      :last-child {
        border-right: 0;
      }
      .resizer {
        display: inline-block;
        background: gray;
        width: 3px;ne;

        &.isResizing {
          background: green;
        }
      }
    }

    &.sticky {
      overflow: auto;
      .header,
      .footer {
        position: sticky;
        z-index: 1;
        width: fit-content;
      }

      .header {
        top: 0;
         ${'' /* box-shadow: 0px 3px 3px #ccc; */}
      }

      .footer {
        bottom: 0;
        ${'' /* box-shadow: 0px -3px 3px #ccc; */}
      }

      .body {
        position: relative;
        z-index: 0;
      }

      [data-sticky-td] {
        position: sticky;
      }

      [data-sticky-last-left-td] {
        ${
          '' /* box-shadow: 2px 0
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        ${'' /* prevents from scrolling while dragging on touch devices */
        }
        touch-action:nopx 3px #ccc; */}
      }

      [data-sticky-first-right-td] {
        ${'' /* box-shadow: -2px 0px 3px #ccc; */}
      }
    }
  }
  
  .table {
    border: 1px solid #ddd;
    .tr {
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }

    .th,
    .td {
      padding: 5px;
      border-bottom: 1px solid #ddd;
      ${'' /* border-right: 1px solid #ddd; */}
       background-color: #fff!important;;
      overflow: hidden;

      :last-child {
        border-right: 0;
      }
      .resizer {
        display: inline-block;
        background: gray;
        width: 3px;ne;

        &.isResizing {
          background: green;
        }
      }
    }
  .thDark,
  .tdDark {
      padding: 5px;
      border-bottom: 1px solid #ddd;
      ${'' /* border-right: 1px solid #ddd; */}
       background-color: #1e1e1e!important;;
      overflow: hidden;

      :last-child {
        border-right: 0;
      }
      .resizer {
        display: inline-block;
        background: gray;
        width: 3px;ne;

        &.isResizing {
          background: green;
        }
      }
    }

    &.sticky {
      overflow: auto;
      .header,
      .footer {
        position: sticky;
        z-index: 1;
        width: fit-content;
      }

      .header {
        top: 0;
        ${'' /* box-shadow: 0px 3px 3px #ccc; */}
      }

      .footer {
        bottom: 0;
        ${'' /* box-shadow: 0px -3px 3px #ccc; */}
      }

      .body {
        position: relative;
        z-index: 0;
      }

      [data-sticky-td] {
        position: sticky;
      }

      [data-sticky-last-left-td] {
        ${
          '' /* box-shadow: 2px 0
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        ${'' /* prevents from scrolling while dragging on touch devices */
        }
        touch-action:nopx 3px #ccc; */}
      }

      [data-sticky-first-right-td] {
        ${'' /* box-shadow: -2px 0px 3px #ccc; */}
      }
    }
  }

  .tableTe {
    border: 1px solid #D4D3D3;
    background-color: #fff!important;;
    .tr {
      :last-child {
        .td {
          border-bottom: 0;
        }
      }
    }

    .th,
    .tdTe {
      padding: 5px;
      border-bottom: 1px solid #D4D3D3;
      ${'' /* border-right: 1px solid #D4D3D3; */}
       background-color: #fff!important;;
      overflow: hidden;
  
      :last-child {
        border-right: 0;
      }
      .resizer {
        display: inline-block;
        background: gray;
        width: 3px;ne;

        &.isResizing {
          background: green;
        }
      }
    }

    &.sticky {
      overflow: auto;
      .header,
      .footer {
        position: sticky;
        z-index: 1;
        width: fit-content;
      }

      .header {
        top: 0;
        ${'' /* box-shadow: 0px 3px 3px #ccc; */}
      }

      .footer {
        bottom: 0;
        ${'' /* box-shadow: 0px -3px 3px #ccc; */}
      }

      .body {
        position: relative;
        z-index: 0;
      }

      [data-sticky-td] {
        position: sticky;
      }

      [data-sticky-last-left-td] {
        ${
          '' /* box-shadow: 2px 0
        height: 100%;
        position: absolute;
        right: 0;
        top: 0;
        transform: translateX(50%);
        z-index: 1;
        ${'' /* prevents from scrolling while dragging on touch devices */
        }
        touch-action:nopx 3px #D4D3D3; */}
      }

      [data-sticky-first-right-td] {
        ${'' /* box-shadow: -2px 0px 3px #ccc; */}
      }
    }
  }
`;
