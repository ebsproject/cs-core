import React, { forwardRef, useContext, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';

import {
  Column,
  ColumnDef,
  ColumnFiltersState,
  RowData,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  getSortedRowModel,
  useReactTable,
  createColumnHelper
} from '@tanstack/react-table'

// // import { useSticky } from 'react-table-sticky';
import Select from 'components/Atoms/Select';
import Progress from 'components/Atoms/Progress';
import Toolbar from 'components/Molecules/Toolbar';
import Pagination from 'components/Organisms/Pagination';
import DefaultFilter from 'components/Atoms/DefaultFilter';
import MenuColumn from 'components/Molecules/MenuColumn';
import { Styles } from './styles';
import { CustomStyles } from './custom-styles';
import { Core, Icons, Styles as EbsStyles } from '@ebs/styleguide';
const { Box, Divider, Typography, IconButton, Paper, Grid, TextField, Button } = Core;
import InfoMessage from 'components/Atoms/InfoMessage';
// * Icons
const { ExpandLess, ExpandMore, KeyboardArrowUp, KeyboardArrowDown } = Icons;
import {
  getContext,
  useLayoutContext
} from '@ebs/layout';
// const { useTheme } = EbsStyles;

// * MAIN FUNCTION

/**
 @prop columns columns to build headers
 @prop data data to display
 @prop title Table title
 @prop loading Loading flag
 @prop pageCount: controlledPageCount controlled pagination
 @prop height Height table
 @prop indexing Indexing flag to build indexing column
 @prop rowActions Component to display on every row as actions
 @prop toolbarActions Component to display on top table as global actions
 @prop select Select flag and kind of select to apply (it can be a custom component setup by develop)
 @prop renderRowSubComponent Component to display on every row as Master detail component
 @param ref reference made by React.forward
*/
const columnHelper = createColumnHelper()
const columns = [
  columnHelper.accessor('description', {
    cell: info => info.getValue(),
    footer: info => info.column.id,
  }),
  columnHelper.accessor(row => row.description, {
    id: 'description',
    cell: info => <i>{info.getValue()}</i>,
    header: () => <span>Last Name</span>,
    footer: info => info.column.id,
  }),
  columnHelper.accessor('id', {
    header: () => 'ID',
    cell: info => info.renderValue(),
    footer: info => info.column.id,
  }),
  columnHelper.accessor('name', {
    header: () => <span>Name</span>,
    footer: info => info.column.id,
  }),
]
const MainTableV2 = forwardRef(
  (
    {
      resetPage,
      setResetPage,
      defaultFilters,
      // columns,
      counter,
      csvFileName,
      data,
      displayGlobalFilter,
      entity,
      errorMessage,
      fetchData,
      GetCSVData,
      height,
      id,
      indexing,
      loading,
      pageCount: controlledPageCount,
      raWidth,
      renderRowSubComponent,
      rowActions,
      select,
      styleStatic,
      title,
      toolbar,
      toolbarActions,
      totalElements,
      uri,
      disabledViewConfiguration,
      disabledViewDownloadCSV,
      disabledViewPrintOunt,
      disabledHeadTable,
      color,
      disabledPagination,
      disabledFiedPagination,
      disabledDetails,
    },
    ref,
  ) => {
    //  const theme = useTheme();
    const { filterListToApply, filterGridToApply, userProfile } = useLayoutContext()
    const [refresh, setRefresh] = useState(0);
    const [downloadCSV, setDownloadCSV] = useState(false);
    const [selectedRows, setSelectedRows] = useState([]);
    const refreshTable = () => setRefresh(refresh + 1);
    const [globalFilter, setGlobalFilter] = useState('');
    const [sorting, setSorting] = useState([]);
    const [pagination, setPagination] = useState({
      pageIndex: 0,
      pageSize: 10,
    });
    //const [data, _setData] = React.useState(() => [...defaultData])
    const rerender = React.useReducer(() => ({}), {})[1]
    useEffect(() => {
      fetchData({ pageSize: 10, pageIndex: 0, sortBy: [], filters: [], globalFilter: [], defaultFilters: [], defaultFiltersData: [] });
    }, [])
    console.log(data)
    const table = useReactTable({
      data,
      columns,
      state: {
        globalFilter,
        sorting,
        pagination,
      },
      onSortingChange: setSorting,
      onPaginationChange: setPagination,
      onGlobalFilterChange: setGlobalFilter,
      getCoreRowModel: getCoreRowModel(),
      getPaginationRowModel: getPaginationRowModel(),
      getSortedRowModel: getSortedRowModel(),
      getFilteredRowModel: getFilteredRowModel(),
    });

    return (


   <Box>
   <Toolbar
          id={id}
          setDownloadCSV={setDownloadCSV}
          data={data}
          title={title}
          columns={columns}
          toolbar={true}
          allColumns={[]}
          displayGlobalFilter={false}
          refreshTable={refreshTable}
          toolbarActions={() => { }}
          csvFileName={""}
          globalFilter={{}}
          setGlobalFilter={() => { }}
          selectedFlatRows={[]}
          getToggleHideAllColumnsProps={() => { }}
          uri={uri}
          entity={entity}
          disabledViewConfiguration={disabledViewConfiguration}
          disabledViewDownloadCSV={disabledViewDownloadCSV}
          disabledViewPrintOunt={disabledViewPrintOunt}
          objectColumns={{}}
        />
       <Box>
      <Grid container justifyContent="space-between" alignItems="center" mb={2}>
        <Grid item>
          <TextField
            label="Search"
            variant="outlined"
            onChange={e => setGlobalFilter(e.target.value)}
            size="small"
          />
        </Grid>
        <Grid item>
          <TextField
            select
            label="Rows per page"
            value={table.getState().pagination.pageSize}
            onChange={e => table.setPageSize(Number(e.target.value))}
            SelectProps={{
              native: true,
            }}
            size="small"
          >
            {[10, 20, 30, 40, 50].map(pageSize => (
              <option key={pageSize} value={pageSize}>
                {pageSize}
              </option>
            ))}
          </TextField>
        </Grid>
      </Grid>
      <Grid container>
        {table.getHeaderGroups().map(headerGroup => (
          <Grid container key={headerGroup.id} sx={{ borderBottom: '1px solid #d8d5ce', padding: '8px' }}>
            {headerGroup.headers.map(header => (
              <Grid
                item
                xs
                key={header.id}
                sx={{ cursor: header.column.getCanSort() ? 'pointer' : 'default' }}
                onClick={header.column.getToggleSortingHandler()}
              >
                <Typography variant="body2" component="div" fontWeight="bold">
                  {flexRender(header.column.columnDef.header, header.getContext())}
                  {header.column.getCanSort() && (
                    <IconButton size="small" sx={{ marginLeft: '4px' }}>
                      {header.column.getIsSorted() === 'desc' ? (
                        <KeyboardArrowDown fontSize="small" />
                      ) : header.column.getIsSorted() === 'asc' ? (
                        <KeyboardArrowUp fontSize="small" />
                      ) : null}
                    </IconButton>
                  )}
                </Typography>
              </Grid>
            ))}
          </Grid>
        ))}
      </Grid>
      <Grid container>
        {table.getRowModel().rows.map(row => (
          <Grid container key={row.id} sx={{ borderBottom: '1px solid #d8d5ce', padding: '8px' }}>
            {row.getVisibleCells().map(cell => (
              <Grid item xs key={cell.id}>
                <Typography variant="body2" component="div">
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </Typography>
              </Grid>
            ))}
          </Grid>
        ))}
      </Grid>
      <Grid container justifyContent="space-between" alignItems="center" mt={2}>
        <Grid item>
          <Button onClick={() => table.setPageIndex(0)} disabled={!table.getCanPreviousPage()}>
            First
          </Button>
          <Button onClick={() => table.previousPage()} disabled={!table.getCanPreviousPage()}>
            Previous
          </Button>
        </Grid>
        <Grid item>
          <Typography variant="body2" component="div">
            Page{' '}
            <strong>
              {table.getState().pagination.pageIndex + 1} of {table.getPageCount()}
            </strong>
          </Typography>
        </Grid>
        <Grid item>
          <Button onClick={() => table.nextPage()} disabled={!table.getCanNextPage()}>
            Next
          </Button>
          <Button onClick={() => table.setPageIndex(table.getPageCount() - 1)} disabled={!table.getCanNextPage()}>
            Last
          </Button>
        </Grid>
      </Grid>
    </Box>
      <div className="h-4" />
        <button onClick={() => rerender()} className="border p-2">
          Refresh
        </button>

    </Box>


    )

  },
);


export default MainTableV2;
