import { gql } from "@apollo/client";
import { client } from "utils/Apollo";

export const modifyUserPreference = async (user, uri) => {
    try {
        const { data } = await client(uri).mutate({
            mutation: MODIFY_PREFERENCE_USER,
            variables: { user },
        });
    } catch (error) {
        console.log(error)
    }
};
export const MODIFY_PREFERENCE_USER = gql`
  mutation modifyUser($user: UserInput!) {
    modifyUser(user: $user) {
      id }
  }
`;
export const FIND_USER_LIST = gql`
  # Write your query or mutation here
  query findUserList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findUserList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        externalId
        userName
        isActive
        isAdmin
        tenants {
          id
          name
          instances {
            id
            name
            domaininstances {
              domain {
                name
                info
                icon
              }
              context
              sgContext
            }
          }
        }
        contact {
          id
          contactInfos {
            id
            value
            default
            contactInfoType {
              id
              name
            }
          }
          person {
            familyName
            additionalName
            givenName
            jobTitle
            knowsAbout
          }
        }
      }
    }
  }
`;