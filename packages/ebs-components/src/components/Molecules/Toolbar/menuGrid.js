import React, { Fragment, useEffect } from "react";
import PropTypes from "prop-types";
import { Core, Icons } from "@ebs/styleguide";
import UserPreferences from 'components/Molecules/UserPreferences';
import { RBAC } from '@ebs/layout';
import ToolbarMenuPOAtom from 'components/Atoms/ToolbarMenuPO/ToolbarMenuPO';
import GlobalFilter from 'components/Atoms/GlobalFilter';

const { ListItem, Box, Button, Popover, Tooltip, Typography } = Core;
const { Menu, Refresh, GetApp } = Icons;
const MenuGrid = React.forwardRef(({
    setDownloadCSV,
    refreshTable,
    globalFilter,
    toolbarActions,
    displayGlobalFilter,
    setGlobalFilter,
    selectedFlatRows,
    uri,
    disabledViewConfiguration,
    disabledViewDownloadCSV,
    disabledViewPrintOunt,
    rest,
},
    ref,
) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [openPop, setOpenPop] = React.useState(false);
    const [placement, setPlacement] = React.useState();
    const csvExport = () => {
        setDownloadCSV(true);
    };
    const refreshAction = () => {
        refreshTable();
    };
    const handleClick = (newPlacement) => (event) => {
        setAnchorEl(event.currentTarget);
        setOpenPop((prev) => placement !== newPlacement || !prev);
        setPlacement(newPlacement);
    };
    const handleClose = () => {
        setOpenPop(false);
    };
    useEffect(() => {
        if (window.innerWidth > 1300) {
            setOpenPop(false);
        }

    }, [window.innerWidth])
    return (
        <div className="flex flex-row p-1 items-center">
            <Tooltip
                arrow
                title={
                    <Typography className='font-ebs text-lg' placement='top'>
                        Grid Menu
                    </Typography>
                }
            >
                <Button
                    variant='contained'
                    onClick={handleClick("bottom-end")}
                    aria-label='Grid menu'
                    startIcon={
                        <Menu className='fill-current text-white ml-3' />
                    }
                ></Button>
            </Tooltip>
            <Popover
                open={openPop}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: "bottom",
                    horizontal: "right",
                }}
                transformOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}
            >
                <Fragment>
                    {<ListItem >
                        <Box display='flex' alignItems='center' flexDirection='column' className={"ml-10" }>
                            {disabledViewConfiguration ? null : (
                                <Box>
                                    <UserPreferences {...rest} />
                                </Box>
                            )}
                            <RBAC allowedAction={'Export'}>
                                <Box>
                                    <Tooltip
                                        arrow
                                        title={
                                            <Typography className='font-ebs text-lg' placement='top'>
                                                refresh
                                            </Typography>
                                        }
                                    >
                                        <Button
                                            variant='contained'
                                            onClick={refreshAction}
                                            aria-label='refresh'
                                            startIcon={
                                                <Refresh className='fill-current text-white ml-3' />
                                            }
                                        ></Button>
                                    </Tooltip>
                                </Box>
                            </RBAC>
                            {disabledViewDownloadCSV ? null : (
                                <RBAC allowedAction={'Export'}>
                                    <Box>
                                        <Tooltip
                                            arrow
                                            title={
                                                <Typography className='font-ebs text-lg' placement='top'>
                                                    Download CSV
                                                </Typography>
                                            }
                                        >
                                            <Button
                                                variant='contained'
                                                onClick={csvExport}
                                                aria-label='download file'
                                                startIcon={
                                                    <GetApp className='fill-current text-white ml-3' />
                                                }
                                            ></Button>
                                        </Tooltip>
                                    </Box>
                                </RBAC>
                            )}
                            {disabledViewPrintOunt ? null : (
                                <Box>
                                    <RBAC allowedAction={'Print'}>
                                        <ToolbarMenuPOAtom
                                            rowData={selectedFlatRows.map((d) => d.original)}
                                        />
                                    </RBAC>
                                </Box>
                            )}

                        </Box>
                    </ListItem>}
                    {toolbarActions && (
                        <Box>
                            {typeof toolbarActions === 'function'
                                ? toolbarActions(
                                    selectedFlatRows.map((d) => d.original),
                                    refreshTable,
                                )
                                : toolbarActions.render({
                                    selectedRows: selectedFlatRows.map((d) => d.original),
                                    refresh: refreshTable,
                                })}
                        </Box>
                    )}
                    {uri
                        ? displayGlobalFilter && (
                            <Box>
                                <GlobalFilter
                                    globalFilter={globalFilter}
                                    setGlobalFilter={setGlobalFilter}
                                />
                            </Box>
                        )
                        : null}
                </Fragment>
            </Popover>
        </div>
    );
});
// Type and required properties
MenuGrid.propTypes = {
    children: PropTypes.node,
};
// Default properties
MenuGrid.defaultProps = {
    children: null,
};

export default MenuGrid;  