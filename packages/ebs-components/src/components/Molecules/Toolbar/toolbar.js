import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Icons } from '@ebs/styleguide';
import GlobalFilter from 'components/Atoms/GlobalFilter';
import UserPreferences from 'components/Molecules/UserPreferences';
import { RBAC } from '@ebs/layout';
import ToolbarMenuPOAtom from 'components/Atoms/ToolbarMenuPO/ToolbarMenuPO';
import MenuGrid from './menuGrid';
const { GetApp, Refresh } = Icons;
const { Box, Button, Tooltip, Typography } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ToolbarMolecule = React.forwardRef(
  (
    {
      setDownloadCSV,
      data,
      title,
      columns,
      toolbar,
      refreshTable,
      globalFilter,
      toolbarActions,
      displayGlobalFilter,
      csvFileName,
      setGlobalFilter,
      selectedFlatRows,
      uri,
      disabledViewConfiguration,
      disabledViewDownloadCSV,
      disabledViewPrintOunt,
      objectColumns,
      setAllFilters,
      ...rest
    },
    ref,
  ) => {
    const csvExport = () => {
      setDownloadCSV(true);
    };

    const refreshAction = () => {
      refreshTable();
      setAllFilters([])
    };
    const [windowWidth, setWindowWidth] = useState(window.innerWidth);
    useEffect(() => {
      const handleResize = () => {
        setWindowWidth(window.innerWidth || 1300);
      };
      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, []);
    const excludeGrids = ['ServiceId', 'items-id'];
    return (
      /* 
     @prop data-testid: Id to use inside toolbar.test.js file.
     */
      <div ref={ref} data-testid={'ToolbarTestId'}>
        <Box display='flex' alignItems='center'>
          <Box flexGrow={1}>{title}</Box>
        </Box>
        {toolbar && (
          <Fragment>
            <Box display='flex' alignItems='center' flexDirection='row-reverse' className="gap-1">
              {!excludeGrids.includes(rest.id) && (
                <div style={{ display: `${windowWidth < 1300 ? 'block' : 'none'}` }}>
                  <Box
                    display='flex'
                    alignItems='center'
                    flexDirection='row-reverse'
                  >
                    <Box>
                      <MenuGrid
                        disabledViewConfiguration={disabledViewConfiguration}
                        rest={rest}
                        refreshTable={refreshTable}
                        disabledViewDownloadCSV={disabledViewDownloadCSV}
                        setDownloadCSV={setDownloadCSV}
                        disabledViewPrintOunt={disabledViewPrintOunt}
                        selectedFlatRows={selectedFlatRows}
                        toolbarActions={toolbarActions}
                        globalFilter={globalFilter}
                        setGlobalFilter={selectedFlatRows}
                        displayGlobalFilter={displayGlobalFilter}
                      />
                    </Box>
                  </Box>
                </div>
              )}
              <div
                style={{
                  display: `${
                    windowWidth > 1300 || excludeGrids.includes(rest.id)
                      ? 'block'
                      : 'none'
                  }`,
                }}
              >
                <Box display='flex' alignItems='center' flexDirection='row-reverse'>
                  {disabledViewConfiguration ? null : (
                    <Box>
                      <UserPreferences {...rest} objectColumns={objectColumns} />
                    </Box>
                  )}
                  <RBAC allowedAction={'Export'}>
                    <Box>
                      <Tooltip
                        arrow
                        title={
                          <Typography className='font-ebs text-lg' placement='top'>
                            Refresh and reset filters
                          </Typography>
                        }
                      >
                        <Button
                          variant='contained'
                          onClick={refreshAction}
                          aria-label='refresh'
                          startIcon={
                            <Refresh className='fill-current text-white ml-3' />
                          }
                        ></Button>
                      </Tooltip>
                    </Box>
                  </RBAC>
                  {disabledViewDownloadCSV ? null : (
                    <RBAC allowedAction={'Export'}>
                      <Box>
                        <Tooltip
                          arrow
                          title={
                            <Typography className='font-ebs text-lg' placement='top'>
                              Download CSV
                            </Typography>
                          }
                        >
                          <Button
                            variant='contained'
                            onClick={csvExport}
                            aria-label='download file'
                            startIcon={
                              <GetApp className='fill-current text-white ml-3' />
                            }
                          ></Button>
                        </Tooltip>
                      </Box>
                    </RBAC>
                  )}
                  {disabledViewPrintOunt ? null : (
                    <Box>
                      <RBAC allowedAction={'Print'}>
                        <ToolbarMenuPOAtom
                          rowData={selectedFlatRows.map((d) => d.original)}
                        />
                      </RBAC>
                    </Box>
                  )}
                  {toolbarActions && (
                    <Box>
                      {typeof toolbarActions === 'function'
                        ? toolbarActions(
                            selectedFlatRows.map((d) => d.original),
                            refreshTable,
                          )
                        : toolbarActions.render({
                            selectedRows: selectedFlatRows.map((d) => d.original),
                            refresh: refreshTable,
                          })}
                    </Box>
                  )}
                  {uri
                    ? displayGlobalFilter && (
                        <Box>
                          <GlobalFilter
                            globalFilter={globalFilter}
                            setGlobalFilter={setGlobalFilter}
                          />
                        </Box>
                      )
                    : null}
                </Box>
              </div>
            </Box>
            <br />
          </Fragment>
        )}
      </div>
    );
  },
);
// Type and required properties
ToolbarMolecule.propTypes = {
  title: PropTypes.node,
  toolbar: PropTypes.bool.isRequired,
  refreshTable: PropTypes.func,
  displayGlobalFilter: PropTypes.bool,
  toolbarActions: PropTypes.any,
  setGlobalFilter: PropTypes.func.isRequired,
  selectedFlatRows: PropTypes.array.isRequired,
};
// Default properties
ToolbarMolecule.defaultProps = {
  toolbar: true,
  displayGlobalFilter: true,
};

export default ToolbarMolecule;
