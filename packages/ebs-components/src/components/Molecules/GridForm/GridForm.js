import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core } from '@ebs/styleguide';
const { Grid, Typography } = Core;
import TextField from 'components/Atoms/TextFieldForm';
import Checkbox from 'components/Atoms/CheckboxForm';
import DatePicker from 'components/Atoms/DatePickerForm';
import RadioGroup from 'components/Atoms/RadioForm';
import Switch from 'components/Atoms/SwitchForm';
import Button from 'components/Atoms/ButtonForm';
import UploadFile from 'components/Atoms/UploadFile';
import Select from 'components/Atoms/DropDownForm';
import MapFormAtom from 'components/Atoms/MapForm/MapForm';
import ModalForm from 'components/Atoms/ModalForm';
import DialogForm from 'components/Atoms/DialogForm';

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const GridFormMolecule = React.forwardRef(
  ({ components, setValue, getValues, control, errors, watch, defaultValues }, ref) => {

    /**
     * @param {String} component
     * @param {String} name
     * @returns node
     */
    const getComponent = ({ component, ...rest }) => {
      const { label, style } = rest; 
      switch (component.toLowerCase()) {
        case 'textfield':
          return (
            <TextField
              watch={watch}
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'checkbox':
          // const { name } = rest;
          // if (name === 'byOccurrence') return;
          return (
            <Checkbox
              watch={watch}
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'datepicker':
          return (
            <DatePicker
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'radio':
          return (
            <RadioGroup
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'select':
          return (
            <Select
              defaultValues={defaultValues}
              watch={watch}
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'switch':
          return (
            <Switch
              watch={watch}
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'button':
          return <Button {...rest} />;
        case 'modal':
          const { uiComponent, parentControl } = rest;
          return <ModalForm parentControl={parentControl} watch={watch} component={uiComponent} {...rest} />
        case 'dialog':
          return <DialogForm getValues={getValues} watch={watch} {...rest} />
        case 'file':
          return (
            <UploadFile
              watch={watch}
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'map':
          return (
            <MapFormAtom
              setValue={setValue}
              getValues={getValues}
              control={control}
              errors={errors}
              {...rest}
            />
          );
        case 'divider':
          return (
            <>
              <hr style={style} />
              <Typography
                variant='h5'
                style={{ fontWeight: 800, padding: 10 }}
              >
                {label}
              </Typography>
            </>
          );
        default:
          break;
      }
    };
    return (
      /**
       * @prop data-testid: Id to use inside GridForm.test.js file.
       */
      <Grid
        container
        ref={ref}
        data-testid={'GridFormTestId'}
        direction='row'
        justifyContent='flex-start'
        alignItems='flex-start'
        spacing={2}
      >
        {components.map((component) => (
          <Grid
            item
            xs={component.sizes[0]}
            sm={component.sizes[1]}
            md={component.sizes[2]}
            lg={component.sizes[3]}
            xl={component.sizes[4]}
            key={component.name}
          >
            {getComponent(component)}
          </Grid>
        ))}
      </Grid>
    );
  },
);
// Type and required properties
GridFormMolecule.propTypes = {
  component: PropTypes.array,
  setValue: PropTypes.func.isRequired,
  getValues: PropTypes.func.isRequired,
  control: PropTypes.any,
  errors: PropTypes.object,
};
// Default properties
GridFormMolecule.defaultProps = {};

export default GridFormMolecule;
