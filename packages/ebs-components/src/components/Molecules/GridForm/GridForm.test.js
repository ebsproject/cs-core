import GridForm from './GridForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('GridForm is in the DOM', () => {
  render(<GridForm></GridForm>)
  expect(screen.getByTestId('GridFormTestId')).toBeInTheDocument();
})
