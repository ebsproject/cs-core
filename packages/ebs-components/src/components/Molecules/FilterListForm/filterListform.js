import React, { useState, useEffect } from 'react';
import { Core, Icons, Lab } from '@ebs/styleguide';
const {
  Box,
  Typography,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  List,
  Tooltip,
  Button,
  Snackbar,
} = Core;
const { Delete, Star } = Icons;
const { Alert } = Lab;
import Cookies from "js-cookie";
import FilterItems from '@ebs/layout';
// import { filterListToApply_f, useDispatch } from '@ebs/layout';
import { useLayoutContext } from '@ebs/layout';
//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FilterListFormMolecule = React.forwardRef(
  ({ userProfile, objectProperties, context, setOpenPop }, ref) => {
    const {setFilterListToApply} = useLayoutContext()
    let preferenceObject = Object.assign({},userProfile.preference);
    const [state, setState] = useState(false);
    const [openMessage, setOpenMessage] = React.useState(false);
    const userInfo = objectProperties.userInfo;
    // useEffect(() => {
    //   if (state) {
    //     objectProperties.setUserData(objectProperties.userState)(
    //       objectProperties.dispatch,
    //       objectProperties.getState,
    //     );
    //   }
    //   return () => setState(false);
    // }, []);
    const deleteFilter = (index) => {
      preferenceObject.filters.splice(index, 1);
      objectProperties.modifyUserPreference({
        id: Number(userInfo.id),
        userName: userInfo.userName,
        active: userInfo.isActive,
        contactId: userInfo.contact.id,
        preference: preferenceObject,
      })(objectProperties.dispatch);
      setState(true);
    };
    const setFilterContext = (item, index) => {
      updateDefaultFilter(item, index);
      setFilterListToApply({
        filterName: 'default',
        default: true,
        domainId: context.domainId,
        tenantId: context.tenantId,
        filterValues: item.filterValues,
      })
      Cookies.remove("filters");
      setOpenPop(false);
      handleClickMessage();
    };

    function updateDefaultFilter(item, index) {
      for (let i = 0; i < preferenceObject.filters.length; i++) {
        if (preferenceObject.filters[i].domainId === context.domainId)
          preferenceObject.filters[i].default = false;
      }
      preferenceObject.filters[index].default = true;

      objectProperties.modifyUserPreference({
        id: Number(userInfo.id),
        userName: userInfo.userName,
        active: userInfo.isActive,
        contactId: userInfo.contact.id,
        preference: preferenceObject,
      })(objectProperties.dispatch);
      setState(true);
    }
    const handleClickMessage = () => {
      setOpenMessage(true);
    };

    const handleCloseMessage = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
      setOpenMessage(false);
    };
    let itemsByDomain = preferenceObject.filters.filter(
      (item) => item.domainId === context.domainId,
    );
    if (preferenceObject.filters.length === 0 || itemsByDomain.length === 0) {
      return (
        <Typography className='font-ebs text-ebs-green-default p-3'>
          No Filter List
        </Typography>
      );
    } else {
      return (
        <Box>
          <List className='max-w-auto'>
            {preferenceObject.filters.map((item, index) => (
              <ListItem
                className={item.domainId === context.domainId ? '' : 'hidden'}
                button
                key={index}
                id={index}
              >
                {item.default ? (
                  <div>
                    <Star color='primary' />
                  </div>
                ) : (
                  <div>
                    <Star color='disabled' />
                  </div>
                )}
                <Tooltip
                  arrow
                  title={
                    <Typography className='font-ebs text-xl'>
                      {'Set filter as default'}
                    </Typography>
                  }
                >
                  <Button
                    disabled={item.domainId === context.domainId ? false : true}
                    color='primary'
                    aria-label='delete'
                    onClick={() => setFilterContext(item, index)}
                  >
                    Apply
                  </Button>
                </Tooltip>
                <ListItemText
                  className='font-ebs text-lg text-gray-800 p-5 '
                  primary={item.filterName}
                />
                <FilterItems
                  className={item.domainId === context.domainId ? '' : 'hidden'}
                  item={item}
                />
                <ListItemSecondaryAction>
                  <IconButton
                    className={item.domainId === context.domainId ? '' : 'hidden'}
                    disabled={item.domainId === context.domainId ? false : true}
                    size='small'
                    color='primary'
                    onClick={() => deleteFilter(index)}
                  >
                    <Delete />
                  </IconButton>
                </ListItemSecondaryAction>
              </ListItem>
            ))}
          </List>
          <Snackbar
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'right',
            }}
            open={openMessage}
            autoHideDuration={2000}
            onClose={handleCloseMessage}
          >
            <Alert variant='filled' onClose={handleCloseMessage} severity='success'>
              Filter applied and added to context!!
            </Alert>
          </Snackbar>
        </Box>
      );
    }
  },
);
// Type and required properties
FilterListFormMolecule.propTypes = {};
// Default properties
FilterListFormMolecule.defaultProps = {};

export default FilterListFormMolecule;
