import ButtonPreference from './ButtonPreference';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ButtonPreference is in the DOM', () => {
  render(<ButtonPreference></ButtonPreference>)
  expect(screen.getByTestId('ButtonPreferenceTestId')).toBeInTheDocument();
})
