import React, { useState, useEffect } from "react";
import { Core, Lab } from "@ebs/styleguide";
const { Button, Snackbar } = Core;
const { Alert } = Lab;

import Progress from "components/Atoms/Progress";
import { useLayoutContext, getCoreSystemContext } from "@ebs/layout";
import { client } from "utils/Apollo";
import { FIND_USER_LIST } from "components/helpers";
import { modifyUserPreference } from "components/helpers";

const ButtonPreferenceMolecule = React.forwardRef(({handleClose, columnsGrid }, ref) => {
  const { graphqlUri } = getCoreSystemContext();
  const { userProfile } = useLayoutContext();
  const [stateMessage, setStateMessage] = React.useState({
    open: false,
    vertical: "top",
    horizontal: "right",
  });

  const { vertical, horizontal, open } = stateMessage;
  if (!userProfile)
    return (
      <div>
        <Progress />
      </div>
    );

  const handleCloseSB = () => {
    setStateMessage({ ...stateMessage, open: false });
  };
  const savePreferenceColumns = async () => {
    let _userProfile = Object.assign({},userProfile);
    if (!columnsGrid.idGrid) {
      setStateMessage({
        open: true,
        vertical: "top",
        horizontal: "right",
      });
      console.error(
        "The grid needs a necessary identification, to be able to save the preference columns in the database, assign the property to the corresponding grid, example: id: value. Warning: the id value must be unique in each grid, there cannot be two grids with the same value in the id property."
      );
      handleClose();
    } else {
      let userInfo = null
      try {
        const { data } = await client(graphqlUri).query({
          query: FIND_USER_LIST,
          variables: {
            filters: [{ col: "userName", val: _userProfile.account, mod: "EQ" }]
          }
        })
        if (data.findUserList.content.length > 0)
          userInfo = data.findUserList.content[0];
        else return;
      } catch (error) {
        console.log(error)
        return;
      }

      let preferenceObject = _userProfile.preference;
      if (_userProfile.preference.favorites.columns.length === 0) {
        preferenceObject.favorites.columns.push(columnsGrid);
        modifyUserPreference({
          id: Number(userInfo.id),
          userName: userInfo.userName,
          active: userInfo.isActive,
          admin: userInfo.isAdmin,
          contactId: userInfo.contact.id,
          preference: preferenceObject,
        }, graphqlUri)
      } else {
        const idGridPreference = columnsGrid.idGrid;
        const dataIndexPreference =
          preferenceObject.favorites.columns.findIndex(
            (element) => element.idGrid === idGridPreference
          );
        if (dataIndexPreference >= 0) {
          preferenceObject.favorites.columns[dataIndexPreference] = columnsGrid;
          modifyUserPreference({
            id: Number(userInfo.id),
            userName: userInfo.userName,
            active: userInfo.isActive,
            admin: userInfo.isAdmin,
            contactId: userInfo.contact.id,
            preference: preferenceObject,
          }, graphqlUri)

        } else {
          preferenceObject.favorites.columns.push(columnsGrid);
          modifyUserPreference({
            id: Number(userInfo.id),
            userName: userInfo.userName,
            active: userInfo.isActive,
            admin: userInfo.isAdmin,
            contactId: userInfo.contact.id,
            preference: preferenceObject,
          }, graphqlUri)
        }
      }
      handleClose();
    }
  };

  return (
    <div style={{ paddingLeft: 93 + "%", marginTop: 10 + "px" }}>
      <Button
        onClick={savePreferenceColumns}
      >
        Save
      </Button>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleCloseSB}
        anchorOrigin={{ vertical, horizontal }}
      >
        <Alert
          severity="error"
          variant="filled"
          onClose={handleCloseSB}
          sx={{ width: "100%" }}
        >
          The grid needs a necessary identification, to be able to save the
          preference columns in the database, assign the property to the
          corresponding grid, example: id: value. Warning: the id value must be
          unique in each grid, there cannot be two grids with the same value in
          the id property.
        </Alert>
      </Snackbar>
    </div>
  );
});
// Type and required properties
ButtonPreferenceMolecule.propTypes = {};
// Default properties
ButtonPreferenceMolecule.defaultProps = {};

export default ButtonPreferenceMolecule;
