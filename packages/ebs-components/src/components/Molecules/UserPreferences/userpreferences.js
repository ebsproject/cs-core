import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Styles, Icons } from '@ebs/styleguide';
import HiddenColumns from '../../Atoms/HiddenColumns';
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Slide,
  Tooltip,
  Typography,
} = Core;
const { useTheme } = Styles;
const { Close, Settings } = Icons;
//import SettingsIcon from '@material-ui/icons/Settings';



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const UserPreferencesMolecule = React.forwardRef(({ ...rest }, ref) => {
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const classes = {
    icon: {
      rotate: '90deg',
    },
    closeButton: {
      position: 'absolute',
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    /* 
     @prop data-testid: Id to use inside userpreferences.test.js file.
     */
    <div ref={ref} data-testid={'UserPreferencesTestId'}>
      <Tooltip
        arrow
        title={
          <Typography className='font-ebs text-lg' placement='top'>
            Personalize grid settings
          </Typography>
        }
      >
        <Button
          variant='contained'
          className='bg-ebs-brand-default hover:bg-ebs-brand-900 text-white p-2 m-2 rounded-md'
          onClick={handleClickOpen}
          color='primary'
          aria-label='preferences'
          startIcon={<Settings className='ml-3' />}
        ></Button>
      </Tooltip>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        fullWidth
        maxWidth='md'
        keepMounted
        onClose={handleClose}
        aria-labelledby='alert-dialog-slide-title'
        aria-describedby='alert-dialog-slide-description'
      >
        <DialogTitle id='alert-dialog-slide-title'>
          <Typography variant="h4" color='secondary'>User Preferences</Typography>
          <IconButton
            aria-label='close'
            sx={classes.closeButton}
            onClick={handleClose}
          >
            <Close />
          </IconButton>
        </DialogTitle>
        <DialogContent>
          <HiddenColumns handleClose={handleClose} {...rest} />
        </DialogContent>
        <DialogActions>
          {/* <Button onClick={handleClose} color='secondary'>
            Close
          </Button>
          <ButtonPreference preference={preference} /> */}
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
UserPreferencesMolecule.propTypes = {};
// Default properties
UserPreferencesMolecule.defaultProps = {};

export default UserPreferencesMolecule;
