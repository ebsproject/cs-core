import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Styles, Icons } from '@ebs/styleguide';
const { useTheme } = Styles;
const { IconButton, ListItemIcon, ListItemText, Menu, MenuItem } = Core;
const {
  ArrowDownward,
  ArrowUpward,
  FormatIndentDecrease,
  FormatIndentIncrease,
  MoreVert,
  SortByAlpha,
} = Icons;

const StyledMenu = (props) => (
  <Menu
    sx={{ paper: { border: '1px solid #d3d4d5' } }}
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'center',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'center',
    }}
    {...props}
  />
);

const StyledMenuItem = ({ theme }) => <MenuItem />;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const MenuColumnMolecule = React.forwardRef(({ column }, ref) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const theme = useTheme();
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    /* 
     @prop data-testid: Id to use inside menucolumn.test.js file.
     */
    <div data-testid={'MenuColumnTestId'}>
      <IconButton onClick={handleClick} size='small' color='primary'>
        <MoreVert />
      </IconButton>
      <Menu
        id='customized-menu'
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        {/* // * If the column can be grouped, let's add a toggle */}
        {column.groupBy === true || column.groupBy === undefined ? (
          <MenuItem {...column.getGroupByToggleProps()}>
            <ListItemIcon>
              {column.isGrouped ? (
                <FormatIndentDecrease />
              ) : (
                <FormatIndentIncrease />
              )}
            </ListItemIcon>
            <ListItemText primary='Group by' />
          </MenuItem>
        ) : null}
        {/* // * If the column can be shorted, let's add a toggle */}
        {column.sort === true || column.sort === undefined ? (
          <MenuItem theme={theme} {...column.getSortByToggleProps()}>
            <ListItemIcon>
              {column.isSorted ? (
                column.isSortedDesc ? (
                  <ArrowUpward />
                ) : (
                  <ArrowDownward />
                )
              ) : (
                <SortByAlpha />
              )}
            </ListItemIcon>
            <ListItemText primary='Sort by' />
          </MenuItem>
        ) : null}
      </Menu>
    </div>
  );
});
// Type and required properties
MenuColumnMolecule.propTypes = {
  column: PropTypes.object.isRequired,
};
// Default properties
MenuColumnMolecule.defaultProps = {};

export default MenuColumnMolecule;
