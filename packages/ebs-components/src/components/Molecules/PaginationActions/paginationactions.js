import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Core, FirstPage,
  KeyboardArrowLeft,
  KeyboardArrowRight,
  LastPage, Styles
} from "@ebs/styleguide";
import NumericPagination from 'components/Atoms/NumericPagination';
const { IconButton } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PaginationActionsMolecule = React.forwardRef(
  ({ count, page, rowsPerPage, onPageChange, pageCount }, ref) => {
    const theme = useTheme();
    const classes = {
      root: {
        flexShrink: 0,
        marginLeft: theme.spacing(2.5),
      },
    };


    const handleFirstPageButtonClick = (event) => {
      onPageChange(event, 0);
    };

    const handleBackButtonClick = (event) => {
      onPageChange(event, page - 1);
    };

    const handleNextButtonClick = (event) => {
      onPageChange(event, page + 1);
    };

    const handleLastPageButtonClick = (event) => {
      onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
    };

    return (
      /* 
     @prop data-testid: Id to use inside paginationactions.test.js file.
     */
      <div sx={classes.root} data-testid={'PaginationActionsTestId'}>
        <IconButton
          onClick={handleFirstPageButtonClick}
          disabled={page === 0}
          aria-label='first page'
        >
          {theme.direction === 'rtl' ? <LastPage /> : <FirstPage />}
        </IconButton>
        <IconButton
          onClick={handleBackButtonClick}
          disabled={page === 0}
          aria-label='previous page'
        >
          {theme.direction === 'rtl' ? (
            <KeyboardArrowRight />
          ) : (
            <KeyboardArrowLeft />
          )}
        </IconButton>
        <NumericPagination
          page={page}
          pageCount={pageCount}
          onPageChange={onPageChange}
        />
        <IconButton
          onClick={handleNextButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label='next page'
        >
          {theme.direction === 'rtl' ? (
            <KeyboardArrowLeft />
          ) : (
            <KeyboardArrowRight />
          )}
        </IconButton>
        <IconButton
          onClick={handleLastPageButtonClick}
          disabled={page >= Math.ceil(count / rowsPerPage) - 1}
          aria-label='last page'
        >
          {theme.direction === 'rtl' ? <FirstPage /> : <LastPage />}
        </IconButton>
      </div>
    );
  },
);
// Type and required properties
PaginationActionsMolecule.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};
// Default properties
PaginationActionsMolecule.defaultProps = {};

export default PaginationActionsMolecule;
