import React, { useState, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import DropDownFilter from 'components/Atoms/DropDownFilter';
import { Core, Icons, Lab } from '@ebs/styleguide';
const {
  Grid,
  TextField,
  Typography,
  Box,
  Button,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  IconButton,
  Popover,
  Snackbar,
} = Core;
const { ArrowDropDown } = Icons;
const {Alert} = Lab;
import FilterListForm from 'components/Molecules/FilterListForm';
import Cookies from "js-cookie";
import Filter from "../../../assets/images/icons/filter.svg";
import { getContext, useLayoutContext } from '@ebs/layout';
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const FilterFormMolecule = React.forwardRef(
  ({ handleFilterClose, objectProperties, userProfile }, ref) => {
    const {setFilterListToApply} = useLayoutContext();
    const [data, setData] = useState(null);
    const [openDialog, setOpenDialog] = useState(false);
    const [applyContext, setApplyContext] = useState(false);
    const [openFilterList, sethandleFilterListOpen] = React.useState(false);
    const [filterName, setFilterName] = useState('');
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [openPop, setOpenPop] = React.useState(false);
    const [placement, setPlacement] = React.useState();
    const [values, setValues] = useState([]);
    const [openMessage, setOpenMessage] = React.useState(false);

    const handleClickPop = (newPlacement) => (event) => {
      setAnchorEl(event.currentTarget);
      setOpenPop((prev) => placement !== newPlacement || !prev);
      setPlacement(newPlacement);
    };
    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
      getValues,
    } = useForm({
      defaultValues: {},
    });
    const allowedTenantsByUser = objectProperties?.userInfo?.tenants.map((item) => {
      return item.id;
    });
    const context = getContext();
    if (!context) return <div>Loading...</div>;
    const handleCloseMessage = (event, reason) => {
      if (reason === 'clickaway') {
        return;
      }
      setOpenMessage(false);
    };
    const onSubmit = (data, e) => {
      setValues(Object.values(data));
      // save the preference in user profile
      if (applyContext) {
        setFilterListToApply({
          filterName: 'default',
          default: true,
          domainId: context.domainId,
          tenantId: context.tenantId,
          filterValues: data,
        });
        Cookies.remove("filters");
        setOpenMessage(true);
        setOpenDialog(false);
        handleFilterClose();
      } else {
        setData(data);
        setOpenDialog(true);
      }
    };
    const saveFilter = () => {
      let preferenceObject = Object.assign({},userProfile.preference);
      const userInfo = objectProperties.userInfo;
      preferenceObject.filters.push({
        filterName: filterName,
        default: preferenceObject.filters.length > 0 ? false : true,
        domainId: context.domainId,
        tenantId: context.tenantId,
        filterValues: data,
      });
      objectProperties.modifyUserPreference({
        id: Number(userInfo.id),
        userName: userInfo.userName,
        active: userInfo.isActive,
        contactId: userInfo.contact.id,
        preference: preferenceObject,
      })(objectProperties.dispatch);
      handleCloseDialog();
      setFilterListToApply({
        filterName: 'default',
        default: true,
        domainId: context.domainId,
        tenantId: context.tenantId,
        filterValues: data,
      });
      Cookies.remove("filters");
    };
    const handleCloseDialog = () => {
      setOpenDialog(false);
    };
    const handleFilterListOpen = () => {
      sethandleFilterListOpen(true);
    };
    const handleFilterListClose = () => {
      sethandleFilterListOpen(false);
    };
    const setFilterNameOnChange = (e) => {
      setFilterName(e.target.value);
    };
    return (
      /**
       * @prop data-testid: Id to use inside GridForm.test.js file.
       */
      <div>
        <Popover
          open={openPop}
          anchorEl={anchorEl}
          onClose={() => setOpenPop(false)}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}
        >
          <FilterListForm
            setOpenPop={setOpenPop}
            context={context}
            userProfile={userProfile}
            objectProperties={objectProperties}
          />
        </Popover>

        <Dialog
          onClose={handleFilterListClose}
          open={openFilterList}
          aria-label='filterList'
        >
          <DialogTitle>Filter Stored List</DialogTitle>
          <DialogContent>
            <FilterListForm
              userProfile={userProfile}
              objectProperties={objectProperties}
            />
            <DialogActions>
              <Button onClick={handleFilterListClose} color='secondary'>
                Close
              </Button>
            </DialogActions>
          </DialogContent>
        </Dialog>
        <Dialog
          fullWidth={true}
          onClose={handleCloseDialog}
          open={openDialog}
          aria-label='applyFilterDialog'
        >
          <DialogTitle>Save filter</DialogTitle>
          <DialogContent>
            <div className='flex flex-nowrap'>
              <TextField
                label='Filter Name'
                required
                name='filterName'
                onChange={setFilterNameOnChange}
                className='w-3/4'
              ></TextField>

              <Typography>{values ? values.map((i) => i?.name) : ''}</Typography>
            </div>

            <DialogActions>
              <Button onClick={handleCloseDialog} color='secondary'>
                Close
              </Button>
              <Button
                disabled={filterName.length > 1 ? false : true}
                onClick={() => saveFilter()}
              >
                Save
              </Button>
            </DialogActions>
          </DialogContent>
        </Dialog>

        <Box sx={{ flexGrow: 1, overflow: 'hidden', px: 3 }}>
          <form
            onSubmit={handleSubmit(onSubmit)}
            name='FilterForm'
            data-testid='filterForm'
          >
            <Box className='flex flex-row flex-nowrap justify-end mt-2'>
              <Box className='flex flex-nowrap grid-cols-2 gap-2 items-center'>
                <Box className='flex flex-nowrap justify-center gap-4 text-ebs-green-900 font-bold'>
                  <Box>
                    <img src={Filter} />
                  </Box>

                  <Typography
                    variant='h3'
                    className='font-ebs font-bold text-lg text-center'
                  >
                    Data Filter
                  </Typography>
                </Box>
                <div className='gap-1'>
                  <DialogActions>
                    <Button
                      type='submit'
                      className=' bg-gray-200 text-black p-1 m-1 hover:bg-gray-400 rounded-none'
                      onClick={() => {
                        setApplyContext(false);
                      }}
                    >
                      Save
                    </Button>
                    <IconButton
                      className='bg-gray-200 text-black hover:bg-gray-400'
                      onClick={
                        handleClickPop('bottom-end')
                        // sethandleFilterListOpen(true);
                      }
                      //}
                    >
                      <ArrowDropDown />
                    </IconButton>
                  </DialogActions>
                </div>
              </Box>
            </Box>
            <Button
              onClick={() => reset()}
              color='primary'
              className='font-bold text-ebs-green-900'
            >
              Clear all
            </Button>
            <Grid
              container
              ref={ref}
              data-testid={'GridFormTestId'}
              direction='row'
              justifyContent='flex-start'
              alignItems='flex-start'
              spacing={2}
              wrap={'nowrap'}
            >
              <Grid item xs>
                  <DropDownFilter
                    control={control}
                    setValue={setValue}
                    errors={errors}
                    allowedTenantsByUser={allowedTenantsByUser}
                  />
              </Grid>
            </Grid>
            <DialogActions>
              <Button
                data-testid='cancel'
                onClick={handleFilterClose}
                className=' text-black p-1 m-1 hover:bg-slate-500'
              >
                Cancel
              </Button>
              <Button
                data-testid='save'
                type='submit'
                onClick={() => setApplyContext(true)}
                className='bg-ebs-brand-default text-white p-1 m-1 hover:bg-ebs-brand-900'
              >
                Apply
              </Button>
            </DialogActions>
            <Box className='flex flex-row flex-nowrap justify-end mt-5'>
              <Box className='grid grid-cols-2 gap-1'>
                <Snackbar
                  anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                  }}
                  open={openMessage}
                  autoHideDuration={2000}
                  onClose={handleCloseMessage}
                >
                  <Alert
                    variant='filled'
                    onClose={handleCloseMessage}
                    severity='success'
                  >
                    Filter applied and added to context!!
                  </Alert>
                </Snackbar>
              </Box>
            </Box>
          </form>
        </Box>
      </div>
    );
  },
);
// Type and required properties
FilterFormMolecule.propTypes = {
  component: PropTypes.array,
  setValue: PropTypes.func,
  getValues: PropTypes.func,
  control: PropTypes.any,
  errors: PropTypes.object,
};
// Default properties
FilterFormMolecule.defaultProps = {};

export default FilterFormMolecule;
