import React, { useEffect, useState } from 'react';
import { Core } from '@ebs/styleguide';
const { TextField, MenuItem } = Core;
import { useLayoutContext, getCoreSystemContext } from '@ebs/layout';
import { client } from 'utils/Apollo';
import { FIND_USER_LIST } from 'components/helpers';
import { modifyUserPreference } from 'components/helpers';

const MenuItemGridMolecule = React.forwardRef(
  ({ pageSize, setPageSize, gotoPage, id }, ref) => {
    const { userProfile } = useLayoutContext();
    const { graphqlUri } = getCoreSystemContext();
    const [filterPreference, setFilterPreference] = useState(null);
    const [dataGrid, setDataGrid] = useState([]);

    useEffect(() => {
      if (userProfile) {
        let _dataGrid = [];
        userProfile.preference.favorites.columns.map(
          (item) => (_dataGrid = [..._dataGrid, item]),
        );
        let _filterPreference = _dataGrid.filter((item) => item.idGrid === id);
        setDataGrid(_dataGrid);
        setFilterPreference(_filterPreference);
      }
    }, [userProfile]);

    const handleChangeRowsPerPage = async (event) => {
      let _DG = [...dataGrid];
      let _userProfile = Object.assign({}, userProfile);
      let userInfo = null;
      try {
        const { data } = await client(graphqlUri).query({
          query: FIND_USER_LIST,
          variables: {
            filters: [{ col: 'userName', val: _userProfile.account, mod: 'EQ' }],
          },
        });
        if (data.findUserList.content.length > 0)
          userInfo = data.findUserList.content[0];
        else return;
      } catch (error) {
        console.log(error);
        return;
      }
      if (filterPreference.length !== 0) {
        if (_DG.length !== 0) {
          for (let i = 0; i < _DG.length; i++) {
            if (_DG[i].idGrid === id) {
              if (_DG[i].pagination) {
                _DG[i].pagination = event.target.value;
                _userProfile.preference.favorites.columns = _DG;
                await modifyUserPreference(
                  {
                    id: Number(userInfo.id),
                    userName: userInfo.userName,
                    active: userInfo.isActive,
                    admin: userInfo.isAdmin,
                    contactId: userInfo.contact.id,
                    preference: _userProfile.preference,
                  },
                  graphqlUri,
                );
              } else {
                _DG[i] = {
                  pagination: event.target.value,
                  ..._DG[i],
                };
                _userProfile.preference.favorites.columns = _DG;
                await modifyUserPreference(
                  {
                    id: Number(userInfo.id),
                    userName: userInfo.userName,
                    active: userInfo.isActive,
                    admin: userInfo.isAdmin,
                    contactId: userInfo.contact.id,
                    preference: _userProfile.preference,
                  },
                  graphqlUri,
                );
              }
            }
          }
        } else {
          _DG.push({
            idGrid: id,
            pagination: event.target.value,
          });
          _userProfile.preference.favorites.columns = _DG;
          await modifyUserPreference(
            {
              id: Number(userInfo.id),
              userName: userInfo.userName,
              active: userInfo.isActive,
              admin: userInfo.isAdmin,
              contactId: userInfo.contact.id,
              preference: _userProfile.preference,
            },
            graphqlUri,
          );
        }
      } else {
        _DG.push({
          idGrid: id,
          pagination: event.target.value,
        });
        _userProfile.preference.favorites.columns = _DG;
        await modifyUserPreference(
          {
            id: Number(userInfo.id),
            userName: userInfo.userName,
            active: userInfo.isActive,
            admin: userInfo.isAdmin,
            contactId: userInfo.contact.id,
            preference: _userProfile.preference,
          },
          graphqlUri,
        );
      }

      setPageSize(parseInt(event.target.value, 10));
      gotoPage(0);
    }; //SSSSS

    const pageSizeFunction = () => {
      const valuePagination = filterPreference.filter((item) => item.pagination);
      if (valuePagination.length > 0) {
        return valuePagination[0].pagination;
      } else {
        return pageSize;
      }
    };
    if (!filterPreference) return null;
    return (
      <div>
        <TextField
          className='place-self-center font-ebs px-3'
          select
          variant='outlined'
          size='small'
          value={pageSizeFunction()}
          onChange={handleChangeRowsPerPage}
        >
          <MenuItem value={10}>10</MenuItem>
          <MenuItem value={50}>50</MenuItem>
          <MenuItem value={100}>100</MenuItem>
        </TextField>
      </div>
    );
  },
);

MenuItemGridMolecule.propTypes = {};

export default MenuItemGridMolecule;
