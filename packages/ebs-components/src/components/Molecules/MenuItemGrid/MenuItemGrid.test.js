import MenuItemGrid from "./MenuItemGrid";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("MenuItemGrid is in the DOM", () => {
  render(<MenuItemGrid></MenuItemGrid>);
  expect(screen.getByTestId("MenuItemGridTestId")).toBeInTheDocument();
});
