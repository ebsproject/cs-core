import React from 'react';
import PropTypes from 'prop-types';
import { Core } from '@ebs/styleguide';
import { useWorkflow } from '../../../hooks/useWorkflow';
const { Button } = Core;

export const ButtonWorkflow = React.forwardRef(
  ({ className, id, jobWorkflowId, label, onFinish, recordId, style }, ref) => {
    const { sendNotification } = useWorkflow({
      jobWorkflowId,
    });

    const handleClick = () => {
      const result = sendNotification({ recordId });
      onFinish({ result });
    };

    return (
      <div id={`div-workflow_${id}`}>
        <Button
          ref={ref}
          className={className}
          data-testid={'ButtonWorkflowTestId'}
          id={`button-workflow_${id}`}
          onClick={() => {
            handleClick();
          }}
          style={style}
        >
          {label}
        </Button>
      </div>
    );
  },
);

ButtonWorkflow.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string.isRequired,
  jobWorkflowId: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
  onFinish: PropTypes.func,
  recordId: PropTypes.number.isRequired,
  style: PropTypes.object,
};

// export default ButtonWorkflow;
