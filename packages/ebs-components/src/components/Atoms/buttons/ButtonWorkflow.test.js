import ButtonWorkflow from './ButtonWorkflow';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

afterEach(cleanup)

test('ButtonWorkflow is in the DOM', () => {
  render(<ButtonWorkflow></ButtonWorkflow>)
  expect(screen.getByTestId('ButtonWorkflowTestId')).toBeInTheDocument();
})
