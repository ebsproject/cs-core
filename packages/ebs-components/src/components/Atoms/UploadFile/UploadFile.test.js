import UploadFile from './UploadFile';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('UploadFile is in the DOM', () => {
  render(<UploadFile></UploadFile>)
  expect(screen.getByTestId('UploadFileTestId')).toBeInTheDocument();
})
