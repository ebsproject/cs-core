import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core, DropzoneDialog } from "@ebs/styleguide";
import { Controller } from 'react-hook-form';
const { Tooltip, Button, Typography, Grid } = Core;
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const UploadFileAtom = React.forwardRef(
  ({ control, errors, name, label, helper, customProps, rules, defaultRules, getValues, watch }, ref) => {
    let field = null;
    if (defaultRules && defaultRules["showControlRules"]) {
      field = watch()[defaultRules.showControlRules.parentControl];
    }
    const [show, setShow] = useState(!defaultRules ? true : defaultRules["showControlRules"] ? false : true);
    const [open, setOpen] = React.useState(false);
    const [fileNames, setFileNames] = useState("");

    useEffect(() => {
      if (field !== null) {
        if(field !== undefined){
          if (String(field) === defaultRules.showControlRules.expectedValue)
            setShow(true);
          else
            setShow(false);
        }
      }
    }, [field]);
    const onClick = (files) => {
      let _names = []
      if (files && files.length > 0)
        _names = files.map(file => file.name);
      setFileNames(_names.join(","));
      setOpen(!open);
    };
    if (!show) return null;
    return (
      /**
       * @prop data-testid: Id to use inside UploadFile.test.js file.
       */
      <>
        {helper ? (
          <Tooltip
            {...helper}
            title={
              <Typography className='font-ebs text-xl'>{helper.title}</Typography>
            }
          >
            <Grid container direction="row">
              <Grid item xs={6}>
                <Button
                  data-testid={'UploadFileTestId'}
                  ref={ref}
                  onClick={onClick}
                  {...customProps?.button}
                >
                  {`${label} ${(rules?.required && '*') || ''}`}
                </Button>
              </Grid>
              <Grid item xs={6}>
                <Typography variant="body1"> {fileNames} </Typography>
              </Grid>
            </Grid>
          </Tooltip>
        ) : (
          <Button
            data-testid={'UploadFileTestId'}
            onClick={onClick}
            {...customProps?.button}
          >
            {`${label} ${(rules?.required && '*') || ''}`}
          </Button>
        )}
        <Controller
          control={control}
          name={name}
          render={({ field: { onChange, value, ref } }) => (
            <DropzoneDialog
              error={errors[name]}
              open={open}
              onClose={onClick}
              value={value}
              onSave={(files) => {
                onChange(files);
                onClick(files);
              }}
              inputRef={ref}
              {...customProps?.input}
            />
          )}
          rules={rules}
        />
        <Typography
        component={'span'}
        color={"error"}
        >
          {errors[name] && errors[name].message}
        </Typography>
      </>
    );
  },
);
// Type and required properties
UploadFileAtom.propTypes = {
  control: PropTypes.any,
  errors: PropTypes.object,
  name: PropTypes.string.isRequired,
  label: PropTypes.node,
  helper: PropTypes.object,
  customProps: PropTypes.object,
  rules: PropTypes.object,
};
// Default properties
UploadFileAtom.defaultProps = {};

export default UploadFileAtom;
