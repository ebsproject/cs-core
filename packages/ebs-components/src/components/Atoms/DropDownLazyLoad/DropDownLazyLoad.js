import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
// CORE COMPONENTS
import { Core, Lab } from '@ebs/styleguide';
import { useEbsFormContext } from 'context/ebsForm-context';
const { Autocomplete } = Lab;
const { Checkbox, TextField, Tooltip, Typography } = Core;
import { buildQuery } from 'functions/query';
import { client } from 'utils/Apollo';
import { transform } from 'node-json-transform';

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DropDownFormAtom = React.forwardRef(
  (
    {
      getValues,
      setValue,
      control,
      errors,
      name,
      helper,
      onChange,
      options,
      inputProps,
      rules,
      defaultRules,
    },
    ref,
  ) => {

    const { setAllValues, allValues } = useEbsFormContext();
    const [optionsValues, setOptionsValues] = useState(options);

    useEffect(() => {
      if (allValues && defaultRules) {
        const query = buildQuery(defaultRules.entity, defaultRules.apiContent);
        const filter = allValues[`${defaultRules.parentControl}`];
        const fetchData = async () => {
          try {
            const { data } = await client(`${defaultRules.uri}`).query({
              query: query,
              variables: {
                filters: [{ col: defaultRules.columnFilter, mod: "EQ", val: filter.value }]
              }
            })
            const val = data[`find${defaultRules.entity}List`].content[0];
            const mapObject = {
              item: {
                value: "id",
                label: defaultRules.label
              }
            }

            const value = val[`${defaultRules.field}`];
            let to = [{ ...value, id: val.id }]
            const optionTrans = transform(Array.isArray(value) ? value : to, mapObject);

            const finalOptions = optionTrans.map(item =>{
              let label = item.label;
              if(Array.isArray(label)){
                return {value:item.value, label: label.join(", ")}
              }else
              return item;
            })

            setOptionsValues(finalOptions);
            
          } catch (error) {
            setOptionsValues([]);
          }

        }
        fetchData();
      }
    }, [allValues]);


    const onActionChange = (e, options) => {
      setAllValues(prev => ({ ...prev, [`${name}`]: options }));
      setValue(name, options);
      onChange(e, options);
    };
    const optionLabel = (option) => {
      return `${option.label || ""}`;
    };
    const renderOption = (option, { selected }) => (
      <>
        <Checkbox checked={selected} />
        {option.label}
      </>
    );
    /**
     * @param {Any} label
     * @returns node
     */
    const getLabel = (label) => {
      return (
        <Typography
          component={'span'}
          className='font-ebs text-2xl font-bold'
        >{`${label} ${(rules?.required && '*') || ''}`}</Typography>
      );
    };
    const input = (params) => {
      return (
        <TextField
          {...params}
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors[name])}
          error={Boolean(errors[name])}
          label={getLabel(inputProps.label)}
          helperText={errors[name]?.message}
          variant={inputProps[`variant`]}
        />
      );
    };
    /**
     * @param {Object} option
     * @param {Object} value
     * @returns {Boolean}
     */
    const optionSelected = (option, value) => {
      return option.value === value.value;
    };

    return (
      <Controller
        control={control}
        name={name}
        render={({ field }) =>
          helper ? (
            <Tooltip
              {...helper}
              title={
                <Typography className='font-ebs text-xl'>{helper.title}</Typography>
              }
            >
              <Autocomplete
                {...field}
                id={name}
                data-testid={name}
                fullWidth
                getOptionLabel={optionLabel}
                isOptionEqualToValue={optionSelected}
                onChange={onActionChange}
                options={optionsValues || []}
                renderInput={input}
                renderOption={renderOption}
                {...inputProps}
                value={
                  inputProps?.multiple
                    ? (field?.value?.length && field.value) || []
                    : field?.value || { value: 0, label: '' }
                }
              />
            </Tooltip>
          ) : (
            <Autocomplete
              {...field}
              id={name}
              data-testid={name}
              fullWidth
              getOptionLabel={optionLabel}
              isOptionEqualToValue={optionSelected}
              inputValue={getValues(name)?.name || ''}
              onChange={onActionChange}
              options={optionsValues || []}
              renderInput={input}
              renderOption={renderOption}
              {...inputProps}
              value={
                inputProps?.multiple
                  ? (field?.value?.length && field.value) || []
                  : field?.value || {}
              }
            />
          )
        }
        rules={rules}
      />
    );
  },
);
// Type and required properties
DropDownFormAtom.propTypes = {
  onChange: PropTypes.func.isRequired,
  getValues: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  control: PropTypes.any,
  errors: PropTypes.object,
  name: PropTypes.string.isRequired,
  helper: PropTypes.object,
  options: PropTypes.array,
  inputProps: PropTypes.object,
  rules: PropTypes.object,
};
// Default properties
DropDownFormAtom.defaultProps = {
  onChange: (e, options) => { },
};

export default DropDownFormAtom;
