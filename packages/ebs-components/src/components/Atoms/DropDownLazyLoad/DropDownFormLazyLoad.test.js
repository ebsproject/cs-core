import DropDownForm from './DropDownLazyLoad';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('DropDownForm is in the DOM', () => {
  render(<DropDownForm></DropDownForm>)
  expect(screen.getByTestId('DropDownFormTestId')).toBeInTheDocument();
})
