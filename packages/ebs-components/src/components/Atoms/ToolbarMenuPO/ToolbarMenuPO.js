import React, { useEffect, useState } from 'react';
// GLOBALIZATION COMPONENT
// import { FormattedMessage } from "react-intl";
import { Core, Icons } from '@ebs/styleguide';
const {
  Typography,
  Button,
  Grow,
  Paper,
  ClickAwayListener,
  Grid,
  Popper,
  List,
  ListItem,
} = Core;
const { ArrowDropDown, PrintRounded } = Icons;
import { findPrintOutList } from 'utils/Axios';
import Cookies from 'js-cookie';
import PrintPreview from './modal-preview';
//const PrintPreview = React.lazy(() => import('./modal-preview'));
//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ToolbarMenuPOAtom = React.forwardRef(({ rowData }, ref) => {
  const [openTemplate, setOpenTemplate] = React.useState(false);
  const [open, setOpen] = React.useState(false);
  const [templateList, setTemplateList] = React.useState(null);
  const anchorRefG = React.useRef(null);
  const [productSelected, setProductSelected] = useState(null);
  const [props, setProps] = useState({});
  useEffect(() => {
    const _ps =
      Cookies.get('productSelected') && JSON.parse(Cookies.get('productSelected'));
    setProductSelected(_ps);
  }, []);
  useEffect(() => {
    if (productSelected) {
      const fetchList = async () => {
        try {
          const dataTemplate = await findPrintOutList(productSelected.id);
          setTemplateList(dataTemplate);
        } catch (error) {
          setTemplateList([]);
        }
      };
      fetchList();
    }
  }, [productSelected]);
  const handleToggle = () => {
    setOpenTemplate((prevOpenTemplate) => !prevOpenTemplate);
  };
  const closeModal = () => {
    setOpen(false);
  };
  const launchReportPreview = (report) => {
    let _props = {};
    if (rowData.length > 0) {
      _props = {
        reportName: report.name,
        paramValues: { name: 'id', value: rowData[0].id },
      };
    } else {
      _props = {
        reportName: report.name,
      };
    }
    setProps(_props);
    handleToggle();
  };
  const handleClose = (event) => {
    if (anchorRefG.current && anchorRefG.current.contains(event.target)) {
      return;
    }
    setOpenTemplate(false);
  };
  return templateList === null || templateList.length === 0 ? null : (
    <div className='ml-2'>
      <PrintPreview handleClose={closeModal} props={props} open={open} />
      <Grid container direction='column' alignItems='center'>
        <Grid item xs={12}>
          <Button
            startIcon={<PrintRounded className='fill-current text-white ml-3' />}
            ref={anchorRefG}
            onClick={handleToggle}
          >
            <Typography className='text-white text-sm font-ebs'>
              {'Printout'}
            </Typography>
            <ArrowDropDown />
          </Button>
          <Popper open={openTemplate} anchorEl={anchorRefG.current} transition>
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                style={{
                  transformOrigin:
                    placement === 'bottom' ? 'center top' : 'center bottom',
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={handleClose}>
                    <List id='split-button-menu-template'>
                      {templateList &&
                        templateList.map((option, index) => (
                          <ListItem
                            key={option.name}
                            onClick={() => {
                              setOpen(true);
                              launchReportPreview(option);
                            }}
                            style={{ cursor: 'pointer' }}
                          >
                            <PrintRounded color={'primary'} />
                            {option.label === null
                              ? option.name.toUpperCase()
                              : option.label.toUpperCase()}
                          </ListItem>
                        ))}
                    </List>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Popper>
        </Grid>
      </Grid>
    </div>
  );
});
// Type and required properties
ToolbarMenuPOAtom.propTypes = {};
// Default properties
ToolbarMenuPOAtom.defaultProps = {};

export default ToolbarMenuPOAtom;
