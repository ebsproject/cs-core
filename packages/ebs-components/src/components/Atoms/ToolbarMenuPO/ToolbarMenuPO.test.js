import ToolbarMenuPO from './ToolbarMenuPO';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ToolbarMenuPO is in the DOM', () => {
  render(<ToolbarMenuPO></ToolbarMenuPO>)
  expect(screen.getByTestId('ToolbarMenuPOTestId')).toBeInTheDocument();
})
