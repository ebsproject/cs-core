import { Core, Icons } from '@ebs/styleguide';
const { Typography, Dialog, DialogContent, Toolbar, IconButton}  = Core;
const { Close } = Icons;
//import { POReportDesigner } from "@ebs/po"
import { lazy, Suspense } from 'react';
const POReportDesigner = lazy(() => import('@ebs/po').then(module => ({ default: module.POReportDesigner })));
const ModalPrintPreview = ({ open, handleClose, props }) => {
    if(!open) return null
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            fullScreen
        >
            <Toolbar className="bg-ebs-brand-default text-white font-ebs" >
                <IconButton
                    edge="start"
                    color="inherit"
                    onClick={handleClose}
                    aria-label="close"
                >
                    <Close />
                </IconButton>
                <Typography>
                    {"Report Preview"}
                </Typography>
            </Toolbar>
            <DialogContent>
                <Suspense fallback={"Loading Preview....."}>
                <POReportDesigner {...props} mode={"preview"} />
                </Suspense>
            </DialogContent>
        </Dialog>)
}
export default ModalPrintPreview;