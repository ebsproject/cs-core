import InfoMessage from './InfoMessage';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('InfoMessage is in the DOM', () => {
  render(<InfoMessage></InfoMessage>)
  expect(screen.getByTestId('InfoMessageTestId')).toBeInTheDocument();
})
