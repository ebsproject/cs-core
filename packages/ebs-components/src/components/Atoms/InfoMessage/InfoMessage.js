import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {Core,Lab} from "@ebs/styleguide";
const { Typography, Collapse } = Core;
const {Alert} = Lab

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const InfoMessageAtom = React.forwardRef(({ errorMessage }, ref) => {
  return (
    /**
     * @prop data-testid: Id to use inside InfoMessage.test.js file.
     */
    <Collapse in={errorMessage ? true : false} className='w-full'>
      <Alert
        data-testid={'InfoMessageTestId'}
        ref={ref}
        className='w-full'
        severity='error'
      >
        <Typography className='font-ebs text-lx'>
          {errorMessage?.toString()}
        </Typography>
      </Alert>
    </Collapse>
  );
});
// Type and required properties
InfoMessageAtom.propTypes = {
  errorMessage: PropTypes.any,
};
// Default properties
InfoMessageAtom.defaultProps = {};

export default InfoMessageAtom;
