import RadioForm from './RadioForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('RadioForm is in the DOM', () => {
  render(<RadioForm></RadioForm>)
  expect(screen.getByTestId('RadioFormTestId')).toBeInTheDocument();
})
