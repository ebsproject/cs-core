import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const {
  RadioGroup,
  FormControl,
  FormControlLabel,
  Radio,
  FormLabel,
  Tooltip,
  Typography,
} = Core;
import { useEbsFormContext } from 'context/ebsForm-context';
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const RadioFormAtom = React.forwardRef(
  ({ control, errors, row, name, label, helper, onChange, options, rules }, ref) => {
    const { setAllValues } = useEbsFormContext();
    const handleChange = (e, field) => {
      onChange && onChange(e.target.value);
      field.onChange(e);
      setAllValues(prev => ({ ...prev, [`${name}`]: { value: e.target.value } }))
    }
    return (
      /**
       * @prop data-testid: Id to use inside RadioForm.test.js file.
       */
      <FormControl
        data-testid={'RadioFormTestId'}
        ref={ref}
        component='fieldset'
        fullWidth
        error={errors[name] ? true : false}
      >
        {helper ? (
          <>
            <FormLabel component='legend'>
              <Typography
                 variant={"h6"}
              >{`${label} ${(rules?.required && '*') || ''}`}</Typography>
            </FormLabel>
            <Controller
              control={control}
              name={name}
              render={({ field }) => (
                <Tooltip
                  {...helper}
                  title={
                    <Typography className='font-ebs text-xl'>
                      {helper.title}
                    </Typography>
                  }
                >
                  <RadioGroup
                    {...field}
                    row={row}
                    aria-label={name}
                    onChange={(e) => handleChange(e, field)}
                  >
                    {options.map((radio) => (
                      <FormControlLabel
                        checked={radio.value?.toString() === field.value?.toString()}
                        key={radio.value}
                        control={<Radio {...radio} />}
                        {...radio}
                      />
                    ))}
                  </RadioGroup>
                </Tooltip>
              )}
              rules={rules}
            />
            {errors[name] && (
              <Typography color={"error"}>
                {errors[name].message}
              </Typography>
            )}
          </>
        ) : (
          <>
            <FormLabel component='legend'>
              <Typography
                   variant={"h6"}
              >{`${label} ${(rules?.required && '*') || ''}`}</Typography>
            </FormLabel>
            <Controller
              control={control}
              name={name}
              render={({ field }) => (
                <RadioGroup
                  {...field}
                  row={row}
                  aria-label={name}
                  onChange={(e) => handleChange(e, field)}
                >
                  {options.map((radio) => (
                    <FormControlLabel
                      checked={radio.value?.toString() === field.value?.toString()}
                      key={radio.value}
                      control={<Radio {...radio} />}
                      {...radio}
                    />
                  ))}
                </RadioGroup>
              )}
              rules={rules}
            />
            {errors[name] && (
              <Typography color={"error"}>
                {errors[name].message}
              </Typography>
            )}
          </>
        )}
      </FormControl>
    );
  },
);
// Type and required properties
RadioFormAtom.propTypes = {
  control: PropTypes.any,
  errors: PropTypes.object,
  row: PropTypes.bool,
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  helper: PropTypes.object,
  onChange: PropTypes.func,
  options: PropTypes.array,
  rules: PropTypes.object,
};
// Default properties
RadioFormAtom.defaultProps = {};

export default RadioFormAtom;
