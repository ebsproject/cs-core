import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core, Styles, Icons } from '@ebs/styleguide';
import Select from '../Select';
const { Box, List, ListSubheader, Typography } = Core;
const { useTheme } = Styles;
const { Forward, Visibility, VisibilityOff } = Icons;

import ButtonPreference from 'components/Molecules/ButtonPreference';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const HiddenColumnsAtom = React.forwardRef(
  ({ allColumns, id, objectColumns, handleClose }, ref) => {
    const theme = useTheme();
    const classes = {
      root: {
        width: '100%',
      },
      list: {
        height: '40vh',
        backgroundColor: theme.palette.background.default,
        position: 'relative',
        overflow: 'auto',
      },
      icon: {
        fontSize: 60,
        color: theme.palette.background.paper,
      },
      iconRotate: {
        fontSize: 60,
        rotate: '-180deg',
        color: theme.palette.text.hint,
      },
    };
    let object = allColumns
      .filter((item) => !item.isVisible)
      .map((item) => {
        return { isVisible: item.isVisible, nameColumn: item.id };
      });
    const arrayData = () => {
      if (objectColumns?.length === 0) {
        const array = { idGrid: id, columns: object };
        return array;
      } else {
        const array = {
          idGrid: id,
          columns: object,
          pagination: objectColumns?.[0]?.pagination,
        };

        return array;
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside hiddencolumns.test.js file.
     */
      <>
        <div data-testid={'HiddenColumnsTestId'} ref={ref} sx={classes.root}>
          <Box display='flex' alignItems='center'>
            <Box flexGrow={1}>
              <List
                sx={classes.list}
                subheader={
                  <ListSubheader>
                    <Typography variant='h5' color='secondary'>
                      Displayed Columns
                    </Typography>
                  </ListSubheader>
                }
              >
                {allColumns
                  .filter((column) => column.isVisible)
                  .map((column, key) => {
                    var nameColumn = column.id;
                    if (
                      nameColumn === 'indexing' ||
                      nameColumn === 'selection' ||
                      nameColumn === 'rowActions' ||
                      nameColumn === 'details' ||
                      nameColumn === 'status.name'
                    ) {
                      return null;
                    }
                    if (key === 2 || key === 3) {
                      return (
                        <Box
                          display='flex'
                          justifyContent='flex-start'
                          flexDirection='row'
                          alignItems='center'
                          key={key}
                        >
                          <Box>
                            <Select disabled icon={<VisibilityOff />} />
                          </Box>
                          <Box>
                            {column.accessor ? (
                              column.Header
                            ) : (
                              <Typography
                                variant='subtitle1'
                                color='primary'
                                gutterBottom
                              >
                                {column.id}
                              </Typography>
                            )}
                          </Box>
                        </Box>
                      );
                    } else {
                      return (
                        <Box
                          display='flex'
                          justifyContent='flex-start'
                          flexDirection='row'
                          alignItems='center'
                          key={key}
                        >
                          <Box>
                            <Select
                              checkedIcon={<Visibility />}
                              icon={<VisibilityOff />}
                              {...column.getToggleHiddenProps()}
                            />
                          </Box>
                          <Box>
                            {column.accessor ? (
                              column.Header
                            ) : (
                              <Typography
                                variant='subtitle1'
                                color='primary'
                                gutterBottom
                              >
                                {column.id}
                              </Typography>
                            )}
                          </Box>
                        </Box>
                      );
                    }
                  })}
              </List>
            </Box>
            <Box>
              {/* <Forward className={classes.iconRotate} /> */}
              <Forward sx={classes.icon} />
            </Box>
            <Box flexGrow={1}>
              <List
                sx={classes.list}
                subheader={
                  <ListSubheader>
                    <Typography variant='h5' color='secondary'>
                      Hidden Columns
                    </Typography>
                  </ListSubheader>
                }
              >
                {allColumns
                  .filter((column) => {
                    if (!column.isVisible) {
                      if (!column.hidden) {
                        return column;
                      }
                    }
                  })
                  .map((column, key) => {
                    var nameColumn = column.id;
                    if (
                      nameColumn === 'indexing' ||
                      nameColumn === 'selection' ||
                      nameColumn === 'rowActions' ||
                      nameColumn === 'details'
                    ) {
                      return null;
                    } else {
                      return (
                        <Box
                          display='flex'
                          justifyContent='flex-start'
                          flexDirection='row'
                          alignItems='center'
                          key={key}
                        >
                          <Box>
                            <Select
                              checkedIcon={<Visibility />}
                              icon={<VisibilityOff />}
                              {...column.getToggleHiddenProps()}
                            />
                          </Box>
                          <Box>
                            {column.accessor ? (
                              column.Header
                            ) : (
                              <Typography
                                variant='subtitle1'
                                color='primary'
                                gutterBottom
                              >
                                {column.id}
                              </Typography>
                            )}
                          </Box>
                        </Box>
                      );
                    }
                  })}
              </List>
            </Box>
          </Box>
        </div>
        <ButtonPreference handleClose={handleClose} columnsGrid={arrayData()} />
      </>
    );
  },
);
// Type and required properties
HiddenColumnsAtom.propTypes = {};
// Default properties
HiddenColumnsAtom.defaultProps = {};

export default HiddenColumnsAtom;
