import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {Core, Styles} from "@ebs/styleguide";
const { useTheme } = Styles;
const {
  Input,
  InputLabel,
  MenuItem,
  FormControl,
  ListItemText,
  Select,
  Checkbox,
} =Core;


const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 200,
    },
  },
};

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const CustomColumnsAtom = React.forwardRef((props, ref) => {
  const theme = useTheme();
  const classes = {
    formControl: {
      margin: theme.spacing(1),
      minWidth: '100%',
      maxWidth: '100%',
    },
  };
  // Properties of the atom
  const { allColumns, getToggleHideAllColumnsProps, ...rest } = props;
  const [personName, setPersonName] = React.useState([]);
  const handleChange = (event) => {
    setPersonName(event.target.value);
  };

  return (
    /* 
     @prop data-testid: Id to use inside customcolumns.test.js file.
     */
    <FormControl
      data-testid='CustomColumnsTestId'
      sx={classes.formControl}
      ref={ref}
    >
      <InputLabel>Columns</InputLabel>
      <Select
        variant='standard'
        multiple
        value={personName}
        onChange={handleChange}
        input={<Input />}
        renderValue={(selected) => `Custom`}
        MenuProps={MenuProps}
      >
        {allColumns.map((column) =>
          !column.hidden ? (
            <MenuItem key={column.id} value={column.id}>
              <Checkbox color='default' {...column.getToggleHiddenProps()} />
              <ListItemText primary={column.id} />
            </MenuItem>
          ) : null,
        )}
      </Select>
    </FormControl>
  );
});
// Type and required properties
CustomColumnsAtom.propTypes = {};

export default CustomColumnsAtom;
