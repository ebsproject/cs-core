import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
const { Tooltip, FormControlLabel, Switch, Typography } = Core;
import { Controller } from 'react-hook-form';
import { useEbsFormContext } from 'context/ebsForm-context';

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const SwitchFormAtom = React.forwardRef(
  (
    {
      defaultRules,
      setValue,
      getValues,
      control,
      errors,
      name,
      label,
      helper,
      onChange,
      inputProps,
      rules,
      watch
    },
    ref,
  ) => {
    let field = null;
    if (defaultRules && defaultRules["showControlRules"]) {
      field = watch()[defaultRules.showControlRules.parentControl];
    }
    const [show, setShow] = useState(!defaultRules ? true : defaultRules["showControlRules"] ? false : true);
    useEffect(() => {
      if (field !== null) {
        if(field !== undefined){
          if (String(field) === defaultRules.showControlRules.expectedValue)
            setShow(true);
          else
            setShow(false);
        }
      }
    }, [field]);

    const { setBlockChanges } = useEbsFormContext();
    const handleOnChange = (e, field) => {
      onChange && setBlockChanges(e.target.checked);
      field.onChange(e.target.checked);
      onChange && onChange(e, { setValue, getValues });
    }
  if(!show) return null;
    return (
      /**
       * @prop data-testid: Id to use inside SwitchForm.test.js file.
       */
      <>
        <Controller
          control={control}
          name={name}
          render={({ field }) =>
            helper ? (
              <Tooltip
                {...helper}
                title={
                  <Typography className='font-ebs text-xl'>
                    {helper.title}
                  </Typography>
                }
              >
                <FormControlLabel
                  control={
                    <Switch
                      {...field}
                      data-testid={name}
                      checked={field.value}
                      onChange={(e) => handleOnChange(e,field)}
                      {...inputProps}
                    />
                  }
                  label={(rules && rules.required && label + ' *') || label}
                />
              </Tooltip>
            ) : (
              <FormControlLabel
                control={
                  <Switch
                    value={field.value}
                    inputRef={ref}
                    checked={field.value}
                    onChange={(e) => handleOnChange(e,field)}
                    {...inputProps}
                  />
                }
                label={(rules && rules.required && label + ' *') || label}
              />
            )
          }
          rules={rules}
        />
        {errors[name] && (
          <Typography className='font-ebs text-red-600 text-lg'>
            {errors[name].message}
          </Typography>
        )}
      </>
    );
  },
);
// Type and required properties
SwitchFormAtom.propTypes = {
  control:PropTypes.any,
  errors:PropTypes.object,
  name:PropTypes.string.isRequired,
  label:PropTypes.node,
  helper:PropTypes.object,
  onChange:PropTypes.func,
  inputProps:PropTypes.object,
  rules:PropTypes.object,
};
// Default properties
SwitchFormAtom.defaultProps = {};

export default SwitchFormAtom;
