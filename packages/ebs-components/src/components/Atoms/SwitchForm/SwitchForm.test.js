import SwitchForm from './SwitchForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('SwitchForm is in the DOM', () => {
  render(<SwitchForm></SwitchForm>)
  expect(screen.getByTestId('SwitchFormTestId')).toBeInTheDocument();
})
