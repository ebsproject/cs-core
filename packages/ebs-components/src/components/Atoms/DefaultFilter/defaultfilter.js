import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { useAsyncDebounce } from 'react-table';
import { Core } from '@ebs/styleguide';
const { TextField } = Core;

//MAIN FUNCTION
/*
 @prop column: properties to handle filters
 @param ref: reference made by React.forward
*/
const DefaultFilterAtom = (
  { column: { filterValue, setFilter } },
  setResetPage,
  refresh,
) => {
  const [value, setValue] = React.useState(filterValue);

  useEffect(() => {
    setValue(filterValue);
  }, [setFilter]);

  const onChange = useAsyncDebounce((value) => {
    setFilter(value.trim() || undefined);
    setResetPage(true);
  }, 400);
  return (
    /* 
     @prop data-testid: Id to use inside defaultfilter.test.js file.
     */
    <div data-testid={'DefaultFilterTestId'}>
      <TextField
        fullWidth
        variant='standard'
        value={value || ''}
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value); // * Set undefined to remove the filter entirely
        }}
      />
    </div>
  );
};

// * Type and required properties
DefaultFilterAtom.propTypes = {
  filterValue: PropTypes.string,
  setFilter: PropTypes.func.isRequired,
};
// * Default properties
DefaultFilterAtom.defaultProps = {
  filterValue: '',
};

export default DefaultFilterAtom;
