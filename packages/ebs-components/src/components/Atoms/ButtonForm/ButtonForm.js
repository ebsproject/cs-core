import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
const { Button, Tooltip, Typography } = Core;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ButtonFormAtom = React.forwardRef(
  ({ label, helper, onClick, inputProps }, ref) => {
    if (helper) {
      return (
        /**
         * @prop data-testid: Id to use inside ButtonForm.test.js file.
         */
        <Tooltip
          {...helper}
          title={
            <Typography className='font-ebs text-xl'>{helper.title}</Typography>
          }
        >
          <span>
            <Button
              data-testid={'ButtonFormTestId'}
              ref={ref}
              onClick={onClick}
              {...inputProps}
            >
              {label}
            </Button>
          </span>
        </Tooltip>
      );
    } else {
      return (
        /**
         * @prop data-testid: Id to use inside ButtonForm.test.js file.
         */
        <Button
          data-testid={'ButtonFormTestId'}
          ref={ref}
          onClick={onClick}
          {...inputProps}
        >
          {label}
        </Button>
      );
    }
  },
);
// Type and required properties
ButtonFormAtom.propTypes = {
  label: PropTypes.node,
  helper: PropTypes.object,
  onClick: PropTypes.func,
  inputProps: PropTypes.object,
};
// Default properties
ButtonFormAtom.defaultProps = {};

export default ButtonFormAtom;
