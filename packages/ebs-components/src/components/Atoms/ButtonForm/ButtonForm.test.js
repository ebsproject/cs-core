import ButtonForm from './ButtonForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ButtonForm is in the DOM', () => {
  render(<ButtonForm></ButtonForm>)
  expect(screen.getByTestId('ButtonFormTestId')).toBeInTheDocument();
})
