import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
// CORE COMPONENTS
import { Core, Lab } from '@ebs/styleguide';
import { useEbsFormContext } from 'context/ebsForm-context';
const { Autocomplete } = Lab;
const { Checkbox, TextField, Tooltip, Typography } = Core;
import { buildQuery } from 'functions/query';
import { client } from 'utils/Apollo';
import { transform } from 'node-json-transform';
import { getSgContext } from '../TextFieldForm/functions';
import { getOptions } from './functions';
import spinning from "./utils/spinning.gif"

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DropDownFormAtom = React.forwardRef(
  (
    {
      defaultValues,
      getValues,
      setValue,
      control,
      errors,
      name,
      helper,
      onChange,
      options,
      inputProps,
      rules,
      defaultRules,
      watch,
    },
    ref,
  ) => {
    let filter = null;
    let field = null;
    if (defaultRules) {
      filter = watch()[defaultRules.parentControl];
    }
    if (defaultRules && defaultRules['showControlRules'])
      field = watch()[defaultRules.showControlRules.parentControl];
    const [show, setShow] = useState(
      !defaultRules ? true : defaultRules['showControlRules'] ? false : true,
    );
    const {
      setAllValues,
      refresh,
      allowEdit,
      setAllowEdit,
      blockChanges,
      setBlockChanges,
    } = useEbsFormContext();
    const [optionsValues, setOptionsValues] = useState([]);
    const [showSpinning, setShowSpinning] = useState(false);
    useEffect(() => {
      if (defaultRules && defaultRules['applyRules'] === false && defaultRules.fieldAttributes && options.length === 0) {
        const getAllOptions = async () => {
          setShowSpinning(true);
          options = await getOptions({ fieldAttributes: defaultRules.fieldAttributes });
          setOptionsValues(options);
          setShowSpinning(false);
        }
        getAllOptions();
      }
    }, []);
    useEffect(() => {
      if (field !== null) {
        if (field !== undefined) {
          const { expectedValue } = defaultRules.showControlRules;
          const defaultValue = getValues(name);
          if (defaultValue && defaultValue.length > 0) setShow(true);
          if (typeof field === 'object' && field !== null) {
            if (field.label === expectedValue) setShow(true);
            else setShow(false);
          } else {
            let _value = String(field);
            if (_value === expectedValue) setShow(true);
            else setShow(false);
          }
        }
      }
    }, [field]);

    useEffect(() => {
      if (!blockChanges) {
        if (filter && defaultRules && defaultRules.applyRules) {
          const query = buildQuery(defaultRules.entity, defaultRules.apiContent);
          const fetchData = async () => {
            try {
              const sgContext = await getSgContext(defaultRules.uri);
              let defaultFilters = [{ col: defaultRules.columnFilter, mod: "EQ", val: Number(filter.value) }];
              if (defaultRules.customFilters && defaultRules.customFilters.length > 0) {
                defaultFilters = [...defaultFilters, ...defaultRules.customFilters]
              }
              const { data } = await client(sgContext).query({
                query: query,
                variables: {
                  filters: defaultFilters,
                },
                fetchPolicy: 'no-cache',
              });
              const val = data[`find${defaultRules.entity}List`].content[0];
              const mapObject = {
                item: {
                  value: 'id',
                  label: defaultRules.label,
                },
              };
              const value =  val[`${defaultRules.field}`] ? val[`${defaultRules.field}`] : data[`find${defaultRules.entity}List`].content ;
              let to = [{ ...value, id: val.id }];
              const optionTrans = transform(
                Array.isArray(value) ? value : to,
                mapObject,
              );
              const finalOptions = optionTrans.map((item) => {
                let label = item.label;
                if (Array.isArray(label)) {
                  return { value: item.value, label: label.join(', ') };
                } else return item;
              });
              if (finalOptions.length > 0) {
                const default_value = defaultValues[name];
                setValue(name, default_value ? allowEdit ? finalOptions[0] : default_value : finalOptions[0] );
                setAllValues((prev) => ({...prev, [`${name}`]: default_value ? allowEdit  ? finalOptions[0] : default_value : finalOptions[0] }));                
                setOptionsValues(finalOptions);
              } else {
                setOptionsValues([]);
                setAllValues((prev) => ({ ...prev, [`${name}`]: null }));
                setValue(name, null);
              }
            } catch (error) {
              setOptionsValues([]);
            }
          };
          fetchData();
        }
      }
      return () => setAllValues(null);
    }, [refresh, filter]);

    const onActionChange = (e, options) => {
      setAllowEdit(true);
      if (blockChanges) setBlockChanges(false);
      setAllValues((prev) => ({ ...prev, [`${name}`]: options }));
      setValue(name, options);
      onChange && onChange(e, options, setValue, getValues);
    };
    const optionLabel = (option) => {
      return `${option.label || ''}`;
    };
    const renderOption = (props, option, { selected }) => {
      const { key, ...restProps } = props;
      return (
        <li key={key} {...restProps}>
          <Checkbox checked={selected} />
          {option.label}
        </li>
      );
    };
    /**
     * @param {Any} label
     * @returns node
     */
    const getLabel = (label) => {
      return (
        <>
          <Typography variant={'h5'} style={{ fontWeight: 600 }}>
            {`${label} ${(rules?.required && '*') || ''}`.trim()}
          </Typography>
          {showSpinning && <img src={spinning} height={'35px'} width={'35px'} />}
        </>

      );
    };
    const input = (params) => {
      return (
        <TextField
          {...params}
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors[name])}
          error={Boolean(errors[name])}
          label={getLabel(inputProps.label)}
          helperText={
            errors[name] && (
              <Typography
                component={'span'}
                className='font-ebs text-red-600 text-lg'
              >
                {errors[name].message}
              </Typography>
            )
          }
          variant={inputProps[`variant`]}
        />
      );
    };
    /**
     * @param {Object} option
     * @param {Object} value
     * @returns {Boolean}
     */
    const optionSelected = (option, value) => {
      if (option.value > 0) return Number(option.value) === Number(value.value);
    };
    if (!show) return null;
    return (
      <Controller
        control={control}
        name={name}
        render={({ field }) =>
          helper ? (
            <Tooltip
              {...helper}
              title={
                <Typography className='font-ebs text-xl'>{helper.title}</Typography>
              }
            >
              <Autocomplete
                {...field}
                id={name}
                data-testid={name}
                getOptionDisabled={(option) => option.value === 0}
                fullWidth
                getOptionLabel={optionLabel}
                isOptionEqualToValue={optionSelected}
                onChange={onActionChange}
                options={optionsValues.length === 0 ? options : optionsValues || []}
                renderInput={input}
                renderOption={renderOption}
                {...inputProps}
                value={
                  inputProps?.multiple
                    ? (field?.value?.length && field.value) || []
                    : field?.value || null
                }
              />
            </Tooltip>
          ) : (
            <Autocomplete
              {...field}
              id={name}
              data-testid={name}
              fullWidth
              getOptionLabel={optionLabel}
              isOptionEqualToValue={optionSelected}
              getOptionDisabled={(option) => option.value === 0}
              inputValue={getValues(name)?.name || ''}
              onChange={onActionChange}
              options={optionsValues.length === 0 ? options : optionsValues || []}
              renderInput={input}
              renderOption={renderOption}
              {...inputProps}
              value={
                inputProps?.multiple
                  ? (field?.value?.length && field.value) || []
                  : field?.value || null
              }
            />
          )
        }
        rules={rules}
      />
    );
  },
);
// Type and required properties
DropDownFormAtom.propTypes = {
  onChange: PropTypes.func.isRequired,
  getValues: PropTypes.func.isRequired,
  setValue: PropTypes.func.isRequired,
  control: PropTypes.any,
  errors: PropTypes.object,
  name: PropTypes.string.isRequired,
  helper: PropTypes.object,
  options: PropTypes.array,
  inputProps: PropTypes.object,
  rules: PropTypes.object,
};
// Default properties
DropDownFormAtom.defaultProps = {
  onChange: (e, options) => {},
};

export default DropDownFormAtom;
