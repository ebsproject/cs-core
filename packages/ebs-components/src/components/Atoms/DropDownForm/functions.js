
import { getUserProfile } from "@ebs/layout";
import { gql } from '@apollo/client';
import fetch from 'cross-fetch';
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from '@apollo/client';
import { getTokenId, getCoreSystemContext } from "@ebs/layout";
const { graphqlUri } = getCoreSystemContext();

const httpLink = new HttpLink({
  uri: graphqlUri,
  fetch,
});

const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      authorization: `Bearer ${getTokenId()}`,
    },
  }));
  return forward(operation);
});


export const client = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  })

});

 const buildQuery = (entity, apiContent) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: [SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            content {
                ${apiContent.map((item) => {
                  const levels = item.split('.');
                  // Setting levels
                  return extractLevels(levels);
                })}
            }
        }
    }`;
};
function extractLevels(arr) {
    let levels = arr;
    if (levels.length > 1) {
      return `${levels.shift()}{
          ${extractLevels(levels)}
      }`;
    } else {
      return `${levels[0]}`;
    }
  }
async function getAllPages(variables, QUERY, entity) {

    const { data } = await client.query({
      query: QUERY,
      variables: variables,
    });
    return data[`find${entity}List`].content;
  }

  function getNestedProperty(obj, path) {
    return path.split('.').reduce((acc, key) => acc && acc[key], obj);
  }

export async function getOptions(item) {
    let userProfile = getUserProfile();
    try {
      if (item.fieldAttributes.stateValues) {
        return [];
      }
      const entity = item.fieldAttributes.entity;
      const apiContent = item.fieldAttributes.apiContent;
      const QUERY = buildQuery(entity, apiContent);
      let variables = { page: { number: 1, size: 300 } };
      if (item.fieldAttributes.filters) {
        variables.filters = item.fieldAttributes.filters;
      }
      
      const { data } = await client.query({
        query: QUERY,
        variables: variables,
      });
  
      let result = [...data[`find${entity}List`].content];
      let totalPages = data[`find${entity}List`].totalPages;
  
      if (totalPages > 1) {
        for (let i = 2; i <= totalPages; i++) {
          variables.page = { number: i, size: 300 };
          result = [...result, ...await getAllPages(variables, QUERY, entity)];
        }
      }
      const _labels = item.fieldAttributes["labels"] ?  item.fieldAttributes["labels"] : [ "name" ];
      entity === "Program" ? (result = userProfile.permissions.memberOf.programs) : null; //TODO review to get from a dynamic rest API
      const formattedRes = result.map((data) => {
        let label= "";
        label = _labels.map(i => {
          return getNestedProperty(data, i)
        }).join("-");
        return { label: label, value: data.id };
      });
      return formattedRes;
    } catch (error) {
      console.error(error);
      return [];
    }
  }
