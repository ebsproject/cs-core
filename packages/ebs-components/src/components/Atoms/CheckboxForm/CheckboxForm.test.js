import CheckboxForm from './CheckboxForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('CheckboxForm is in the DOM', () => {
  render(<CheckboxForm></CheckboxForm>)
  expect(screen.getByTestId('CheckboxFormTestId')).toBeInTheDocument();
})
