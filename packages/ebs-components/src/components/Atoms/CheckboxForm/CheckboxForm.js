import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const {
  Checkbox,
  FormControlLabel,
  FormGroup,
  FormLabel,
  Tooltip,
  Typography,
} = Core;
import { Controller } from 'react-hook-form';
import { useEbsFormContext } from 'context/ebsForm-context';

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const CheckboxFormAtom = React.forwardRef(
  (
    { setValue, control, title, name, options, helper, inputProps, onChange, defaultRules, watch },
    ref,
  ) => {
    const { setAllValues } = useEbsFormContext();
    let field = null;
    if (defaultRules && defaultRules["showControlRules"]) {
      field = watch()[defaultRules.showControlRules.parentControl];
    }
    const [show, setShow] = useState(!defaultRules ? true : defaultRules["showControlRules"] ? false : true);
    useEffect(() => {
      if (field !== null) {
        if (field !== undefined) {
          let values = defaultRules.showControlRules.expectedValue.split(",");
          let result = false;
          values.forEach(val => {
            if (String(val) === field)
              result = true
          })
          if (result)
            setShow(true);
          else{
            setShow(false);
            setValue(name, null)
          }      
        }
      }
    }, [field]);
    const handleChange = (e) => {
      onChange && onChange(e);
      setValue(`${name}`, e.target.checked);
      setAllValues(prev => ({ ...prev, [`${name}`]: e.target.checked }));
    }
    if (!show) return null;
    if (helper) {
      return (
        <>
          <FormLabel component='legend' className='font-ebs text-lg font-bold'>
            {title}
          </FormLabel>
          <FormGroup>
            <FormControlLabel
              control={
                <Controller
                  control={control}
                  name={`${name}`}
                  render={({ field }) => (
                    <Tooltip
                      {...helper}
                      title={
                        <Typography className='font-ebs text-xl'>{helper.title}</Typography>
                      }
                    >
                      <Checkbox
                        {...field}
                        data-testid={`${name}`}
                        onChange={handleChange}
                        checked={field.value}
                        {...inputProps}
                      />
                    </Tooltip>
                  )}
                />
              }
              label={inputProps.label}
            />

          </FormGroup>
        </>

      );
    } else {
      return (
        <>
          <FormLabel component='legend' className='font-ebs text-lg font-bold'>
            {title}
          </FormLabel>
          <FormGroup>
            <Controller
              key={1}
              control={control}
              name={`${name}`}
              render={({ field }) => (
                <FormControlLabel
                  control={
                    <Checkbox
                      {...field}
                      data-testid={`${name}`}
                      onChange={handleChange}
                      checked={field.value}
                      {...inputProps}
                    />
                  }
                  label={inputProps.label}
                />
              )}
            />
          </FormGroup>
        </>
      );
    }
  },
);
// Type and required properties
CheckboxFormAtom.propTypes = {
  setValue: PropTypes.func.isRequired,
  control: PropTypes.any,
  title: PropTypes.node,
  name: PropTypes.string.isRequired,
  options: PropTypes.array,
  helper: PropTypes.object,
  inputProps: PropTypes.object,
  onChange: PropTypes.func,
};
// Default properties
CheckboxFormAtom.defaultProps = {};

export default CheckboxFormAtom;
