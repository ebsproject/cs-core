import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import {Core , Styles, Icons} from "@ebs/styleguide";
const  { useTheme }= Styles;
const  { Divider, IconButton, InputBase, Paper }= Core;
const { Clear, Search } = Icons;
// Global filter styles

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const GlobalFilterAtom = React.forwardRef((props, ref) => {
  const theme = useTheme();
  const classes ={
    root: {
      padding: "2px 8px",
      display: "flex",
      alignItems: "center",
      width: 400,
    },
    input: {
      marginLeft: theme.spacing(1),
      flex: 1,
    },
    iconButton: {
      padding: 10,
    },
    divider: {
      height: 28,
      margin: 4,
    },
  };

  const { globalFilter, setGlobalFilter } = props;
  const [value, setValue] = React.useState(globalFilter);
  const search = (value) => {
    setGlobalFilter(value);
  };

  return (
    /* 
     @prop data-testid: Id to use inside globalfilter.test.js file.
     */
    <div data-testid={"GlobalFilterTestId"} ref={ref}>
      <Paper sx={classes.root}>
        <InputBase
          value={value || ""}
          color="primary"
          onChange={(e) => {
            setValue(e.target.value);
          }}
          sx={classes.input}
          placeholder="Search..."
          inputProps={{ "aria-label": "search..." }}
        />
        <IconButton
          onClick={() => search(value)}
          color="primary"
          sx={classes.iconButton}
          aria-label="clear"
        >
          <Search />
        </IconButton>
        <Divider sx={classes.divider} orientation="vertical" />
        <IconButton
          onClick={(e) => {
            setValue("");
            search(undefined);
          }}
          color="secondary"
          sx={classes.iconButton}
          aria-label="clear"
        >
          <Clear />
        </IconButton>
      </Paper>
    </div>
  );
});
// Type and required properties
GlobalFilterAtom.propTypes = {
  globalFilter: PropTypes.string,
  setGlobalFilter: PropTypes.func.isRequired,
};
// Default properties
GlobalFilterAtom.defaultProps = {
  globalFilter: "",
};

export default GlobalFilterAtom;
