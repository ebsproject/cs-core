import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
const { Checkbox }= Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SelectAtom = React.forwardRef(
 // ({ indeterminate,defaultRow,idsArray,idRow,setSelectedRows, ...rest}, ref) => {
    ({ indeterminate, ...rest}, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {
      resolvedRef.current.indeterminate = indeterminate;
    }, [resolvedRef, indeterminate]);
// const removeItem=(id)=>{
//   if(idsArray.includes(id))
//   {
//     let index = idsArray.indexOf(id)
//     idsArray.splice(index,1);
//     setSelectedRows(idsArray);
//   }

// }
    return (
      /* 
     @prop data-testid: Id to use inside select.test.js file.
     */
      <div data-testid={"SelectTestId"} ref={ref}>
         <Checkbox ref={resolvedRef} {...rest} />
        {/* {defaultRow && (
          <Checkbox ref={resolvedRef} defaultChecked onChange={()=>removeItem(idRow)} />
        )}
        {!defaultRow && (
          <Checkbox ref={resolvedRef} {...rest} />
        )} */}
      </div>
    );
  }
);
// Type and required properties
SelectAtom.propTypes = {
  indeterminate: PropTypes.bool,
};
// Default properties
SelectAtom.defaultProps = {};

export default SelectAtom;
