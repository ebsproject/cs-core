import React, {useEffect} from 'react';
import { useEbsFormContext } from 'context/ebsForm-context';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
const { DialogContent, IconButton } = Core;
const { Add } = Icons;
import { useState } from 'react';

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ModalForm = ({parentControl, component: Component, watch, defaultRules }) => {
  let field = null;
  const { setRefresh, refresh } = useEbsFormContext();
  const parentControlValue = watch()[parentControl];
  const [open, setOpen] = useState(false);
  if (defaultRules && defaultRules["showControlRules"]) {
    field = watch()[defaultRules.showControlRules.parentControl];
  }
  const [show, setShow] = useState(!defaultRules ? true : defaultRules["showControlRules"] ? false : true);

  useEffect(() => {
    if (field !== null) {
      if(field !== undefined){
        if (String(field) === defaultRules.showControlRules.expectedValue)
          setShow(true);
        else
          setShow(false);
      }
    }
  }, [field]);
  const handleClose = () => {
    setRefresh(!refresh);
    setOpen(false);
  }

if(!show) return null;
  return (
    <>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogContent>
          <Component parentControl ={parentControlValue} handleClose={handleClose} />
        </DialogContent>
      </EbsDialog>
      <IconButton      
      onClick={() => setOpen(true)} 
      size={"small"} 
      color={"primary"} 
      style={{ top:10, paddingLeft:2}} >
        <Add />
      </IconButton>
    </>

  );

}
export default ModalForm;
