import TextFieldForm from './TextFieldForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('TextFieldForm is in the DOM', () => {
  render(<TextFieldForm></TextFieldForm>)
  expect(screen.getByTestId('TextFieldFormTestId')).toBeInTheDocument();
})
