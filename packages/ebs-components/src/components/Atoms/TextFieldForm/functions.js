import { restClient } from "utils/Axios";
import { FIND_DOMAIN_LIST } from "functions/query";
import { client } from "utils/Apollo";
import { getCoreSystemContext } from "@ebs/layout";


export async function generateCode(program,segment) {
    const client = restClient();
    try {
        const { data } = await client.get(`sequence/1`);
        const sequence = data.segments.filter(item => item.name === segment)[0];
        const { data: next } = await client.post(`sequence/next/1`, { [`${sequence.id}`]: program.id });
        return next;
    } catch (error) {
        return error.toString()
    }

}
export async function generateDefaultCode(segment) {
    const client = restClient();
    try {
        const { data } = await client.get(`sequence`);
        const sequence = data.content.find(item => item.name === segment);
        if(!sequence) return `Error! sequence rule not found: ${segment} `
        const { data: next } = await client.post(`sequence/next/${sequence['id']}`, {"":""});
        return next;
    } catch (error) {
        return error.toString()
    }
}

export function getNestedValue(obj,field){
    let keys = field.split(".");
    let val;
    for (let i = 0; i < keys.length; i++){
        let key = keys[i];
        if(obj.hasOwnProperty(key)){
            obj = obj[key]
            if(Array.isArray(obj)){
                obj = obj[0];
                continue;
            }
            if (obj)
                val = obj;
            else
                val = "";
        }
    }
    return val;
}
export async function getSgContext(prefix) {
    const { graphqlUri } = getCoreSystemContext();
    const { data } = await client(graphqlUri).query({
      query: FIND_DOMAIN_LIST,
      variables:{
        filters:[{col:"prefix",mod:"EQ",val:prefix}]
      }
    })
    const sgContext = new URL('graphql',data.findDomainList.content[0].domaininstances[0].sgContext).toString();
    return sgContext;
  }