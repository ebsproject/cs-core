import MapForm from './MapForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('MapForm is in the DOM', () => {
  render(<MapForm></MapForm>)
  expect(screen.getByTestId('MapFormTestId')).toBeInTheDocument();
})
