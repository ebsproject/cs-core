import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
// GLOBALIZATION COMPONENT
// CORE COMPONENTS
import { Core, Icons, Lab } from '@ebs/styleguide';
const { Button, Typography, TextField, Box, Grid, Collapse, IconButton } = Core;
const { Alert } = Lab;
const { Close } = Icons;
import { MapContainer as Map, TileLayer, FeatureGroup } from 'react-leaflet';
import './styles.css';
import L from 'leaflet';
import { EditControl } from 'react-leaflet-draw';
import { Controller } from 'react-hook-form';
import '@ebs/messaging';

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const MapFormAtom = React.forwardRef(
  (
    { control, errors, name, helper, inputProps, rules, getValues, setValue },
    ref,
  ) => {
    const [formData, setFormData] = useState({ lat: '', lng: '' });
    const [submittedData, setSubmittedData] = useState(null);
    const [location, setLocation] = useState({ lat: null, lng: null });
    const [open, setOpen] = React.useState(false);
    const [error, setError] = useState(null);
    const litElementRef = useRef(null);
    const [geojson, setGeojson] = useState(null);

    useEffect(() => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            setLocation({
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            });
          },
          (err) => {
            setError(err.message);
          },
        );
      } else {
        setError('Geolocation is not supported by this browser.');
      }
    }, []);
    useEffect(() => {
      const litElement = litElementRef.current;
      const handleGeojsonUpdated = (event) => {
        setGeojson(event.detail);
      };
      litElement.addEventListener('geojson-updated', handleGeojsonUpdated);
      return () => {
        litElement.removeEventListener('geojson-updated', handleGeojsonUpdated);
      };
    }, []);

    useEffect(() => {
      if (litElementRef.current) {
        if (submittedData !== null) {
          litElementRef.current.lat = submittedData.lat;
          litElementRef.current.lng = submittedData.lng;
        }
      }
    }, [submittedData]);

    const validateCoordinates = (formData) => {
      const lat = parseFloat(formData.lat);
      const lon = parseFloat(formData.lng);

      if (isNaN(lat) || isNaN(lon)) {
        return false;
      }
      if (lat < -90 || lat > 90) {
        return false;
      }

      if (lon < -180 || lon > 180) {
        return false;
      }

      return true;
    };

    const handleChange = (event) => {
      const { name, value } = event.target;
      setFormData({
        ...formData,
        [name]: value,
      });
    };
    const handleSubmit = () => {
      if (validateCoordinates(formData)) {
        setSubmittedData(formData);
      } else {
        setOpen(true);
      }
    };
    if (geojson !== null) {
      setValue(name, geojson);
    } else {
      setValue(name, inputProps.defaultValue);
    }

    return (
      <div>
        {inputProps.type ? null : (
          <div>
            <br />
            <h6>Coordinates</h6>
            <br />
          </div>
        )}
        <Controller
          control={control}
          name={name}
          render={({ field }) => {
            return (
              <div>
                {inputProps.type ? null : (
                  <Grid container spacing={1} alignItems='center'>
                    <Grid item>
                      <TextField
                        id='lat'
                        name='lat'
                        label='Latitude'
                        size='small'
                        value={formData.lat}
                        onChange={handleChange}
                        styled={{ marginRigth: '10px' }}
                      />
                    </Grid>
                    <Grid item>
                      <TextField
                        id='lng'
                        name='lng'
                        label='Longitude'
                        size='small'
                        value={formData.lng}
                        onChange={handleChange}
                      />
                    </Grid>
                    <Grid item>
                      <Button
                        variant='contained'
                        color='primary'
                        onClick={handleSubmit}
                        disabled={
                          formData.lat === '' || formData.lng === '' ? true : false
                        }
                        className='bg-ebs-brand-default rounded-md text-white p-2 m-8 hover:bg-ebs-brand-900'
                      >
                        Send coordinates
                      </Button>
                    </Grid>
                  </Grid>
                )}
                <br />
                {error && <p>Error: {error}</p>}
                <div>
                  <Collapse in={open}>
                    <Alert
                      severity='error'
                      action={
                        <IconButton
                          aria-label='close'
                          color='inherit'
                          size='small'
                          onClick={() => {
                            setOpen(false);
                          }}
                        >
                          <Close fontSize='inherit' />
                        </IconButton>
                      }
                    >
                      The aggregated data is not valid
                    </Alert>
                  </Collapse>
                </div>
                <ebs-map
                  ref={litElementRef}
                  type={inputProps.type}
                  lat={
                    inputProps?.coordinates === undefined
                      ? submittedData !== null
                        ? submittedData.lat
                        : location.lat !== null
                          ? location.lat
                          : 2
                      : inputProps.coordinates.lat
                  }
                  lng={
                    inputProps?.coordinates === undefined
                      ? submittedData !== null
                        ? submittedData.lng
                        : location.lng !== null
                          ? location.lng
                          : 2
                      : inputProps.coordinates.lng
                  }
                  zoom={13}
                  geojsonData={JSON.stringify(inputProps.defaultValue)}
                ></ebs-map>
              </div>
            );
          }}
        />
      </div>
    );
  },
);
// Type and required properties
MapFormAtom.propTypes = {};
// Default properties
MapFormAtom.defaultProps = {};

export default MapFormAtom;
