import DatePickerForm from './DatePickerForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('DatePickerForm is in the DOM', () => {
  render(<DatePickerForm></DatePickerForm>)
  expect(screen.getByTestId('DatePickerFormTestId')).toBeInTheDocument();
})
