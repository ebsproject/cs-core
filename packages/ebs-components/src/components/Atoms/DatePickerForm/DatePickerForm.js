import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core, Pickers, MUIAdapterDayjs, Dayjs } from "@ebs/styleguide";
const { LocalizationProvider, DatePicker } = Pickers;
import { Controller } from 'react-hook-form';
const { Tooltip, Typography } = Core;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DatePickerFormAtom = React.forwardRef(
  ({ control, errors, name, helper, onChange, inputProps, rules, setValue }, ref) => {
    /**
     * @param {Any} label
     * @returns node
     */
    const getLabel = (label) => {
      return (
        <Typography
          variant={"h5"}
          style={{ fontWeight: 600}}
        >{`${label} ${(rules?.required && '*') || ''}`}</Typography>
      );
    };
    const getDatePickerComponent = (field) => {
      return (
        <DatePicker
          sx={{
            '& .MuiInputLabel-root': {
              transform: 'translate(14px, -9px) scale(0.75)',
              fontSize: '0.85rem',
            },
            ...inputProps.sx
           // width: "100%"
          }}
          slotProps={{
            field: { clearable: true, onClear: () => setValue(name, null) },
            textField: {
              InputLabelProps: {
                shrink: true,
              },
              error: !!errors[name],
              helperText: errors[name] ? errors[name].message : '',
            },
          }}
          fullWith
          {...field}
          closeOnSelect
          label={getLabel(inputProps.label)}
          value={field.value ? Dayjs(field.value) : null}
          data-testid={name}
          onChange={(date) => {
            setValue(name, date ? Dayjs(date).format('YYYY-MM-DD') : null)
          }}
        />
      )
    }
    return (
      /**
       * @prop data-testid: Id to use inside DatePickerForm.test.js file.
       */
      <>
        <LocalizationProvider dateAdapter={MUIAdapterDayjs}>
          <Controller
            control={control}
            name={name}
            render={({ field }) =>
              helper ? (
                <Tooltip
                  {...helper}
                  title={
                    <Typography className='font-ebs text-xl'>
                      {helper.title}
                    </Typography>
                  }
                >
                  {getDatePickerComponent(field)}
                </Tooltip>
              ) : (getDatePickerComponent(field)
              )
            }
            rules={rules}
          />
        </LocalizationProvider>
        {
          errors[name] && (
            <Typography color="error">
              {errors[name].message}
            </Typography>
          )
        }
      </>
    );
  },
);
// Type and required properties
DatePickerFormAtom.propTypes = {
  control: PropTypes.any,
  errors: PropTypes.object,
  name: PropTypes.string.isRequired,
  helper: PropTypes.object,
  onChange: PropTypes.func,
  inputProps: PropTypes.object,
  rules: PropTypes.object,
};
// Default properties
DatePickerFormAtom.defaultProps = {};

export default DatePickerFormAtom;
