import React, { useEffect } from 'react';
import { useEbsFormContext } from 'context/ebsForm-context';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
const { DialogContent, Button } = Core;
const { Add, CheckCircle } = Icons;
import { useState } from 'react';

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DialogForm = ({ getValues, watch, ...rest }) => {
  const { defaultRules, parentControl, uiComponent: Component, name, label } = rest;
  const { setRefresh, refresh, allValues, setAllValues } = useEbsFormContext();
  const [open, setOpen] = useState(false);
  const [show, setShow] = useState(!defaultRules ? true : defaultRules.showIfValue ? false : true);
  useEffect(() => {
    if (defaultRules && defaultRules["showIfValue"]) {
      const defaultValue = getValues(name);
      if (defaultValue && defaultValue.length > 0)
        setShow(true);
      else {
        if (allValues) {
          const value = allValues[`${defaultRules.parentControl}`];
          if (String(value) === defaultRules.showIfValue)
            setShow(true);
          else
            setShow(false);
        }
      }
    }
    return () => setAllValues(null);
  }, [allValues]);

  const handleClose = () => {
    setRefresh(!refresh);
    setOpen(false);
  }
  const parentControlValue = watch()[parentControl];
  if (!show) return null;

  return (
    <>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        maxWidth={"lg"}
        fullWidth
      >
        <DialogContent>
          <Component parentControl={parentControlValue || 0} handleClose={handleClose} />
        </DialogContent>
      </EbsDialog>
      <Button
        onClick={() => setOpen(true)}
        size={"small"}
        color={"primary"}
        style={{ left: 0 }}
        startIcon={label ? <CheckCircle /> : <Add />} >
        {label || ""}
      </Button>
    </>

  );

}
export default DialogForm;
