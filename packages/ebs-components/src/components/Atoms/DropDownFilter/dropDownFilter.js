import React, { useContext } from 'react';

import { Controller } from 'react-hook-form';
// CORE COMPONENTS
import {Core, Lab} from "@ebs/styleguide";
const { Box, TextField} = Core;
const {Autocomplete} = Lab;
import {useLayoutContext} from "@ebs/layout";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */

const DropDownFilterAtom = React.forwardRef(
  ({  control, errors, setValue,allowedTenantsByUser }, ref,) => {
    const { userProfile } = useLayoutContext();   
    const programs = userProfile?.permissions?.memberOf?.programs;       
    const onFilterChange = (e, option) => setValue("Program", [option]);
    const optionLabel = (option) => `${option.name}`;   
    return (
      <Box className='py-1 px-2'>
        <Controller
          name="Program"
          control={control}
          render={({ field }) => (
            <Autocomplete
              size='small'
              {...field}
              id={"Program"}
              options=  { programs || [] }
              onChange={onFilterChange}
              getOptionLabel={optionLabel}
              renderInput={(params) => <TextField {...params} label="Program" />}                         
            />
          )}
        />
      </Box>
    )

  }
);
// Type and required properties
DropDownFilterAtom.propTypes = {
};
// Default properties
DropDownFilterAtom.defaultProps = {

};

export default DropDownFilterAtom;
