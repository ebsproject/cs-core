import React, { useCallback, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import Client from 'utils/Client';
import MainTable from './components/Organisms/MainTable';
import { extractColumns } from 'utils/Client';
import 'regenerator-runtime/runtime';
import CSVExport from 'functions/csv';
import { Core } from '@ebs/styleguide';
const { Typography } = Core;
import { transform } from 'node-json-transform';
/**
 * @param uri: endpoint Uri
 * @param title: Table title
 * @param select: Component or kind of select implemented
 * @param entity: entity to handle
 * @param height: table height
 * @param indexing: table indexing
 * @param rowactions: Component to add on every row as Actions
 * @param callstandar: Brapi/Graphql standards to handle
 * @param toolbaractions: Component to add on top table as Global actions
 * @param detailcomponent: Component used as master detail on every row
 */
export const EbsGrid = React.forwardRef(
  (
    {
      callstandard,
      columns: originalColumns,
      csvfilename,
      defaultFilters,
      detailcomponent,
      entity,
      fetch,
      globalFilter,
      height,
      id,
      indexing,
      raWidth,
      rowactions,
      select,
      styleStatic,
      title,
      toolbar,
      toolbaractions,
      uri,
      disabledViewConfiguration,
      disabledViewDownloadCSV,
      disabledViewPrintOunt,
      disabledHeadTable,
      disabledPagination,
      color,
      disabledFiedPagination,
      defaultSort,
      disabledDetails,
      auditColumns,
    },
    ref,
  ) => {
    if (auditColumns) {
      originalColumns = [
        ...originalColumns,
        ...[
          {
            Header: (
              <Typography variant='h5' color='primary'>
                Created by
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant='body1'>{value}</Typography>;
            },
            accessor: 'createdBy.userName',
            csvHeader: 'created By',
            width: 200,
            Filter: false,
          },
          {
            Header: (
              <Typography variant='h5' color='primary'>
                Created On
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant='body1'>{value}</Typography>;
            },
            accessor: 'createdOn',
            csvHeader: 'created On',
            width: 200,
            Filter: false,
          },
          {
            Header: (
              <Typography variant='h5' color='primary'>
                Updated By
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant='body1'>{value}</Typography>;
            },
            accessor: 'updatedBy.userName',
            csvHeader: 'Updated By',
            width: 200,
            Filter: false,
          },
          {
            Header: (
              <Typography variant='h5' color='primary'>
                Updated On
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant='body1'>{value}</Typography>;
            },
            accessor: 'updatedOn',
            csvHeader: 'updated On',
            width: 200,
            Filter: false,
          },
        ],
      ];
    }
    const getItemsColumnsFilterType = (filter) => {
      const columnsItems = ['id', 'number', 'weight', 'packageCount', 'isDangerous'];
      return columnsItems.includes(filter.id);
    };
    const getItemsColumnsFilterValue = (filter) => {
      let value = filter.value;
      if (isNaN(value) && (value === 'true' || value === 'false')) return value;
      if (isNaN(value)) return -1;
      return value;
    };
    const columns = useMemo(() => originalColumns, []);
    // * We'll start our table without any data
    const [data, setData] = useState([]);
    const [errorMessage, setErrorMessage] = useState(null);
    const [counter, setCounter] = useState([{ from: 0, to: 0 }]);
    const [loading, setLoading] = useState(false);
    const [pageCount, setPageCount] = useState(0);
    const [totalElements, setTotalElements] = useState(0);
    const fetchIdRef = React.useRef(0);
    const extractedColumns = extractColumns(originalColumns);
    const [resetPage, setResetPage] = useState(false);
    // * Fetch our data function
    const fetchData = useCallback(
      ({
        pageSize,
        pageIndex,
        sortBy,
        filters,
        globalFilter,
        defaultFiltersData,
        defaultFilters,
      }) => {
        //Filters
        const Filters = [];
        const page = { number: pageIndex + 1, size: pageSize };
        const sort = [];
        if (defaultSort && sortBy.length === 0) sort.push(defaultSort);
        sortBy.map((column) => {
          sort.push({ col: column.id, mod: column.desc ? 'DES' : 'ASC' });
        });
        // * This will get called when the table needs new data
        // * Give this fetch an ID
        const fetchId = ++fetchIdRef.current;
        defaultFiltersData.length > 0
          ? defaultFiltersData.map((item) => {
              Filters.push(item);
            })
          : null;
        // * Set the loading state
        setLoading(true);

        // * Setting up filters
        filters.map((filter) => {
          if (
            filter.id === 'isActive' ||
            filter.id === 'isAdmin' ||
            filter.id === 'institution.isCgiar'
          ) {
            return Filters.push({ col: filter.id, mod: 'EQ', val: filter.value });
          } else {
            return Filters.push({
              col: filter.id,
              mod: getItemsColumnsFilterType(filter) ? 'EQ' : 'LK',
              val: getItemsColumnsFilterType(filter)
                ? getItemsColumnsFilterValue(filter)
                : filter.value.toLowerCase(),
            });
          }
        });
        // ? Global filter ?
        //   globalFilter &&
        extractedColumns.forEach((column) => {
          if (column.disableGlobalFilter) {
            let index = Filters.findIndex((item) => item.col === column.accessor);
            index >= 0 ? Filters.splice(index, 1) : null;
          }
        });

        // ? Default Filter ?
        defaultFilters && defaultFilters.map((filter) => Filters.push(filter));
        // * Only update the data if this is the latest fetch
        if (fetchId === fetchIdRef.current) {
          // * Fetching from custom data
          if (fetch) {
            fetch({ page: page, sort: sort, filters: Filters })
              .then(({ pages, data, elements }) => {
                let newCounter = new Array(pages);
                newCounter.push({ from: 0, to: 0 });
                const nan = isNaN(elements / pages) ? 1 : elements / pages;
                // * rows per page
                const rowsByPage =
                  pageSize >= Math.ceil(nan) ? pageSize : Math.ceil(nan);
                for (let index = 0; index < pages; index++) {
                  newCounter[index] = {
                    from: newCounter[index - 1]?.to + 1 || 1,
                    to:
                      index === pages - 1
                        ? elements
                        : newCounter[index - 1]?.to + rowsByPage || rowsByPage,
                  };
                }
                setErrorMessage(null);
                setCounter(newCounter);
                setPageCount(pages);
                setLoading(false);
                setData(data);
                setTotalElements(elements);
              })
              .catch(({ message }) => {
                setLoading(false);
                setErrorMessage(message);
              });
          } else {
            Client({
              id: id,
              uri: uri,
              page: page,
              sort: sort,
              entity: entity,
              filters: Filters,
              columns: extractedColumns,
              callStandard: callstandard,
            })
              .then(({ pages, data, elements }) => {
                let newCounter = new Array(pages);
                newCounter.push({ from: 0, to: 0 });
                const nan = isNaN(elements / pages) ? 1 : elements / pages;
                // * rows per page
                const rowsByPage =
                  pageSize >= Math.ceil(nan) ? pageSize : Math.ceil(nan);
                for (let index = 0; index < pages; index++) {
                  newCounter[index] = {
                    from: newCounter[index - 1]?.to + 1 || 1,
                    to:
                      index === pages - 1
                        ? elements
                        : newCounter[index - 1]?.to + rowsByPage || rowsByPage,
                  };
                }
                setErrorMessage(null);
                setCounter(newCounter);
                setPageCount(pages);
                setLoading(false);
                setData(data);
                setTotalElements(elements);
              })
              .catch(({ message }) => {
                setLoading(false);
                setErrorMessage(message);
              });
          }
        }
      },
      [resetPage],
    );
    //get csv data function
    const GetCSVData = useCallback(
      async (
        { filters, globalFilter, totalElements, defaultFilters },
        selectedRows,
        setSelectedRows,
      ) => {
        if (selectedRows.length > 0) {
          downloadCSVData(selectedRows);
          setSelectedRows([]);
          return;
        }

        const Filters = [];
        const page = { number: 1, size: totalElements };
        const sort = [];
        // * This will get called when the table needs new data
        // * Give this fetch an ID
        const fetchId = ++fetchIdRef.current;
        // * Set the loading state
        setLoading(true);
        // * Setting up filters

        filters.map((filter) =>
          Filters.push({ col: filter.id, mod: 'LK', val: filter.value }),
        );

        // ? Global filter ?
        globalFilter &&
          extractedColumns.map((column) => {
            if (!column.disableGlobalFilter) {
              Filters.push({
                col: column.accessor,
                mod: 'LK',
                val: globalFilter,
              });
            }
          });
        // ? Default Filter ?
        defaultFilters && defaultFilters.map((filter) => Filters.push(filter));
        if (fetchId === fetchIdRef.current) {
          // * Fetching from custom data
          if (fetch) {
            let _data = [];
            try {
              const { pages, data, elements } = await fetch({
                page: { number: 1, size: 100 },
                sort: sort,
                filters: Filters,
              });
              _data = [..._data, ...data];
              if (pages > 1) {
                for (let p = 2; p <= pages; p++) {
                  const { data } = await fetch({
                    page: { number: p, size: 100 },
                    sort: sort,
                    filters: Filters,
                  });
                  _data = [..._data, ...data];
                }
              }
              downloadCSVData(_data);
              setErrorMessage(null);
              setLoading(false);
            } catch (error) {
              setLoading(false);
              setErrorMessage(error);
            }
            // fetch({ page: page, sort: sort, filters: Filters })
            //   .then(({ pages, data, elements }) => {
            //     downloadCSVData(data);
            //     setErrorMessage(null);
            //     setLoading(false);
            //   })
            //   .catch(({ message }) => {
            //     setLoading(false);
            //     setErrorMessage(message);
            //   });
          } else {
            // Client({
            //   uri: uri,
            //   page: page,
            //   sort: sort,
            //   entity: entity,
            //   filters: Filters,
            //   columns: extractedColumns,
            //   callStandard: callstandard,
            //   downloadCSV: true,
            // })
            //   .then(({ pages, data, elements }) => {
            //     downloadCSVData(data);
            //     setErrorMessage(null);
            //     setLoading(false);
            //   })
            //   .catch(({ message }) => {
            //     setLoading(false);
            //     setErrorMessage(message);
            //   });
            let _data = [];
            try {
              const { pages, data, elements } = await Client({
                uri: uri,
                page: { number: 1, size: 100 },
                sort: sort,
                entity: entity,
                filters: Filters,
                columns: extractedColumns,
                callStandard: callstandard,
                downloadCSV: true,
              });
              _data = [..._data, ...data];
              if (pages > 1) {
                for (let p = 2; p <= pages; p++) {
                  const { data } = await Client({
                    uri: uri,
                    page: { number: p, size: 100 },
                    sort: sort,
                    entity: entity,
                    filters: Filters,
                    columns: extractedColumns,
                    callStandard: callstandard,
                    downloadCSV: true,
                  });
                  _data = [..._data, ...data];
                }
              }
              downloadCSVData(_data);
              setErrorMessage(null);
              setLoading(false);
            } catch (error) {
              setLoading(false);
              setErrorMessage(error);
            }
          }
        }
      },
      [],
    );

    function removeBeforeFirstDot(input) {
      const firstDotIndex = input.indexOf('.');
      if (firstDotIndex === -1) {
        return input;
      }
      return input.substring(firstDotIndex + 1);
    }

    function getValueObject(obj, path) {
      let keys = path.split('.');
      let valor = obj;
      for (let key of keys) {
        valor = valor[key];
      }
      return valor;
    }

    function downloadCSVData(data) {
      let headers = {};
      let map = {};
      let formattedData = [];
      // * Building Map and Headers
      for (let i = 0; i < columns.length; i++) {
        const column = columns[i];
        if (!column.hidden) {
          map[column.accessor.split('.')[0]] = column.accessor;
          headers[column.accessor.split('.')[0]] = column.csvHeader;
        }
      }
      // * Building formattedData
      for (let i = 0; i < data.length; i++) {
        const item = data[i];
        let mapKeys = Object.keys(map);
        let objTransformed = {}; //transform(item, { item: map })
        for (let i = 0; i < mapKeys.length; i++) {
          const _k =
            mapKeys[i].includes('[0]') === true
              ? mapKeys[i].replace('[0]', '')
              : mapKeys[i];
          if (!item[_k]) {
            objTransformed[_k] = '';
          } else {
            let _item = item[_k];
            //is object
            if (Object.prototype.toString.call(_item) === '[object Object]') {
              let obj = _item['name'];
              if (obj) {
                objTransformed[_k] = obj;
              } else {
                objTransformed[_k] = _item;
              }
            } else {
              objTransformed[_k] = _item;
            }
            //is array
            if (Object.prototype.toString.call(_item) === '[object Array]') {
              let obj = _item;
              let path = map[_k + '[0]'];
              const result = removeBeforeFirstDot(path);
              let valor = getValueObject(obj[0], result);
              if (valor) {
                objTransformed[_k] = valor;
              } else {
                objTransformed[_k] = '';
              }
            }
          }
        }
        formattedData = [...formattedData, objTransformed];
      }

      // * Export Data to CSV
      for (let i = 0; i < formattedData.length; i++) {
        let item = formattedData[i];
        let obj = item;
        let objKeys = Object.keys(obj);
        for (let i = 0; i < objKeys.length; i++) {
          let key = objKeys[i];
          if (Object.prototype.toString.call(item[key]) === '[object Object]') {
            let _obj = item[key];
            let _objKeys = Object.keys(_obj);
            let stringValue = [];
            _objKeys.forEach((_key) => {
              if (typeof _obj[_key] === 'string')
                stringValue = [...stringValue, _obj[_key]];
            });
            item[key] = stringValue.join(',');
          }
          if (item[key] === null) {
            item[key] = 'No Data';
          }
          if (Array.isArray(item[key])) {
            item[key] = item[key].length;
          }
        }
      }

      CSVExport(headers, formattedData, csvfilename);
    }

    return (
      <MainTable
        defaultFilters={defaultFilters}
        columns={columns}
        counter={counter}
        csvFileName={csvfilename}
        data={data}
        displayGlobalFilter={false}
        entity={entity}
        errorMessage={errorMessage}
        fetchData={fetchData}
        GetCSVData={GetCSVData}
        height={height}
        id={id}
        indexing={indexing}
        loading={loading}
        pageCount={pageCount}
        setResetPage={setResetPage}
        raWidth={raWidth}
        renderRowSubComponent={detailcomponent}
        rowActions={rowactions}
        select={select}
        styleStatic={styleStatic}
        title={title}
        toolbar={toolbar}
        toolbarActions={toolbaractions}
        totalElements={totalElements}
        uri={uri}
        disabledViewConfiguration={disabledViewConfiguration}
        disabledViewDownloadCSV={disabledViewDownloadCSV}
        disabledViewPrintOunt={disabledViewPrintOunt}
        disabledHeadTable={disabledHeadTable}
        color={color}
        disabledPagination={disabledPagination}
        disabledFiedPagination={disabledFiedPagination}
        resetPage={resetPage}
        disabledDetails={disabledDetails}
      />
    );
  },
);

MainTable.propTypes = {
  uri: PropTypes.string,
  title: PropTypes.node,
  select: PropTypes.any,
  entity: PropTypes.string,
  height: PropTypes.any,
  columns: PropTypes.array.isRequired,
  raWidth: PropTypes.number,
  toolbar: PropTypes.bool,
  indexing: PropTypes.bool,
  rowactions: PropTypes.func,
  callstandard: PropTypes.string,
  defaultFilters: PropTypes.array,
  toolbaractions: PropTypes.func,
  detailcomponent: PropTypes.func,
  globalFilter: PropTypes.bool,
  disabledHeadTable: PropTypes.bool,
  disabledViewConfiguration: PropTypes.bool,
  disabledViewDownloadCSV: PropTypes.bool,
  disabledViewPrintOunt: PropTypes.bool,
};
