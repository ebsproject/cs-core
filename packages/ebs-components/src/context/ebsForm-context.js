import { createContext, useState, useContext } from 'react';

const EbsFormContext = createContext();

export function EbsFormContextProvider({ children }) {
  const [allValues, setAllValues] = useState(null);  
  const [refresh, setRefresh] = useState(false);
  const [allowEdit, setAllowEdit] = useState(false);
  const [blockChanges, setBlockChanges] = useState(false);
  return (
    <EbsFormContext.Provider value={{ allValues, setAllValues, refresh, setRefresh, allowEdit, setAllowEdit, blockChanges, setBlockChanges }}>
      {children}
    </EbsFormContext.Provider>
  );
}

export function useEbsFormContext() {
  return useContext(EbsFormContext);
}
