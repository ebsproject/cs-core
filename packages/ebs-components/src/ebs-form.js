import React from 'react';
import PropTypes from 'prop-types';
import { useForm } from 'react-hook-form';
import { getDefaultValues } from 'utils/getDefaultValues';
import GridForm from 'components/Molecules/GridForm';
import { Core } from '@ebs/styleguide';
import { EbsFormContextProvider } from './context/ebsForm-context';
const { Button } = Core;

export const EbsForm = React.forwardRef(
  ({ definition, onSubmit, actionPlacement, children, buttonReset, onError }, ref) => {
    const def = definition({});
    const defaultError = (error) => console.error(error);
    const defaultValues = getDefaultValues(def);
    const {
      handleSubmit,
      reset,
      setValue,
      getValues,
      control,
      register,
      watch,
      formState: { errors },
    } = useForm({
      defaultValues: defaultValues,
      mode: 'all',
    });
    const { name, components } = definition({ setValue, getValues, reset });
    const onSubmitAction = (data) => {
      onSubmit(data);
      reset();
    };
    return (
      <EbsFormContextProvider>
        <form name={name} onSubmit={handleSubmit(onSubmitAction, onError || defaultError)}>
          {actionPlacement === 'top' && children}
          <GridForm
            defaultValues={defaultValues}
            components={components}
            reset={reset}
            setValue={setValue}
            getValues={getValues}
            control={control}
            errors={errors}
            register={register}
            watch={watch}
          />
          <br />
          {buttonReset && (
            <Button
              onClick={() => reset()}
              className='bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900'
            >
              Reset Form
            </Button>
          )}
          {actionPlacement === 'bottom' && children}
        </form>
      </EbsFormContextProvider>
    );
  },
);

EbsForm.propTypes = {
  definition: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  actionPlacement: PropTypes.oneOf(['top', 'bottom']),
  children: PropTypes.node,
};
EbsForm.defaultProps = {
  actionPlacement: 'top',
};
