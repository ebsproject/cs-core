import { client } from "utils/Apollo";
import { gql } from '@apollo/client';


export const getWorkflowData_jopType = async ({ jopTypeId, uri }) => {
  let dataSet = { status: { success: false, message: '' }, data: {} };
  try {
    const query = gql`
      query findJobWorkflowList($val: String!) {
        findJobWorkflowList(filters: { col: "jobType.id", mod: EQ, val: $val }) {
            content{
                id
                name
                tenant{
                  id
                  name
                }
                jobType {
                  id
                  name
                  description
                }
                productFunction{
                  id
                  description
                  action
                  product{
                    id
                    name
                    
                  }
                }
              }
            }
      }
    `;
    await client(uri)
      .query({
        query: query,
        variables: { val: jopTypeId },
        fetchPolicy: 'no-cache',
      })
      .then((result) => {
        if (result.data.findJobWorkflowList.content.length > 1) {
          let products = []
          result.data.findJobWorkflowList.content.forEach(element => {
            const index = products.findIndex(product => product.productName === element.productFunction.product.name);
            if (index > 0) {
              products[index].actions.push({ actionName: element.productFunction.action, workflowJobId: element.id })
            } else {
              products.push({ productName: element.productFunction.product.name, actions: [{ actionName: element.productFunction.action, workflowJobId: element.id }] })
            }
          });
          dataSet.data = products;
          dataSet.status.success = true;
        } else {
          dataSet.status.success = false;
          dataSet.message = 'Job workflow did not found';
        }
      }).catch((error) => {
        console.log(error);
      });
  } catch (ex) {
    dataSet.status.success = false;
    dataSet.message = ex.toString();
  }
  return dataSet;
}
