import axios from 'axios';
import { getTokenId, getCoreSystemContext } from '@ebs/layout';

export const ClientAxios = (
  uri,
  entity,
  columns,
  page,
  pageSize,
  columnsToFilter,
  value,
  defaultFilter,
) => {
  return new Promise((resolve, reject) => {
    axios
      .get(uri, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
        },
      })
      .then((result) => {
        let payload = {
          data: result.data.result.data,
          pages: result.data.metadata.pagination.totalPages,
        };
        resolve(payload);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

export async function findPrintOutList(id) {
  const { printoutUri } = getCoreSystemContext();
  const client = axios.create({
    baseURL: printoutUri,
    headers: {
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
  });
  client.interceptors.request.use(function (config) {
    config.headers.Authorization = `Bearer ${getTokenId()}`;
    return config;
  });

  const data = await client.get(`api/Template/product/${id}`);
  return data.data.result.data;
}
export function restClient() {
  const { graphqlUri } = getCoreSystemContext();
  const client = axios.create({
    baseURL: graphqlUri.replace("graphql",""),
    headers: {
      Accept: 'application/json',
      'Access-Control-Allow-Origin': '*',
    },
  });
  client.interceptors.request.use(function (config) {
    config.headers.Authorization = `Bearer ${getTokenId()}`;
    return config;
  });
  return client
}
