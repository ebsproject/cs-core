import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from '@apollo/client';
import { getTokenId } from '@ebs/layout';

const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      authorization: `Bearer ${getTokenId()}`,
      ...headers,
    },
  }));
  return forward(operation);
});

export const client = (uri) =>
  new ApolloClient({
    link: from([
      authLink,
      new HttpLink({
        uri: uri,
      }),
    ]),
    cache: new InMemoryCache({
      addTypename: false,
    }),
  });
