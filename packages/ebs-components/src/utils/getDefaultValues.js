export const getDefaultValues = ({ components }) => {
  const defaultValues = {};
  components.map(({ component, name, defaultValue, inputProps, options }) => {
    switch (component.toLowerCase()) {
      case 'textfield':
        defaultValues[name] = defaultValue || '';
        break;
      case 'checkbox':
        // defaultValues[name] = [];
        // options.map((option) => {
        //   const value = {};
        //   value[option.id] = option.defaultValue || false;
        //   defaultValues[name].push(value);
        // });
        defaultValues[name] = defaultValue || false;
        break;
      case 'datepicker':
        defaultValues[name] = defaultValue || null;
        break;
      case 'radio':
        defaultValues[name] = defaultValue || "";
        break;
      case 'switch':
        defaultValues[name] = defaultValue || false;
        break;
      case 'select':
        if (inputProps.multiple) {
          defaultValues[name] = defaultValue || null;
        } else {
          defaultValues[name] = defaultValue || null;
        }
        break;
      default:
        break;
    }
  });
  return defaultValues;
};
