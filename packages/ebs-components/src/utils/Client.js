import { client as Graphql } from './Apollo';
import { ClientAxios } from './Axios';
import { buildQuery, buildQuerySM } from '../functions/query';

// * Function to extract header columns
export function extractColumns(headers) {
  let columns = [];
  headers.map((header) => {
    header.accessor
      ? columns.push(header)
      : header.columns.map((header) => {
          columns.push(header);
        });
  });
  return columns;
}
// * Function to call different clients (Brapi or Graphql)
// @param uri: api endpoint
// @param page: Query parameter to handle page number
// @param sort: Query parameter to handle sorting columns
// @param entity: Api Entity
// @param filters: Query parameter to handle filters
// @param columns: Columns extracted to handle content from API
// @param callStandard: Brapi or Graphql
export default async function Client({
  uri,
  id,
  page,
  sort,
  entity,
  filters,
  columns,
  callStandard,
  downloadCSV,
}) {
  return new Promise((resolve, reject) => {
    const dataOrder = () => {
      if (sort.length === 0) {
        switch (id) {
          case 'Users':
            return [{ col: 'userName', mod: 'ASC' }];
          case 'Roles':
            return [
              { col: 'isSystem', mod: 'ASC' },
              { col: 'createdOn', mod: 'DES' },
              { col: 'updatedOn', mod: 'ASC' },
            ];
          default:
            return [];
        }
      } else {
        return sort;
      }
    };

    const dataFilterDisjunction = () => {
      if (filters) {
        return filters.length == 1
          ? true
          : downloadCSV && filters.length > 0
          ? true
          : false;
      }
    };

    switch (callStandard.toLowerCase()) {
      case 'brapi':
        ClientAxios(uri, entity, query, page, sort, filters)
          .then((response) => {
            resolve(response);
          })
          .catch((error) => {
            reject(error);
          });
        break;
      case 'graphql':
        const QUERY = uri.includes('smapi')
          ? buildQuerySM(entity, columns)
          : buildQuery(entity, columns);
        Graphql(uri)
          .query({
            query: QUERY,
            variables: {
              page: page,
              sort: uri.includes('smapi') ? dataOrder()[0] : dataOrder(),
              filters: filters,
              disjunctionFilters: dataFilterDisjunction(),
            },
            fetchPolicy: 'no-cache',
          })
          .then(({ data }) => {
            let payload = {
              data: data[`find${entity}List`].content,
              pages: data[`find${entity}List`].totalPages,
              elements: data[`find${entity}List`].totalElements,
            };
            resolve(payload);
          })
          .catch((error) => reject(error));
        break;
      default:
        return;
    }
  });
}
