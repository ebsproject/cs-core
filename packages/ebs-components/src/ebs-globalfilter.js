import React from 'react';
import FilterForm from 'components/Molecules/FilterForm';

export const EbsGlobalFilter = React.forwardRef(
  ({ setOpenFilter, objectProperties, userProfile }, ref) => {
    const handleFilterClose = () => {
      setOpenFilter(false);
    };

    return (
      <div className='mt-10 ml-3'>
        <div className='mt-4'>
          <FilterForm            
            handleFilterClose={handleFilterClose}
            objectProperties={objectProperties}
            userProfile={userProfile}
          />
        </div>
      </div>
    );
  },
);
