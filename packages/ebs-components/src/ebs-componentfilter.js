import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Core, Lab } from '@ebs/styleguide';
const { Button, Tooltip, Typography, Box, Grid } = Core;
import FiedFilter from 'components/Organisms/FiedFilter';
import { useLayoutContext } from '@ebs/layout';

export const EbsFilter = React.forwardRef(({ componentsFilter }, ref) => {
  const { setFilterGridToApply } = useLayoutContext();

  const [filterArray, setfilterArray] = React.useState([]);
  const [state, setState] = React.useState(false);
  const [array, setArray] = React.useState([]);
  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth || 1300)
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [window.innerWidth])

  useEffect(() => {
    if (filterArray.length > 0) {
      setFilterGridToApply(filterArray);
    }
    setState(false);
  }, [filterArray]);
  const buttonAction = () => {
    setFilterGridToApply([]);
    setState(true);
    setfilterArray([]);
    setArray([]);
  };

  return (
    <>
      {
        width < 1300 ? (
          <>
            <Button
              aria-label='reset'
              color='primary'
              onClick={buttonAction}
            >
              Clear
            </Button>
            <FiedFilter
              componentsFilter={componentsFilter}
              filterArray={filterArray}
              setfilterArray={setfilterArray}
              state={state}
              array={array}
              setArray={setArray}
            />
          </>
        )
          : (
            <>
              <Button
                color='primary'
                onClick={buttonAction}
              >
                Clear
              </Button>
              <FiedFilter
                componentsFilter={componentsFilter}
                filterArray={filterArray}
                setfilterArray={setfilterArray}
                state={state}
                array={array}
                setArray={setArray}
              />
            </>
          )
      }
    </>
  );
});

//Type and required properties
EbsFilter.propTypes = {};
//Default properties
EbsFilter.defaultProps = {};
