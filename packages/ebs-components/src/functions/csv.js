function convertToCSV(objArray) {
  // From https://stackoverflow.com/a/46948292
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = '';

  console.log('str:', str);
  return array.reduce((str, next) => {
    str +=
      `${Object.values(next)
        .map((value) => `"${value}"`)
        .join(',')}` + '\r\n';
    return str;
  }, str);
}

export default function exportCSVFile(headers, data, fileTitle) {
  if (headers) {
    data.unshift(headers);
    // data.pop()
  }
  // Convert Object to JSON
  var jsonObject = JSON.stringify(data);

  var csv = convertToCSV(jsonObject);

  var exportedFilenmae = fileTitle + '.csv' || 'export.csv';

  var blob = new Blob([csv], { type: 'text/csvcharset=utf-8' });
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, exportedFilenmae);
  } else {
    var link = document.createElement('a');
    if (link.download !== undefined) {
      // feature detection
      // Browsers that support HTML5 download attribute
      var url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', exportedFilenmae);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}
