import { gql } from '@apollo/client';
export const buildQuery = (entity, apiContent) => {
  let res;
  entity !== "WorkflowCustomField" ?
 res = gql`
    query find${entity}List($page: PageInput, $sort:[SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            content
             {
                ${apiContent.map((item) => {
                  const levels = item.accessor.split('.');
                  // Setting levels
                  return extractLevels(levels);
                })}
            }
        }
    }`
:
res = gql`
query find${entity}List($page: PageInput, $sort:[SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
    find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            numberOfElements
            content
    }
}`;
return res;
};

export const buildQuerySM = (entity, apiContent) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: SortInput, $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            content {
                ${apiContent.map((item) => {
                  const levels = item.accessor.split('.');
                  // Setting levels
                  return extractLevels(levels);
                })}
            }
        }
    }`;
};

function extractLevels(arr) {
  let levels = arr;
  if (levels.length > 1) {
    return `${levels.shift()}{
        ${extractLevels(levels)}
    }`;
  } else {
    return `${levels[0]}`;
  }
}

export const FIND_DOMAIN_LIST = gql`
  query findDomainList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findDomainList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        prefix
        name
        domaininstances{
          sgContext
        }
      }
    }
  }
`;