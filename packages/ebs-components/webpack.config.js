const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'ebs',
    projectName: 'components',
    webpackConfigEnv,
    argv,
  });

  defaultConfig.externals.push(
    "@ebs/layout"
  );

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    resolve: {
      alias: {
        components: path.resolve(__dirname, './src/components/'),
        functions: path.resolve(__dirname, './src/functions/'),
        utils: path.resolve(__dirname, './src/utils/'),
        hooks: path.resolve(__dirname, './src/hooks/'),
        context:path.resolve(__dirname, './src/context/'),
      },
    },
    // plugins: [
    //   new BundleAnalyzerPlugin()
    // ]
  });
};
