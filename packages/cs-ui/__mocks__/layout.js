import { forwardRef, createElement } from "react";

export const getContext = jest.fn();
export const getAuthState = jest.fn();
export const getTokenId = jest.fn();
export const getCoreSystemContext = () => ({ graphqlUri: "https://localhost:8080/graphql" });
export const getDomainContext = () => ({sgContext:"https://cbapi-dev.ebsproject.org/v3/" });
export const RBAC = forwardRef(({allowedAction}, ref) => <div />)
