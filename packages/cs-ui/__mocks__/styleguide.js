import { forwardRef, createElement } from "react";
export const DropZone ={
  DropzoneDialog :forwardRef(({ children, ...rest }, ref) => <>{children}</>)
}
export const Lab = {
  Autocomplete : forwardRef(({ children, ...rest }, ref) => <>{children}</>)
}
export const Core = {
  AppBar: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Box: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Backdrop: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CircularProgress: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  DialogContentText: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  DialogTitle: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Dialog: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Select: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  MenuItem: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Divider: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Switch: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  List: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Card: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Grid: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CardHeader: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CardContent: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  CardMedia: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Toolbar: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Button: forwardRef(({ children,startIcon, ...rest }, ref) => (
    <button {...rest}>{children}</button>
  )),
  ListItem: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Snackbar: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  LinearProgress: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  SnackbarContent: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  DialogActions: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  DialogContent: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Icon: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  IconButton: forwardRef(({ children, ...rest }, ref) => (
    <button {...rest}>{children}</button>
  )),
  TextField: forwardRef(
    (
      {
        helperText,
        fullWidth,
        InputLabelProps,
        InputProps,
        inputProps,
        error,
        ...rest
      },
      ref
    ) => createElement("input", { ...rest })
  ),
  Tooltip: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Typography: forwardRef(({ children, ...rest }, ref) => (
    <label {...rest}>{children}</label>
  )),
  FormControl: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  InputLabel: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  Input: forwardRef(
    ({ inputComponent, InputLabelProps, InputProps, ...rest }, ref) =>
      createElement("input", { ...rest })
  ),
  Checkbox: forwardRef(({ inputProps, ...rest }, ref) =>
    createElement("input", { ...rest })
  ),
  FormControlLabel: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
  //   Typography: forwardRef(({ children,...rest }, ref) =>     <>{children}</>),
  //   Typography: forwardRef(({ children,...rest }, ref) =>     <>{children}</>),
  //   Typography: forwardRef(({ children,...rest }, ref) =>     <>{children}</>),
};

export const Icons = {
  Add: forwardRef((props, ref) => <div {...props} />),
  Close: forwardRef((props, ref) => <div {...props} />),
  PostAdd: forwardRef((props, ref) => <div {...props} />),
  Edit: forwardRef((props, ref) => <div {...props} />),
  Delete: forwardRef((props, ref) => <div {...props} />),
  CheckCircle: forwardRef((props, ref) => <div {...props} />),
  Cancel: forwardRef((props, ref) => <div {...props} />),
  ExpandLess: forwardRef((props, ref) => <div {...props} />),
  ExpandMore: forwardRef((props, ref) => <div {...props} />),
  Folder: forwardRef((props, ref) => <div {...props} />),
  AccountBox: forwardRef((props, ref) => <div {...props} />),
};
export const Colors = {};

export const EbsDialog = forwardRef(({ children, title, ...rest }, ref) => (
  <>
    <label>{title}</label>
    {children}
  </>
));

export const EbsTabsLayout = forwardRef(({ tabs, ...rest }, ref) => (
  <div {...rest} ref={ref}>
    {tabs.map((tab, key) => (
      <div key={key}>
        <label>{tab.label}</label>
        {tab.component}
      </div>
    ))}
  </div>
));

export const EBSMaterialUIProvider = forwardRef(({ children }, ref) => {
  return (
    <div>
      Material UI Provider
      <div>{children}</div>
    </div>
  );
});
export const Styles = {
  makeStyles: jest.fn().mockImplementation((callback) => {
    callback({
      options: { common: { fonts: { sizes: {} } } },
      spacing: jest.fn(),

      palette: {
        background:{paper:{}},
        text:{ primary: {}, secondary: {main:{}},common:{}}
        ,primary: {}
        ,success: {dark:{}}
        ,secondary: {main:{}}
        ,common:{}
        ,
      
      },
    }); // this will execute the fn passed in which is missing the coverage
    return jest.fn().mockReturnValue({ root: "" }); // here the expected MUI styles });
  }),
};