import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import App from "./root.component";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("Root component", async () => {
  render(<App />);
  expect(screen.getByTestId("root")).toBeInTheDocument();
});
