import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { EbsForm } from "@ebs/components";
import {Core,Icons} from "@ebs/styleguide";
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Tooltip,
  Typography,
} = Core;
import { useDispatch, useSelector } from "react-redux";
const {Edit} = Icons;
import { modifyCustomer } from "store/modules/TenantManagement";

const ModifyProductButton = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch();
    const { success } = useSelector(({ tenant }) => tenant);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    const handleClickOpen = () => {
      // console.log(rowSelected);
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const definitionCustomer = ({ getValues, setValue, reset }) => {
      return {
        name: "ModifyCustomer",
        components: [
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "id",
            inputProps: {
              "data-testid": "fromDomain",
              label: "Id",
              disabled: true,
              hidden: true,
            },
            defaultValue: rowSelected && rowSelected.id,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "fromDomain",
              label: "Name",
              disabled: false,
            },
            rules: {
              required: "A name is required",
            },
            defaultValue: rowSelected && rowSelected.name,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "officialEmail",
            inputProps: {
              "data-testid": "officialEmail",
              label: "Email",
            },
            rules: {
              required: "A Email is required",
            },
            defaultValue: rowSelected && rowSelected.officialEmail,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "alternateEmail",
            inputProps: {
              "data-testid": "alternateEmail",
              label: "Alternate Email",
            },
            defaultValue: rowSelected && rowSelected.alternateEmail,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "phone",
            inputProps: {
              "data-testid": "phone",
              label: "Phone",
            },
            rules: {
              required: "A Phone is required",
            },
            defaultValue: rowSelected && rowSelected.phone,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "jobTitle",
            inputProps: {
              "data-testid": "jobTitle",
              label: "Job Title",
            },
            defaultValue: rowSelected && rowSelected.jobTitle,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "languagePreference",
            inputProps: {
              "data-testid": "languagePreference",
              label: "Language Preference",
            },
            defaultValue: rowSelected && rowSelected.languagePreference,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "phoneExtension",
            inputProps: {
              "data-testid": "phoneExtension",
              label: "Phone Extension",
            },
            defaultValue: rowSelected && rowSelected.phoneExtension,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "File",
            name: "logo",
            label: "UPLOAD CUSTOMER LOGO...",
            customProps: {
              button: { color: "primary", size: "large" },
              input: {
                acceptedFiles: [".png",".jpg",".img"],
                cancelButtonText: "cancel",
                submitButtonText: "submit",
                maxFileSize: 5000000,
                showPreviews: true,
                showFileNamesInPreview: true,
                isMulti: false,
              },
            },
            helper: { title: "Logo", placement: "right", arrow: true },
            defaultValue: rowSelected && rowSelected.logo,
          },
        ],
      };
    };

    const onSubmit = (FormData) => {
      const imagen = FormData.logo;
      var base64regex =
        /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
      var validateBase64 = base64regex.test(imagen);
      // validation if the logo is null
      if (imagen === undefined) {
        //const valueOrganization = FormData.owningOrganizationId.value
        modifyCustomer({
          ...FormData,
          id: Number(FormData.id),
          active: false,
          logo: null,
          //owningOrganizationId: valueOrganization,
          phoneExtension:
            FormData.phoneExtension === ""
              ? null
              : Number(FormData.phoneExtension),
        })(dispatch);
      } else {
        // validate base64 for modify
        if (validateBase64 === true) {
          modifyCustomer({
            ...FormData,
            id: Number(FormData.id),
            active: false,
            logo: imagen,
            //owningOrganizationId: valueOrganization,
            phoneExtension:
              FormData.phoneExtension === ""
                ? null
                : Number(FormData.phoneExtension),
          })(dispatch);
        } else {
          const img = FormData.logo[0];
          var reader = new FileReader();
          reader.readAsDataURL(img);
          // conversion to base 64
          reader.onload = function () {
            var arrayAuxiliar = [];
            var base64 = reader.result;
            arrayAuxiliar = base64.split(",");
            var imagen64 = arrayAuxiliar[1];
            modifyCustomer({
              ...FormData,
              id: Number(FormData.id),
              active: false,
              logo: imagen64,
              //owningOrganizationId: valueOrganization,
              phoneExtension:
                FormData.phoneExtension === ""
                  ? null
                  : Number(FormData.phoneExtension),
            })(dispatch);
          };
        }
      }
    };

    return (
      /* 
     @prop data-testid: Id to use inside addproductbutton.test.js file.
     */
      <div ref={ref} data-testid={"ModifyCustomerButtonTestId"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Update Customer" />
            </Typography>
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="modify-customer"
            color="primary"
          >
            <Edit />
          </IconButton>
        </Tooltip>
        <Dialog
          onClose={handleClose}
          open={open}
          aria-label="modifyCustomerDialog"
        >
          <DialogTitle className="absolute">
            <FormattedMessage id="none" defaultMessage="Update Customer" />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={onSubmit} definition={definitionCustomer}>
              <DialogActions>
                <Button onClick={handleClose}>
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
ModifyProductButton.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
ModifyProductButton.defaultProps = {};

export default ModifyProductButton;
