import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import ModifyOrganizationButton from "./ModifyOrganizationButton";
import '@testing-library/jest-dom/extend-expect'
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";
let translation={
  none:"test"
}

afterEach(cleanup);

test('ModifyOrganizationButton is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <ModifyOrganizationButton rowSelected={[{}]} refresh={() => {}}>        
       </ModifyOrganizationButton>
       </IntlProvider>
    </Provider>
  )
  expect(screen.getByTestId('ModifyOrganizationButtonTestId')).toBeInTheDocument();
})
