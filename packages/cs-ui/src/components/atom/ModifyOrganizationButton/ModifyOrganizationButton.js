import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { Core, Styles, Icons } from "@ebs/styleguide";
import { EbsForm } from "@ebs/components";

const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Tooltip,
  Typography
} = Core;
const { Edit } = Icons;
import { useDispatch, useSelector } from "react-redux";
import { modifyOrganization } from "store/modules/TenantManagement";



const ModifyOrganizationButton = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch();
    const { success } = useSelector(({ tenant }) => tenant);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const definitionOrganization = ({ getValues, setValue, reset }) => {
      return {
        name: "modifyOrganization",
        components: [
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "id",
            inputProps: {
              "data-testid": "id",
              label: "Id",
              disabled: true,
              hidden: true,
            },
            defaultValue: rowSelected && rowSelected.id,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Organization name",
            },
            rules: {
              required: "An Organization name is required",
            },
            defaultValue: rowSelected && rowSelected.name,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "legalName",
            inputProps: {
              "data-testid": "legalName",
              label: "Organization Legal Name",
            },
            defaultValue: rowSelected && rowSelected.legalName,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "code",
            inputProps: {
              "data-testid": "code",
              label: "Organization code",
              disabled: true,
            },
            rules: {
              required: "An Organization code is required",
            },
            defaultValue: rowSelected && rowSelected.code,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "phone",
            inputProps: {
              "data-testid": "phone",
              label: "Organization phone",
            },
            rules: {
              required: "An Organization code is required",
            },
            defaultValue: rowSelected && rowSelected.phone,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "webPage",
            inputProps: {
              "data-testid": "webPage",
              label: "Organization Web Page",
            },
            defaultValue: rowSelected && rowSelected.webPage,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "slogan",
            inputProps: {
              "data-testid": "slogan",
              label: "Organization slogan",
            },
            defaultValue: rowSelected && rowSelected.slogan,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "File",
            name: "logo",
            label: "UPLOAD ORGANIZATION LOGO...",
            customProps: {
              button: { color: "primary", size: "large" },
              input: {
                acceptedFiles: ["image/*"],
                cancelButtonText: "cancel",
                submitButtonText: "submit",
                maxFileSize: 5000000,
                showPreviews: true,
                showFileNamesInPreview: true,
                isMulti: false,
              },
            },
            helper: { title: "Logo", placement: "right", arrow: true },
            defaultValue: rowSelected && rowSelected.logo,
          },
        ],
      };
    };

    const onSubmit = (formData) => {
      const imagen = formData.logo;
      var base64regex =
        /^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/;
      var validateBase64 = base64regex.test(imagen);
      // validation if the logo is null
      if (imagen === undefined) {
        modifyOrganization({
          ...formData,
          active: true,
          logo: null,
          phone: Number(formData.phone),
          defaultAuthenticationId:
            formData.defaultAuthenticationId == ""
              ? null
              : Number(formData.defaultAuthenticationId),
          defaultThemeId: Number(
            formData.defaultThemeId == ""
              ? null
              : Number(formData.defaultThemeId)
          ),
        })(dispatch);
      } else {
        if (validateBase64 === true) {
          modifyOrganization({
            ...formData,
            active: true,
            logo: imagen,
            phone: Number(formData.phone),
            defaultAuthenticationId:
              formData.defaultAuthenticationId == ""
                ? null
                : Number(formData.defaultAuthenticationId),
            defaultThemeId: Number(
              formData.defaultThemeId == ""
                ? null
                : Number(formData.defaultThemeId)
            ),
          })(dispatch);
        } else {
          const img = formData.logo[0];
          var reader = new FileReader();
          reader.readAsDataURL(img);
          // conversion to base 64
          reader.onload = function () {
            var arrayAuxiliar = [];
            var base64 = reader.result;
            arrayAuxiliar = base64.split(",");
            var imagen64 = arrayAuxiliar[1];
            modifyOrganization({
              ...formData,
              active: true,
              logo: imagen64,
              phone: Number(formData.phone),
              defaultAuthenticationId:
                formData.defaultAuthenticationId == ""
                  ? null
                  : Number(formData.defaultAuthenticationId),
              defaultThemeId: Number(
                formData.defaultThemeId == ""
                  ? null
                  : Number(formData.defaultThemeId)
              ),
            })(dispatch);
          };
        }
      }
    };

    return (
      <div ref={ref} data-testid={"ModifyOrganizationButtonTestId"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Update Organization" />
            </Typography>
          }
        >
          <IconButton
            aria-label="modify-organization"
            color="primary"
            onClick={handleClickOpen}
          >
            <Edit />
          </IconButton>
        </Tooltip>
        <Dialog onClose={handleClose} open={open} aria-label="addProductDialog">
          <DialogTitle className="absolute">
            <FormattedMessage id="none" defaultMessage="Update Organization" />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={onSubmit} definition={definitionOrganization}>
              <DialogActions>
                <Button onClick={handleClose} >
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
ModifyOrganizationButton.propTypes = {
  refresh: PropTypes.func.isRequired,
};

ModifyOrganizationButton.defaultProps = {};

export default ModifyOrganizationButton;
