import React, { useEffect } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import {Core,Icons} from "@ebs/styleguide";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Typography,
  Tooltip,
} = Core;
const {Delete}= Icons
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { deleteOrganization } from "store/modules/TenantManagement";

const DeleteOrganizationButton = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();
    const { success } = useSelector(({ tenant }) => tenant);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleDelete = () => {
      deleteOrganization(rowSelected.id)(dispatch);
    };

    return (
      <div ref={ref} data-testid={"DeleteOrganizationButtonTestId"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Delete Organization" />
            </Typography>
          }
        >
          <IconButton
            aria-label="delete-organization"
            color="primary"
            onClick={handleClickOpen}
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <Dialog
          fullWidth
          keepMounted
          maxWidth="sm"
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Delete Organization" />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id="none"
                defaultMessage="Are you sure to delete the Organization?"
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <Typography variant="button" color="secondary">
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Typography>
            </Button>
            <Button onClick={handleDelete}>
              <Typography variant="button">
                <FormattedMessage id="none" defaultMessage="Confirm" />
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
);

DeleteOrganizationButton.propTypes = {
  refresh: PropTypes.func.isRequired,
};

DeleteOrganizationButton.defaultProps = {};

export default DeleteOrganizationButton;
