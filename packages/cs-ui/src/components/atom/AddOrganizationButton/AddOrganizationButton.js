import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { EbsForm } from "@ebs/components";
import {Core,Icons} from "@ebs/styleguide";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} = Core;
import { useDispatch, useSelector } from "react-redux";
const { PostAdd } = Icons;
import { createOrganization } from "store/modules/TenantManagement";

const AddOrganizationButton = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const { success } = useSelector(({ tenant }) => tenant);
    const dispatch = useDispatch();
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const definitionOrganization = ({ getValues, setValue, reset }) => {
      return {
        name: "createOrganization",
        components: [
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Name",
            },
            rules: {
              required: "An Organization name is required",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "legalName",
            inputProps: {
              "data-testid": "legalName",
              label: "Legal Name",
            },
            rules: {
              required: "An Organization legal name is required",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "code",
            inputProps: {
              "data-testid": "code",
              label: "Code",
            },
            rules: {
              required: "An Organization code is required",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "phone",
            inputProps: {
              "data-testid": "phone",
              label: "Phone",
            },
            rules: {
              required: "An Organization code is required",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "webPage",
            inputProps: {
              "data-testid": "webPage",
              label: "Web Page",
            },
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "slogan",
            inputProps: {
              "data-testid": "slogan",
              label: "Slogan",
            },
          },
          // {
          //   sizes: [12, 12, 12, 12, 12],
          //   component: "Select",
          //   name: "defaultAuthenticationId",
          //   label: "Organization Default Authentication Id",
          //   options: [
          //     { label: "1", value: 1 },
          //     { label: "2", value: 2 },
          //   ],
          //   inputProps: {
          //     "data-testid": "defaultAuthenticationId",
          //     placeholder: "Organization Default Authentication Id",
          //   },
          //   rules: {
          //     required: "An Organization name is required",
          //   },
          //   helper: {
          //     title: "Organization Default Authentication Id",
          //     placement: "top",
          //   },
          // },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "File",
            name: "logo",
            label: "UPLOAD ORGANIZATION LOGO...",
            customProps: {
              button: { color: "primary", size: "large" },
              input: {
                acceptedFiles: [".png"],
                cancelButtonText: "cancel",
                submitButtonText: "submit",
                maxFileSize: 5000000,
                showPreviews: true,
                showFileNamesInPreview: true,
                isMulti: false,
              },
            },
            helper: { title: "Logo", placement: "right", arrow: true },
          },
        ],
      };
    };

    const onSubmit = (formData) => {
      const logoOrganization = formData.logo;
      // validation is the is null
      if (logoOrganization === undefined) {
        createOrganization({
          id: 0,
          active: true,
          taxId: "sads",
          defaultAuthenticationId: null,
          defaultThemeId: null,
          // phone: toString(formData.phone),
          ...formData,
        })(dispatch);
      } else {
        const imagen = logoOrganization[0];
        var reader = new FileReader();
        reader.readAsDataURL(imagen);
        // conversion to base 64
        reader.onload = function () {
          var arrayAuxiliar = [];
          var base64 = reader.result;
          arrayAuxiliar = base64.split(",");
          var imagen64 = arrayAuxiliar[1];
          createOrganization({
            ...formData,
            id: 0,
            active: true,
            taxId: null,
            logo: imagen64,
            defaultAuthenticationId: null,
            defaultThemeId: null,
            // phone: toString(formData.phone),
          })(dispatch);
        };
      }
    };

    return (
      <div ref={ref} data-testid={"AddOrganizationButtonTestId"}>
        <Button
          className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          variant="contained"
          aria-label="add-organization"
          color="primary"
          startIcon={<PostAdd className="fill-current text-white" />}
          onClick={handleClickOpen}
          disabled={rowSelected ? false : true}
        >
          <Typography className="text-white">
            <FormattedMessage id="none" defaultMessage="Add Organization" />
          </Typography>
        </Button>
        <Dialog onClose={handleClose} open={open}>
          <DialogTitle className="absolute">
            <FormattedMessage id="none" defaultMessage="New Organization" />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={onSubmit} definition={definitionOrganization}>
              <DialogActions>
                <Button onClick={handleClose} color="secondary">
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);

AddOrganizationButton.propTypes = {
  refresh: PropTypes.func.isRequired,
};

AddOrganizationButton.defaultProps = {};

export default AddOrganizationButton;
