import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useForm, Controller } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import { createInfoType } from "store/modules/CRM";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, Icons, EbsDialog } from "@ebs/styleguide";
// CORE COMPONENTS
const { Box, Button, DialogActions, TextField, Typography } = Core;
const { Cancel, CheckCircle, PostAdd } = Icons;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ContactInfoTypeFormAtom = React.forwardRef(({ refresh }, ref) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const { contactInfoSuccess } = useSelector(({ crm }) => crm);
  const {
    control,
    formState: { errors },
    handleSubmit,
    reset,
    setValue,
    getValues,
  } = useForm({
    defaultValues: { id: 0 },
  });
  useEffect(() => {
    if (contactInfoSuccess) {
      handleClose();
      refresh();
    }
  }, [contactInfoSuccess]);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const submit = (data) => {
    createInfoType({ data })(dispatch);
  };
  return (
    /**
     * @prop data-testid: Id to use inside ContactInfoTypeForm.test.js file.
     */
    <div data-testid={"ContactInfoTypeFormTestId"} ref={ref}>
      <Button
        startIcon={<PostAdd className="fill-current text-white" />}
        onClick={handleOpen}
        className="bg-ebs-brand-default rounded-md text-white hover:bg-ebs-brand-900 ml-3"
      >
        <Typography className="text-white">
          <FormattedMessage id={"none"} defaultMessage={"New Info Type"} />
        </Typography>
      </Button>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        title={
          <Typography className="font-ebs text-xl">
            <FormattedMessage id={"none"} defaultMessage={"Info Type"} />
          </Typography>
        }
        maxWidth="sm"
      >
        <form
          name="infoTypeForm"
          onSubmit={handleSubmit(submit)}
          className="p-4"
        >
          <Box className="grid grid-cols-1 gap-3 ml-4">
            <Controller
              name="id"
              control={control}
              render={({ field }) => (
                <TextField sx={{ display: "none" }} {...field} label={"id"} />
              )}
            />
            <Controller
              name="name"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  label={
                    <Typography className="font-ebs font-semibold">
                      <FormattedMessage id={"none"} defaultMessage={"Name"} />
                      {` *`}
                    </Typography>
                  }
                  error={
                    field.value || Boolean(errors["name"]) === false
                      ? false
                      : true
                  }
                  helperText={
                    field.value ||
                    Boolean(errors["name"]) === false ? null : (
                      <Typography className="text-sm font-ebs text-red-600">
                        <FormattedMessage
                          id="none"
                          defaultMessage="⚠ Field is required"
                        />
                      </Typography>
                    )
                  }
                />
              )}
              rules={{required: true}}
            />
            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  label={
                    <Typography className="font-ebs font-semibold">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"Description"}
                      />
                    </Typography>
                  }
                  error={
                    field.value || Boolean(errors["description"]) === false
                      ? false
                      : true
                  }
                  helperText={
                    field.value ||
                    Boolean(errors["description"]) === false ? null : (
                      <Typography className="text-sm font-ebs text-red-600">
                        <FormattedMessage
                          id="none"
                          defaultMessage="⚠ Field is required"
                        />
                      </Typography>
                    )
                  }
                />
              )}
              rules={{required: true}}
            />
          </Box>
          <DialogActions>
            <Button
              onClick={handleClose}
              startIcon={<Cancel />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
            </Button>
            <Button
              type="submit"
              startIcon={<CheckCircle />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Save"} />
            </Button>
          </DialogActions>
        </form>
      </EbsDialog>
    </div>
  );
});
// Type and required properties
ContactInfoTypeFormAtom.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
ContactInfoTypeFormAtom.defaultProps = {};

export default ContactInfoTypeFormAtom;
