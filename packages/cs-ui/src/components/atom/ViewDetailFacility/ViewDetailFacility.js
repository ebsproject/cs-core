import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import { clientCb } from "utils/apollo/apollo";
import { useQuery } from "@apollo/client";
import { FIND_FACILITY_LIST } from "utils/apollo/gql/placeManager";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ViewDetailFacilityAtom = React.forwardRef(({ data }, ref) => {
  //Data Details
  const {
    loading,
    data: dataList,
    error,
  } = useQuery(FIND_FACILITY_LIST, {
    variables: {
      page: { number: 1, size: 10 },
      filters: [{ col: "id", mod: "EQ", val: data?.id }],
    },
    fetchPolicy: "no-cache",
    client: clientCb,
  });

  if (loading) return "Loading...";
  const dataDetail = dataList?.findFacilityList?.content[0];
  const definitionCreateSite = ({ getValues, setValue, reset }) => {
    return {
      name: "Details",
      components: [
        {
          sizes: [12, 12, 12, 12, 12],
          component: "map",
          name: "coordinates",
          inputProps: {
            "data-testid": "field",
            label: "Choose Field",
            coordinates: {
              lat: 19.5307604699681,
              lng: -98.84719079491951,
            },
            defaultValue: dataDetail && dataDetail?.coordinates,
          },
          rules: {
            required: "A Field is required",
          },
        },
      ],
    };
  };

  const onSubmit = (formData) => {};
  return dataDetail?.facilityClass === "structure" &&
    dataDetail?.parentFacility === null ? (
    <div>
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Facility Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Facility Code"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityCode}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Site" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObject.geospatialObjectName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Site Type"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityType}
          </Typography>
        </div>
        {/* <div className="col-span-2">
            <Typography variant="h6" display="block" gutterBottom>
              <FormattedMessage
                id="none"
                defaultMessage="Geospatial Coordinates"
              />
            </Typography>
            <Typography variant="body1" display="block" gutterBottom>
              {dataDetail?.coordinates?.features?.[0]?.geometry.type ===
              "Polygon"
                ? dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()
                : dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()}
            </Typography>
          </div> */}
      </div>
      <br />
      {/* <EbsForm
          onSubmit={onSubmit}
          definition={definitionCreateSite}
          actionPlacement="top"
        ></EbsForm> */}
    </div>
  ) : dataDetail?.facilityClass === "container" ? (
    <div>
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Container Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Container Code"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityCode}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Facility" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.parentFacility.facilityName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Container Type"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityType}
          </Typography>
        </div>
      </div>
    </div>
  ) : (
    <div>
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Sub-Facility Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Sub-Facility Code"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityCode}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Facility" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.parentFacility.facilityName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Sub-Facility Type"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.facilityType}
          </Typography>
        </div>
        {/* <div className="col-span-2">
            <Typography variant="h6" display="block" gutterBottom>
              <FormattedMessage
                id="none"
                defaultMessage="Geospatial Coordinates"
              />
            </Typography>
            <Typography variant="body1" display="block" gutterBottom>
              {dataDetail?.coordinates?.features?.[0]?.geometry.type ===
              "Polygon"
                ? dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()
                : dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()}
            </Typography>
          </div> */}
      </div>
      <br />
      {/* <EbsForm
          onSubmit={onSubmit}
          definition={definitionCreateSite}
          actionPlacement="top"
        ></EbsForm> */}
    </div>
  );
});
// Type and required properties
ViewDetailFacilityAtom.propTypes = {};
// Default properties
ViewDetailFacilityAtom.defaultProps = {};

export default ViewDetailFacilityAtom;
