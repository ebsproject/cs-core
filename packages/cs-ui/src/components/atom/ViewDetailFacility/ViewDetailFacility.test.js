import ViewDetailFacility from './ViewDetailFacility';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ViewDetailFacility is in the DOM', () => {
  render(<ViewDetailFacility></ViewDetailFacility>)
  expect(screen.getByTestId('ViewDetailFacilityTestId')).toBeInTheDocument();
})
