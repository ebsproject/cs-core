import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import DeleteHierarchyTreeInfo from "components/atom/DeleteHierarchyTreeInfo";
import {RBAC} from "@ebs/layout"
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const PermissionsRowActionsAtom = React.forwardRef(({ rowData, refresh }, ref) => {

  /**
   * @param {String} type
   * @param {String} method
   * @param {ID} id: optional
   */
  return (
    /**
     * @prop data-testid: Id to use inside ContactRowActions.test.js file.
     */
    <div
      data-testid={"PermissionsRowActionsTestId"}
      ref={ref}
      className="flex flex-row flex-nowrap gap-2"
    >
      <RBAC allowedAction={"Delete"}>
      <DeleteHierarchyTreeInfo rowData={rowData} refresh={refresh}/>
      </RBAC>
   
    </div>
  );
});
// Type and required properties
PermissionsRowActionsAtom.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func,
};
// Default properties
PermissionsRowActionsAtom.defaultProps = {};

export default PermissionsRowActionsAtom;
