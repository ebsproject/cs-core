import UserActiveSwitch from './ActiveSwitch';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import '@testing-library/jest-dom/extend-expect'
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";
let translation={
  none:"test"
}

afterEach(cleanup);

test('UserActiveSwitch is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <UserActiveSwitch>        
       </UserActiveSwitch>
       </IntlProvider>
    </Provider>
  )
  expect(screen.getByTestId('UserActiveSwitchTestId')).toBeInTheDocument();
})