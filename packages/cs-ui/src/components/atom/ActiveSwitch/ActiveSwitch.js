import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
import { useSelector, useDispatch } from "react-redux";
import { modifyIsActive } from "store/modules/UserManagement";
import { modifyCgiar } from "store/modules/CRM";
import { getContext } from "@ebs/layout";
const { Button, Typography, Switch, Tooltip } = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const UserActiveSwitchAtom = React.forwardRef(
  ({ rowData, refresh, method, type }, ref) => {
    const [disabled, setDisabled] = useState(false);
    const dispatch = useDispatch();
    const context = getContext();
    const handleChange = async (event) => {
      event.preventDefault();
      setDisabled(true);
      let newData = {
        id: Number(rowData.id),
        active: event.target.checked,
        userName: rowData.userName,
        contactId: rowData.contact.id,
        tenantIds: [context.tenantId],
      };
      await modifyIsActive({ data: newData, dispatch });
      setDisabled(false);
      refresh();
    };
    const handleChangeInstitution = (event) => {
      let newData = {
        id: Number(rowData?.id),
        category: rowData?.category,
        purposeIds: rowData?.purposes?.map((item) => item.id),
        institution: {
          commonName: rowData?.institution?.commonName,
          legalName: rowData?.institution?.legalName,
          cgiar: event.target.checked,
          cropIds:
            rowData?.institution?.crops?.length === 0
              ? []
              : rowData?.institution?.crops?.map((item) => item.id),
        },
        addresses: {
          id: rowData.addresses[0].id,
          location: rowData.addresses[0].location,
          region: rowData.addresses[0].region,
          zipCode: rowData.addresses[0].zipCode,
          streetAddress: rowData.addresses[0].streetAddress,
          default: rowData.addresses[0].default,
          countryId: rowData.addresses[0].country.id,
          additionalStreetAddress: rowData.addresses[0].additionalStreetAddress,
          purposeIds: rowData.addresses[0].purposes.map((data) =>
            Number(data.id)
          ),
        },
      };

      event.preventDefault();
      modifyCgiar(newData)(dispatch);
      refresh();
    };
    if (type === "USER") {
      return (
        /* 
       @prop data-testid: Id to use inside UserActiveSwitch.test.js file.
       */
        <div data-testid={"UserActiveSwitchTestId"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Change user status"}
                />
              </Typography>
            }
          >
            <Switch
              disabled={disabled}
              size="small"
              color="primary"
              className="mt-2"
              checked={rowData?.isActive}
              onChange={handleChange}
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
          </Tooltip>
        </div>
      );
    } else {
      return (
        /* 
       @prop data-testid: Id to use inside UserActiveSwitch.test.js file.
       */
        <div data-testid={"UserActiveSwitchTestId"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Change institution status"}
                />
              </Typography>
            }
          >
            <Switch
              size="small"
              color="primary"
              className="mt-2"
              checked={rowData?.institution.isCgiar}
              onChange={handleChangeInstitution}
              inputProps={{ "aria-label": "secondary checkbox" }}
            />
          </Tooltip>
        </div>
      );
    }
  }
);
// Type and required properties
UserActiveSwitchAtom.propTypes = {};
// Default properties
UserActiveSwitchAtom.defaultProps = {};

export default UserActiveSwitchAtom;
