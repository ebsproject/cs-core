import React from "react";
import { client } from "utils/apollo";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/client";
import { useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons, Lab } from "@ebs/styleguide";
const {
  Box,
  Button,
  Checkbox,
  DialogActions,
  FormControlLabel,
  TextField,
  Typography,
  Grid
} = Core
const { Autocomplete } = Lab;
const { Cancel, CheckCircle } = Icons;
import { FIND_CONTACT_INFO_TYPE_LIST } from "utils/apollo/gql/crm";
import { mutationContactInfo } from "store/modules/CRM";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ContactInfoFormAtom = React.forwardRef(
  ({ method, defaultValues, handleClose, contactId }, ref) => {
    const dispatch = useDispatch();
    const {
      control,
      formState: { errors },
      handleSubmit,
      reset,
      setValue,
      getValues,
    } = useForm({
      defaultValues:
        method === "POST"
          ? {
            contactId: contactId,
            default: false,
            value: "",
            id: 0,
            contactInfoType: null,
          }
          : defaultValues,
    });
    const { data, errors: error } = useQuery(FIND_CONTACT_INFO_TYPE_LIST, {
      variables: { page: { number: 1, size: 300 } },
      fetchPolicy: "no-cache",
      client: client,
    });

    /**
     * @param {Object} e
     * @param {Object} options
     */
    const onChange = (e, options) => {
      e.preventDefault();
      setValue("contactInfoType", options);
    };
    /**
     * @param {Object} option
     */
    const getOptionLabel = (option) => `${option.name}`;
    /**
     * @param {Object} params
     */
    const renderInput = (params) => (
      <TextField
        {...params}
        focused={Boolean(errors["contactInfoType"])}
        error={Boolean(errors["contactInfoType"])}
        label={
          <Typography className="font-ebs font-semibold text-">
            <FormattedMessage id={"none"} defaultMessage={"Type"} />
            {` *`}
          </Typography>
        }
        helperText={errors["contactTypeInfo"]?.message}
      />
    );
    /**
     *
     * @param {String} value
     */
    const validate = (value) => {
      // ? Validate email
      if (getValues("contactInfoType").id === 2) {
        const emailExpression = new RegExp("^[^@]+@[^@]+.[a-zA-Z]{2,}$");
        return emailExpression.test(value) ? true : "⚠ Invalid email";
      } else {
        return true;
      }
    };
    /**
     * @param {Object} data
     */
    const submit = (data, e) => {
      e.target.name === "ContactInfoForm" &&
        mutationContactInfo({ method, data })(dispatch);
    };

    return (
      /**
       * @prop data-testid: Id to use inside ContactInfoForm.test.js file.
       */
      <form onSubmit={handleSubmit(submit)} name="ContactInfoForm" data-testid={"ContactInfoFormTestId"}>
        <div
          data-testid={"ContactInfoFormTestId"}
          ref={ref}

        >
          <Controller
            id={"contactId"}
            name={"contactId"}
            control={control}
            render={({ field }) => (
              <TextField {...field} label={"id"} sx={{ display: "none" }} />
            )}
          />
          <Controller
            name={`id`}
            control={control}
            render={({ field }) => (
              <TextField {...field} label={"id"} sx={{ display: "none" }} />
            )}
          />
          <Grid container spacing={2} direction="column">
            <Grid item xs={6}>
              <Controller
                name={`contactInfoType`}
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    id={"contactInfoType"}
                    options={data?.findContactInfoTypeList?.content || []}
                    onChange={onChange}
                    getOptionLabel={getOptionLabel}
                    renderInput={renderInput}
                  />
                )}
              />
            </Grid>
            <Grid item xs={6}>
              <Controller
                name={`value`}
                control={control}
                render={({ field }) => (
                  <TextField
                  fullWidth
                  InputLabelProps={{ shrink: true }}
                    {...field}
                    label={
                      <FormattedMessage id={"none"} defaultMessage={"Value"} />
                    }
                    focused={Boolean(errors["value"])}
                    error={Boolean(errors["value"])}
                    helperText={errors["value"]?.message}
                  />
                )}
                rules={{
                  validate: validate,
                }}
              />
            </Grid>

          </Grid>
<Grid item xs={12}>

<Controller
            name={"default"}
            control={control}
            render={({ field }) => (
              <FormControlLabel
                control={
                  <Checkbox {...field} className="x-6" checked={field.value} />
                }

                label={
                  <Typography className="font-ebs">
                    <FormattedMessage id={"none"} defaultMessage={"Default"} />
                  </Typography>
                }
              />
            )}
          />
</Grid>

        </div>
        <DialogActions>
          <Button
            onClick={handleClose}
            startIcon={<Cancel />}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            type="submit"
            startIcon={<CheckCircle />}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Save"} />
          </Button>
        </DialogActions>
      </form>
    );
  }
);
// Type and required properties
ContactInfoFormAtom.propTypes = {
  defaultValues: PropTypes.shape({
    contactId: PropTypes.number.isRequired,
    value: PropTypes.string.isRequired,
    contactInfoType: PropTypes.shape({
      id: PropTypes.number.isRequired,
      value: PropTypes.string,
      description: PropTypes.string,
    }),
    id: PropTypes.number.isRequired,
    default: PropTypes.bool.isRequired,
  }),
  method: PropTypes.oneOf(["POST", "PUT"]),
  handleClose: PropTypes.func,
};
// Default properties
ContactInfoFormAtom.defaultProps = {
  defaultValues: {
    contactId: 0,
    value: "",
    contactInfoType: null,
    id: 0,
    value: "",
    default: false,
  },
  method: "POST",
};

export default ContactInfoFormAtom;
