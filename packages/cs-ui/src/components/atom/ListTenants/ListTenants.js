import React, { memo } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
const { Typography, List, ListItem } = Core;

//MAIN FUNCTION
/**
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const ListTenants = React.forwardRef(({ data }, ref) => {
  // Columns

  /** 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    /*  <div data-testid={"ProductsGridTestId"} ref={ref}>
      <EbsGrid toolbar={false} columns={columns} fetch={fetch} height="20vh" />
    </div>*/
    <div className=" bg-white">
      <List>
        <ListItem>
          <Typography className="font-ebs font-bold text-lg">
            {"TENANT / ORGANIZATION"}
          </Typography>
        </ListItem>
        {data.map((item, index) => (
          <ListItem key={index}>
            <Typography className="font-ebs font bold">
              {`${index + 1}. ${item.name} / ${item.organization.name}`}
            </Typography>
          </ListItem>
        ))}
      </List>
    </div>
  );
});

// Type and required properties
ListTenants.propTypes = {
  data: PropTypes.array.isRequired,
};
// Default properties
ListTenants.defaultProps = {};

export default memo(ListTenants);
