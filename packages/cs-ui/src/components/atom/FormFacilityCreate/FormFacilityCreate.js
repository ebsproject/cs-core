import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const {
  Button,
  DialogActions,
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
} = Core;
import { EbsForm } from "@ebs/components";
import { createFacility } from "store/modules/PlaceManager";
import { useDispatch } from "react-redux";
import { FIND_PLACES_LIST, FIND_FACILITY_LIST } from "utils/apollo/gql/placeManager";
import { clientCb } from "utils/apollo/apollo";
import { useNavigate } from "react-router-dom";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FormFacilityCreateAtom = React.forwardRef(({ type }, ref) => {
  const [open, setOpen] = React.useState(false);
  const [selectData, setSelectData] = React.useState(null);
  const [dataForm, setDataForm] = React.useState(null);
  const [statusCreate, setStatusCreate] = useState(null);
  const [dataList, setDataList] = useState(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const optionsType = [
    { value: 1, label: "Seed warehouse" },
    { value: 2, label: "Green House" },
    { value: 3, label: "Laboratory" },
    { value: 4, label: "building" },
    { value: 5, label: "room" },
    { value: 6, label: "farm house" },
    { value: 8, label: "shelf column" },
  ];
  const optionsContainer = [
    { value: 1, label: "crate" },
    { value: 2, label: "tray" },
    { value: 3, label: "compactus" },
    { value: 4, label: "megabin" },
  ];

  const optionTypeForm = [
    { value: 1, label: "Facility" },
    { value: 2, label: "Sub-Facility" },
    { value: 3, label: "Container" },
    { value: 4, label: "Nothing" },
  ];

  useEffect(() => {
    if (type !== "Facility") {
      let number = 1;
      let dataFetched = [];
      const fetchData = async () => {
        const { data } = await clientCb.query({
          query: FIND_FACILITY_LIST,
          variables: {
            page: { number: number, size: 1000 },
            // disjunctionFilters: true,
          },
        });
        number = data?.findFacilityList?.totalPages;
        dataFetched = data.findFacilityList?.content;
        if (number > 1) {
          for (let i = 1; i < number; i++) {
            const { data } = await clientCb.query({
              query: FIND_FACILITY_LIST,
              variables: {
                page: { number: i + 1, size: 1000 },
                // disjunctionFilters: true,
              },
            });
            dataFetched = [...dataFetched, ...data.findFacilityList.content];
          }
        }
        setDataList(dataFetched);
      };
      fetchData();
    } else {
      let number = 1;
      let dataFetched = [];
      const fetchData = async () => {
        const { data } = await clientCb.query({
          query: FIND_PLACES_LIST,
          variables: {
            page: { number: number, size: 1000 },
            filters: [{ col: "geospatialObjectType", mod: "EQ", val: "site" }],
            disjunctionFilters: true,
          },
        });
        number = data?.findGeospatialObjectList?.totalPages;
        dataFetched = data.findGeospatialObjectList.content;
        if (number > 1) {
          for (let i = 1; i < number; i++) {
            const { data } = await clientCb.query({
              query: FIND_PLACES_LIST,
              variables: {
                page: { number: i + 1, size: 1000 },
                filters: [
                  { col: "geospatialObjectType", mod: "EQ", val: "site" },
                ],
                disjunctionFilters: true,
              },
            });
            dataFetched = [
              ...dataFetched,
              ...data.findGeospatialObjectList.content,
            ];
          }
        }
        setDataList(dataFetched);
      };
      fetchData();
    }
  }, [type]);

  if (!dataList) return "Loading.....";

  const handleClose = () => {
    setOpen(false);
  };

  let optionFacility = dataList
    .filter(
      (item) =>
        item.facilityClass === "structure" && item.parentFacility === null
    )
    .map((item) => {
      return { label: item.facilityName, value: item.id };
    });

  const conditionalTypeForm = () => {
    switch (type) {
      case "Facility":
        let optionsSite = dataList?.map((item) => {
          return { label: item.geospatialObjectName, value: item.id };
        });
        const definitionCreateSite = ({ getValues, setValue, reset }) => {
          return {
            name: "createFacility",
            components: [
              {
                sizes: [7, 7, 7, 7, 7],
                component: "TextField",
                name: "name",
                inputProps: {
                  "data-testid": "name",
                  label: "Facility Name",
                  // variant: "outlined",
                },
                rules: {
                  required: "A Facility Name is required",
                },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "TextField",
                name: "code",
                inputProps: {
                  "data-testid": "FacilityCode",
                  label: "Facility Code",
                  // variant: "outlined",
                },
                rules: {
                  required: "A Facility Code is required",
                },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "type",
                options: optionsType,
                inputProps: {
                  "data-testid": "FacilityType",
                  label: "Facility Type",
                },
                helper: { title: "Choose Facility Type", placement: "bottom" },
                rules: {
                  required: "A Facility Type is required",
                },
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "site",
                label: "Choose Site",
                options: optionsSite,
                inputProps: {
                  "data-testid": "chooseCountryId",
                  label: "Choose Site",
                },
                helper: { title: "Choose Site", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "createAnotherPlace",
                label: "Create another place",
                options: optionTypeForm,
                inputProps: {
                  "data-testid": "createAnotherPlaceId",
                  label: "Create another place",
                },
                helper: { title: "Create another place", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: { label: "", value: 0 },
              },
              // {
              //   sizes: [12, 12, 12, 12, 12],
              //   component: "map",
              //   name: "coordinates",
              //   inputProps: {
              //     "data-testid": "field",
              //     label: "Choose Field",
              //     coordinates: {
              //       lat: 19.5307604699681,
              //       lng: -98.84719079491951,
              //     },
              //   },
              //   rules: {
              //     required: "A Field is required",
              //   },
              // },
            ],
          };
        };
        return definitionCreateSite;
      case "Sub Facility":
        const definitionSubFacilityField = ({ getValues, setValue, reset }) => {
          return {
            name: "createFacility",
            components: [
              {
                sizes: [7, 7, 7, 7, 7],
                component: "TextField",
                name: "name",
                inputProps: {
                  "data-testid": "name",
                  label: "Sub Facility Name",
                  // variant: "outlined",
                },
                rules: {
                  required: "A Sub Facility Name is required",
                },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "TextField",
                name: "code",
                inputProps: {
                  "data-testid": "code",
                  label: "Sub Facility Code",
                  // variant: "outlined",
                },
                rules: {
                  required: "A Sub Facility Code is required",
                },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "type",
                options: optionsType,
                inputProps: {
                  "data-testid": "type",
                  label: "Sub Facility Type",
                },
                helper: {
                  title: "Choose Sub Facility Type",
                  placement: "bottom",
                },
                rules: {
                  required: "A Sub Facility Type is required",
                },
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "facility",
                inputProps: {
                  "data-testid": "facility",
                  label: "Choose Facility",
                },
                rules: {
                  required: "A Facility is required",
                },
                helper: { title: "Choose Facility", placement: "bottom" },
                options: optionFacility,
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "createAnotherPlace",
                label: "Create another place",
                options: optionTypeForm,
                inputProps: {
                  "data-testid": "createAnotherPlaceId",
                  label: "Create another place",
                },
                helper: { title: "Create another place", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: { label: "", value: 0 },
              },
              // {
              //   sizes: [12, 12, 12, 12, 12],
              //   component: "map",
              //   name: "coordinates",
              //   inputProps: {
              //     "data-testid": "field",
              //     label: "Choose Field",
              //     coordinates: {
              //       lat: 19.5307604699681,
              //       lng: -98.84719079491951,
              //     },
              //   },
              //   rules: {
              //     required: "A Field is required",
              //   },
              // },
            ],
          };
        };
        return definitionSubFacilityField;
      case "Container":
        let filterFacility = dataList?.filter(
          (filter) =>
            filter?.parentFacility?.facilityName === selectData &&
            filter.facilityClass === "structure"
        );

        let optionsFields = filterFacility?.map((item) => {
          return { label: item.facilityName, value: item.id };
        });
        const definitionCreateContainer = ({ getValues, setValue, reset }) => {
          return {
            name: "createContainer",
            components: [
              {
                sizes: [7, 7, 7, 7, 7],
                component: "TextField",
                name: "name",
                inputProps: {
                  "data-testid": "name",
                  label: "Container Name",
                  // variant: "outlined",
                },
                rules: {
                  required: "A Container Name is required",
                },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "TextField",
                name: "code",
                inputProps: {
                  "data-testid": "code",
                  label: "Container Code",
                  // variant: "outlined",
                },
                rules: {
                  required: "A Container Code is required",
                },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "type",
                options: optionsContainer,
                inputProps: {
                  "data-testid": "type",
                  label: "Container Type",
                },
                helper: {
                  title: "Choose Container Type",
                  placement: "bottom",
                },
                rules: {
                  required: "A Container Type is required",
                },
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "facility",
                inputProps: {
                  "data-testid": "facility",
                  label: "Choose Facility",
                },
                onChange: (e) => setSelectData(e.target.textContent),
                helper: { title: "Choose Facility", placement: "bottom" },
                options: optionFacility,
                rules: {
                  required: "A Facility is required",
                },
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "subFacility",
                inputProps: {
                  "data-testid": "subFacility",
                  label: "Choose Sub Facility",
                },
                helper: { title: "Choose Sub Facility", placement: "bottom" },
                options: optionsFields,
                // rules: {
                //   required: "A Sub Facility is required",
                // },
                defaultValue: { label: "", value: 0 },
              },
              {
                sizes: [7, 7, 7, 7, 7],
                component: "Select",
                name: "createAnotherPlace",
                label: "Create another place",
                options: optionTypeForm,
                inputProps: {
                  "data-testid": "createAnotherPlaceId",
                  label: "Create another place",
                },
                helper: { title: "Create another place", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: { label: "", value: 0 },
              },
            ],
          };
        };
        return definitionCreateContainer;
      default:
        null;
    }
  };

  const onSubmit = (formData) => {
    setStatusCreate(formData?.createAnotherPlace?.label);
    setDataForm(formData);
    setOpen(true);
  };

  const handleProcess = async () => {
    switch (type) {
      case "Facility":
        let dataFacility = {
          id: 0,
          facilityName: dataForm.name,
          facilityCode: dataForm.code,
          facilityType: dataForm.type.label,
          geospatialObjectId: dataForm.site.value,
          facilityClass: "structure",
          // coordinates: dataForm.coordinates,
        };
        await createFacility(
          {
            data: dataFacility,
          },
          dispatch
        );
        break;
      case "Sub Facility":
        let subFacility = {
          id: 0,
          facilityName: dataForm.name,
          facilityCode: dataForm.code,
          facilityType: dataForm.type.label,
          parentFacilityId: dataForm.facility.value,
          facilityClass: "structure",
          // coordinates: dataForm.coordinates,
        };

        await createFacility(
          {
            data: subFacility,
          },
          dispatch
        );
        break;
      case "Container":
        if (dataForm.subFacility !== null || dataForm.subFacility.value === 0) {
          let container = {
            id: 0,
            facilityName: dataForm.name,
            facilityCode: dataForm.code,
            facilityType: dataForm.type.label,
            parentFacilityId: dataForm.facility.value,
            facilityClass: "container",
            // coordinates: dataForm.coordinates,
          };
          await createFacility(
            {
              data: container,
            },
            dispatch
          );
        } else {
          let container = {
            id: 0,
            facilityName: dataForm.name,
            facilityCode: dataForm.code,
            facilityType: dataForm.type.label,
            parentFacilityId: dataForm.subFacility.value,
            facilityClass: "container",
            // coordinates: dataForm.coordinates,
          };
          await createFacility(
            {
              data: container,
            },
            dispatch
          );
        }
        break;
      default:
        break;
    }
    setOpen(false);
  };

  return (
    <div>
      <EbsForm
        onSubmit={onSubmit}
        definition={conditionalTypeForm()}
        actionPlacement="top"
        buttonReset
      >
        <DialogActions>
          <Button
            onClick={() => navigate(`/cs/place-manager`)}
            color="primary"
          >
            Cancel
          </Button>
          <Button
            type="submit"
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id="none" defaultMessage="Create" />
          </Button>
        </DialogActions>
      </EbsForm>
      <Dialog open={open} maxWidth="xs">
        <DialogTitle>{"Create" + " " + type}</DialogTitle>
        <DialogContent className="font-black">
          <Typography className="text-ebs text-bold">
            <FormattedMessage
              id="none"
              defaultMessage={
                "You are about to create " +
                " " +
                dataForm?.name +
                " " +
                'If this is correct click "Create" below, and you will be able to access the Facility in the "Facility List" menu on the left of the page. '
              }
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={handleProcess}
            color="primary"
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
FormFacilityCreateAtom.propTypes = {};
// Default properties
FormFacilityCreateAtom.defaultProps = {};

export default FormFacilityCreateAtom;
