import FormFacilityCreate from './FormFacilityCreate';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FormFacilityCreate is in the DOM', () => {
  render(<FormFacilityCreate></FormFacilityCreate>)
  expect(screen.getByTestId('FormFacilityCreateTestId')).toBeInTheDocument();
})
