import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import FormSelect from './formselect'
// Test Library
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
// import '@testing-library/jest-dom/extend-expect'
// import { Provider } from 'react-redux';
// import { store } from '../../../store';
// import { IntlProvider } from "react-intl";
// let translation={
//   none:"test"
// }

// afterEach(cleanup);

// test('FormSelect is in the DOM', () => {
//   render(
//     <Provider store={store}>
//        <IntlProvider locale="es" messages={translation} defaultLocale="en">
//        <FormSelect></FormSelect>
//        </IntlProvider>
//     </Provider>
//   )
//   expect(screen.getByTestId('FormSelectTestId')).toBeInTheDocument();
// })

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FormSelect></FormSelect>, div)
})
// Props to send component to be rendered
// const props = {
//   properyName: 'Value',
// }
// test('Render correctly', () => {
//   const { getByTestId } = render(<FormSelect {...props}></FormSelect>)
//   expect(getByTestId('FormSelectTestId')).toBeInTheDocument()
// })
