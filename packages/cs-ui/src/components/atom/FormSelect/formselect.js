import React, { useState, useEffect } from "react";
import { useQuery } from "@apollo/client";
import { FIND_FORMS_LIST } from "utils/apollo/gql/sm";
import { clientSM } from "utils/apollo";
import { Controller } from "react-hook-form";
import PropTypes from "prop-types";
import { Core, Lab } from "@ebs/styleguide";
const { Autocomplete } = Lab;
const { FormControl, TextField, Typography } = Core;
import { FormattedMessage } from "react-intl";
import { useFormContext } from "react-hook-form";

import { findServiceByIdOrgUnit } from "store/modules/SM";
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const FormSelectMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { method, defaultValues, ...rest } = props;
  const {setValue, control, formState: { errors } } = useFormContext();

  const [itemValue, setItemValue] = useState(null);
  const [formSelected, setFormSelected] = useState(null);
  useEffect(() => {
    if (method === "PUT") {
      const serviceProv = async () => {
        const result = await findServiceByIdOrgUnit(
          defaultValues?.id
        );
        setFormSelected(result?.findServiceProvider?.ebsForms[0]);
      };
      serviceProv();
    }
  }, [method]);

  useEffect(() => {
    if (formSelected) {
      setValue("formsSelector", formSelected);
    }
  }, [formSelected]);

  const { loading, error, data } = useQuery(FIND_FORMS_LIST, {
    variables: {
      tenantId: "1",
    },
    client: clientSM,
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const result = data.findEbsFormList.content;

  return (
    <FormControl fullWidth>
      <Controller
        name={`formsSelector`}
        control={control}
        defaultValue={formSelected}
        render={({ field }) => (
          <Autocomplete
            {...field}
            options={result}
            getOptionLabel={(option) => option.name}
            renderInput={(params) => (
              <TextField
                {...params}
                placeholder="Choose a Form"
                error={Boolean(errors["formsSelector"])}
                label="Forms Available"
              />
            )}
            onChange={(event, newValue) => {
              setItemValue(newValue);
              setValue("formsSelector", newValue);
            }}
          />
        )}
        // rules={{
        //   required: (
        //     <Typography>
        //       <FormattedMessage
        //         id={"none"}
        //         defaultMessage={"Please, Select a Form"}
        //       />
        //     </Typography>
        //   ),
        // }}
      />
    </FormControl>
  );
});
// Type and required properties
FormSelectMolecule.propTypes = {};
// Default properties
FormSelectMolecule.defaultProps = {};

export default FormSelectMolecule;
