import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
// CORE COMPONENTS
import MaskedInput from "react-text-mask";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const PhoneInputAtom = React.forwardRef(({ inputRef, ...other }, ref) => {
  return (
    /**
     * @prop data-testid: Id to use inside PhoneInput.test.js file.
     */
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null);
      }}
      mask={[
        "(",
        /[1-9]/,
        /\d/,
        /\d/,
        ")",
        " ",
        /\d/,
        /\d/,
        /\d/,
        "-",
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ]}
      placeholderChar={"\u2000"}
      showMask
      keepCharPositions
    />
  );
});
// Type and required properties
PhoneInputAtom.propTypes = {
  inputRef: PropTypes.func.isRequired,
};
// Default properties
PhoneInputAtom.defaultProps = {};

export default PhoneInputAtom;
