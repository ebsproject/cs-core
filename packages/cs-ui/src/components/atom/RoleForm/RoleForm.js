import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import { useForm, Controller } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import CheckboxTree from "react-checkbox-tree";
import "react-checkbox-tree/lib/react-checkbox-tree.css";
import { mutationRole } from "store/modules/UserManagement";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons, Lab } from "@ebs/styleguide";
import { findRulesList } from "store/modules/Rules";
const { Autocomplete } = Lab;
const {
  Chip,
  Backdrop,
  Box,
  Button,
  Checkbox,
  CircularProgress,
  TextField,
  Typography,
  Grid
} = Core;
const { Cancel, CheckCircle, ExpandLess, ExpandMore, Folder } = Icons;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const RoleFormAtom = React.forwardRef(({ method, defaultValues }, ref) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { productFunctionList } = useSelector(({ persist }) => persist);
  const { rulesList } = useSelector(({ rules }) => rules);
  const { loading } = useSelector(({ um }) => um);
  const [process, setProcess] = useState(false);
  useEffect(() => {
    const fetchRulesList = async () => {
      await findRulesList()(dispatch);
    };
    fetchRulesList();
  }, []);

  useEffect(() => {
    if (process) {
      navigate("/cs/usermanagement", {state:{ index: 1 }});
    }
  }, [process]);
  const {
    control,
    getValues,
    setValue,
    formState: { errors },
    handleSubmit,
  } = useForm({
    defaultValues: (method === "PUT" && defaultValues) || {},
  });
  /**
   * @param {Object} data
   */
  const submit = (data) => {
    mutationRole({
      data,
      method,
      lastPFs: defaultValues?.productFunction?.checked,
      setProcess,
    })(dispatch);
  };
  /**
   * @param {Object[]} checked
   */
  const onCheck = (checked) => {
    setValue("productFunction", {
      checked: checked,
      expanded: getValues("productFunction")?.expanded,
    });
  };
  /**
   * @param {Object[]} expanded
   */
  const onExpand = (expanded) => {
    setValue("productFunction", {
      checked: getValues("productFunction")?.checked,
      expanded: expanded,
    });
  };
  const optionLabel = (option) => `${option.name}`;
  const ruleOption = (props, option, { selected }) => {
    const { key, ...restProps } = props
    return (
      <li key={key} {...restProps} >
        <Checkbox checked={selected} />
        {option.name}
      </li>
    );
  };
  const renderInputRule = (params, field) => {
    const newInputProps = new Object();
    if (!getValues(field)) {
      Object.assign(newInputProps, { value: "" });
    }
    return (
      <TextField
        {...params}
        inputProps={{
          ...params.inputProps,
          ...newInputProps,
        }}
        InputLabelProps={{ shrink: true }}
        focused={Boolean(errors[field])}
        error={Boolean(errors[field])}
        label={
          <Typography className="text-ebs">
            <FormattedMessage id={"none"} defaultMessage={"Rules"} />
            {` *`}
          </Typography>
        }
        helperText={errors[field]?.message}
      />
    );
  };
  const getRuleSelected = (option, value) => option.id === value.id;
  const handleCancel = () => {
    navigate("/cs/usermanagement", {state:{ index: 1 }});
  };
  return (
    /**
     * @prop data-testid: Id to use inside RoleForm.test.js file.
     */
    <>
      <form
        data-testid={"RoleFormTestId"}
        ref={ref}
        onSubmit={handleSubmit(submit)}
      >
        <Backdrop className="z-10 bg-gray-600" open={loading}>
          <CircularProgress color="inherit" />
        </Backdrop>
        <Typography className="flex-grow font-ebs text-ebs-green-900 font-bold">
          <FormattedMessage id="none" defaultMessage="Basic Information" />
        </Typography>
        <Grid container direction="row" justifyContent="flex-end" >

          <Button
            startIcon={<Cancel />}
            onClick={handleCancel}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            startIcon={<CheckCircle />}
            type={"submit"}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Save"} />
          </Button>
        </Grid>
        <Grid container direction="row" spacing={2} >
          <Controller
            name="id"
            control={control}
            render={({ field }) => (
              <TextField {...field} sx={{ display: "none" }} data-testid={"id"} />
            )}
          />
          <Grid item xs={6}>
            <Controller
              name="name"
              control={control}
              render={({ field }) => (
                <TextField
                 fullWidth
                  {...field}
                  error={!!errors["name"]}
                  helperText={errors["name"] ? errors["name"].message : ''}
                  label={<FormattedMessage id={"none"} defaultMessage={"Name"} />}
                  data-testid={"name"}
                />
              )}
              rules={{required:"This field is required"}}
            />
          </Grid>
          <Grid item xs={6}>
            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <TextField
                fullWidth
                  {...field}
                  error={!!errors["description"]}
                  helperText={errors["description"] ? errors["description"].message : ''}
                  label={
                    <FormattedMessage id={"none"} defaultMessage={"Description"} />
                  }
                  data-testid={"description"}
                />
              )}
              rules={{required:"This field is required"}}
            />

          </Grid>
          <Grid item xs={6}>
            <Controller
              name="rules"
              control={control}
              render={({ field }) => (
                <Autocomplete
                  {...field}
                  multiple
                  limitTags={6}
                  id={"rules"}
                  data-testid={"rules"}
                  options={rulesList || []}
                  value={field?.value || []}
                  renderTags={(tagValue, getTagProps) =>
                    tagValue.map((option, index) => (
                      <Chip
                        label={
                          rulesList?.filter((item) => item.id === option.id)[0].name
                        }
                        {...getTagProps({ index })}
                      />
                    ))
                  }
                  inputValue={getValues("rules")?.name || ""}
                  disableCloseOnSelect
                  onChange={(e, options) => {
                    setValue("rules", options);
                  }}
                  getOptionLabel={optionLabel}
                  renderOption={ruleOption}
                  renderInput={(params) => renderInputRule(params, "rules")}
                  isOptionEqualToValue={getRuleSelected}
                />
              )}
            />
          </Grid>
          <Grid item xs={12}>
            <Box className="col-span-2">
              <Typography className="flex-grow font-ebs text-ebs-green-900 font-bold">
                <FormattedMessage id={"none"} defaultMessage={"Permissions"} />
              </Typography>
              <Controller
                name="productFunction"
                control={control}
                render={({ field }) => (
                  <CheckboxTree
                    nodes={productFunctionList}
                    checked={field?.value?.checked}
                    expanded={field?.value?.expanded}
                    onCheck={onCheck}
                    onExpand={onExpand}
                    icons={{
                      check: <Checkbox checked />,
                      uncheck: <Checkbox checked={false} />,
                      halfCheck: <Checkbox checked={false} />,
                      expandClose: <ExpandLess />,
                      expandOpen: <ExpandMore />,
                      leaf: <Folder />,
                      parentClose: null,
                      parentOpen: null,
                    }}
                  />
                )}
              />
            </Box>
          </Grid>



          {/* <Controller
          name="securityGroup"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              label={
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Security Group"}
                />
              }
              data-testid={"securityGroup"}
            />
          )}
        /> */}

        </Grid>
      </form>
    </>
  );
});
// Type and required properties
RoleFormAtom.propTypes = {
  method: PropTypes.oneOf(["POST", "PUT"]),
  defaultValues: PropTypes.shape({}),
};
// Default properties
RoleFormAtom.defaultProps = {
  method: "POST",
};

export default RoleFormAtom;
