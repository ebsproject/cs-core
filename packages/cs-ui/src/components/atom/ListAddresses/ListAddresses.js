import React, { memo } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
const { Typography, List, ListItem } = Core;

//MAIN FUNCTION
/**
    @param { }: component properties.
    @param ref: reference made by React.forward.
*/
const ListAddresses = React.forwardRef(({ data }, ref) => {
  // Columns
  /** 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    /*  <div data-testid={"ProductsGridTestId"} ref={ref}>
      <EbsGrid toolbar={false} columns={columns} fetch={fetch} height="20vh" />
    </div>*/
    <div className=" bg-white">
      <List>
        <ListItem>
          <Typography className="font-ebs font-bold text-lg">
            {"ADDRESS / PURPOSE"}
          </Typography>
        </ListItem>
        {data.map((item, index) => {
          return(
            <ListItem key={index}>
              <Typography className="font-ebs font bold">
                {`${index + 1}. ${item.streetAddress} / ${item.purposes.map((item) => item.name)}`}
              </Typography>
            </ListItem>
          )  
        })}
      </List>
    </div>
  );
});

// Type and required properties
ListAddresses.propTypes = {
  data: PropTypes.array.isRequired,
};
// Default properties
ListAddresses.defaultProps = {};

export default memo(ListAddresses);
