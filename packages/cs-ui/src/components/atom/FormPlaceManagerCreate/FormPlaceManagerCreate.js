import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Button, DialogActions } = Core;
import { EbsForm } from "@ebs/components";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import AlertMessage from "./alertMessage";
import { getData, setDefinitionType } from "./functions";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FormPlaceManagerCreateAtom = React.forwardRef(
  ({ type, setIdSite, idSite, setType }, ref) => {
    const [open, setOpen] = React.useState(false);
    const [selectData, setSelectData] = React.useState(null);
    const [dataSite, setDataSite] = React.useState(null);
    const [typeName, setTypeName] = useState("");
    const navigate = useNavigate();
    const [dataList, setDataList] = useState([]);
    const [statusCreate, setStatusCreate] = useState(null);
    const [reloading, setReloading] = useState(false);
    const [total, setTotal] = useState(0);
    let siteFields = [];
    let percentage = 0;

    useEffect(() => {
      setDataList([]);
      const fetchData = async () => {
        await getData(setDataList, setReloading, setTotal);
      };
      fetchData();
    }, [type]);

    if (dataList.length === 0) return "Loading.....";

    percentage = Math.round((dataList.length / total) * 100, 2);

    let op = dataList
      .filter((item) => item.geospatialObjectType === "site")
      .map((item) => {
        return { label: item.geospatialObjectName, value: item.id };
      });
    siteFields = [...siteFields, ...op];

    const handleClose = () => {
      setOpen(false);
    };

    const conditionalTypeForm = () => {
      return setDefinitionType(
        dataList,
        type,
        setSelectData,
        setIdSite,
        siteFields,
        selectData,
        reloading,
        percentage
      );
    };

    const onSubmit = async (formData) => {
      let data = {
        id: 0,
        geospatialObjectCode: formData.code,
        geospatialObjectName: formData.name,
        geospatialObjectType: type.toLowerCase(),
        coordinates:
          formData.coordinates === null ? null : formData.coordinates,
      };
      setTypeName(formData.name);
      setStatusCreate(formData?.createAnotherPlace?.label);

      switch (type) {
        case "Site":
          data.geospatialObjectSubtype = formData.type.label;
          data.parentGeospatialObjectId = Number(formData.country.value);
          break;
        case "Field":
          data.parentGeospatialObjectId = Number(formData.site.value);
          break;
        case "Planting Area":
          data.parentGeospatialObjectId =
            Number(formData.field.value) === 0
              ? Number(formData.site.value)
              : Number(formData.field.value);
          break;
      }
      setDataSite(data);
      setOpen(true);
    };
    const handleNavigate = () => {
      navigate(`/cs/place-manager`);
    };
    return (
      <div>
        <AlertMessage
          setIdSite={setIdSite}
          handleNavigate={handleNavigate}
          statusCreate={statusCreate}
          open={open}
          handleClose={handleClose}
          setType={setType}
          type={type}
          typeName={typeName}
          selectData={selectData}
          data={dataSite}
        />
        <EbsForm
          onSubmit={onSubmit}
          definition={conditionalTypeForm()}
          actionPlacement="top"
          buttonReset
        >
          <DialogActions>
            <Button onClick={handleNavigate} color="primary">
              Cancel
            </Button>
            <Button
              type="submit"
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id="none" defaultMessage="Create" />
            </Button>
          </DialogActions>
        </EbsForm>
      </div>
    );
  }
);
// Type and required properties
FormPlaceManagerCreateAtom.propTypes = {};
// Default properties
FormPlaceManagerCreateAtom.defaultProps = {};

export default FormPlaceManagerCreateAtom;
