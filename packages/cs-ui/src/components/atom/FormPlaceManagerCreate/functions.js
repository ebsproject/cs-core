import { clientCb } from "utils/apollo/apollo";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";

export async function getData(setDataList, setReloading, setTotal) {
  setReloading(true);
  let number = 1;
  const { data } = await clientCb.query({
    query: FIND_PLACES_LIST,
    variables: {
      page: { number: number, size: 200 },
      filters: [
        { col: "geospatialObjectType", mod: "EQ", val: "field" },
        { col: "geospatialObjectType", mod: "EQ", val: "site" },
        { col: "geospatialObjectType", mod: "EQ", val: "country" },
      ],
      disjunctionFilters: true,
    },
    fetchPolicy: "no-cache",
  });
  number = data?.findGeospatialObjectList?.totalPages;
  setTotal(data?.findGeospatialObjectList?.totalElements);

  setDataList((d) => [...d, ...data.findGeospatialObjectList.content]);
  if (number > 1) {
    for (let i = 1; i < number; i++) {
      const { data } = await clientCb.query({
        query: FIND_PLACES_LIST,
        variables: {
          page: { number: i + 1, size: 200 },
          filters: [
            { col: "geospatialObjectType", mod: "EQ", val: "field" },
            { col: "geospatialObjectType", mod: "EQ", val: "site" },
            { col: "geospatialObjectType", mod: "EQ", val: "country" },
          ],
          disjunctionFilters: true,
        },
        fetchPolicy: "no-cache",
      });
      setDataList((d) => [...d, ...data.findGeospatialObjectList.content]);
    }
  }
  setReloading(false);
}

export const optionTypeForm = [
  { value: "Site", label: "Site" },
  { value: "Field", label: "Field" },
  { value: "Planting Area", label: "Planting Area" },
  { value: "Nothing", label: "Nothing" },
];
export const optionsType = [
  { value: 1, label: "Seed warehouse" },
  { value: 2, label: "Green House" },
  { value: 3, label: "Laboratory" },
  { value: 4, label: "breeding location" },
  { value: 5, label: "national agricultural research center" },
  { value: 6, label: "met location" },
  { value: 7, label: "international agricultural research center" },
  { value: 8, label: "third sub-national division" },
];

export function setDefinitionType(
  dataList,
  type,
  setSelectData,
  setIdSite,
  optionSiteField,
  selectData,
  reloading,
  percentage
) {
  let optionsCountry = dataList
    ?.filter((item) => item.geospatialObjectType === "country")
    ?.map((item) => {
      return { label: item.geospatialObjectName, value: item.id };
    });
  switch (type) {
    case "Field":
      const definitionCreateField = ({ getValues, setValue, reset }) => {
        return {
          name: "createField",
          components: [
            {
              sizes: [7, 7, 7, 7, 7],
              component: "TextField",
              name: "name",
              inputProps: {
                "data-testid": "name",
                label: "Field Name",
                // variant: "outlined",
              },
              rules: {
                required: "A Field Name is required",
              },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "TextField",
              name: "code",
              inputProps: {
                "data-testid": "code",
                label: "Field Code",
                // variant: "outlined",
              },
              rules: {
                required: "A Field Code is required",
              },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "Select",
              name: "site",
              inputProps: {
                "data-testid": "site",
                label: reloading
                  ? `Loading sites, ${percentage}% completed`
                  : "Choose Site",
              },
              onChange: (e, option) => {
                setSelectData(e.target.textContent),
                  setIdSite(Number(option.value));
              },
              rules: {
                required: "A Site is required",
              },
              helper: { title: "Choose Site", placement: "bottom" },
              options: optionSiteField,
              defaultValue: { value: 0, label: "" },
            },
            {
              sizes: [12, 12, 12, 12, 12],
              component: "map",
              name: "coordinates",
              inputProps: {
                "data-testid": "field",
                label: "Choose Field",
              },
              rules: {
                required: "A Field is required",
              },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "Select",
              name: "createAnotherPlace",
              label: "Create another place",
              options: optionTypeForm,
              inputProps: {
                "data-testid": "createAnotherPlaceId",
                label: "Create another place",
              },
              helper: {
                title: "Create another place",
                placement: "bottom",
              },
              rules: {
                required: "It's required",
              },
              defaultValue: optionTypeForm[3],
            },
          ],
        };
      };
      return definitionCreateField;
    case "Planting Area":
      let optionSite = dataList
        .filter((item) => item.geospatialObjectType === "site")
        .map((item) => {
          return { label: item.geospatialObjectName, value: item.id };
        });

      let filterFields = dataList
        .filter((item) => item.geospatialObjectType === "field")
        .filter(
          (item) =>
            item?.parentGeospatialObject?.geospatialObjectName === selectData
        );
      let optionsFields = filterFields?.map((field) => {
        return { label: field.geospatialObjectName, value: field.id };
      });
      const definitionCreatePlantingArea = ({ getValues, setValue, reset }) => {
        return {
          name: "createPlating",
          components: [
            {
              sizes: [7, 7, 7, 7, 7],
              component: "TextField",
              name: "name",
              inputProps: {
                "data-testid": "name",
                label: "Planting Area Name",
                // variant: "outlined",
              },
              rules: {
                required: "A Name is required",
              },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "TextField",
              name: "code",
              inputProps: {
                "data-testid": "code",
                label: "Planting Area Code",
                // variant: "outlined",
              },
              rules: {
                required: "A Code is required",
              },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "Select",
              name: "site",
              inputProps: {
                "data-testid": "site",
                label: reloading
                  ? `Loading sites, ${percentage}% completed`
                  : "Choose Site",
              },
              onChange: (e, option) => {
                setSelectData(e.target.textContent);
                setIdSite(Number(option.value));
              },
              helper: { title: "Choose Site", placement: "bottom" },
              options: optionSite,
              rules: {
                required: "A Site Type is required",
              },
              defaultValue: { value: 0, label: "" },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "Select",
              name: "field",
              inputProps: {
                "data-testid": "field",
                label: "Choose Field",
              },
              helper: { title: "Choose Field", placement: "bottom" },
              options: optionsFields,
              // rules: {
              //   required: "A Field is required",
              // },
              defaultValue: { value: 0, label: "" },
            },
            {
              sizes: [12, 12, 12, 12, 12],
              component: "map",
              name: "coordinates",
              inputProps: {
                "data-testid": "planting",
                label: "Choose Planting",
              },
              rules: {
                required: "A Planting is required",
              },
            },
            {
              sizes: [7, 7, 7, 7, 7],
              component: "Select",
              name: "createAnotherPlace",
              label: "Create another place",
              options: optionTypeForm,
              inputProps: {
                "data-testid": "createAnotherPlaceId",
                label: "Create another place",
              },
              helper: {
                title: "Create another place",
                placement: "bottom",
              },
              rules: {
                required: "It's required",
              },
              defaultValue: optionTypeForm[3],
            },
          ],
        };
      };
      return definitionCreatePlantingArea;
    default:
      const definitionDefault = ({ getValues, setValue, reset }) => {
        return {
          name: "createSite",
          components: [
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "name",
              inputProps: {
                "data-testid": "name",
                label: "Site Name",
                // variant: "outlined",
              },
              rules: {
                required: "A Site Name is required",
              },
              defaultValue: "",
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "code",
              inputProps: {
                "data-testid": "siteCode",
                label: "Site Code",
                // variant: "outlined",
              },
              rules: {
                required: "A Site Code is required",
              },
              defaultValue: "",
            },

            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "type",
              options: optionsType,
              inputProps: {
                "data-testid": "Type",
                label: "Choose Type",
              },
              helper: { title: "ChoosE Type", placement: "bottom" },
              rules: {
                required: "A type is required",
              },
              defaultValue: { value: 0, label: "" },
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "country",
              label: "Choose Country",
              options: optionsCountry,
              inputProps: {
                "data-testid": "chooseCountryId",
                label: "Choose Country",
              },
              helper: { title: "Choose Country", placement: "bottom" },
              rules: {
                required: "It's required",
              },
              defaultValue: { value: 0, label: "" },
            },
            {
              sizes: [12, 12, 12, 12, 12],
              component: "map",
              name: "coordinates",
              inputProps: {
                "data-testid": "field",
                label: "Choose Field",
              },
              rules: {
                required: "A Field is required",
              },
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "createAnotherPlace",
              label: "Create another place",
              options: optionTypeForm,
              inputProps: {
                "data-testid": "createAnotherPlaceId",
                label: "Create another place",
              },
              helper: {
                title: "Create another place",
                placement: "bottom",
              },
              rules: {
                required: "It's required",
              },
              defaultValue: optionTypeForm[3],
            },
          ],
        };
      };
      return definitionDefault;
  }
}
