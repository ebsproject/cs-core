import { Core } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { createGeospatialObject } from "store/modules/PlaceManager";
const {
  Button,
  DialogActions,
  Dialog,
  DialogContent,
  DialogTitle,
  Typography,
} = Core;
const AlertMessage = ({
  handleNavigate,
  type,
  open,
  handleClose,
  typeName,
  selectData,
  data,
  setType,
  setIdSite,
  statusCreate,
}) => {
  const dispatch = useDispatch();
  const TextFormattedMessage = () => {
    switch (type) {
      case "Site":
        return (
          <div>
            You are about to create <strong>{typeName}</strong>. If this is
            correct click "Create" below, and you will be able to access the
            Site in the "Places List" menu on the left of the page.
          </div>
        );
      case "Field":
        return (
          <div>
            You are about to create <strong>{typeName}</strong> within{" "}
            <strong>{selectData}</strong>. If this is correct click "Create"
            below, and you will be able to access the Field in the "Places List"
            menu on the left of the page.
          </div>
        );
      case "Planting Area":
        return (
          <div>
            You are about to create <strong>{typeName}</strong> within{" "}
            <strong>{selectData}</strong>. If this is correct click "Create"
            below, and you will be able to access the Planting Area in the
            "Places List" menu on the left of the page.
          </div>
        );
      default:
        null;
    }
  };
  const handleProcess = async () => {
    await createGeospatialObject({ data, setIdSite }, dispatch);
    handleClose();
    if (statusCreate === "Nothing") handleNavigate();
    else setType(statusCreate);
  };
  return (
    <>
      <Dialog open={open} maxWidth="xs">
        <DialogTitle>{"Create" + " " + type}</DialogTitle>
        <DialogContent className="font-black">
          <Typography className="text-ebs text-bold">
            <TextFormattedMessage />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button
            onClick={handleProcess}
            color="primary"
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            Create
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
export default AlertMessage;
