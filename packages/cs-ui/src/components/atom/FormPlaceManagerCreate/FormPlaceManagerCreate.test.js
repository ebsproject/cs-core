import FormPlaceManagerCreate from './FormPlaceManagerCreate';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FormPlaceManagerCreate is in the DOM', () => {
  render(<FormPlaceManagerCreate></FormPlaceManagerCreate>)
  expect(screen.getByTestId('FormPlaceManagerCreateTestId')).toBeInTheDocument();
})
