import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EbsDialog } from "@ebs/styleguide";
import { useDispatch, useSelector } from "react-redux";
import { deleteContactInfo } from "store/modules/CRM";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import {Core,Icons} from "@ebs/styleguide";
// CORE COMPONENTS
const {
  Box,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  Tooltip,
  Typography,
} = Core;
const {Delete} = Icons;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DeleteContactInfoAtom = React.forwardRef(
  ({ refresh, contactInfoId }, ref) => {
    const [open, setOpen] = useState(false);
    const { contactInfoSuccess } = useSelector(({ crm }) => crm);
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    useEffect(() => {
      if (contactInfoSuccess) {
        handleClose();
        refresh();
      }
    }, [contactInfoSuccess]);
    const submit = () => {
      deleteContactInfo(contactInfoId)(dispatch);
    };
    return (
      /**
       * @prop data-testid: Id to use inside DeleteContactInfo.test.js file.
       */
      <div data-testid={"DeleteContactInfoTestId"} ref={ref}>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id="none" defaultMessage="Delete" />
            </Typography>
          }
          className="border-red-700"
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-contact"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            <FormattedMessage
              id="none"
              defaultMessage="Delete Additional Information"
            />
          }
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this information permanently?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Submit" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
DeleteContactInfoAtom.propTypes = {
  refresh: PropTypes.func.isRequired,
  contactInfoId: PropTypes.number,
};
// Default properties
DeleteContactInfoAtom.defaultProps = {};

export default DeleteContactInfoAtom;
