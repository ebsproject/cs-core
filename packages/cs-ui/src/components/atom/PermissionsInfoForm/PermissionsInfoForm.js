import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons, Lab } from "@ebs/styleguide";
const { Box, Button, DialogActions, TextField, Typography } = Core;
const { Cancel, CheckCircle } = Icons;
const { Autocomplete } = Lab;
import { mutationTeamInfo } from "store/modules/TeamInfo";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const PermissionsInfoFormAtom = React.forwardRef(
  ({ method, defaultValues, handleClose, contactId, userName }, ref) => {
    const { hierarchyTreeList } = useSelector(({ team }) => team);
    const { roleList } = useSelector(({ persist }) => persist);

    const dispatch = useDispatch();
    const {
      control,
      formState: { errors },
      setValue,
      getValues,
    } = useForm({
      defaultValues:
        method === "POST"
          ? {
              contactId: contactId,
              id: 0,
              roles: null,
              team: null,
            }
          : defaultValues,
    });
    /**
     * @param {Object} e
     * @param {Object} options
     */
    const onChangeH = (e, options) => {
      e.preventDefault();
      setValue("team", options);
    };
    /**
     * @param {Object} option
     */
    const getOptionLabelH = (option) => `${option.value}`;
    /**
     * @param {Object} params
     * @returns {Node}
     */
    const renderInputRole = (params) => {
      return (
        <TextField
          {...params}
          required
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors["roles"])}
          error={Boolean(errors["roles"])}
          label={
            <Typography className="font-ebs font-semibold text-xl">
              <FormattedMessage id={"none"} defaultMessage={"Roles"} />
              {` *`}
            </Typography>
          }
          helperText={errors["roles"]?.message}
        />
      );
    };
    /**
     * @param {Object} params
     */
    const renderTeamInput = (params) => (
      <TextField
        {...params}
        required
        InputLabelProps={{ shrink: true }}
        focused={Boolean(errors["team"])}
        error={Boolean(errors["team"])}
        label={
          <Typography className="font-ebs font-semibold text-xl">
            <FormattedMessage id={"none"} defaultMessage={"Team"} />
            {` *`}
          </Typography>
        }
        helperText={errors["team"]?.message}
      />
    );

    /**
     * @param {Object} data
     */
    const submit = () => {
      const data = {
        contactId: contactId,
        id: 0,
        roles: getValues("roles"),
        team: getValues("team"),
      };
      mutationTeamInfo({ method, data, userName })(dispatch);
    };
    const onChangeR = (e, options) => {
      e.preventDefault();
      setValue("roles", options);
    };
    const optionLabel = (option) => `${option.name}`;
    return (
      /**
       * @prop data-testid: Id to use inside ContactInfoForm.test.js file.
       */ <div>
        <form name="PermissionsInfoForm" className="col-span-2">
          <Box
            component={"div"}
            data-testid={"PermissionsInfoFormTestId"}
            ref={ref}
          >
            <div>
              <Controller
                name={`team`}
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    id={"team"}
                    options={hierarchyTreeList.content || []}
                    className="flex-none w-52 m-5"
                    onChange={onChangeH}
                    getOptionLabel={getOptionLabelH}
                    renderInput={renderTeamInput}
                  />
                )}
              />
              <Controller
                name="roles"
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    id={"roles"}
                    data-testid={"roles"}
                    options={roleList || []}
                    className="flex-none w-52 m-5"
                    onChange={onChangeR}
                    getOptionLabel={optionLabel}
                    renderInput={renderInputRole}
                  />
                )}
              />
            </div>
          </Box>
          <DialogActions>
            <Button
              onClick={handleClose}
              startIcon={<Cancel />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
            </Button>
            <Button
              onClick={submit}
              startIcon={<CheckCircle />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Save"} />
            </Button>
          </DialogActions>
        </form>
      </div>
    );
  }
);
// Type and required properties
PermissionsInfoFormAtom.propTypes = {
  method: PropTypes.oneOf(["POST", "PUT"]),
  handleClose: PropTypes.func,
};
// Default properties
PermissionsInfoFormAtom.defaultProps = {
  defaultValues: {
    contactId: 0,
    roles: null,
    id: 0,
    team: null,
  },
  method: "POST",
};

export default PermissionsInfoFormAtom;
