import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EbsDialog } from "@ebs/styleguide";
import { useDispatch, useSelector,useStore } from "react-redux";
import { deleteTreeInfo } from "store/modules/TeamInfo";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  IconButton,
  Tooltip,
  Typography,
  DialogTitle
} = Core
const {Delete} = Icons;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DeleteHierarchyTreeInfoAtom = React.forwardRef(
  ({ rowData,refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const { deleteSuccess } = useSelector(({ team }) => team);
    const { getState } = useStore();
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    useEffect(() => {
      if (deleteSuccess) {
        handleClose();
        refresh();
      }
    }, [deleteSuccess]);
    const submit = () => {
     deleteTreeInfo(rowData)(dispatch,getState);
    };
    return (
      /**
       * @prop data-testid: Id to use inside DeleteContactInfo.test.js file.
       */
      <Box data-testid={"DeleteContactInfoTestId"} ref={ref}>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id="none" defaultMessage="Delete" />
            </Typography>
          }
          className="border-red-700"
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-contact"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            <FormattedMessage
              id="none"
              defaultMessage="Delete Team Information"
            />
          }
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage= {"Do you want to delete this information permanently?"}
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Submit" />
            </Button>
          </DialogActions>
        </EbsDialog>

      </Box>
    );
  }
);
// Type and required properties
DeleteHierarchyTreeInfoAtom.propTypes = {
  refresh: PropTypes.func.isRequired,
  //contactInfoId: PropTypes.number.isRequired,
};
// Default properties
DeleteHierarchyTreeInfoAtom.defaultProps = {};

export default DeleteHierarchyTreeInfoAtom;
