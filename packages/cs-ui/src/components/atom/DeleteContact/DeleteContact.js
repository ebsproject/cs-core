import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { deleteContact } from "store/modules/CRM";
import { useDispatch, useSelector, useStore } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { EbsDialog,Core,Icons } from "@ebs/styleguide";
const {
  DialogContent,
  Typography,
  DialogActions,
  Button,
  IconButton,
  Tooltip,
  Box,
} = Core
const {Delete} = Icons;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DeleteContactAtom = React.forwardRef(
  ({ refresh, contactId, type }, ref) => {
    const [open, setOpen] = useState(false);
    const { success } = useSelector(({ crm }) => crm);
    const { getState } = useStore();
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);
    const submit = () => {
      deleteContact(contactId)(dispatch, getState);
      refresh()
    };
    return (
      /**
       * @prop data-testid: Id to use inside DeleteContact.test.js file.
       */
      <div data-testid={"DeleteContactTestId"} ref={ref}>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-xl">
              {type === "PERSON" ? (
                <FormattedMessage id="none" defaultMessage="Delete Person" />
              ) : (
                <FormattedMessage
                  id="none"
                  defaultMessage="Delete"
                />
              )}
            </Typography>
          }
          className="border-red-700"
        >
          <IconButton
          size="small"
            onClick={handleClickOpen}
            aria-label="delete-contact"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          maxWidth="sm"
          title={
            type === "PERSON" ? (
              <FormattedMessage id="none" defaultMessage="Delete Person" />
            ) : (
              <FormattedMessage id="none" defaultMessage="Delete Register" />
            )
          }
        >
          <DialogContent dividers>
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to delete this contact permanently?"
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>
            <Button onClick={submit}>
              <FormattedMessage id="none" defaultMessage="Submit" />
            </Button>
          </DialogActions>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
DeleteContactAtom.propTypes = {
  refresh: PropTypes.func.isRequired,
  contactId: PropTypes.number.isRequired,
  type: PropTypes.oneOf(["PERSON", "INSTITUTION"]),
};
// Default properties
DeleteContactAtom.defaultProps = {
  type: "PERSON",
};

export default DeleteContactAtom;
