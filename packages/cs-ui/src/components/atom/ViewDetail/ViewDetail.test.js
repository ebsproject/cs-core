import ViewDetail from './ViewDetail';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('ViewDetail is in the DOM', () => {
  render(<ViewDetail></ViewDetail>)
  expect(screen.getByTestId('ViewDetailTestId')).toBeInTheDocument();
})
