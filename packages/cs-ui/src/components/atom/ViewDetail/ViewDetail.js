import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Button, DialogActions, Typography } = Core;
import { useDispatch, useSelector } from "react-redux";
import { EbsForm } from "@ebs/components";
import { clientCb } from "utils/apollo/apollo";
import { useQuery } from "@apollo/client";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ViewDetailAtom = React.forwardRef(({ data, type, dataType }, ref) => {
  //Data Details
  const {
    loading,
    data: dataList,
    error,
  } = useQuery(FIND_PLACES_LIST, {
    variables: {
      page: { number: 1, size: 10 },
      filters: [{ col: "id", mod: "EQ", val: data?.id }],
    },
    fetchPolicy: "no-cache",
    client: clientCb,
  });

  if (dataList === undefined) return "Loading...";
  const dataDetail = dataList?.findGeospatialObjectList?.content[0];
  const definitionCreateSite = ({ getValues, setValue, reset }) => {
    return {
      name: "Details",
      components: [
        {
          sizes: [12, 12, 12, 12, 12],
          component: "map",
          name: "coordinates",
          inputProps: {
            "data-testid": "field",
            label: "Choose Field",
            coordinates: {
              lat:
                dataDetail?.coordinates === null ||
                dataDetail?.coordinates === undefined
                  ? 19.5307604699681
                  : dataDetail?.coordinates?.features?.[0]?.geometry?.type ===
                      "Point"
                    ? dataDetail?.coordinates?.features?.[0]?.geometry
                        ?.coordinates[1]
                    : dataDetail?.coordinates?.features?.[0]?.geometry
                        ?.coordinates[0][0][1],
              lng:
                dataDetail?.coordinates === null ||
                dataDetail?.coordinates === undefined
                  ? -98.84719079491951
                  : dataDetail?.coordinates?.features?.[0]?.geometry?.type ===
                      "Point"
                    ? dataDetail?.coordinates?.features?.[0]?.geometry
                        ?.coordinates[0]
                    : dataDetail?.coordinates?.features?.[0]?.geometry
                        ?.coordinates[0][0][0],
            },
            defaultValue: dataDetail && dataDetail?.coordinates,
            type: "view",
          },
          rules: {
            required: "A Field is required",
          },
        },
      ],
    };
  };
  const onSubmit = (formData) => {};
  return dataType === "site" ? (
    /* 
         @prop data-testid: Id to use inside ViewDetail.test.js file.
         */
    <div>
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Site Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Code Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectCode}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Country" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.parentGeospatialObject.geospatialObjectName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Site Type"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectSubtype}
          </Typography>
        </div>
        <div className="col-span-2">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage
              id="none"
              defaultMessage="Geospatial Coordinates"
            />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.coordinates?.features?.[0]?.geometry.type === "Polygon"
              ? dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()
              : dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()}
          </Typography>
        </div>
      </div>
      <br />
      <EbsForm
        onSubmit={onSubmit}
        definition={definitionCreateSite}
        actionPlacement="top"
      ></EbsForm>
    </div>
  ) : dataType === "field" ? (
    <div>
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Field Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Field Code"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectCode}
          </Typography>
        </div>
        <div className="col-span-2">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Site" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.parentGeospatialObject.geospatialObjectName}
          </Typography>
        </div>
        <div className="col-span-2">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage
              id="none"
              defaultMessage="Geospatial Coordinates"
            />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.coordinates?.features?.[0]?.geometry.type === "Polygon"
              ? dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()
              : dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()}
          </Typography>
        </div>
      </div>
      <br />
      <EbsForm
        onSubmit={onSubmit}
        definition={definitionCreateSite}
        actionPlacement="top"
      ></EbsForm>
    </div>
  ) : (
    <div>
      <div className="grid grid-cols-2 gap-4">
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Planting Area Name"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage={"Planting Area Code"} />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.geospatialObjectCode}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Site" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.parentGeospatialObject?.parentGeospatialObject
              ?.geospatialObjectType === "country"
              ? dataDetail?.parentGeospatialObject?.geospatialObjectName
              : dataDetail?.parentGeospatialObject?.parentGeospatialObject
                  ?.geospatialObjectName}
          </Typography>
        </div>
        <div className="">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage id="none" defaultMessage="Field" />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.parentGeospatialObject?.parentGeospatialObject
              ?.geospatialObjectType === "country"
              ? ""
              : dataDetail?.parentGeospatialObject?.geospatialObjectName}
          </Typography>
        </div>
        <div className="col-span-2">
          <Typography variant="h6" display="block" gutterBottom>
            <FormattedMessage
              id="none"
              defaultMessage="Geospatial Coordinates"
            />
          </Typography>
          <Typography variant="body1" display="block" gutterBottom>
            {dataDetail?.coordinates?.features?.[0]?.geometry.type === "Polygon"
              ? dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()
              : dataDetail?.coordinates?.features?.[0]?.geometry?.coordinates?.toString()}
          </Typography>
        </div>
      </div>
      <br />
      <EbsForm
        onSubmit={onSubmit}
        definition={definitionCreateSite}
        actionPlacement="top"
      ></EbsForm>
    </div>
  );
});
// Type and required properties
ViewDetailAtom.propTypes = {};
// Default properties
ViewDetailAtom.defaultProps = {};

export default ViewDetailAtom;
