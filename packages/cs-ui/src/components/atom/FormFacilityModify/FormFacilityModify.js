import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Button, DialogActions } = Core;
import { EbsForm } from "@ebs/components";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { modifyFacility } from "store/modules/PlaceManager";
import { useQuery } from "@apollo/client";
import {
  FIND_PLACES_LIST,
  FIND_FACILITY_LIST,
} from "utils/apollo/gql/placeManager";
import { clientCb } from "utils/apollo/apollo";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FormFacilityModifyAtom = React.forwardRef(({ data, type }, ref) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const optionsType = [
    { value: 1, label: "Seed warehouse" },
    { value: 2, label: "Green House" },
    { value: 3, label: "Laboratory" },
    { value: 4, label: "building" },
    { value: 5, label: "room" },
    { value: 6, label: "farm house" },
    { value: 8, label: "shelf column" },
  ];
  const optionsContainer = [
    { value: 1, label: "crate" },
    { value: 2, label: "tray" },
    { value: 3, label: "compactus" },
    { value: 4, label: "megabin" },
  ];
  //Data DETAILS
  const {
    loading,
    data: dataList,
    error,
  } = useQuery(FIND_FACILITY_LIST, {
    variables: {
      page: { number: 1, size: 10 },
      filters: [{ col: "id", mod: "EQ", val: data?.id }],
    },
    fetchPolicy: "no-cache",
    client: clientCb,
  });
  //Site List
  const {
    loading: loadingList,
    data: sites,
    error: errorList,
  } = useQuery(FIND_PLACES_LIST, {
    variables: {
      page: { number: 1, size: 50000 },
      filters: [{ col: "geospatialObjectType", mod: "EQ", val: "site" }],
    },
    fetchPolicy: "no-cache",
    client: clientCb,
  });
  //Field List
  const {
    loading: loadingField,
    data: fields,
    error: errorFieldList,
  } = useQuery(FIND_FACILITY_LIST, {
    variables: {
      page: { number: 1, size: 50000 },
      filters: [
        {
          col: "parentFacility",
          mod: "NULL",
          val: "",
        },
      ],
    },
    fetchPolicy: "no-cache",
    client: clientCb,
  });

  if (loading || loadingList || loadingField) return "Loading...";
  const dataDetails = dataList?.findFacilityList?.content?.[0];
  if (dataDetails === undefined) {
    navigate(`/cs/place-manager`);
  }
  const siteList = sites?.findGeospatialObjectList?.content;
  const fieldList = fields?.findFacilityList?.content;
  let fieldOptions = fieldList?.map((item) => {
    return { label: item.facilityName, value: item.id };
  });
  let siteOptionslist = siteList?.map((item) => {
    return { label: item.geospatialObjectName, value: item.id };
  });
  const definitionModifyFacility = ({ getValues, setValue, reset }) => {
    if (dataDetails?.facilityClass === "structure") {
      if (dataDetails?.parentFacility === null) {
        let defaultValueType = optionsType?.filter(
          (item) => item.label === dataDetails?.facilityType
        );
        let defaultValueSite = siteOptionslist?.filter(
          (item) =>
            item.label === dataDetails?.geospatialObject?.geospatialObjectName
        );
        return {
          name: "Facility",
          components: [
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "name",
              inputProps: {
                "data-testid": "name",
                label: "Facility Name",
                disabled: type === "VIEW" ? true : false,
                // variant: "outlined",
              },
              defaultValue: dataDetails && dataDetails.facilityName,
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "code",
              inputProps: {
                "data-testid": "facilityCode",
                label: "Facility Code",
                disabled: type === "VIEW" ? true : false,
                // variant: "outlined",
              },
              defaultValue: dataDetails && dataDetails.facilityCode,
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "type",
              label: "Type",
              options: optionsType,
              inputProps: {
                "data-testid": "typeyId",
                label: "Type",
                disabled: type === "VIEW" ? true : false,
              },
              helper: { title: "Type", placement: "bottom" },
              rules: {
                required: "It's required",
              },
              defaultValue: defaultValueType && defaultValueType[0],
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "siteName",
              label: "Site",
              options: siteOptionslist,
              inputProps: {
                "data-testid": "typeyId",
                label: "Site",
                disabled: type === "VIEW" ? true : false,
              },
              helper: { title: "Site", placement: "bottom" },
              rules: {
                required: "It's required",
              },
              defaultValue: defaultValueSite && defaultValueSite[0],
            },
            // {
            //   sizes: [12, 12, 12, 12, 12],
            //   component: "map",
            //   name: "coordinates",
            //   inputProps: {
            //     "data-testid": "field",
            //     label: "Choose Field",
            //     coordinates: {
            //       lat: 19.5307604699681,
            //       lng: -98.84719079491951,
            //     },
            //     // defaultValue: data && data?.coordinates,
            //   },
            //   rules: {
            //     required: "A Field is required",
            //   },
            // },
          ],
        };
      }
      let defaultValueTypeS = optionsType?.filter(
        (item) => item.label === dataDetails?.facilityType
      );
      let defaultValueFacility = fieldOptions?.filter(
        (item) => item.label === dataDetails?.parentFacility.facilityName
      );
      return {
        name: "SubFacility",
        components: [
          {
            sizes: [6, 6, 6, 6, 6],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Sub-Facility Name",
              disabled: type === "VIEW" ? true : false,
              // variant: "outlined",
            },
            defaultValue: dataDetails && dataDetails.facilityName,
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "TextField",
            name: "code",
            inputProps: {
              "data-testid": "facilityCode",
              label: "Sub-Facility Code",
              disabled: type === "VIEW" ? true : false,
              // variant: "outlined",
            },
            defaultValue: dataDetails && dataDetails.facilityCode,
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "Select",
            name: "type",
            label: "Type",
            options: optionsType,
            inputProps: {
              "data-testid": "typeyId",
              label: "Type",
              disabled: type === "VIEW" ? true : false,
            },
            helper: { title: "Type", placement: "bottom" },
            rules: {
              required: "It's required",
            },
            defaultValue: defaultValueTypeS && defaultValueTypeS[0],
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "Select",
            name: "FieldName",
            label: "Field",
            options: fieldOptions,
            inputProps: {
              "data-testid": "typeyId",
              label: "Field",
              disabled: type === "VIEW" ? true : false,
            },
            helper: { title: "Site", placement: "bottom" },
            rules: {
              required: "It's required",
            },
            defaultValue: defaultValueFacility && defaultValueFacility[0],
          },
          // {
          //   sizes: [12, 12, 12, 12, 12],
          //   component: "map",
          //   name: "coordinates",
          //   inputProps: {
          //     "data-testid": "field",
          //     label: "Choose Field",
          //     coordinates: {
          //       lat: 19.5307604699681,
          //       lng: -98.84719079491951,
          //     },
          //     defaultValue: dataDetails && dataDetails?.coordinates,
          //   },
          //   rules: {
          //     required: "A Field is required",
          //   },
          // },
        ],
      };
    }
    if (dataDetails?.facilityClass === "container") {
      let defaultValueTypeC = optionsContainer?.filter(
        (item) => item.label === dataDetails?.facilityType
      );
      let defaultValueFacility = fieldOptions?.filter(
        (item) => item.label === dataDetails?.parentFacility.facilityName
      );
      return {
        name: "Container",
        components: [
          {
            sizes: [6, 6, 6, 6, 6],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Container Name",
              disabled: type === "VIEW" ? true : false,
              // variant: "outlined",
            },
            defaultValue: dataDetails && dataDetails.facilityName,
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "TextField",
            name: "code",
            inputProps: {
              "data-testid": "facilityCode",
              label: "Container Code",
              disabled: type === "VIEW" ? true : false,
              // variant: "outlined",
            },
            defaultValue: dataDetails && dataDetails.facilityCode,
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "Select",
            name: "type",
            label: "Type",
            options: optionsContainer,
            inputProps: {
              "data-testid": "typeyId",
              label: "Type",
              disabled: type === "VIEW" ? true : false,
            },
            helper: { title: "Type", placement: "bottom" },
            rules: {
              required: "It's required",
            },
            defaultValue: defaultValueTypeC && defaultValueTypeC[0],
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "Select",
            name: "FacilityName",
            label: "Facility",
            options: fieldOptions,
            inputProps: {
              "data-testid": "facilityId",
              label: "Facility",
              disabled: type === "VIEW" ? true : false,
            },
            helper: { title: "Facility", placement: "bottom" },
            rules: {
              required: "It's required",
            },
            defaultValue: defaultValueFacility && defaultValueFacility[0],
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "Select",
            name: "Sub-Facility",
            label: "Sub-Facility",
            options: fieldOptions,
            inputProps: {
              "data-testid": "sub-facilityId",
              label: "Sub-Facility",
              disabled: type === "VIEW" ? true : false,
            },
            helper: { title: "Sub-Facility", placement: "bottom" },
            rules: {
              required: "It's required",
            },
            defaultValue: defaultValueFacility && defaultValueFacility[0],
          },
        ],
      };
    }
    return {
      name: "Container",
      components: [],
    };
  };

  const onSubmit = async (formData) => {
    if (dataDetails?.facilityClass === "structure") {
      if (dataDetails?.parentFacility === null) {
        let dataFacility = {
          id: dataDetails?.id,
          facilityName: formData.name,
          facilityCode: formData.code,
          facilityType: formData.type.label,
          geospatialObjectId: formData.siteName.value,
          facilityClass: dataDetails?.facilityClass,
          // coordinates: formData.coordinates,
        };
        await modifyFacility(
          {
            data: dataFacility,
          },
          dispatch
        );
      } else {
        let subFacility = {
          id: dataDetails?.id,
          facilityName: formData.name,
          facilityCode: formData.code,
          facilityType: formData.type.label,
          parentFacilityId: formData.FieldName.value,
          facilityClass: dataDetails?.facilityClass,
          // coordinates: formData.coordinates,
        };

        await modifyFacility(
          {
            data: subFacility,
          },
          dispatch
        );
      }
    } else {
      let container = {
        id: dataDetails?.id,
        facilityName: formData.name,
        facilityCode: formData.code,
        facilityType: formData.type.label,
        parentFacilityId: formData.FacilityName.value,
        facilityClass: dataDetails?.facilityClass,
        // coordinates: formData.coordinates,
      };
      await modifyFacility(
        {
          data: container,
        },
        dispatch
      );
    }
  };

  return (
    /* 
     @prop data-testid: Id to use inside FormFacilityModify.test.js file.
     */
    <div>
      <EbsForm
        onSubmit={onSubmit}
        definition={definitionModifyFacility || { components: [] }}
        actionPlacement="top"
      >
        {type === "VIEW" ? null : (
          <DialogActions>
            <Button
              onClick={() => navigate(`/cs/place-manager`)}
              color="primary"
            >
              Cancel
            </Button>
            <Button
              type="submit"
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id="none" defaultMessage="Update" />
            </Button>
          </DialogActions>
        )}
      </EbsForm>
    </div>
  );
});
// Type and required properties
FormFacilityModifyAtom.propTypes = {};
// Default properties
FormFacilityModifyAtom.defaultProps = {};

export default FormFacilityModifyAtom;
