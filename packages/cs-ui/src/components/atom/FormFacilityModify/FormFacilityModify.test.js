import FormFacilityModify from './FormFacilityModify';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FormFacilityModify is in the DOM', () => {
  render(<FormFacilityModify></FormFacilityModify>)
  expect(screen.getByTestId('FormFacilityModifyTestId')).toBeInTheDocument();
})
