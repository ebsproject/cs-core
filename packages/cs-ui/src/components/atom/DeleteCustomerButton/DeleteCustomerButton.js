import React, { useEffect } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import {Core,Icons} from "@ebs/styleguide";
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Typography,
  Tooltip,
} = Core;
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { deleteCustomer } from "store/modules/TenantManagement";
const {Delete} = Icons;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteCustomerButtonMolecule = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();
    const { success } = useSelector(({ tenant }) => tenant);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleDelete = () => {
      deleteCustomer(rowSelected.id)(dispatch);
    };
    return (
      /* 
     @prop data-testid: Id to use inside deletecustomerbutton.test.js file.
     */
      <div ref={ref} data-testid={"DeleteCustomerButtonTestId"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
               <FormattedMessage id="none" defaultMessage="Delete Customer" />
            </Typography>
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="delete-customer"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <Dialog
          fullWidth
          keepMounted
          maxWidth="sm"
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Delete Customer" />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id="none"
                defaultMessage="Are you sure to delete following customer?"
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <Typography variant="button" >
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Typography>
            </Button>
            <Button onClick={handleDelete}>
              <Typography variant="button">
                <FormattedMessage id="none" defaultMessage="Confirm" />
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </div >
    );
  }
);
// Type and required properties
DeleteCustomerButtonMolecule.propTypes = {
  // rowSelected: PropTypes.object.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
DeleteCustomerButtonMolecule.defaultProps = {};

export default DeleteCustomerButtonMolecule;
