import React, { useEffect, useState } from "react";
import { useQuery } from "@apollo/client";
import { FIND_SERVICE_TYPE_LIST } from "utils/apollo/gql/sm";
import { clientSM } from "utils/apollo";
import { Controller } from "react-hook-form";
import PropTypes from "prop-types";
import { Core, Lab } from "@ebs/styleguide";
const { Autocomplete } = Lab;
const { FormControl, TextField, Typography } = Core;
import { FormattedMessage } from "react-intl";
import { findServiceByIdOrgUnit } from "store/modules/SM";
import { useFormContext } from "react-hook-form";


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ServiceTypeMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const {
    label,
    children,
    method,
    defaultValues,
    ...rest
  } = props;
  const {setValue, control, formState: { errors }, register } = useFormContext();
  const [itemValue, setItemValue] = useState(null);
  const [serviceTypeSelected, setServiceTypeSelected] = useState(null);
  useEffect(() => {
    if (method === "PUT") {
      const serviceProv = async () => {
        const result = await findServiceByIdOrgUnit(defaultValues?.id);
        setServiceTypeSelected(result?.findServiceProvider?.servicetypes[0]);
      };
      serviceProv();
    }
  }, [method]);

  useEffect(() => {
    if (serviceTypeSelected) {
      setValue("serviceTypeSelector", serviceTypeSelected);
    }
  }, [serviceTypeSelected]);

  const { loading, error, data } = useQuery(FIND_SERVICE_TYPE_LIST, {
    variables: {
      tenantId: "1",
    },
    client: clientSM,
  });

  if (loading) return null;
  if (error) return `Error! ${error}`;

  const result = data.findServiceTypeList.content;

  return (
    <FormControl fullWidth>
      <Controller
        name={`serviceTypeSelector`}
        control={control}
        render={({ field }) => (
          <Autocomplete
            {...field}
            options={result}
            getOptionLabel={(option) => (option.name ? option.name : "")}
            isOptionEqualToValue={(option, value) =>
              value === undefined || value === "" || option.name === value.name
            }
            renderInput={(params) => (
              <TextField
                {...params}
                error={Boolean(errors["serviceTypeSelector"])}
                placeholder="Choose a Services Types"
                label="Service Types"
              />
            )}
            onChange={(event, newValue) => {
              setValue("serviceTypeSelector", newValue);
              setItemValue(newValue);
            }}
          />
        )}
        // rules={{
        //   required: (
        //     <Typography>
        //       <FormattedMessage
        //         id={"none"}
        //         defaultMessage={"Please, Select one or more Service Types"}
        //       />
        //     </Typography>
        //   ),
        // }}
        defaultValue={serviceTypeSelected}
      />
    </FormControl>
  );
});
// Type and required properties
ServiceTypeMolecule.propTypes = {};
// Default properties
ServiceTypeMolecule.defaultProps = {};

export default ServiceTypeMolecule;
