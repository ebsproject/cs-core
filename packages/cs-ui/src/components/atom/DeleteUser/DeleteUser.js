import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector, useDispatch } from "react-redux";
import { EbsDialog } from "@ebs/styleguide";
import { deleteUser } from "store/modules/UserManagement";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const {
  Box,
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  Tooltip,
  Typography,
}  = Core;
const {Delete} = Icons;
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const DeleteUserAtom = React.forwardRef(({ refresh, userId, name, familyName }, ref) => {
  const [open, setOpen] = useState(false);
  const { success } = useSelector(({ um }) => um);
  const dispatch = useDispatch();
  const handleClickOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  useEffect(() => {
    if (success) {
      handleClose();
      refresh();
    }
  }, [success]);
  const submit = () => {
    deleteUser({ id: userId })(dispatch);
  };
  return (
    /**
     * @prop data-testid: Id to use inside DeleteUser.test.js file.
     */
    <div data-testid={"DeleteUserTestId"} ref={ref}>
      <Tooltip
        arrow
        placement="top"
        title={
          <Typography className="font-ebs text-xl">
            <FormattedMessage id="none" defaultMessage="Delete User" />
          </Typography>
        }
        className="border-red-700"
      >
        <IconButton
          onClick={handleClickOpen}
          aria-label="delete-user"
          color="primary"
        >
          <Delete />
        </IconButton>
      </Tooltip>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        maxWidth="sm"
        title={<FormattedMessage id="none" defaultMessage="Delete User" />}
      >
        <DialogContent dividers>
          <Typography className="text-ebs text-bold">
            <FormattedMessage
              id="none"
              defaultMessage={"You are about to permanently delete the user " + name + " " + familyName + ". Click SUBMIT to proceed."}
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id="none" defaultMessage="Cancel" />
          </Button>
          <Button onClick={submit}>
            <FormattedMessage id="none" defaultMessage="Submit" />
          </Button>
        </DialogActions>
      </EbsDialog>
    </div>
  );
});
// Type and required properties
DeleteUserAtom.propTypes = {
  refresh: PropTypes.func,
  userId: PropTypes.number,
};
// Default properties
DeleteUserAtom.defaultProps = {};

export default DeleteUserAtom;
