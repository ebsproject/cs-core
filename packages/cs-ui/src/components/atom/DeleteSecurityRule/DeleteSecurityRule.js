import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EbsDialog } from "@ebs/styleguide";
import { useDispatch, useSelector } from "react-redux";
import { mutationSecurityRule } from "store/modules/Rules";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const {
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  Tooltip,
  Typography,
} = Core;
const {Delete} = Icons;
//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DeleteSecurityRuleAtom = React.forwardRef(({ id, name, refresh }, ref) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const { success } = useSelector(({ um }) => um);
  useEffect(() => {
    if (success) {
      setOpen(false);
      refresh();
    }
  }, [success]);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const submit = async () => {
   await mutationSecurityRule({ id, method: "DELETE", refresh })(dispatch);
  };
  return (
    /* 
     @prop data-testid: Id to use inside DeleteRole.test.js file.
     */
    <>
      <Tooltip
        arrow
        title={
          <Typography className="font-ebs text-xl">
            <FormattedMessage id={"none"} defaultMessage={"Delete Security Rule"} />
          </Typography>
        }
      >
        <IconButton
          data-testid={"DeleteSecurityRuleTestId"}
          onClick={handleOpen}
          color="primary"
          //className="text-green-600"
        >
          <Delete />
        </IconButton>
      </Tooltip>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        title={<FormattedMessage id={"none"} defaultMessage={"Delete Security Rule"} />}
        maxWidth="sm"
      >
        <DialogContent dividers>
          <Typography className="font-ebs text-bold">
            <FormattedMessage
              id={"none"}
              defaultMessage={
                "You are about to permanently delete the security role " +
                name +
                ". Click DELETE to proceed."
              }
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id="none" defaultMessage="Cancel" />
          </Button>
          <Button onClick={submit}>
            <FormattedMessage id="none" defaultMessage="Delete" />
          </Button>
        </DialogActions>
      </EbsDialog>
    </>
  );
});
// Type and required properties
DeleteSecurityRuleAtom.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  refresh: PropTypes.func,
};
// Default properties
DeleteSecurityRuleAtom.defaultProps = {};

export default DeleteSecurityRuleAtom;
