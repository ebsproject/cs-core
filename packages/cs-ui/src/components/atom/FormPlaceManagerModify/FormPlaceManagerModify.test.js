import FormPlaceManagerModify from './FormPlaceManagerModify';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FormPlaceManagerModify is in the DOM', () => {
  render(<FormPlaceManagerModify></FormPlaceManagerModify>)
  expect(screen.getByTestId('FormPlaceManagerModifyTestId')).toBeInTheDocument();
})
