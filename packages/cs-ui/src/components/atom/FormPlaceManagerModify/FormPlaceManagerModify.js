import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Button, DialogActions } = Core;
import { EbsForm } from "@ebs/components";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { modifyGeospatialObject } from "store/modules/PlaceManager";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";
import { clientCb } from "utils/apollo/apollo";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FormPlaceManagerModifyAtom = React.forwardRef(({ data, type }, ref) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [dataDetail, setDataDetail] = useState(null);
  const [dataList, setDataList] = useState(null);
  const optionsType = [
    { value: 1, label: "Seed warehouse" },
    { value: 2, label: "Green House" },
    { value: 3, label: "Laboratory" },
    { value: 4, label: "breeding location" },
    { value: 5, label: "national agricultural research center" },
    { value: 6, label: "met location" },
    { value: 7, label: "international agricultural research center" },
    { value: 8, label: "third sub-national division" },
  ];

  useEffect(() => {
    let number = 1;
    let dataFetched = [];
    const fetchData = async () => {
      const { data } = await clientCb.query({
        query: FIND_PLACES_LIST,
        variables: {
          page: { number: number, size: 1000 },
          filters: [
            { col: "geospatialObjectType", mod: "EQ", val: "field" },
            { col: "geospatialObjectType", mod: "EQ", val: "site" },
            { col: "geospatialObjectType", mod: "EQ", val: "country" },
          ],
          disjunctionFilters: true,
        },
        fetchPolicy: "no-cache",
      });
      number = data?.findGeospatialObjectList?.totalPages;
      dataFetched = data.findGeospatialObjectList.content;
      if (number > 1) {
        for (let i = 1; i < number; i++) {
          const { data } = await clientCb.query({
            query: FIND_PLACES_LIST,
            variables: {
              page: { number: i + 1, size: 1000 },
              filters: [
                { col: "geospatialObjectType", mod: "EQ", val: "field" },
                { col: "geospatialObjectType", mod: "EQ", val: "site" },
                { col: "geospatialObjectType", mod: "EQ", val: "country" },
              ],
              disjunctionFilters: true,
            },
            fetchPolicy: "no-cache",
          });
          dataFetched = [
            ...dataFetched,
            ...data.findGeospatialObjectList.content,
          ];
        }
      }
      setDataList(dataFetched);
    };
    fetchData();
  }, []);

  useEffect(() => {
    if (data) {
      setDataDetail(null);
      const fetchDataList = async () => {
        const { data: dataRes } = await clientCb.query({
          query: FIND_PLACES_LIST,
          variables: {
            filters: [{ col: "id", mod: "EQ", val: data?.id }],
          },
          fetchPolicy: "no-cache",
        });
        setDataDetail(dataRes.findGeospatialObjectList.content[0]);
      };
      fetchDataList();
    }
  }, [data]);

  if (!dataDetail || !dataList) return "Loading.....";

  let siteOptionslist = dataList
    .filter((item) => item.geospatialObjectType === "site")
    ?.map((item) => {
      return { label: item.geospatialObjectName, value: item.id };
    });

  const definitionCreateSite = ({ getValues, setValue, reset }) => {
    switch (dataDetail?.geospatialObjectType) {
      case "site":
        const defaultValueType = optionsType.filter(
          (item) => item.label === dataDetail?.geospatialObjectSubtype
        );
        let optionsCountry = dataList
          .filter((item) => item.geospatialObjectType === "country")
          ?.map((item) => {
            return { label: item.geospatialObjectName, value: item.id };
          });

        const defaultValueCountry = optionsCountry.filter(
          (item) =>
            item.label ===
            dataDetail?.parentGeospatialObject?.geospatialObjectName
        );
        return {
          name: "Site",
          components: [
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "geospatialObjectName",
              inputProps: {
                "data-testid": "geospatialObjectName",
                label: "Site Name",
                disabled: type === "VIEW" ? true : false,
                // variant: "outlined",
              },
              defaultValue: dataDetail.geospatialObjectName,
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "geospatialObjectCode",
              inputProps: {
                "data-testid": "geospatialObjectCode",
                label: "Site Code",
                disabled: type === "VIEW" ? true : false,
                // variant: "outlined",
              },
              defaultValue: dataDetail.geospatialObjectCode,
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "type",
              label: "Type",
              options: optionsType,
              inputProps: {
                "data-testid": "typeyId",
                label: "Type",
                disabled: type === "VIEW" ? true : false,
              },
              helper: { title: "Type", placement: "bottom" },
              rules: {
                required: "It's required",
              },
              defaultValue:
                defaultValueType.length === 0
                  ? { label: "", value: "" }
                  : defaultValueType[0],
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "country",
              label: "Choose Country",
              options: optionsCountry,
              inputProps: {
                "data-testid": "chooseCountryId",
                label: "Country",
                disabled: type === "VIEW" ? true : false,
              },
              helper: { title: "Country", placement: "bottom" },
              rules: {
                required: "It's required",
              },
              defaultValue: defaultValueCountry[0],
            },
            {
              sizes: [12, 12, 12, 12, 12],
              component: "map",
              name: "coordinates",
              inputProps: {
                "data-testid": "field",
                label: "Choose Field",
                coordinates: {
                  lat:
                    dataDetail.coordinates === null
                      ? 19.5307604699681
                      : dataDetail.coordinates.features[0].geometry.type ===
                        "Point"
                      ? dataDetail.coordinates.features[0].geometry
                          .coordinates[1]
                      : dataDetail.coordinates.features[0].geometry
                          .coordinates[0][0][1],
                  lng:
                    dataDetail.coordinates === null
                      ? -98.84719079491951
                      : dataDetail.coordinates.features[0].geometry.type ===
                        "Point"
                      ? dataDetail.coordinates.features[0].geometry
                          .coordinates[0]
                      : dataDetail.coordinates.features[0].geometry
                          .coordinates[0][0][0],
                },
                defaultValue: dataDetail?.coordinates,
              },
              rules: {
                required: "A Field is required",
              },
            },
          ],
        };
      case "field":
        let defaultValueSite = siteOptionslist?.filter(
          (item) =>
            item.label ===
            dataDetail?.parentGeospatialObject?.geospatialObjectName
        );

        return {
          name: "Field",
          components: [
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "geospatialObjectName",
              inputProps: {
                "data-testid": "geospatialObjectName",
                label: "Field Name",
                disabled: type === "VIEW" ? true : false,
                // variant: "outlined",
              },
              defaultValue: dataDetail.geospatialObjectName,
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "TextField",
              name: "geospatialObjectCode",
              inputProps: {
                "data-testid": "geospatialObjectCode",
                label: "Field Code",
                disabled: type === "VIEW" ? true : false,
                // variant: "outlined",
              },
              defaultValue: dataDetail.geospatialObjectCode,
            },
            {
              sizes: [6, 6, 6, 6, 6],
              component: "Select",
              name: "siteParent",
              label: "Site",
              options: siteOptionslist,
              inputProps: {
                "data-testid": "SiteId",
                label: "Site",
                // disabled: true,
              },
              helper: { title: "Site", placement: "bottom" },
              rules: {
                required: "It's required",
              },
              defaultValue: defaultValueSite[0],
            },
            {
              sizes: [12, 12, 12, 12, 12],
              component: "map",
              name: "coordinates",
              inputProps: {
                "data-testid": "field",
                label: "Choose Field",
                coordinates: {
                  lat:
                    dataDetail.coordinates === null
                      ? 19.5307604699681
                      : dataDetail.coordinates.features[0].geometry.type ===
                        "Point"
                      ? dataDetail.coordinates.features[0].geometry
                          .coordinates[1]
                      : dataDetail.coordinates.features[0].geometry
                          .coordinates[0][0][1],
                  lng:
                    dataDetail.coordinates === null
                      ? -98.84719079491951
                      : dataDetail.coordinates.features[0].geometry.type ===
                        "Point"
                      ? dataDetail.coordinates.features[0].geometry
                          .coordinates[0]
                      : dataDetail.coordinates.features[0].geometry
                          .coordinates[0][0][0],
                },
                defaultValue: dataDetail?.coordinates,
              },
              rules: {
                required: "A Field is required",
              },
            },
          ],
        };
      case "planting area":
        let defaultValueSiteP = siteOptionslist?.filter(
          (item) =>
            item.label ===
            dataDetail?.parentGeospatialObject?.geospatialObjectName
        );

        let defaultValueSiteP2 = siteOptionslist?.filter(
          (item) =>
            item.label ===
            dataDetail?.parentGeospatialObject?.parentGeospatialObject
              ?.geospatialObjectName
        );

        let filterFields = dataList
          .filter((item) => item.geospatialObjectType === "field")
          .filter(
            (filter) =>
              filter.parentGeospatialObject?.geospatialObjectName ===
              defaultValueSiteP2[0]?.label
          );

        let optionsFields = filterFields?.map((field) => {
          return { label: field.geospatialObjectName, value: field.id };
        });

        let defaultValueField = optionsFields?.filter(
          (item) =>
            item.label ===
            dataDetail?.parentGeospatialObject?.geospatialObjectName
        );

        //conditional
        if (
          dataDetail?.parentGeospatialObject?.parentGeospatialObject
            ?.geospatialObjectType === "country"
        ) {
          return {
            name: "Planting Area",
            components: [
              {
                sizes: [6, 6, 6, 6, 6],
                component: "TextField",
                name: "geospatialObjectName",
                inputProps: {
                  "data-testid": "geospatialObjectName",
                  label: "Planting Area Name",
                  disabled: type === "VIEW" ? true : false,
                  // variant: "outlined",
                },
                defaultValue: dataDetail && dataDetail.geospatialObjectName,
              },
              {
                sizes: [6, 6, 6, 6, 6],
                component: "TextField",
                name: "geospatialObjectCode",
                inputProps: {
                  "data-testid": "geospatialObjectCode",
                  label: "Planting Area Code",
                  disabled: type === "VIEW" ? true : false,
                  // variant: "outlined",
                },
                defaultValue: dataDetail && dataDetail.geospatialObjectCode,
              },
              {
                sizes: [6, 6, 6, 6, 6],
                component: "Select",
                name: "siteParent",
                label: "Site",
                options: siteOptionslist,
                inputProps: {
                  "data-testid": "SiteId",
                  label: "Site",
                  // disabled: true,
                },
                helper: { title: "Site", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: defaultValueSiteP?.[0],
              },
              // {
              //   sizes: [6, 6, 6, 6, 6],
              //   component: "Select",
              //   name: "fieldParent",
              //   label: "Field",
              //   options: optionField,
              //   inputProps: {
              //     "data-testid": "FieldId",
              //     label: "Field",
              //     disabled: type === "VIEW" ? true : false,
              //   },
              //   helper: { title: "Field", placement: "bottom" },
              //   rules: {
              //     required: "It's required",
              //   },
              //   defaultValue: { label: "", value: "" },
              // },
              {
                sizes: [12, 12, 12, 12, 12],
                component: "map",
                name: "coordinates",
                inputProps: {
                  "data-testid": "field",
                  label: "Choose Field",
                  coordinates: {
                    lat:
                      dataDetail.coordinates === null
                        ? 19.5307604699681
                        : dataDetail.coordinates.features[0].geometry.type ===
                          "Point"
                        ? dataDetail.coordinates.features[0].geometry
                            .coordinates[1]
                        : dataDetail.coordinates.features[0].geometry
                            .coordinates[0][0][1],
                    lng:
                      dataDetail.coordinates === null
                        ? -98.84719079491951
                        : dataDetail.coordinates.features[0].geometry.type ===
                          "Point"
                        ? dataDetail.coordinates.features[0].geometry
                            .coordinates[0]
                        : dataDetail.coordinates.features[0].geometry
                            .coordinates[0][0][0],
                  },
                  defaultValue: dataDetail && dataDetail?.coordinates,
                },
                rules: {
                  required: "A Field is required",
                },
              },
            ],
          };
        } else {
          return {
            name: "Planting Area",
            components: [
              {
                sizes: [6, 6, 6, 6, 6],
                component: "TextField",
                name: "geospatialObjectName",
                inputProps: {
                  "data-testid": "geospatialObjectName",
                  label: "Planting Area Name",
                  disabled: type === "VIEW" ? true : false,
                  // variant: "outlined",
                },
                defaultValue: dataDetail && dataDetail.geospatialObjectName,
              },
              {
                sizes: [6, 6, 6, 6, 6],
                component: "TextField",
                name: "geospatialObjectCode",
                inputProps: {
                  "data-testid": "geospatialObjectCode",
                  label: "Planting Area Code",
                  disabled: type === "VIEW" ? true : false,
                  // variant: "outlined",
                },
                defaultValue: dataDetail && dataDetail.geospatialObjectCode,
              },
              {
                sizes: [6, 6, 6, 6, 6],
                component: "Select",
                name: "siteParent",
                label: "Site",
                options: siteOptionslist,
                inputProps: {
                  "data-testid": "SiteId",
                  label: "Site",
                  disabled: true,
                },
                helper: { title: "Site", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: defaultValueSiteP2?.[0],
              },
              {
                sizes: [6, 6, 6, 6, 6],
                component: "Select",
                name: "fieldParent",
                label: "Field",
                options: optionsFields,
                inputProps: {
                  "data-testid": "FieldId",
                  label: "Field",
                  disabled: type === "VIEW" ? true : false,
                },
                helper: { title: "Field", placement: "bottom" },
                rules: {
                  required: "It's required",
                },
                defaultValue: defaultValueField?.[0],
              },
              {
                sizes: [12, 12, 12, 12, 12],
                component: "map",
                name: "coordinates",
                inputProps: {
                  "data-testid": "field",
                  label: "Choose Field",
                  coordinates: {
                    lat:
                      dataDetail.coordinates === null
                        ? 19.5307604699681
                        : dataDetail.coordinates.features[0].geometry.type ===
                          "Point"
                        ? dataDetail.coordinates.features[0].geometry
                            .coordinates[1]
                        : dataDetail.coordinates.features[0].geometry
                            .coordinates[0][0][1],
                    lng:
                      dataDetail.coordinates === null
                        ? -98.84719079491951
                        : dataDetail.coordinates.features[0].geometry.type ===
                          "Point"
                        ? dataDetail.coordinates.features[0].geometry
                            .coordinates[0]
                        : dataDetail.coordinates.features[0].geometry
                            .coordinates[0][0][0],
                  },
                  defaultValue: dataDetail && dataDetail?.coordinates,
                },
                rules: {
                  required: "A Field is required",
                },
              },
            ],
          };
        }
      default:
        null;
    }
  };

  const onSubmit = async (formData) => {
    switch (dataDetail.geospatialObjectType) {
      case "site":
        let dataSite = {
          id: dataDetail?.id,
          geospatialObjectCode: formData.geospatialObjectCode,
          geospatialObjectName: formData.geospatialObjectName,
          geospatialObjectType: dataDetail.geospatialObjectType,
          geospatialObjectSubtype: formData.type.label,
          parentGeospatialObjectId: formData.country.value,
          coordinates:
            formData.coordinates === null ? null : formData.coordinates,
        };
        await modifyGeospatialObject(
          {
            data: dataSite,
          },
          dispatch
        );
        break;
      case "field":
        let dataField = {
          id: dataDetail?.id,
          geospatialObjectCode: formData.geospatialObjectCode,
          geospatialObjectName: formData.geospatialObjectName,
          geospatialObjectType: dataDetail.geospatialObjectType,
          parentGeospatialObjectId: formData.siteParent.value,
          coordinates:
            formData.coordinates === null ? null : formData.coordinates,
        };
        await modifyGeospatialObject(
          {
            data: dataField,
          },
          dispatch
        );
        break;
      case "planting area":
        let dataPl = {
          id: dataDetail?.id,
          geospatialObjectCode: formData.geospatialObjectCode,
          geospatialObjectName: formData.geospatialObjectName,
          geospatialObjectType: dataDetail.geospatialObjectType,
          parentGeospatialObjectId:
            formData.fieldParent === undefined
              ? formData.siteParent.value
              : formData.fieldParent.value,
          coordinates:
            formData.coordinates === null ? null : formData.coordinates,
        };
        await modifyGeospatialObject(
          {
            data: dataPl,
          },
          dispatch
        );
        break;
    }
  };
  return (
    <div>
      <EbsForm
        onSubmit={onSubmit}
        definition={definitionCreateSite}
        actionPlacement="top"
      >
        {type === "VIEW" ? null : (
          <DialogActions>
            <Button
              onClick={() => navigate(`/cs/place-manager`)}
              color="primary"
            >
              Cancel
            </Button>
            <Button
              type="submit"
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id="none" defaultMessage="Update" />
            </Button>
          </DialogActions>
        )}
      </EbsForm>
    </div>
  );
});
// Type and required properties
FormPlaceManagerModifyAtom.propTypes = {};
// Default properties
FormPlaceManagerModifyAtom.defaultProps = {};

export default FormPlaceManagerModifyAtom;
