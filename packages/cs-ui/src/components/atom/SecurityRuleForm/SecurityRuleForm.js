import React from "react";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import { useForm, Controller } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
import "react-checkbox-tree/lib/react-checkbox-tree.css";
import { mutationSecurityRule } from "store/modules/Rules";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons, Lab } from "@ebs/styleguide";
const {
  Switch,
  Backdrop,
  Box,
  Button,
  FormControl,
  FormLabel,
  Grid,
  TextField,
  Typography
} = Core;
const { Cancel, CheckCircle } = Icons;

const SecurityRuleFormAtom = React.forwardRef(({ method, defaultValues }, ref) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const {
    control,
    getValues,
    setValue,
    formState: { errors },
    handleSubmit,
  } = useForm({
    defaultValues: (method === "PUT" && defaultValues) || {},
  });
  /**
   * @param {Object} data
   */
  const submit = (data) => {
    mutationSecurityRule({
      data,
      method,
    })(dispatch);
   navigate("/cs/usermanagement", { index: 2 }); //TODO:: REVIEW the object passed to navigate
  };

  const handleCancel = () => {
    navigate("/cs/usermanagement", { index: 2 });//TODO:: REVIEW the object passed to navigate
  };
  return (
    /**
     * @prop data-testid: Id to use inside RoleForm.test.js file.
     */
    <>
      <form
        data-testid={"SecurityRuleFormTestId"}
        ref={ref}
        className="grid grid-cols-2 gap-5"
        onSubmit={handleSubmit(submit)}
      >
          <Typography className="flex-grow font-ebs text-ebs-green-900 font-bold">
            <FormattedMessage id="none" defaultMessage="Basic Information" />
          </Typography>
        <Grid container justifyContent="flex-end">

          <Button
            startIcon={<Cancel />}
            onClick={handleCancel}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            startIcon={<CheckCircle />}
            type={"submit"}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Save"} />
          </Button>
        </Grid>
        <Controller
          name="id"
          control={control}
          render={({ field }) => (
            <TextField {...field} sx={{display:"none"}} data-testid={"id"} />
          )}
        />
        <Controller
          name="name"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              error={!!errors["name"]}
              helperText={errors["name"] ? errors["name"].message : ''}
              label={<FormattedMessage id={"none"} defaultMessage={"Name"} />}
              data-testid={"name"}
            />
          )}
          rules={{required:"This field is required"}}
        />
        <Controller
          name="description"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              error={!!errors["description"]}
              helperText={errors["description"] ? errors["description"].message : ''}
              label={
                <FormattedMessage id={"none"} defaultMessage={"Description"} />
              }
              data-testid={"description"}
            />
          )}
          rules={{required:"This field is required"}}
        />
        <Controller
          name="usedByWorkflow"
          control={control}
          render={({ field }) => (
            <FormControl
              data-testid={'RadioFormTestId'}
              ref={ref}
              component='fieldset'
              fullWidth
            >
              <FormLabel component='legend' className='font-ebs text-lg font-bold'>
                {"Used by Workflow"}
              </FormLabel>
              <Switch
                color="primary"
                className="mt-2"
                checked={field.value}
                onChange={(e) => setValue("usedByWorkflow", e.target.checked)}
                inputProps={{ "aria-label": "secondary checkbox" }}
              />

            </FormControl>


          )}
        />

      </form>
    </>
  );
});
// Type and required properties
SecurityRuleFormAtom.propTypes = {
  method: PropTypes.oneOf(["POST", "PUT"]),
  defaultValues: PropTypes.shape({}),
};
// Default properties

export default SecurityRuleFormAtom;
