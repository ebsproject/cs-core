import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import AddCustomerButton from "./AddCustomerButton";

afterEach(cleanup);

test("AddCustomerButton is in the DOM", () => {
  render(
    <AddCustomerButton
      rowSelected={[{}]}
      refresh={() => {}}
      handleMenuClose={() => {}}
    ></AddCustomerButton>,
    { wrapper: Wrapper }
  );
  expect(screen.getByTestId("AddCustomerButtonTestId")).toBeInTheDocument();
});
