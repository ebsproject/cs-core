import React, { useEffect, useState } from "react";
import { client } from "utils/apollo";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { EbsForm } from "@ebs/components";
import { FIND_ORGANIZATION_LIST } from "utils/apollo/gql/tenantManagement";
import {Core,Icons} from "@ebs/styleguide";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
} = Core;
import { useDispatch, useSelector } from "react-redux";
const { PostAdd }= Icons;
import { createCustomer } from "store/modules/TenantManagement";
import { useQuery } from "@apollo/client";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AddCustomerButton = React.forwardRef(({ rowSelected, refresh }, ref) => {
  const [open, setOpen] = useState(false);
  const { success } = useSelector(({ tenant }) => tenant);
  const dispatch = useDispatch();
  const { data, error } = useQuery(FIND_ORGANIZATION_LIST, {
    variables: { },
    fetchPolicy: "no-cache",
    client: client,
  });

  useEffect(() => {
    if (success) {
      handleClose();
      refresh();
    }
  }, [refresh, success]);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const definitionCustomer = ({ getValues, setValue, reset }) => {
    const organizationList = data.findOrganizationList.content;
    const optionsOrganization = organizationList.map((item) => {
      return { label: item.name, value: item.id };
    });

    return {
      name: "createCustomer",
      components: [
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "name",
          inputProps: {
            "data-testid": "name",
            label: "Customer Name",
          },
          rules: {
            required: "A name Customer is required",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "officialEmail",
          inputProps: {
            "data-testid": "officialEmail",
            label: "Email",
          },
          rules: {
            required: "A Email is required",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "alternateEmail",
          inputProps: {
            "data-testid": "alternateEmail",
            label: "Alternate Email",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "phone",
          inputProps: {
            "data-testid": "phone",
            label: "Phone",
          },
          rules: {
            required: "A Phone is required",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "jobTitle",
          inputProps: {
            "data-testid": "jobTitle",
            label: "Job Title",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "languagePreference",
          inputProps: {
            "data-testid": "languagePreference",
            label: "Language Preference",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "TextField",
          name: "phoneExtension",
          inputProps: {
            "data-testid": "phoneExtension",
            label: "Phone Extension",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "Select",
          name: "owningOrganizationId",
          label: "Owning Organization ID",
          options: optionsOrganization,
          inputProps: {
            "data-testid": "owningOrganizationId",
            label: "Owning Organization ID",
          },
          helper: { title: "Owning Organization ID", placement: "top" },
          rules: {
            required: "It's required",
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: "File",
          name: "logo",
          label: "UPLOAD CUSTOMER LOGO...",
          customProps: {
            button: { color: "primary", size: "large" },
            input: {
              acceptedFiles: ["image/*"],
              cancelButtonText: "cancel",
              submitButtonText: "submit",
              maxFileSize: 5000000,
              showPreviews: true,
              showFileNamesInPreview: true,
              isMulti: false,
            },
          },
          helper: { title: "Logo", placement: "right", arrow: true },
        },
      ],
    };
  };

  const onSubmit = (formData) => {
    const valueOrganization = formData.owningOrganizationId.value;
    const imagen = formData.logo[0];
    // validation if the logo is null
    if (imagen === undefined || "" || null) {
      createCustomer({
        ...formData,
        id: 0,
        active: false,
        logo: null,
        owningOrganizationId: valueOrganization,
        phoneExtension:
          formData.phoneExtension === ""
            ? null
            : Number(formData.phoneExtension),
      })(dispatch);
    } else {
      var reader = new FileReader();
      reader.readAsDataURL(imagen);
      // conversion to base 64
      reader.onload = function () {
        var arrayAuxiliar = [];
        var base64 = reader.result;
        arrayAuxiliar = base64.split(",");
        var imagen64 = arrayAuxiliar[1];
        createCustomer({
          ...formData,
          id: 0,
          active: false,
          logo: imagen64,
          owningOrganizationId: valueOrganization,
          phoneExtension:
            formData.phoneExtension === ""
              ? null
              : Number(formData.phoneExtension),
        })(dispatch);
      };
    }
  };

  return (
    /* 
     @prop data-testid: Id to use inside addproductbutton.test.js file.
     */
    <div ref={ref} data-testid={"AddCustomerButtonTestId"}>
      <Button
       className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
       variant="contained"
        aria-label="add-customer"
        color="primary"
        startIcon={<PostAdd className="fill-current text-white" />}
        onClick={handleClickOpen}
        disabled={rowSelected ? false : true}
      >
        <Typography className="text-white">
          <FormattedMessage id="none" defaultMessage="Add Customer" />
        </Typography>
      </Button>
      <Dialog onClose={handleClose} open={open}>
        <DialogTitle className="absolute">
          <FormattedMessage id="none" defaultMessage="New Customer"/>
        </DialogTitle>
        <DialogContent>
          <EbsForm onSubmit={onSubmit} definition={definitionCustomer} actionPlacement="top">
            <DialogActions>
              <Button onClick={handleClose} >
                <FormattedMessage id="none" defaultMessage="Close" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Save" />
              </Button>
            </DialogActions>
          </EbsForm>
        </DialogContent>
      </Dialog>
    </div>
  );
});
// Type and required properties
AddCustomerButton.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
AddCustomerButton.defaultProps = {};
export default AddCustomerButton;
