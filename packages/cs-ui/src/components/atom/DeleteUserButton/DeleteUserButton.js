import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EbsDialog } from "@ebs/styleguide";
import { useDispatch, useSelector } from "react-redux";
import { deleteUser } from "store/modules/UserManagement";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const {
  Button,
  DialogActions,
  DialogContent,
  IconButton,
  Tooltip,
  Typography,
} = Core;
const {Delete} = Icons;
//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DeleteUserAtom = React.forwardRef(({ rowSelected, refresh }, ref) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const { success } = useSelector(({ um }) => um);
  useEffect(() => {
    if (success) {
      setOpen(false);
      refresh();
    }
  }, [success]);

  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const submit = () => {
    deleteUser(rowSelected.id)(dispatch);
  };
  return (
    /* 
     @prop data-testid: Id to use inside DeleteUser.test.js file.
     */
    <>
      <Tooltip
        arrow
        title={
          <Typography className="font-ebs text-xl">
            <FormattedMessage id={"none"} defaultMessage={"Delete User"} />
          </Typography>
        }
      >
        <IconButton
          data-testid={"DeleteUserTestId"}
          onClick={handleOpen}
          color="primary"
        >
          <Delete />
        </IconButton>
      </Tooltip>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        title={<FormattedMessage id={"none"} defaultMessage={"Delete User"} />}
        maxWidth="sm"
      >
        <DialogContent dividers>
          <Typography className="font-ebs text-bold">
            <FormattedMessage
              id={"none"}
              defaultMessage={
                "You are going to delete this user permanently, are you sure?:"
              }
            />
            {name}
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id="none" defaultMessage="Cancel" />
          </Button>
          <Button onClick={submit}>
            <FormattedMessage id="none" defaultMessage="Delete" />
          </Button>
        </DialogActions>
      </EbsDialog>
    </>
  );
});
// Type and required properties
DeleteUserAtom.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  refresh: PropTypes.func,
};
// Default properties
DeleteUserAtom.defaultProps = {};

export default DeleteUserAtom;
