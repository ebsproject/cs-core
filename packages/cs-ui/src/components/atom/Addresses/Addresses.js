import React, { Fragment, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Controller, useFieldArray } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { removeAddress } from "store/modules/CRM";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { showMessage } from "store/modules/message";
const {
  Box,
  Button,
  TextField,
  Tooltip,
  Typography,
  Chip,
  Checkbox,
  FormControlLabel,
  Radio,
} = Core;
const { Add, Close } = Icons;
const { Autocomplete } = Lab;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const AddressesAtom = React.forwardRef(
  (
    {
      control,
      errors,
      setValue,
      getValues,
      type,
      valuesRadio,
      setValueRadio,
      method,
      idUser,
      regex,
      arrayInstitutionDelete,
      setArrayInstitutionDelete,
    },
    ref
  ) => {
    const { countryList, purposeList, defaultValues } = useSelector(
      ({ persist }) => persist
    );
    const dispatch = useDispatch();
    const {
      fields: addresses,
      append: addAddress,
      remove: deleteAddress,
    } = useFieldArray({
      control,
      name: "addresses",
    });
    let regexPostal = new RegExp("^[0-9a-zA-Z_-_,_.'° ]*$");
    /**
     * @param {Int} index
     * @param {ID} id Address id
     */
    const arrayAddress = defaultValues?.addresses;
    let arrayIndex = [];
    //console.log(method);
    useEffect(() => {
      if (arrayInstitutionDelete.length > 0) {
        arrayInstitutionDelete.map((item) => {
          arrayAddress.map((data, index) => {
            if (data.id === item) {
              arrayIndex.push(index);
              removeAddress({ id: item, idUser: idUser })(dispatch);
            }
          });
        });
        deleteAddress(arrayIndex);
        setArrayInstitutionDelete([]);
      }
    }, [arrayInstitutionDelete]);

    const rAddress = ({ index }) => {
      const idDefaultAddress = arrayAddress[index]?.default;
      if (idDefaultAddress === true) {
        dispatch(
          showMessage({
            message:
              "Default addresses cannot be deleted, if you want to delete this address, select another default option.",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } else {
        deleteAddress(index);
        const idDeleteAddress = arrayAddress[index]?.id;
        if (idDeleteAddress !== undefined) {
          removeAddress({ id: idDeleteAddress, idUser: idUser })(dispatch);
        }
      }
    };
    if (
      method === "POST" &&
      addresses.length === 1 &&
      addresses[0].country === undefined
    ) {
      rAddress({ id: addresses[0].id, index: 0 });
    }
    useEffect(() => {
      if (valuesRadio) {
        addAddress(valuesRadio);
      }
      return () => setValueRadio(null);
    }, [valuesRadio]);
    /**
     * @param {Object} option
     * @returns {Node}
     */
    const optionLabel = (option) => `${option.name}`;
    /**
     * @param {Object} e
     * @param {Object} options
     * @param {String} field
     */
    const onCountryChange = (e, options, field) => {
      e.preventDefault();
      setValue(field, options);
    };
    /**
     * @param {Object} params
     * @param {String} field
     * @returns {Node}
     */
    const renderInputCountry = (params, field) => {
      return (
        <TextField
          required
          {...params}
          error={Boolean(errors[field])}
          label={
            <Typography className="text-ebs">
              <FormattedMessage id={"none"} defaultMessage={"Country"} />
              {` *`}
            </Typography>
          }
          InputLabelProps={{
            shrink: true,
          }}
          helperText={errors[field]?.message}
        />
      );
    };
    /**
     * @param {Object} params
     * @param {String} field
     * @returns {Node}
     */
    const renderInputPurpose = (params, field, options) => {
      const newInputProps = new Object();
      if (!getValues(field)) {
        Object.assign(newInputProps, { value: "" });
      }
      return (
        <TextField
          {...params}
          required={
            options === null || options === undefined || options.length === 0
          }
          inputProps={{
            ...params.inputProps,
            ...newInputProps,
          }}
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors[field])}
          error={
            options === null || options === undefined || options.length === 0
          }
          label={
            <Typography className="text-ebs">
              <FormattedMessage id={"none"} defaultMessage={"Purposes"} />
              {` *`}
            </Typography>
          }
          helperText={errors[field]?.message}
        />
      );
    };
    /**
     * @param {Object} option
     * @param {Object} value
     * @returns {Boolean}
     */
    const getOptionSelected = (option, value) => option.id === value.id;
    const porpuseTypeOption = (option, { selected }) => {
      return (
        <>
          <Checkbox checked={selected} />
          {option.name}
        </>
      );
    };
    return (
      /**
       * @prop data-testid: Id to use inside Addresses.test.js file.
       */
      <Box data-testid={"AddressesTestId"} ref={ref}>
        <div className="col-span-4">
          {addresses?.map((address, index) => (
            <Fragment key={address.id}>
              <Controller
                name={`addresses.${index}.id`}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    inputProps={{ "data-testid": `addresses.${index}.id` }}
                    label={"id"}
                    className="hidden"
                  />
                )}
              />
              <div className="flex flex-nowrap">
                <div className="flex-grow">
                  <div className="grid grid-cols-2 gap-6">
                    <Controller
                      name={`addresses.${index}.country`}
                      control={control}
                      render={({ field }) => {
                        return (
                          <Autocomplete
                            // {...field}
                            className="p-2"
                            data-testid={`addresses.${index}.country`}
                            id={`addresses.${index}.country`}
                            options={countryList || []}
                            onChange={(e, options) => {
                              setValue(`addresses.${index}.country`, options);
                              onCountryChange(
                                e,
                                options,
                                `addresses.${index}.country`
                              );
                            }}
                            value={field?.value || { id: 0, name: "" }}
                            autoHighlight
                            // inputValue={
                            //   getValues(`addresses.${index}.country`)?.name || ''
                            // }
                            getOptionLabel={optionLabel}
                            disabled={address.disabled}
                            isOptionEqualToValue={getOptionSelected}
                            renderInput={(params) =>
                              renderInputCountry(
                                params,
                                `addresses.${index}.country`
                              )
                            }
                          />
                        );
                      }}
                      rules={{
                        required: (
                          <Typography>
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Please, Select a Country"}
                            />
                          </Typography>
                        ),
                      }}
                    />
                    <Controller
                      name={`addresses.${index}.location`}
                      control={control}
                      render={({ field }) => (
                        <TextField
                          className="p-2"
                          disabled={address.disabled}
                          error={regex.test(field.value) ? false : true}
                          helperText={
                            regex.test(field.value)
                              ? ""
                              : "This field only allows characters [a-z], it does not allow special characters."
                          }
                          InputLabelProps={{ shrink: true }}
                          {...field}
                          inputProps={{
                            "data-testid": `addresses.${index}.location`,
                          }}
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Locality"
                            />
                          }
                        />
                      )}
                      rules={{
                        validate: (value) => {
                          if (regex.test(value) === false) {
                            return "This field only allows characters [a-z], it does not allow special characters.";
                          } else {
                            return true;
                          }
                        },
                      }}
                    />
                    <Controller
                      name={`addresses.${index}.region`}
                      control={control}
                      render={({ field }) => (
                        <TextField
                          className="p-2"
                          disabled={address.disabled}
                          InputLabelProps={{ shrink: true }}
                          {...field}
                          error={regex.test(field.value) ? false : true}
                          helperText={
                            regex.test(field.value)
                              ? ""
                              : "This field only allows characters [a-z], it does not allow special characters."
                          }
                          inputProps={{
                            "data-testid": `addresses.${index}.region`,
                          }}
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Region"
                            />
                          }
                        />
                      )}
                      rules={{
                        validate: (value) => {
                          if (regex.test(value) === false) {
                            return "This field only allows characters [a-z], it does not allow special characters.";
                          } else {
                            return true;
                          }
                        },
                      }}
                    />
                    <Controller
                      name={`addresses.${index}.zipCode`}
                      control={control}
                      render={({ field }) => (
                        <TextField
                          className="p-2"
                          disabled={address.disabled}
                          InputLabelProps={{ shrink: true }}
                          {...field}
                          error={regexPostal.test(field.value) ? false : true}
                          helperText={
                            regexPostal.test(field.value)
                              ? ""
                              : "This field only allows characters [a-z] [0-9], it does not allow special characters."
                          }
                          inputProps={{
                            "data-testid": `addresses.${index}.zipCode`,
                          }}
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Postal Code"
                            />
                          }
                        />
                      )}
                      rules={{
                        validate: (value) => {
                          if (regexPostal.test(value) === false) {
                            return "This field only allows characters [a-z] [0-9], it does not allow special characters.";
                          } else {
                            return true;
                          }
                        },
                      }}
                    />
                    <Controller
                      name={`addresses.${index}.streetAddress`}
                      control={control}
                      render={({ field }) => (
                        <TextField
                          className="p-2"
                          disabled={address.disabled}
                          InputLabelProps={{ shrink: true }}
                          {...field}
                          // error={regexPostal.test(field.value) ? false : true}
                          // helperText={
                          //   regexPostal.test(field.value)
                          //     ? ""
                          //     : "This field only allows characters [a-z] [0-9], it does not allow special characters."
                          // }
                          inputProps={{
                            "data-testid": `addresses.${index}.streetAddress`,
                          }}
                          required
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="Street"
                            />
                          }
                        />
                      )}
                    />
                    <Controller
                      name={`addresses.${index}.purposes`}
                      control={control}
                      render={({ field }) => (
                        <Autocomplete
                          {...field}
                          multiple
                          limitTags={4}
                          disabled={address.disabled}
                          className="p-2"
                          id={`addresses.${index}.purposes`}
                          data-testid={`addresses.${index}.purposes`}
                          options={
                            purposeList?.filter(
                              (item) => item.category.name === "Address"
                            ) || []
                          }
                          value={field?.value || []}
                          renderTags={(tagValue, getTagProps) =>
                            tagValue.map((option, index) => (
                              <Chip
                                label={option.name}
                                {...getTagProps({ index })}
                              />
                            ))
                          }
                          inputValue={
                            getValues(`addresses.${index}.purposes`)?.name || ""
                          }
                          disableCloseOnSelect
                          onChange={(e, options) => {
                            setValue(`addresses.${index}.purposes`, options);
                            onCountryChange(
                              e,
                              options,
                              `addresses.${index}.purposes`
                            );
                          }}
                          getOptionLabel={optionLabel}
                          renderOption={porpuseTypeOption}
                          renderInput={(params) =>
                            renderInputPurpose(
                              params,
                              `addresses.${index}.purposes`,
                              field.value
                            )
                          }
                          isOptionEqualToValue={getOptionSelected}
                        />
                      )}
                      rules={{
                        required: (
                          <Typography>
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Please, Select a Purpose"}
                            />
                          </Typography>
                        ),
                      }}
                    />
                    {/* {type !== "INSTITUTION" && address.name ? (
                      <Controller
                        name={`addresses.${index}.name`}
                        control={control}
                        render={({ field }) => (
                          <TextField
                            className="p-2"
                            InputLabelProps={{ shrink: true }}
                            disabled
                            {...field}
                            inputProps={{
                              "data-testid": `addresses.${index}.name`,
                            }}
                            required
                            label={
                              <FormattedMessage
                                id="none"
                                defaultMessage="Institution"
                              />
                            }
                          />
                        )}
                      />
                    ) : (
                      <></>
                    )} */}
                  </div>
                </div>
                {/* <div className="grid grid-cols-1">
                  <Controller
                    name={`addresses.${index}.default`}
                    control={control}
                    render={({ field }) => (
                      <FormControlLabel
                        control={
                          <Checkbox
                            {...field}
                            data-testid={`addresses.${index}.default`}
                            className="x-6"
                            checked={field.value}
                            // disabled={field.value === false}
                          />
                        }
                        label={
                          <Typography className="font-ebs">
                            <FormattedMessage
                              id="none"
                              defaultMessage="Primary Address"
                            />
                          </Typography>
                        }
                      />
                    )}
                  />
                  <div>
                    {index != -1 && (
                      <Tooltip
                        placement="top"
                        title={
                          <Typography className="font-ebs text-md">
                            <FormattedMessage
                              id="none"
                              defaultMessage="Remove address"
                            />
                          </Typography>
                        }
                        arrow
                      >
                        <Button
                          data-testid="remove"
                          className="fill-current text-red-400"
                          onClick={() =>
                            rAddress({ defaultAddress: address.default, index })
                          }
                          startIcon={<Close />}
                        >
                          <FormattedMessage id="none" defaultMessage="Remove" />
                        </Button>
                      </Tooltip>
                    )}
                  </div>
                </div> */}
              </div>
              <br />
            </Fragment>
          ))}
        </div>
      </Box>
    );
  }
);
// Type and required properties
AddressesAtom.propTypes = {
  control: PropTypes.object.isRequired,
  errors: PropTypes.object,
  setValue: PropTypes.func.isRequired,
  getValues: PropTypes.func.isRequired,
};
// Default properties
AddressesAtom.defaultProps = {};

export default AddressesAtom;
