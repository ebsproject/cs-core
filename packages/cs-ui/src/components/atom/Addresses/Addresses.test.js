import Addresses from "./Addresses";
import React from "react";
import { cleanup, fireEvent, screen, within } from "@testing-library/react";
import render from "utils/test/mockWapper";
import { useForm } from "react-hook-form";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

const Component = () => {
  const {
    control,
    formState: { errors },
    setValue,
    getValues,
  } = useForm();
  return (
    <form>
      <Addresses
        control={control}
        errors={errors}
        setValue={setValue}
        getValues={getValues}
      />
    </form>
  );
};

test("Addresses is in the DOM", () => {
  render(<Component />, {});
  expect(screen.getByTestId("AddressesTestId")).toBeInTheDocument();
});

test("Addresses is adding new Address section", async () => {
  render(<Component />, {});
  const addButton = screen.getByTestId("add");
  fireEvent.click(addButton);
  expect(screen.getByTestId("addresses.0.id")).toBeInTheDocument();
});

test("Addresses is removing Address section", async () => {
  render(<Component />, {});
  const addButton = screen.getByTestId("add");
  fireEvent.click(addButton);
  expect(screen.getByTestId("addresses.0.id")).toBeInTheDocument();
  fireEvent.click(addButton);
  expect(screen.getByTestId("addresses.1.id")).toBeInTheDocument();
  const removeButton = screen.getByTestId("remove");
  fireEvent.click(removeButton);
});

test("Every addresses component is handling data correctly", async () => {
  render(<Component />, {
    preloadedState: {
      persist: {
        countryList: [{ id: 1, name: "Mexico" }],
        purposeList: [{ id: 1, name: "mail", category: { name: "Address" } }],
      },
    },
  });
  const addButton = screen.getByTestId("add");
  fireEvent.click(addButton);
  const id = screen.getByTestId("addresses.0.id");
  const country = screen.getByTestId("addresses.0.country");
  const location = screen.getByTestId("addresses.0.location");
  const region = screen.getByTestId("addresses.0.region");
  const zipCode = screen.getByTestId("addresses.0.zipCode");
  const streetAddress = screen.getByTestId("addresses.0.streetAddress");
  const purpose = screen.getByTestId("addresses.0.purpose");
  const checkbox = screen.getByTestId("addresses.0.default");
  expect(id).toHaveValue("");
  country.focus();
  const countryInput = within(country).getByRole("textbox");
  fireEvent.change(countryInput, {
    target: { value: "Mexico" },
  });
  fireEvent.keyDown(country, { key: "ArrowDown" });
  fireEvent.keyDown(country, { key: "Enter" });
  expect(countryInput).toHaveValue("Mexico");
  location.focus();
  fireEvent.change(location, { target: { value: "Michoacan" } });
  expect(location).toHaveValue("Michoacan");
  region.focus();
  fireEvent.change(region, { target: { value: "Patzcuaro" } });
  expect(region).toHaveValue("Patzcuaro");
  zipCode.focus();
  fireEvent.change(zipCode, { target: { value: "61800" } });
  expect(zipCode).toHaveValue("61800");
  streetAddress.focus();
  fireEvent.change(streetAddress, {
    target: { value: "Iturbide Street, col. centro, S/N" },
  });
  expect(streetAddress).toHaveValue("Iturbide Street, col. centro, S/N");
  purpose.focus();
  const purposeInput = within(purpose).getByRole("textbox");
  fireEvent.change(purposeInput, {
    target: { value: "mail" },
  });
  fireEvent.keyDown(purpose, { key: "ArrowDown" });
  fireEvent.keyDown(purpose, { key: "Enter" });
  expect(purposeInput).toHaveValue("mail");
  checkbox.focus();
  fireEvent.click(checkbox);
  const inputCheck = within(checkbox).getByRole("checkbox");
  expect(inputCheck).toBeChecked();
});
