import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { showMessage } from "store/modules/message";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
const { Button, Typography, Box } = Core;
import { useDispatch, useSelector } from "react-redux";
import { createHierarchy } from "store/modules/CRM";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const AssignContactAtom = React.forwardRef(
  ({ rowSelected, refresh, newObjectInstitution, contacts }, ref) => {
    const [open, setOpen] = useState(false);
    const dispatch = useDispatch();
    // console.log(contacts);
    const { success } = useSelector(({ crm }) => crm);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    var arrayContact = rowSelected;
    var newObjectContact = Object.assign({}, arrayContact);

    const handleClickOpen = () => {
      setOpen(true);
    };

    function handleClose() {
      setOpen(false);
    }
    const actionButton = () => {
      const id = contacts.filter((item) => item.id === newObjectContact.id);
      if (id.length > 0) {
        dispatch(
          showMessage({
            message: "This contact is already assigned to this institution",
            variant: "warning",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } else {
        createHierarchy({
          contactId: newObjectContact.id,
          institutionId: newObjectInstitution.id,
          principalContact: false,
        })(dispatch);
      }
    };
    return (
      /**
       * @prop data-testid: Id to use inside AssignContact.test.js file.
       */
      <div data-testid={"AssignContactTestId"} ref={ref}>
        <Button
          aria-label="add-contact"
          color="primary"
          onClick={actionButton}
          disabled={rowSelected ? false : true}
        >
          <Typography className="font-ebs text-black">
            <FormattedMessage id="none" defaultMessage="Select contact" />
          </Typography>
        </Button>
        {/* <Dialog
          fullWidth
          keepMounted
          maxWidth="sm"
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Assign Contact" />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id="none"
                defaultMessage="Do you want to relate this contact with the Institution?"
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <Typography variant="button" color="secondary">
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Typography>
            </Button>
            <Button onClick={actionButton}>
              <Typography variant="button">
                <FormattedMessage id="none" defaultMessage="Confirm" />
              </Typography>
            </Button>
          </DialogActions>
        </Dialog> */}
      </div>
    );
  }
);
// Type and required properties
AssignContactAtom.propTypes = {};
// Default properties
AssignContactAtom.defaultProps = {};

export default AssignContactAtom;
