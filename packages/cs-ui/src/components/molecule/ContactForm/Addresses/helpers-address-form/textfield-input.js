import { Controller, useFormContext } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;

const TextfieldInput = ({ name, label, rules, rows }) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();
  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <TextField
          multiline
          rows={rows || 1}
          variant={"outlined"}
          fullWidth
          InputLabelProps={{ shrink: true }}
          {...field}
          error={!!errors[name]}
          helperText={errors[name] ? errors[name].message : ""}
          label={<FormattedMessage id="none" defaultMessage={label} />}
          data-testid={`testid-${name}`}
        />
      )}
      rules={rules}
    />
  );
};
export default TextfieldInput;
