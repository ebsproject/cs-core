import { Core, Icons } from "@ebs/styleguide";
const { Typography, Icon } = Core;
import { FormattedMessage } from "react-intl";
const { Star } = Icons;
export const columns = [
  { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Default" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return (
        <Typography variant="body1">
          {value ? "Yes" : "No"}{" "}
          {value && (
            <Icon aria-label="default-addresses" color="primary">
              <Star />
            </Icon>
          )}
        </Typography>
      );
    },
    accessor: "default",
    width: 100,
    filter: true,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Address Institution" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return (
        <Typography variant="body1">
          {value ? "Yes " : "No"} {`${value || ""}`}
        </Typography>
      );
    },
    accessor: "name",
    width: 200,
    filter: true,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Street Address" />
      </Typography>
    ),
    accessor: "streetAddress",
    width: 300,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Location" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return <Typography variant="body1">{value}</Typography>;
    },
    accessor: "location",
    width: 200,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Region" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return <Typography variant="body1">{value}</Typography>;
    },
    accessor: "region",
    width: 200,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Country" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return <Typography variant="body1">{value}</Typography>;
    },
    accessor: "country.name",
    width: 200,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Zip Code" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return <Typography variant="body1">{value}</Typography>;
    },
    accessor: "zipCode",
    width: 200,
  },
  {
    Header: (
      <Typography variant="h6" color="primary">
        <FormattedMessage id="none" defaultMessage="Purposes" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return (
        <Typography variant="body1">
          {value.map((item) => item.name).join(",")}
        </Typography>
      );
    },
    accessor: "purposes",
    width: 200,
  },
];
