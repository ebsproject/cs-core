import AddressesList from './AddressesList';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('AddressesList is in the DOM', () => {
  render(<AddressesList></AddressesList>)
  expect(screen.getByTestId('AddressesListTestId')).toBeInTheDocument();
})
