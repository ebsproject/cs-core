import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const {
  List,
  Box,
  Card,
  CardContent,
  Dialog,
  DialogContent,
  CircularProgress,
} = Core;
import {
  FIND_CONTACT,
  FIND_CONTACT_LIST,
  FIND_CONTACT_INSTITUTIONS,
} from "utils/apollo/gql/crm";
import { client } from "utils/apollo";
import AddressesFormAtom from "../AddressesForm/AddressesForm";
import { removeAddress } from "store/modules/CRM";
import { useDispatch, useSelector } from "react-redux";
import ViewAddressList from "../ViewListComponent/ViewAddressList";
import { useContactContext } from "corecontext";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const AddressesListAtom = React.forwardRef(
  ({ type, method, dataContact, updateData }, ref) => {
    const dispatch = useDispatch();
    const { success } = useSelector(({ crm }) => crm);
    const [open, setOpen] = React.useState(false);
    const [dataUpdate, setDataUpdate] = React.useState({});
    const { refresh, setRefresh } = useContactContext();
    const [dataAddresses, setDataAddresses] = React.useState(null);
    const [dataAddressesInstitution, setDataAddressesInstitution] =
      useState(null);
    const [showComponent, setShowComponent] = useState(false);

    useEffect(() => {
      setDataAddresses(null);
      setDataAddressesInstitution(null);
      const fetchAddresses = async () => {
        try {
          const { data } = await client.query({
            query:
              type === "INSTITUTION" ? FIND_CONTACT_INSTITUTIONS : FIND_CONTACT,
            variables: {
              id: Number(dataContact.id),
            },
            fetchPolicy: "no-cache",
          });
          setDataAddresses(data.findContact);
          fetch();
        } catch (e) {}
      };

      const fetch = async () => {
        try {
          const { data } = await client.query({
            query: FIND_CONTACT_LIST,
            variables: {
              filters: [
                { col: "category.name", mod: "EQ", val: "Institution" },
              ],
            },
            fetchPolicy: "no-cache",
          });
          setDataAddressesInstitution(data.findContactList.content);
        } catch (e) {}
      };
      fetchAddresses();
    }, [refresh]);
    if (!dataAddresses || !dataAddressesInstitution)
      return (
        <Box display="flex" justifyContent="center" alignItems="center">
          <CircularProgress color="primary" />
        </Box>
      );

    if (type !== "INSTITUTION") {
      let dataInstitution = dataAddresses.contactHierarchies;
      let newContactListHerarchy = [];
      let newDefaultValuesAddress = [];
      let newArrayAddress = [];
      let newArrayInstitutionAddress = [];

      if (dataInstitution !== undefined) {
        // InstitutionInput
        newContactListHerarchy.push(
          dataInstitution.map((item) => ({
            id: item.institution.id,
            name: item.institution.institution.commonName,
            addresses: item.institution.addresses,
          }))
        );
      }

      // InstitutionAddress
      newArrayInstitutionAddress.push(
        dataAddressesInstitution.map((item) => ({
          id: item.institution.id,
          name: item.institution.commonName,
          addresses: item.addresses,
        }))
      );

      newArrayInstitutionAddress[0].map((item) => {
        newArrayAddress.push(
          item.addresses.map((i) => ({
            name: item.name,
            id: i.id,
            country: i.country,
            default: i.default,
            location: i.location,
            purposes: i.purposes,
            region: i.region,
            streetAddress: i.streetAddress,
            zipCode: i.zipCode,
          }))
        );
      });

      let idAddressContact = dataAddresses.addresses.map((item) => item.id);
      idAddressContact.map((idContactAddress) => {
        newArrayAddress.map((addressIntitution) => {
          let dataContact = addressIntitution.filter(
            (i) => i.id === idContactAddress
          );
          if (dataContact.length > 0) {
            newDefaultValuesAddress.push(dataContact[0]);
          }
        });
      });
      for (
        let element = 0;
        element < dataAddresses.addresses.length;
        element++
      ) {
        for (let value = 0; value < newDefaultValuesAddress.length; value++) {
          if (
            dataAddresses.addresses[element].id ===
            newDefaultValuesAddress[value].id
          ) {
            Object.assign(dataAddresses.addresses[element], {
              name: newDefaultValuesAddress[value].name,
              disabled: true,
            });
          }
        }
      }
    }

    const handleClickOpen = (option) => {
      setOpen(true);
      setDataUpdate(option);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const deleteAddress = async (option) => {
      await removeAddress({ id: option.id, idUser: dataContact.id })(dispatch);
      setRefresh(!refresh);
      updateData()
    };

    return (
      <div>
        <br />
        <Card>
          <CardContent>
            <List sx={{ width: "100%", bgcolor: "background.paper" }}>
              <ViewAddressList
                dataAddresses={dataAddresses}
                deleteAddress={deleteAddress}
                handleClickOpen={handleClickOpen}
                type={type}
                dataContact={dataContact}
              />
            </List>
          </CardContent>
        </Card>
        <Dialog
          fullWidth
          maxWidth="md"
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogContent dividers>
            <AddressesFormAtom
              type={type}
              dataUpdate={dataUpdate}
              method={method}
              setOpen={setOpen}
              dataContact={dataContact}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
AddressesListAtom.propTypes = {};
// Default properties
AddressesListAtom.defaultProps = {};

export default AddressesListAtom;
