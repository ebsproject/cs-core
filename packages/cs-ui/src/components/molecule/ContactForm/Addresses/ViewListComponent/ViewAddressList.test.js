import ViewAddressList from "./ViewAddressList";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

test("ViewAddressList is in the DOM", () => {
  render(<ViewAddressList></ViewAddressList>);
  expect(screen.getByTestId("ViewAddressListTestId")).toBeInTheDocument();
});
