import React, { Fragment, useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { ListItem, ListItemText, IconButton, Icon, Box } = Core;
const { Edit, Delete, Star } = Icons;
import { RBAC } from "@ebs/layout";
import { columns } from "../AddressesList/columns";
import { EbsGrid } from "@ebs/components";
import { client } from "utils/apollo";
import { FIND_USER } from "utils/apollo/gql/userManagement";
import { FIND_CONTACT } from "utils/apollo/gql/crm";

const ViewAddressList = React.forwardRef(
  (
    { type, dataAddresses, handleClickOpen, deleteAddress, dataContact },
    ref
  ) => {
    const [addressList, setAddressList] = useState(null);
    useEffect(() => {
      let addresses = [...dataAddresses.addresses]
      let _addressList = addresses.sort((a, b) => {
        if (a.default === true && b.default !== true) {
          return -1;
        }
        if (a.default !== true && b.default === true) {
          return 1;
        }
        if (a.default === false && b.default !== false) {
          return -1;
        }
        if (a.default !== false && b.default === false) {
          return 1;
        }
        if (!a.name && b.name) {
          return 1;
        }
        if (a.name && !b.name) {
          return -1;
        }
        return 0;
      });
      setAddressList(_addressList);
    }, []);
    const rowActions = (option, refresh) => {
      return (
        <Box className="flex flex-row flex-nowrap gap-2">
          {option.default === false && (
            <>
              <RBAC
                allowedAction={"Modify"}
                product={"CRM"}
                domain={"Core System"}
              >
                <IconButton
                  aria-label="edit-addresses"
                  color="primary"
                  disabled={option.name ? true : false}
                  onClick={() => handleClickOpen(option)}
                >
                  <Edit />
                </IconButton>
              </RBAC>
              <RBAC
                allowedAction={"Delete"}
                product={"CRM"}
                domain={"Core System"}
              >
                <IconButton
                  aria-label=""
                  color="primary"
                  onClick={() => deleteAddress(option, refresh)}
                >
                  <Delete />
                </IconButton>
              </RBAC>
            </>
          )}
        </Box>
      );
    };
    const fetch = ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        try {
          client
            .query({
              query: FIND_CONTACT,
              variables: { id: Number(dataContact.id) },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                const addressList = data.findContact.addresses.sort(
                  (a, b) => b.default - a.default
                );
                resolve({
                  elements: data.findContact.addresses.length,
                  data: addressList,
                  pages: 1,
                });
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    };
    if (!addressList) return "Loading...";
    return (
      <EbsGrid
        id="AddressGridList"
        toolbar={true}
        columns={columns}
        rowactions={rowActions}
        fetch={fetch}
        height="60vh"
        csvfilename="AdditionalInformation"
        disabledViewConfiguration
        disabledViewDownloadCSV
      />
    );
    //     if (addressList.length > 0) {
    //       return addressList.map((option, index) => (
    //         <ListItem key={index.id} disableGutters>
    //           {option.default === false ? (
    //             <>
    //               <RBAC
    //                 allowedAction={"Modify"}
    //                 product={"CRM"}
    //                 domain={"Core System"}
    //               >
    //                 <IconButton
    //                   aria-label="edit-addresses"
    //                   color="primary"
    //                   disabled={option.name}
    //                   onClick={() => handleClickOpen(option)}
    //                 >
    //                   <Edit />
    //                 </IconButton>
    //               </RBAC>
    //               <br />
    //               <RBAC
    //                 allowedAction={"Delete"}
    //                 product={"CRM"}
    //                 domain={"Core System"}
    //               >
    //                 <IconButton
    //                   aria-label=""
    //                   color="primary"
    //                   onClick={() => deleteAddress(option)}
    //                 >
    //                   <Delete />
    //                 </IconButton>
    //               </RBAC>

    //               <br />
    //             </>
    //           ) : null}
    //           <br />
    //           {type === "INSTITUTION" || option.name === undefined ? (
    //             <ListItemText
    //               primary={
    //                 " " +
    //                 option.fullAddress +
    //                 " ( " +
    //                 option.purposes.map((item) => item.name) +
    //                 " ) "
    //               }
    //             />
    //           ) : (
    //             <ListItemText
    //               primary={
    //                 "(" +
    //                 (option.name ? option.name : "") +
    //                 ")" +
    //                 " " +
    //                 option.fullAddress +
    //                 " ( " +
    //                 option.purposes.map((item) => item.name) +
    //                 " ) "
    //               }
    //             />
    //           )}
    //           <br />
    //           {option.default === true ? (
    //             <Icon aria-label="delete-addresses" color="primary">
    //               <Star />
    //             </Icon>
    //           ) : null}
    //         </ListItem>
    //       ));
    //     } else {
    //       return "There are no associated addresses";
    //     }
  }
);

export default ViewAddressList;
