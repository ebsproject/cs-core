import AddressesForm from './AddressesForm';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('AddressesForm is in the DOM', () => {
  render(<AddressesForm></AddressesForm>)
  expect(screen.getByTestId('AddressesFormTestId')).toBeInTheDocument();
})
