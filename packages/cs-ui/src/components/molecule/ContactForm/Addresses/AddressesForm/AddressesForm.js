import React, { useState, useEffect } from "react";
import { useForm, FormProvider } from "react-hook-form";
import { useSelector, useDispatch } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { modifyAddress } from "store/modules/CRM";
import { Core, Icons } from "@ebs/styleguide";
const { Box, Button, Typography, Grid } = Core;
const { Cancel, CheckCircle } = Icons;
import { clientRest } from "utils/axios/axios";
import { useContactContext } from "corecontext";
import DropdownSelect from "../helpers-address-form/dropdown-select";
import TextfieldInput from "../helpers-address-form/textfield-input";
import FormHeader from "../helpers-address-form/form-header";

const AddressesFormAtom = React.forwardRef(
  (
    {
      method,
      setOpen,
      dataContact,
      dataUpdate,
      type,
      setvalueAddresses,
      conditionMethod,
      setDataAddressDefault,
      coditionComponent,
      updateData,
    },
    ref
  ) => {
    const { countryList, purposeList } = useSelector(({ persist }) => persist);
    const [dataSequence, setDataSequence] = useState(null);
    const { refresh, setRefresh } = useContactContext();

    const dispatch = useDispatch();
    const formMethods = useForm({
      defaultValues:
        method === "POST"
          ? {
              id: 0,
              location: "",
              region: "",
              ZipCode: "",
              streetAddress: "",
              countryId: "",
              purposes: [],
              additionalStreetAddress: "",
            }
          : dataUpdate,
    });
    const { watch, handleSubmit } = formMethods;
    const watchAllFields = watch();

    let updatedAddressTemplate = "";
    if (dataSequence !== null) {
      const secuenceFields = dataSequence?.sort(
        (a, b) => a.position - b.position
      );
      secuenceFields.forEach((secuence) => {
        switch (secuence.name) {
          case "address-streetAddress":
            watchAllFields.streetAddress === undefined ||
            watchAllFields.streetAddress === ""
              ? ""
              : (updatedAddressTemplate += watchAllFields.streetAddress);
            break;
          case "colon":
            updatedAddressTemplate += ",";
            break;
          case "blank":
            updatedAddressTemplate += "  ";
            break;
          case "address-zipCode":
            watchAllFields.zipCode === undefined ||
            watchAllFields.zipCode === null
              ? ""
              : (updatedAddressTemplate += watchAllFields.zipCode);
            break;
          case "address-region":
            watchAllFields.region === undefined ||
            watchAllFields.region === null
              ? ""
              : (updatedAddressTemplate += watchAllFields.region);
            break;
          case "address-location":
            watchAllFields?.country?.name === undefined
              ? ""
              : (updatedAddressTemplate += watchAllFields?.country?.name);
            break;
          default:
            updatedAddressTemplate += "";
        }
      });
    }
    useEffect(() => {
      const generateFullAddress = async () => {
        try {
          const { data } = await clientRest.get(`sequence/8`);
          setDataSequence(data.segments);
        } catch (error) {
          return error.toString();
        }
      };
      generateFullAddress();
    }, []);
    const onSubmit = async (data, e) => {
      switch (type) {
        case "PERSON":
          {
            const contact = {
              id: dataContact.id,
              category: dataContact.category,
              addresses: {
                default: data.default === undefined ? false : data.default,
                location: data.location,
                region: data.region,
                streetAddress: data.streetAddress,
                zipCode: data.zipCode,
                countryId: data.country.id,
                purposeIds: data.purposes.map((i) => Number(i.id)),
                additionalStreetAddress: data.additionalStreetAddress,
                id: Number(data.id) || 0,
              },
              purposeIds: dataContact.purposes.map((data) => Number(data.id)),
            };
            await modifyAddress(contact, dispatch);
          }
          break;
        case "INSTITUTION":
          if (conditionMethod === "NEWPOST") {
            const addresses = {
              default: true,
              location: data.location,
              region: data.region,
              streetAddress: data.streetAddress,
              zipCode: data.zipCode,
              countryId: data.country.id,
              purposeIds: data.purposes.map((i) => Number(i.id)),
              additionalStreetAddress: data.additionalStreetAddress,
              id: Number(data.id) || 0,
            };
            setvalueAddresses(addresses);
            handleCancel();
          } else {
            const contact = {
              id: dataContact.id,
              category: dataContact.category,
              addresses: {
                default: dataContact.addresses.length === 0 ? true : false,
                location: data.location,
                region: data.region,
                streetAddress: data.streetAddress,
                zipCode: data.zipCode,
                countryId: data.country.id,
                purposeIds: data.purposes.map((i) => Number(i.id)),
                additionalStreetAddress: data.additionalStreetAddress,
                id: Number(data.id) || 0,
              },
              purposeIds: dataContact.purposes.map((data) => Number(data.id)),
              email: dataContact.officialEmail,
              institution: {
                commonName: dataContact.commonName,
                legalName: dataContact.legalName,
                cgiar: dataContact.isCgiar,
                cropIds:
                  dataContact.crop.length === 0
                    ? []
                    : dataContact.crop.map((item) => item.id),
              },
            };
            await modifyAddress(contact, dispatch);
            updateData();
          }
          break;
        case "USER":
          {
            const contact = {
              id: dataContact.id,
              category: { id: 1 },
              addresses: {
                default: data.default === undefined ? false : data.default,
                location: data.location,
                region: data.region,
                streetAddress: data.streetAddress,
                zipCode: data.zipCode,
                countryId: data.country.id,
                purposeIds: data.purposes.map((i) => Number(i.id)),
                additionalStreetAddress: data.additionalStreetAddress,
                id: Number(data.id) || 0,
              },
              purposeIds: dataContact.purposes.map((data) => Number(data.id)),
            };
            await modifyAddress(contact, dispatch);
          }
          break;
      }
      handleCancel();
    };

    const handleCancel = () => {
      setOpen(false);
      setTimeout(() => {
        setRefresh(!refresh);
      }, 2000);
    };
    if (coditionComponent === "OrganizationUnits") {
      setDataAddressDefault(updatedAddressTemplate);
    }

    return (
      <Box data-testid={"AddressesTestId"} ref={ref}>
        <FormProvider {...formMethods}>
          <form
            onSubmit={handleSubmit(onSubmit)}
            name="AddressForm"
            data-testid="AddressForm"
          >
            <FormHeader handleCancel={handleCancel} />
            <Grid container direction={"row"} spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <DropdownSelect
                  label={"Country"}
                  name={"country"}
                  options={countryList || []}
                  rules={{ required: "This field is required" }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextfieldInput
                  label={"Location"}
                  name={"location"}
                  rules={{ required: "This field is required" }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextfieldInput
                  label={"Region"}
                  name={"region"}
                  rules={{ required: "This field is required" }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextfieldInput
                  label={"Zip Code"}
                  name={"zipCode"}
                  rules={{ required: "This field is required" }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextfieldInput
                  rows={3}
                  label={"Street Address"}
                  name={"streetAddress"}
                  rules={{ required: "This field is required" }}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextfieldInput
                  rows={3}
                  label={"Street 2"}
                  name={"additionalStreetAddress"}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <DropdownSelect
                  multiple
                  label={"Purposes"}
                  name={"purposes"}
                  options={
                    purposeList?.filter(
                      (item) => item.category.name === "Address"
                    ) || []
                  }
                  rules={{
                    required: "Please, Select a Purpose",
                  }}
                />
              </Grid>
            </Grid>
          </form>
        </FormProvider>
        <div>
          <Typography className="flex-grow pt-8 font-ebs col-span-2 text-ebs-green-900 font-bold uppercase text-lg">
            <FormattedMessage id="none" defaultMessage="Full Address" />
          </Typography>
          <br />
          <h4>
            {updatedAddressTemplate === undefined ? "" : updatedAddressTemplate}
          </h4>
        </div>
      </Box>
    );
  }
);
// Type and required properties
AddressesFormAtom.propTypes = {};
// Default properties
AddressesFormAtom.defaultProps = {};

export default AddressesFormAtom;
