import React, { useEffect, memo, Fragment, useState } from "react";
import { EbsTabsLayout, Core, Icons, Lab, Styles } from "@ebs/styleguide";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
const { Button, TextField, Typography, Checkbox, Dialog, DialogContent } = Core;
import { useDispatch, useSelector } from "react-redux";
const { Autocomplete } = Lab;
import { client } from "utils/apollo";
import { modifyAddress } from "store/modules/CRM";
import { FIND_CONTACT, FIND_CONTACT_INSTITUTIONS } from "utils/apollo/gql/crm";
import AddressesFormAtom from "../Addresses/AddressesForm/AddressesForm";
import { useFormContext } from "react-hook-form";
import { useContactContext } from "corecontext";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FieldSecondaryAddressAtom = React.forwardRef(
  (
    {
      optionsAddress,
      method,
      contact,
      type,
      dataContact,
      setvalueAddresses,
      updateData,
    },
    ref
  ) => {
    const {
      getValues,
      formState: { errors },
    } = useFormContext();
    const { refresh, setRefresh } = useContactContext();
    let addressArray = [];
    const [open, setOpen] = React.useState(false);
    const [dataMethod, setDataMethod] = React.useState("");
    const [dataAddresses, setDataAddresses] = React.useState(null);
    const [selectAddresses, setselectAddresses] = React.useState(null);
    const [value, setValue] = useState(null);
    const dispatch = useDispatch();
    const { success } = useSelector(({ crm }) => crm);
    const addressOption = [];
    useEffect(() => {
      const fetchAddresses = async () => {
        return new Promise((resolve, reject) => {
          try {
            client
              .query({
                query:
                  type === "INSTITUTION"
                    ? FIND_CONTACT_INSTITUTIONS
                    : FIND_CONTACT,
                variables: {
                  id: Number(dataContact?.id),
                },
                fetchPolicy: "no-cache",
              })

              .then(({ data, errors }) => {
                if (errors) {
                  reject(errors);
                } else {
                  setDataAddresses(data?.findContact);
                }
              });
          } catch (e) {
            reject(e);
          } finally {
          }
        });
      };
      fetchAddresses();
    }, [dataContact, success]);

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
      setRefresh(!refresh);
    };

    addressArray.push({
      name: "Other",
      country: "",
      id: 0,
      location: "",
      purposes: [],
      streetAddress: "Other",
      region: "",
      zipCode: "",
    });

    if (optionsAddress !== undefined) {
      optionsAddress?.forEach((address) => {
        address?.addresses?.forEach((ele) => {
          addressArray.push({
            name: address.name,
            country: ele.country,
            id: ele.id,
            location: ele.location,
            purposes: ele.purposes,
            streetAddress: ele.streetAddress,
            region: ele.region,
            zipCode: ele.zipCode,
            disabled: true,
          });
        });
      });
    }

    if (contact !== undefined) {
      contact?.forEach((address) => {
        addressArray.push(address);
      });
    }

    var hash = {};
    const optionArray = addressArray.filter(function (current) {
      var exists = !hash[current.id];
      hash[current.id] = true;
      return exists;
    });

    /**
     * @param {Object} option
     * @param {Boolean} selected
     * @returns {Node}
     */
    const PrimaryAddressOption = (props, option, { selected }) => {
      const { key, ...restProps } = props;
      return (
        <li key={key} {...restProps}>
          <Checkbox checked={selected} />
          {type === "INSTITUTION" || option.name === undefined
            ? " " +
              (option?.streetAddress ? option?.streetAddress : "") +
              "  " +
              (option?.zipCode ? option?.zipCode : "") +
              "  " +
              (option?.location ? option?.location : "") +
              "  " +
              (option?.region ? option?.region : "")
            : " ( " +
              (option?.name ? option?.name : "") +
              " ) " +
              " " +
              (option?.streetAddress ? option?.streetAddress : "") +
              "  " +
              (option?.zipCode ? option?.zipCode : "") +
              "  " +
              (option?.location ? option?.location : "") +
              "  " +
              (option?.region ? option?.region : "") +
              " ( " +
              option.purposes.map((item) => item.name) +
              " ) "}
        </li>
      );
    };
    /**
     * @param {Object} option
     * @returns {Node}
     */
    const optionInstitutionAddressLabel = (option) =>
      type === "INSTITUTION" || option.name === undefined
        ? `${
            " " +
            (option?.streetAddress ? option?.streetAddress : "") +
            "  " +
            (option?.zipCode ? option?.zipCode : "") +
            "  " +
            (option?.location ? option?.location : "") +
            "  " +
            (option?.region ? option?.region : "")
          }`
        : `${
            " ( " +
            (option?.name ? option?.name : "") +
            " ) " +
            " " +
            (option?.streetAddress ? option?.streetAddress : "") +
            "  " +
            (option?.zipCode ? option?.zipCode : "") +
            "  " +
            (option?.location ? option?.location : "") +
            "  " +
            (option?.region ? option?.region : "") +
            " ( " +
            option?.purposes.map((item) => item.name) +
            " ) "
          }`;

    /**
     * @param {Object} params
     * @param {String} field
     * @returns {Node}
     */
    const renderInputPrimaryAddress = (params, field) => {
      const newInputProps = new Object();
      if (!getValues(field)) {
        Object.assign(newInputProps, { value: "" });
      }
      return (
        <TextField
          {...params}
          variant={"outlined"}
          inputProps={{
            ...params.inputProps,
            ...newInputProps,
          }}
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors[field])}
          error={Boolean(errors[field])}
          label={
            <Typography className="text-ebs">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Secondary Address"}
              />
            </Typography>
          }
          helperText={errors[field]?.message}
        />
      );
    };

    const AddAddressesToArray = (e, options) => {
      if (!options) return;
      e.preventDefault();
      if (options.name === "Other") {
        handleClickOpen();
        setDataMethod("POST");
      } else {
        setselectAddresses(options);
      }
    };

    const AddAddressesToControl = async () => {
      const addresses = [];
      if (selectAddresses) {
        switch (type) {
          case "PERSON":
            if (method === "POST") {
              addresses.push({
                default: false,
                location: selectAddresses.location,
                region: selectAddresses.region,
                streetAddress: selectAddresses.streetAddress,
                zipCode: selectAddresses.zipCode,
                countryId: selectAddresses.country.id,
                purposeIds: selectAddresses.purposes.map((i) => Number(i.id)),
                id: Number(selectAddresses.id) || 0,
              });
              setvalueAddresses(addresses[0]);
            } else {
              addresses.push({
                default: false,
                location: selectAddresses.location,
                region: selectAddresses.region,
                streetAddress: selectAddresses.streetAddress,
                zipCode: selectAddresses.zipCode,
                countryId: selectAddresses.country.id,
                purposeIds: selectAddresses.purposes.map((i) => Number(i.id)),
                id: Number(selectAddresses.id) || 0,
              });
              const contactData = {
                id: dataContact.id,
                category: dataContact.category,
                addresses: addresses,
                purposeIds: dataContact.purposes.map((data) => Number(data.id)),
              };
              await modifyAddress(contactData, dispatch);
              updateData();
              setTimeout(() => {
                setValue(null);
              }, 0);
            }
            break;
          case "INSTITUTION":
            if (method === "POST") {
              addresses.push({
                default: false,
                location: selectAddresses.location,
                region: selectAddresses.region,
                streetAddress: selectAddresses.streetAddress,
                zipCode: selectAddresses.zipCode,
                countryId: selectAddresses.country.id,
                purposeIds: selectAddresses.purposes.map((i) => Number(i.id)),
                id: Number(selectAddresses.id) || 0,
              });
              setvalueAddresses(addresses[0]);
            }
            break;
          case "USER":
            if (method === "POST") {
              addresses.push({
                default: false,
                location: selectAddresses.location,
                region: selectAddresses.region,
                streetAddress: selectAddresses.streetAddress,
                zipCode: selectAddresses.zipCode,
                countryId: selectAddresses.country.id,
                purposeIds: selectAddresses.purposes.map((i) => Number(i.id)),
                id: Number(selectAddresses.id) || 0,
              });
              setvalueAddresses(addresses[0]);
            } else {
              addresses.push({
                default: false,
                location: selectAddresses.location,
                region: selectAddresses.region,
                streetAddress: selectAddresses.streetAddress,
                zipCode: selectAddresses.zipCode,
                countryId: selectAddresses.country.id,
                purposeIds: selectAddresses.purposes.map((i) => Number(i.id)),
                id: Number(selectAddresses.id) || 0,
              });
              const contactData = {
                id: dataContact.id,
                category: { id: 1 },
                addresses: addresses,
                purposeIds: dataContact.purposes.map((data) => Number(data.id)),
              };
              await modifyAddress(contactData, dispatch);
              updateData();
              setTimeout(() => {
                setValue(null);
              }, 0);
            }
            break;
        }
      }
    };

    //Delete addresses no relation ship
    if (dataAddresses?.addresses) {
      if (dataAddresses?.addresses.length > 0) {
        for (var i = 0; i < optionArray.length; i++) {
          var igual = false;
          for (var j = 0; (j < dataAddresses?.addresses.length) & !igual; j++) {
            if (
              Number(optionArray[i]["id"]) ==
              Number(dataAddresses?.addresses[j]["id"])
            )
              igual = true;
          }
          if (!igual) addressOption.push(optionArray[i]);
        }
      } else {
        addressOption.push(optionArray[0]);
      }
    }

    return method === "POST" || method === "ASSIGN" ? null : (
      <div>
        <Autocomplete
          className="p-2"
          id="addresses"
          data-testid={"addresses"}
          options={method === "POST" ? optionArray : addressOption || []}
          onChange={AddAddressesToArray}
          getOptionLabel={optionInstitutionAddressLabel}
          renderOption={PrimaryAddressOption}
          renderInput={renderInputPrimaryAddress}
          isOptionEqualToValue={(option, value) => option.id === value.id}
          value={value}
        />

        {type === "INSTITUTION" ? null : (
          <Button
            onClick={AddAddressesToControl}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Add New Address"} />
          </Button>
        )}
        <Dialog
          fullWidth
          maxWidth="md"
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogContent dividers>
            <AddressesFormAtom
              type={type}
              contact={contact}
              method={dataMethod}
              setOpen={setOpen}
              dataContact={dataContact}
              updateData={updateData}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
FieldSecondaryAddressAtom.propTypes = {};
// Default properties
FieldSecondaryAddressAtom.defaultProps = {};

export default FieldSecondaryAddressAtom;
