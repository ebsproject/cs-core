import FieldSecondaryAddress from './FieldSecondaryAddress';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FieldSecondaryAddress is in the DOM', () => {
  render(<FieldSecondaryAddress></FieldSecondaryAddress>)
  expect(screen.getByTestId('FieldSecondaryAddressTestId')).toBeInTheDocument();
})
