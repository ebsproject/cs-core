import ContactForm from "./ContactForm";
import {
  cleanup,
  fireEvent,
  screen,
  within,
  waitFor,
} from "@testing-library/react";
import React from "react";
import render from "utils/test/mockWapper";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";

afterEach(cleanup);

jest.mock("store/modules/DataPersist", () => ({
  findPurposeList: () => jest.fn(),
}));
const mockMutationContact = jest.fn();
jest.mock("store/modules/CRM", () => {
  return jest.fn().mockImplementation(() => {
    return { mutationContact: mockMutationContact };
  });
});
test("ContactForm is in the DOM", async () => {
  render(<ContactForm type={"PERSON"} method={"POST"} />, {});
  await waitFor(() => new Promise((res) => setTimeout(res, 100)));
  expect(screen.getByTestId("ContactFormTestId")).toBeInTheDocument();
});

test("ContactForm is handling information correctly", async () => {
  render(<ContactForm type={"PERSON"} method={"POST"} />, {
    preloadedState: {
      persist: {
        purposeList: [
          { id: 1, name: "Shipment", category: { name: "Person" } },
        ],
      },
    },
  });
  await waitFor(() => new Promise((res) => setTimeout(res, 100)));
  const add = screen.getByTestId("add");
  add.focus();
  fireEvent.click(add);
  const id = within(screen.getByTestId("id")).getByRole("textbox");
  const institutionId = within(screen.getByTestId("institutionId")).getByRole(
    "textbox"
  );
  const familyName = within(screen.getByTestId("familyName")).getByRole(
    "textbox"
  );
  const givenName = within(screen.getByTestId("givenName")).getByRole(
    "textbox"
  );
  const additionalName = within(screen.getByTestId("additionalName")).getByRole(
    "textbox"
  );
  const officialEmail = within(screen.getByTestId("officialEmail")).getByRole(
    "textbox"
  );
  const contactType = screen.getByTestId("contactType");
  const contactTypeInput = within(contactType).getByRole("textbox");
  const purpose = screen.getByTestId("purpose");
  const purposeInput = within(purpose).getByRole("textbox");
  const phoneId = within(screen.getByTestId("phones.0.id")).getByRole(
    "textbox"
  );
  const phoneValue = within(screen.getByTestId("phones.0.value")).getByRole(
    "textbox"
  );
  expect(id).toHaveValue("");
  expect(institutionId).toHaveValue("");
  familyName.focus();
  fireEvent.change(familyName, { target: { value: "Ortega" } });
  expect(familyName).toHaveValue("Ortega");
  givenName.focus();
  fireEvent.change(givenName, { target: { value: "Salvador" } });
  expect(givenName).toHaveValue("Salvador");
  additionalName.focus();
  fireEvent.change(additionalName, { target: { value: "Arai" } });
  expect(additionalName).toHaveValue("Arai");
  officialEmail.focus();
  fireEvent.change(officialEmail, {
    target: { event: {}, value: "s.ortega@gmail.com" },
  });
  expect(officialEmail).toHaveValue("s.ortega@gmail.com");
  contactType.focus();
  fireEvent.change(contactTypeInput, { target: { value: "Collaborator" } });
  fireEvent.keyDown(contactType, { key: "ArrowDown" });
  fireEvent.keyDown(contactType, { key: "Enter" });
  expect(contactTypeInput).toHaveValue("Collaborator");
  purpose.focus();
  fireEvent.change(purposeInput, { target: { value: "Shipment" } });
  fireEvent.keyDown(purpose, { key: "ArrowDown" });
  fireEvent.keyDown(purpose, { key: "Enter" });
  expect(purposeInput).toHaveValue("Shipment");
  expect(phoneId).toHaveValue("0");
  phoneValue.focus();
  fireEvent.change(phoneValue, { target: { value: "7896543218" } });
  expect(phoneValue).toHaveValue("(789) 654-3218");
});

test("ContactForm is saving Person information", async () => {
  render(<ContactForm type={"PERSON"} method={"POST"} />, {
    preloadedState: {
      persist: {
        purposeList: [
          { id: 1, name: "Shipment", category: { name: "Person" } },
        ],
      },
    },
  });
  await waitFor(() => new Promise((res) => setTimeout(res, 100)));
  const cancel = screen.getByTestId("cancel");
  const save = screen.getByTestId("save");
  const add = screen.getByTestId("add");
  add.focus();
  fireEvent.click(add);
  const id = within(screen.getByTestId("id")).getByRole("textbox");
  const institutionId = within(screen.getByTestId("institutionId")).getByRole(
    "textbox"
  );
  const familyName = within(screen.getByTestId("familyName")).getByRole(
    "textbox"
  );
  const givenName = within(screen.getByTestId("givenName")).getByRole(
    "textbox"
  );
  const additionalName = within(screen.getByTestId("additionalName")).getByRole(
    "textbox"
  );
  const officialEmail = within(screen.getByTestId("officialEmail")).getByRole(
    "textbox"
  );
  const contactType = screen.getByTestId("contactType");
  const contactTypeInput = within(contactType).getByRole("textbox");
  const purpose = screen.getByTestId("purpose");
  const purposeInput = within(purpose).getByRole("textbox");
  const phoneId = within(screen.getByTestId("phones.0.id")).getByRole(
    "textbox"
  );
  const phoneValue = within(screen.getByTestId("phones.0.value")).getByRole(
    "textbox"
  );
  expect(id).toHaveValue("");
  expect(institutionId).toHaveValue("");
  familyName.focus();
  fireEvent.change(familyName, { target: { value: "Ortega" } });
  expect(familyName).toHaveValue("Ortega");
  givenName.focus();
  fireEvent.change(givenName, { target: { value: "Salvador" } });
  expect(givenName).toHaveValue("Salvador");
  additionalName.focus();
  fireEvent.change(additionalName, { target: { value: "Arai" } });
  expect(additionalName).toHaveValue("Arai");
  officialEmail.focus();
  fireEvent.change(officialEmail, {
    target: { event: {}, value: "s.ortega@gmail.com" },
  });
  expect(officialEmail).toHaveValue("s.ortega@gmail.com");
  contactType.focus();
  fireEvent.change(contactTypeInput, { target: { value: "Collaborator" } });
  fireEvent.keyDown(contactType, { key: "ArrowDown" });
  fireEvent.keyDown(contactType, { key: "Enter" });
  expect(contactTypeInput).toHaveValue("Collaborator");
  purpose.focus();
  fireEvent.change(purposeInput, { target: { value: "Shipment" } });
  fireEvent.keyDown(purpose, { key: "ArrowDown" });
  fireEvent.keyDown(purpose, { key: "Enter" });
  expect(purposeInput).toHaveValue("Shipment");
  expect(phoneId).toHaveValue("0");
  phoneValue.focus();
  fireEvent.change(phoneValue, { target: { value: "7896543218" } });
  expect(phoneValue).toHaveValue("(789) 654-3218");
  fireEvent.click(save);
});

test("ContactForm is saving Institution information", async () => {
  render(<ContactForm type={"INSTITUTION"} method={"POST"} />, {
    preloadedState: {
      persist: {
        purposeList: [
          { id: 1, name: "Shipment", category: { name: "Institution" } },
        ],
      },
    },
  });
  await waitFor(() => new Promise((res) => setTimeout(res, 100)));
  const cancel = screen.getByTestId("cancel");
  const save = screen.getByTestId("save");
  const add = screen.getByTestId("add");
  add.focus();
  fireEvent.click(add);
  const id = within(screen.getByTestId("id")).getByRole("textbox");
  const institutionId = within(screen.getByTestId("institutionId")).getByRole(
    "textbox"
  );
  const commonName = within(screen.getByTestId("commonName")).getByRole(
    "textbox"
  );
  const legalName = within(screen.getByTestId("legalName")).getByRole(
    "textbox"
  );
  const officialEmail = within(screen.getByTestId("officialEmail")).getByRole(
    "textbox"
  );
  const contactType = screen.getByTestId("contactType");
  const contactTypeInput = within(contactType).getByRole("textbox");
  const purpose = screen.getByTestId("purpose");
  const purposeInput = within(purpose).getByRole("textbox");
  const phoneId = within(screen.getByTestId("phones.0.id")).getByRole(
    "textbox"
  );
  const phoneValue = within(screen.getByTestId("phones.0.value")).getByRole(
    "textbox"
  );
  expect(id).toHaveValue("");
  expect(institutionId).toHaveValue("");
  commonName.focus();
  fireEvent.change(commonName, { target: { value: "Cimmyt" } });
  expect(commonName).toHaveValue("Cimmyt");
  legalName.focus();
  fireEvent.change(legalName, { target: { value: "CIMMYT" } });
  expect(legalName).toHaveValue("CIMMYT");
  officialEmail.focus();
  fireEvent.change(officialEmail, {
    target: { event: {}, value: "s.ortega@gmail.com" },
  });
  expect(officialEmail).toHaveValue("s.ortega@gmail.com");
  contactType.focus();
  fireEvent.change(contactTypeInput, { target: { value: "Collaborator" } });
  fireEvent.keyDown(contactType, { key: "ArrowDown" });
  fireEvent.keyDown(contactType, { key: "Enter" });
  expect(contactTypeInput).toHaveValue("Collaborator");
  purpose.focus();
  fireEvent.change(purposeInput, { target: { value: "Shipment" } });
  fireEvent.keyDown(purpose, { key: "ArrowDown" });
  fireEvent.keyDown(purpose, { key: "Enter" });
  expect(purposeInput).toHaveValue("Shipment");
  expect(phoneId).toHaveValue("0");
  phoneValue.focus();
  fireEvent.change(phoneValue, { target: { value: "7896543218" } });
  expect(phoneValue).toHaveValue("(789) 654-3218");
  fireEvent.click(save);
});

test("ContactForm is Updating Person information", async () => {
  render(<ContactForm type={"PERSON"} method={"PUT"} />, {
    preloadedState: {
      persist: {
        contactTypeList: [
          { id: 1, name: "Collaborator", category: { name: "Person" } },
          { id: 2, name: "Ebs Member", category: { name: "Person" } },
        ],
        purposeList: [
          { id: 1, name: "mail", category: { name: "Person" } },
          { id: 2, name: "shipment", category: { name: "Person" } },
        ],
        defaultValues: {
          id: 1,
          institutionId: 1,
          phones: [{ id: 1, value: "(753) 789-4589" }],
          familyName: "Ortega",
          givenName: "Salvador",
          additionalName: "",
          officialEmail: "s.test@cgiar.org",
          contactType: [
            {
              id: 1,
              name: "Collaborator",
              category: { id: 1, name: "Institution" },
            },
          ],
          purpose: { id: 1, name: "mail" },
        },
      },
    },
  });
  await waitFor(() => new Promise((res) => setTimeout(res, 100)));
  const cancel = screen.getByTestId("cancel");
  const save = screen.getByTestId("save");
  const contactId = within(screen.getByTestId("id")).getByRole("textbox");
  const institutionId = within(screen.getByTestId("institutionId")).getByRole(
    "textbox"
  );
  const familyName = within(screen.getByTestId("familyName")).getByRole(
    "textbox"
  );
  const givenName = within(screen.getByTestId("givenName")).getByRole(
    "textbox"
  );
  const additionalName = within(screen.getByTestId("additionalName")).getByRole(
    "textbox"
  );
  const officialEmail = within(screen.getByTestId("officialEmail")).getByRole(
    "textbox"
  );
  const contactType = screen.getByTestId("contactType");
  const contactTypeInput = within(contactType).getByRole("textbox");
  const purpose = screen.getByTestId("purpose");
  const purposeInput = within(purpose).getByRole("textbox");
  const phoneId = within(screen.getByTestId("phones.0.id")).getByRole(
    "textbox"
  );
  const phoneValue = within(screen.getByTestId("phones.0.value")).getByRole(
    "textbox"
  );
  expect(contactId).toHaveValue("1");
  expect(institutionId).toHaveValue("1");
  expect(familyName).toHaveValue("Ortega");
  familyName.focus();
  fireEvent.change(familyName, { target: { value: "Lucas" } });
  expect(familyName).toHaveValue("Lucas");
  expect(givenName).toHaveValue("Salvador");
  givenName.focus();
  fireEvent.change(givenName, { target: { value: "Chava" } });
  expect(givenName).toHaveValue("Chava");
  expect(officialEmail).toHaveValue("s.test@cgiar.org");
  officialEmail.focus();
  fireEvent.change(officialEmail, {
    target: { event: {}, value: "s.ortega@gmail.com" },
  });
  expect(officialEmail).toHaveValue("s.ortega@gmail.com");
  contactType.focus();
  fireEvent.change(contactTypeInput, { target: { value: "Ebs Member" } });
  fireEvent.keyDown(contactType, { key: "ArrowDown" });
  fireEvent.keyDown(contactType, { key: "Enter" });
  expect(contactType).toHaveTextContent(/collaborator/i);
  expect(contactType).toHaveTextContent(/ebs member/i);
  expect(purposeInput).toHaveValue("mail");
  purpose.focus();
  fireEvent.change(purposeInput, { target: { value: "shipment" } });
  fireEvent.keyDown(purpose, { key: "ArrowDown" });
  fireEvent.keyDown(purpose, { key: "Enter" });
  expect(purposeInput).toHaveValue("shipment");
  expect(phoneId).toHaveValue("1");
  expect(phoneValue).toHaveValue("(753) 789-4589");
  phoneValue.focus();
});

test("ContactForm is Updating Institution information", async () => {
  render(<ContactForm type={"INSTITUTION"} method={"PUT"} />, {
    preloadedState: {
      persist: {
        contactTypeList: [
          { id: 1, name: "Collaborator", category: { name: "Institution" } },
          { id: 2, name: "Ebs Member", category: { name: "Institution" } },
        ],
        purposeList: [
          { id: 1, name: "mail", category: { name: "Institution" } },
          { id: 2, name: "shipment", category: { name: "Institution" } },
        ],
        defaultValues: {
          id: 1,
          institutionId: 1,
          phones: [{ id: 1, value: "(753) 789-4589" }],
          commonName: "cimmyt",
          legalName: "CIMMYT",
          principalContact: [{ id: 1, name: "Salvador" }],
          officialEmail: "cimmyt@cgiar.org",
          contactType: [
            {
              id: 1,
              name: "Collaborator",
              category: { id:1, name: "Institution" },
            },
          ],
          purpose: { id: 1, name: "mail" },
        },
      },
    },
  });
  await waitFor(() => new Promise((res) => setTimeout(res, 100)));
  const cancel = screen.getByTestId("cancel");
  const save = screen.getByTestId("save");
  const contactId = within(screen.getByTestId("id")).getByRole("textbox");
  const institutionId = within(screen.getByTestId("institutionId")).getByRole(
    "textbox"
  );
  const commonName = within(screen.getByTestId("commonName")).getByRole(
    "textbox"
  );
  const legalName = within(screen.getByTestId("legalName")).getByRole(
    "textbox"
  );
  const officialEmail = within(screen.getByTestId("officialEmail")).getByRole(
    "textbox"
  );
  const contactType = screen.getByTestId("contactType");
  const contactTypeInput = within(contactType).getByRole("textbox");
  const purpose = screen.getByTestId("purpose");
  const purposeInput = within(purpose).getByRole("textbox");
  const phoneId = within(screen.getByTestId("phones.0.id")).getByRole(
    "textbox"
  );
  const phoneValue = within(screen.getByTestId("phones.0.value")).getByRole(
    "textbox"
  );
  expect(contactId).toHaveValue("1");
  expect(institutionId).toHaveValue("1");
  expect(commonName).toHaveValue("cimmyt");
  commonName.focus();
  fireEvent.change(commonName, { target: { value: "Cimmyt" } });
  expect(commonName).toHaveValue("Cimmyt");
  expect(legalName).toHaveValue("CIMMYT");
  legalName.focus();
  fireEvent.change(legalName, { target: { value: "CIMMYT 2" } });
  expect(legalName).toHaveValue("CIMMYT 2");
  expect(officialEmail).toHaveValue("cimmyt@cgiar.org");
  officialEmail.focus();
  fireEvent.change(officialEmail, {
    target: { event: {}, value: "s.ortega@gmail.com" },
  });
  expect(officialEmail).toHaveValue("s.ortega@gmail.com");
  contactType.focus();
  fireEvent.change(contactTypeInput, { target: { value: "Ebs Member" } });
  fireEvent.keyDown(contactType, { key: "ArrowDown" });
  fireEvent.keyDown(contactType, { key: "Enter" });
  expect(contactType).toHaveTextContent(/collaborator/i);
  expect(contactType).toHaveTextContent(/ebs member/i);
  expect(purposeInput).toHaveValue("mail")
  purpose.focus();
  fireEvent.change(purposeInput, { target: { value: "shipment" } });
  fireEvent.keyDown(purpose, { key: "ArrowDown" });
  fireEvent.keyDown(purpose, { key: "Enter" });
  expect(purposeInput).toHaveValue("shipment");
  expect(phoneId).toHaveValue("1");
  expect(phoneValue).toHaveValue("(753) 789-4589");
  phoneValue.focus();
});
