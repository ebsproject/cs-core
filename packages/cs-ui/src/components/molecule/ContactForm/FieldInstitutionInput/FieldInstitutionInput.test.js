import FieldInstitutionInput from './FieldInstitutionInput';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FieldInstitutionInput is in the DOM', () => {
  render(<FieldInstitutionInput></FieldInstitutionInput>)
  expect(screen.getByTestId('FieldInstitutionInputTestId')).toBeInTheDocument();
})
