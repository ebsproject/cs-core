import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { useQuery } from "@apollo/client";
import { client } from "utils/apollo";
import { useDispatch } from "react-redux";
// CORE COMPONENTS
import { Core, Lab } from "@ebs/styleguide";
const { TextField, Typography, Checkbox, Button, Dialog, DialogContent, Chip } =
  Core;
const { Autocomplete } = Lab;
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";
import { assignInstitution, removeAddress } from "store/modules/CRM";
import { showMessage } from "store/modules/message";
import { Controller, useFormContext } from "react-hook-form";

const findInstitution = async (setIntitution, setReloading, setTotal) => {
  setReloading(true);
  let number = 1;
  const { data } = await client.query({
    query: FIND_CONTACT_LIST,
    variables: {
      page: { number: number, size: 100 },
      filters: [{ col: "category.name", mod: "EQ", val: "Institution" }],
      disjuncionFilters: true,
    },
    fetchPolicy: "no-cache",
  });
  number = data?.findContactList?.totalPages;
  setTotal(data?.findContactList?.totalElements);

  setIntitution((d) => [...d, ...data.findContactList.content]);
  if (number > 1) {
    for (let i = 1; i < number; i++) {
      const { data } = await client.query({
        query: FIND_CONTACT_LIST,
        variables: {
          page: { number: i + 1, size: 100 },
          filters: [{ col: "category.name", mod: "EQ", val: "Institution" }],
          disjuncionFilters: true,
        },
        fetchPolicy: "no-cache",
      });
      setIntitution((d) => [...d, ...data.findContactList.content]);
    }
  }
  setReloading(false);
};

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FieldInstitutionInputAtom = (
  { setOptionsAddress, defaultValues, method, setOptionInstitution },
  ref
) => {
  const {
    setValue,
    control,
    formState: { errors },
    getValues,
  } = useFormContext();
  const dispatch = useDispatch();
  const [institution, setIntitution] = useState([]);
  const [newContactList, setNewContactList] = useState(null);
  const [reloading, setReloading] = useState(false);
  const [total, setTotal] = useState(1);
  const institutionOption = getValues("institutionInput");
  let percentage = 0;

  if(institutionOption !== undefined){
    setOptionInstitution(institutionOption)
  }

  useEffect(() => {
    setIntitution([]);
    const fetchDataContact = async () => {
      await findInstitution(setIntitution, setReloading, setTotal);
    };
    fetchDataContact();
  }, [method]);
  useEffect(() => {
    //const contactList = institution;
    if (institution) {
      let _newContactList = [];
      _newContactList.push(
        institution.map((item) => ({
          id: item.id,
          name: item.institution.commonName,
          addresses: item.addresses,
        }))
      );      
      setNewContactList(_newContactList);
    }
  }, [institution]);
  percentage = Math.round((institution.length / total) * 100, 2);

  /**
   * @param {Object} options
   * @param {String} field
   */
  const getInstitutionSelected = (option, value) => option.id === value.id;
  /**
   * @param {Object} option
   * @returns {Node}
   */
  const optionLabelInstitution = (option) => `${option.name}`;

  /**
   * @param {Object} option
   * @param {Boolean} selected
   * @returns {Node}
   */
  const institutionTypeOption = (props, option, { selected }) => {
    const { key, ...restProps } = props;
    return (
      <li key={key} {...restProps}>
        <Checkbox checked={selected} />
        {option.name}
      </li>
    );
  };
  /**
   * @param {Object} params
   * @param {String} field
   * @returns {Node}
   */
  const renderInputInstitution = (params, field) => {
    return (
      <TextField
        variant={"outlined"}
        {...params}
        InputLabelProps={{ shrink: true }}
        focused={Boolean(errors[field])}
        error={Boolean(errors[field])}
        label={
          <Typography className="text-ebs">
            <FormattedMessage
              id={"none"}
              defaultMessage={
                reloading
                  ? `Loading Institution, ${percentage}% completed`
                  : "Institution"
              }
            />
            {` *`}
          </Typography>
        }
        helperText={errors[field]?.message}
      />
    );
  };

  const onInstitutionChange = (e, options, field) => {
    e.preventDefault();
    setValue(field, options);
    setOptionInstitution(options)
  };

  const handleDeleteInstitution = (options) => {
    const idDelete = options.id;
    const arrayAddress = defaultValues?.addresses;
    const deleteAddress = options.addresses.map((item) => item.id);

    const deleteChipsAddress = async () => {
      const arrayInstitution = getValues("institutionInput");
      const data = arrayInstitution.filter((item) => item.id !== idDelete);
      setValue("institutionInput", data);
      setOptionsAddress(data);
      if (method === "PUT") {
        for (let i = 0; i < deleteAddress.length; i++) {
          const item = deleteAddress[i];
          const filterAddress = arrayAddress.filter(
            (address) => address.id === item
          );
          if (filterAddress.length > 0) {
            await removeAddress({
              id: filterAddress[0]?.id,
              idUser: defaultValues?.id,
            })(dispatch);
          }
        }

        const institutionsData = defaultValues?.institutionInput.filter(
          (institution) => institution.id !== options.id
        );
        assignInstitution({
          institutionData: institutionsData,
          contactId: defaultValues.id,
          previousInstitution: defaultValues?.institutionInput,
        })(dispatch);
      }
    };

    if (deleteAddress?.length > 0 && method === "PUT") {
      const defaultAddress = arrayAddress.filter(
        (data) => data.default === true
      );

      deleteAddress.includes(defaultAddress[0].id) === true
        ? dispatch(
            showMessage({
              message:
                "For now we only remove the contact-Institution relationship. Failed to delete this address, as default addresses cannot be deleted, if you want to delete this address, please select another default option.",
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          )
        : deleteChipsAddress();
    } else {
      deleteChipsAddress();
    }
  };
  if (!newContactList) return "Loading....";
  return (
    <Controller
      name="institutionInput"
      control={control}
      render={({ field }) => (
        <Autocomplete
          {...field}
          multiple
          limitTags={6}
          id={"institutionInput"}
          data-testid={"institutionInput"}
          options={
            (newContactList &&
              newContactList.length > 0 &&
              newContactList[0]) ||
            []
          }
          disableCloseOnSelect
          onChange={(e, options) => {
            onInstitutionChange(e, options, "institutionInput");
            setOptionsAddress(options);
            // if (method === "PUT") {
            //   assignInstitution({
            //     institutionData: options,
            //     contactId: defaultValues.id,
            //     previousInstitution: defaultValues?.institutionInput,
            //   })(dispatch);
            // }
          }}
          renderTags={(tagValue, getTagProps) => {
            return tagValue.map((option, index) => {
              const { key, ...restProps } = getTagProps({ index });
              return (
                <Chip
                  onDelete={() => handleDeleteInstitution(option)}
                  key={key}
                  label={option.name}
                  {...restProps}
                />
              );
            });
          }}
          value={field?.value || []}
          isOptionEqualToValue={getInstitutionSelected}
          getOptionLabel={optionLabelInstitution}
          renderOption={institutionTypeOption}
          renderInput={(params) =>
            renderInputInstitution(params, "institutionInput")
          }
        />
      )}
      rules={{
        required: "Please, Select a Institution",
      }}
    />
  );
};
// Type and required properties
FieldInstitutionInputAtom.propTypes = {};

export default FieldInstitutionInputAtom;
