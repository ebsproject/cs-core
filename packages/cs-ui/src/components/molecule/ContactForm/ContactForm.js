import React, { useEffect, memo, Fragment, useState, useRef } from "react";
import PropTypes from "prop-types";
import { Core, Lab } from "@ebs/styleguide";
import { useSelector, useDispatch } from "react-redux";
import { useForm, FormProvider } from "react-hook-form";
import { mutationContact } from "store/modules/CRM";
import {
  findPurposeList,
  findContact,
  findContactByAccount,
  contactProcess,
  findUser,
} from "store/modules/DataPersist";
import { useBouncer } from "utils/other/Bouncer";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
const { TextField, Typography, Box, CircularProgress, Grid, Autocomplete } =
  Core;
import { useNavigate } from "react-router-dom";
import { mutationUser } from "store/modules/UserManagement";
import FieldPrimaryAddresses from "components/molecule/ContactForm/FieldPrimaryAddresses/FieldPrimaryAddresses";
import FieldSecondaryAddressAtom from "components/molecule/ContactForm/FieldSecondaryAddress/FieldSecondaryAddress";
import { ContactTabs } from "./ContactTabs";
import FieldInstitutionInputAtom from "./FieldInstitutionInput/FieldInstitutionInput";
import { findCropList } from "components/organism/OrganizationUnits/fuctionsOrganizations";
import TextfieldInput from "./helpers/textfield-input";
import DropdownSelect from "./helpers/dropdown-select";
import ModalConfirm from "./helpers/modal-confirm";
import FormHeader from "./helpers/form-header";
import RadioSelect from "./helpers/radio-select";
import ModalValidation from "./helpers/modal-validation";
import { client } from "utils/apollo";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";

const ContactFormMolecule = React.forwardRef(({ type, method }, ref) => {
  const [valueRadio, setValueRadio] = useState(null);
  const [open, setOpen] = useState(false);
  const navigate = useNavigate();
  const { contactTypeList, defaultValues, purposeList, institutionId } =
    useSelector(({ persist }) => persist);
  const [optionsAddress, setOptionsAddress] = useState(null);
  const [createUser, setCreateUser] = useState("NO");
  const [valueAddresses, setValueAddresses] = useState(null);
  const [arrayInstitutionDelete, setArrayInstitutionDelete] = useState([]);
  const [clearFields, setClearFields] = useState(false);
  const [dataCropList, setDataCropList] = useState(null);
  const [optionInstitution, setOptionInstitution] = useState(null);
  const [validationContact, setValidationContact] = useState(false);
  const [memberOu, setMemberOu] = useState([]);
  const [refreshKey, setRefreshKey] = useState(0);
  const dispatch = useDispatch();
  const buttonRef = useRef(null);
  const genderType = [
    { id: "prefer not to say", name: "Prefer not to say" },
    { id: "non binary", name: "Non binary" },
    { id: "male", name: "Male" },
    { id: "female", name: "Female" },
    { id: "other", name: "Other" },
  ];
  const salutation = [
    { id: "mr", name: "Mr" },
    { id: "mrs", name: "Mrs" },
    { id: "ms", name: "Ms" },
    { id: "dr", name: "Dr" },
  ];
  useEffect(() => {
    const fetchDataCropList = async () => {
      await findCropList(setDataCropList);
    };
    fetchDataCropList();
  }, []);

  useEffect(() => {
    findPurposeList({ type: type != "INSTITUTION" ? "Person" : "Institution" })(
      dispatch
    );
  }, [type]);
  const formMethods = useForm({
    defaultValues:
      method === "PUT"
        ? { ...defaultValues }
        : {
            isAdmin: "user",
            institutionId: institutionId,
            addresses: [],
          },
  });
  const { handleSubmit, reset, setValue } = formMethods;

  useEffect(() => {
    if (defaultValues !== null) {
      let address =
        type === "INSTITUTION"
          ? defaultValues.addresses
          : defaultValues?.institutionInput;
      setOptionsAddress(address);
    } else {
      setOptionsAddress(null);
    }
  }, [defaultValues]);
  if (type === "USER") {
    defaultValues?.contactType.length === 0 || method === "POST"
      ? setValue("contactType", [
          { id: 9, name: "Ebs member", category: { name: "Person" } },
        ])
      : null;
  }
  useEffect(() => {
    if (createUser === "YES") {
      setValue("contactType", [
        { id: 9, name: "EBS member", category: { name: "Person" } },
      ]);
    } else {
      setValue("contactType", []);
    }
  }, [createUser]);

  const handleBusinessAccount = useBouncer((e) => {
    findContactByAccount({ userName: e.target.value }, type)(dispatch);
  }, 1000);
  const updateData = async () => {
    if (method === "PUT") {
      switch (type) {
        case "PERSON":
          {
            const { data } = await findContact({
              id: defaultValues?.id,
              type: "PERSON",
            });
            dispatch(contactProcess({ type, method, defaultValues: data }));
          }
          break;
        case "INSTITUTION":
          {
            const { data } = await findContact({
              id: defaultValues.id,
              type: "INSTITUTION",
            });
            dispatch(contactProcess({ type, method, defaultValues: data }));
          }
          break;
        case "USER":
          {
            const { data } = await findUser({ id: defaultValues?.userId });
            dispatch(contactProcess({ type, method, defaultValues: data }));
          }
          break;
      }
    }
  };
  if (type !== "INSTITUTION") {
    if (method === "POST") {
      setValue("gender", { name: "Prefer not to say" });
    }
  }
  useEffect(() => {
    if (method === "PUT") reset({ ...defaultValues });
    setValue("gender", defaultValues?.gender || "Prefer not to say");
    setValue("gender", defaultValues?.salutation || "Mr");
  }, [defaultValues]);
  useEffect(() => {
    if (method === "PUT") reset({ ...defaultValues });
    setValue("salutation", defaultValues?.salutation);
  }, [defaultValues]);

  const handleCancel = (e) => {
    e.preventDefault();
    if (type === "INSTITUTION") {
      navigate("/cs/crm", { index: 1 }); //TODO:: REVIEW the object passed to navigate
    } else if (type === "PERSON") {
      navigate("/cs/crm", { index: 0 }); //TODO:: REVIEW the object passed to navigate
    } else {
      navigate("/cs/usermanagement", { index: 0 }); //TODO:: REVIEW the object passed to navigate
    }
  };

  const onSubmit = async (data, e) => {
    // Validation to insert new values ​​into the address, this only happens when valueAddress exists
    if (valueAddresses) {
      if (
        (valueAddresses.countryId !== undefined &&
          valueAddresses.streetAddress !== "Other") ||
        (valueAddresses.streetAddress !== "Default" &&
          valueAddresses.purposeIds.length !== 0)
      ) {
        data.addresses.push(valueAddresses);
      }
    }
    type !== "INSTITUTION" && data.addresses.length === 0
      ? data.addresses.push({
          default: true,
          location: data.institutionInput[0].addresses[0].location,
          region: data.institutionInput[0].addresses[0].region,
          streetAddress: data.institutionInput[0].addresses[0].streetAddress,
          zipCode: data.institutionInput[0].addresses[0].zipCode,
          countryId: data.institutionInput[0].addresses[0].country.id,
          purposeIds: data.institutionInput[0].addresses[0].purposes.map(
            (item) => item.id
          ),
          id: data.institutionInput[0].addresses[0].id
            ? data.institutionInput[0].addresses[0].id
            : 0,
        })
      : null;

    //Validation to execute the ContactForm
    if (e.target.name === "ContactForm") {
      if (method === "POST" && type !== "INSTITUTION") {
        const { data: condition } = await client.query({
          query: FIND_CONTACT_LIST,
          variables: {
            filters: [
              { col: "person.givenName", mod: "EQ", val: data.givenName },
              { col: "person.familyName", mod: "EQ", val: data.familyName },
            ],
            disjunctionFilters: false,
          },
          fetchPolicy: "no-cache",
        });
        if (condition.findContactList.content.length > 0) {
          setValidationContact(true);
        } else {
          type === "USER"
            ? (await mutationUser({
                data,
                method,
                lastInst: defaultValues?.institutionInput,
                lastRoles: defaultValues?.role,
                lastSPs: defaultValues?.serviceProvider,
                active: defaultValues?.active,
                memberOu,
              })(dispatch),
              setOpen(true))
            : (await mutationContact({
                type,
                method,
                data,
                lastInst: defaultValues?.institutionInput,
                addresses: defaultValues?.addresses || [],
                createUser,
              })(dispatch),
              setOpen(true));
        }
      } else {
        type === "USER"
          ? (await mutationUser({
              data,
              method,
              lastInst: defaultValues?.institutionInput,
              lastRoles: defaultValues?.role,
              lastSPs: defaultValues?.serviceProvider,
              active: defaultValues?.active,
              memberOu,
            })(dispatch),
            setOpen(true))
          : (await mutationContact({
              type,
              method,
              data,
              lastInst: defaultValues?.institutionInput,
              addresses: defaultValues?.addresses || [],
              createUser,
            })(dispatch),
            setOpen(true));
      }
    }
    //Execute an action if validation Contact equals true
    if (validationContact === true) {
      type === "USER"
        ? (await mutationUser({
            data,
            method,
            lastInst: defaultValues?.institutionInput,
            lastRoles: defaultValues?.role,
            lastSPs: defaultValues?.serviceProvider,
            active: defaultValues?.active,
            memberOu,
          })(dispatch),
          setOpen(true))
        : (await mutationContact({
            type,
            method,
            data,
            lastInst: defaultValues?.institutionInput,
            addresses: defaultValues?.addresses || [],
            createUser,
          })(dispatch),
          setOpen(true));
      setValidationContact(false);
    }
    //Validation for Update data
    if (method === "PUT" && (type === "USER" || type === "PERSON")) {
      updateData();
    }
  };

  const handleClose = (e) => {
    setOpen(false);
    e.preventDefault();
    if (type === "USER") {
      navigate("/cs/usermanagement");
    } else {
      navigate("/cs/crm");
    }
  };
  const handleUpdate = () => {
    setOpen(false);
    if (method === "POST" || method === "ASSIGN") {
      reset({
        institutionId: 0,
        phones: [{ id: 0, value: "" }],
        addresses: [],
        id: 0,
        familyName: "",
        givenName: "",
        additionalName: "",
        commonName: "",
        legalName: "",
        institutionInput: [],
        principalContact: null,
        officialEmail: "",
        userName: "",
        purposes: [],
        contactType: [],
      });
      setClearFields(true);
      setOptionsAddress(null);
      setMemberOu([]);
      setRefreshKey((prev) => prev + 1);
    }
  };

  return (
    <Box data-testid={"ContactFormTestId"} ref={ref}>
      <ModalConfirm
        open={open}
        handleClose={handleClose}
        handleUpdate={handleUpdate}
        method={method}
      />
      <ModalValidation
        open={validationContact}
        method={method}
        setValidationContact={setValidationContact}
        buttonRef={buttonRef}
      />
      <FormProvider {...formMethods}>
        <form
          onSubmit={handleSubmit(onSubmit)}
          name="ContactForm"
          data-testid="personForm"
        >
          <FormHeader handleCancel={handleCancel} buttonRef={buttonRef} />
          <Grid container direction="row" spacing={2}>
            <Grid
              item
              xs={12}
              sm={12}
              md={6}
              lg={6}
              xl={6}
              sx={{ display: `${type === "USER" ? "block" : "none"}` }}
            >
              {type === "USER" && (
                <TextfieldInput
                  name={"userName"}
                  label={"User Name/Account"}
                  rules={{ required: `This field is required` }}
                  disabled={method === "PUT" ? true : false}
                />
              )}
            </Grid>
            {type != "INSTITUTION" && (
              <Fragment>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <DropdownSelect
                    name={"salutation"}
                    options={salutation}
                    label={"Salutation"}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <DropdownSelect
                    name={"gender"}
                    options={genderType}
                    label={"Gender"}
                    rules={{ required: "Please, Select a Gender" }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <TextfieldInput
                    name={"familyName"}
                    label={"Family Name"}
                    rules={{ required: `This field is required` }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <TextfieldInput
                    name={"givenName"}
                    label={"Given Name"}
                    rules={{ required: `This field is required` }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <TextfieldInput
                    name={"additionalName"}
                    label={"Additional Name"}
                  />
                </Grid>
              </Fragment>
            )}
            {type != "USER" && (
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <TextfieldInput
                  rules={{
                    required: "Official Email is required",
                    pattern: {
                      value:
                        /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/,
                      message: "⚠️ Enter a valid email address",
                    },
                  }}
                  name={"officialEmail"}
                  label={"Official Email"}
                />
              </Grid>
            )}
            <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
              <DropdownSelect
                multiple
                name={"contactType"}
                label={"Contact Type"}
                rules={{ required: "Please, Select one or more Contact Types" }}
                options={
                  contactTypeList?.filter((item) => {
                    if (type != "INSTITUTION") {
                      if (item.category.name === "Person") return item;
                    } else {
                      if (item.category.name === "Institution") return item;
                    }
                  }) || []
                }
              />
            </Grid>
            <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
              <DropdownSelect
                multiple
                label={"Purposes"}
                name={"purposes"}
                options={
                  purposeList?.filter((item) => {
                    if (type !== "INSTITUTION")
                      return item.category.name === "Person" ? item : null;
                    else
                      return item.category.name === "Institution" ? item : null;
                  }) || []
                }
                rules={{
                  required: "Please, Select a Purpose",
                }}
              />
            </Grid>
            {type != "INSTITUTION" && method !== "ASSIGN" && (
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <FieldInstitutionInputAtom
                  setOptionsAddress={setOptionsAddress}
                  defaultValues={{ ...defaultValues }}
                  method={method}
                  setOptionInstitution={setOptionInstitution}
                />
              </Grid>
            )}
            {type === "INSTITUTION" && (
              <Fragment>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <TextfieldInput
                    name={"commonName"}
                    label={"Common Name"}
                    rules={{ required: `This field is required` }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  <TextfieldInput
                    name={"legalName"}
                    label={"Legal Name"}
                    rules={{ required: `This field is required` }}
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                  {dataCropList && (
                    <DropdownSelect
                      multiple
                      label={"Crop"}
                      name={"crop"}
                      options={
                        dataCropList.filter(
                          (item) => item.name !== "Multi-crop"
                        ) || []
                      }
                    />
                  )}
                </Grid>
                {method === "PUT" && (
                  <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                    <DropdownSelect
                      multiple
                      options={[...defaultValues?.principalContact] || []}
                      label={"Principal Contact"}
                      name={"principalContact"}
                    />
                  </Grid>
                )}
              </Fragment>
            )}
            <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
              <FieldPrimaryAddresses
                clearFields={clearFields}
                setClearFields={setClearFields}
                method={method}
                contact={(defaultValues && [...defaultValues.addresses]) || []}
                valueAddresses={valueAddresses}
                optionsAddress={optionsAddress}
                setOptionsAddress={setOptionsAddress}
                setValueAddresses={setValueAddresses}
                type={type}
                dataContact={{ ...defaultValues }}
                updateData={updateData}
              />
            </Grid>
            {type === "USER" && (
              <Grid item xs={12} sm={12} md={12} lg={6} xl={6}>
                <RadioSelect
                  name={"isAdmin"}
                  options={[
                    { value: "user", label: "User" },
                    { value: "admin", label: "Admin" },
                  ]}
                />
              </Grid>
            )}
            {method === "PUT" && (
              <Grid item xs={12} sm={12} md={6} lg={6} xl={6}>
                <FieldSecondaryAddressAtom
                  method={method}
                  contact={[...defaultValues?.addresses]}
                  optionsAddress={optionsAddress}
                  setValueAddresses={setValueAddresses}
                  type={type}
                  dataContact={{ ...defaultValues }}
                  updateData={updateData}
                />
              </Grid>
            )}
          </Grid>
        </form>
        {type === "PERSON" && method === "POST" && (
          <Grid container direction="row">
            <Grid item xs={6} sm={6} md={6} lg={6} xl={6}>
              <Typography className="flex-grow font-ebs text-black font-bold text-md">
                <FormattedMessage
                  id="none"
                  defaultMessage="Would you like to make this person a user?"
                />
              </Typography>
              <Autocomplete
                defaultValue={createUser}
                onChange={(event, newValue) => {
                  setCreateUser(newValue);
                }}
                id="userCreate"
                options={["NO", "YES"]}
                renderInput={(params) => (
                  <TextField
                    variant={"outlined"}
                    fullWidth
                    {...params}
                    label=""
                  />
                )}
              />
            </Grid>
          </Grid>
        )}
        <Box className="col-span-2">
          <ContactTabs
            key={refreshKey}
            orientation="horizontal"
            defaultValues={defaultValues}
            type={type}
            valueRadio={valueRadio}
            setValueRadio={setValueRadio}
            method={method}
            arrayInstitutionDelete={arrayInstitutionDelete}
            setArrayInstitutionDelete={setArrayInstitutionDelete}
            optionInstitution={optionInstitution}
            updateData={updateData}
            setMemberOu={setMemberOu}
          />
        </Box>
      </FormProvider>
    </Box>
  );
});
// Type and required properties
ContactFormMolecule.propTypes = {
  type: PropTypes.oneOf(["PERSON", "INSTITUTION", "USER"]),
  method: PropTypes.oneOf(["POST", "PUT"]),
};
export default memo(ContactFormMolecule);
