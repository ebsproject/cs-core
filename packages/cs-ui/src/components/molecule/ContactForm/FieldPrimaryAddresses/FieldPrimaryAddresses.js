import React, { useState, useEffect, useRef } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { useDispatch } from "react-redux";
import { Core, Lab } from "@ebs/styleguide";
import { modifyAddress } from "store/modules/CRM";
import AddressesFormAtom from "../Addresses/AddressesForm/AddressesForm";
import { useFormContext } from "react-hook-form";

const { TextField, Typography, Checkbox, Button, Dialog, DialogContent } = Core;
const { Autocomplete } = Lab;
import { useContactContext } from "corecontext";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FieldAddressesAtom = React.forwardRef(
  (
    {
      optionsAddress,
      clearFields,
      setClearFields,
      method,
      contact,
      type,
      dataContact,
      setValueAddresses,
      updateData,
    },
    ref
  ) => {
    const {
      getValues,
      formState: { errors },
    } = useFormContext();
    const { refresh, setRefresh } = useContactContext();
    const [open, setOpen] = React.useState(false);
    const [dataMethod, setDataMethod] = React.useState("");
    const [rendField, setRendField] = React.useState(0);
    const dispatch = useDispatch();
    const [conditionMethod, setConditionMethod] = useState("");
    const [addressArray, setAddressArray] = useState(null);
    const [optionArray, setOptionArray] = useState(null);

    useEffect(() => {
      if (clearFields === true) {
        setRendField(rendField + 1);
        setClearFields(false);
      }
    }, [clearFields]);

    useEffect(() => {
      if (type === "INSTITUTION" && method === "POST") {
        setConditionMethod("NEWPOST");
      }
    }, []);

    useEffect(() => {
      if (optionsAddress) {
        let _addressArray = [];
        if (method === "POST") {
          optionsAddress.forEach((address) => {
            address?.addresses?.forEach((ele) => {
              _addressArray.push({
                name: address.name,
                country: ele.country,
                id: ele.id,
                location: ele.location,
                purposes: ele.purposes,
                streetAddress: ele.streetAddress,
                region: ele.region,
                zipCode: ele.zipCode,
                disabled: true,
              });
            });
          });
        }
        if (contact) {
          contact?.forEach((address) => {
            _addressArray.push(address);
          });
        }
        setAddressArray(_addressArray);
      } else {
        let _addressArray = [];
        if (method == "POST" && type === "INSTITUTION") {
          _addressArray.push({
            name: "Other",
            country: "",
            id: 0,
            location: "",
            purposes: [],
            streetAddress: "Other",
            region: "",
            zipCode: "",
          });
        }
        setAddressArray(_addressArray);
      }
    }, [optionsAddress, contact]);

    useEffect(() => {
      if (addressArray) {
        var hash = {};
        let _optionArray = addressArray.filter(function (current) {
          var exists = !hash[current.id];
          hash[current.id] = true;
          return exists;
        });
        setOptionArray(_optionArray);
      }
    }, [addressArray]);

    const handleClickOpen = (option) => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    /**
     * @param {Object} option
     * @param {Boolean} selected
     * @returns {Node}
     */
    const PrimaryAddressOption = (props, option, { selected }) => {
      const { key, ...restProps } = props;
      return (
        <li key={key} {...restProps}>
          <Checkbox checked={selected} />
          {type === "INSTITUTION" || option.name === undefined
            ? " " +
              (option?.streetAddress ? option?.streetAddress : "") +
              "  " +
              (option?.zipCode ? option?.zipCode : "") +
              "  " +
              (option?.location ? option?.location : "") +
              "  " +
              (option?.region ? option?.region : "")
            : " ( " +
              (option?.name ? option?.name : "") +
              " ) " +
              " " +
              (option?.streetAddress ? option?.streetAddress : "") +
              "  " +
              (option?.zipCode ? option?.zipCode : "") +
              "  " +
              (option?.location ? option?.location : "") +
              "  " +
              (option?.region ? option?.region : "") +
              " ( " +
              option.purposes.map((item) => item.name) +
              " ) "}
        </li>
      );
    };
    /**
     * @param {Object} option
     * @returns {Node}
     */
    const optionInstitutionAddressLabel = (option) =>
      type === "INSTITUTION" || option.name === undefined
        ? `${
            " " +
            (option?.streetAddress ? option?.streetAddress : "") +
            "  " +
            (option?.zipCode ? option?.zipCode : "") +
            "  " +
            (option?.location ? option?.location : "") +
            "  " +
            (option?.region ? option?.region : "")
          }`
        : `${
            " ( " +
            (option?.name ? option?.name : "") +
            " ) " +
            " " +
            (option?.streetAddress ? option?.streetAddress : "") +
            "  " +
            (option?.zipCode ? option?.zipCode : "") +
            "  " +
            (option?.location ? option?.location : "") +
            "  " +
            (option?.region ? option?.region : "") +
            " ( " +
            option?.purposes.map((item) => item.name) +
            " ) "
          }`;

    /**
     * @param {Object} params
     * @param {String} field
     * @returns {Node}
     */
    const renderInputPrimaryAddress = (params, field) => {
      const newInputProps = new Object();
      if (!getValues(field)) {
        Object.assign(newInputProps, { value: "" });
      }
      return (
        <TextField
          {...params}
          variant={"outlined"}
          inputProps={{
            ...params.inputProps,
            ...newInputProps,
          }}
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors[field])}
          error={Boolean(errors[field])}
          label={
            <Typography className="text-ebs">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Primary Address"}
              />
            </Typography>
          }
          helperText={errors[field]?.message}
        />
      );
    };

    const defaultAddress = () => {
      const data = [];
      if (method === "PUT") {
        if (contact) {
          contact?.map((item) => {
            if (item.default === true) {
              data.push(item);
            }
          });
          return data[0];
        } else {
          return null;
        }
      }
    };

    const AddAddressesToArray = async (e, options) => {
      let addresses = [];
      e.preventDefault();
      if (options) {
        switch (method) {
          case "POST":
            addresses.push({
              default: true,
              location: options.location,
              region: options.region,
              streetAddress: options.streetAddress,
              zipCode: options.zipCode,
              countryId: options.country.id,
              purposeIds: options.purposes.map((i) => Number(i.id)),
              id: Number(options.id) || 0,
            });
            setValueAddresses(addresses[0]);

            if (options.name === "Other") {
              handleClickOpen();
              setDataMethod("POST");
            }

            break;
          case "PUT":
            if (options.name !== "Other") {
              contact?.map((item) => {
                if (item.default) {
                  addresses.push({
                    default: false,
                    location: item.location,
                    region: item.region,
                    streetAddress: item.streetAddress,
                    zipCode: item.zipCode,
                    countryId: item.country.id,
                    purposeIds: item.purposes.map((i) => Number(i.id)),
                    id: Number(item.id) || 0,
                  });
                }
              });
              addresses.push({
                default: true,
                location: options.location,
                region: options.region,
                streetAddress: options.streetAddress,
                zipCode: options.zipCode,
                countryId: options.country.id,
                purposeIds: options.purposes.map((i) => Number(i.id)),
                id: Number(options.id) || 0,
              });
              const contactData = {
                id: dataContact.id,
                category:
                  dataContact.category === undefined
                    ? (dataContact.category = { id: 1 })
                    : dataContact.category,
                addresses: addresses,
                purposeIds: dataContact.purposes.map((data) => Number(data.id)),
                email: dataContact.officialEmail,
                institution: {
                  commonName: dataContact.commonName || "",
                  legalName: dataContact.legalName || "",
                  cgiar: dataContact.isCgiar || false,
                },
              };
              await modifyAddress(contactData, dispatch);
            } else {
              handleClickOpen();
              setDataMethod("POST");
            }
            break;
        }
        updateData();
        setRefresh(!refresh);
      }else{
        setValueAddresses(null)
      }
    };
    return method === "ASSIGN" ? null : (
      <div>
        <Autocomplete
          key={rendField}
          className="p-2"
          id="primaryAddresses"
          data-testid={"primaryAddresses"}
          options={optionArray || []}
          defaultValue={() => defaultAddress()}
          onChange={AddAddressesToArray}
          getOptionLabel={optionInstitutionAddressLabel}
          renderOption={PrimaryAddressOption}
          renderInput={renderInputPrimaryAddress}
          isOptionEqualToValue={(option, value) => option?.id === value?.id}
        />

        <Dialog
          fullWidth
          maxWidth="md"
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
        >
          <DialogContent dividers>
            <AddressesFormAtom
              type={type}
              contact={contact}
              method={dataMethod}
              conditionMethod={conditionMethod}
              setOpen={setOpen}
              dataContact={dataContact}
              setvalueAddresses={setValueAddresses}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
FieldAddressesAtom.propTypes = {};
// Default properties
FieldAddressesAtom.defaultProps = {};

export default FieldAddressesAtom;
