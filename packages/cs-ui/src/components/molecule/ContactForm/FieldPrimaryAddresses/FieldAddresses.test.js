import FieldAddresses from './FieldPrimaryAddresses';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FieldAddresses is in the DOM', () => {
  render(<FieldAddresses></FieldAddresses>)
  expect(screen.getByTestId('FieldAddressesTestId')).toBeInTheDocument();
})
