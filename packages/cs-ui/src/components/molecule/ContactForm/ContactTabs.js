import { EbsTabsLayout, Core } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import ContactInfo from "components/molecule/ContactInfo";
import React, { useState, useEffect } from "react";
import AddressesListAtom from "./Addresses/AddressesList/AddressesList";
import {
  findOU,
  findRolesUser,
} from "components/organism/OrganizationUnits/fuctionsOrganizations";
import UnitMembers from "components/organism/OrganizationUnits/UnitMembers";
const { CircularProgress } = Core;
import { useFormContext } from "react-hook-form";
import UnitMemberPost from "components/organism/OrganizationUnits/UnitMemberPost";

export const ContactTabs = React.forwardRef(
  (
    { type, method, defaultValues, optionInstitution, updateData, setMemberOu },
    ref
  ) => {
    const {
      control,
      setValue,
      formState: { errors },
    } = useFormContext();
    const tabs = [];
    const [rolesUser, setRolesUser] = useState(null);
    const [dataOUList, setDataOUList] = useState(null);
    const [reloading, setReloading] = useState(false);
    const [reloadingRol, setReloadingRol] = useState(false);
    const [total, setTotal] = useState(1);
    const [totalRol, setTotalRol] = useState(1);
    let percentage = 0;
    let percentageRol = 0;

    useEffect(() => {
      setRolesUser([]);
      setDataOUList([]);
      const fetchRolesUser = async () => {
        await findRolesUser(setRolesUser, setReloadingRol, setTotalRol);
      };
      const fetchOU = async () => {
        await findOU(setDataOUList, setReloading, setTotal);
      };
      fetchRolesUser();
      fetchOU();
    }, []);

    if (rolesUser === null) {
      return (
        <div className="flex justify-center pt-8 place-items-center">
          <CircularProgress color="primary" className="place-self-center" />
          <br />
        </div>
      );
    }

    percentage = Math.round((dataOUList.length / total) * 100, 2);
    percentageRol = Math.round((rolesUser.length / totalRol) * 100, 2);

    if (method === "PUT") {
      tabs.push({
        label: <FormattedMessage id={"none"} defaultMessage={"Addresses"} />,
        component: (
          <AddressesListAtom
            type={type}
            method={method}
            dataContact={defaultValues}
            updateData={updateData}
          />
        ),
      });
      if (type === "USER") {
        tabs.push({
          label: (
            <FormattedMessage
              id={"none"}
              defaultMessage={"Member Of Organization Unit"}
            />
          ),
          component: (
            <UnitMembers
              method="PUT"
              dataOUList={dataOUList}
              rolesUser={rolesUser}
              defaultAddreses={{}}
              process="userManagement"
              userId={defaultValues.userId}
              contactId={defaultValues.id}
              reloading={reloading}
              percentage={percentage}
              reloadingRol={reloadingRol}
              percentageRol={percentageRol}
              optionInstitution={optionInstitution}
            />
          ),
        });
      }
      tabs.push({
        label: (
          <FormattedMessage
            id={"none"}
            defaultMessage={"Additional Information"}
          />
        ),
        component: (
          <ContactInfo
            contactId={defaultValues?.id}
            type={type}
            control={control}
            errors={errors}
            setValue={setValue}
          />
        ),
      });
    }
    if (type === "USER" && method === "POST") {
      tabs.push({
        label: (
          <FormattedMessage
            id={"none"}
            defaultMessage={"Member Of Organization Unit"}
          />
        ),
        component: (
          <UnitMemberPost
            dataOUList={dataOUList}
            rolesUser={rolesUser}
            process="userManagement"
            percentage={percentage}
            reloading={reloading}
            reloadingRol={reloadingRol}
            percentageRol={percentageRol}
            optionInstitution={optionInstitution}
            setMemberOu={setMemberOu}
          />
        ),
      });
    }
    return (
      <>
        <EbsTabsLayout orientation="horizontal" tabs={tabs} />
      </>
    );
  }
);
