import { Core } from "@ebs/styleguide";
const { Dialog, DialogTitle, Typography, DialogActions, Button } = Core;
import { FormattedMessage } from "react-intl";

const ModalValidation = ({
  open,
  method,
  setValidationContact,
  buttonRef
}) => {
  const handleClose = (e) => {
    setValidationContact(false);
  };
  const triggerButtonClick = () =>{
    if(buttonRef.current){
      buttonRef.current.click()
    }
  }

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        <Typography className="font-ebs">
          <FormattedMessage
            id={"none"}
            defaultMessage={
              method === "POST"
                ? "We have detected that the Name and Surname already exist in the database. Do you want to register this contact as new? If you accept, this contact will be considered different from the one already registered."
                : "We have detected that the Name and Surname already exist in the database. Do you want to edit this contact? If you accept, this contact will be considered different from the one already registered."
            }
          />
        </Typography>
      </DialogTitle>
      <DialogActions>
        <Button onClick={handleClose}>
          <FormattedMessage id={"none"} defaultMessage={"No"} />
        </Button>
        <Button onClick={triggerButtonClick}>
          <FormattedMessage id={"none"} defaultMessage={"Save"} />
        </Button>
      </DialogActions>
    </Dialog>
  );
};
export default ModalValidation;
