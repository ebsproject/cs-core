import { Controller, useFormContext } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { TextField, Typography } = Core;

const TextfieldInput = ({ name, label, rules, disabled }) => {
  const {
    control,
    formState: { errors },
  } = useFormContext();
  return (
    <Controller
      name={name}
      control={control}
      render={({ field }) => (
        <TextField
          disabled={disabled}
          variant={"outlined"}
          fullWidth
          InputLabelProps={{ shrink: true }}
          {...field}
          error={!!errors[name]}
          helperText={errors[name] ? errors[name].message : ""}
          label={
            <Typography variant={"h5"} style={{ fontWeight: 600 }}>
              {`${label} ${(rules?.required && "*") || ""}`.trim()}
            </Typography>
          }
          data-testid={`testid-${name}`}
        />
      )}
      rules={rules}
    />
  );
};
export default TextfieldInput;
