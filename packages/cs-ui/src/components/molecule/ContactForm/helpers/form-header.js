import { Core, Icons } from "@ebs/styleguide";
const { Typography, Button, Grid } = Core;
import { FormattedMessage } from "react-intl";
const { Cancel, CheckCircle } = Icons;

const FormHeader = ({ handleCancel, buttonRef }) => {
  return (
    <>
      <Typography className="flex-grow font-ebs col-span-2 text-ebs-green-900 font-bold uppercase text-lg">
        <FormattedMessage id="none" defaultMessage="Basic Information" />
      </Typography>
      <Grid container justifyContent="flex-end">
        <Button
          data-testid="cancel"
          onClick={handleCancel}
          startIcon={<Cancel />}
          className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
        >
          <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
        </Button>
        <Button
          ref={buttonRef}
          data-testid="save"
          type="submit"
          startIcon={<CheckCircle />}
          className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
        >
          <FormattedMessage id={"none"} defaultMessage={"Save"} />
        </Button>
      </Grid>
    </>
  );
};
export default FormHeader;
