import React, { memo } from "react";
import { Core, Icons } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { rowActions } from "components/EmailTemplate/templateRowActions";
const {
  Box,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Grid,
  CircularProgress,
  List,
  ListItem,
  Chip,
} = Core;
const { Star } = Icons;

const ViewContact = React.forwardRef(({ open, handleClose, data }, ref) => {
  const ComponentContactInfo = () => {
    return data?.contactInfos?.map((item, index) => (
      <Chip key={index} label={item.value} variant="outlined" />
    ));
  };

  const ComponentContactTypes = () => {
    return data?.contactTypes?.map((item, index) => (
      <Chip key={index} label={item.name} variant="outlined" />
    ));
  };

  const ComponentPurposes = () => {
    return data?.purposes?.map((item, index) => (
      <Chip key={index} label={item.name} variant="outlined" />
    ));
  };

  const ComponentInstitution = () => {
    const dataFilter = data?.parents?.filter(
      (filterItem) => filterItem.institution.category.name === "Institution"
    );
    return dataFilter?.map((item, index) => (
      <Chip
        key={index}
        label={item.institution.institution.legalName}
        variant="outlined"
      />
    ));
  };

  const ComponentUsers = () => {
    return data?.users?.map((item, index) => (
      <Chip key={index} label={item.userName} variant="outlined" />
    ));
  };

  const ComponentOU = () => {
    const dataFilter = data?.parents?.filter(
      (ouItem) => ouItem.institution.category.name === "Internal Unit"
    );
    return dataFilter?.map((item, index) => (
      <Chip
        key={index}
        label={item.institution.institution.legalName}
        variant="outlined"
      />
    ));
  };

  const addreessList = data?.addresses?.sort((a, b) => b.default - a.default);

  return (
    <div data-testid={""}>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
        maxWidth="md"
        fullWidth
      >
        <DialogContent dividers>
          <Typography variant="h6" className="mb-5 " color="primary">
            <FormattedMessage id="none" defaultMessage="Basic Information" />
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Salutation" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    data?.person?.salutation !== null
                      ? data?.person?.salutation
                      : " "
                  }
                />
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Family Name" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    data?.person?.familyName !== undefined
                      ? data?.person?.familyName
                      : ""
                  }
                />
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Given Name" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    data?.person?.givenName !== undefined
                      ? data?.person?.givenName
                      : ""
                  }
                />
              </Typography>
            </Grid>

            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg "
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Additional Name" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    data?.person?.additionalName !== undefined
                      ? data?.person?.additionalName
                      : ""
                  }
                />
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Job Title" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    data?.person?.jobTitle !== undefined
                      ? data?.person?.jobTitle
                      : " "
                  }
                />
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Gender" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    data?.person?.gender !== undefined
                      ? data?.person?.gender
                      : " "
                  }
                />
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Email" />
              </Typography>
              <Typography variant="subtitle2" className="mb-5">
                <FormattedMessage
                  id="none"
                  defaultMessage={data?.email !== undefined ? data?.email : " "}
                />
              </Typography>
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage
                  id="none"
                  defaultMessage="Contact Information"
                />
              </Typography>
              <ComponentContactInfo />
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Contact Types" />
              </Typography>
              <ComponentContactTypes />
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Purposes" />
              </Typography>
              <ComponentPurposes />
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Institution" />
              </Typography>
              <ComponentInstitution />
            </Grid>
            <Grid item xs={2}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage id="none" defaultMessage="Login Account" />
              </Typography>
              <ComponentUsers />
            </Grid>
            <Grid item xs={8}>
              <Typography
                variant="h6"
                className="mb-5 text-lg"
                color="secondary"
              >
                <FormattedMessage
                  id="none"
                  defaultMessage="Member of Organisation Unit"
                />
              </Typography>
              <ComponentOU />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogContent dividers>
          <Typography variant="h6" className="mb-5 " color="primary">
            <FormattedMessage id="none" defaultMessage="Addreess" />
          </Typography>
          <Grid container spacing={1}>
            <Grid item xs={6}>
              <List>
                {addreessList?.length > 0
                  ? addreessList?.map((item, index) => {
                      return item?.default === true ? (
                        <ListItem key={index} disableGutters>
                          <Star color="primary" className="mr-5" />
                          {item?.fullAddress}
                        </ListItem>
                      ) : (
                        <ListItem key={index} disableGutters>
                          {item?.fullAddress}
                        </ListItem>
                      );
                    })
                  : null}
              </List>
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button
            onClick={handleClose}
            className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
          >
            Back
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

ViewContact.propTypes = {};
ViewContact.defaultProps = {};
export default memo(ViewContact);
