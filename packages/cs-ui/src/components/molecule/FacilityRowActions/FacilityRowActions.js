import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { RBAC } from "@ebs/layout";
import { Core, Icons } from "@ebs/styleguide";
import { Link, useNavigate } from "react-router-dom";
const {
  Typography,
  Tooltip,
  IconButton,
  Box,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
} = Core;
const { Edit, Delete, Visibility } = Icons;
import { useDispatch } from "react-redux";
import { deleteFacility, setFindDataPlace } from "store/modules/PlaceManager";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FacilityRowActionsMolecule = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleGoTo = ({ type, gridType }) => {
      dispatch(
        setFindDataPlace({
          id: rowData?.id,
          gridType: gridType,
          data: rowData,
        })
      );
      navigate("/cs/place-manager/view-facility",{
        state: {
          type: type,
        },
      });
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleProcess = () => {
      setOpen(true);
    };

    const handleDelete = async () => {
      await deleteFacility(
        {
          id: rowData?.id,
        },
        dispatch
      );
      refresh();
    };
    return (
      <Box component="div" ref={ref} className="-ml-2 flex flex-auto">
        <RBAC allowedAction={"View"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"View"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              color="primary"
              onClick={() =>
                handleGoTo({
                  type: "VIEW",
                  gridType: rowData?.facilityClass,
                })
              }
            >
              <Visibility />
            </IconButton>
          </Tooltip>
        </RBAC>
        <RBAC allowedAction={"Modify"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Edit"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              color="primary"
              onClick={() =>
                handleGoTo({
                  type: "EDIT",
                  gridType: rowData?.facilityClass,
                })
              }
            >
              <Edit />
            </IconButton>
          </Tooltip>
        </RBAC>
        <RBAC allowedAction={"Delete"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Delete"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              color="primary"
              onClick={() => handleProcess()}
            >
              <Delete />
            </IconButton>
          </Tooltip>
        </RBAC>

        <Dialog open={open} maxWidth="xs">
          <DialogTitle>{"Delete"}</DialogTitle>
          <DialogContent className="font-black">
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage={
                  "Are you sure to delete the selected record? If you delete it, you will also delete your relationships with your parents."
                }
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button
              onClick={handleDelete}
              color="primary"
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    );
  }
);
// Type and required properties
FacilityRowActionsMolecule.propTypes = {};
// Default properties
FacilityRowActionsMolecule.defaultProps = {};

export default FacilityRowActionsMolecule;
