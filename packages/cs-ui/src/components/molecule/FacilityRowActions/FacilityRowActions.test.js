import FacilityRowActions from './FacilityRowActions';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FacilityRowActions is in the DOM', () => {
  render(<FacilityRowActions></FacilityRowActions>)
  expect(screen.getByTestId('FacilityRowActionsTestId')).toBeInTheDocument();
})
