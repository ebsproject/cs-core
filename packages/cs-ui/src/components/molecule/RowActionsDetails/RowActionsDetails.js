import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
import { useSelector, useDispatch } from "react-redux";
import { deleteGeospatialObject } from "store/modules/PlaceManager";
import { RBAC } from "@ebs/layout";
const {
  Typography,
  Tooltip,
  IconButton,
  Box,
  Dialog,
  DialogTitle,
  DialogActions,
  DialogContent,
  Button,
} = Core;
const { Edit, Delete, Visibility } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const RowActionsDetailsMolecule = React.forwardRef(
  ({ type, rowData, setListData, refresh }, ref) => {
    const { placemanager } = useSelector((placemanager) => placemanager);
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
      if (placemanager.success) {
        refresh();
      }
    }, [placemanager.success]);

    const handleGoTo = () => {
      setListData([rowData]);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleProcess = () => {
      setOpen(true);
    };

    const handleDelete = async () => {
      await deleteGeospatialObject(
        {
          id: rowData.id,
        },
        dispatch
      );
      refresh();
    };
    return (
      <Box component="div" ref={ref} className="-ml-2 flex flex-auto">
        <RBAC allowedAction={"View"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"View"} />
              </Typography>
            }
          >
            <span>
              <IconButton
                size="small"
                color="primary"
                disabled={type === "VIEW" ? false : true}
                onClick={() => handleGoTo()}
              >
                <Visibility />
              </IconButton>
            </span>
          </Tooltip>
        </RBAC>
        <RBAC allowedAction={"Modify"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Edit"} />
              </Typography>
            }
          >
            <span>
              <IconButton
                size="small"
                color="primary"
                disabled={type === "VIEW" ? true : false}
                onClick={() => handleGoTo()}
              >
                <Edit />
              </IconButton>
            </span>
          </Tooltip>
        </RBAC>
        <RBAC allowedAction={"Delete"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Delete"} />
              </Typography>
            }
          >
            <span>
              <IconButton
                size="small"
                color="primary"
                disabled={type === "VIEW" ? true : false}
                onClick={() => handleProcess()}
              >
                <Delete />
              </IconButton>
            </span>
          </Tooltip>
        </RBAC>
        <Dialog open={open} maxWidth="xs">
          <DialogTitle>{"Delete"}</DialogTitle>
          <DialogContent className="font-black">
            <Typography className="text-ebs text-bold">
              <FormattedMessage
                id="none"
                defaultMessage={
                  "Are you sure to delete the selected record? If you delete it, you will also delete your relationships with your parents."
                }
              />
            </Typography>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Cancel
            </Button>
            <Button
              onClick={handleDelete}
              color="primary"
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    );
  }
);
// Type and required properties
RowActionsDetailsMolecule.propTypes = {};
// Default properties
RowActionsDetailsMolecule.defaultProps = {};

export default RowActionsDetailsMolecule;
