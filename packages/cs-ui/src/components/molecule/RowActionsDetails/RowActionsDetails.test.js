import RowActionsDetails from './RowActionsDetails';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('RowActionsDetails is in the DOM', () => {
  render(<RowActionsDetails></RowActionsDetails>)
  expect(screen.getByTestId('RowActionsDetailsTestId')).toBeInTheDocument();
})
