import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { findUser, contactProcess } from "store/modules/DataPersist";
import DeleteUser from "components/atom/DeleteUser";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Box, IconButton, Tooltip, Typography, Switch, CircularProgress } = Core;
const { Edit } = Icons;
import { RBAC } from "@ebs/layout";
import UserActiveSwitch from "components/atom/ActiveSwitch";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const UserRowActionsMolecule = React.forwardRef(({ rowData, refresh }, ref) => {
  const name = rowData?.contact?.person?.givenName;
  const familyName = rowData?.contact?.person?.familyName;
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  /**
   * @param {String} type
   * @param {String} method
   * @param {ID} id: optional
   */
  const handleGoTo = async ({ type, method, path }) => {
    setLoading(true);
    const { data } = await findUser({ id: rowData?.id });
    dispatch(contactProcess({ type, method, defaultValues: data }));
    setLoading(false);
    navigate(`/cs${path}`);
  };

  return (
    /**
     * @prop data-testid: Id to use inside UserRowActions.test.js file.
     */

    <div className="content-center" data-testid={"UserRowActionsTestId"}>
      <Box component="div" ref={ref} className="-ml-2 flex flex-auto">
        <RBAC
          allowedAction={"Modify"}
          product={"User Management"}
          domain={"Core System"}
        >
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Update User"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              color="primary"
              onClick={() =>
                handleGoTo({
                  type: "USER",
                  method: "PUT",
                  path: "/contact",
                })
              }
            >
              {loading === true ? <CircularProgress /> : <Edit />}
            </IconButton>
          </Tooltip>
        </RBAC>
        <RBAC
          allowedAction={"Delete"}
          product={"User Management"}
          domain={"Core System"}
        >
          <DeleteUser
            refresh={refresh}
            userId={Number(rowData?.id)}
            name={name}
            familyName={familyName}
          />
        </RBAC>
        <RBAC
          allowedAction={"Modify"}
          product={"User Management"}
          domain={"Core System"}
        >
          <UserActiveSwitch
            method="PUT"
            refresh={refresh}
            rowData={rowData}
            type="USER"
          />
        </RBAC>
      </Box>
    </div>
  );
});
// Type and required properties
UserRowActionsMolecule.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func,
};
// Default properties
UserRowActionsMolecule.defaultProps = {};

export default UserRowActionsMolecule;
