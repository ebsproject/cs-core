import React, { useState, useEffect } from "react";
import { client } from "utils/apollo";
import PropTypes from "prop-types";
import { useQuery } from "@apollo/client";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const {
  IconButton,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Typography,
  Tooltip,
  Box,
} = Core;
const {AccountBox} = Icons;
import { EbsGrid } from "@ebs/components";
import { useSelector } from "react-redux";
import AssignContact from "components/atom/AssignContact";
import { FIND_CONTACT } from "utils/apollo/gql/crm";
import { getCoreSystemContext } from "@ebs/layout";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const AssignContactsMolecule = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const { graphqlUri } = getCoreSystemContext();
    const [open, setOpen] = React.useState(false);
    const { success } = useSelector(({ crm }) => crm);
    const { data, errors } = useQuery(FIND_CONTACT, {
      variables: { id: (rowSelected && Number(rowSelected.id)) || 0 },
      fetchPolicy: "no-cache",
      client: client,
    });
    const contacts = data?.findContact?.institution?.contacts;
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    var arrayInstitution = rowSelected;
    var newObjectInstitution = Object.assign({}, arrayInstitution);
    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };
    // console.log(data);
    const columns = [
      { Header: "id", accessor: "id", hidden: true },
      {
        Header: (
          <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage=" Family Name" />
          </Typography>
        ),
        accessor: "person.familyName",
        csvHeader: "Name",
        disableGlobalFilter: true,
        disableFilters: false,
        filter: true,
        width: 300,
      },
      {
        Header: (
          <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Email" />
          </Typography>
        ),
        accessor: "email",
        csvHeader: "Email",
        disableGlobalFilter: true,
        disableFilters: false,
        filter: true,
        width: 300,
      },
    ];
    const toolbarActions = (rowSelected, refresh) => {
      return (
        <Box className="px-2">
          <AssignContact
            rowSelected={(rowSelected.length > 0 && rowSelected[0]) || null}
            refresh={refresh}
            newObjectInstitution={newObjectInstitution}
            contacts={contacts}
          />
        </Box>
      );
    };
    return (
      /**
       * @prop data-testid: Id to use inside AssignContacts.test.js file.
       */
      <div ref={ref} data-testid={"AssignContactsTestId"}>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id="none" defaultMessage="Assign existing contact" />
            </Typography>
          }
        >
          <IconButton
            size="small"
            color="primary"
            onClick={handleClickOpen}
          >
            <AccountBox />
          </IconButton>
        </Tooltip>
        <Dialog
          onClose={handleClose}
          maxWidth="xl"
          open={open}
          aria-label="assingContactsDialog"
        >
          <DialogTitle >
            <Typography className="font-ebs text-lg">
            <FormattedMessage  id="none" defaultMessage="Assign existing contact" />
            </Typography>
          
          </DialogTitle>
          <DialogContent>
            <EbsGrid
              id="ContactListBelongsTo"
              toolbar={true}
              columns={columns}
              uri={graphqlUri}
              entity="Contact"
              toolbaractions={toolbarActions}
              select="single"
              callstandard="graphql"
              height="60vh"
            />
            <DialogActions>
              <Button onClick={handleClose} color="secondary">
                <FormattedMessage id="none" defaultMessage="Close" />
              </Button>
            </DialogActions>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
AssignContactsMolecule.propTypes = {};
// Default properties
AssignContactsMolecule.defaultProps = {};

export default AssignContactsMolecule;
