import React, { memo, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import PermissionsInfoDialog from "components/molecule/PermissionsInfoDialog";
import { Core } from "@ebs/styleguide";
const { Box, Typography } = Core;
import PermissionsRowActions from "components/atom/PermissionsRowActions";

//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const Permissions = React.forwardRef(({ contactId, userName }, ref) => {
  // Columns
  const columnsDefinitions = [
    // { Header: "Id", accessor: "id", hidden: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Teams" />
        </Typography>
      ),
      csvHeader: "Team",
      accessor: "parent.parent.value",
      width: 700,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Roles" />
        </Typography>
      ),
      csvHeader: "Role",
      accessor: "parent.value",
      width: 700,
    },
  ];

  /**
   * @param selectedRows: Array object with data rows selected.
   * @param refresh: Function to refresh Grid data.
   */
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box className="flex flex-row flex-nowrap">
        <PermissionsInfoDialog
          contactId={contactId}
          refresh={refresh}
          userName={userName}
          allowAddTeam={true} // change to add just one role per user & team , replace with 'allowAddTeam' const
        />
      </Box>
    );
  };
  /**
   * @prop data-testid: Id to use inside grid.test.js file.
   */
  return (
    <div data-testid={"PermissionsInfoTestId"}>
      <EbsGrid
        id="PermissionsInfo"
        toolbar={true}
        columns={columnsDefinitions}
        toolbaractions={toolbarActions}
        rowactions={PermissionsRowActions}
        // entity="HierarchyTree"
        // uri={graphqlUri}
        fetch={fetch}
        // callstandard="graphql"
        //defaultFilters={[{ col: "recordId", mod: "EQ", val: contactId }]}
        height="85vh"
        select="multi"
        csvfilename="PermissionsInformation"
      />
    </div>
  );
});

// Type and required properties
Permissions.propTypes = {};
// Default properties
Permissions.defaultProps = {};

export default memo(Permissions);
