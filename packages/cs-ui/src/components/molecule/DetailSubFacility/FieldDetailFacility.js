import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core, Icons, Styles } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
const { Typography } = Core;
import { clientCb } from "utils/apollo/apollo";
import { EbsGrid } from "@ebs/components";
import PlaceRowActions from "components/molecule/PlaceRowActions/PlaceRowActions";
import { FIND_FACILITY_LIST } from "utils/apollo/gql/placeManager";
import FacilityRowActionsMolecule from "../FacilityRowActions/FacilityRowActions";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const FieldDetailFacilityMolecule = React.forwardRef(({ rowData }, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      accessor: "null2",
      width: 200,
      // filter: false,
    },
    {
      accessor: "null3",
      width: 200,
      // filter: false,
    },
    {
      accessor: "null4",
      width: 200,
      // filter: false,
    },
    {
      accessor: "null5",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "subFacility.facilityName",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "subFacility.facilityCode",
      width: 200,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "subFacility.facilityType",
      width: 200,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "container.facilityName",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "container.facilityCode",
      width: 200,
      // filter: false,
    },
    // {
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "dateCreated",
    //   width: 200,
    //   // filter: false,
    // },
    // {
    //   Cell: ({ value }) => {
    //     return (
    //       <Typography variant="body1">
    //         {value?.type === "Polygon"
    //           ? value?.coordinates[0][0]
    //           : value?.coordinates[0]}
    //       </Typography>
    //     );
    //   },
    //   accessor: "coordinates.features[0].geometry",
    //   csvHeader: "Coordinates",
    //   width: 200,
    //   filter: true,
    // },
  ];
  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        clientCb
          .query({
            query: FIND_FACILITY_LIST,
            variables: {
              page: page,
              filters: [
                ...filters,
                {
                  col: "parentFacility.id",
                  mod: "EQ",
                  val: rowData.id,
                },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              let dataList = data.findFacilityList.content;
              const listParents = dataList.map((item) => {
                switch (item.facilityClass) {
                  case "structure":
                    const dataSubFaci = {
                      id: item.id,
                      altitude: item.altitude,
                      description: item.description,
                      coordinates: item.coordinates,
                      dateCreated: item.dateCreated,
                      parentFacility: item.parentFacility,
                      facilityClass: item.facilityClass,
                      subFacility: {
                        facilityClass: item.facilityClass,
                        facilityName: item.facilityName,
                        facilityCode: item.facilityCode,
                        facilityType: item.facilityType,
                      },
                    };
                    return dataSubFaci;
                  case "container":
                    const dataContainer = {
                      id: item.id,
                      altitude: item.altitude,
                      description: item.description,
                      coordinates: item.coordinates,
                      dateCreated: item.dateCreated,
                      parentFacility: item.parentFacility,
                      facilityClass: item.facilityClass,
                      container: {
                        facilityClass: item.facilityClass,
                        facilityName: item.facilityName,
                        facilityCode: item.facilityCode,
                        facilityType: item.facilityType,
                      },
                    };
                    return dataContainer;
                }
              });
              resolve({
                pages: data.findFacilityList.totalPages,
                elements: data.findFacilityList.totalElements,
                data: listParents,
              });
            }
          });
      } catch (error) {
        reject(error);
      }
    });
  };

  const DetailComponent = ({ rowData: data }) => {
    if (data.facilityClass !== "container") {
      const columnsContainer = [
        {
          Header: "Id",
          accessor: "id",
          hidden: true,
          disableGlobalFilter: true,
        },
        {
          accessor: "null2",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null3",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null4",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null5",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null6",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null7",
          width: 200,
        },
        {
          accessor: "null8",
          width: 200,
        },
        {
          Cell: ({ value }) => {
            return <Typography variant="body1">{value}</Typography>;
          },
          accessor: "facilityName",
          width: 200,
          // filter: false,
        },
        {
          Cell: ({ value }) => {
            return <Typography variant="body1">{value}</Typography>;
          },
          accessor: "facilityCode",
          width: 200,
          // filter: false,
        },
        // {
        //   Cell: ({ value }) => {
        //     return <Typography variant="body1">{value}</Typography>;
        //   },
        //   accessor: "dateCreated",
        //   width: 200,
        //   // filter: false,
        // },
        // {
        //   Cell: ({ value }) => {
        //     return (
        //       <Typography variant="body1">
        //         {value?.type === "Polygon"
        //           ? value?.coordinates[0][0]
        //           : value?.coordinates[0]}
        //       </Typography>
        //     );
        //   },
        //   accessor: "coordinates.features[0].geometry",
        //   csvHeader: "Coordinates",
        //   width: 200,
        //   filter: true,
        // },
      ];
      const fetchContainer = async ({ page, sort, filters }) => {
        return new Promise((resolve, reject) => {
          try {
            clientCb
              .query({
                query: FIND_FACILITY_LIST,
                variables: {
                  page: page,
                  filters: [
                    ...filters,
                    {
                      col: "parentFacility.id",
                      mod: "EQ",
                      val: data.id,
                    },
                  ],
                  disjunctionFilters: false,
                },
                fetchPolicy: "no-cache",
              })
              .then(({ data, errors }) => {
                if (errors) {
                  reject(errors);
                } else {
                  resolve({
                    pages: data.findFacilityList.totalPages,
                    elements: data.findFacilityList.totalElements,
                    data: data.findFacilityList.content,
                  });
                }
              });
          } catch (error) {
            reject(error);
          }
        });
      };

      const DetailComponentAction = () => {
        return null;
      };
      return (
        <div data-testid={"DivContainer"} style={{ width: 2020 }}>
          <EbsGrid
            id="Container"
            toolbar={false}
            data-testid={"Container"}
            columns={columnsContainer}
            rowactions={FacilityRowActionsMolecule}
            csvfilename="PlacesList"
            fetch={fetchContainer}
            detailcomponent={DetailComponentAction}
            height="85vh"
            select="multi"
            disabledHeadTable
            color="terciary"
          />
        </div>
      );
    }
  };

  return (
    /* 
     @prop data-testid: Id to use inside FieldDetailFacility.test.js file.
     */
    <div data-testid={"FieldFacilityTestId"} style={{ width: 2020 }}>
      <EbsGrid
        id="FieldFacility"
        toolbar={false}
        data-testid={"FieldFacility"}
        columns={columns}
        rowactions={FacilityRowActionsMolecule}
        csvfilename="FieldFacility"
        detailcomponent={DetailComponent}
        fetch={fetch}
        height="85vh"
        select="multi"
        disabledHeadTable
        color="secondary"
      />
    </div>
  );
});
// Type and required properties
FieldDetailFacilityMolecule.propTypes = {};
// Default properties
FieldDetailFacilityMolecule.defaultProps = {};

export default FieldDetailFacilityMolecule;
