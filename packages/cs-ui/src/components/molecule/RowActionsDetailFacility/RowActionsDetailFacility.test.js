import RowActionsDetailFacility from './RowActionsDetailFacility';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('RowActionsDetailFacility is in the DOM', () => {
  render(<RowActionsDetailFacility></RowActionsDetailFacility>)
  expect(screen.getByTestId('RowActionsDetailFacilityTestId')).toBeInTheDocument();
})
