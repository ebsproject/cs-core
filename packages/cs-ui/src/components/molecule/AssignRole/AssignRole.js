import React, { useState, useEffect, Fragment } from "react";
import { client } from "utils/apollo";
import PropTypes from "prop-types";
import { useForm, Controller } from "react-hook-form";
import { FIND_ROLE_LIST } from "utils/apollo/gql/userManagement";
import { useQuery } from "@apollo/client";
import { assignRole } from "store/modules/UserManagement";
import { useSelector, useDispatch } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons,Lab,EbsDialog } from "@ebs/styleguide";
const {
  Box,
  Button,
  Checkbox,
  DialogActions,
  IconButton,
  TextField,
  Tooltip,
  Typography,
} = Core;
const {Cancel, CheckCircle, Edit} = Icons;
const {Autocomplete} = Lab;
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const AssignRoleMolecule = React.forwardRef(
  ({ id, userName, roles, contact, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const { success } = useSelector(({ um }) => um);
    const dispatch = useDispatch();
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [success]);

    const { data } = useQuery(FIND_ROLE_LIST, {
      variables: { page: { number: 1, size: 10 } },
      fetchPolicy: "no-cache",
      client: client,
    });
    const {
      control,
      formState: { errors },
      handleSubmit,
      reset,
      setValue,
      getValues,
    } = useForm({
      defaultValues: {
        id,
        userName,
        givenName: contact?.person?.givenName,
        familyName: contact?.person?.familyName,
        role: roles,
      },
    });
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    /**
     * @param {Object} data
     */
    const submit = (data) => {
      assignRole({ data, lastRoles: roles })(dispatch);
    };
    /**
     * @param {Object} e
     * @param {Object} options
     */
    const onChange = (e, options) => {
      e.preventDefault();
      setValue("role", options);
    };
    /**
     * @param {Object} option
     * @returns
     */
    const getOptionLabel = (option) => `${option.name}`;
    /**
     * @param {Object} option
     * @param {Boolean} selected
     * @returns {node}
     */
    const renderOption = (option, { selected }) => {
      return (
        <Fragment>
          <Checkbox checked={selected} />
          {option.name}
        </Fragment>
      );
    };
    /**
     * @param {Object} params
     * @returns
     */
    const renderInput = (params) => {
      return (
        <TextField
          {...params}
          focused={Boolean(errors["role"])}
          error={Boolean(errors["role"])}
          label={
            <Typography className="font-ebs">
              <FormattedMessage id={"none"} defaultMessage={"Role"} />
            </Typography>
          }
          helperText={errors["role"]?.message}
        />
      );
    };
    /**
     * @param {Object} option
     * @param {Object} value
     * @returns {Boolean}
     */
    const getOptionSelected = (option, value) => option.id === value.id;

    return (
      /**
       * @prop data-testid: Id to use inside AssignRole.test.js file.
       */
      <div component="div" ref={ref} data-testid={"AssignRoleTestId"}>
        <Tooltip
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id={"none"} defaultMessage={"Assign Role"} />
            </Typography>
          }
          arrow
        >
          <IconButton color="primary" onClick={handleOpen}>
            <Edit />
          </IconButton>
        </Tooltip>
        <EbsDialog
          open={open}
          handleClose={handleClose}
          title={<FormattedMessage id={"none"} defaultMessage={"Edit User"} />}
          maxWidth={"sm"}
        >
          <form onSubmit={handleSubmit(submit)}>
            <Typography className="text-ebs-green-default m-5">
              <FormattedMessage
                id={"none"}
                defaultMessage={"User Information"}
              />
            </Typography>
            <Box component={"div"} className="grid grid-cols-2 gap-2 m-5">
              <Controller
                name={"userName"}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    disabled
                    className="col-span-2"
                    label={
                      <Typography className="font-ebs">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={"Account"}
                        />
                      </Typography>
                    }
                  />
                )}
              />
              <Controller
                name={"givenName"}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    disabled
                    label={
                      <Typography className="font-ebs">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={"Given Name"}
                        />
                      </Typography>
                    }
                  />
                )}
              />
              <Controller
                name={"familyName"}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    disabled
                    label={
                      <Typography className="font-ebs">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={"Family Name"}
                        />
                      </Typography>
                    }
                  />
                )}
              />
            </Box>
            <Box className="grid grid-cols-1 m-5">
              <Typography className="text-ebs-green-default">
                <FormattedMessage id={"none"} defaultMessage={"Security"} />
              </Typography>
              <Controller
                name={"id"}
                control={control}
                render={({ field }) => (
                  <TextField
                    {...field}
                    className="hidden"
                    label={
                      <Typography className="font-ebs">
                        <FormattedMessage id={"none"} defaultMessage={"Id"} />
                      </Typography>
                    }
                  />
                )}
              />
              <Controller
                name={"role"}
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    multiple
                    limitTags={2}
                    options={data?.findRoleList?.content || []}
                    disableCloseOnSelect
                    onChange={onChange}
                    getOptionLabel={getOptionLabel}
                    renderOption={renderOption}
                    renderInput={renderInput}
                    isOptionEqualToValue={getOptionSelected}
                  />
                )}
                rules={{
                  required: (
                    <Typography className="font-ebs">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"Please. Select one or more roles"}
                      />
                    </Typography>
                  ),
                }}
              />
            </Box>
            <DialogActions>
              <Button
                onClick={handleClose}
                startIcon={<Cancel />}
                className="bg-red-600 hover:bg-red-700 text-white"
              >
                <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
              </Button>
              <Button
                type="submit"
                startIcon={<CheckCircle />}
                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
              >
                <FormattedMessage id={"none"} defaultMessage={"Save"} />
              </Button>
            </DialogActions>
          </form>
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
AssignRoleMolecule.propTypes = {
  id: PropTypes.string,
  roles: PropTypes.array,
  contact: PropTypes.object,
  refresh: PropTypes.func,
};
// Default properties
AssignRoleMolecule.defaultProps = {};

export default AssignRoleMolecule;
