import AssignRole from "./AssignRole";
import React from "react";
import { render, cleanup, screen, waitFor } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";

let translation={
  none:"test"
}

afterEach(cleanup);

test('AssignRole is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <AssignRole></AssignRole>
       </IntlProvider>
    </Provider>
  )
  expect(screen.getByTestId('AssignRoleTestId')).toBeInTheDocument();
})
