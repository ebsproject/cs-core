import { Core } from "@ebs/styleguide";
const { Typography} = Core;
import { FormattedMessage } from "react-intl";
export const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Site" />
        </Typography>
      ),
      accessor: "geospatialObjectName",
      csvHeader: "Site",
      width: 180,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Site Code" />
        </Typography>
      ),
      accessor: "geospatialObjectCode",
      csvHeader: "Site Code",
      width: 180,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Field" />
        </Typography>
      ),
      accessor: "geospatialObjectType",
      csvHeader: "Site Type",
      width: 180,
      filter: true,
    },
  ];