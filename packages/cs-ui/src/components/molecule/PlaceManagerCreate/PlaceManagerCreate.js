import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Typography, Card, CardContent, Button } = Core;
import { EbsTabsLayout } from "@ebs/styleguide";
import { useNavigate, useLocation } from "react-router-dom";
import { EbsGrid } from "@ebs/components";
import { getCoreSystemContext } from "@ebs/layout";
import FormPlaceManagerCreate from "components/atom/FormPlaceManagerCreate/FormPlaceManagerCreate";
import { columns } from "./columns";

/*
  @param { }: page props,
*/

const ActionRefresh = ({ rowData, refresh, idSite }) => {
  useEffect(() => {
    refresh();
  }, [idSite]);
  return null;
};
export default function PlaceManagerCreateView() {
  const location = useLocation();
  const [type, setType] = useState(location.state.type);
  const { cbGraphqlUri } = getCoreSystemContext();
  const [idSite, setIdSite] = React.useState(0);


  const Action = (rowData, refresh) => {
    return <ActionRefresh rowData={rowData} refresh={refresh} idSite={idSite} />;
  };

  /*
  @prop data-testid: Id to use inside placemanagercreate.test.js file.
 */
  return (
    <div data-testid={"PlaceManagerCreateTestId"}>
      <div className="grid grid-cols-2 gap-4">
        <div>
          <Typography variant="h4">
            <FormattedMessage
              id="none"
              defaultMessage={"Place Manager >>  Create" + " " + type}
            />
          </Typography>
          <br />
          <Typography variant="h5">
            <FormattedMessage
              id="none"
              defaultMessage="The left table will populate after a site has been chosen."
            />
          </Typography>
          <br />
        </div>
      </div>
      <div className="grid grid-cols-2 gap-4">
        <div className="flex-auto">
          <Card>
            <CardContent>
              <EbsGrid
                id="PlacesCreated"
                toolbar={true}
                entity={"GeospatialObject"}
                callstandard="graphql"
                data-testid={"PlacesCreatedTestId"}
                columns={columns}
                toolbaractions={Action}
                // fetch={fetch}
                uri={cbGraphqlUri}
                defaultFilters={[
                  { col: "id", mod: "EQ", val: Number(idSite) },
                ]}
                csvfilename="PlacesCreatedList"
                height="85vh"
                select="multi"
                disabledViewConfiguration
                disabledViewDownloadCSV
                disabledViewPrintOunt
              />
            </CardContent>
          </Card>
        </div>
        <div className="flex-auto">
          <Card>
            <CardContent>
              <Typography variant="h5">
                <FormattedMessage id="none" defaultMessage={"Create " + type} />
              </Typography>
              <br />
              <EbsTabsLayout
                orientation="horizontal"
                index={location?.state?.index || 0} //TODO:: Review this
                tabs={[
                  {
                    label: (
                      <FormattedMessage
                        id="none"
                        defaultMessage="Information"
                      />
                    ),
                    component: (
                      <div>
                        <FormPlaceManagerCreate
                          type={type}
                          setIdSite={setIdSite}
                          idSite={idSite}
                          setType={setType}
                        />
                      </div>
                    ),
                  },
                ]}
              />
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}
// Type and required properties
PlaceManagerCreateView.propTypes = {};
// Default properties
PlaceManagerCreateView.defaultProps = {};
