import PlaceManagerCreate from './PlaceManagerCreate';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('PlaceManagerCreate is in the DOM', () => {
  render(<PlaceManagerCreate></PlaceManagerCreate>)
  expect(screen.getByTestId('PlaceManagerCreateTestId')).toBeInTheDocument();
})
