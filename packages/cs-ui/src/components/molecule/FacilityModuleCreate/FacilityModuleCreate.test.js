import FacilityModuleCreate from './FacilityModuleCreate';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FacilityModuleCreate is in the DOM', () => {
  render(<FacilityModuleCreate></FacilityModuleCreate>)
  expect(screen.getByTestId('FacilityModuleCreateTestId')).toBeInTheDocument();
})
