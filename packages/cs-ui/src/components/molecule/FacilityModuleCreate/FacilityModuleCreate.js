import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Typography, Card, CardContent, Button } = Core;
const { Star } = Icons;
import { EbsTabsLayout } from "@ebs/styleguide";
import { useLocation } from "react-router-dom";
import { EbsGrid } from "@ebs/components";
import FormFacilityCreateAtom from "components/atom/FormFacilityCreate/FormFacilityCreate";

/*
  @param { }: page props,
*/
export default function FacilityModuleCreateView({ match }) {
  const location = useLocation();
  const type = location.state.type;
  const data = [];

  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "name",
      csvHeader: "Facility",
      width: 100,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "code",
      csvHeader: "Facility Code",
      width: 150,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility Type" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "type",
      csvHeader: "Facility Type",
      width: 125,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Site" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "site",
      csvHeader: "Site",
      width: 125,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Data Created" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "dataCreated",
      csvHeader: "Data Created",
      width: 125,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility Coordinates" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "coordinates",
      csvHeader: "Facility Coordinates",
      width: 190,
      filter: true,
    },
  ];

  const fetch = ({ page, sort, filter }) => {
    return new Promise((resolve, reject) => {
      try {
        resolve({
          data: data,
        });
      } catch (error) {
        reject(error);
      }
    });
  };
  return (
    <div data-testid={"PlaceManagerCreateTestId"}>
      <div className="grid grid-cols-2 gap-4">
        <div>
          <Typography variant="h4">
            <FormattedMessage
              id="none"
              defaultMessage={"Place Manager >>  Create" + " " + type}
            />
          </Typography>
          <br />
          <Typography variant="h6">
            <FormattedMessage
              id="none"
              defaultMessage="The left table will populate after a facility has been created."
            />
          </Typography>
          <br />
        </div>
        <div className="relative">
        </div>
      </div>
      <div className="grid grid-cols-2 gap-4">
        <div className="flex-auto">
          <Card>
            <CardContent>
              <EbsGrid
                id="PlacesCreated"
                toolbar={true}
                data-testid={"PlacesCreatedTestId"}
                columns={columns}
                csvfilename="PlacesCreatedList"
                fetch={fetch}
                height="85vh"
                select="multi"
                disabledViewConfiguration
                disabledViewDownloadCSV
                disabledViewPrintOunt
              />
            </CardContent>
          </Card>
        </div>
        <div className="flex-auto">
          <Card>
            <CardContent>
              <Typography variant="h6">
                <FormattedMessage id="none" defaultMessage={"Create " + type} />
              </Typography>
              <br />
              <EbsTabsLayout
                orientation="horizontal"
                index={location?.state?.index || 0} //TODO:: REVIEW the object properties of location
                tabs={[
                  {
                    label: (
                      <FormattedMessage
                        id="none"
                        defaultMessage="Information"
                      />
                    ),
                    component: (
                      <div>
                        <FormFacilityCreateAtom type={type} />
                      </div>
                    ),
                  },
                ]}
              />
            </CardContent>
          </Card>
        </div>
      </div>
    </div>
  );
}
// Type and required properties
FacilityModuleCreateView.propTypes = {};
// Default properties
FacilityModuleCreateView.defaultProps = {};
