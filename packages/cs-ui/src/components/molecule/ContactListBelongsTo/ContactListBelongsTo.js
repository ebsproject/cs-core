import React from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
import { useSelector } from "react-redux";
import { getCoreSystemContext } from "@ebs/layout";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
const { Box, Typography } = Core

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ContactListBelongsToMolecule = React.forwardRef(({ type }, ref) => {
  const { id } = useSelector(({ persist }) => persist?.defaultValues);
  const { graphqlUri } = getCoreSystemContext();
  const filter =
    type === "INSTITUTION"
      ? { mod: "EQ", col: "institution.id", val: id.toString() }
      : { mod: "EQ", col: "contact.id", val: id.toString() };
  let columns = [
    {
      Header: (
        <Typography className="font-ebs text-ebs-green-default text-xl">
          {(type === "INSTITUTION" && (
            <FormattedMessage id={"none"} defaultMessage={"Family Name"} />
          )) || <FormattedMessage id={"none"} defaultMessage={"Legal Name"} />}
        </Typography>
      ),
      accessor:
        (type == "INSTITUTION" && "contact.person.familyName") ||
        "institution.institution.legalName",
      csvHeader: (type == "INSTITUTION" && "Family Name") || "Legal Name",
      width: 500,
    },
    {
      Header: (
        <Typography className="font-ebs text-ebs-green-default text-xl">
          {(type === "INSTITUTION" && (
            <FormattedMessage id={"none"} defaultMessage={"Given Name"} />
          )) || <FormattedMessage id={"none"} defaultMessage={"Common Name"} />}
        </Typography>
      ),
      accessor:
        (type == "INSTITUTION" && "contact.person.givenName") ||
        "institution.institution.commonName",
      csvHeader: (type == "INSTITUTION" && "Given Name") || "Common Name",
      width: 500,
    },
    {
      Header: (
        <Typography className="font-ebs text-ebs-green-default text-xl">
          <FormattedMessage id={"none"} defaultMessage={"Email"} />
        </Typography>
      ),
      accessor:
        (type == "INSTITUTION" && "contact.email") || "institution.email",
      csvHeader: "Email",
      width: 500,
    },
  ];

  return (
    /**
     * @prop data-testid: Id to use inside ContactListBelongsTo.test.js file.
     */
    <div data-testid={"ContactListBelongsToTestId"} ref={ref}>
      <EbsGrid
        id="ContactListBelongs"
        title={
          <Typography className="font-ebs text-ebs-green-default text-xl">
            {(type === "PERSON" && (
              <FormattedMessage id={"none"} defaultMessage={"Institutions"} />
            )) || <FormattedMessage id={"none"} defaultMessage={"People"} />}
          </Typography>
        }
        columns={columns}
        uri={graphqlUri}
        entity="Hierarchy"
        defaultFilters={[filter]}
        callstandard={"graphql"}
        csvfilename="ContactListBelongsTo"
        height="35vh"
      />
    </div>
  );
});
// Type and required properties
ContactListBelongsToMolecule.propTypes = {
  type: PropTypes.oneOf(["PERSON", "INSTITUTION","USER"]),
};
// Default properties
ContactListBelongsToMolecule.defaultProps = {
  type: "PERSON",
};

export default ContactListBelongsToMolecule;
