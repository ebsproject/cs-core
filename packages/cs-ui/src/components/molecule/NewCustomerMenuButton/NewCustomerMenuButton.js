import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND ATOMS TO USE
import AddCustomerButton from "components/atom/AddCustomerButton";
import {Core} from "@ebs/styleguide";
const { Tooltip }= Core

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewCustomerMenuButton = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    return (
      /* 
   @prop data-testid: Id to use inside newdomainsmenubutton.test.js file.
   */
      <div ref={ref} data-testid={"NewCustomerMenuButtonTestId"} className="px-2">
        <Tooltip
          placement="top-start"
          title={
            <FormattedMessage id="none" defaultMessage="ADD CUSTOMER" />
          }
        >
          <AddCustomerButton
            rowSelected={selectedRows}
            refresh={refresh}
          />
        </Tooltip>
      </div>
    );
  }
);
// Type and required properties
NewCustomerMenuButton.propTypes = {};
// Default properties
NewCustomerMenuButton.defaultProps = {};

export default NewCustomerMenuButton;
