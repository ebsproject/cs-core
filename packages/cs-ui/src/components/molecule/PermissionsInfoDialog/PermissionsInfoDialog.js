import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import PermissionsInfoForm from "components/atom/PermissionsInfoForm";
import { Core, Icons, EbsDialog } from "@ebs/styleguide";
const {
  Box,
  Button,
  Typography,
} = Core;
const { PostAdd } = Icons;
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const PermissionsInfoDialogMolecule = React.forwardRef(
  ({ contactId, userName, refresh, method, defaultValues, allowAddTeam }, ref) => {
    const [open, setOpen] = useState(false);
    const { success } = useSelector(({ team }) => team);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    /**
     * @returns {func}
     */
    const handleClose = () => {
      setOpen(false);
    }
    /**
     * @returns {func}
     */
    const handleOpen = () => setOpen(true);
    return (
      /**
       * @prop data-testid: Id to use inside PermissionsInfoDialog.test.js file.
       */
      <Box component="div" ref={ref} data-testid={"PermissionsInfoDialogTestId"}>

        <Button
          variant="contained"
          aria-label="add-contactInfo"
          className="bg-ebs-brand-default rounded-md text-white hover:bg-ebs-brand-900 ml-3"
          startIcon={<PostAdd className="fill-current text-white" />}
          onClick={handleOpen}
          data-testid={"PostButton"}
        >
          <Typography className="text-white">
            <FormattedMessage id="none" defaultMessage="Assign Team" />
          </Typography>
        </Button>

        <EbsDialog
          open={open}
          handleClose={handleClose}
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Team"}
              />
            </Typography>
          }
          maxWidth="sm"
        >
          {!allowAddTeam ? (
            <Box className=" p-5 m-5 text-center" >
              <Typography className="font-ebs text-lg">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Please Remove the team-role assigned before add a new one."}
                />
              </Typography>
            </Box>

          ) : (

            <PermissionsInfoForm
              defaultValues={defaultValues}
              handleClose={handleClose}
              contactId={contactId}
              method={method}
              userName={userName}
            />)
          }
        </EbsDialog>
      </Box>
    );
  }
);
// Type and required properties
PermissionsInfoDialogMolecule.propTypes = {};
// Default properties
PermissionsInfoDialogMolecule.defaultProps = {};

export default PermissionsInfoDialogMolecule;
