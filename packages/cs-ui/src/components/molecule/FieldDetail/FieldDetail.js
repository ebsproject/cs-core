import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS AND MOLECULES TO USE

const { Typography } = Core;
import { clientCb } from "utils/apollo/apollo";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";
import { EbsGrid } from "@ebs/components";
import PlaceRowActions from "components/molecule/PlaceRowActions/PlaceRowActions";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/

const FieldDetailMolecule = React.forwardRef(({ rowData }, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      accessor: "null",
      width: 174,
      // filter: false,
    },
    {
      accessor: "null2",
      width: 200,
      // filter: false,
    },
    {
      accessor: "null3",
      width: 200,
      // filter: false,
    },
    {
      accessor: "null4",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "field.geospatialObjectName",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "field.geospatialObjectCode",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "planting.geospatialObjectName",
      width: 200,
      // filter: false,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "planting.geospatialObjectCode",
      width: 200,
      // filter: false,
    },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Field" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "dateCreated",
    //   width: 200,
    //   // filter: false,
    // },
    {
      Cell: ({ value }) => {
        return (
          <Typography variant="body1">
            {value?.type === "Polygon"
              ? value?.coordinates[0][0]
              : value?.coordinates[0]}
          </Typography>
        );
      },
      accessor: "coordinates.features[0].geometry",
      csvHeader: "Coordinates",
      width: 200,
      filter: true,
    },
  ];

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        clientCb
          .query({
            query: FIND_PLACES_LIST,
            variables: {
              page: page,
              filters: [
                ...filters,
                {
                  col: "parentGeospatialObject.id",
                  mod: "EQ",
                  val: rowData.id,
                },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              let dataList = data.findGeospatialObjectList.content;
              const listParents = dataList.map((item) => {
                switch (item.geospatialObjectType) {
                  case "field":
                    const dataField = {
                      id: item.id,
                      geospatialObjectType: item.geospatialObjectType,
                      altitude: item.altitude,
                      description: item.description,
                      coordinates: item.coordinates,
                      dateCreated: item.dateCreated,
                      parentGeospatialObject: item.parentGeospatialObject,
                      field: {
                        geospatialObjectType: item.geospatialObjectType,
                        geospatialObjectName: item.geospatialObjectName,
                        geospatialObjectCode: item.geospatialObjectCode,
                      },
                    };
                    return dataField;
                  case "planting area":
                    const dataPla = {
                      id: item.id,
                      geospatialObjectCode: item.geospatialObjectCode,
                      geospatialObjectType: item.geospatialObjectType,
                      altitude: item.altitude,
                      description: item.description,
                      coordinates: item.coordinates,
                      dateCreated: item.dateCreated,
                      parentGeospatialObject: item.parentGeospatialObject,
                      planting: {
                        geospatialObjectType: item.geospatialObjectType,
                        geospatialObjectName: item.geospatialObjectName,
                        geospatialObjectCode: item.geospatialObjectCode,
                      },
                    };
                    return dataPla;
                }
              });
              resolve({
                pages: data.findGeospatialObjectList.totalPages,
                elements: data.findGeospatialObjectList.totalElements,
                data: listParents,
              });
            }
          });
      } catch (error) {
        reject(error);
      }
    });
  };
  const DetailComponent = ({ rowData: data }) => {
    if (data.geospatialObjectType !== "planting area") {
      const columnsPlanting = [
        {
          Header: "Id",
          accessor: "id",
          hidden: true,
          disableGlobalFilter: true,
        },
        {
          accessor: "null",
          width: 174,
          // filter: false,
        },
        {
          accessor: "null2",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null3",
          width: 200,
          // filter: false,
        },
        {
          accessor: "null4",
          width: 200,
          // filter: false,
        },
        {
          accessor: "field6",
          width: 200,
          // filter: false,
        },
        {
          accessor: "field7",
          width: 200,
          // filter: false,
        },
        {
          Cell: ({ value }) => {
            return <Typography variant="body1">{value}</Typography>;
          },
          accessor: "geospatialObjectName",
          width: 200,
          // filter: false,
        },
        {
          Cell: ({ value }) => {
            return <Typography variant="body1">{value}</Typography>;
          },
          accessor: "geospatialObjectCode",
          width: 200,
          // filter: false,
        },
        // {
        //   Cell: ({ value }) => {
        //     return <Typography variant="body1">{value}</Typography>;
        //   },
        //   accessor: "dateCreated",
        //   width: 200,
        //   // filter: false,
        // },
        {
          Cell: ({ value }) => {
            return (
              <Typography variant="body1">
                {value?.type === "Polygon"
                  ? value?.coordinates[0][0]
                  : value?.coordinates[0]}
              </Typography>
            );
          },
          accessor: "coordinates.features[0].geometry",
          csvHeader: "Coordinates",
          width: 200,
          filter: true,
        },
      ];
      const fetchPlanting = async ({ page, sort, filters }) => {
        return new Promise((resolve, reject) => {
          try {
            clientCb
              .query({
                query: FIND_PLACES_LIST,
                variables: {
                  page: page,
                  filters: [
                    ...filters,
                    {
                      col: "parentGeospatialObject.id",
                      mod: "EQ",
                      val: data.id,
                    },
                  ],
                  disjunctionFilters: false,
                },
                fetchPolicy: "no-cache",
              })
              .then(({ data, errors }) => {
                if (errors) {
                  reject(errors);
                } else {
                  resolve({
                    pages: data.findGeospatialObjectList.totalPages,
                    elements: data.findGeospatialObjectList.totalElements,
                    data: data.findGeospatialObjectList.content,
                  });
                }
              });
          } catch (error) {
            reject(error);
          }
        });
      };

      return (
        <div data-testid={"DivPlantingArea"} style={{ width: 2000 }}>
          <EbsGrid
            id="PlantingArea"
            toolbar={false}
            data-testid={"PlantingArea"}
            columns={columnsPlanting}
            rowactions={PlaceRowActions}
            csvfilename="PlacesList"
            fetch={fetchPlanting}
            detailcomponent
            height="85vh"
            select="multi"
            disabledHeadTable
            color="terciary"
          />
        </div>
      );
    }
  };

  return (
    /* 
     @prop data-testid: Id to use inside FieldDetail.test.js file.
     */
    <div data-testid={"FieldTestId"} style={{ width: 2000 }}>
      <EbsGrid
        id="Fields"
        toolbar={false}
        data-testid={"FieldTestId"}
        columns={columns}
        rowactions={PlaceRowActions}
        csvfilename="FieldTestId"
        detailcomponent={DetailComponent}
        fetch={fetch}
        height="85vh"
        select="multi"
        disabledHeadTable
        color="secondary"
      />
    </div>
  );
});
// Type and required properties
FieldDetailMolecule.propTypes = {};
// Default properties
FieldDetailMolecule.defaultProps = {};

export default FieldDetailMolecule;
