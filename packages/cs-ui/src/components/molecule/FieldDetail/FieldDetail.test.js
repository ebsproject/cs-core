import FieldDetail from './FieldDetail';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FieldDetail is in the DOM', () => {
  render(<FieldDetail></FieldDetail>)
  expect(screen.getByTestId('FieldDetailTestId')).toBeInTheDocument();
})
