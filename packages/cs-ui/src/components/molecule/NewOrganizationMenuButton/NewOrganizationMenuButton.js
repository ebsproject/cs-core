import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import {Core} from "@ebs/styleguide";
const { Tooltip } = Core;
import AddOrganizationsButton from "components/atom/AddOrganizationButton/AddOrganizationButton";

const NewOrganizationButtonMenuMolecule = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    return (
      <div ref={ref} data-testid={"NewOrganizationMenuButton"} className="px-2">
        <Tooltip
          placement="top-start"
          title={
            <FormattedMessage id="none" defaultMessage="ADD ORGANIZATION" />
          }
        >
          <AddOrganizationsButton
            rowSelected={selectedRows}
            refresh={refresh}
          />
        </Tooltip>
      </div>
    );
  }
);

NewOrganizationButtonMenuMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
  selectedRows: PropTypes.array,
};

NewOrganizationButtonMenuMolecule.defaultProps = {};

export default NewOrganizationButtonMenuMolecule;
