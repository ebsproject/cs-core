import PlaceRowActions from './PlaceRowActions';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('PlaceRowActions is in the DOM', () => {
  render(<PlaceRowActions></PlaceRowActions>)
  expect(screen.getByTestId('PlaceRowActionsTestId')).toBeInTheDocument();
})
