import DetailComponentFacility from './DetailComponentFacility';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('DetailComponentFacility is in the DOM', () => {
  render(<DetailComponentFacility></DetailComponentFacility>)
  expect(screen.getByTestId('DetailComponentFacilityTestId')).toBeInTheDocument();
})
