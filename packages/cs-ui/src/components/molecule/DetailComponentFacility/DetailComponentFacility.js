import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Styles } from "@ebs/styleguide";
import { EbsGrid } from "@ebs/components";
import { clientCb } from "utils/apollo/apollo";
import { FIND_FACILITY_LIST } from "utils/apollo/gql/placeManager";
import RowActionsDetailFacilityMolecule from "../RowActionsDetailFacility/RowActionsDetailFacility";
const { Typography } = Core;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DetailComponentFacilityMolecule = React.forwardRef(
  ({ rowData, type, SetListData }, ref) => {
    // const columns = [
    //   { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    //   {
    //     accessor: "null",
    //     width: 180,
    //     // filter: false,
    //   },
    //   {
    //     accessor: "null2",
    //     width: 180,
    //     // filter: false,
    //   },
    //   {
    //     Cell: ({ value }) => {
    //       return <Typography variant="body1">{value}</Typography>;
    //     },
    //     accessor: "field.geospatialObjectName",
    //     width: 180,
    //     // filter: false,
    //   },
    //   {
    //     Cell: ({ value }) => {
    //       return <Typography variant="body1">{value}</Typography>;
    //     },
    //     accessor: "planting.geospatialObjectName",
    //     width: 180,
    //     // filter: false,
    //   },
    // ];
    const columns = [
      { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
      {
        accessor: "null2",
        width: 150,
        // filter: false,
      },
      {
        accessor: "null3",
        width: 150,
        // filter: false,
      },
      {
        accessor: "null4",
        width: 150,
        // filter: false,
      },
      {
        accessor: "null5",
        width: 150,
        // filter: false,
      },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value}</Typography>;
        },
        accessor: "subFacility.facilityName",
        width: 150,
        // filter: false,
      },
      // {
      //   Cell: ({ value }) => {
      //     return <Typography variant="body1">{value}</Typography>;
      //   },
      //   accessor: "subFacility.facilityCode",
      //   width: 150,
      // },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value}</Typography>;
        },
        accessor: "subFacility.facilityType",
        width: 150,
      },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value}</Typography>;
        },
        accessor: "container.facilityName",
        width: 150,
        // filter: false,
      },
      // {
      //   Cell: ({ value }) => {
      //     return <Typography variant="body1">{value}</Typography>;
      //   },
      //   accessor: "container.facilityCode",
      //   width: 150,
      //   // filter: false,
      // },
      // {
      //   Cell: ({ value }) => {
      //     return <Typography variant="body1">{value}</Typography>;
      //   },
      //   accessor: "dateCreated",
      //   width: 150,
      //   // filter: false,
      // },
    ];
    const fetch = async ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        try {
          clientCb
            .query({
              query: FIND_FACILITY_LIST,
              variables: {
                page: page,
                filters: [
                  ...filters,
                  {
                    col: "parentFacility.id",
                    mod: "EQ",
                    val: rowData.id,
                  },
                ],
                disjunctionFilters: false,
              },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                let dataList = data.findFacilityList.content;
                const listParents = dataList.map((item) => {
                  switch (item.facilityClass) {
                    case "structure":
                      const dataSubFaci = {
                        id: item.id,
                        altitude: item.altitude,
                        description: item.description,
                        coordinates: item.coordinates,
                        dateCreated: item.dateCreated,
                        parentFacility: item.parentFacility,
                        facilityClass: item.facilityClass,
                        subFacility: {
                          facilityClass: item.facilityClass,
                          facilityName: item.facilityName,
                          facilityCode: item.facilityCode,
                          facilityType: item.facilityType,
                        },
                      };
                      return dataSubFaci;
                    case "container":
                      const dataContainer = {
                        id: item.id,
                        altitude: item.altitude,
                        description: item.description,
                        coordinates: item.coordinates,
                        dateCreated: item.dateCreated,
                        parentFacility: item.parentFacility,
                        facilityClass: item.facilityClass,
                        container: {
                          facilityClass: item.facilityClass,
                          facilityName: item.facilityName,
                          facilityCode: item.facilityCode,
                          facilityType: item.facilityType,
                        },
                      };
                      return dataContainer;
                  }
                });
                resolve({
                  pages: data.findFacilityList.totalPages,
                  elements: data.findFacilityList.totalElements,
                  data: listParents,
                });
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    };

    const rowActionsDetail = (rowData, refresh) => {
      return (
        <RowActionsDetailFacilityMolecule
          type={type}
          rowData={rowData}
          SetListData={SetListData}
          refresh={refresh}
        />
      );
    };

    const DetailComponent = ({ rowData: data }) => {
      if (data.facilityClass !== "container") {
        const columnsContainer = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            accessor: "null2",
            width: 150,
            // filter: false,
          },
          {
            accessor: "null3",
            width: 150,
            // filter: false,
          },
          {
            accessor: "null4",
            width: 150,
            // filter: false,
          },
          {
            accessor: "null5",
            width: 150,
            // filter: false,
          },
          // {
          //   accessor: "null6",
          //   width: 150,
          //   // filter: false,
          // },
          {
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityName",
            width: 150,
            // filter: false,
          },
          {
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityCode",
            width: 150,
            // filter: false,
          },
          {
            accessor: "null6",
            width: 150,
          },
          // {
          //   Cell: ({ value }) => {
          //     return <Typography variant="body1">{value}</Typography>;
          //   },
          //   accessor: "dateCreated",
          //   width: 150,
          //   // filter: false,
          // },
        ];
        const fetchContainer = async ({ page, sort, filters }) => {
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    page: page,
                    filters: [
                      ...filters,
                      {
                        col: "parentFacility.id",
                        mod: "EQ",
                        val: data.id,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    resolve({
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                      data: data.findFacilityList.content,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        };

        const DetailComponentAction = () => {
          return null;
        };
        return (
          <div data-testid={"DivContainer"} style={{ width: 1470 }}>
            <EbsGrid
              id="Container"
              toolbar={false}
              data-testid={"Container"}
              columns={columnsContainer}
              rowactions={rowActionsDetail}
              csvfilename="PlacesList"
              fetch={fetchContainer}
              detailcomponent={DetailComponentAction}
              height="85vh"
              select="multi"
              disabledHeadTable
              color="terciary"
            />
          </div>
        );
      }
    };

    return (
      <div data-testid={"FacilityTestId"} style={{ width: 1470 }}>
        <EbsGrid
          id="Facility"
          toolbar={false}
          data-testid={"PlacesTestId"}
          columns={columns}
          rowactions={rowActionsDetail}
          csvfilename="PlacesList"
          detailcomponent={DetailComponent}
          fetch={fetch}
          height="85vh"
          select="multi"
          disabledHeadTable
          color="secondary"
        />
      </div>
    );
  }
);
// Type and required properties
DetailComponentFacilityMolecule.propTypes = {};
// Default properties
DetailComponentFacilityMolecule.defaultProps = {};

export default DetailComponentFacilityMolecule;
