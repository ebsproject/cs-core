import PlaceToolbarActions from './PlaceToolbarActions';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('PlaceToolbarActions is in the DOM', () => {
  render(<PlaceToolbarActions></PlaceToolbarActions>)
  expect(screen.getByTestId('PlaceToolbarActionsTestId')).toBeInTheDocument();
})
