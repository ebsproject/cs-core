import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { RBAC } from "@ebs/layout";
import { Core, Icons } from "@ebs/styleguide";
import { Link, useNavigate } from "react-router-dom";
const {
  Typography,
  Tooltip,
  Button,
  Grow,
  Paper,
  ClickAwayListener,
  Grid,
  ButtonGroup,
  Popper,
  MenuItem,
  MenuList,
} = Core;
const { ArrowDropDown, AddCircle } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const PlaceToolbarActionsMolecule = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);
    const navigate = useNavigate();
    const options = ["Site", "Field", "Planting Area"];

    const handleMenuItemClick = (event, option) => {
      setOpen(false);
      navigate( "/cs/place-manager/create",{
        state: {
          type: option,
        },
      });
    };

    const handleToggle = () => {
      setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
      if (anchorRef.current && anchorRef.current.contains(event.target)) {
        return;
      }
      setOpen(false);
    };

    return (
      <div>
        <RBAC allowedAction={"Create"}>
          <Grid container direction="column" alignItems="center">
            <Grid item xs={12}>
              <ButtonGroup
                variant="contained"
                color="primary"
                className=" bg-ebs-brand-default"
                ref={anchorRef}
              >
                <Button
                  disabled
                  startIcon={
                    <AddCircle className="fill-current text-white ml-3" />
                  }
                >
                  <Typography className="text-white text-sm font-ebs">
                    <FormattedMessage id="none" defaultMessage="CREATE" />
                  </Typography>
                </Button>
                <Button
                  color="primary"
                  size="small"
                  aria-controls={open ? "split-button-menu" : undefined}
                  aria-expanded={open ? "true" : undefined}
                  aria-label="select merge strategy"
                  aria-haspopup="menu"
                  onClick={handleToggle}
                >
                  <ArrowDropDown />
                </Button>
              </ButtonGroup>
              <Popper open={open} anchorEl={anchorRef.current} transition>
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{
                      transformOrigin:
                        placement === "bottom" ? "center top" : "center bottom",
                    }}
                  >
                    <Paper>
                      <ClickAwayListener onClickAway={handleClose}>
                        <MenuList id="split-button-menu">
                          {options.map((option, index) => (
                            <MenuItem
                              key={option}
                              onClick={(event) =>
                                handleMenuItemClick(event, option)
                              }
                            >
                              {option}
                            </MenuItem>
                          ))}
                        </MenuList>
                      </ClickAwayListener>
                    </Paper>
                  </Grow>
                )}
              </Popper>
            </Grid>
          </Grid>
        </RBAC>
      </div>
    );
  }
);
// Type and required properties
PlaceToolbarActionsMolecule.propTypes = {};
// Default properties
PlaceToolbarActionsMolecule.defaultProps = {};

export default PlaceToolbarActionsMolecule;
