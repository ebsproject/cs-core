import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import { roleProcess } from "store/modules/DataPersist";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Box, Button, Tooltip, Typography } = Core;
const { PostAdd } = Icons;
import { RBAC } from "@ebs/layout";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const RolesToolbarMolecule = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const handleGo = () => {
      dispatch(roleProcess({ method: "POST" }));
      navigate("/cs/usermanagement/role");
    };
    return (
      /**
       * @prop data-testid: Id to use inside RolesToolbar.test.js file.
       */
      <div
        component="div"
        ref={ref}
        data-testid={"RolesToolbarTestId"}
        className="ml-3"
      >
        <Box
          sx={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <RBAC
            allowedAction={"Create_Role"}
            product={"User Management"}
            domain={"Core System"}
          >
            <Tooltip
              arrow
              title={
                <Typography className="font-ebs text-xl">
                  <FormattedMessage
                    id={"none"}
                    defaultMessage={"Add new role"}
                  />
                </Typography>
              }
            >
              <Button
                onClick={handleGo}
                startIcon={<PostAdd />}
                className="bg-ebs-brand-default hover:bg-ebs-brand-900 border-l-8 text-white"
              >
                <Typography className="text-sm font-ebs">
                  <FormattedMessage id={"none"} defaultMessage={"New Role"} />
                </Typography>
              </Button>
            </Tooltip>
          </RBAC>
        </Box>
      </div>
    );
  }
);
// Type and required properties
RolesToolbarMolecule.propTypes = {
  selectedRows: PropTypes.array,
  refresh: PropTypes.func,
};
// Default properties
RolesToolbarMolecule.defaultProps = {};

export default RolesToolbarMolecule;
