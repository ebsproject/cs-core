import PlaceManagerModify from './PlaceManagerModify';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('PlaceManagerModify is in the DOM', () => {
  render(<PlaceManagerModify></PlaceManagerModify>)
  expect(screen.getByTestId('PlaceManagerModifyTestId')).toBeInTheDocument();
})
