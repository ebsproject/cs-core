import React, { useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Typography, Card, CardContent, Button } = Core;
import { EbsTabsLayout } from "@ebs/styleguide";
import { useNavigate, useLocation } from "react-router-dom";
import { EbsGrid } from "@ebs/components";
import { useSelector } from "react-redux";
import FormPlaceManagerModifyAtom from "components/atom/FormPlaceManagerModify";
import { clientCb } from "utils/apollo/apollo";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";
import DetailComponentSiteMolecule from "components/molecule/DetailComponentSite/DetailComponentSite";
import ViewDetailAtom from "components/atom/ViewDetail/ViewDetail";
import RowActionsDetailsMolecule from "components/molecule/RowActionsDetails/RowActionsDetails";

/*
  @param { }: page props,
*/
export default function PlaceManagerModifyView({ match }) {
  const [listData, setListData] = React.useState(null);
  const navigate = useNavigate();
  const location = useLocation();
  const type = location.state.type;
  // const dispatch = useDispatch();
  const { placemanager } = useSelector((placemanager) => placemanager);
  useEffect(() => {
    if (placemanager.setDataPlace === null) {
      navigate(`/cs/place-manager`);
    }
  }, [placemanager]);
  const columnsOption = () => {
    switch (placemanager?.setDataPlace?.gridType) {
      case "site":
        const columnsSite = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Site" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectName",
            csvHeader: "Site",
            width: 180,
            // filter: false,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Site Code" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectCode",
            csvHeader: "Site Code",
            width: 180,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Field" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1"></Typography>;
            },
            accessor: "field",
            csvHeader: "Field",
            width: 180,
            filter: false,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Planting Area" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1"></Typography>;
            },
            accessor: "plantingArea",
            csvHeader: "Planting Area",
            width: 180,
            filter: false,
          },
        ];
        return columnsSite;
      case "field":
        const columnsField = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Field" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectName",
            csvHeader: "Field",
            width: 180,
            // filter: false,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Field Code" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectCode",
            csvHeader: "Field Code",
            width: 180,
            filter: true,
          },
          // {
          //   Header: (
          //     <Typography variant="h5" color="primary">
          //       <FormattedMessage id="none" defaultMessage="Date Created" />
          //     </Typography>
          //   ),
          //   Cell: ({ value }) => {
          //     return <Typography variant="body1">{value}</Typography>;
          //   },
          //   accessor: "dateCreated",
          //   csvHeader: "Date Created",
          //   width: 180,
          //   filter: true,
          // },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Planting Area" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1"></Typography>;
            },
            accessor: "plantingArea",
            csvHeader: "Planting Area",
            width: 180,
            filter: false,
          },
        ];
        return columnsField;
      case "planting area":
        const columnsArea = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Planting Area" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectName",
            csvHeader: "Planting Area",
            width: 180,
            // filter: false,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage
                  id="none"
                  defaultMessage="Planting Area Code"
                />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectCode",
            csvHeader: "Planting Area Code",
            width: 200,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h5" color="primary">
                <FormattedMessage id="none" defaultMessage="Coordinates" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return (
                <Typography variant="body1">
                  {value?.type === "Polygon"
                    ? value?.coordinates[0][0]
                    : value?.coordinates[0]}
                </Typography>
              );
            },
            accessor: "coordinates.features[0].geometry",
            csvHeader: "Coordinates",
            width: 200,
            filter: true,
          },
        ];
        return columnsArea;
      default:
        const columns = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            accessor: "geospatialObjectName",
            width: 180,
            // filter: false,
          },
          {
            accessor: "geospatialObjectCode",
            width: 250,
            filter: true,
          },
        ];
        return columns;
    }
  };

  const fetch = ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        clientCb
          .query({
            query: FIND_PLACES_LIST,
            variables: {
              page: page,
              filters: [
                ...filters,
                {
                  col: "id",
                  mod: "EQ",
                  val: Number(placemanager.setDataPlace.id),
                },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              setListData(data.findGeospatialObjectList.content);
              resolve({
                pages: data.findGeospatialObjectList.totalPages,
                elements: data.findGeospatialObjectList.totalElements,
                data: data.findGeospatialObjectList.content,
              });
            }
          });
      } catch (error) {
        reject(error);
      }
    });
  };

  const DetailComponent = ({ rowData }) => {
    return (
      <DetailComponentSiteMolecule
        rowData={rowData}
        type={type}
        setListData={setListData}
      />
    );
  };

  const RowActionsDetail = (rowData, refresh) => {
    return (
      <RowActionsDetailsMolecule
        type={type}
        rowData={rowData}
        setListData={setListData}
        refresh={refresh}
      />
    );
  };

  /*
  @prop data-testid: Id to use inside placemanagercreate.test.js file.
 */
  return (
    <div data-testid={"PlaceManagerCreateTestId"}>
      <div className="grid grid-cols-2 gap-4">
        <div>
          {type === "VIEW" ? (
            <Typography variant="h4">
              <FormattedMessage
                id="none"
                defaultMessage={
                  "Place Manager" +
                  " " +
                  ">" +
                  " " +
                  "Places View " +
                  " " +
                  "(" +
                  listData?.[0]?.geospatialObjectName +
                  ")"
                }
              />
            </Typography>
          ) : (
            <Typography variant="h4">
              <FormattedMessage
                id="none"
                defaultMessage={
                  "Place Manager" +
                  " " +
                  ">>" +
                  " " +
                  "Places View " +
                  " " +
                  "(" +
                  listData?.[0]?.geospatialObjectName +
                  ")"
                }
              />
            </Typography>
          )}

          <br />
          <Typography variant="h6">
            <FormattedMessage
              id="none"
              defaultMessage={
                "Use the left to navigate between the places assingned to " +
                " " +
                "(" +
                listData?.[0]?.geospatialObjectName +
                ")"
              }
            />
          </Typography>
        </div>
        <div className="relative">
          {type === "VIEW" ? (
            <Button
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900 absolute bottom-0 right-0"
              onClick={() => navigate(`/cs/place-manager`)}
            >
              Back
            </Button>
          ) : null}
        </div>
        <br />
      </div>
      <div className="grid grid-cols-2 gap-4">
        <div className="flex-auto">
          <Card
            variant="outlined"
            style={{ background: "#f4f4f4", height: "100%" }}
          >
            <CardContent>
              <Typography variant="h6">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    "Fields that belong to" +
                    " " +
                    "(" +
                    listData?.[0]?.geospatialObjectName +
                    ")"
                  }
                />
              </Typography>
              <EbsGrid
                id="PlacesFieldModify"
                toolbar={true}
                data-testid={"PlacesCreatedTestId"}
                columns={columnsOption()}
                detailcomponent={DetailComponent}
                rowactions={RowActionsDetail}
                csvfilename="PlacesCreatedList"
                fetch={fetch}
                height="85vh"
                select="multi"
                disabledViewConfiguration
                disabledViewDownloadCSV
                disabledViewPrintOunt
                disabledFiedPagination
                // disabledPagination
              />
            </CardContent>
          </Card>
        </div>
        <div className="flex-auto">
          <Card
            variant="outlined"
            style={{ background: "#f4f4f4", height: "100%" }}
          >
            <CardContent>
              {type === "VIEW" ? (
                <Typography variant="h6">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      "Information" +
                      " " +
                      "(" +
                      listData?.[0]?.geospatialObjectName +
                      ")"
                    }
                  />
                </Typography>
              ) : (
                <Typography variant="h6">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      "Information" +
                      " " +
                      "(" +
                      listData?.[0]?.geospatialObjectName +
                      ")"
                    }
                  />
                </Typography>
              )}
              <br />
              <div style={{ background: "#fff" }}>
                <EbsTabsLayout
                  orientation="horizontal"
                  index={history?.location?.state?.index || 0}
                  tabs={[
                    {
                      label: (
                        <FormattedMessage
                          id="none"
                          defaultMessage="Information"
                        />
                      ),
                      component:
                        type === "VIEW" ? (
                          <div>
                            <ViewDetailAtom
                              data={listData?.[0]}
                              type={type}
                              dataType={listData?.[0]?.geospatialObjectType}
                            />
                          </div>
                        ) : (
                          <div>
                            <FormPlaceManagerModifyAtom
                              data={listData?.[0]}
                              type={type}
                            />
                          </div>
                        ),
                    },
                  ]}
                />
              </div>
            </CardContent>
          </Card>
        </div>
      </div>
      {/* <FormDetailsUpdateAtom
        open={open}
        setOpenDetail={setOpenDetail}
        dataDetail={dataDetail}
        type={dataType}
        dataHierarchy={dataHierarchy}
      /> */}
    </div>
  );
}
// Type and required properties
PlaceManagerModifyView.propTypes = {};
// Default properties
PlaceManagerModifyView.defaultProps = {};
