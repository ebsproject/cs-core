import ContactInfo from "./ContactInfo";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("ContactInfo is in the DOM", () => {
  render(<ContactInfo refresh={() => {}}></ContactInfo>, { wrapper: Wrapper });
  expect(screen.getByTestId("ContactInfoTestId")).toBeInTheDocument();
});
