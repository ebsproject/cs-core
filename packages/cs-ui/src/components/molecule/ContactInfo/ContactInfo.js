import React from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
import { getCoreSystemContext } from "@ebs/layout";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core} from "@ebs/styleguide";
import ContactInfoTypeForm from "components/atom/ContactInfoTypeForm";
import ContactInfoDialog from "components/molecule/ContactInfoDialog";
import DeleteContactInfo from "components/atom/DeleteContactInfo";
const { Box, Checkbox, Typography } = Core;

//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const ContactInfo = React.forwardRef(({ contactId }, ref) => {
  const { graphqlUri } = getCoreSystemContext();
  // Columns
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Type" />
        </Typography>
      ),
      accessor: "contactInfoType.name",
      csvHeader: "Type",
      width: 600,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Value" />
        </Typography>
      ),
      accessor: "value",
      csvHeader: "Value",
      width: 700,
    },
    {
      Header: "Id",
      accessor: "contactInfoType.id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Default" />
        </Typography>
      ),
      Cell: ({ cell, value }) => {
        if (cell.isAggregated > 0) {
          return <></>;
        } else {
          return <Checkbox checked={value} disabled />;
        }
      },
      accessor: "default",
      csvHeader: "Default",
      width: 100,
      disableResizing: true,
      disableGlobalFilter: true,
    },
  ];

  /**
   * @param dataSelection: Array object with data rows selected.
   * @param refresh: Function to refresh Grid data.
   */
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box className="flex flex-row flex-nowrap">
        <ContactInfoDialog
          contactId={contactId}
          refresh={refresh}
          method={"POST"}
        />
        <ContactInfoTypeForm refresh={refresh} />
      </Box>
    );
  };

  /**
   * @param rowData: Object with row data.
   * @param refresh: Function to refresh Grid data.
   */
  const rowActions = (rowData, refresh) => {
    return (
      <Box className="flex flex-row flex-nowrap gap-2">
        <ContactInfoDialog
          defaultValues={{ ...rowData, contactId: contactId }}
          contactId={contactId}
          refresh={refresh}
          method="PUT"
        />
        <DeleteContactInfo contactInfoId={rowData.id} refresh={refresh} />
      </Box>
    );
  };
  /**
   * @prop data-testid: Id to use inside grid.test.js file.
   */
  return (
    <div data-testid={"ContactInfoTestId"}>
      <EbsGrid
        id="ContactInfo"
        toolbar={true}
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        entity="ContactInfo"
        uri={graphqlUri}
        callstandard="graphql"
        defaultFilters={[{ col: "contact.id", mod: "EQ", val: contactId }]}
        height="25vh"
        select="multi"
        csvfilename="AdditionalInformation"
      />
    </div>
  );
});

// Type and required properties
ContactInfo.propTypes = {
  contactId: PropTypes.number,
};
// Default properties
ContactInfo.defaultProps = {};

export default ContactInfo;
