import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
import { useDispatch } from "react-redux";
import { useQuery } from "@apollo/client";
import {Core,Icons,Styles} from "@ebs/styleguide";
import { QUERY_ROWS, FIND_TENANT_ID } from "utils/apollo/gql/tenant";
import {
  DELETE_DOMAIN_INSTANCE,
  MODIFY_DOMAIN_INSTANCE,
  CREATE_DOMAIN_INSTANCE,
  MODIFY_INSTANCE,
  MODIFY_TENANT,
} from "utils/apollo/gql/tenantManagement";
import {
  CREATE_TENANT,
} from "utils/apollo/gql/tenant"
import { showMessage } from "store/modules/message";
const {
  Avatar,
  ListItem,
  ListItemAvatar,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  Divider,
  FormControl,
  Icon,
  InputLabel,
  List,
  ListItemSecondaryAction,
  ListItemText,
  MenuItem,
  Switch,
  TextField,
  Typography,
  DialogTitle,
  Grid,
  DialogActions,
  Select,
} = Core;
const {PostAdd} = Icons;
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AddTenantButton = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    // * Setting Default Values
    const [tenantFormValues, setTenantFormValues] = useState({
      name: "",
      organization: "1",
      customer: "1",
      crop: "0",
      address: "2",
      activeDomains: []
    });
    
const classes = {
  formControl: {
    minWidth: "100%",
  },
};
    const dispatch = useDispatch();

    const { error: errorOrg, data: Organizations } = useQuery(
      QUERY_ROWS("Organization", "content { id name}"),
      {
        variables: { size: 100, number: 1 },
        client: client,
      }
    );
    const { error: errorCust, data: Customers } = useQuery(
      QUERY_ROWS("Customer", "content { id name}"),
      {
        variables: { size: 100, number: 1 },
        client: client,
      }
    );
    const { error: errorCrop, data: Crops } = useQuery(
      QUERY_ROWS("Crop", "content { id name}"),
      {
        variables: { size: 100, number: 1 },
        client: client,
      }
    );
    // * Domains List
    const { error: errorDomain, data: Domains } = useQuery(
      QUERY_ROWS(
        "Domain",
        "content { id name info icon core domaininstances{context sgContext} }"
      ),
      {
        variables: { size: 100, number: 1 },
        client: client,
      }
    );
    
    useEffect(() => {
      let activeDomains = [];
      if (Domains && Domains.findDomainList) {
        Domains.findDomainList.content.map((domain) => {
          if (domain.core)
            activeDomains.push({isActive: true, ...domain});
        })
      }
      setTenantFormValues({...tenantFormValues, activeDomains: activeDomains});
    }, [Domains]);
    
    const handleClickOpen = () => {
      let activeDomains = [];
      if (Domains && Domains.findDomainList) {
        Domains.findDomainList.content.map((domain) => {
          if (domain.core)
            activeDomains.push({isActive: true, ...domain});
        })
      }
      setTenantFormValues({...tenantFormValues, activeDomains: activeDomains});
      setOpen(true);
    };

    const handleClose = () => {
      setTenantFormValues({
        name: "",
        organization: "1",
        customer: "1",
        crop: "0",
        address: "2",
        activeDomains: []
      });
      setOpen(false);
    };

    const handleChange = (label, newValue) => {
      setTenantFormValues({ ...tenantFormValues, [label]: newValue });
    };

    const handleContextChange = (domainName, context) => {
      let newFormValues = tenantFormValues;
      const index = newFormValues.activeDomains.findIndex(
        (domain) => domain.name === domainName
      );
      newFormValues.activeDomains[index].context = context;
      setTenantFormValues(newFormValues);
    };

    const handleSgContextChange = (domainName, sgContext) => {
      let newFormValues = tenantFormValues;
      const index = newFormValues.activeDomains.findIndex(
        (domain) => domain.name === domainName
      );
      newFormValues.activeDomains[index].sgContext = sgContext;
      setTenantFormValues(newFormValues);
    };

    const buildContextFields = (domainName) => {
      const index = tenantFormValues.activeDomains.findIndex(
        (domain) => domain.name === domainName
      );
      if (index != -1 && tenantFormValues.activeDomains[index].isActive)
        return (
          <FormControl fullWidth>
            <TextField
              label={
                <FormattedMessage id="none" defaultMessage="Web App URL" />
              }
              onChange={(e) =>
                handleContextChange(
                  tenantFormValues.activeDomains[index].name,
                  e.target.value
                )
              }
              inputProps={{
                type: "url",
              }}
              //disabled={tenantFormValues.activeDomains[index].domain.core || false}
              required
              defaultValue={tenantFormValues.activeDomains[index].context}
            />
            <TextField
              label={
                <FormattedMessage
                  id="none"
                  defaultMessage="Service Gateway URL"
                />
              }
              onChange={(e) =>
                handleSgContextChange(
                  tenantFormValues.activeDomains[index].name,
                  e.target.value
                )
              }
              inputProps={{
                type: "url",
              }}
              //disabled={tenantFormValues.activeDomains[index].domain.core || false}
              required
              defaultValue={tenantFormValues.activeDomains[index].sgContext}
            />
          </FormControl>
        );
    };

    const handleSwitchChange = (event, domainData) => {
      let index = tenantFormValues.activeDomains.findIndex(
        (domain) => domain.name === event.target.name
      );
      let newFormValues = tenantFormValues;
      // ? The Domain already exist on this Tenant
      if (index != -1) {
        newFormValues.activeDomains.splice(index, 1);
      } else {
        // * Otherwise enable the domain
        newFormValues.activeDomains.push({
          isActive: true,
          ...domainData,
        });
      }
      setTenantFormValues({ ...newFormValues });
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      CreateTenant({formValues: tenantFormValues});
    };

    const CreateTenant = async ({formValues}) => {
      try {
        // Create tenant
        const { errors: tenantErrors, data: tenantData } = await client.mutate({
          mutation: CREATE_TENANT,
          variables: {
            Tenant: {
              id: 0,
              name: formValues.name,
              expiration: "2050-12-31",
              expired: false,
              organizationId: Number(formValues.organization ? formValues.organization : 1),
              customerId: Number(formValues.customer ? formValues.customer : 1),
              cropId: Number(formValues.crop ? formValues.crop : null),
              sync:false
            },
          },
        });
        if (tenantErrors) {
          throw new Error(`${tenantErrors[0].message}`);
        }
        // Set Instance data
        ModifyInstance(
          tenantData.createTenant.instances[0].id,
          tenantData.createTenant.id,
          tenantData.createTenant.instances[0].server,
          tenantData.createTenant.instances[0].port,
          formValues.crop,
          formValues.address
        );
        // Create DomainInstances
        formValues.activeDomains.map((domain) => {
          let domainInstanceData = {};
          domainInstanceData.id = 0,
          domainInstanceData.tenantId = tenantData.createTenant.id,
          domainInstanceData.domainId = domain.id,
          domainInstanceData.context = domain.context,
          domainInstanceData.sgContext = domain.sgContext
          CreateDomainInstance(domainInstanceData);
        });
        handleClose();
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } finally {
        //refresh();
      }
    }

    function CreateDomainInstance(domainInstanceData) {
      client
        .mutate({
          mutation: CREATE_DOMAIN_INSTANCE,
          variables: {
            DomainInstance: { ...domainInstanceData },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Create Tenant success`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    function ModifyDomainInstance(instanceId, tenantId, sgContext, context) {
      client
        .mutate({
          mutation: MODIFY_DOMAIN_INSTANCE,
          variables: {
            DomainInstance: {
              id: Number(instanceId),
              tenantId: Number(tenantId ? tenantId : 1),
              sgContext: sgContext,
              context: context,
            },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Tenant modified success`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    function DeleteDomainInstance(instanceId) {
      client
        .mutate({
          mutation: DELETE_DOMAIN_INSTANCE,
          variables: { id: instanceId },
        })
        .then(() => {
          dispatch(
            showMessage({
              message: `Tenant modified success`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    function ModifyInstance(instanceId, tenantId, server, port, cropId, addressId) {
      client
        .mutate({
          mutation: MODIFY_INSTANCE,
          variables: {
            Instance: {
              id: Number(instanceId),
              tenantId: Number(tenantId ? tenantId : 1),
              server: server,
              port: port,
              cropId: Number(cropId ? cropId : null),
              addressId: Number(addressId ? addressId : null),
            },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Instance modified success`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    return (
      /* 
     @prop data-testid: Id to use inside addtenantbutton.test.js file.
     */
      <div ref={ref} data-testid={"AddTenantButtonTestId"}>
      <Button
        variant="contained"
        aria-label="add-customer"
        color="primary"
        startIcon={<PostAdd className="fill-current text-white" />}
        onClick={handleClickOpen}
        disabled={rowSelected ? false : true}
        className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
      >
        <Typography className="text-white">
          <FormattedMessage id="none" defaultMessage="Add Tenant" />
        </Typography>
      </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-label="addTenantDialog"
          fullWidth
          maxWidth="md"
          scroll="paper"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title" className="absolute">
            <FormattedMessage id="none" defaultMessage="Add Tenant" />
          </DialogTitle>
          <br/>
          <form onSubmit={handleSubmit}>
          <DialogActions>
            <Button onClick={handleClose} color="secondary">
              <FormattedMessage id="none" defaultMessage="Close" />
            </Button>
            <Button type="submit">
              <FormattedMessage id="none" defaultMessage="Save" />
            </Button>
          </DialogActions>
            <DialogContent>
              <DialogContentText id="scroll-dialog-description">
                <FormattedMessage
                  id="none"
                  defaultMessage="Add new Tenant"
                />
              </DialogContentText>
              <Grid
                container
                direction="row"
                //justifyContent="center"
                alignItems="flex-start"
                spacing={1}
              >
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl fullWidth>
                    <TextField
                      label={
                        <FormattedMessage id="none" defaultMessage="Name" />
                      }
                      id="name"
                      onChange={(e) =>
                        handleChange(
                          "name",
                          e.target.value
                        )
                      }
                      //value={""}
                      defaultValue={""}
                      required
                    >
                    </TextField>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}></Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl fullWidth>
                    <InputLabel id="organization-label">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Organization"
                      />
                    </InputLabel>
                    <Select
                      //disabled
                      labelId="organization-label"
                      id="organization"
                      value={tenantFormValues.organization}
                      //value={""}
                      onChange={(e) =>
                        handleChange("organization", e.target.value)
                      }
                    >
                      {Organizations &&
                        Organizations.findOrganizationList.content.map(
                          (organization, key) => (
                            <MenuItem key={key} value={organization.id}>
                              {organization.name}
                            </MenuItem>
                          )
                        )}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl fullWidth>
                    <InputLabel id="customer-label">
                      <FormattedMessage id="none" defaultMessage="Customer" />
                    </InputLabel>
                    <Select
                      //disabled
                      labelId="customer-label"
                      id="customer"
                      value={tenantFormValues.customer}
                      //value={""}
                      onChange={(e) => handleChange("customer", e.target.value)}
                    >
                      {Customers &&
                        Customers.findCustomerList.content.map(
                          (customer, key) => (
                            <MenuItem key={key} value={customer.id}>
                              {customer.name}
                            </MenuItem>
                          )
                        )}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl fullWidth>
                    <InputLabel id="crop-label">
                      <FormattedMessage id="none" defaultMessage="Crop" />
                    </InputLabel>
                    <Select
                      labelId="crop-label"
                      id="crop"
                      //value={""}
                      value={tenantFormValues.crop || ''}
                      onChange={(e) => handleChange("crop", e.target.value)}
                    >
                      {Crops &&
                        Crops.findCropList.content.map(
                          (crop, key) => (
                            <MenuItem key={key} value={crop.id}>
                              {crop.name}
                            </MenuItem>
                          )
                        )}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl fullWidth>
                    <InputLabel id="address-label">
                      <FormattedMessage id="none" defaultMessage="Address" />
                    </InputLabel>
                    <Select
                      labelId="address-label"
                      id="address"
                      value={tenantFormValues.address || ''}
                      //value={""}
                      onChange={(e) => handleChange("address", e.target.value)}
                    >
                      {
                        [
                          <MenuItem key={1} value={2}>
                            {"Mexico-Veracruz, El Batan Km. 45, 56237 Mex."}
                          </MenuItem>,
                          <MenuItem key={2} value={3}>
                            {"Pili Drive, University of the Philippines Los Baños, Los Baños, 4030 Laguna, Filipinas"}
                          </MenuItem>
                        ]
                      }
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item={12} sm={12} md={12} lg={12} xl={12}>
                  <Divider />
                </Grid>
                <Grid item={12} sm={12} md={12} lg={12} xl={12}>
                  <Typography variant="h6">
                    <FormattedMessage
                      id="none"
                      defaultMessage="Edit the EBS Services/Components"
                    />
                  </Typography>
                  <List>
                    {Domains &&
                      Domains.findDomainList.content
                        .slice()
                        .sort((a, b) => a.id - b.id)
                        .map((domain, key) => (
                          <React.Fragment key={key}>
                            <ListItem>
                              <ListItemAvatar>
                                <Avatar>
                                  <Icon
                                    sx={classes.icon}
                                    color="inherit"
                                    aria-label={`${domain.name}-icon`}
                                    edge="start"
                                  >
                                    <img
                                      sx={classes.iconImage}
                                      alt="domain"
                                      src={icons[`./${domain.icon}`]}
                                    />
                                  </Icon>
                                </Avatar>
                              </ListItemAvatar>
                              <ListItemText
                                primary={domain.name}
                                secondary={domain.info}
                              />
                              <ListItemSecondaryAction>
                                <Switch
                                  checked={
                                    tenantFormValues.activeDomains &&
                                    tenantFormValues.activeDomains.some(
                                      (item) =>
                                        item.name === domain.name &&
                                        item.isActive
                                    )
                                    /* activeDomains &&
                                    activeDomains.some(
                                      (item) =>
                                        item.name === domain.name &&
                                        item.isActive
                                    ) */
                                  }
                                  name={domain.name}
                                    onChange={(e) =>
                                    handleSwitchChange(e, domain)
                                  }
                                  disabled={domain.core}
                                />
                              </ListItemSecondaryAction>
                            </ListItem>
                            {
                              tenantFormValues.activeDomains &&
                              tenantFormValues.activeDomains.some(
                                (item) => item.name === domain.name
                              )
                                ? buildContextFields(domain.name)
                                : null
                              /* activeDomains &&
                              activeDomains.some(
                                (item) => item.name === domain.name
                              )
                                ? buildContextFields(domain.name)
                                : null */
                            }
                          </React.Fragment>
                        ))}
                  </List>
                </Grid>
              </Grid>
            </DialogContent>
          </form>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
AddTenantButton.propTypes = {
  refresh: PropTypes.func.isRequired,
};
// Default properties
AddTenantButton.defaultProps = {};

export default AddTenantButton;
