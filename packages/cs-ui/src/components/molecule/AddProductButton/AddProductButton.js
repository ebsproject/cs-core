import React from "react";
import PropTypes from "prop-types";
//import _ from "lodash";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
import { useQuery } from "@apollo/client";
import { useDispatch } from "react-redux";
import { EbsForm } from "@ebs/components";
import { client } from "utils/apollo";
import { FIND_DOMAIN, CREATE_PRODUCT } from "utils/apollo/gql/tenantManagement";
import { showMessage } from "store/modules/message";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  MenuItem,
} = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AddProductButtonMolecule = React.forwardRef(
  ({ rowSelected, refresh, handleMenuClose }, ref) => {
    const [open, setOpen] = React.useState(false);
    const [totalProducts, setTotalProducts] = React.useState(null);
    const [icons, setIcons] = React.useState([]);
    const [icon, setIcon] = React.useState(null);
    const dispatch = useDispatch();

    // React.useEffect(() => {
    //   // * Filtering icons list
    //   let newIconsList = Object.keys(Icons).filter((icon) => {
    //     if (
    //       !_.endsWith(icon, "Outlined") &&
    //       !_.endsWith(icon, "Rounded") &&
    //       !_.endsWith(icon, "TwoTone") &&
    //       !_.endsWith(icon, "Sharp")
    //     ) {
    //       return icon;
    //     }
    //   });
    //   setIcons(newIconsList);
    // }, []);

    const { data, error } = useQuery(FIND_DOMAIN, {
      variables: {
        id: rowSelected ? rowSelected.id : 0,
      },
    });

    React.useEffect(() => {
      data &&
        data.findDomain &&
        setTotalProducts(data.findDomain.products.length);
    }, [data]);

    const handleClickOpen = () => {
      handleMenuClose();
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const definition = ({ getValues, setValue, reset }) => {
      return {
        name: "AddProduct",
        components: [
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "domain",
            inputProps: {
              "data-testid": "fromDomain",
              label: "From Domain",
              disabled: true,
            },
            rules: {
              required: "A name is required",
            },
            defaultValue: rowSelected && rowSelected.name,
          },
          {
            sizes: [12, 12, 12, 12, 12],
            component: "TextField",
            name: "name",
            inputProps: {
              "data-testid": "name",
              label: "Product Name",
            },
            rules: {
              required: "A name is required",
              validate: (value) => {
                let isValidName = null;
                data.findDomain.products.map((product) => {
                  isValidName =
                    product.name === value ? "This product aready exist" : true;
                });
                return isValidName;
              },
            },
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "TextField",
            name: "description",
            inputProps: {
              "data-testid": "description",
              label: "Description",
              multiline: true,
              rows: 2,
            },
            rules: {
              required: "A description is required",
            },
          },
          {
            sizes: [6, 6, 6, 6, 6],
            component: "TextField",
            name: "help",
            inputProps: {
              "data-testid": "help",
              label: "Help",
              multiline: true,
              rows: 2,
            },
            rules: {
              required: "A help is required",
            },
          },
          {
            sizes: [10, 10, 10, 10, 10],
            component: "TextField",
            name: "path",
            inputProps: {
              "data-testid": "path",
              label: "Path",
            },
            rules: {
              required: "A path is required",
              validate: (value) => {
                if (value.length < 251) {
                  return true;
                } else {
                  return "Path too long";
                }
              },
            },
          },
          {
            sizes: [2, 2, 2, 2, 2],
            component: "TextField",
            name: "menuOrder",
            inputProps: {
              "data-testid": "menuOrder",
              label: "Menu Order",
              type: "Number",
            },
            rules: {
              required: "A menu order is required",
            },
            defaultValue: totalProducts + 1,
          },
        ],
      };
    };

    const mutation = (formData) => {
      client
        .mutate({
          mutation: CREATE_PRODUCT,
          variables: {
            ProductInput: {
              ...formData,
              mainEntity: "N/A",
              domainId: Number(rowSelected.id),
              icon: icon,
              htmltagId: 1,
              id: 0,
            },
          },
        })
        .then(({ data }) => {
          handleClose();
          refresh();
          dispatch(
            showMessage({
              message: "New product added sucessfully",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: `${message}`,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    };

    const handleChange = (event, value, reason) => {
      setIcon(value);
    };

    return (
      /* 
     @prop data-testid: Id to use inside addproductbutton.test.js file.
     */
      <div ref={ref} data-testid={"AddProductButtonTestId"}>
        <MenuItem
          onClick={handleClickOpen}
          disabled={rowSelected ? false : true}
        >
          <FormattedMessage id="none" defaultMessage="New Product" />
        </MenuItem>
        <Dialog onClose={handleClose} open={open} aria-label="addProductDialog">
          <DialogTitle className="absolute">
            <FormattedMessage id="none" defaultMessage="New Product" />
          </DialogTitle>
          <DialogContent>
            <EbsForm onSubmit={mutation} definition={definition}>
            <DialogActions>
                <Button onClick={handleClose} >
                  <FormattedMessage id="none" defaultMessage="Close" />
                </Button>
                <Button type="submit">
                  <FormattedMessage id="none" defaultMessage="Save" />
                </Button>
              </DialogActions>
              <br/>
            </EbsForm>
          </DialogContent>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
AddProductButtonMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
  rowSelected: PropTypes.object,
  handleMenuClose: PropTypes.func.isRequired,
};
// Default properties
AddProductButtonMolecule.defaultProps = {};

export default AddProductButtonMolecule;
