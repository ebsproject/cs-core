import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND ATOMS TO USE
import AddTenantButton from "components/molecule/AddTenantButton";
import {Core} from "@ebs/styleguide";
const { Tooltip } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewTenantMenuButton = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    return (
      /* 
   @prop data-testid: Id to use inside newdomainsmenubutton.test.js file.
   */
      <div ref={ref} data-testid={"NewTenantMenuButtonTestId"} className="px-2">
        <Tooltip
          placement="top-start"
          title={
            <FormattedMessage id="none" defaultMessage="ADD TENANT" />
          }
        >
          <AddTenantButton
            rowSelected={selectedRows}
            refresh={refresh}
          />
        </Tooltip>
      </div>
    );
  }
);
// Type and required properties
NewTenantMenuButton.propTypes = {};
// Default properties
NewTenantMenuButton.defaultProps = {};

export default NewTenantMenuButton;
