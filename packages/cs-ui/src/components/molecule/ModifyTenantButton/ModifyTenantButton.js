import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
import { useDispatch } from "react-redux";
import { useQuery } from "@apollo/client";
import { QUERY_ROWS, FIND_TENANT_ID } from "utils/apollo/gql/tenant";
import {
  DELETE_DOMAIN_INSTANCE,
  MODIFY_DOMAIN_INSTANCE,
  CREATE_DOMAIN_INSTANCE,
  MODIFY_INSTANCE,
  MODIFY_TENANT,
} from "utils/apollo/gql/tenantManagement";
import { showMessage } from "store/modules/message";
import { Core, Icons, Styles } from "@ebs/styleguide";
const {
  Avatar,
  ListItem,
  ListItemAvatar,
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  Divider,
  FormControl,
  Icon,
  IconButton,
  InputLabel,
  List,
  ListItemSecondaryAction,
  ListItemText,
  MenuItem,
  Switch,
  TextField,
  Tooltip,
  Typography,
  DialogTitle,
  Grid,
  DialogActions,
  Select,
} = Core;
const { Edit } = Icons;
const iconList = require.context("assets/images/domains", true, /\.svg$/);
const icons = iconList.keys().reduce((images, path) => {
  images[path] = iconList(path);
  return images;
}, {});


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ModifyTenantButtonMolecule = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const [formValues, setFormValues] = useState({});
    const [ActiveDomains, setActiveDomains] = useState(null);
    // Select last crop read from tenant instances
 
    // Select last address read from tenant instances
    let currentAddress;
    const classes = {
      formControl: {
        minWidth: "100%",
      },
    };
    const dispatch = useDispatch();
    const [checked, setChecked] = React.useState(rowData?.sync);
    // * Active Domains by Tenant
    useEffect(() => {
      client
        .query({
          query: FIND_TENANT_ID,
          variables: { id: Number(rowData.id) },
          fetchPolicy: "no-cache",
        })
        .then(({ data }) => setActiveDomains(data))
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }, [dispatch, rowData]);
    // * Setting Default Values
    useEffect(() => {
      // * save activeDomains by tenant
      let domainsByTenant = [];
      ActiveDomains &&
        ActiveDomains.findTenant &&
        ActiveDomains.findTenant.instances.map((instance) => {
          domainsByTenant = domainsByTenant.concat(instance.domaininstances);
          if (instance.address != undefined) {
            currentAddress = instance.address;
          }
        });
      let realActiveDomains = [];
      removeDuplicates(domainsByTenant).map((domain) =>
        realActiveDomains.push({ isActive: true, ...domain })
      );
      setFormValues({
        organization: rowData.organization.id,
        customer: rowData.customer.id,
        activeDomains: realActiveDomains,
        sync:rowData.sync
      });
      if (currentAddress) {
        setFormValues(previousState => {
          return { ...previousState, address: currentAddress.id }
        });
      }
    }, [rowData, ActiveDomains]);
    // * Delete duplicate domains from activeDomains
    function removeDuplicates(array) {
      const uniqueValuesSet = new Set();
      const filteredArr = array.filter((obj) => {
        // check if domain.name property value is already in the set
        const isPresentInSet = uniqueValuesSet.has(obj.domain.name);
        // add domain.name property value to Set
        uniqueValuesSet.add(obj.domain.name);
        // return the negated value of
        // isPresentInSet variable
        return !isPresentInSet;
      });
      return filteredArr;
    }
    // * Filling select values
    const { error: errorOrg, data: Organizations } = useQuery(
      QUERY_ROWS("Organization", "content { id name}"),
      {
        variables: { size: 100, number: 1 },
      }
    );
    const { error: errorCust, data: Customers } = useQuery(
      QUERY_ROWS("Customer", "content { id name}"),
      {
        variables: { size: 100, number: 1 },
      }
    );
    // * Domains List
    const { error: errorDomain, data: Domains } = useQuery(
      QUERY_ROWS(
        "Domain",
        "content { id name info icon core domaininstances{context sgContext} }"
      ),
      {
        variables: { size: 100, number: 1 },
      }
    );

    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleChange = (label, newValue) => {
      setFormValues({ ...formValues, [label]: newValue });
    };

    const handleContextChange = (domainName, context) => {
      let newFormValues = formValues;
      const index = newFormValues.activeDomains.findIndex(
        (domain) => domain.domain.name === domainName
      );
      newFormValues.activeDomains[index].context = context;
      setFormValues(newFormValues);
    };

    const handleSgContextChange = (domainName, sgContext) => {
      let newFormValues = formValues;
      const index = newFormValues.activeDomains.findIndex(
        (domain) => domain.domain.name === domainName
      );
      newFormValues.activeDomains[index].sgContext = sgContext;
      setFormValues(newFormValues);
    };

    const buildContextFields = (domainName) => {
      const index = formValues.activeDomains.findIndex(
        (domain) => domain.domain.name === domainName
      );
      if (index != -1 && formValues.activeDomains[index].isActive)
        return (
          <FormControl fullWidth>
            <TextField
              label={
                <FormattedMessage id="none" defaultMessage="Web App URL" />
              }
              onChange={(e) =>
                handleContextChange(
                  formValues.activeDomains[index].domain.name,
                  e.target.value
                )
              }
              inputProps={{
                type: "url",
              }}
              //disabled={formValues.activeDomains[index].domain.core || false}
              required
              defaultValue={formValues.activeDomains[index].context}
            />
            <TextField
              label={
                <FormattedMessage
                  id="none"
                  defaultMessage="Service Gateway URL"
                />
              }
              onChange={(e) =>
                handleSgContextChange(
                  formValues.activeDomains[index].domain.name,
                  e.target.value
                )
              }
              inputProps={{
                type: "url",
              }}
              //disabled={formValues.activeDomains[index].domain.core || false}
              required
              defaultValue={formValues.activeDomains[index].sgContext}
            />
          </FormControl>
        );
    };

    const handleSwitchChange = (event, domainData) => {
      let index = formValues.activeDomains.findIndex(
        (domain) => domain.domain.name === event.target.name
      );
      let newFormValues = formValues;
      // ? The Domain already exist on this Tenant
      if (index != -1) {
        // * isActive the Domain from current Tenant
        newFormValues.activeDomains.splice(index, 0, {
          ...newFormValues.activeDomains[index],
          isActive: !newFormValues.activeDomains[index].isActive,
        });
        newFormValues.activeDomains.splice(index + 1, 1);
      } else {
        // * Otherwise enable the domain
        newFormValues.activeDomains.push({
          isActive: true,
          domain: { ...domainData },
        });
      }
      setFormValues({ ...newFormValues });
    };
    const handleSwitchSync = (event) => {
      setChecked(event.target.checked)
      let newFormValues = formValues;
      newFormValues.sync = event.target.checked;
      setFormValues({ ...newFormValues });
    };

    const handleSubmit = (e) => {
      e.preventDefault();
      let domainsByTenant = [];
      let tenantId = ActiveDomains.findTenant.id;
      // instance id for the domains, initialized to 0
      let currentInstance;
      ActiveDomains.findTenant.instances.map((instance) => {
        domainsByTenant = domainsByTenant.concat(instance.domaininstances);
        currentInstance = instance;
      });
      ModifyTenantAsync({ formValues }, { ActiveDomains }, domainsByTenant, currentInstance);
    };

    function CreateDomainInstance(domainInstanceData) {
      client
        .mutate({
          mutation: CREATE_DOMAIN_INSTANCE,
          variables: {
            DomainInstance: { ...domainInstanceData },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Successfully updated tenant`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    function ModifyDomainInstance(instanceId, tenantId, sgContext, context) {
      client
        .mutate({
          mutation: MODIFY_DOMAIN_INSTANCE,
          variables: {
            DomainInstance: {
              id: Number(instanceId),
              tenantId: Number(tenantId ? tenantId : 1),
              sgContext: sgContext,
              context: context,
            },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Successfully updated tenant`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    function DeleteDomainInstance(instanceId) {
      client
        .mutate({
          mutation: DELETE_DOMAIN_INSTANCE,
          variables: { id: instanceId },
        })
        .then(() => {
          dispatch(
            showMessage({
              message: `Successfully updated tenant`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    function ModifyInstance(instanceId, tenantId, server, port,addressId) {
      client
        .mutate({
          mutation: MODIFY_INSTANCE,
          variables: {
            Instance: {
              id: Number(instanceId),
              tenantId: Number(tenantId ? tenantId : 1),
              server: server,
              port: port,
              addressId: Number(addressId ? addressId : null),
            },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Tenant modified success`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    const ModifyTenantAsync = async ({ formValues }, tenantInfo, tenantDomains, instanceData) => {
      try {
        // Create tenant
        const { errors: tenantErrors, data: tenantData } = await client.mutate({
          mutation: MODIFY_TENANT,
          variables: {
            Tenant: {
              id: tenantInfo.ActiveDomains.findTenant.id,
              name: tenantInfo.ActiveDomains.findTenant.name,
              expiration: tenantInfo.ActiveDomains.findTenant.expiration,
              expired: tenantInfo.ActiveDomains.findTenant.expired,
              organizationId: Number(formValues.organization ? formValues.organization : 1),
              customerId: Number(formValues.customer ? formValues.customer : 1),
              sync:formValues.sync
            },
          },
        });
        if (tenantErrors) {
          throw new Error(`${tenantErrors[0].message}`);
        }
        // Edit Instance information
        ModifyInstance(
          instanceData.id,
          tenantInfo.ActiveDomains.findTenant.id,
          instanceData.server,
          instanceData.port,
          formValues.address,
        );

        // * Domains in the database for the current Tenant
        let currentDomainList = removeDuplicates(tenantDomains);
        // * Actived Domains in the current Form
        const { activeDomains } = formValues;
        activeDomains.map((domain) => {
          let domainIndex = currentDomainList.findIndex(
            (item) => item.domain.name === domain.domain.name
          );
          // ? the domain is disabled
          if (!domain.isActive) {
            // ? Was the domain in the database
            if (domainIndex != -1) {
              // * Deleting disabled domains from the database if there are
              DeleteDomainInstance(currentDomainList[domainIndex].id);
            }
          } else {
            // ? is the Domain already in the database
            if (domainIndex != -1) {
              // * Edit current Domains from database to change sgContext or URL
              ModifyDomainInstance(
                Number(domain.id),
                null,
                domain.sgContext,
                domain.context
              );
            } else {
              // * Adding new Domain to database
              const payload = {
                id: 0,
                domainId: Number(domain.domain.id),
                tenantId: Number(rowData.id),
                context: domain.context,
                sgContext: domain.sgContext,
                mfe: true,
              };
              CreateDomainInstance(payload);
            }
          }
          handleClose();
        });
      } catch (error) {
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      }
    }

    function ModifyTenant(tenantId, name, expiration, expired, organizationId, customerId) {
      client
        .mutate({
          mutation: MODIFY_TENANT,
          variables: {
            Tenant: {
              id: Number(tenantId),
              name: name,
              expiration: expiration,
              expired: expired,
              organizationId: Number(organizationId ? organizationId : 1),
              customerId: Number(customerId ? customerId : 1)
            },
          },
        })
        .then(({ data }) => {
          dispatch(
            showMessage({
              message: `Tenant modified success`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        })
        .catch(({ message }) => {
          dispatch(
            showMessage({
              message: message,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }

    return (
      /* 
     @prop data-testid: Id to use inside modifytenantbutton.test.js file.
     */
      <div ref={ref} data-testid={"ModifyTenantButtonTestId"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Edit tenant" />
            </Typography>
          }
        >
          <IconButton
            aria-label="EditTenantButton"
            onClick={handleClickOpen}
            color="primary"
          >
            <Edit />
          </IconButton>
        </Tooltip>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-label="modifyTenantDialog"
          fullWidth
          maxWidth="md"
          scroll="paper"
          aria-labelledby="scroll-dialog-title"
          aria-describedby="scroll-dialog-description"
        >
          <DialogTitle id="scroll-dialog-title" className="absolute">
            <FormattedMessage id="none" defaultMessage="Edit Tenant" />
          </DialogTitle>
          <br />
          <form onSubmit={handleSubmit}>
            <DialogActions>
              <Button onClick={handleClose} >
                <FormattedMessage id="none" defaultMessage="Close" />
              </Button>
              <Button type="submit">
                <FormattedMessage id="none" defaultMessage="Save" />
              </Button>
            </DialogActions>
            <DialogContent>
              <DialogContentText id="scroll-dialog-description">
                <FormattedMessage
                  id="none"
                  defaultMessage="Edit current Tenant"
                />
              </DialogContentText>
              <Grid
                container
                direction="row"
                //justifyContent="center"
                alignItems="flex-start"
                spacing={1}
              >
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl variant="standard" fullWidth>
                    <InputLabel id="organization-label">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Organization"
                      />
                    </InputLabel>
                    <Select
                      disabled
                      labelId="organization-label"
                      id="organization"
                      value={formValues.organization}
                      onChange={(e) =>
                        handleChange("organization", e.target.value)
                      }
                    >
                      {Organizations &&
                        Organizations.findOrganizationList.content.map(
                          (organization, key) => (
                            <MenuItem key={key} value={organization.id}>
                              {organization.name}
                            </MenuItem>
                          )
                        )}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl variant="standard" fullWidth>
                    <InputLabel id="customer-label">
                      <FormattedMessage id="none" defaultMessage="Customer" />
                    </InputLabel>
                    <Select
                      //disabled
                      labelId="customer-label"
                      id="customer"
                      value={formValues.customer}
                      onChange={(e) => handleChange("customer", e.target.value)}
                    >
                      {Customers &&
                        Customers.findCustomerList.content.map(
                          (customer, key) => (
                            <MenuItem key={key} value={customer.id}>
                              {customer.name}
                            </MenuItem>
                          )
                        )}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={6} md={6} lg={6} xl={6}>
                  <FormControl variant="standard" fullWidth>
                    <InputLabel id="address-label">
                      <FormattedMessage id="none" defaultMessage="Address" />
                    </InputLabel>
                    <Select
                      labelId="address-label"
                      id="address"
                      value={formValues.address || ''}
                      onChange={(e) => handleChange("address", e.target.value)}
                    >
                      {
                        [
                          <MenuItem key={1} value={2}>
                            {"Mexico-Veracruz, El Batan Km. 45, 56237 Mex."}
                          </MenuItem>,
                          <MenuItem key={2} value={3}>
                            {"Pili Drive, University of the Philippines Los Baños, Los Baños, 4030 Laguna, Filipinas"}
                          </MenuItem>
                        ]
                      }
                    </Select>
                  </FormControl>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                  <FormControl fullWidth>
                    <Typography variant="subtitle">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Enable/Disable Sync CB -> CS"
                      />
                    </Typography>
                    <Switch
                      checked={checked}
                      onChange={handleSwitchSync}
                      inputProps={{ 'aria-label': 'Enable/Disable Sync CB -> CS' }}
                    />
                  </FormControl>

                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                  <Divider />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                  <Typography variant="h6">
                    <FormattedMessage
                      id="none"
                      defaultMessage="Edit the EBS Services/Components"
                    />
                  </Typography>
                  <List>
                    {Domains &&
                      Domains.findDomainList.content
                        .slice()
                        .sort((a, b) => a.id - b.id)
                        .map((domain, key) => (
                          <React.Fragment key={key}>
                            <ListItem>
                              <ListItemAvatar>
                                <Avatar>
                                  <Icon
                                    sx={classes.icon}
                                    color="inherit"
                                    aria-label={`${domain.name}-icon`}
                                    edge="start"
                                  >
                                    <img
                                      sx={classes.iconImage}
                                      alt="domain"
                                      src={icons[`./${domain.icon}`]}
                                    />
                                  </Icon>
                                </Avatar>
                              </ListItemAvatar>
                              <ListItemText
                                primary={domain.name}
                                secondary={domain.info}
                              />
                              <ListItemSecondaryAction>
                                <Switch
                                  checked={
                                    formValues.activeDomains &&
                                    formValues.activeDomains.some(
                                      (item) =>
                                        item.domain.name === domain.name &&
                                        item.isActive
                                    )
                                  }
                                  name={domain.name}
                                  onChange={(e) =>
                                    handleSwitchChange(e, domain)
                                  }
                                  disabled={domain.core}
                                />
                              </ListItemSecondaryAction>
                            </ListItem>
                            {formValues.activeDomains &&
                              formValues.activeDomains.some(
                                (item) => item.domain.name === domain.name
                              )
                              ? buildContextFields(domain.name)
                              : null}
                          </React.Fragment>
                        ))}
                  </List>
                </Grid>
              </Grid>
            </DialogContent>
          </form>
        </Dialog>
      </div>
    );
  }
);
// Type and required properties
ModifyTenantButtonMolecule.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func.isRequired,
};
// Default properties
ModifyTenantButtonMolecule.defaultProps = {};

export default ModifyTenantButtonMolecule;
