import React, { memo,useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { EbsTabsLayout,Core,Icons } from "@ebs/styleguide";
const {PrintRounded} = Icons;
const { Box,IconButton,Dialog,DialogActions,DialogContent,DialogTitle,TextField,Typography,Button } = Core;
import ListContactTypes from "components/atom/ListContactTypes";
import ListTenants from "components/atom/ListTenants";
import ListAddresses from "components/atom/ListAddresses";
//MAIN FUNCTION
/**
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const CrmContactDetail = React.forwardRef(
  ({ contact_types,addresses,tenants }, ref) => {
    return (
      /** 
     @prop data-testid: Id to use inside ContactsDetail.test.js file.
     */
      <Box
        component="div"
        ref={ref}
        data-testid={"ContactsDetailTestId"}
        className="flex flex-row"
      >
        <EbsTabsLayout
          orientation={"horizontal"}
          tabs={[
            {
              label: <FormattedMessage id="none" defaultMessage={"Contact Types"} />,
              component: <ListContactTypes data={contact_types} />,
            },
            {
              label: <FormattedMessage id="none" defaultMessage={"Tenants"} />,
              component: <ListTenants data={tenants} />,
            },
            {
              label: <FormattedMessage id="none" defaultMessage={"Addresses"} />,
              component: <ListAddresses data={addresses} />,              
            },
          ]}
        />
      </Box>
    );
  }
);
// Type and required properties
CrmContactDetail.propTypes = {
  contactTypes: PropTypes.array.isRequired,
  // tenants: PropTypes.array.isRequired,
  addresses: PropTypes.array.isRequired,
};
// Default properties
CrmContactDetail.defaultProps = {};

export default memo(CrmContactDetail);
