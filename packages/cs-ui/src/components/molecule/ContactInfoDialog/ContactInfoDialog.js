import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import ContactInfoForm from "components/atom/ContactInfoForm";
import {Core, Icons,EbsDialog } from "@ebs/styleguide";
const {
  Box,
  Button,
  IconButton,
  Tooltip,
  Typography,
} = Core;
const {Edit,PostAdd} = Icons;

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ContactInfoDialogMolecule = React.forwardRef(
  ({ contactId, refresh, method, defaultValues }, ref) => {
    const [open, setOpen] = useState(false);
    const { contactInfoSuccess } = useSelector(({ crm }) => crm);
    useEffect(() => {
      if (contactInfoSuccess) {
        handleClose();
        refresh();
      }
    }, [contactInfoSuccess]);
    /**
     * @returns {func}
     */
    const handleClose = () => setOpen(false);
    /**
     * @returns {func}
     */
    const handleOpen = () => setOpen(true);
    return (
      /**
       * @prop data-testid: Id to use inside ContactInfoDialog.test.js file.
       */
      <div component="div" ref={ref} data-testid={"ContactInfoDialogTestId"}>
        {method === "POST" ? (
          <Button
            variant="contained"
            aria-label="add-contactInfo"
            className="bg-ebs-brand-default rounded-md text-white hover:bg-ebs-brand-900 ml-3"
            startIcon={<PostAdd className="fill-current text-white" />}
            onClick={handleOpen}
            data-testid={"PostButton"}
          >
            <Typography className="text-white">
              <FormattedMessage id="none" defaultMessage="Add Information" />
            </Typography>
          </Button>
        ) : (
          <Tooltip
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Edit"} />
              </Typography>
            }
            arrow
          >
            <IconButton color="primary" onClick={handleOpen}>
              <Edit />
            </IconButton>
          </Tooltip>
        )}
        <EbsDialog
          open={open}
          handleClose={handleClose}
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Additional Information"}
              />
            </Typography>
          }
          maxWidth="sm"
        >
          <ContactInfoForm
            defaultValues={defaultValues}
            handleClose={handleClose}
            contactId={contactId}
            method={method}
          />
        </EbsDialog>
      </div>
    );
  }
);
// Type and required properties
ContactInfoDialogMolecule.propTypes = {};
// Default properties
ContactInfoDialogMolecule.defaultProps = {};

export default ContactInfoDialogMolecule;
