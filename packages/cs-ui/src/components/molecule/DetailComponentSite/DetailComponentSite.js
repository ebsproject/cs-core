import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core, Styles } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE

const { Typography } = Core;
import { clientCb } from "utils/apollo/apollo";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";
import { EbsGrid } from "@ebs/components";
import RowActionsDetailsMolecule from "../RowActionsDetails/RowActionsDetails";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DetailComponentSiteMolecule = React.forwardRef(
  ({ rowData, type, setListData }, ref) => {
    // const [setListData, SetListData] = React.useState(null);

    const columns = [
      { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
      {
        accessor: "null",
        width: 180,
        // filter: false,
      },
      {
        accessor: "null2",
        width: 180,
        // filter: false,
      },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value}</Typography>;
        },
        accessor: "field.geospatialObjectName",
        width: 180,
        // filter: false,
      },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value}</Typography>;
        },
        accessor: "planting.geospatialObjectName",
        width: 180,
        // filter: false,
      },
    ];

    const fetch = async ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        try {
          clientCb
            .query({
              query: FIND_PLACES_LIST,
              variables: {
                page: page,
                filters: [
                  ...filters,
                  {
                    col: "parentGeospatialObject.id",
                    mod: "EQ",
                    val: rowData.id,
                  },
                ],
                disjunctionFilters: false,
              },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                let dataList = data.findGeospatialObjectList.content;
                const listParents = dataList.map((item) => {
                  switch (item.geospatialObjectType) {
                    case "field":
                      const dataField = {
                        id: item.id,
                        geospatialObjectCode: item.geospatialObjectCode,
                        geospatialObjectType: item.geospatialObjectType,
                        altitude: item.altitude,
                        description: item.description,
                        coordinates: item.coordinates,
                        dateCreated: item.dateCreated,
                        parentGeospatialObject: item.parentGeospatialObject,
                        field: {
                          geospatialObjectType: item.geospatialObjectType,
                          geospatialObjectName: item.geospatialObjectName,
                        },
                        geospatialObjectType: item.geospatialObjectType,
                        geospatialObjectName: item.geospatialObjectName,
                      };
                      return dataField;
                    case "planting area":
                      const dataPla = {
                        id: item.id,
                        geospatialObjectCode: item.geospatialObjectCode,
                        geospatialObjectType: item.geospatialObjectType,
                        altitude: item.altitude,
                        description: item.description,
                        coordinates: item.coordinates,
                        dateCreated: item.dateCreated,
                        parentGeospatialObject: item.parentGeospatialObject,
                        planting: {
                          geospatialObjectType: item.geospatialObjectType,
                          geospatialObjectName: item.geospatialObjectName,
                        },
                        geospatialObjectType: item.geospatialObjectType,
                        geospatialObjectName: item.geospatialObjectName,
                      };
                      return dataPla;
                  }
                });
                // SetListData(listParents);
                resolve({
                  pages: data.findGeospatialObjectList.totalPages,
                  elements: data.findGeospatialObjectList.totalElements,
                  data: listParents,
                });
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    };

    const DetailComponent = ({ rowData: plantingData }) => {
      if (plantingData.geospatialObjectType !== "planting area") {
        const columnsPla = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            accessor: "null3",
            width: 180,
            // filter: false,
          },
          {
            accessor: "null4",
            width: 180,
            // filter: false,
          },
          {
            accessor: "field6",
            width: 180,
            // filter: false,
          },
          {
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "geospatialObjectName",
            width: 180,
            // filter: false,
          },
        ];
        const fetchPlanting = async ({ page, sort, filters }) => {
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_PLACES_LIST,
                  variables: {
                    page: page,
                    filters: [
                      ...filters,
                      {
                        col: "parentGeospatialObject.id",
                        mod: "EQ",
                        val: plantingData.id,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    resolve({
                      pages: data.findGeospatialObjectList.totalPages,
                      elements: data.findGeospatialObjectList.totalElements,
                      data: data.findGeospatialObjectList.content,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        };

        return (
          <div data-testid={"DivPlantingArea"} style={{ width: 950 }}>
            <EbsGrid
              id="PlantingArea"
              toolbar={false}
              data-testid={"PlantingArea"}
              columns={columnsPla}
              rowactions={rowActionsDetail}
              csvfilename="PlacesList"
              fetch={fetchPlanting}
              detailcomponent
              height="85vh"
              select="multi"
              disabledHeadTable
              color="terciary"
            />
          </div>
        );
      }
    };

    const rowActionsDetail = (rowData, refresh) => {
      return (
        <RowActionsDetailsMolecule
          type={type}
          rowData={rowData}
          setListData={setListData}
          refresh={refresh}
        />
      );
    };

    return (
      /* 
     @prop data-testid: Id to use inside FieldDetail.test.js file.
     */
      <div data-testid={"FieldTestId"} style={{ width: 950 }}>
        <EbsGrid
          id="Fields"
          toolbar={false}
          data-testid={"PlacesTestId"}
          columns={columns}
          rowactions={rowActionsDetail}
          csvfilename="PlacesList"
          detailcomponent={DetailComponent}
          fetch={fetch}
          height="85vh"
          select="multi"
          disabledHeadTable
          color="secondary"
        />
      </div>
    );
  }
);
// Type and required properties
DetailComponentSiteMolecule.propTypes = {};
// Default properties
DetailComponentSiteMolecule.defaultProps = {};

export default DetailComponentSiteMolecule;
