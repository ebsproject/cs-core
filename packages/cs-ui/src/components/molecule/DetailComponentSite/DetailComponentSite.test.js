import DetailComponentSite from './DetailComponentSite';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('DetailComponentSite is in the DOM', () => {
  render(<DetailComponentSite></DetailComponentSite>)
  expect(screen.getByTestId('DetailComponentSiteTestId')).toBeInTheDocument();
})
