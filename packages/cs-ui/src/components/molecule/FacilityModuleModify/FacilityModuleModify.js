import React from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { Typography, Card, CardContent, Button } = Core;
import { EbsTabsLayout } from "@ebs/styleguide";
import { useNavigate, useLocation } from "react-router-dom";
import { EbsGrid } from "@ebs/components";
import { useSelector, useDispatch } from "react-redux";
import FormFacilityModifyAtom from "components/atom/FormFacilityModify/FormFacilityModify";
import { clientCb } from "utils/apollo/apollo";
import { FIND_FACILITY_LIST } from "utils/apollo/gql/placeManager";
import ViewDetailFacilityAtom from "components/atom/ViewDetailFacility/ViewDetailFacility";
import RowActionsDetailFacilityMolecule from "components/molecule/RowActionsDetailFacility/RowActionsDetailFacility";
import DetailComponentFacilityMolecule from "components/molecule/DetailComponentFacility/DetailComponentFacility";

/*
  @param { }: page props,
*/
export default function FacilityModuleModifyView({ match }) {
  const [setListData, SetListData] = React.useState(null);
  const navigate = useNavigate();
  const location = useLocation();
  // const dispatch = useDispatch();
  const type = location.state.type;
  const { placemanager } = useSelector((placemanager) => placemanager);
  if (placemanager?.setDataPlace === null) {
    navigate(`/cs/place-manager`);
  }

  const columnsOption = () => {
    const optionGrid = placemanager?.setDataPlace?.gridType;
    switch (optionGrid) {
      case "structure":
        if (placemanager?.setDataPlace?.data?.parentFacility === null) {
          const columnsFacility = [
            {
              Header: "Id",
              accessor: "id",
              hidden: true,
              disableGlobalFilter: true,
            },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage id="none" defaultMessage="Facility" />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1">{value}</Typography>;
              },
              accessor: "facilityName",
              csvHeader: "Facility",
              width: 150,
              filter: true,
            },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage id="none" defaultMessage="Facility Code" />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1">{value}</Typography>;
              },
              accessor: "facilityCode",
              csvHeader: "Facility Code",
              width: 150,
              filter: true,
            },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage id="none" defaultMessage="Facility Type" />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1">{value}</Typography>;
              },
              accessor: "facilityType",
              csvHeader: "Facility Type",
              width: 150,
              filter: true,
            },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage id="none" defaultMessage="Site" />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1">{value}</Typography>;
              },
              accessor: "geospatialObject.geospatialObjectName",
              csvHeader: "Site",
              width: 150,
              filter: true,
            },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage id="none" defaultMessage="Sub Facility" />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1"></Typography>;
              },
              accessor: "subFacility",
              csvHeader: "subFacility",
              width: 150,
              filter: true,
            },
            // {
            //   Header: (
            //     <Typography variant="h6" color="primary">
            //       <FormattedMessage
            //         id="none"
            //         defaultMessage="Sub Facility Code"
            //       />
            //     </Typography>
            //   ),
            //   Cell: ({ value }) => {
            //     return <Typography variant="body1"></Typography>;
            //   },
            //   accessor: "subFacilityCode",
            //   csvHeader: "subFacilityCode",
            //   width: 150,
            //   filter: true,
            // },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage
                    id="none"
                    defaultMessage="Sub Facility Type"
                  />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1"></Typography>;
              },
              accessor: "subFacilityType",
              csvHeader: "subFacilityType",
              width: 150,
              filter: true,
            },
            {
              Header: (
                <Typography variant="h6" color="primary">
                  <FormattedMessage id="none" defaultMessage="Container" />
                </Typography>
              ),
              Cell: ({ value }) => {
                return <Typography variant="body1"></Typography>;
              },
              accessor: "container",
              csvHeader: "container",
              width: 150,
              filter: true,
            },
            // {
            //   Header: (
            //     <Typography variant="h6" color="primary">
            //       <FormattedMessage id="none" defaultMessage="Container Code" />
            //     </Typography>
            //   ),
            //   Cell: ({ value }) => {
            //     return <Typography variant="body1"></Typography>;
            //   },
            //   accessor: "containerCode",
            //   csvHeader: "containerCode",
            //   width: 150,
            //   filter: true,
            // },
            // {
            //   Header: (
            //     <Typography variant="h6" color="primary">
            //       <FormattedMessage id="none" defaultMessage="Date Created" />
            //     </Typography>
            //   ),
            //   Cell: ({ value }) => {
            //     return <Typography variant="body1">{value}</Typography>;
            //   },
            //   accessor: "dateCreated",
            //   csvHeader: "Data Created",
            //   width: 200,
            //   filter: true,
            // },
            // {
            //   Header: (
            //     <Typography variant="h5" color="primary">
            //       <FormattedMessage id="none" defaultMessage="Coordinates" />
            //     </Typography>
            //   ),
            //   Cell: ({ value }) => {
            //     return (
            //       <Typography variant="body1">
            //         {value?.type === "Polygon"
            //           ? value?.coordinates[0][0]
            //           : value?.coordinates[0]}
            //       </Typography>
            //     );
            //   },
            //   accessor: "coordinates.features[0].geometry",
            //   csvHeader: "Coordinates",
            //   width: 200,
            //   filter: true,
            // },
          ];
          return columnsFacility;
        }
        const columnsSubFacility = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage id="none" defaultMessage="Sub Facility" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityName",
            csvHeader: "subFacility",
            width: 200,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage
                  id="none"
                  defaultMessage="Sub Facility Code"
                />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityCode",
            csvHeader: "subFacilityCode",
            width: 200,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage
                  id="none"
                  defaultMessage="Sub Facility Type"
                />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityType",
            csvHeader: "subFacilityType",
            width: 200,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage id="none" defaultMessage="Container" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "container",
            csvHeader: "container",
            width: 200,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage id="none" defaultMessage="Container Code" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "containerCode",
            csvHeader: "containerCode",
            width: 200,
            filter: true,
          },
          // {
          //   Header: (
          //     <Typography variant="h6" color="primary">
          //       <FormattedMessage id="none" defaultMessage="Date Created" />
          //     </Typography>
          //   ),
          //   Cell: ({ value }) => {
          //     return <Typography variant="body1">{value}</Typography>;
          //   },
          //   accessor: "dateCreated",
          //   csvHeader: "Data Created",
          //   width: 200,
          //   filter: true,
          // },
          // {
          //   Header: (
          //     <Typography variant="h5" color="primary">
          //       <FormattedMessage id="none" defaultMessage="Coordinates" />
          //     </Typography>
          //   ),
          //   Cell: ({ value }) => {
          //     return (
          //       <Typography variant="body1">
          //         {value?.type === "Polygon"
          //           ? value?.coordinates[0][0]
          //           : value?.coordinates[0]}
          //       </Typography>
          //     );
          //   },
          //   accessor: "coordinates.features[0].geometry",
          //   csvHeader: "Coordinates",
          //   width: 200,
          //   filter: true,
          // },
        ];
        return columnsSubFacility;
      case "container":
        const columnsContainer = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage id="none" defaultMessage="Container" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityName",
            csvHeader: "container",
            width: 200,
            filter: true,
          },
          {
            Header: (
              <Typography variant="h6" color="primary">
                <FormattedMessage id="none" defaultMessage="Container Code" />
              </Typography>
            ),
            Cell: ({ value }) => {
              return <Typography variant="body1">{value}</Typography>;
            },
            accessor: "facilityCode",
            csvHeader: "containerCode",
            width: 200,
            filter: true,
          },
          // {
          //   Header: (
          //     <Typography variant="h6" color="primary">
          //       <FormattedMessage id="none" defaultMessage="Date Created" />
          //     </Typography>
          //   ),
          //   Cell: ({ value }) => {
          //     return <Typography variant="body1">{value}</Typography>;
          //   },
          //   accessor: "dateCreated",
          //   csvHeader: "Data Created",
          //   width: 200,
          //   filter: true,
          // },
        ];
        return columnsContainer;
      default:
        const columnsDefault = [
          {
            Header: "Id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
          },
          {
            accessor: "geospatialObjectName",
            width: 180,
            // filter: false,
          },
          {
            accessor: "geospatialObjectCode",
            width: 250,
            filter: true,
          },
        ];
        return columnsDefault;
    }
  };

  const fetch = ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      clientCb
        .query({
          query: FIND_FACILITY_LIST,
          variables: {
            page: page,
            filters: [
              ...filters,
              { col: "id", mod: "EQ", val: placemanager?.setDataPlace?.id },
            ],
            disjunctionFilters: false,
          },
          fetchPolicy: "no-cache",
        })
        .then(({ data, errors }) => {
          if (errors) {
            reject(errors);
          } else {
            SetListData(data.findFacilityList.content);
            resolve({
              pages: data.findFacilityList.pages,
              elements: data.findFacilityList.totalPages,
              data: data.findFacilityList.content,
            });
          }
        });
    });
  };

  const handleGoTo = ({ dataField, type, hierarchy }) => {
    //   setOpenDetail(true);
    //   setDataDetail(dataField);
    //   setDataType(type);
    //   setDataHierarchy(hierarchy);
  };

  const DetailComponent = ({ rowData }) => {
    return (
      <DetailComponentFacilityMolecule
        rowData={rowData}
        type={type}
        SetListData={SetListData}
      />
    );
  };

  const modifyRowActions = (rowData, refresh) => {
    return (
      <RowActionsDetailFacilityMolecule
        type={type}
        rowData={rowData}
        SetListData={SetListData}
        refresh={refresh}
      />
    );
  };

  return (
    <div data-testid={"SubFacilityModuleModifyTestId"}>
      <div className="grid grid-cols-2 gap-4">
        <div>
          {type === "VIEW" ? (
            <Typography variant="h4">
              <FormattedMessage
                id="none"
                defaultMessage={
                  "Place Manager" +
                  " " +
                  ">>" +
                  " " +
                  "Facility View " +
                  " " +
                  "(" +
                  setListData?.[0]?.facilityName +
                  ")"
                }
              />
            </Typography>
          ) : (
            <Typography variant="h4">
              <FormattedMessage
                id="none"
                defaultMessage={
                  "Place Manager" +
                  " " +
                  ">>" +
                  " " +
                  "Modify Facility" +
                  " " +
                  "(" +
                  setListData?.[0]?.facilityName +
                  ")"
                }
              />
            </Typography>
          )}

          <br />
          <Typography variant="h6">
            <FormattedMessage
              id="none"
              defaultMessage={
                "Use the left to navigate between the facility assingned to " +
                " " +
                "(" +
                setListData?.[0]?.facilityName +
                ")"
              }
            />
          </Typography>
        </div>
        <div className="relative">
          {type === "VIEW" ? (
            <Button
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900 absolute bottom-0 right-0"
              onClick={() => navigate(`/cs/place-manager`)}
            >
              Back
            </Button>
          ) : null}
        </div>
        <br />
      </div>
      <div className="grid grid-cols-2 gap-4">
        <div className="flex-auto">
          <Card>
            <CardContent>
              <Typography variant="h6">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    "Sub-Facility that belong to" +
                    " " +
                    "(" +
                    setListData?.[0]?.facilityName +
                    ")"
                  }
                />
              </Typography>
              <EbsGrid
                id="PlacesModify"
                toolbar={true}
                data-testid={"PlacesCreatedTestId"}
                columns={columnsOption()}
                rowactions={modifyRowActions}
                detailcomponent={DetailComponent}
                csvfilename="PlacesCreatedList"
                fetch={fetch}
                height="85vh"
                select="multi"
                disabledViewConfiguration
                disabledViewDownloadCSV
                disabledViewPrintOunt
                disabledFiedPagination
              />
            </CardContent>
          </Card>
        </div>
        <div className="flex-auto">
          <Card>
            <CardContent>
              {type === "VIEW" ? (
                <Typography variant="h6">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      "Information" +
                      " " +
                      "(" +
                      setListData?.[0]?.facilityName +
                      ")"
                    }
                  />
                </Typography>
              ) : (
                <Typography variant="h6">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      "Information" +
                      " " +
                      "(" +
                      setListData?.[0]?.facilityName +
                      ")"
                    }
                  />
                </Typography>
              )}
              <br />
              <EbsTabsLayout
                orientation="horizontal"
                index={location?.state?.index || 0} //TODO:: REVIEW the object properties of location
                tabs={[
                  {
                    label: (
                      <FormattedMessage
                        id="none"
                        defaultMessage="Information"
                      />
                    ),
                    component:
                      type === "VIEW" ? (
                        <div>
                          <ViewDetailFacilityAtom
                            data={setListData?.[0]}
                            type={type}
                          />
                        </div>
                      ) : (
                        <div>
                          <FormFacilityModifyAtom
                            data={setListData?.[0]}
                            type={type}
                          />
                        </div>
                      ),
                  },
                ]}
              />
            </CardContent>
          </Card>
        </div>
      </div>
      {/* <FormDetailsFacilityAtom
        open={open}
        setOpenDetail={setOpenDetail}
        dataDetail={dataDetail}
        type={dataType}
        dataHierarchy={dataHierarchy}
      /> */}
    </div>
  );
}
// Type and required properties
FacilityModuleModifyView.propTypes = {};
// Default properties
FacilityModuleModifyView.defaultProps = {};
