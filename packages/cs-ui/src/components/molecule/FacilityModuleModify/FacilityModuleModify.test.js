import FacilityModuleModify from './FacilityModuleModify';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FacilityModuleModify is in the DOM', () => {
  render(<FacilityModuleModify></FacilityModuleModify>)
  expect(screen.getByTestId('FacilityModuleModifyTestId')).toBeInTheDocument();
})
