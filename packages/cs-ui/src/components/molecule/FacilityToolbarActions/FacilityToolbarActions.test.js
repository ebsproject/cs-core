import FacilityToolbarActions from './FacilityToolbarActions';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('FacilityToolbarActions is in the DOM', () => {
  render(<FacilityToolbarActions></FacilityToolbarActions>)
  expect(screen.getByTestId('FacilityToolbarActionsTestId')).toBeInTheDocument();
})
