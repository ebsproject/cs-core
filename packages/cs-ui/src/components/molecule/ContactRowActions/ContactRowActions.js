import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  findContact,
  contactProcess,
  findHierarchyTree,
} from "store/modules/DataPersist";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
import DeleteContact from "components/atom/DeleteContact";
const { IconButton, Tooltip, Typography, Box, CircularProgress } = Core;
const { Edit, PersonAdd, RoomService, ViewList, Visibility } = Icons;
import { RBAC } from "@ebs/layout";
import ContactsListAssing from "components/molecule/ContactsListAssign";
import InstitutionActiveSwitch from "components/atom/ActiveSwitch";
import ViewContact from "../ContactForm/ViewContact";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const ContactRowActionsAtom = React.forwardRef(({ rowData, refresh }, ref) => {
  const [defaultValues, setDefaultValues] = useState(null);
  const { type } = useSelector(({ persist }) => persist);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isVisible, setIsVisible] = useState(false);
  const [dataUnit, setDataUnit] = useState(null);
  const [open, setOpen] = useState(false);

  /**
   * @param {String} type
   * @param {String} method
   * @param {ID} id: optional
   */
  const handleGoTo = async ({ type, method, path }) => {
    setLoading(true);
    const { data } = await findContact({ id: rowData.id, type: type });
    setDefaultValues(data);
    dispatch(contactProcess({ type, method, defaultValues: data }));
    setLoading(false);
    navigate(`/cs${path}`);
  };
  const handleGoToV1 = ({ type, method, path, institutionId }) => {
    contactProcess({ type, method, institutionId })(dispatch);
    navigate(`/cs${path}`);
  };

  const handleOpen = () => {
    setDataUnit(rowData);
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    /**
     * @prop data-testid: Id to use inside ContactRowActions.test.js file.
     */
    <div
      className="content-center"
      data-testid={"ContactRowActionsTestId"}
      //className="flex flex-row flex-nowrap gap-2"
    >
      <Box
        component="div"
        ref={ref}
        className="-ml-2 flex flex-auto"
        //  className="ml-4 flex flex-auto"
      >
        <RBAC allowedAction={"View"} product={"CRM"} domain={"Core System"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"View"} />
              </Typography>
            }
          >
            <IconButton size="small" color="primary" onClick={handleOpen}>
              <Visibility />
            </IconButton>
          </Tooltip>
        </RBAC>
        <RBAC allowedAction={"Modify"} product={"CRM"} domain={"Core System"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Edit"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              color="primary"
              onClick={() =>
                handleGoTo({
                  type: type,
                  method: "PUT",
                  path: "/contact",
                })
              }
            >
              {loading === false ? <Edit /> : <CircularProgress />}
            </IconButton>
          </Tooltip>
        </RBAC>
        <RBAC allowedAction={"Delete"} product={"CRM"} domain={"Core System"}>
          <DeleteContact contactId={rowData.id} refresh={refresh} type={type} />
        </RBAC>
        {type === "INSTITUTION" && (
          <div className="ml-1 flex flex-auto">
            <RBAC allowedAction={"Assign_New_Contact"}>
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"Create and assign new contact"}
                    />
                  </Typography>
                }
              >
                <IconButton
                  size="small"
                  color="primary"
                  onClick={() =>
                    handleGoToV1({
                      type: "PERSON",
                      method: "ASSIGN",
                      path: "/contact",
                      institutionId: rowData.id,
                    })
                  }
                >
                  <PersonAdd />
                </IconButton>
              </Tooltip>
            </RBAC>

            <RBAC allowedAction={"Assign_Contact"}>
              <ContactsListAssing rowSelected={rowData} refresh={refresh} />
            </RBAC>
            {isVisible ? (
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"Manage Units"}
                    />
                  </Typography>
                }
              >
                <IconButton
                  size="small"
                  color="primary"
                  onClick={() =>
                    handleGoToV1({
                      type: "INSTITUTION",
                      method: "ASSIGN",
                      path: "/units",
                      institutionId: rowData,
                    })
                  }
                >
                  <ViewList />
                </IconButton>
              </Tooltip>
            ) : (
              ""
            )}
            <RBAC
              allowedAction={"Modify"}
              product={"CRM"}
              domain={"Core System"}
            >
              <InstitutionActiveSwitch
                method="PUT"
                refresh={refresh}
                rowData={rowData}
                type="INSTITUTION"
              />
            </RBAC>
          </div>
        )}
      </Box>
      <ViewContact open={open} handleClose={handleClose} data={rowData} />
    </div>
  );
});
// Type and required properties
ContactRowActionsAtom.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func,
};
// Default properties
ContactRowActionsAtom.defaultProps = {};

export default ContactRowActionsAtom;
