import ContactRowActions from "./ContactRowActions";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";
let translation={
  none:"test"
}

afterEach(cleanup);

test('ContactRowActions is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <ContactRowActions
      rowData={{ id: "1" }}
      refresh={() => {}}
    ></ContactRowActions>
       </IntlProvider>
    </Provider>
  )
  expect(screen.getByTestId('ContactRowActionsTestId')).toBeInTheDocument();
})

