import React from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND ATOMS TO USE
import AddProductButton from "components/molecule/AddProductButton";
import { Core, Icons, Styles } from "@ebs/styleguide"
const { Typography, Button, Menu, Tooltip } = Core
const { PostAdd } = Icons;
const { useTheme } = Styles;



//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const NewDomainsMenuButtonMolecule = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const theme = useTheme();
    const classes = {
      root: {
        display: "flex",
      },
      paper: {
        marginRight: theme.spacing(2),
      },
      button: {
        "&:hover": {
          backgroundColor: theme.palette.secondary.main,
        },
        color: theme.palette.common.white,
        backgroundColor: theme.palette.primary.main,
        marginLeft: theme.spacing(1),
        borderRadius: 4,
      },
    };

    const handleOpen = (event) => {
      setAnchorEl(event.currentTarget);
    };

    const handleMenuClose = () => {
      setAnchorEl(null);
    };

    return (
      /* 
     @prop data-testid: Id to use inside newdomainsmenubutton.test.js file.
     */
      <div
        ref={ref}
        data-testid={"NewDomainsMenuButtonTestId"}
        sx={classes.root}
      >
        <Tooltip title={<FormattedMessage id="none" defaultMessage="New Module" />}>
          <Button
            aria-controls="new-domain-menu"
            aria-haspopup="true"
            onClick={handleOpen}
          >
            <PostAdd />
            <Typography className="text-white">
              <FormattedMessage id="none" defaultMessage="Register a Module" />
            </Typography>
          </Button>
        </Tooltip>
        <Menu
          id="new-domain-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleMenuClose}
        >
          <AddProductButton
            rowSelected={rowSelected}
            refresh={refresh}
            handleMenuClose={handleMenuClose}
          />
        </Menu>
      </div>
    );
  }
);
// Type and required properties
NewDomainsMenuButtonMolecule.propTypes = {
  refresh: PropTypes.func.isRequired,
  rowSelected: PropTypes.object,
};
// Default properties
NewDomainsMenuButtonMolecule.defaultProps = {};

export default NewDomainsMenuButtonMolecule;
