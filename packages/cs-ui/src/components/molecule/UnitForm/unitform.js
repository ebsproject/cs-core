import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { client, clientSM } from "utils/apollo";
import { useQuery } from "@apollo/client";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
const { CheckCircle, Cancel } = Icons;
const { TextField, Tooltip, Button, Typography, Dialog, DialogTitle } = Core;
import { FormattedMessage } from "react-intl";
import FormSelect from "components/atom/FormSelect";
import ServiceTypeSelect from "components/atom/ServiceTypeSelect";
import { useSelector, useDispatch } from "react-redux";
import { showMessage } from "store/modules/message";
import {
  CreateUnit,
  CreateServiceProvider,
  ModifyServiceProvider,
  ModifyUnit,
} from "./functions";
import { getContext } from "@ebs/layout";
import { findServiceById } from "store/modules/SM";

const UnitFormMolecule = React.forwardRef((props, ref) => {
  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState("sm");
  const [unitId, setUnitId] = React.useState(0);

  const { institutionId } = useSelector(({ persist }) => persist);

  // Properties of the molecule
  const { onClose, open, rowData, metodo, refresh, ...rest } = props;

  const {
    control,
    register,
    handleSubmit,
    watch,
    formState: { errors },
    setValue,
  } = useForm({
    defaultValues:
      metodo === "PUT"
        ? {
            commonName: rowData.institution.commonName,
            legalName: rowData.institution.legalName,
          }
        : {},
  });

  const onSubmit = async (data) => {   
  };

  return (
    <Dialog
      onClose={onClose}
      aria-labelledby="dialog-title"
      open={open}
      fullWidth={fullWidth}
      maxWidth={maxWidth}
    >
      <DialogTitle id="dialog-title">
        {metodo === "PUT"
          ? "Modify Unit of " + `${institutionId?.legalName}`
          : "New Unit of " + `${institutionId?.legalName}`}
      </DialogTitle>

      <form
        onSubmit={handleSubmit(onSubmit)}
        name="UnitForm"
        data-testid="UnitForm"
        className="grid grid-cols-2 gap-4 m-5"
      >
        <Controller
          name="commonName"
          control={control}
          render={({ field }) => (
            <TextField
              InputLabelProps={{ shrink: true }}
              {...field}
              required
              label={
                <FormattedMessage id="none" defaultMessage="Abbreviation" />
              }
              data-testid={"commonName"}
            />
          )}
        />
        <Controller
          name="legalName"
          control={control}
          render={({ field }) => (
            <TextField
              InputLabelProps={{ shrink: true }}
              {...field}
              required
              label={<FormattedMessage id="none" defaultMessage="Unit" />}
              data-testid={"legalName"}
            />
          )}
        />

        <ServiceTypeSelect
          control={control}
          errors={errors}
          {...register("serviceTypeSelector")}
          setValue={setValue}
          serviceTypeSelected={
            metodo === "PUT" ? serviceProvider?.servicetypes[0] : []
          }
        />

        <FormSelect
          control={control}
          errors={errors}
          {...register("formsSelector")}
          setValue={setValue}
          formSelected={metodo === "PUT" ? serviceProvider?.ebsForms[0] : []}
        />

        <Button
          data-testid="cancel"
          onClick={onClose}
          startIcon={<Cancel />}
          className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
        >
          <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
        </Button>

        <Button
          data-testid="save"
          type="submit"
          startIcon={<CheckCircle />}
          className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
        >
          <FormattedMessage
            id={"none"}
            defaultMessage={"Save"}
            refresh={refresh}
          />
        </Button>
      </form>
    </Dialog>
  );
});
// Type and required properties
UnitFormMolecule.propTypes = {};
// Default properties
UnitFormMolecule.defaultProps = {};

export default UnitFormMolecule;
