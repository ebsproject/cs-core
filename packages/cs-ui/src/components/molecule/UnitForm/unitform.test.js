import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import UnitForm from './unitform'
// Test Library
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";
let translation={
  none:"test"
}

afterEach(cleanup);

test('UnitForm is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <UnitForm></UnitForm>
       </IntlProvider>
    </Provider>
  )
  expect(screen.getByTestId('UnitForm')).toBeInTheDocument();
})

