import { client, clientSM } from "utils/apollo";
import {
  CREATE_SERVICE_PROVIDER,
  MODIFY_SERVICE_PROVIDER,
} from "utils/apollo/gql/sm";

export const CreateServiceProvider = async (
  serviceProviderId,
  code,
  name,
  tenant,
  crop,
  serviceType,
  ebsForm,
  dispatch
) =>
  new Promise(async (resolve, reject) => {
    try {
      const serviceProviderInput = {
        id: serviceProviderId,
        code: code,
        name: name,
        tenant: tenant,
        idCrop: crop,
        serviceType: {
          id: serviceType,
        },
        ebsForm: {
          id: ebsForm,
        },
      };

      clientSM
        .mutate({
          mutation: CREATE_SERVICE_PROVIDER,
          variables: {
            ServiceProviderTo: serviceProviderInput,
          },
        })
        .then((result) => {
          resolve(true);
        });
      // .catch(async (e) => {
      //   await client.mutate({
      //     mutation: DELETE_CONTACT,
      //     variables: { contactId: serviceProviderId },
      //   });
      //   dispatch(
      //     showMessage({
      //       message: `Unit was not created `,
      //       variant: "error",
      //       anchorOrigin: {
      //         vertical: "top",
      //         horizontal: "right",
      //       },
      //     })
      //   );
      //   reject(false);
      // });
    } catch (error) {
      reject(error);
    }
  });

export const ModifyServiceProvider = async (
  id,
  code,
  name,
  tenant,
  crop,
  serviceType,
  ebsForm
) =>
  new Promise(async (resolve, reject) => {
    try {
      const serviceProviderInput = {
        id: `${id}`,
        code: code,
        name: name,
        tenant: tenant,
        idCrop: crop,
        serviceType: {
          id: serviceType,
        },
        ebsForm: {
          id: ebsForm,
        },
      };
      clientSM
        .mutate({
          mutation: MODIFY_SERVICE_PROVIDER,
          variables: {
            ServiceProviderTo: serviceProviderInput,
          },
        })
        .then((result) => {
          resolve(true);
        })
        .catch((e) => {
          reject(e);
        });
    } catch (error) {
      reject(error);
    }
  });
