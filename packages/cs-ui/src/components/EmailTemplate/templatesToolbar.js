import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import {Core,Icons} from "@ebs/styleguide";
const { Button, Tooltip, Typography } = Core;
const {PostAdd} = Icons;
import {RBAC} from "@ebs/layout"
import { setMethod } from "store/modules/EmailTemplate";
//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const TemplatesToolbar = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const handleGo = () => {
      dispatch(setMethod("POST"))
      navigate(`/cs/template-editor`);       
    };    
    return (
      /**
       * @prop data-testid: Id to use inside RolesToolbar.test.js file.
       */
      <div
        component="div"
        ref={ref}
        data-testid={"RolesToolbarTestId"}
        className="ml-3"
      >
        <RBAC allowedAction = {"Create"} domain = {"Core System"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id={"none"} defaultMessage={"Add new Email template"} />
            </Typography>
          }
        >
          <Button
            onClick={handleGo}
            startIcon={<PostAdd />}
            className="bg-ebs-brand-default hover:bg-ebs-brand-900 border-l-8 text-white"
          >
              <Typography className="text-sm font-ebs">
                <FormattedMessage id={"none"} defaultMessage={"New Template"} />
              </Typography>  
          </Button>
        </Tooltip>
        </RBAC>
      </div>
    );
  }
);
// Type and required properties
TemplatesToolbar.propTypes = {};
// Default properties
TemplatesToolbar.defaultProps = {};

export default TemplatesToolbar;