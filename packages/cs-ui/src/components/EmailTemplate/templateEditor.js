import React, { useEffect, useState, useContext } from "react";
import { useQuill } from "react-quilljs";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import 'quill/dist/quill.snow.css'
import { FormattedMessage } from "react-intl";
import toolbarEditor from "./toolbarEditor";
import { MutationEmailTemplate, modifyJobWf, deleteRelation, } from "./functionsEmailTemplates";
import { useDispatch } from "react-redux";
import { showMessage } from "store/modules/message";
import { userContext } from "@ebs/layout";
import JobWorkflowList from "./jobWorkflowList";
const { Autocomplete } = Lab;
const { Typography, Button, TextField, Checkbox, Chip, Grid } = Core;
const { CheckCircle, Cancel } = Icons;
//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const TemplateEditor = React.forwardRef(({ }, ref) => {
    const { defaultValues, method } = useSelector(({ emailTemplate }) => emailTemplate);
    const { userProfile } = useContext(userContext);
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const userTenants = userProfile.subscription.tenants;
    const [show, setShow] = useState(false);
    const [jobWf, setJobWf] = useState(null);
    const { quill, quillRef } = useQuill({
        modules: {
            toolbar: toolbarEditor
        }
    })
    let regex = new RegExp("^[A-ZÑa-zñáéíóúÁÉÍÓÚ'°]*");
    const onSubmit = (data) => {
        const tenants = [];
        data.tenants.map((item, index) => tenants.push(Number(item.id) || 0));
        const dataTemplate = {
            id: method === "PUT" ? defaultValues.id : 0,
            name: data.name,
            subject: data.subject,
            template: templateQuill,
            tenantIds: [1],
        };

        MutationEmailTemplate(dataTemplate, method).then(async (result) => {

            // const dataWf= {
            //     id: jobWf.id,
            //     jobTypeId: jobWf.jobType.id,
            //     productFunctionId:jobWf.productFunction.id,
            //     tenantId:jobWf.tenant.id,
            //     emailTemplateId: method === "PUT" ? result.data.modifyEmailTemplate.id : result.data.createEmailTemplate.id,            
            // }
            // let emailId = method === "PUT" ? result.data.modifyEmailTemplate.id : 0;

            // let Res = await deleteRelation(emailId);
            // modifyJobWf(dataWf)

            dispatch(showMessage({
                message: `Template was ${method == "PUT" ? "edited" : "created"} successfully `,
                variant: "success",
                anchorOrigin: {
                    vertical: "top",
                    horizontal: "right"
                }
            }));
        });
        navigate("/cs/email-template");
    }

    const [templateQuill, setTemplateQuill] = useState();

    useEffect(() => {
        if (quill) {
            quill.on('text-change', () => {
                setTemplateQuill(quillRef.current.firstChild.innerHTML)
            })
        }
    }, [quill]);
    useEffect(() => {
        if (method == "PUT") {
            const fetching = async () => {
                let editTemplate = quill.clipboard.convert({ html: defaultValues.template });
                setTemplateQuill(defaultValues.template)
                quill.setContents(editTemplate, 'silent');
            }
            if (quill) {
                fetching();
            }
        }
    }, [quill]);
    useEffect(() => {
        if (userTenants.length > 1) {
            setShow(true);
        } else {
            setValue("tenants", userTenants.map(item => { return { id: item.id, name: item.customer } }))
        }
    }, []);


    const {
        control,
        handleSubmit,
        register,
        formState: { errors },
        reset,
        setValue,
        getValues,
    } = useForm({
        defaultValues:
            method === "PUT"
                ? defaultValues
                : {
                    name: "",
                    subject: "",
                },
    });
    const handleCancel = (e) => {
        e.preventDefault();
        navigate("/cs/email-template");
    };

    return (
        /**
         * @prop data-testid: Id to use inside contact.test.js file.
         */
        <div ref={ref} data-testid={"TemplateEditorTestId"} >
            <form on onSubmit={handleSubmit(onSubmit)} style={{
                paddingTop: "14px",
                position: "relative",
                paddingLeft: "10px",
                paddingRight: "20px",
            }}>
                <Typography variant="h5" color="primary" className="flex-grow font-ebs col-span-2">
                    <FormattedMessage id="none" defaultMessage="Email Template" />
                </Typography>
                <Grid container justifyContent="flex-end" >

                    <Button
                        data-testid="cancel"
                        onClick={handleCancel}
                        startIcon={<Cancel />}
                        className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
                    >
                        <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
                    </Button>
                    <Button
                        data-testid="save"
                        type="submit"
                        startIcon={<CheckCircle />}
                        className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
                    >
                        <FormattedMessage id={"none"} defaultMessage={"Save"} />
                    </Button>
                </Grid>
                <div>
                    <div className="grid grid-cols-2 gap-5  " >
                        <Controller
                            name="name"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    {...field}
                                    error={!!errors["name"]}
                                    helperText={errors["name"] ? errors["name"].message : ''}
                                    label={
                                        <FormattedMessage id="none" defaultMessage="Name" />
                                    }
                                    onChange={(e) => {
                                        setValue("name", e.target.value);
                                    }}
                                />
                            )}
                            rules={{required:"This field is required"}}
                        />
                        <Controller
                            name="subject"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    InputLabelProps={{ shrink: true }}
                                    {...field}
                                    error={!!errors["subject"]}
                                    helperText={errors["subject"] ? errors["subject"].message : ''}
                                    label={
                                        <FormattedMessage id="none" defaultMessage="subject" />
                                    }
                                    onChange={(e) => {
                                        setValue("subject", e.target.value);
                                    }}
                                />
                            )}
                            rules={{required:"This field is required"}}

                        />
                    </div>
                    <div style={{
                        paddingTop: "20px",
                        position: "relative",
                        paddingLeft: "5px",
                        paddingBottom: "70px",
                        height: "600px",
                    }}
                    >
                        <Typography className="flex-grow font-ebs text-black  text-base">
                            <FormattedMessage
                                id="none"
                                defaultMessage="Template"
                            />
                        </Typography>
                        <div ref={quillRef} />
                    </div>

                </div>
            </form>
        </div>
    );
});
// Type and required properties
TemplateEditor.propTypes = {};
// Default properties
TemplateEditor.defaultProps = {};

export default TemplateEditor;
