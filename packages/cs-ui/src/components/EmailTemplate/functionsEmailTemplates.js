import { client } from "utils/apollo";
import {
    CREATE_EMAIL_TEMPLATE,
    MODIFY_EMAIL_TEMPLATE,
    DELETE_EMAIL_TEMPLATE,
    FIND_EMAIL_TEMPLATE_LIST,
    FIND_JOB_WORKFLOW_LIST,
    MODIFY_JOBWF,
} from "utils/apollo/gql/emailTemplate";


export const MutationEmailTemplate = (dataTemplate, method) =>
    new Promise((resolve, reject) => {
        try {
            client.mutate({
                mutation: method == "POST" ? CREATE_EMAIL_TEMPLATE : MODIFY_EMAIL_TEMPLATE,
                variables: {
                    emailTemplate: dataTemplate
                }
            })
                .then(result => {
                    resolve(result);
                }).
                catch(e => {
                    reject(e)
                })
        } catch (error) {
            reject(error)
        }
    })

export const deleteEmailTemplate = (emailTemplateId) =>
    new Promise((resolve, reject) => {
        try {
            client.mutate({
                mutation: DELETE_EMAIL_TEMPLATE,
                variables: { id: emailTemplateId }
            })
                .then(result => {
                    resolve(result)
                })
        } catch (error) {
            reject(error)
        }
    });
    export const modifyJobWf = async (jobWorkflow) =>
    {
        try {
            await client.mutate({
                mutation: MODIFY_JOBWF,
                variables: { jobWorkflow: jobWorkflow }
            })
            return true;
        } catch (error) {
           return false;
        }
    };

    export const deleteRelation = async (emailId) =>
    {
        try {
            const {data}= await client.mutate({
                mutation: FIND_JOB_WORKFLOW_LIST,
                variables: { 
                    filters: [
                        { col: "emailTemplate.id", mod: "EQ", val: emailId }
                      ]
                }
            })
            let res = data.findJobWorkflowList.content;
               if (res.length > 0 ){
                for (let index = 0; index < res.length; index++) {
                    const jobWf = res[index];
                    const dataWf= {
                        id: jobWf.id,
                        jobTypeId: jobWf.jobType.id,
                        productFunctionId:jobWf.productFunction.id,
                        tenantId:jobWf.tenant.id,
                        emailTemplateId: null,            
                    }
                    await modifyJobWf (dataWf);
                }
               }         
            return true;

        } catch (error) {
           return false;
        }
    };
