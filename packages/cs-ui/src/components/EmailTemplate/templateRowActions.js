import React from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
import { setDefaultValues, setMethod } from "store/modules/EmailTemplate";
import { useDispatch } from "react-redux";
import DeleteEmailTemplateButton from "./deleteEmailTemplateButton";
const {
    Box,
    Tooltip,
    Typography,
    IconButton,
} = Core;
const { Edit, } = Icons;
/**
  * @param rowData: Object with row data.
  * @param refresh: Function to refresh Grid data.
  */
export const rowActions = (rowData, refresh) => {
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const tenants = [];
    rowData.tenants.map((item, index) => tenants.push(Number(item.id) || 0));
    const defaultValues = {
        id: rowData.id,
        name: rowData.name,
        subject: rowData.subject,
        template: rowData.template,
        tenants: rowData.tenants,
    };
    const handleGoTo = () => {
        dispatch(setDefaultValues(defaultValues))
        dispatch(setMethod("PUT"))
        navigate("/cs/template-editor");
    };
    return (
        <div>
            {rowData.isSystem === true ? null : (
                <Box className="flex flex-row flex-nowrap gap-2">
                    <Tooltip
                        arrow
                        title={
                            <Typography className="font-ebs text-xl">
                                <FormattedMessage
                                    id={"none"}
                                    defaultMessage={"Edit template"}
                                />
                            </Typography>
                        }
                    >
                        <IconButton
                            onClick={handleGoTo}
                            color="primary"
                            className="text-green-600"
                        >
                            <Edit />
                        </IconButton>
                    </Tooltip>
                    <Tooltip>
                        <DeleteEmailTemplateButton rowSelected={rowData} refresh={refresh} />
                    </Tooltip>
                </Box>
            )}
        </div>
    );
};
