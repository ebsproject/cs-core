import React, { memo } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { getCoreSystemContext } from "@ebs/layout";
import { Core } from "@ebs/styleguide";
import TemplatesToolbar from "./templatesToolbar";
import { rowActions } from "./templateRowActions";
const { graphqlUri } = getCoreSystemContext();
const { Typography } = Core;
//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const Templates = () => {
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    { Header: "template", accessor: "template", hidden: true, disableGlobalFilter: true },
    { Header: "tenants.id ", accessor: "tenants.id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Tenant" />
        </Typography>
      ),
      accessor: "tenants.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Name",
      width: 500,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Subject" />
        </Typography>
      ),
      csvHeader: "Subject",
      accessor: "subject",
      width: 800,
    },
  ];
  return (
    <EbsGrid
      id="EmailTemplate"
      toolbar={true}
      columns={columns}
      uri={graphqlUri}
      entity="EmailTemplate"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Email Template" />
        </Typography>
      }
      rowactions={rowActions}
      toolbaractions={TemplatesToolbar}
      csvfilename="EmailTemplateList"
      callstandard="graphql"
      raWidth={110}
      select="multi"
      height="85vh"
    />
  )
};


export default memo(Templates);