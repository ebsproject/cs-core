import React, { useEffect } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Icons } from "@ebs/styleguide";
const {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Typography,
  Tooltip,
} = Core;
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { deleteEmailTemplate } from "./functionsEmailTemplates";
import { showMessage } from "store/modules/message";
const { Delete } = Icons;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteEmailTemplateButton = React.forwardRef(
  ({ rowSelected, refresh }, ref) => {
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();
    const { success } = useSelector(({ tenant }) => tenant);
    useEffect(() => {
      if (success) {
        handleClose();
        refresh();
      }
    }, [refresh, success]);
    const handleClickOpen = () => {
      setOpen(true);
    };

    const handleClose = () => {
      setOpen(false);
    };

    const handleDelete = () => {
      deleteEmailTemplate(rowSelected.id).then((result) => {
        dispatch(

          showMessage({
            message: "Deleted EmailTemplate",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })

        );
      }).
        catch(e => {
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
          reject(e)
        })
      handleClose();
      refresh();

    };
    return (
      /* 
     @prop data-testid: Id to use inside deletecustomerbutton.test.js file.
     */
      <div ref={ref} data-testid={"DeleteEmailTemplateButton"}>
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Delete Email Template" />
            </Typography>
          }
        >
          <IconButton
            onClick={handleClickOpen}
            aria-label="Delete Email Template"
            color="primary"
          >
            <Delete />
          </IconButton>
        </Tooltip>
        <Dialog
          fullWidth
          keepMounted
          maxWidth="sm"
          open={open}
          onClose={handleClose}
        >
          <DialogTitle>
            <FormattedMessage id="none" defaultMessage="Delete Email Template" />
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <FormattedMessage
                id="none"
                defaultMessage="Are you sure to delete following  Email Template?"
              />
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>
              <Typography variant="button" color="secondary">
                <FormattedMessage id="none" defaultMessage="Cancel" />
              </Typography>
            </Button>
            <Button onClick={handleDelete}>
              <Typography variant="button">
                <FormattedMessage id="none" defaultMessage="Confirm" />
              </Typography>
            </Button>
          </DialogActions>
        </Dialog>
      </div >
    );
  }
);
// Type and required properties
DeleteEmailTemplateButton.propTypes = {
  // rowSelected: PropTypes.object.isRequired,
  refresh: PropTypes.func.isRequired,
};
// Default properties
DeleteEmailTemplateButton.defaultProps = {};

export default DeleteEmailTemplateButton;
