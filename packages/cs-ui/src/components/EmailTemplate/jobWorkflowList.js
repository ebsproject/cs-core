import React, { useEffect, useState } from 'react'
import { useQuery } from "@apollo/client";
import { FIND_JOB_WORKFLOW_LIST } from "utils/apollo/gql/emailTemplate";
import { client } from 'utils/apollo'
import { Controller } from "react-hook-form";
import PropTypes from 'prop-types'
import { Core, Lab } from "@ebs/styleguide";
const { Autocomplete } = Lab;
const { FormControl, TextField, Typography } = Core;
import { FormattedMessage } from "react-intl";
import { sortBy } from 'lodash';


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const JobWorkflowList = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, control, setValue, errors, jobWfSelected,setJobWf,templateId,method, ...rest } = props

  //const [itemValue, setItemValue] = useState(null);
  //const [inputValue, setInputValue] = React.useState('');
  
  // useEffect (()=>{
  //   if (jobWfSelected != null)
  //   {
  //     setValue('jobWfList', jobWfSelected)
  //   }
  // },[])

  let defaultValues = {
    name: "",
    description: "",
    productFunction:{
      description: "",
    }
  }
  const { loading, error, data } = useQuery(FIND_JOB_WORKFLOW_LIST, {
    variables: {
           
    },
    client: client
  });
  if (loading) return null;
  if (error) return `Error! ${error}`;
  const result = data.findJobWorkflowList.content.filter(item => item.emailTemplate === null);  
  const defVal = data.findJobWorkflowList.content.filter( item => item.emailTemplate !== null && item.emailTemplate.id === templateId)
  if (method === "PUT" && defVal.length > 0)
  defaultValues = defVal[0]


  return (

    <FormControl fullWidth>      
      <Controller
        name={`jobWfList`}
        control={control}        
        render={({ field }) => (
          <Autocomplete
            {...field}
            options={result}
            getOptionLabel={(option) => 
              `${option.name}  /  ${option.description}  /  ${option.productFunction.description}`
            }
            isOptionEqualToValue={(option, value) =>
                  value === undefined ||
                  value === "" ||
                  option.name === value.name
                }
            
            renderInput={(params) => (
              <TextField
                {...params}
                error={Boolean(errors["jobWfList"])}
                label="Choose a Job Workflow List"
                placeholder="Job Workflow List"
              />
            )}
            
            onChange={(event, newValue) => {
              setValue('jobWfList', newValue)
              setJobWf(newValue)
            }}
          />
        )}    
        
        defaultValue={defaultValues}        
      />      
    </FormControl>
  )
})
// Type and required properties
JobWorkflowList.propTypes = {
}
// Default properties
JobWorkflowList.defaultProps = {
}

export default JobWorkflowList
