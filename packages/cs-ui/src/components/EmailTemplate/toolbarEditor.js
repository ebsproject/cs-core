const toolbarEditor = [    
    ['bold', 'italic', 'underline', 'strike'],
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],    
    [{ align: [] }],
    [{ color: [] }, { background: [] }],
    [],
  
    [{ list: 'ordered' }, { list: 'bullet' }],
    [{ indent: '-1' }, { indent: '+1' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],  
    [{ 'direction': 'rtl' }],    
    
    ['blockquote', 'code-block'],
    [{ 'header': 1 }, { 'header': 2 }],
    ['link'], 
    [],
    
  
    ['clean']
  ]
  
  export default toolbarEditor
  