import React, { useEffect, memo, useContext, useState } from "react";
import PropTypes from "prop-types";
import { client } from "utils/apollo";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import ContactRowActions from "components/molecule/ContactRowActions";
import { FormattedMessage } from "react-intl";
import { EbsGrid } from "@ebs/components";
import { contactProcess } from "store/modules/DataPersist";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";
import { Core, DropzoneDialog, Icons } from "@ebs/styleguide";
const { Button, Typography, Tooltip, DialogActions, Box } = Core;
const { PostAdd, GetApp, Publish } = Icons;
import { SET_TYPE_CONTACT, process } from "store/modules/DataPersist";
import { printoutClient, client as clientApi } from "utils/axios";
import { showMessage } from "store/modules/message";

import { RBAC, useLayoutContext } from "@ebs/layout";
import { Designer } from "@ebs/po";
import Cookies from "js-cookie";
import ContactsDetail from "components/molecule/CrmDetail";
import { useQuery } from "@apollo/client";
import { FIND_ORGANIZATION_LIST } from "utils/apollo/gql/tenantManagement";
import { FIND_CONTACT_TYPE_LIST } from "utils/apollo/gql/crm";
import { EbsFilter } from "@ebs/components";
import axios from "axios";
import { getTokenId, getCoreSystemContext } from "@ebs/layout";
const { restUri } = getCoreSystemContext();

//MAIN FUNCTION
/**
 * @param {}: component properties
 * @param ref: reference made by React.forward
 */
const PeopleOrganism = React.forwardRef(({}, ref) => {
  const [width, setWidth] = useState(window.innerWidth);
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { userProfile } = useLayoutContext();
  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth || 1300);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [window.innerWidth]);
  useEffect(() => {
    dispatch(process({ type: SET_TYPE_CONTACT, payload: "PERSON" }));
  }, []);
  const { data, error } = useQuery(FIND_ORGANIZATION_LIST, {
    variables: {},
    fetchPolicy: "no-cache",
    client: client,
  });
  const { data: dataContacType } = useQuery(FIND_CONTACT_TYPE_LIST, {
    variables: {
      page: { number: 1, size: 10 },
      filters: [{ col: "category.name", mod: "EQ", val: "Person" }],
    },
    fetchPolicy: "no-cache",
    client: client,
  });
  const typeContactList = dataContacType?.findContactTypeList?.content;
  const organizationList = data?.findOrganizationList?.content;

  const permissions = userProfile.permissions;
  let roleActive = permissions?.memberOf?.roles;
  let ebsGrid = roleActive?.includes("Admin" || "CS Admin");
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Family Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.familyName",
      csvHeader: "Family Name",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Given Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.givenName",
      csvHeader: "Given Name",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Additional name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.additionalName",
      csvHeader: "Additional Name",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Job Title" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "person.jobTitle",
      csvHeader: "Job Title",
      width: 200,
      filter: true,
    },
    {
      Header: "contactTypes.id",
      accessor: "contactTypes.id",
      hidden: true,
    },
    {
      Header: "contactTypes.name",
      accessor: "contactTypes.name",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "tenants.id",
      accessor: "tenants.id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "tenants.name",
      accessor: "tenants.name",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "tenants.organization.name",
      accessor: "tenants.organization.name",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "addresses.id",
      accessor: "addresses.id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "addresses.streetAddress",
      accessor: "addresses.streetAddress",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "addresses.purpose.name",
      accessor: "addresses.purpose.name",
      hidden: true,
      disableGlobalFilter: true,
    },
  ];
  const ContactDetail = ({ rowData }) => {
    return (
      <ContactsDetail
        contact_types={rowData.contactTypes}
        addresses={rowData.addresses}
        tenants={rowData.tenants}
      />
    );
  };
  /**
   * @param {String} type
   * @param {String} method
   * @param {ID} id: optional
   */
  const handleGoTo = ({ type, method, path }) => {
    contactProcess({ type, method })(dispatch);
    navigate(`/cs${path}`);
  };
  const actionButtonDownload = () => {
    const columns = [
      "email",
      "family_Name",
      "given_Name",
      "additional_Name",
      "gender",
      "job_Title",
      "knows_About",
    ];
    const file = columns.join(",") + "\n";
    const blob = new Blob([file], { type: "text/csv;charset=utf-8;" });
    const link = document.createElement("a");
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "person_import_template.csv"); // Nombre del archivo
    link.style.visibility = "hidden";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const cookie = Cookies.get("filters");
  let filterItem;
  if (cookie) filterItem = JSON.parse(cookie);

  const filterGlobalCookie = () => {
    return [];
    // let defaultFilter = [];
    // if (
    //   filterItem === undefined ||
    //   Object.entries(filterItem.filterValues).length === 0
    // ) {
    //   return [];
    // } else {
    //   const datafilter = filterItem.filterValues.Tenant;
    //   datafilter.map((item) => {
    //     defaultFilter.push({ col: "tenants.id", mod: "EQ", val: item.id });
    //   });
    //   return defaultFilter;
    // }
  };
  /**
   * @param dataSelection: Array object with data rows selected.
   * @param refresh: Function to refresh Grid data.
   */
  const toolbarActions = (selectedRows, refresh) => {
    const onSubmit = async (files) => {
      try {
        const formData = new FormData();
        let _file = files[0];
        let headers = {
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${getTokenId()}`,
        };
        headers = { ...headers, "Content-Type": "multipart/form-data" };
        formData.append("file", _file);
        const data = await axios.request({
          url: new URL(`/importFiles/person`, restUri).toString(),
          method: "post",
          headers: headers,
          data: formData,
        });
        const responseJson = data.status;
        if (responseJson === 200) {
          dispatch(
            showMessage({
              message: "Create contact",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        }
      } catch (error) {
        dispatch(
          showMessage({
            message:
              "An error occurred when entering your file, check that the data: email, familyName, givenName, additionalName are different, or that these same data do not already exist in CRM.",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      }
      setOpen(false);
    };
    const componentsFilter = [
      {
        id: "organizations",
        nameFilter: "organizations",
        options: organizationList === undefined ? [] : organizationList,
        product: "People",
      },
      {
        id: "contactType",
        nameFilter: "contactType",
        options: typeContactList === undefined ? [] : typeContactList,
        product: "People",
      },
    ];
    return (
      <div>
        {width > 1300 && (
          <Box
            sx={{
              display: "flex",
              flexDirection: "row-reverse",
              alignItems: "center",
            }}
          >
            <DialogActions>
              <RBAC
                allowedAction={"New_Person"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Tooltip
                  arrow
                  placement="top"
                  title={
                    <Typography className="font-ebs text-lg">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"New Person"}
                      />
                    </Typography>
                  }
                >
                  <Button
                    variant="contained"
                    aria-label="add-contact"
                    color="primary"
                    className=" bg-ebs-brand-default"
                    startIcon={<PostAdd className="text-white" />}
                    onClick={() =>
                      handleGoTo({
                        type: "PERSON",
                        method: "POST",
                        path: "/contact",
                      })
                    }
                    data-testid={"PostButton"}
                  >
                    <Typography className="text-white text-sm font-ebs">
                      <FormattedMessage id="none" defaultMessage="New Person" />
                    </Typography>
                  </Button>
                </Tooltip>
              </RBAC>
              <RBAC
                allowedAction={"Download_Template_People"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Tooltip
                  arrow
                  placement="top"
                  title={
                    <Typography className="font-ebs text-lg">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"Download Template"}
                      />
                    </Typography>
                  }
                >
                  <Button
                    variant="contained"
                    color="primary"
                    className=" bg-ebs-brand-default"
                    startIcon={<GetApp className="fill-current text-white" />}
                    download="Person_import"
                    onClick={actionButtonDownload}
                  >
                    <Typography className="text-white text-sm font-ebs">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Download Template"
                      />
                    </Typography>
                  </Button>
                </Tooltip>
              </RBAC>
              <RBAC
                allowedAction={"Import_Template_People"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Tooltip
                  arrow
                  placement="top"
                  title={
                    <Typography className="font-ebs text-lg ">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"Import Person"}
                      />
                    </Typography>
                  }
                >
                  <Button
                    variant="contained"
                    className=" bg-ebs-brand-default"
                    color="primary"
                    startIcon={
                      <Publish className="fill-current text-white font-ebs" />
                    }
                    onClick={handleClickOpen}
                  >
                    <Typography className="text-white text-sm font-ebs">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Import People"
                      />
                    </Typography>
                  </Button>
                </Tooltip>
              </RBAC>
              <RBAC
                allowedAction={"Print"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Designer
                  reportName={"sys_people_contact_report"}
                  mode={"preview"}
                />
              </RBAC>
            </DialogActions>
            <EbsFilter componentsFilter={componentsFilter} />
          </Box>
        )}
        {width < 1300 && (
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
            }}
            className={"gap-4 ml-2"}
          >
            <RBAC
              allowedAction={"New_Person"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"New Person"}
                    />
                  </Typography>
                }
              >
                <Button
                  variant="contained"
                  aria-label="add-contact"
                  color="primary"
                  className=" bg-ebs-brand-default"
                  startIcon={<PostAdd className="text-white" />}
                  onClick={() =>
                    handleGoTo({
                      type: "PERSON",
                      method: "POST",
                      path: "/contact",
                    })
                  }
                  data-testid={"PostButton"}
                ></Button>
              </Tooltip>
            </RBAC>
            <RBAC
              allowedAction={"Download_Template_People"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"Download Template"}
                    />
                  </Typography>
                }
              >
                <Button
                  variant="contained"
                  color="primary"
                  className=" bg-ebs-brand-default"
                  startIcon={<GetApp className="fill-current text-white" />}
                  download="Person_import"
                  onClick={actionButtonDownload}
                ></Button>
              </Tooltip>
            </RBAC>
            <RBAC
              allowedAction={"Import_Template_People"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg ">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"Import Person"}
                    />
                  </Typography>
                }
              >
                <Button
                  variant="contained"
                  className=" bg-ebs-brand-default"
                  color="primary"
                  startIcon={
                    <Publish className="fill-current text-white font-ebs" />
                  }
                  onClick={handleClickOpen}
                ></Button>
              </Tooltip>
            </RBAC>
            <RBAC
              allowedAction={"Print"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Designer
                reportName={"sys_people_contact_report"}
                mode={"preview"}
              />
            </RBAC>
            <EbsFilter componentsFilter={componentsFilter} />
          </Box>
        )}

        <DropzoneDialog
          acceptedFiles={[".csv"]}
          cancelButtonText={"cancel"}
          submitButtonText={"submit"}
          maxFileSize={5000000}
          open={open}
          onClose={handleClose}
          onSave={onSubmit}
          showPreviews={true}
          showFileNamesInPreview={true}
        />
      </div>
    );
  };
  /**
   * @param {Object} page
   * @param {Object[]} sort
   * @param {Object[]} filters
   * @returns Promise
   */
  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      const data = filterGlobalCookie();
      filters.length == 0
        ? filters.push({ col: "category.name", mod: "EQ", val: "Person" })
        : null;
      if (data)
        try {
          client
            .query({
              query: FIND_CONTACT_LIST,
              variables: {
                page: page,
                sort:
                  sort.length === 0
                    ? [{ col: "person.familyName", mod: "ASC" }]
                    : sort,
                filters: [...filters, ...data],
                disjunctionFilters: filters.length === 1 ? true : false,
              },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                resolve({
                  pages: data.findContactList.totalPages,
                  elements: data.findContactList.totalElements,
                  data: data.findContactList.content,
                });
              }
            });
        } catch (e) {
          reject(e);
        } finally {
        }
    });
  };
  if (ebsGrid) {
    return (
      /**
       * @prop data-testid: Id to use inside people.test.js file.
       */
      <div data-testid={"PeopleTestId"}>
        <EbsGrid
          id="People"
          toolbar={true}
          data-testid={"PeopleTestId"}
          columns={columns}
          title={
            <Typography variant="h5" color="primary">
              <FormattedMessage id="none" defaultMessage="Registers" />
            </Typography>
          }
          toolbaractions={toolbarActions}
          detailcomponent={ContactDetail}
          rowactions={ContactRowActions}
          csvfilename="peopleList"
          fetch={fetch}
          height="85vh"
          select="multi"
          auditColumns
        />
      </div>
    );
  } else {
    return (
      /**
       * @prop data-testid: Id to use inside people.test.js file.
       */
      <div data-testid={"PeopleTestId"}>
        <EbsGrid
          id="People"
          toolbar={true}
          data-testid={"PeopleTestId"}
          columns={columns}
          title={
            <Typography variant="h5" color="primary">
              <FormattedMessage id="none" defaultMessage="Registers" />
            </Typography>
          }
          toolbaractions={toolbarActions}
          rowactions={ContactRowActions}
          csvfilename="peopleList"
          fetch={fetch}
          height="85vh"
          select="multi"
        />
      </div>
    );
  }
});
// Type and required properties
PeopleOrganism.propTypes = {};
// Default properties
PeopleOrganism.defaultProps = {};

export default memo(PeopleOrganism);
