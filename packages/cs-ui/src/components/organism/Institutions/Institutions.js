import React, { useEffect, useContext, useState } from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
import { client } from "utils/apollo";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import ContactRowActions from "components/molecule/ContactRowActions";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/crm";
import { Core, DropzoneDialog, Icons, Styles } from "@ebs/styleguide";

const {
  Button,
  Tooltip,
  Typography,
  DialogActions,
  Chip,
  FormControl,
  Select,
  OutlinedInput,
  MenuItem,
  Box,
} = Core;
const { PostAdd, GetApp, Publish } = Icons;
import { contactProcess } from "store/modules/DataPersist";
import { SET_TYPE_CONTACT, process } from "store/modules/DataPersist";
import {
  getCoreSystemContext,
  RBAC,
  userContext,
  getTokenId,
} from "@ebs/layout";
import { printoutClient, client as clientApi } from "utils/axios";
import { showMessage } from "store/modules/message";

import { Designer } from "@ebs/po";
import Cookies from "js-cookie";
import ContactsDetail from "components/molecule/CrmDetail";
import axios from "axios";
const { restUri } = getCoreSystemContext();

//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const InstitutionsOrganism = React.forwardRef(({}, ref) => {
  const { userProfile } = useContext(userContext);
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { printoutUri } = getCoreSystemContext();
  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth || 1300);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [window.innerWidth]);
  useEffect(() => {
    dispatch(process({ type: SET_TYPE_CONTACT, payload: "INSTITUTION" }));
  }, []);

  const permissions = userProfile?.permissions;
  let roleActive = permissions?.memberOf.roles;
  let ebsGrid = roleActive?.includes("Admin" || "CS Admin");

  const SelectColumnFilter = ({ column: { filterValue, setFilter } }) => {
    // console.log(filterValue);
    const classes = {
      formControl: {
        minWidth: 120,
      },
      input: {
        padding: "10px 14px",
      },
    };

    return (
      <RBAC
        allowedAction={"Modify"}
        product={"User Management"}
        domain={"Core System"}
      >
        <FormControl variant="outlined" sx={classes.formControl}>
          <Select
            value={filterValue || ""}
            onChange={(e) => {
              let filterData = e.target.value;
              setFilter(filterData.toString());
            }}
            input={<OutlinedInput sx={{ input: classes.input }} />}
            displayEmpty
          >
            <MenuItem value="">All</MenuItem>
            <MenuItem value="true">Is CGIAR</MenuItem>
            <MenuItem value="false">Not CGIAR</MenuItem>
          </Select>
        </FormControl>
      </RBAC>
    );
  };

  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <RBAC
          allowedAction={"Modify"}
          product={"User Management"}
          domain={"Core System"}
        >
          <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="CGIAR" />
          </Typography>
        </RBAC>
      ),
      Cell: ({ value }) => {
        return (
          <RBAC allowedAction={"Modify"} product={"CRM"} domain={"Core System"}>
            <Chip
              color={value === true ? "primary" : "secondary"}
              label={value === true ? "Is CGIAR" : "Not CGIAR"}
            />
          </RBAC>
        );
      },
      Filter: SelectColumnFilter,
      accessor: "institution.isCgiar",
      csvHeader: "User status",
      width: 150,
      sticky: "left",
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Common Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.commonName",
      csvHeader: "Common Name",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Legal Name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.legalName",
      csvHeader: "Legal Name",
      width: 200,
    },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Contact Name" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "institution.principalContact.person.familyName",
    //   csvHeader: "Contact Name",
    //   disableGlobalFilter: true,
    //   disableFilters: true,
    //   width: 200,
    // },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Contact Given Name" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "institution.principalContact.person.givenName",
    //   csvHeader: "Contact Given Name",
    //   disableGlobalFilter: true,
    //   disableFilters: true,
    //   width: 200,
    // },
    {
      Header: "contactTypes.id",
      accessor: "contactTypes.id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "contactTypes.name",
      accessor: "contactTypes.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "tenants.id",
      accessor: "tenants.id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "email",
      accessor: "email",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "tenants.name",
      accessor: "tenants.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "tenants.organization.name",
      accessor: "tenants.organization.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "addresses.id",
      accessor: "addresses.id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "addresses.streetAddress",
      accessor: "addresses.streetAddress",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "addresses.purpose.name",
      accessor: "addresses.purpose.name",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
  ];
  const ContactDetail = ({ rowData }) => {
    // console.log(rowData);
    return (
      <ContactsDetail
        contact_types={rowData.contactTypes}
        addresses={rowData.addresses}
        tenants={rowData.tenants}
      />
    );
  };
  /**
   * @param {String} type
   * @param {String} method
   * @param {ID} id: optional
   */
  const handleGoTo = ({ type, method, path, institutionId }) => {
    contactProcess({ type, method, institutionId })(dispatch);
    navigate(`/cs${path}`);
  };

  const actionButtonDownload = async () => {
    const columns = ["email", "common_Name", "legal_Name"];
    const file = columns.join(",") + "\n";
    const blob = new Blob([file], { type: "text/csv;charset=utf-8;" });
    const link = document.createElement("a");
    const url = URL.createObjectURL(blob);
    link.setAttribute("href", url);
    link.setAttribute("download", "person_import_template.csv"); // Nombre del archivo
    link.style.visibility = "hidden";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const cookie = Cookies.get("filters");
  let filterItem;
  if (cookie) filterItem = JSON.parse(cookie);

  const filterGlobalCookie = () => {
    return [];
    // let defaultFilter = [];
    // if (
    //   filterItem === undefined ||
    //   Object.entries(filterItem.filterValues).length === 0
    // ) {
    //   return [];
    // } else {
    //   const datafilter = filterItem.filterValues.Tenant;
    //   datafilter.map((item) => {
    //     defaultFilter.push({ col: "tenants.id", mod: "EQ", val: item.id });
    //   });
    //   return defaultFilter;
    // }
  };

  /**
   * @param dataSelection: Array object with data rows selected.
   * @param refresh: Function to refresh Grid data.
   */
  const toolbarActions = (selectedRows, refresh) => {
    const onSubmit = async (files) => {
      try {
        const formData = new FormData();
        let _file = files[0];
        let headers = {
          Accept: "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${getTokenId()}`,
        };
        headers = { ...headers, "Content-Type": "multipart/form-data" };
        formData.append("file", _file);
        const data = await axios.request({
          url: new URL(`/importFiles/institution`, restUri).toString(),
          method: "post",
          headers: headers,
          data: formData,
        });
        const responseJson = data.status;
        if (responseJson === 200) {
          dispatch(
            showMessage({
              message: "Create contact",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          refresh();
        }
      } catch (error) {
        dispatch(
          showMessage({
            message:
              "An error occurred when entering your file, check that the data: email, familyName, givenName, additionalName are different, or that these same data do not already exist in CRM.",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      }
      setOpen(false);
    };

    return (
      <div>
        {width > 1300 && (
          <Box
            sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}
          >
            <DialogActions>
              <RBAC
                allowedAction={"New_Institution"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Tooltip
                  arrow
                  placement="top"
                  title={
                    <Typography className="font-ebs text-lg">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"New Institution"}
                      />
                    </Typography>
                  }
                >
                  <Button
                    className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                    aria-label="add-contact"
                    startIcon={<PostAdd className="fill-current text-white" />}
                    onClick={() =>
                      handleGoTo({
                        type: "INSTITUTION",
                        method: "POST",
                        path: "/contact",
                      })
                    }
                  >
                    <Typography className="text-white text-sm font-ebs">
                      <FormattedMessage
                        id="none"
                        defaultMessage="New Institution"
                      />
                    </Typography>
                  </Button>
                </Tooltip>
              </RBAC>

              <RBAC
                allowedAction={"Download_Template_Institution"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Tooltip
                  arrow
                  placement="top"
                  title={
                    <Typography className=" font-ebs text-white text-lg">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Download Template"
                      />
                    </Typography>
                  }
                >
                  <Button
                    className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                    variant="contained"
                    color="primary"
                    startIcon={<GetApp className="fill-current text-white" />}
                    download="Institution_import"
                    onClick={actionButtonDownload}
                  >
                    <Typography className="text-white text-sm font-ebs">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Download Template"
                      />
                    </Typography>
                  </Button>
                </Tooltip>
              </RBAC>
              <RBAC
                allowedAction={"Import_Template_Institution"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Tooltip
                  arrow
                  placement="top"
                  title={
                    <Typography className="font-ebs text-white text-lg">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Import Institution"
                      />
                    </Typography>
                  }
                >
                  <Button
                    className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                    variant="contained"
                    color="primary"
                    startIcon={<Publish className="fill-current text-white" />}
                    onClick={handleClickOpen}
                  >
                    <Typography className="text-white text-sm font-ebs">
                      <FormattedMessage
                        id="none"
                        defaultMessage="Import Institution"
                      />
                    </Typography>
                  </Button>
                </Tooltip>
              </RBAC>
              <RBAC
                allowedAction={"Print"}
                product={"CRM"}
                domain={"Core System"}
              >
                <Designer
                  reportName={"sys_institution_contact_report"}
                  mode={"preview"}
                />
              </RBAC>
            </DialogActions>
          </Box>
        )}
        {width < 1300 && (
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
            className={"gap-4 ml-5"}
          >
            <RBAC
              allowedAction={"New_Institution"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"New Institution"}
                    />
                  </Typography>
                }
              >
                <Button
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                  aria-label="add-contact"
                  startIcon={<PostAdd className="fill-current text-white" />}
                  onClick={() =>
                    handleGoTo({
                      type: "INSTITUTION",
                      method: "POST",
                      path: "/contact",
                    })
                  }
                ></Button>
              </Tooltip>
            </RBAC>

            <RBAC
              allowedAction={"Download_Template_Institution"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className=" font-ebs text-white text-lg">
                    <FormattedMessage
                      id="none"
                      defaultMessage="Download Template"
                    />
                  </Typography>
                }
              >
                <Button
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                  variant="contained"
                  color="primary"
                  startIcon={<GetApp className="fill-current text-white" />}
                  download="Institution_import"
                  onClick={actionButtonDownload}
                ></Button>
              </Tooltip>
            </RBAC>
            <RBAC
              allowedAction={"Import_Template_Institution"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-white text-lg">
                    <FormattedMessage
                      id="none"
                      defaultMessage="Import Institution"
                    />
                  </Typography>
                }
              >
                <Button
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                  variant="contained"
                  color="primary"
                  startIcon={<Publish className="fill-current text-white" />}
                  onClick={handleClickOpen}
                ></Button>
              </Tooltip>
            </RBAC>
            <RBAC
              allowedAction={"Print"}
              product={"CRM"}
              domain={"Core System"}
            >
              <Designer
                reportName={"sys_institution_contact_report"}
                mode={"preview"}
              />
            </RBAC>
          </Box>
        )}

        <DropzoneDialog
          acceptedFiles={[".csv"]}
          cancelButtonText={"cancel"}
          submitButtonText={"submit"}
          maxFileSize={5000000}
          open={open}
          onClose={handleClose}
          onSave={onSubmit}
          showPreviews={true}
          showFileNamesInPreview={true}
        />
      </div>
    );
  };
  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      const data = filterGlobalCookie();
      try {
        client
          .query({
            query: FIND_CONTACT_LIST,
            variables: {
              page: page,
              sort:
                sort.length === 0
                  ? [{ col: "institution.commonName", mod: "ASC" }]
                  : sort,
              filters: [
                ...filters,
                ...data,
                { col: "category.name", mod: "EQ", val: "Institution" },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              resolve({
                pages: data.findContactList.totalPages,
                elements: data.findContactList.totalElements,
                data: data.findContactList.content,
              });
            }
          });
      } catch (e) {
        // console.log(e);
      }
    });
  };
  if (ebsGrid) {
    return (
      /**
       * @prop data-testid: Id to use inside institutions.test.js file.
       */
      <div data-testid={"InstitutionsTestId"}>
        <EbsGrid
          id="Institutions"
          toolbar={true}
          columns={columns}
          title={
            <Typography variant="h5" color="primary">
              <FormattedMessage id="none" defaultMessage="Institutions" />
            </Typography>
          }
          toolbaractions={toolbarActions}
          detailcomponent={ContactDetail}
          rowactions={ContactRowActions}
          csvfilename="institutionsList"
          fetch={fetch}
          height="85vh"
          select="multi"
          raWidth={162}
          auditColumns
        />
      </div>
    );
  } else {
    return (
      /**
       * @prop data-testid: Id to use inside institutions.test.js file.
       */
      <div data-testid={"InstitutionsTestId"}>
        <EbsGrid
          id="Institutions"
          toolbar={true}
          columns={columns}
          title={
            <Typography variant="h5" color="primary">
              <FormattedMessage id="none" defaultMessage="Institutions" />
            </Typography>
          }
          toolbaractions={toolbarActions}
          rowactions={ContactRowActions}
          csvfilename="institutionsList"
          fetch={fetch}
          height="85vh"
          select="multi"
          raWidth={162}
          auditColumns
        />
      </div>
    );
  }
});
// Type and required properties
InstitutionsOrganism.propTypes = {};
// Default properties
InstitutionsOrganism.defaultProps = {};

export default InstitutionsOrganism;
