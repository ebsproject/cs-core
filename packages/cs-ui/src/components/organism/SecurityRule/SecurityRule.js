import React from "react";

import { EbsTabsLayout } from "@ebs/styleguide";
import { useSelector } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import SecurityRuleForm from "components/atom/SecurityRuleForm";


//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const SecurityRuleOrganism = React.forwardRef(({}, ref) => {
  const { title, method, defaultValues } = useSelector(
    ({ rules }) => rules
  );
  return (
    /**
     * @prop data-testid: Id to use inside role.test.js file.
     */
    <div data-testid={"SecurityRuleTestId"} ref={ref}>
      <EbsTabsLayout
        orientation={"horizontal"}
        tabs={[
          {
            label: <FormattedMessage id={"none"} defaultMessage={title} />,
            component: (
              <SecurityRuleForm method={method} defaultValues={defaultValues} />
            ),
          },
        ]}
      />
    </div>
  );
});
// Type and required properties
SecurityRuleOrganism.propTypes = {};
// Default properties
SecurityRuleOrganism.defaultProps = {};

export default SecurityRuleOrganism;
