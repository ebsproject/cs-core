import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
// CORE COMPONENTS AND MOLECULES TO USE
const { Typography } = Core;
import { EbsGrid } from "@ebs/components";
import FieldDetailMolecule from "components/molecule/FieldDetail/FieldDetail";
import PlaceRowActions from "components/molecule/PlaceRowActions/PlaceRowActions";
import PlaceToolbarActions from "components/molecule/PlaceToolbarActions/PlaceToolbarActions";
import { FIND_PLACES_LIST } from "utils/apollo/gql/placeManager";
import { clientCb } from "utils/apollo/apollo";
import { sortBy } from "lodash";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PlacesOrganism = React.forwardRef(({}, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Site" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "geospatialObjectName",
      csvHeader: "Site",
      width: 174,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Site Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "geospatialObjectCode",
      csvHeader: "Site Code",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Site Type" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "geospatialObjectSubtype",
      csvHeader: "Site Type",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Country" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "parentGeospatialObject.geospatialObjectName",
      csvHeader: "Country",
      width: 200,
      sort: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Field" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "field",
      csvHeader: "Field",
      width: 200,
      disabledMenu: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Field Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "fieldCode",
      csvHeader: "FieldCode",
      width: 200,
      disabledMenu: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Planting Area" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "plantingArea",
      csvHeader: "Planting Area",
      width: 200,
      disabledMenu: false,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Planting Area Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "plantingAreaCode",
      csvHeader: "Planting Area Code",
      width: 200,
      disabledMenu: false,
    },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Date Created" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "dateCreated",
    //   csvHeader: "Date Created",
    //   width: 200,
    //   filter: true,
    // },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Coordinates" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return (
          <Typography variant="body1">
            {value?.type === "Polygon"
              ? value?.coordinates[0][0]
              : value?.coordinates[0]}
          </Typography>
        );
      },
      accessor: "coordinates.features[0].geometry",
      csvHeader: "Coordinates",
      width: 200,
      Filter: false,
      disabledMenu: false,
    },
  ];

  const fetch = async ({ page, sort, filters }) => {
    if (filters.length === 0) {
      return new Promise((resolve, reject) => {
        try {
          clientCb
            .query({
              query: FIND_PLACES_LIST,
              variables: {
                sort:
                  sort.length === 0
                    ? [
                        { col: "updatedOn", mod: "DES" },
                        { col: "createdOn", mod: "DES" },
                      ]
                    : sort,
                page: page,
                filters: [
                  ...filters,
                  // {
                  //   col: "updatedOn",
                  //   mod: "NOTNULL",
                  //   val: "",
                  // },
                  { col: "geospatialObjectType", mod: "EQ", val: "site" },
                ],
                disjunctionFilters: false,
              },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                resolve({
                  pages: data.findGeospatialObjectList.totalPages,
                  elements: data.findGeospatialObjectList.totalElements,
                  data: data.findGeospatialObjectList.content,
                });
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    } else {
      switch (filters[0].col) {
        case "plantingArea":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_PLACES_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "geospatialObjectType",
                        mod: "EQ",
                        val: "planting area",
                      },
                      {
                        col: "geospatialObjectName",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findGeospatialObjectList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.geospatialObjectType,
                        // geospatialObjectSubtype: item.geospatialObjectSubtype,
                        plantingArea: item.geospatialObjectName,
                        plantingAreaCode: item.geospatialObjectCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        altitude: item.altitude,
                      })
                    );
                    resolve({
                      pages: data.findGeospatialObjectList.totalPages,
                      elements: data.findGeospatialObjectList.totalElements,
                      // data: data.findGeospatialObjectList.content,
                      data: arrayList,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "plantingAreaCode":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_PLACES_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "geospatialObjectType",
                        mod: "EQ",
                        val: "planting area",
                      },
                      {
                        col: "geospatialObjectCode",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findGeospatialObjectList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.geospatialObjectType,
                        // geospatialObjectSubtype: item.geospatialObjectSubtype,
                        plantingArea: item.geospatialObjectName,
                        plantingAreaCode: item.geospatialObjectCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        altitude: item.altitude,
                      })
                    );
                    resolve({
                      pages: data.findGeospatialObjectList.totalPages,
                      elements: data.findGeospatialObjectList.totalElements,
                      // data: data.findGeospatialObjectList.content,
                      data: arrayList,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "field":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_PLACES_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "geospatialObjectType",
                        mod: "EQ",
                        val: "field",
                      },
                      {
                        col: "geospatialObjectName",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findGeospatialObjectList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.geospatialObjectType,
                        // geospatialObjectSubtype: item.geospatialObjectSubtype,
                        field: item.geospatialObjectName,
                        fieldCode: item.geospatialObjectCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        altitude: item.altitude,
                      })
                    );
                    resolve({
                      pages: data.findGeospatialObjectList.totalPages,
                      elements: data.findGeospatialObjectList.totalElements,
                      // data: data.findGeospatialObjectList.content,
                      data: arrayList,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "fieldCode":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_PLACES_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "geospatialObjectType",
                        mod: "EQ",
                        val: "field",
                      },
                      {
                        col: "geospatialObjectCode",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findGeospatialObjectList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.geospatialObjectType,
                        // geospatialObjectSubtype: item.geospatialObjectSubtype,
                        field: item.geospatialObjectName,
                        fieldCode: item.geospatialObjectCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        altitude: item.altitude,
                      })
                    );
                    resolve({
                      pages: data.findGeospatialObjectList.totalPages,
                      elements: data.findGeospatialObjectList.totalElements,
                      // data: data.findGeospatialObjectList.content,
                      data: arrayList,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        default:
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_PLACES_LIST,
                  variables: {
                    page: page,
                    filters: [
                      ...filters,
                      // {
                      //   col: "updatedOn",
                      //   mod: "NOTNULL",
                      //   val: "",
                      // },
                      { col: "geospatialObjectType", mod: "EQ", val: "site" },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    resolve({
                      pages: data.findGeospatialObjectList.totalPages,
                      elements: data.findGeospatialObjectList.totalElements,
                      data: data.findGeospatialObjectList.content,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
      }
    }
  };

  const DetailComponent = ({ rowData }) => {
    return <FieldDetailMolecule rowData={rowData} />;
  };

  return (
    /* 
     @prop data-testid: Id to use inside places.test.js file.
     */

    <EbsGrid
      id="Places"
      toolbar={true}
      data-testid={"PlacesTestId"}
      columns={columns}
      toolbaractions={PlaceToolbarActions}
      rowactions={PlaceRowActions}
      csvfilename="PlacesList"
      detailcomponent={DetailComponent}
      fetch={fetch}
      height="85vh"
      select="multi"
    />
  );
});
// Type and required properties
PlacesOrganism.propTypes = {};
// Default properties
PlacesOrganism.defaultProps = {};

export default PlacesOrganism;
