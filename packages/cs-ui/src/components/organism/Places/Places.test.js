import Places from './Places';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Places is in the DOM', () => {
  render(<Places></Places>)
  expect(screen.getByTestId('PlacesTestId')).toBeInTheDocument();
})
