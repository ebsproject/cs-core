import React, { useState, memo, useEffect } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { EbsGrid } from "@ebs/components";
import {
  FIND_HIERARCHY_LIST_ROLES,
  FIND_ORG_UNITS_LIST,
} from "utils/apollo/gql/crm";
import { useDispatch } from "react-redux";
import { client, clientSM } from "utils/apollo";
import { findServiceByIdOrgUnit } from "store/modules/SM";
import { FIND_SERVICE_PROVIDER } from "utils/apollo/gql/sm";
const {
  Box,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Button,
  Grid,
  CircularProgress,
  Chip,
} = Core;
const OrganizationUnitView = React.forwardRef(
  ({ dataUnit, open, handleClose, serviceP }, ref) => {
    const cropsData = dataUnit?.institution?.crops;
    const workflowsData = dataUnit?.workflows;

    const columnsServices = [
      { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value?.fullName}</Typography>;
        },
        accessor: "contact.person",
        width: 440,
        Filter: false,
        Header: (
          <Typography variant="h6" color="secondary">
            <FormattedMessage id="none" defaultMessage="Contact" />
          </Typography>
        ),
        disableGlobalFilter: true,
      },
      {
        Cell: ({ value }) => {
          return <Typography variant="body1">{value}</Typography>;
        },
        accessor: "roles[0].name",
        width: 440,
        Filter: false,
        Header: (
          <Typography variant="h6" color="secondary">
            <FormattedMessage id="none" defaultMessage="Role" />
          </Typography>
        ),
      },
    ];
    let UnitLead = dataUnit?.members;
    UnitLead?.forEach((element) => {
      element.roles.forEach((item) => {
        if (
          item.name === "Unit Administrative Contact" ||
          item.name === "Unit Leader"
        ) {
          element.namePerson =
            element.contact.person.givenName +
            " " +
            element.contact.person.familyName;
          element.nameRol = item.name;
        } else {
          element.remove = false;
        }
      });
    });
    const unitAdminData = UnitLead?.filter(
      (item) => item.nameRol === "Unit Administrative Contact"
    );
    const unitLeadData = UnitLead?.filter(
      (item) => item.nameRol === "Unit Leader"
    );

    const ComponentCrop = () => {
      return cropsData?.map((item, index) => (
        <Chip key={index} label={item.name} variant="outlined" />
      ));
    };
    const ComponentWorkflow = () => {
      return workflowsData?.map((item, index) => (
        <Chip key={index} label={item.workflow.name} variant="outlined" />
      ));
    };
    const fetchServices = async ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        try {
          client
            .query({
              query: FIND_HIERARCHY_LIST_ROLES,
              variables: {
                // sort: sort,
                page: page,
                filters: [
                  ...filters,
                  { col: "contact.category.id", mod: "EQ", val: "1" },
                  { col: "institution.id", mod: "EQ", val: dataUnit?.id },
                ],
                disjunctionFilters: false,
              },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                resolve({
                  elements: data.findHierarchyList.totalElements,
                  data: data.findHierarchyList.content,
                  pages: data.findHierarchyList.totalPages,
                });
              }
            });
        } catch (error) {}
      });
    };
    return (
      <Box>
        <Dialog
          onClose={handleClose}
          aria-labelledby="customized-dialog-title"
          open={open}
          maxWidth="md"
          fullWidth
        >
          <DialogTitle id="customized-dialog-title" onClose={handleClose}>
            {dataUnit?.institution?.unitType?.name !== undefined
              ? dataUnit?.institution?.unitType?.name + " " + "Information"
              : "Information"}
          </DialogTitle>
          {/* Basic Information */}
          <DialogContent dividers>
            <Typography variant="h6" className="mb-5 " color="primary">
              <FormattedMessage id="none" defaultMessage="Basic Information" />
            </Typography>
            <Grid container spacing={1}>
              <Grid item xs={2}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg"
                  color="secondary"
                >
                  <FormattedMessage id="none" defaultMessage="Organization" />
                </Typography>
                <Typography variant="subtitle2" className="mb-5">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      dataUnit?.parents[0]?.institution?.institution
                        ?.commonName !== undefined
                        ? dataUnit?.parents[0]?.institution?.institution
                            ?.commonName
                        : ""
                    }
                  />
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg"
                  color="secondary"
                >
                  <FormattedMessage id="none" defaultMessage="Unit Name" />
                </Typography>
                <Typography variant="subtitle2" className="mb-5">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      dataUnit?.institution?.legalName !== undefined
                        ? dataUnit?.institution?.legalName
                        : ""
                    }
                  />
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg "
                  color="secondary"
                >
                  <FormattedMessage
                    id="none"
                    defaultMessage="Unit Abbreviation"
                  />
                </Typography>
                <Typography variant="subtitle2" className="mb-5">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      dataUnit?.institution?.commonName !== undefined
                        ? dataUnit?.institution?.commonName
                        : ""
                    }
                  />
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg"
                  color="secondary"
                >
                  <FormattedMessage id="none" defaultMessage="Unit Type" />
                </Typography>
                <Typography variant="subtitle2" className="mb-5">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      dataUnit?.institution?.unitType?.name !== undefined
                        ? dataUnit?.institution?.unitType?.name
                        : " "
                    }
                  />
                </Typography>
              </Grid>
              <Grid item xs={2}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg"
                  color="secondary"
                >
                  <FormattedMessage id="none" defaultMessage="Crop" />
                </Typography>
                <ComponentCrop />
              </Grid>
              <Grid item xs={2}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg"
                  color="secondary"
                >
                  <FormattedMessage id="none" defaultMessage="Address" />
                </Typography>
                <Typography variant="subtitle2" className="mb-5">
                  <FormattedMessage
                    id="none"
                    defaultMessage={
                      dataUnit?.addresses?.[0]?.fullAddress !== undefined
                        ? dataUnit?.addresses?.[0]?.fullAddress +
                          ", " +
                          dataUnit?.addresses?.[0]?.country?.name
                        : " "
                    }
                  />
                </Typography>
              </Grid>
            </Grid>
          </DialogContent>
          <DialogContent dividers>
            <Typography variant="h6" className="mb-5 " color="primary">
              <FormattedMessage
                id="none"
                defaultMessage="Service to Provider"
              />
            </Typography>
            <Grid container spacing={1}>
              {serviceP?.servicetypes?.[0]?.name === undefined ? null : (
                <Grid item xs={3}>
                  <Typography
                    variant="h6"
                    className="mb-5 text-lg"
                    color="secondary"
                  >
                    <FormattedMessage id="none" defaultMessage="Service Type" />
                  </Typography>
                  <Typography variant="subtitle2" className="mb-5">
                    <FormattedMessage
                      id="none"
                      defaultMessage={
                        serviceP?.servicetypes?.[0]?.name !== undefined
                          ? serviceP?.servicetypes?.[0]?.name
                          : ""
                      }
                    />
                  </Typography>
                </Grid>
              )}
              {serviceP?.ebsForms?.[0]?.name === undefined ? null : (
                <Grid item xs={3}>
                  <Typography
                    variant="h6"
                    className="mb-5 text-lg"
                    color="secondary"
                  >
                    <FormattedMessage
                      id="none"
                      defaultMessage="Forms Available"
                    />
                  </Typography>
                  <Typography variant="subtitle2" className="mb-5">
                    <FormattedMessage
                      id="none"
                      defaultMessage={
                        serviceP?.ebsForms?.[0]?.name !== undefined
                          ? serviceP?.ebsForms?.[0]?.name
                          : ""
                      }
                    />
                  </Typography>
                </Grid>
              )}
              <Grid item xs={3}>
                <Typography
                  variant="h6"
                  className="mb-5 text-lg"
                  color="secondary"
                >
                  <FormattedMessage
                    id="none"
                    defaultMessage="Workflow Services"
                  />
                </Typography>
                <ComponentWorkflow />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogContent dividers>
            <Typography variant="h6" className="mb-5 " color="primary">
              <FormattedMessage id="none" defaultMessage="Members" />
            </Typography>
            <EbsGrid
              id="ServicesWorkflowf"
              toolbar={false}
              data-testid={"ServicesWorkflowf"}
              columns={columnsServices}
              csvfilename="ServicesWorkflowf"
              fetch={fetchServices}
              height="100vh"
              disabledViewDownloadCSV
              disabledViewPrintOunt
              disabledViewConfiguration
              defaultSort
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={handleClose}
              className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
            >
              Back
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    );
  }
);
OrganizationUnitView.propTypes = {};
OrganizationUnitView.defaultProps = {};

export default memo(OrganizationUnitView);
