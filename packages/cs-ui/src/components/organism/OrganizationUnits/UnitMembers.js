import React, { memo, useEffect, useState } from "react";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
const { Typography, IconButton, TextField, Grid, Paper, Button, Box } = Core;
import { useSelector, useDispatch } from "react-redux";
const { Delete, AddCircle } = Icons;
const { Autocomplete } = Lab;
import { EbsGrid } from "@ebs/components";
import {
  FIND_CONTACT_LIST_OU,
  FIND_HIERARCHY_LIST_ROLES,
  FIND_ORG_UNITS_ID,
  FIND_ORG_UNITS_LIST,
} from "utils/apollo/gql/crm";
import { client } from "utils/apollo";
import {
  createHierarchyUM,
  deleteHierarchyRole,
  modifyOrganizationUnit,
  updateOrganizationbyId,
} from "./fuctionsOrganizations";
import RefreshServices from "./RefreshServices";
import { setDefaultValues } from "store/modules/OrganizationUnit";
import { clientRest } from "utils/axios/axios";

const UnitMembers = React.forwardRef((props, ref) => {
  const {
    method,
    rolesUser,
    defaultAddreses,
    process,
    dataOUList,
    userId,
    contactId,
    percentage,
    reloading,
    dataUser,
    reloadingRol,
    percentageRol,
    optionInstitution,
    institutionData,
    ...rest
  } = props;
  const [rol, setRol] = useState(null);
  const [contact, setContact] = useState(null);
  const [render, setRender] = useState(0);
  const [update, setUpdate] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const [click, setClick] = useState(false);
  const { defaultValues } = useSelector(({ orgUnit }) => orgUnit);
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      if (process === "OrganizationUnit") {
        if (click === true && contact !== null) {
          // User synchronization with CB
          const { errors: errorsSync } = await clientRest.get(
            `/sync/user/${Number(contact.id)}`
          );
          if (errorsSync) {
            throw new Error(`${errorsSync[0].message}`);
          }
        }
      } else {
        if (click) {
          // User synchronization with CB
          const { errors: errorsSync } = await clientRest.get(
            `/sync/user/${Number(userId)}`
          );
          if (errorsSync) {
            throw new Error(`${errorsSync[0].message}`);
          }
        }
        setClick(true);
      }
      setClick(false);
    };
    fetchData();
  }, [click, contact]);

  useEffect(() => {
    const updateData = async () => {
      await updateOrganizationbyId({ id: defaultValues?.id }, dispatch);
    };
    process !== "userManagement" ? updateData() : null;
  }, [update, contact, disabledButton]);

  useEffect(() => {
    if (contact !== null && rol !== null) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
  }, [rol, contact, disabledButton]);

  const columnsServices = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "contact.person.fullName",
      width: 270,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Contact" />
        </Typography>
      ),
      filter: true,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "roles[0].name",
      width: 270,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Role" />
        </Typography>
      ),
    },
    {
      Cell: ({ value }) => {
        const data = value.filter(
          (item) => item?.institution?.category?.name === "Institution"
        );
        const dataInstitution = data.map((dataItem) => {
          return dataItem?.institution?.institution?.legalName;
        });
        return (
          <Typography variant="body1">{dataInstitution.toString()}</Typography>
        );
      },
      accessor: "contact.parents",
      width: 270,
      Filter: false,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Institution" />
        </Typography>
      ),
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "contact.users[0].userName",
      width: 270,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Email" />
        </Typography>
      ),
    },
    {
      Header: "contact.users[0].id",
      accessor: "contact.users[0].id",
      hidden: true,
    },
  ];

  const columnsServicesUm = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Unit" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.institution.legalName",
      width: 500,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Abbreviation" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.institution.commonName",
      width: 300,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Role" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "roles[0].name",
      width: 300,
      filter: true,
    },
  ];

  const optionLabel = (option) =>
    `${option?.contact?.person?.fullName + " " + "(" + option?.userName + ")"}`;
  const optionLabelOU = (option) =>
    `${
      option?.institution?.legalName +
      " " +
      "(" +
      option?.institution?.commonName +
      ")"
    }`;
  const optionLabelRole = (option) => `${option?.name}`;

  const fetchServices = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        filters.map((item) => {
          if (item.col === "roles[0].name") {
            item.col = "roles.name";
          }
          if (item.col === "contact.users[0].userName") {
            item.col = "contact.users.userName";
          }
        });
        client
          .query({
            query: FIND_HIERARCHY_LIST_ROLES,
            variables: {
              // sort: sort,
              page: page,
              filters: [
                ...filters,
                { col: "contact.category.id", mod: "EQ", val: "1" },
                { col: "institution.id", mod: "EQ", val: defaultValues.id },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              let memberDefaul = data.findHierarchyList.content;
              memberDefaul.forEach((element) => {
                if (element.roles.length === 0) {
                  element.remove = false;
                }
                element.roles.forEach((item) => {
                  if (
                    item.name === "Unit Administrative Contact" ||
                    item.name === "Unit Leader"
                  ) {
                    element.remove = true;
                  } else {
                    element.remove = false;
                  }
                });
              });
              resolve({
                elements: data.findHierarchyList.totalElements,
                data: memberDefaul,
                pages: data.findHierarchyList.totalPages,
              });
            }
          });
      } catch (error) {}
    });
  };

  const fetchServicesUser = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        filters.map((item) => {
          if (item.col === "roles[0].name") {
            item.col = "roles.name";
          }
        });
        client
          .query({
            query: FIND_HIERARCHY_LIST_ROLES,
            variables: {
              // sort: sort,
              page: page,
              filters: [
                ...filters,
                { col: "contact.users.id", mod: "EQ", val: userId },
                { col: "institution.category.id", mod: "EQ", val: "4" },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              let memberDefaul = data.findHierarchyList.content;
              memberDefaul.forEach((element) => {
                if (element.roles.length === 0) {
                  element.remove = false;
                }
                element.roles.forEach((item) => {
                  if (
                    item.name === "Unit Administrative Contact" ||
                    item.name === "Unit Leader"
                  ) {
                    element.remove = true;
                  } else {
                    element.remove = false;
                  }
                });
              });
              resolve({
                elements: data.findHierarchyList.totalElements,
                data: memberDefaul,
                pages: data.findHierarchyList.totalPages,
              });
            }
          });
      } catch (error) {}
    });
  };

  const onSubmitService = async () => {
    switch (process) {
      case "OrganizationUnit":
        if (contact !== null && rol !== null) {
          let memberArray = defaultValues.members.map((item) => ({
            memberId: Number(item.contact.id),
            roleIds:
              item?.roles[0]?.id === undefined
                ? [4]
                : [Number(item?.roles[0]?.id)],
          }));
          let createNewMember = {
            memberId: Number(contact.contact.id),
            roleIds: [Number(rol.id)],
          };
          memberArray.push(createNewMember);

          let dataUnit = {
            contact: {
              id: Number(defaultValues.id),
              category: { id: defaultValues.category.id },
              institution: {
                legalName: defaultValues.institution.legalName,
                commonName: defaultValues.institution.commonName,
                cgiar: false,
                unitTypeId: defaultValues.institution.unitType.id,
                cropIds: defaultValues.institution.crops.map((item) =>
                  Number(item.id)
                ),
              },
              addresses:
                defaultValues?.addresses.length === 0 ? [] : defaultAddreses,
            },
            parentOrganizationId: defaultValues.parents[0].institution.id,
            unitMembers: memberArray,
            workflowIds: defaultValues.workflows.map(
              (item) => item.workflow.id
            ),
          };
          await modifyOrganizationUnit(
            {
              modifydataUnit: dataUnit,
              type: "MemberEdit",
              dataContact: contact,
              rol: rol,
              action: "add",
            },
            dispatch,
            setClick,
            setUpdate,
            setContact,
            update
          );
        }
        setRender((prev) => prev + 1);
        setRol(null);
        setDisabledButton(true);
        break;
      case "userManagement":
        if (contact !== null && rol !== null) {
          const data = {
            contactId: contactId,
            institutionId: contact.id,
            principalContact: false,
          };

          await createHierarchyUM(
            { createHierarchy: data, idRol: rol.id },
            dispatch,
            setUpdate,
            setClick,
            update
          );
        }
        setRender((prev) => prev + 1);
        setRol(null);
        setContact(null);
        break;
      default:
        null;
    }
  };

  const refreshMemberGrid = (selection, refresh) => {
    return (
      <div>
        <RefreshServices
          refresh={refresh}
          refreshGrid={update}
          setRefreshGrid={setUpdate}
        />
      </div>
    );
  };
  const dataOuFilter = () => {
    if (optionInstitution.length > 0) {
      const institution = optionInstitution.map((item) => item.name);
      let data = dataOUList.filter((filter) =>
        institution.includes(
          filter?.parents?.[0]?.institution?.institution?.commonName
        )
      );
      return data;
    } else {
      return dataOUList;
    }
  };

  const rowActionsMembers = (rowData, refresh) => {
    const handleGoTo = async () => {
      switch (process) {
        case "OrganizationUnit":
          const membersDelete = defaultValues.members.filter(
            (item) => item.id !== rowData.id
          );

          let memberArray = membersDelete.map((item) => ({
            memberId: Number(item.contact.id),
            roleIds: [Number(item.roles[0].id)],
          }));

          let dataUnit = {
            contact: {
              id: Number(defaultValues.id),
              category: { id: defaultValues.category.id },
              institution: {
                legalName: defaultValues.institution.legalName,
                commonName: defaultValues.institution.commonName,
                cgiar: false,
                unitTypeId: defaultValues.institution.unitType.id,
                cropIds: defaultValues.institution.crops.map((item) =>
                  Number(item.id)
                ),
              },
              addresses: defaultAddreses,
            },
            parentOrganizationId: defaultValues.parents[0].institution.id,
            unitMembers: memberArray,
            workflowIds: defaultValues.workflows.map(
              (item) => item.workflow.id
            ),
          };

          await modifyOrganizationUnit(
            {
              modifydataUnit: dataUnit,
              type: "MemberDelete",
              dataDelete: rowData,
              action: "delete",
            },
            dispatch,
            setUpdate,
            update
          );
          setTimeout(() => {
            refresh();
          }, 300);
          break;
        case "userManagement":
          const contactId = rowData.contact.id;
          const institutionId = rowData.institution.id;

          await deleteHierarchyRole(
            {
              contactId: contactId,
              institutionId: institutionId,
              userId: rowData.contact.users[0].id,
            },
            dispatch
          );
          refresh();
          break;
        default:
          null;
      }
    };
    return (
      <div>
        <Box component="div" ref={ref} className="-ml-2 flex flex-auto">
          <IconButton
            size="small"
            onClick={handleGoTo}
            color="primary"
            className="text-green-600"
            disabled={rowData.remove === true}
          >
            <Delete />
          </IconButton>
        </Box>
      </div>
    );
  };

  const rolesUserFilter =
    process !== "userManagement"
      ? rolesUser?.filter(
          (item) =>
            item.name !== "Unit Leader" &&
            item.name !== "Unit Administrative Contact"
        )
      : rolesUser;

  const filterDataUser = () => {
    if (institutionData !== null) {
      const arrayOptions = dataUser.filter((item) =>
        item.contact.parents.some(
          (parent) =>
            parent.institution.institution.commonName === institutionData
        )
      );
      return arrayOptions;
    } else {
      return dataUser;
    }
  };

  return (
    <div>
      <br />
      <Typography variant="h6">
        <FormattedMessage id="none" defaultMessage="Members" />
      </Typography>
      <br />
      <Paper style={{ padding: 20 }} elevation={3} square>
        <Grid container direction="row" spacing={3}>
          {process !== "userManagement" ? (
            <Grid item xs={12} md={8} sm={8} lg={4} xl={5}>
              <Autocomplete
                key={render}
                size="small"
                id={"name"}
                options={filterDataUser() || []}
                onChange={(e, option) => {
                  setContact(option);
                }}
                getOptionLabel={optionLabel}
                // inputValue={{}}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                    label={
                      <Typography variant="h6">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={
                            reloading
                              ? `Loading Contacts, ${percentage}% completed`
                              : "Contacts"
                          }
                        />
                      </Typography>
                    }
                  />
                )}
                fullWidth
              />
            </Grid>
          ) : (
            <Grid item xs={12} md={8} sm={8} lg={4} xl={5}>
              <Autocomplete
                key={render}
                size="small"
                id={"name"}
                options={dataOuFilter() || []}
                onChange={(e, option) => {
                  setContact(option);
                }}
                getOptionLabel={optionLabelOU}
                // inputValue={{}}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                    label={
                      <Typography variant="h6">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={
                            reloading
                              ? `Loading Units, ${percentage}% completed`
                              : "Units"
                          }
                        />
                      </Typography>
                    }
                  />
                )}
                fullWidth
              />
            </Grid>
          )}
          <Grid item xs={12} md={8} sm={8} lg={4} xl={5}>
            <Autocomplete
              key={render}
              size="small"
              id={"name"}
              options={rolesUserFilter || []}
              onChange={(e, option) => {
                setRol(option);
              }}
              // inputValue={{}}
              getOptionLabel={optionLabelRole}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                  label={
                    <Typography variant="h6">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={
                          reloadingRol
                            ? `Loading Roles, ${percentageRol}% completed`
                            : "Roles"
                        }
                      />
                    </Typography>
                  }
                />
              )}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={2} xl={2}>
            <Button
              variant="contained"
              className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white ml-3"
              startIcon={<AddCircle className="fill-current text-white" />}
              onClick={() => onSubmitService()}
              disabled={disabledButton}
            >
              <Typography className="text-white text-sm font-ebs">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    process === "OrganizationUnit"
                      ? "Add new member"
                      : "Add new role in Organization"
                  }
                />
              </Typography>
            </Button>
          </Grid>
        </Grid>

        <EbsGrid
          id="MembersUnit"
          toolbar={true}
          data-testid={"MembersUnit"}
          rowactions={rowActionsMembers}
          toolbaractions={refreshMemberGrid}
          columns={
            process !== "userManagement" ? columnsServices : columnsServicesUm
          }
          csvfilename="MembersUnit"
          fetch={
            process !== "userManagement" ? fetchServices : fetchServicesUser
          }
          height="150vh"
          disabledViewDownloadCSV
          disabledViewPrintOunt
          disabledViewConfiguration
          defaultSort
          select="multi"
        />
      </Paper>
      <br />
    </div>
  );
});

export default memo(UnitMembers);
