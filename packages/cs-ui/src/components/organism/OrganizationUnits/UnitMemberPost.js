import React, { memo, useEffect, useState, useRef } from "react";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
const { Typography, IconButton, TextField, Grid, Paper, Button, Box } = Core;
import { useSelector, useDispatch } from "react-redux";
const { Delete, AddCircle } = Icons;
const { Autocomplete } = Lab;
import { EbsGrid } from "@ebs/components";
import RefreshServices from "./RefreshServices";

const UnitMembersPost = React.forwardRef((props, ref) => {
  const {
    dataOUList,
    rolesUser,
    process,
    percentage,
    reloading,
    reloadingRol,
    percentageRol,
    optionInstitution,
    setMemberOu,
    institutionData,
    dataUser,
    setMemberUser,
    ...rest
  } = props;
  const [rol, setRol] = useState(null);
  const [contact, setContact] = useState(null);
  const [render, setRender] = useState(0);
  const [update, setUpdate] = useState(false);
  const [disabledButton, setDisabledButton] = useState(true);
  const [arrayOuforUser, setArrayOuforUser] = useState([]);
  const [arrayUserforOu, setArrayUserforOu] = useState([]);
  const arrayOuforUserRef = useRef(arrayOuforUser);
  const arrayUserforOuRef = useRef(arrayUserforOu);

  useEffect(() => {
    if (contact !== null && rol !== null) {
      setDisabledButton(false);
    } else {
      setDisabledButton(true);
    }
  }, [rol, contact, disabledButton]);

  //Control to update the status of members for User Management
  useEffect(() => {
    if (process === "userManagement") {
      arrayOuforUserRef.current = arrayOuforUser;
    } else {
      arrayUserforOuRef.current = arrayUserforOu;
    }
  }, [arrayOuforUser, arrayUserforOu]);

  //Fuction to update Egs-Grid
  const refreshMemberGrid = (selection, refresh) => {
    return (
      <div>
        <RefreshServices
          refresh={refresh}
          refreshGrid={update}
          setRefreshGrid={setUpdate}
        />
      </div>
    );
  };

  //Function to filter by institution
  const dataOuFilterUm = () => {
    if (optionInstitution !== null) {
      const institution = optionInstitution.map((item) => item.name);
      let data = dataOUList.filter((filter) =>
        institution.includes(
          filter?.parents?.[0]?.institution?.institution?.commonName
        )
      );
      return data;
    } else {
      return dataOUList;
    }
  };

  //Fucion to filter by User
  const filterDataUser = () => {
    if (institutionData !== null) {
      const arrayOptions = dataUser.filter((item) =>
        item.contact.parents.some(
          (parent) =>
            parent.institution.institution.commonName === institutionData
        )
      );
      return arrayOptions;
    } else {
      return dataUser;
    }
  };

  //Columns to the members in User Management
  const columnsServicesUm = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Unit" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.institution.legalName",
      width: 450,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Abbreviation" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.institution.commonName",
      width: 300,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Role" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "rol.name",
      width: 300,
      filter: true,
    },
  ];

  //Columns to the members in Organization Unit
  const columnsServicesOu = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "user.contact.person.fullName",
      width: 270,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="User" />
        </Typography>
      ),
      filter: true,
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "rol.name",
      width: 270,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Role" />
        </Typography>
      ),
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "user.contact.parents[0].institution.institution.commonName",
      width: 270,
      Filter: false,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Institution" />
        </Typography>
      ),
    },
    {
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "user.userName",
      width: 270,
      Header: (
        <Typography variant="h6" color="primary">
          <FormattedMessage id="none" defaultMessage="Email" />
        </Typography>
      ),
    },
  ];

  //Fuction onSubmit
  const onSubmitService = async () => {
    switch (process) {
      case "OrganizationUnit":
        if (contact !== null && rol !== null) {
          const data = {
            user: contact,
            rol: rol,
          };
          setArrayUserforOu((prevArray) => {
            const dataEqual = prevArray.filter(
              (item) =>
                Number(item.user.id) === Number(contact.id) &&
                Number(item.rol.id) === Number(rol.id)
            );
            return dataEqual.length !== 0
              ? [...prevArray]
              : [...prevArray, data];
          });
        }
        setUpdate(true);
        setRender((prev) => prev + 1);
        setRol(null);
        setContact(null);
        break;
      case "userManagement":
        if (contact !== null && rol !== null) {
          const data = {
            institution: contact,
            rol: rol,
          };
          setArrayOuforUser((prevArray) => {
            const dataEqual = prevArray.filter(
              (item) =>
                Number(item.institution.id) === Number(contact.id) &&
                Number(item.rol.id) === Number(rol.id)
            );
            return dataEqual.length !== 0
              ? [...prevArray]
              : [...prevArray, data];
          });
        }
        setUpdate(true);
        setRender((prev) => prev + 1);
        setRol(null);
        setContact(null);
        break;
      default:
        null;
    }
  };

  //Fuction Fetch to User Management
  const fetchServicesUserUm = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        setMemberOu(arrayOuforUserRef.current);
        resolve({
          elements: arrayOuforUser.length,
          data: arrayOuforUserRef.current,
          pages: 1,
        });
      } catch (error) {}
    });
  };

  //Fuction Fetch to Organization Unit
  const fetchServicesOu = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        setMemberUser(arrayUserforOuRef.current);
        resolve({
          elements: arrayUserforOu.length,
          data: arrayUserforOuRef.current,
          pages: 1,
        });
      } catch (error) {}
    });
  };

  //Fuction to rowAction to EbsGrid
  const rowActionsMembers = (rowData, refresh) => {
    const handleDelete = (selectedItem) => {
      if (process === "userManagement") {
        setArrayOuforUser((prevArray) =>
          prevArray.filter((item) => item !== selectedItem)
        );
        setUpdate(true);
      } else {
        setArrayUserforOu((prevArray) =>
          prevArray.filter((item) => item !== selectedItem)
        );
        setUpdate(true);
      }
    };

    return (
      <div>
        <Box component="div" ref={ref} className="-ml-2 flex flex-auto">
          <IconButton
            size="small"
            onClick={() => handleDelete(rowData)}
            color="primary"
            className="text-green-600"
            disabled={rowData.remove === true}
          >
            <Delete />
          </IconButton>
        </Box>
      </div>
    );
  };

  const rolesUserFilter =
    process !== "userManagement"
      ? rolesUser?.filter(
          (item) =>
            item.name !== "Unit Leader" &&
            item.name !== "Unit Administrative Contact"
        )
      : rolesUser;

  const optionLabelOU = (option) =>
    `${
      option?.institution?.legalName +
      " " +
      "(" +
      option?.institution?.commonName +
      ")"
    }`;

  const optionLabelRole = (option) => `${option?.name}`;
  const optionLabel = (option) =>
    `${option?.contact?.person?.fullName + " " + "(" + option?.userName + ")"}`;

  return (
    <div>
      <br />
      <Typography variant="h6">
        <FormattedMessage id="none" defaultMessage="Members" />
      </Typography>
      <br />
      <Paper style={{ padding: 20 }} elevation={3} square>
        <Grid container direction="row" spacing={3}>
          {process !== "userManagement" ? (
            <Grid item xs={12} md={8} sm={8} lg={4} xl={5}>
              <Autocomplete
                key={render}
                size="small"
                id={"name"}
                options={filterDataUser() || []}
                onChange={(e, option) => {
                  setContact(option);
                }}
                getOptionLabel={optionLabel}
                // inputValue={{}}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                    label={
                      <Typography variant="h6">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={
                            reloading
                              ? `Loading Contacts, ${percentage}% completed`
                              : "Contacts"
                          }
                        />
                      </Typography>
                    }
                  />
                )}
                fullWidth
              />
            </Grid>
          ) : (
            <Grid item xs={12} md={8} sm={8} lg={4} xl={5}>
              <Autocomplete
                key={render}
                size="small"
                id={"name"}
                options={dataOuFilterUm() || []}
                onChange={(e, option) => {
                  setContact(option);
                }}
                getOptionLabel={optionLabelOU}
                // inputValue={{}}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                    label={
                      <Typography variant="h6">
                        <FormattedMessage
                          id={"none"}
                          defaultMessage={
                            reloading
                              ? `Loading Units, ${percentage}% completed`
                              : "Units"
                          }
                        />
                      </Typography>
                    }
                  />
                )}
                fullWidth
              />
            </Grid>
          )}
          <Grid item xs={12} md={8} sm={8} lg={4} xl={5}>
            <Autocomplete
              key={render}
              size="small"
              id={"name"}
              options={rolesUserFilter || []}
              onChange={(e, option) => {
                setRol(option);
              }}
              // inputValue={{}}
              getOptionLabel={optionLabelRole}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="outlined"
                  InputLabelProps={{ shrink: true }}
                  label={
                    <Typography variant="h6">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={
                          reloadingRol
                            ? `Loading Roles, ${percentageRol}% completed`
                            : "Roles"
                        }
                      />
                    </Typography>
                  }
                />
              )}
              fullWidth
            />
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={2} xl={2}>
            <Button
              variant="contained"
              className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white ml-3"
              startIcon={<AddCircle className="fill-current text-white" />}
              onClick={() => onSubmitService()}
              disabled={disabledButton}
            >
              <Typography className="text-white text-sm font-ebs">
                <FormattedMessage
                  id="none"
                  defaultMessage={
                    process === "OrganizationUnit"
                      ? "Add new member"
                      : "Add new role in Organization"
                  }
                />
              </Typography>
            </Button>
          </Grid>
        </Grid>

        <EbsGrid
          id="MembersUnitPost"
          toolbar={true}
          data-testid={"MembersUnitPost"}
          rowactions={rowActionsMembers}
          toolbaractions={refreshMemberGrid}
          columns={
            process === "userManagement" ? columnsServicesUm : columnsServicesOu
          }
          csvfilename="MembersUnitPost"
          fetch={
            process === "userManagement" ? fetchServicesUserUm : fetchServicesOu
          }
          height="150vh"
          disabledViewDownloadCSV
          disabledViewPrintOunt
          disabledViewConfiguration
          defaultSort
          select="multi"
        />
      </Paper>
      <br />
    </div>
  );
});
export default memo(UnitMembersPost);
