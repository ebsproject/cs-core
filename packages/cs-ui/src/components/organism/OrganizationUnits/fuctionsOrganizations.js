import { setDefaultValues } from "store/modules/OrganizationUnit";
import { showMessage } from "store/modules/message";
import { client } from "utils/apollo";
import {
  CREATE_HIERARCHY,
  CREATE_ORGANIZATION_UNIT,
  CREATE_ROLE_CONTACT_HIERARCHY,
  DELETE_ADDRESS,
  DELETE_HIERARCHY,
  DELETE_ORGANIZATION_UNIT,
  FIND_CONTACT_LIST,
  FIND_CONTACT_LIST_OU,
  FIND_CROP_LIST,
  FIND_ORG_UNITS_ID,
  FIND_ORG_UNITS_LIST,
  FIND_USER_LIST_OU,
  MODIFY_ORGANIZATION_UNIT,
} from "utils/apollo/gql/crm";
import {
  CREATE_CONTACT_WORKFLOW,
  DELETE_CONTACT_WORKFLOW,
  FIND_WORKFLOW_LIST,
} from "utils/apollo/gql/hierarchy";

import { FIND_ROLE_LIST } from "utils/apollo/gql/userManagement";
import { clientRest } from "utils/axios/axios";

export async function getDataUser(setDataUser, setReloading, setTotal) {
  setReloading(true);
  let number = 1;
  const { data } = await client.query({
    query: FIND_USER_LIST_OU,
    variables: {
      page: { number: number, size: 100 },
      filters: [],
      disjuncionFilters: true,
    },
    fetchPolicy: "no-cache",
  });
  number = data?.findUserList?.totalPages;
  setTotal(data?.findUserList?.totalElements);

  setDataUser((d) => [...d, ...data.findUserList.content]);
  if (number > 1) {
    for (let i = 1; i < number; i++) {
      const { data } = await client.query({
        query: FIND_USER_LIST_OU,
        variables: {
          page: { number: i + 1, size: 100 },
          filters: [],
          disjuncionFilters: true,
        },
        fetchPolicy: "no-cache",
      });
      setDataUser((d) => [...d, ...data.findUserList.content]);
    }
  }
  setReloading(false);
}

export async function getDataWorkflows(setDataW) {
  /// review
  try {
    const { data } = await client.query({
      query: FIND_WORKFLOW_LIST,
      variables: {
        filters: [],
      },
      fetchPolicy: "no-cache",
    });
    setDataW(data);
  } catch (error) {
    console.log(error);
  }
}

export const createContactWorkflow = (
  { idContact, idWorkflow, setRefreshGrid },
  dispatch
) => {
  const contactWorkflows = {
    id: 0,
    contactId: idContact,
    workflowId: idWorkflow,
    provide: false,
  };
  client
    .mutate({
      mutation: CREATE_CONTACT_WORKFLOW,
      variables: {
        input: contactWorkflows,
      },
    })
    .then((result) => {
      if (result) {
        dispatch(
          showMessage({
            message: `The relationship has been saved correctly`,
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        setRefreshGrid(true);
      }
    })
    .catch((error) => {
      // console.log(error);
      dispatch(
        showMessage({
          message:
            "The action failed, the relationship already exists, please select another option",
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};

export const deleteContactWorkflow = ({ id }, dispatch) => {
  client
    .mutate({
      mutation: DELETE_CONTACT_WORKFLOW,
      variables: {
        id: id,
      },
    })
    .then((result) => {
      if (result) {
        dispatch(
          showMessage({
            message: `The relationship was successfully deleted`,
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      }
    })
    .catch((error) => {
      dispatch(
        showMessage({
          message: "An error occurred while deleting",
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};

export const findOrganizationCgiar = async (setdataOrganizationCgiar) => {
  try {
    const { data } = await client.query({
      query: FIND_CONTACT_LIST,
      variables: {
        filters: [
          { col: "category.id", mod: "EQ", val: "2" },
          { col: "institution.isCgiar", mod: "EQ", val: "True" },
        ],
        disjunctionFilters: false,
      },
      fetchPolicy: "no-cache",
    });
    setdataOrganizationCgiar(data.findContactList.content);
  } catch (error) {
    console.log(error);
  }
};

export const findCropList = async (setDataCropList, dataInstitution) => {
  try {
    const { data } = await client.query({
      query: FIND_CROP_LIST,
      variables: {
        filters:
          dataInstitution === undefined
            ? []
            : [
                {
                  col: "institutions.commonName",
                  mod: "EQ",
                  val: dataInstitution?.toString(),
                },
              ],
        disjunctionFilters: false,
      },
      fetchPolicy: "no-cache",
    });
    setDataCropList(data?.findCropList?.content);
  } catch (error) {
    console.error(error);
  }
};

export async function findRolesUser(
  setRolesUser,
  setReloadingRol,
  setTotalRol
) {
  setReloadingRol(true);
  let number = 1;

  const { data } = await client.query({
    query: FIND_ROLE_LIST,
    variables: {
      page: { number: number, size: 100 },
      filters: [],
      disjunctionFilters: false,
    },
    fetchPolicy: "no-cache",
  });
  number = data?.findRoleList?.totalPages;
  setTotalRol(data?.findRoleList?.totalElements);

  setRolesUser((d) => [...d, ...data.findRoleList.content]);
  if (number > 1) {
    for (let i = 1; i < number; i++) {
      const { data } = await client.query({
        query: FIND_ROLE_LIST,
        variables: {
          page: { number: number, size: 100 },
          filters: [],
          disjunctionFilters: false,
        },
        fetchPolicy: "no-cache",
      });
      setRolesUser((d) => [...d, ...data.findRoleList.content]);
    }
  }
  setReloadingRol(false);
}

export async function findOU(setDataOUList, setReloading, setTotal) {
  setReloading(true);
  let number = 1;

  const { data } = await client.query({
    query: FIND_ORG_UNITS_LIST,
    variables: {
      page: { number: number, size: 15 },
      filters: [{ col: "category.name", mod: "EQ", val: "Internal Unit" }],
      disjunctionFilters: false,
    },
    fetchPolicy: "no-cache",
  });
  number = data?.findContactList?.totalPages;
  setTotal(data?.findContactList?.totalElements);
  setDataOUList((d) => [...d, ...data.findContactList.content]);
  if (number > 1) {
    for (let i = 1; i < number; i++) {
      const { data } = await client.query({
        query: FIND_ORG_UNITS_LIST,
        variables: {
          page: { number: i + 1, size: 15 },
          filters: [{ col: "category.name", mod: "EQ", val: "Internal Unit" }],
          disjunctionFilters: false,
        },
        fetchPolicy: "no-cache",
      });
      setDataOUList((d) => [...d, ...data.findContactList.content]);
    }
  }
  setReloading(false);
}

export const modifyOrganizationUnit = async (
  { modifydataUnit, type, dataContact, rol, dataDelete, action },
  dispatch,
  setUpdate,
  setClick,
  setContact,
  update
) => {
  client
    .mutate({
      mutation: MODIFY_ORGANIZATION_UNIT,
      variables: {
        input: modifydataUnit,
      },
    })
    .then(async (result) => {
      if (result) {
        action === "add" ? setClick(true) : null;
        switch (type) {
          case "OrganizationEdit":
            dispatch(
              showMessage({
                message:
                  "The Organization " +
                  modifydataUnit.contact.institution.legalName +
                  " has been updated successfully.",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            action === "add" ? setContact(null) : null;
            break;
          case "MemberEdit":
            dispatch(
              showMessage({
                message:
                  "New member " +
                  dataContact.contact.person.fullName +
                  " with a role " +
                  rol.name +
                  " has been added succesfully",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            break;
          case "MemberDelete":
            // User synchronization with CB
            const { errors: errorsSync } = await clientRest.get(
              `/sync/user/${Number(dataDelete.contact.users[0].id)}`
            );
            if (errorsSync) {
              throw new Error(`${errorsSync[0].message}`);
            }
            dispatch(
              showMessage({
                message:
                  "The contact " +
                  dataDelete.contact.person.fullName +
                  " has been removed correctly from the unit",
                variant: "success",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
            break;
          default:
            null;
        }
        if (setUpdate) setUpdate(!update);
      }
    })
    .catch((error) => {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};

export const createHierarchyUM = async (
  { createHierarchy, idRol },
  dispatch,
  setUpdate,
  setClick,
  update
) => {
  client
    .mutate({
      mutation: CREATE_HIERARCHY,
      variables: {
        hierarchy: createHierarchy,
      },
    })
    .then((result) => {
      if (result) {
        client
          .mutate({
            mutation: CREATE_ROLE_CONTACT_HIERARCHY,
            variables: {
              roleId: idRol,
              contactHierarchyId: result.data.createHierarchy.id,
            },
          })
          .then((result) => {
            setClick(result.data.createRoleContactHierarchy);
            if (result) {
              dispatch(
                showMessage({
                  message: `The unit has been updated successfully.`,
                  variant: "success",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
              if (setUpdate) setUpdate(!update);
            }
          })
          .catch((error) => {
            dispatch(
              showMessage({
                message: error.toString(),
                variant: "error",
                anchorOrigin: {
                  vertical: "top",
                  horizontal: "right",
                },
              })
            );
          });
      }
    })
    .catch((error) => {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
};

export const createHierarchyUMPost = async ({ createHierarchy, idRol }) => {
  client
    .mutate({
      mutation: CREATE_HIERARCHY,
      variables: {
        hierarchy: createHierarchy,
      },
    })
    .then((result) => {
      if (result) {
        client.mutate({
          mutation: CREATE_ROLE_CONTACT_HIERARCHY,
          variables: {
            roleId: idRol,
            contactHierarchyId: result.data.createHierarchy.id,
          },
        });
      }
    })
    .catch((error) => {
      console.error(error);
    });
};

export const createOrganizationUnit = async (createdataUnitd) =>
  new Promise(async (resolve, reject) => {
    try {
      client
        .mutate({
          mutation: CREATE_ORGANIZATION_UNIT,
          variables: {
            input: createdataUnitd,
          },
        })
        .then((result) => {
          resolve(result.data.createOrganizationalUnit.id);
        })
        .catch((e) => {
          reject(e);
        });
    } catch (error) {
      console.log(error);
    }
  });

export const updateOrganizationbyId = async ({ id }, dispatch) => {
  try {
    const { data } = await client.query({
      query: FIND_ORG_UNITS_ID,
      variables: {
        id: Number(id),
      },
      fetchPolicy: "no-cache",
    });
    dispatch(setDefaultValues(data.findContact));
  } catch (error) {
    console.log(error);
  }
};

export const deleteAddress = async ({ id }, dispatch) => {
  try {
    const { data, errors } = await client.mutate({
      mutation: DELETE_ADDRESS,
      variables: {
        addressId: id,
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};

export const deleteOrganizationalUnit = async (
  { id, name, type, refresh },
  dispatch
) => {
  try {
    const { errors } = await client.mutate({
      mutation: DELETE_ORGANIZATION_UNIT,
      variables: {
        id: id,
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(
      showMessage({
        message: "The Unit " + " " + name + " has been successfully deleted",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
  refresh();
};

export const deleteHierarchyRole = async (
  { contactId, institutionId, userId },
  dispatch
) => {
  try {
    const { data, errors } = await client.mutate({
      mutation: DELETE_HIERARCHY,
      variables: {
        contactId: contactId,
        institutionId: institutionId,
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    if (data.deleteHierarchy === "Hierarchy deleted");
    {
      // User synchronization with CB
      const { errors: errorsSync } = await clientRest.get(
        `/sync/user/${Number(userId)}`
      );
      if (errorsSync) {
        throw new Error(`${errorsSync[0].message}`);
      }
    }
    dispatch(
      showMessage({
        message: "Removed successfully",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
