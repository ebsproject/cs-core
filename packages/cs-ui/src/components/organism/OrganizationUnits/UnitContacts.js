import React, { Fragment, memo, useEffect, useState } from "react";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
const {
  Typography,
  IconButton,
  TextField,
  Grid,
  Paper,
  Tooltip,
  Button,
  CircularProgress,
} = Core;
import { useSelector, useDispatch } from "react-redux";
const { Delete, AddCircle, Visibility } = Icons;
const { Autocomplete } = Lab;
import { Controller } from "react-hook-form";
import { findContactOU } from "store/modules/DataPersist";
import ViewContact from "components/molecule/ContactForm/ViewContact";
import { useFormContext } from "react-hook-form";

const UnitContacts = React.forwardRef((props, ref) => {
  const {
    method,
    disabled,
    reloading,
    percentage,
    dataUser,
    total,
    institutionData,
    ...rest
  } = props;
  const {
    setValue,
    control,
    formState: { errors },
    register,
    clearErrors,
    getValues,
  } = useFormContext();
  const [loading1, setLoading1] = useState(false);
  const [loading2, setLoading2] = useState(false);
  const [dataContact, setDataContact] = useState(null);
  const [open, setOpen] = useState(false);
  const { defaultValues } = useSelector(({ orgUnit }) => orgUnit);

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    if (method === "PUT" && defaultValues) {
      const { members } = defaultValues;
      members.forEach((element) => {
        switch (element?.roles[0]?.name) {
          case "Unit Administrative Contact":
            const contactAdmin = dataUser.filter(
              (item) => item.contact.id === element.contact.id
            );
            setValue("unitContacts.UnitAdminContact", contactAdmin[0]);
            break;
          case "Unit Leader":
            const contactLeader = dataUser.filter(
              (item) => item.contact.id === element.contact.id
            );
            setValue("unitContacts.UnitLeadContact", contactLeader[0]);
            break;
          default:
            "";
        }
      });
    }
  }, [defaultValues, method, dataUser]);

  let dataUnitAdminContact = getValues("unitContacts.UnitAdminContact");
  let dataUnitLeadContact = getValues("unitContacts.UnitLeadContact");

  const optionLabel = (option) =>
    `${
      option?.contact?.person?.fullName
        ? option?.contact?.person?.fullName + " " + "(" + option?.userName + ")"
        : ""
    }`;

  const viewCrmData = async ({ type, viewData }) => {
    switch (viewData) {
      case "UnitAdminContact":
        if (dataUnitAdminContact !== undefined) {
          setLoading1(true);
          const { data } = await findContactOU({
            id: dataUnitAdminContact.id,
            type: type,
          });
          setDataContact(data);
          setOpen(true);
          setLoading1(false);
        }
        break;
      case "UnitLeadContact":
        if (dataUnitLeadContact !== undefined) {
          setLoading2(true);
          const { data } = await findContactOU({
            id: dataUnitLeadContact.id,
            type: type,
          });
          setDataContact(data);
          setOpen(true);
          setLoading2(false);
        }
        break;
      default:
        null;
    }
  };

  const filterDataUser = () => {
    if (institutionData !== null) {
      const arrayOptions = dataUser.filter((item) =>
        item.contact.parents.some(
          (parent) =>
            parent.institution.institution.commonName === institutionData
        )
      );
      return arrayOptions;
    } else {
      return dataUser;
    }
  };

  return (
    <div>
      <br />
      <Typography variant="h6">
        <FormattedMessage id="none" defaultMessage="Contacts" />
      </Typography>
      <br />
      <Paper style={{ padding: 20 }} elevation={4} square>
        <Grid container direction="row" spacing={3}>
          <Grid item xs={12} md={12} sm={5} lg={3} xl={3}>
            <Typography variant="subtitle2" gutterBottom>
              <FormattedMessage id="none" defaultMessage="Contact Type" />
            </Typography>
          </Grid>
          <Grid item xs={12} md={12} sm={5} lg={4} xl={3}>
            <Typography variant="subtitle2" gutterBottom>
              <FormattedMessage id="none" defaultMessage="Name" />
            </Typography>
          </Grid>
        </Grid>
        <Grid container direction="row" spacing={4}>
          <Fragment key={1}>
            <Grid
              item
              xs={12}
              md={12}
              sm={5}
              lg={3}
              xl={3}
              container
              direction="row"
              justifyContent="flex-start"
              alignItems="flex-end"
            >
              <Typography variant="subtitle2" gutterBottom>
                <FormattedMessage
                  id="none"
                  defaultMessage={"Unit Admin Contact"}
                />
              </Typography>
            </Grid>
            <Grid item xs={12} md={10} sm={8} lg={6} xl={6}>
              <Controller
                name={`unitContacts.UnitAdminContact`}
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    size="small"
                    {...field}
                    id={"name"}
                    fullWidth
                    options={dataUser || []}
                    // inputValue={
                    //   getValues("unitContacts.UnitAdminContact")?.contact.person
                    //     .fullName || ""
                    // }
                    isOptionEqualToValue={(option, value) =>
                      option?.contact?.person?.fullName ===
                      value?.contact?.person?.fullName
                    }
                    getOptionLabel={optionLabel}
                    value={field?.value || null}
                    onChange={(e, option) => {
                      let valueNameAdmin = getValues(
                        `unitContacts.UnitLeadContact`
                      )?.contact.person.fullName;
                      if (valueNameAdmin === option?.contact.person.fullName) {
                      } else {
                        setValue(`unitContacts.UnitAdminContact`, option);
                      }
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        InputLabelProps={{ shrink: true }}
                        label={
                          <Typography variant="h6">
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={
                                reloading
                                  ? `Loading Contacts, ${percentage}% completed`
                                  : "Contacts"
                              }
                            />
                          </Typography>
                        }
                        error={
                          field.value ||
                          Boolean(errors.unitContacts?.UnitAdminContact) ===
                            false
                            ? false
                            : true
                        }
                        helperText={
                          field.value ||
                          Boolean(errors.unitContacts?.UnitAdminContact) ===
                            false ? null : (
                            <Typography className="text-sm font-ebs text-red-600">
                              <FormattedMessage
                                id="none"
                                defaultMessage="⚠ Field is required"
                              />
                            </Typography>
                          )
                        }
                      />
                    )}
                    disabled={disabled}
                  />
                )}
              />
            </Grid>
            <Grid item xs={2} md={2} sm={2} lg={2} xl={2}>
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"View Contact in CRM"}
                    />
                  </Typography>
                }
              >
                <IconButton
                  color="primary"
                  style={{ marginTop: "12%" }}
                  disabled={method === "POST" ? true : false}
                  onClick={() =>
                    viewCrmData({
                      type: "PERSON",
                      viewData: "UnitAdminContact",
                    })
                  }
                >
                  {loading1 === false ? <Visibility /> : <CircularProgress />}
                </IconButton>
              </Tooltip>
            </Grid>
          </Fragment>
          <Fragment key={2}>
            <Grid
              item
              xs={12}
              md={12}
              sm={5}
              lg={3}
              xl={3}
              container
              direction="row"
              justifyContent="flex-start"
              alignItems="flex-end"
            >
              <Typography variant="subtitle2" gutterBottom>
                <FormattedMessage
                  id="none"
                  defaultMessage={"Unit Lead Contact *"}
                />
              </Typography>
            </Grid>
            <Grid item xs={12} md={10} sm={8} lg={6} xl={6}>
              <Controller
                name={`unitContacts.UnitLeadContact`}
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    size="small"
                    {...field}
                    id={"name"}
                    fullWidth
                    options={dataUser || []}
                    // inputValue={
                    //   getValues("unitContacts.UnitLeadContact")?.contact.person
                    //     .fullName || ""
                    // }
                    isOptionEqualToValue={(option, value) =>
                      option?.contact?.person?.fullName ===
                      value?.contact?.person?.fullName
                    }
                    getOptionLabel={optionLabel}
                    onChange={(e, option) => {
                      let valueNameAdmin = getValues(
                        `unitContacts.UnitAdminContact`
                      )?.contact.person.fullName;

                      if (valueNameAdmin === option?.contact.person.fullName) {
                      } else {
                        setValue(`unitContacts.UnitLeadContact`, option);
                      }
                    }}
                    value={field?.value || null}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        InputLabelProps={{ shrink: true }}
                        label={
                          <Typography variant="h6">
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={
                                reloading
                                  ? `Loading Contacts, ${percentage}% completed`
                                  : "Contacts"
                              }
                            />
                          </Typography>
                        }
                        error={
                          field.value ||
                          Boolean(errors.unitContacts?.UnitLeadContact) ===
                            false
                            ? false
                            : true
                        }
                        helperText={
                          field.value ||
                          Boolean(errors.unitContacts?.UnitLeadContact) ===
                            false ? null : (
                            <Typography className="text-sm font-ebs text-red-600">
                              <FormattedMessage
                                id="none"
                                defaultMessage="⚠ Field is required"
                              />
                            </Typography>
                          )
                        }
                      />
                    )}
                    disabled={disabled}
                  />
                )}
                // rules={{ required: true }}
              />
            </Grid>
            <Grid item xs={2} md={2} sm={2} lg={2} xl={2}>
              <Tooltip
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-lg">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"View Contact in CRM"}
                    />
                  </Typography>
                }
              >
                <IconButton
                  color="primary"
                  style={{ marginTop: "12%" }}
                  disabled={method === "POST" ? true : false}
                  onClick={() =>
                    viewCrmData({
                      type: "PERSON",
                      viewData: "UnitLeadContact",
                    })
                  }
                >
                  {loading2 === false ? <Visibility /> : <CircularProgress />}
                </IconButton>
              </Tooltip>
            </Grid>
          </Fragment>
        </Grid>
        <br />
      </Paper>
      <br />
      <ViewContact open={open} handleClose={handleClose} data={dataContact} />
    </div>
  );
});

export default memo(UnitContacts);
