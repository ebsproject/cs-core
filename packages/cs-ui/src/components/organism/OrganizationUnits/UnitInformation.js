import React, { memo, useState, useEffect, Fragment } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { useQuery } from "@apollo/client";
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
import { FIND_CONTACT_LIST, FIND_CROP_LIST } from "utils/apollo/gql/crm";
import {
  // FIND_CROP_LIST,
  FIND_HIERARCHY_TREE,
} from "utils/apollo/gql/hierarchy";
import { useSelector, useDispatch } from "react-redux";
import { Core, Icons, Lab } from "@ebs/styleguide";
import {
  useForm,
  useFieldArray,
  Controller,
  useFormContext,
} from "react-hook-form";
import {
  findContactListP,
  findCropList,
  findOrganizationCgiar,
  getDataUserContac,
} from "./fuctionsOrganizations";
import { RBAC } from "@ebs/layout";
import AddressesFormAtom from "components/molecule/ContactForm/Addresses/AddressesForm/AddressesForm";
import { findPurposeList } from "store/modules/DataPersist";
const { Autocomplete } = Lab;
const {
  Typography,
  Button,
  TextField,
  Checkbox,
  Chip,
  Grid,
  Box,
  IconButton,
  Paper,
  Dialog,
  DialogContent,
  List,
  ListItem,
  Card,
  CardContent,
  CircularProgress,
} = Core;
const { Delete, AddCircle, Visibility, Edit } = Icons;

const UnitInformation = React.forwardRef((props, ref) => {
  const {
    setValue,
    getValues,
    control,
    formState: { errors },
  } = useFormContext();
  const [dataUpdate, setDataUpdate] = React.useState({});
  const [methodButton, setMethodButton] = useState(null);
  const [dataOrganizationCgiar, setdataOrganizationCgiar] = useState(null);
  const [dataCropList, setDataCropList] = useState(null);
  const [dataInstitution, setDataInstitution] = useState(undefined);
  const [dataAddressDefault, setDataAddressDefault] = useState("");
  const [writeAddress, setWriteAddress] = useState(null);
  const [open2, setOpen2] = React.useState(false);
  const { defaultValues } = useSelector(({ orgUnit }) => orgUnit);
  const dispatch = useDispatch();
  const {
    method,
    setvalueAddresses,
    valueAddresses,
    setInstitutionAdresss,
    setInstitutionData,
  } = props;

  const opUnitType = [
    { id: 2, name: "Service Unit" },
    { id: 1, name: "Program" },
  ];
  const dataInst = getValues("organization")?.institution?.commonName;
  const addressIntitutionSelect = getValues("organization")?.addresses;
  let arrayNew = [];
  const dataContactAddress = defaultValues?.addresses[0];
  let institutionData =
    defaultValues?.parents?.[0]?.institution?.institution?.commonName;

  useEffect(() => {
    if (method === "PUT") {
      if (dataInst !== undefined) {
        setInstitutionData(dataInst);
      }
    }
  }, [dataInst, method]);

  useEffect(() => {
    findPurposeList({ type: "Institution" })(dispatch);
  }, []);

  useEffect(() => {
    const fetchDataCropList = async () => {
      if (method === "PUT") {
        setDataInstitution(institutionData);
      }
      await findCropList(setDataCropList, dataInstitution);
    };
    fetchDataCropList();
  }, [dataInstitution]);

  useEffect(() => {
    const fetchDataOrganizationCgiar = async () => {
      await findOrganizationCgiar(setdataOrganizationCgiar);
    };
    fetchDataOrganizationCgiar();
  }, []);

  useEffect(() => {
    if (method === "PUT" && dataOrganizationCgiar) {
      const orgName = getValues("organization");
      const typeName = getValues("unitType");
      dataOrganizationCgiar?.forEach((item) => {
        if (item.institution.commonName === orgName)
          setValue("organization", item);
      });
      opUnitType.forEach((i) => {
        if (i.name === typeName) setValue("unitType", i);
      });
    }
  }, [dataOrganizationCgiar]);

  const orgOnChange = (e, option) => {
    setValue("organization", option);
    setDataInstitution(option?.institution?.commonName);
    setInstitutionAdresss(option);
    setInstitutionData(option?.institution?.commonName);
  };
  const optionLabel = (option) => `${option?.institution?.commonName}`;
  const orgOnChangeType = (e, option) => setValue("unitType", option);
  const optionLabelType = (option) => `${option?.name}`;
  const optionLabelCrops = (option) => `${option?.name}`;
  const optionLabelAddress = (option) => `${option?.fullAddress}`;
  const cropsOption = (props, option, { selected }) => {
    const { key, ...restProps } = props;
    return (
      <li key={key} {...restProps}>
        <Checkbox checked={selected} />
        {option.name}
      </li>
    );
  };

  const addressOption = (props, option, { selected }) => {
    const { key, ...restProps } = props;

    if (option) {
      return (
        <li key={key} {...restProps}>
          <Checkbox checked={selected} />
          {option.fullAddress}
        </li>
      );
    }
  };

  /**
   * @param {Object} option
   * @param {Object} value
   * @returns {Boolean}
   */
  const getContactsSelected = (option, value) => option.id === value.id;

  const onCropChange = (e, options, field) => {
    e.preventDefault();
    setValue(field, options);
  };

  const handleClose = () => {
    setOpen2(false);
  };

  const handleClickOpen = (option) => {
    setOpen2(true);
    setDataUpdate(option);
  };
  let arrayOptions = arrayNew.concat(addressIntitutionSelect);

  const optionsSAddressInstitution = () => {
    const options = arrayOptions.filter(
      (item) =>
        !defaultValues?.addresses.some(
          (item2) => item2?.fullAddress === item?.fullAddress
        )
    );
    return options;
  };

  return (
    <div>
      <br />
      <Typography variant="h6">
        <FormattedMessage id="none" defaultMessage="Information" />
      </Typography>
      <br />
      <Paper style={{ padding: 20 }} elevation={3} square>
        <Grid container direction="row" spacing={3}>
          <Grid item xs={12} md={12} sm={5} lg={3} xl={3}>
            <Controller
              name="organization"
              control={control}
              render={({ field }) => (
                <Autocomplete
                  {...field}
                  size="small"
                  id={"organization"}
                  options={dataOrganizationCgiar || []}
                  onChange={orgOnChange}
                  inputValue={
                    getValues("organization")?.institution?.commonName || ""
                  }
                  value={field?.value || null}
                  getOptionLabel={optionLabel}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      InputLabelProps={{ shrink: true }}
                      variant="outlined"
                      error={
                        field.value || Boolean(errors["organization"]) === false
                          ? false
                          : true
                      }
                      helperText={
                        field.value ||
                        Boolean(errors["organization"]) === false ? null : (
                          <Typography className="text-sm font-ebs text-red-600">
                            <FormattedMessage
                              id="none"
                              defaultMessage="⚠ Please, Select an Organization"
                            />
                          </Typography>
                        )
                      }
                      label={
                        <Typography variant="h6">
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Organization"}
                          />
                          {` *`}
                        </Typography>
                      }
                    />
                  )}
                  fullWidth
                  disabled={
                    method === "PUT"
                      ? defaultValues?.parents?.length === 0
                        ? false
                        : true
                      : false
                  }
                />
              )}
              rules={{
                required: true,
              }}
            />
          </Grid>
          <Grid item xs={12} md={12} sm={5} lg={3} xl={3}>
            <Controller
              name="unitName"
              control={control}
              render={({ field }) => (
                <TextField
                  InputLabelProps={{ shrink: true }}
                  {...field}
                  size="small"
                  variant="outlined"
                  fullWidth
                  error={
                    field.value || Boolean(errors["unitName"]) === false
                      ? false
                      : true
                  }
                  helperText={
                    field.value ||
                    Boolean(errors["unitName"]) === false ? null : (
                      <Typography className="text-sm font-ebs text-red-600">
                        <FormattedMessage
                          id="none"
                          defaultMessage="⚠ Field is required"
                        />
                      </Typography>
                    )
                  }
                  label={
                    <Typography variant="h6">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"Unit Name"}
                      />
                      {` *`}
                    </Typography>
                  }
                  data-testid={"unitName"}
                />
              )}
              rules={{
                required: true,
              }}
            />
          </Grid>
          <Grid item xs={12} md={12} sm={5} lg={3} xl={3}>
            <Controller
              name="unitAbbreviation"
              control={control}
              render={({ field }) => (
                <TextField
                  size="small"
                  InputLabelProps={{ shrink: true }}
                  {...field}
                  variant="outlined"
                  fullWidth
                  error={
                    field.value || Boolean(errors["unitAbbreviation"]) === false
                      ? false
                      : true
                  }
                  helperText={
                    field.value ||
                    Boolean(errors["unitAbbreviation"]) === false ? null : (
                      <Typography className="text-sm font-ebs text-red-600">
                        <FormattedMessage
                          id="none"
                          defaultMessage="⚠ Field is required"
                        />
                      </Typography>
                    )
                  }
                  label={
                    <Typography variant="h6">
                      <FormattedMessage
                        id={"none"}
                        defaultMessage={"Unit abbreviation"}
                      />
                      {` *`}
                    </Typography>
                  }
                  data-testid={"unitAbbreviation"}
                />
              )}
              rules={{
                required: true,
              }}
            />
          </Grid>
          <Grid item xs={12} md={12} sm={5} lg={3} xl={3}>
            <Controller
              name="unitType"
              control={control}
              render={({ field }) => (
                <Autocomplete
                  {...field}
                  id={"unitType"}
                  size="small"
                  options={opUnitType || []}
                  onChange={orgOnChangeType}
                  inputValue={getValues("unitType")?.name || ""}
                  isOptionEqualToValue={(option, value) =>
                    option.name === value.name
                  }
                  value={field?.value || null}
                  getOptionLabel={optionLabelType}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      variant="outlined"
                      InputLabelProps={{ shrink: true }}
                      error={
                        field.value || Boolean(errors["unitType"]) === false
                          ? false
                          : true
                      }
                      helperText={
                        field.value ||
                        Boolean(errors["unitType"]) === false ? null : (
                          <Typography className="text-sm font-ebs text-red-600">
                            <FormattedMessage
                              id="none"
                              defaultMessage="⚠ Please, Select an Organization"
                            />
                          </Typography>
                        )
                      }
                      label={
                        <Typography variant="h6">
                          <FormattedMessage
                            id={"none"}
                            defaultMessage={"Unit Type"}
                          />
                          {` *`}
                        </Typography>
                      }
                    />
                  )}
                  fullWidth
                />
              )}
              rules={{
                required: true,
              }}
            />
          </Grid>
          {dataInstitution !== undefined ? (
            <Grid item xs={12} md={12} sm={5} lg={4} xl={3}>
              <Controller
                name="crop"
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    multiple
                    limitTags={2}
                    size="small"
                    id={"crop"}
                    data-testid={"crop"}
                    options={dataCropList || []}
                    value={field?.value || []}
                    renderTags={(tagValue, getTagProps) =>
                      tagValue.map((option, index) => {
                        const { key, ...restProps } = getTagProps({ index });
                        return (
                          <Chip key={key} label={option.name} {...restProps} />
                        );
                      })
                    }
                    inputValue={getValues("crop")?.name || ""}
                    onChange={(e, options) => {
                      setValue("crop", options);
                      onCropChange(e, options, "crop");
                    }}
                    getOptionLabel={optionLabelCrops}
                    renderOption={cropsOption}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        InputLabelProps={{ shrink: true }}
                        focused={Boolean(errors["crop"])}
                        error={Boolean(errors["crop"])}
                        label={
                          <Typography className="text-ebs">
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Crop"}
                            />
                          </Typography>
                        }
                        // helperText={errors["crop"]?.message}
                        helperText={
                          field.value ||
                          Boolean(errors["crop"]) === false ? null : (
                            <Typography className="text-sm font-ebs text-red-600">
                              <FormattedMessage
                                id="none"
                                defaultMessage="⚠ Please, Select an Crop"
                              />
                            </Typography>
                          )
                        }
                      />
                    )}
                    isOptionEqualToValue={getContactsSelected}
                  />
                )}
                rules={{
                  required: true,
                }}
              />
            </Grid>
          ) : null}
          {dataInstitution === undefined ? null : (
            <Grid item xs={12} md={12} sm={5} lg={6} xl={6}>
              <Controller
                name="addressIntitution"
                control={control}
                render={({ field }) => (
                  <Autocomplete
                    {...field}
                    limitTags={2}
                    id={"addressIntitution"}
                    data-testid={"addressIntitution"}
                    options={optionsSAddressInstitution() || []}
                    value={field?.value || []}
                    inputValue={
                      getValues("addressIntitution")?.fullAddress || ""
                    }
                    onChange={(e, options) => {
                      setValue("addressIntitution", options);
                      setWriteAddress(options);
                    }}
                    getOptionLabel={optionLabelAddress}
                    renderOption={addressOption}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="outlined"
                        InputLabelProps={{ shrink: true }}
                        focused={Boolean(errors["addressIntitution"])}
                        error={Boolean(errors["addressIntitution"])}
                        label={
                          <Typography className="text-ebs">
                            <FormattedMessage
                              id={"none"}
                              defaultMessage={"Select Address"}
                            />
                          </Typography>
                        }
                        helperText={
                          field.value ||
                          Boolean(errors["addressIntitution"]) ===
                            false ? null : (
                            <Typography className="text-sm font-ebs text-red-600">
                              <FormattedMessage
                                id="none"
                                defaultMessage="⚠ Please, Select an Address"
                              />
                            </Typography>
                          )
                        }
                      />
                    )}
                  />
                )}
              />
            </Grid>
          )}
          {method !== "PUT" ? (
            <Grid item xs={12} md={12} sm={5} lg={2} xl={3}>
              <Button
                small
                style={{ marginTop: "15px" }}
                disabled={writeAddress === null ? false : true}
                className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
                startIcon={<AddCircle className="fill-current text-white" />}
                onClick={() => {
                  setOpen2(true);
                  setMethodButton("POST");
                }}
              >
                <Typography className="text-white text-sm font-ebs">
                  <FormattedMessage id="none" defaultMessage="Add address" />
                </Typography>
              </Button>
            </Grid>
          ) : null}
          <Grid item xs={12} md={12} sm={12} lg={6} xl={6}>
            <Typography className="text-black text-sm ">
              <FormattedMessage id="none" defaultMessage="Address" />
            </Typography>
            <Card>
              <ListItem>
                <>
                  {valueAddresses !== null ? (
                    <IconButton
                      aria-label="edit-addresses"
                      color="primary"
                      onClick={() => {
                        handleClickOpen(
                          method === "POST"
                            ? valueAddresses
                            : dataContactAddress
                        );
                        setMethodButton("PUT");
                      }}
                    >
                      <Edit />
                    </IconButton>
                  ) : method === "PUT" ? (
                    <IconButton
                      aria-label="edit-addresses"
                      color="primary"
                      onClick={() => {
                        handleClickOpen(
                          method === "POST"
                            ? valueAddresses
                            : dataContactAddress
                        );
                        setMethodButton("PUT");
                      }}
                    >
                      <Edit />
                    </IconButton>
                  ) : null}
                </>
                <br />
                {valueAddresses !== null ? (
                  <h5>{dataAddressDefault}</h5>
                ) : (
                  <h5>
                    {method === "PUT"
                      ? (dataContactAddress?.fullAddress === undefined
                          ? "No Unit address added"
                          : dataContactAddress?.fullAddress) +
                        ", " +
                        (dataContactAddress?.country?.name === undefined
                          ? ""
                          : writeAddress !== null
                            ? writeAddress.fullAddress
                            : dataContactAddress?.country?.name)
                      : "No Unit address added"}
                  </h5>
                )}
              </ListItem>
            </Card>
          </Grid>
        </Grid>
      </Paper>
      <Dialog
        fullWidth
        maxWidth="md"
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open2}
      >
        <DialogContent dividers>
          <AddressesFormAtom
            type="INSTITUTION"
            dataUpdate={dataUpdate}
            method={methodButton}
            conditionMethod="NEWPOST"
            setOpen={setOpen2}
            setvalueAddresses={setvalueAddresses}
            setDataAddressDefault={setDataAddressDefault}
            coditionComponent="OrganizationUnits"
          />
        </DialogContent>
      </Dialog>
    </div>
  );
});

export default UnitInformation;
