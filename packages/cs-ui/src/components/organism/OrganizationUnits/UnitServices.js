import React, { memo, useEffect, useState } from "react";
import { Core, Lab } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
const { Typography, TextField, Grid, Checkbox, Chip, CircularProgress } = Core;
const { Autocomplete } = Lab;
import { Controller } from "react-hook-form";
import { getDataWorkflows } from "./fuctionsOrganizations";
import { useFormContext } from "react-hook-form";

const UnitServices = React.forwardRef((props, ref) => {
  const {
    defaultValues,
    method,
    disabled,
    setValueWorkflow,
    valueWorkflow,
    setGenotypingView,
    ...rest
  } = props;
  const [dataW, setDataW] = useState([]);
  const {setValue, control, getValues } = useFormContext();

  useEffect(() => {
    const fetchData = async () => {
      await getDataWorkflows(setDataW);
    };

    fetchData();
  }, [method]);

  if (dataW.length === 0)
    return (
      <div className="flex justify-center pt-8 place-items-center">
        <CircularProgress color="primary" className="place-self-center" />
        <br />
      </div>
    );

  const dataWf = dataW.findWorkflowList.content;
  const optionLabel = (option) => `${option.name}`;
  const renderOption = (props, option, { selected }) => {
    const { key, ...restProps } = props;
    return (
        <li key={key}{...restProps}>
            <Checkbox checked={selected} />
            {option.name}
        </li>
    );
};
  const getWorkflowSelected = (option, value) => option.id === value.id;

  return (
    <div>
      <br />
      <Typography variant="h6">
        <FormattedMessage id="none" defaultMessage="Workflow Services" />
      </Typography>
      <br />
      <br />
      <Grid container direction="row" spacing={3}>
        <Grid item xs={12} md={8} sm={8} lg={10} xl={10}>
          <Controller
            name="workflow"
            control={control}
            render={({ field }) => (
              <Autocomplete
                {...field}
                multiple
                limitTags={3}
                id={"workflow"}
                data-testid={"workflow"}
                size="small"
                options={dataWf || []}
                value={field?.value || []}
                renderTags={(tagValue, getTagProps) =>
                  tagValue.map((option, index) => 
                    {
                      const { key, ...restProps } = getTagProps({ index });
                      return <Chip key={key} label={option.name} {...restProps} />
                    }
                )
                }
                inputValue={getValues("workflow")?.name || ""}
                onChange={(e, option) => {
                  setValue("workflow", option);
                  const genotypingBoolean = option.filter(
                    (item) => item.name === "Genotyping Service"
                  );
                  if (genotypingBoolean.length === 1) {
                    setGenotypingView(true);
                  } else {
                    setGenotypingView(false);
                  }
                }}
                getOptionLabel={optionLabel}
                renderOption={renderOption}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    variant="outlined"
                    InputLabelProps={{ shrink: true }}
                  />
                )}
                isOptionEqualToValue={getWorkflowSelected}
                fullWidth
                disabled={disabled}
              />
            )}
          />
        </Grid>
      </Grid>
    </div>
  );
});

export default memo(UnitServices);
