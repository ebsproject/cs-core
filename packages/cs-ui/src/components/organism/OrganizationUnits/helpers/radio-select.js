import { Controller, useFormContext } from "react-hook-form";
import { Core } from "@ebs/styleguide";
const { RadioGroup, Radio, FormControlLabel } = Core;


const RadioSelect = ({ name, options, rules }) => {
  const { setValue, control } = useFormContext();
  return (
    <Controller
      control={control}
      name={name}
      render={({ field }) => (
        <RadioGroup
          {...field}
          aria-label={name}
          onChange={(e) => setValue(name, e.target.value)}
        >
          {options.map((radio) => (
            <FormControlLabel
              checked={radio.value.toString() === field.value.toString()}
              key={radio.value}
              control={<Radio {...radio} />}
              {...radio}
            />
          ))}
        </RadioGroup>

      )}
      rules={rules}
    />)
}
export default RadioSelect;