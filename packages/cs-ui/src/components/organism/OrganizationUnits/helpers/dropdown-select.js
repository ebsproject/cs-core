import { Controller, useFormContext } from "react-hook-form";
import { Core } from "@ebs/styleguide";
const { TextField, Autocomplete, Typography, Checkbox } = Core;
import { FormattedMessage } from "react-intl";


const DropdownSelect = ({ name, label, rules, options, multiple, onChange }) => {
    const {setValue, control, formState: { errors } } = useFormContext();
    const renderInput = (params) => {
        return (
            <TextField
                {...params}
                variant="outlined"
                InputLabelProps={{ shrink: true }}
                error={!!errors[name]}
                helperText={errors[name] ? errors[name].message : ''}
                label={
                    <Typography>
                        <FormattedMessage id={"none"} defaultMessage={label} />
                        {`${rules ? "*":""} `}
                    </Typography>
                }
                focused={!!errors[name]}
                inputProps={{
                    ...params.inputProps,
                }}
            />
        )

    };
    const optionLabel = (option) => {
        return `${option.name || ""}`;
      };
    const handleChange = (e, options) => {
        setValue(name, options);
        onChange && onChange(e,options);
    }
    const isOptionEqualToValue = (option, value) => {
        if (option.value > 0)
            return Number(option.value) === Number(value.value);
        }
    const renderOption = (props, option, { selected }) => {
        const { key, ...restProps } = props;
        return (
            <li key={key}{...restProps}>
                <Checkbox checked={selected} />
                {option.name}
            </li>
        );
    };

    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <Autocomplete
                    {...field}
                    multiple={multiple}
                    limitTags={6}
                    id={name}
                    data-testid={`testid-${name}`}
                    options={options}
                    value={
                           multiple
                          ? (field?.value?.length && field.value) || []
                          : field?.value || { value: 0, label: '' }}
                    disableCloseOnSelect
                    onChange={handleChange}
                    getOptionLabel={optionLabel}
                    renderOption={renderOption}
                    renderInput={renderInput}
                    isOptionEqualToValue={isOptionEqualToValue}
                />
            )}
            rules={rules}
        />
    )
}
export default DropdownSelect;