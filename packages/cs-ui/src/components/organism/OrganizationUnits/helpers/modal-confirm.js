import {Core} from "@ebs/styleguide"
const {Dialog, DialogTitle, Typography, DialogActions, Button } = Core;
import { FormattedMessage } from "react-intl";

const ModalConfirm = ({open, handleClose, handleUpdate, method}) =>{
    return(<Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >

          <DialogTitle id="alert-dialog-title">
            <Typography className="font-ebs">
              <FormattedMessage
                id={"none"}
                defaultMessage={ method === "POST" ? "Do you want to add another user?": "Would you like to edit more information in this record?"}
              />
            </Typography>
          </DialogTitle>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id={"none"} defaultMessage={"No"} />
          </Button>
          <Button onClick={handleUpdate} autoFocus>
            <FormattedMessage id={"none"} defaultMessage={"Yes"} />
          </Button>
        </DialogActions>
      </Dialog>)
}
export default ModalConfirm;