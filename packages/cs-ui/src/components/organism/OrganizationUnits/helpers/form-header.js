import { Core, Icons } from "@ebs/styleguide"
const { Typography, Button, Grid } = Core;
import { FormattedMessage } from "react-intl";
const { CheckCircle, KeyboardArrowLeft } = Icons;

const FormHeader = ({ buttonRef,handleClickOpen }) => {

    return (
        <>
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organizational Units" />
        </Typography>
        {/* <Button
          ref={buttonRef}
          color="primary"
          variant="contained"
          style={{ display: "none" }}

        >
          <FormattedMessage id="none" defaultMessage="Save and Continue" />
        </Button> */}
        <Grid container justifyContent="flex-end">
          <Button
            className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
            startIcon={
              <KeyboardArrowLeft className="fill-current text-white" />
            }
            onClick={handleClickOpen}
          >
            <Typography className="text-white text-sm font-ebs">
              <FormattedMessage id="none" defaultMessage="Back" />
            </Typography>
          </Button>
          <Button
            className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white ml-3"
            startIcon={<CheckCircle className="fill-current text-white" />}
            type="submit"
            ref={buttonRef}
          >
            <Typography className="text-white text-sm font-ebs">
              <FormattedMessage id="none" defaultMessage="Save" />
            </Typography>
          </Button>
        </Grid>
        </>
    )
}
export default FormHeader;