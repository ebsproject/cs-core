import React, { memo, useEffect, useRef, useState } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch } from "react-redux";
import { Core, Icons, EbsDialog } from "@ebs/styleguide";
import { FormProvider, useForm, useWatch } from "react-hook-form";
import { showMessage } from "store/modules/message";

//import TemplatesToolbar from "./templatesToolbar";
import { useNavigate } from "react-router-dom";
import UnitInformation from "./UnitInformation";
import {
  CreateServiceProvider,
  ModifyServiceProvider,
} from "components/molecule/UnitForm/functions";
import UnitContacts from "./UnitContacts";
import { findTenantById } from "store/modules/tenant";
import { findServiceByIdOrgUnit } from "store/modules/SM";
import { getContext } from "@ebs/layout";
import UnitReferenceGenotyping from "./UnitReferenceGenotyping";
import UnitServices from "./UnitServices";
import {
  createHierarchyUMPost,
  createOrganizationUnit,
  deleteAddress,
  findRolesUser,
  getDataUser,
  modifyOrganizationUnit,
} from "./fuctionsOrganizations";
import UnitMembers from "./UnitMembers";
import FormHeader from "./helpers/form-header";
import { removeAddress } from "store/modules/CRM";
import UnitMemberPost from "./UnitMemberPost";
const {
  Typography,
  Button,
  Box,
  Paper,
  DialogActions,
  DialogContent,
  Grid,
  Dialog,
  DialogContentText,
  DialogTitle,
} = Core;
const { CheckCircle, KeyboardArrowLeft, Warning } = Icons;

const OrganizationUnitsDetail = React.forwardRef(({}, ref) => {
  const { defaultValues, method } = useSelector(({ orgUnit }) => orgUnit);
  const [open, setOpen] = useState(false);
  const [openSave, setOpenSave] = useState(false);
  const [valueAddresses, setvalueAddresses] = useState(null);
  const [valueWorkflow, setValueWorkflow] = useState([]);
  const [rolesUser, setRolesUser] = useState([]);
  const [serviceP, setServiceP] = useState(null);
  const [institutionAddress, setInstitutionAdresss] = useState(null);
  const [tenantId, setTenantId] = useState(0);
  const [dataUser, setDataUser] = useState([]);
  const [reloading, setReloading] = useState(false);
  const [reloadingRol, setReloadingRol] = useState(false);
  const [total, setTotal] = useState(1);
  const [totalRol, setTotalRol] = useState(1);
  const [genotypingView, setGenotypingView] = useState(false);
  const [unitAdminContact, setUnitAdminContact] = useState(undefined);
  const [unitLeadContact, setUnitLeadContact] = useState(undefined);
  const [institutionData, setInstitutionData] = useState(null);
  const [memberUser, setMemberUser] = useState([]);
  const [refreshKey, setRefreshKey] = useState(0);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const buttonRef = useRef(null);
  let percentage = 0;
  let percentageRol = 0;

  const formMethods = useForm({
    defaultValues:
      method === "POST"
        ? {
            teams: [],
          }
        : {
            id: defaultValues?.id,
            organization:
              defaultValues?.parents?.[0]?.institution?.institution?.commonName,
            unitAbbreviation: defaultValues?.institution?.commonName,
            unitName: defaultValues?.institution?.legalName,
            unitType: defaultValues?.institution?.unitType?.name,
            workflow: defaultValues?.workflows?.map((item) => item.workflow),
            crop: defaultValues?.institution?.crops,
          },
  });
  const { handleSubmit, reset, control } = formMethods;

  const watchAllFields = useWatch({ control });

  useEffect(() => {
    setDataUser([]);
    setRolesUser([]);
    const fetchDataUser = async () => {
      await getDataUser(setDataUser, setReloading, setTotal);
    };
    const fetchRolesUser = async () => {
      await findRolesUser(setRolesUser, setReloadingRol, setTotalRol);
    };
    fetchRolesUser();
    fetchDataUser();
  }, [method]);

  useEffect(() => {
    if (method === "PUT") {
      const fetchServicesP = async () => {
        const result = await findServiceByIdOrgUnit(defaultValues?.id);
        setServiceP(result?.findServiceProvider);
      };
      const dataWorkflowView = defaultValues?.workflows.filter(
        (item) => item.workflow.name === "Genotyping Service"
      );
      if (dataWorkflowView.length === 1) {
        setGenotypingView(true);
      } else {
        setGenotypingView(false);
      }
      fetchServicesP();
    }
  }, [method]);

  useEffect(() => {
    if (method === null) {
      navigate("/cs/organization-units");
    }
  }, [method]);

  useEffect(() => {
    if (method === "PUT") {
      defaultValues?.members?.forEach((element) => {
        switch (element?.roles[0]?.name) {
          case "Unit Administrative Contact":
            const contactAdmin = dataUser.filter(
              (item) => item.contact.id === element.contact.id
            );
            setUnitAdminContact(contactAdmin[0]);
            break;
          case "Unit Leader":
            const contactLeader = dataUser.filter(
              (item) => item.contact.id === element.contact.id
            );
            setUnitLeadContact(contactLeader[0]);
            break;
          default:
            "";
        }
      });
    }
  }, [defaultValues]);

  let dataJsonPut = {
    id: defaultValues?.id,
    organization:
      defaultValues?.parents?.[0]?.institution?.institution?.commonName,
    unitAbbreviation: defaultValues?.institution?.commonName,
    unitName: defaultValues?.institution?.legalName,
    unitType: defaultValues?.institution?.unitType?.name,
    workflow: defaultValues?.workflows
      ?.map((item) => item.workflow.name)
      .toString(),
    crop: defaultValues?.institution?.crops.map((item) => item.name).toString(),
    UnitAdminContact: unitAdminContact?.contact?.person?.fullName,
    UnitLeadContact: unitLeadContact?.contact?.person?.fullName,
  };

  const dataWatchFields = {
    id: watchAllFields?.id,
    crop: watchAllFields?.crop?.map((item) => item.name).toString(),
    organization: watchAllFields?.organization?.institution?.commonName,
    unitAbbreviation: watchAllFields?.unitAbbreviation,
    UnitAdminContact:
      watchAllFields?.unitContacts?.UnitAdminContact?.contact?.person?.fullName,
    UnitLeadContact:
      watchAllFields?.unitContacts?.UnitLeadContact?.contact?.person?.fullName,
    unitName: watchAllFields?.unitName,
    unitType: watchAllFields?.unitType?.name,
    workflow: watchAllFields?.workflow?.map((item) => item.name).toString(),
  };

  const defaultAddreses = {
    id: Number(defaultValues?.addresses[0]?.id),
    countryId: defaultValues?.addresses[0]?.country?.id,
    default:
      defaultValues?.addresses[0]?.default === false
        ? true
        : defaultValues?.addresses[0]?.default,
    location: defaultValues?.addresses[0]?.location,
    purposeIds: defaultValues?.addresses[0]?.purposes?.map((item) =>
      Number(item.id)
    ),
    region: defaultValues?.addresses[0]?.region,
    streetAddress: defaultValues?.addresses[0]?.streetAddress,
    zipCode: defaultValues?.addresses[0]?.zipCode,
    additionalStreetAddress:
      defaultValues?.addresses[0]?.additionalStreetAddress,
  };

  const idUnitAdmin = rolesUser?.filter(
    (item) => item.name === "Unit Administrative Contact"
  );
  const idUnitLead = rolesUser?.filter((item) => item.name === "Unit Leader");

  const onSubmit = async (data, e) => {
    if (e.target.name !== "AddressForm") {
      switch (method) {
        case "PUT":
          let _defaultValues = Object.assign({}, defaultValues);
          let memberDefaul = _defaultValues.members;
          let newMemberObj = {};
          let newMemberArray = [];
          memberDefaul.forEach((element) => {
            element.roles.forEach((item) => {
              if (
                item.name === "Unit Administrative Contact" ||
                item.name === "Unit Leader"
              ) {
                newMemberObj = { ...element, remove: true };
              } else {
                newMemberObj = { ...element, remove: false };
              }
              newMemberArray = [...newMemberArray, newMemberObj];
            });
          });
          let memberUpdate = newMemberArray.filter(
            (item) => item.remove !== true
          );
          let memberArray = memberUpdate.map((item) => ({
            memberId: Number(item?.contact?.id),
            roleIds: [Number(item?.roles?.[0]?.id)],
          }));
          if (data.unitContacts.UnitAdminContact) {
            memberArray.push({
              memberId: data.unitContacts.UnitAdminContact.contact.id,
              roleIds: [Number(idUnitAdmin[0].id)],
            });
          }
          if (data.unitContacts.UnitLeadContact) {
            memberArray.push({
              memberId: data.unitContacts.UnitLeadContact.contact.id,
              roleIds: [Number(idUnitLead[0].id)],
            });
          }
          const addressFilter =
            _defaultValues?.parents?.[0]?.institution?.addresses?.filter(
              (item) => item.default === true
            );
          const defaultAddresesPut = {
            id: 0,
            countryId: addressFilter?.[0]?.country?.id,
            default:
              addressFilter?.[0]?.default === false
                ? true
                : addressFilter?.[0]?.default,
            location: addressFilter?.[0]?.location,
            purposeIds: addressFilter?.[0]?.purposes?.map((item) =>
              Number(item.id)
            ),
            region: addressFilter?.[0]?.region,
            streetAddress: addressFilter?.[0]?.streetAddress,
            zipCode: addressFilter?.[0]?.zipCode,
            additionalStreetAddress:
              addressFilter?.[0]?.additionalStreetAddress,
          };
          let dataAddressPost = {
            id: 0,
            countryId: data?.addressIntitution?.country?.id,
            default: true,
            location: data?.addressIntitution?.location,
            purposeIds: data?.addressIntitution?.purposes?.map((item) =>
              Number(item.id)
            ),
            region: data?.addressIntitution?.region,
            streetAddress: data?.addressIntitution?.streetAddress,
            zipCode: data?.addressIntitution?.zipCode,
            additionalStreetAddress:
              data?.addressIntitution?.additionalStreetAddress,
          };
          if (data.addressIntitution !== undefined) {
            if (defaultValues?.addresses?.length !== 0) {
              await deleteAddress(
                { id: Number(defaultValues?.addresses[0]?.id) },
                dispatch
              );
            }
          }

          let dataUnit = {
            contact: {
              id: method === "PUT" ? Number(data.id) : 0,
              category: {
                id: method === "PUT" ? _defaultValues?.category.id : 4,
              },
              institution: {
                legalName: data.unitName,
                commonName: data.unitAbbreviation,
                cgiar: false,
                unitTypeId: data.unitType.id,
                cropIds: data.crop.map((item) => Number(item.id)),
              },
              addresses:
                valueAddresses !== null
                  ? valueAddresses
                  : _defaultValues?.addresses?.length === 0
                    ? defaultAddresesPut
                    : data.addressIntitution === undefined
                      ? defaultAddreses
                      : dataAddressPost,
            },
            parentOrganizationId: data.organization.id,
            unitMembers: memberArray,
            workflowIds:
              data?.workflow === undefined
                ? []
                : data.workflow.map((item) => item.id),
          };
          await modifyOrganizationUnit(
            { modifydataUnit: dataUnit, type: "OrganizationEdit" },
            dispatch
          );
          if (serviceP === undefined) {
            if (
              data.serviceTypeSelector !== undefined &&
              data.formsSelector !== undefined
            ) {
              CreateServiceProvider(
                _defaultValues?.id,
                data.unitAbbreviation,
                data.unitName,
                tenantId,
                data?.crop[0]?.id,
                data?.serviceTypeSelector?.id,
                data?.formsSelector?.id,
                dispatch
              )
                .then((result) => {})
                .catch((e) => {
                  console.log(e);
                });
            }
          } else {
            if (
              data.serviceTypeSelector !== undefined &&
              data.formsSelector !== undefined
            ) {
              ModifyServiceProvider(
                _defaultValues?.id,
                data.unitAbbreviation,
                data.unitName,
                tenantId,
                data?.crop[0]?.id,
                data.serviceTypeSelector.id,
                data.formsSelector.id
              )
                .then((result) => {})
                .catch((e) => {
                  console.log(e);
                });
            }
          }
          break;
        case "POST":
          let newMemberPost = [];
          let postMember = [];
          if (data.unitContacts.UnitAdminContact) {
            postMember.push({
              memberId: data.unitContacts.UnitAdminContact.contact.id,
              roleIds: [Number(idUnitAdmin[0].id)],
            });
          }
          if (data.unitContacts.UnitLeadContact) {
            postMember.push({
              memberId: data.unitContacts.UnitLeadContact.contact.id,
              roleIds: [Number(idUnitLead[0].id)],
            });
          }
          let dataAddress = {
            id: 0,
            countryId: data?.addressIntitution?.country?.id,
            default: true,
            location: data?.addressIntitution?.location,
            purposeIds: data?.addressIntitution?.purposes?.map((item) =>
              Number(item.id)
            ),
            region: data?.addressIntitution?.region,
            streetAddress: data?.addressIntitution?.streetAddress,
            zipCode: data?.addressIntitution?.zipCode,
            additionalStreetAddress:
              data?.addressIntitution?.additionalStreetAddress,
          };
          const defaultAddresesPost = {
            id: 0,
            countryId: institutionAddress?.addresses?.[0]?.country?.id,
            default:
              institutionAddress?.addresses?.[0]?.default === false
                ? true
                : institutionAddress?.addresses?.[0]?.default,
            location: institutionAddress?.addresses?.[0]?.location,
            purposeIds: institutionAddress?.addresses?.[0]?.purposes?.map(
              (item) => Number(item.id)
            ),
            region: institutionAddress?.addresses?.[0]?.region,
            streetAddress: institutionAddress?.addresses?.[0]?.streetAddress,
            zipCode: institutionAddress?.addresses?.[0]?.zipCode,
            additionalStreetAddress:
              institutionAddress?.addresses?.[0]?.additionalStreetAddress ===
              null
                ? institutionAddress?.addresses?.[0]?.additionalStreetAddress
                : " ",
          };
          if (dataAddress.countryId === undefined) {
            dataAddress = undefined;
          }
          let dataUnitPost = {
            contact: {
              id: method === "PUT" ? Number(data.id) : 0,
              category: {
                id: method === "PUT" ? _defaultValues.category.id : 4,
              },
              institution: {
                legalName: data.unitName,
                commonName: data.unitAbbreviation,
                cgiar: false,
                unitTypeId: data.unitType.id,
                cropIds: data.crop.map((item) => Number(item.id)),
              },
              addresses:
                valueAddresses ??
                dataAddress ??
                data.addressIntitution ??
                defaultAddresesPost,
            },
            parentOrganizationId: data.organization.id,
            unitMembers: [],
            workflowIds:
              data?.workflow === undefined
                ? []
                : data?.workflow?.map((item) => item.id),
          };

          const newMembers = postMember.map((member) => ({
            rol: { id: member.roleIds[0] },
            user: { contact: { id: member.memberId } },
          }));
          newMemberPost = [...memberUser, ...newMembers];
          createOrganizationUnit(dataUnitPost)
            .then((result) => {
              if (result) {
                dispatch(
                  showMessage({
                    message: `Unit was created successfully `,
                    variant: "success",
                    anchorOrigin: {
                      vertical: "top",
                      horizontal: "right",
                    },
                  })
                );
                if (newMemberPost.length !== 0) {
                  newMemberPost.map(async (item) => {
                    const dataItem = {
                      contactId: Number(item.user.contact.id),
                      institutionId: result,
                      principalContact: false,
                    };
                    await createHierarchyUMPost({
                      createHierarchy: dataItem,
                      idRol: Number(item.rol.id),
                    });
                  });
                }
                if (data.formsSelector && data.formsSelector) {
                  CreateServiceProvider(
                    result,
                    data.unitAbbreviation,
                    data.unitName,
                    tenantId,
                    data?.crop[0]?.id,
                    data.serviceTypeSelector.id,
                    data.formsSelector.id,
                    dispatch
                  )
                    .then((result) => {
                      if (result) {
                      }
                    })
                    .catch((e) => {
                      console.log(e);
                      dispatch(
                        showMessage({
                          message: e.toString(),
                          variant: "error",
                          anchorOrigin: {
                            vertical: "top",
                            horizontal: "right",
                          },
                        })
                      );
                    });
                } else {
                  dispatch(
                    showMessage({
                      message: `Unit was created successfully `,
                      variant: "success",
                      anchorOrigin: {
                        vertical: "top",
                        horizontal: "right",
                      },
                    })
                  );
                }
              }
            })
            .catch((e) => {
              console.error(e);
              dispatch(
                showMessage({
                  message: e.toString(),
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
            });
          break;
        default:
          null;
      }
      setOpenSave(true);
    }
  };

  const handleClose = () => setOpen(false);
  const handleCloseSave = () => setOpenSave(false);
  const resetAction = () => {
    reset({
      unitName: "",
      unitAbbreviation: "",
    }),
      setvalueAddresses(null);
    setOpenSave(false);
    setValueWorkflow([]);
    setRefreshKey((prev) => prev + 1);
  };
  const handleClickOpen = () => {
    if (method === "PUT") {
      const formChange = _.isEqual(dataJsonPut, dataWatchFields);
      if (formChange) {
        navigate("/cs/organization-units");
      } else {
        setOpen(true);
      }
    } else {
      let valores = Object.values(dataWatchFields);
      function goBackPage() {
        for (let i = 0; i < valores.length; i++) {
          if (valores[i] !== undefined && valores[i] !== "") {
            return true;
          }
        }
        return false;
      }

      if (goBackPage()) {
        setOpen(true);
      } else {
        navigate("/cs/organization-units");
      }
    }
  };
  const handleCloseModify = () => {
    setOpen(false);
    navigate("/cs/organization-units");
  };
  percentage = Math.round((dataUser.length / total) * 100, 2);
  percentageRol = Math.round((rolesUser.length / totalRol) * 100, 2);
  return (
    <div>
      <EbsDialog
        open={open}
        handleClose={handleClose}
        maxWidth="sm"
        title={"You have unsaved changes."}
      >
        <DialogContent>
          <Typography className="text-ebs text-black text-base">
            <FormattedMessage
              id="none"
              defaultMessage="You are navigating to a new page. Do yo wish to save your changes first? You can choose to save and continue, cancel and stay on this page, or discard changes and continue."
            />
          </Typography>
        </DialogContent>

        <DialogActions className="grid grid-cols-3 gap-3">
          <div className="col-start-2">
            <Button
              onClick={handleCloseModify}
              variant={"text"}
              color={"error"}
            >
              <FormattedMessage id="none" defaultMessage="Yes. Discard" />
            </Button>
          </div>
          <Grid container direction={"row"} justifyContent={"flex-end"}>
            <Button onClick={handleClose} color="primary">
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Button>

            <Button
              onClick={() => {
                buttonRef.current.click(), handleClose();
              }}
              color="primary"
              variant="contained"
              className=" bg-ebs-brand-default"
            >
              <FormattedMessage id="none" defaultMessage="Save and Continue" />
            </Button>
          </Grid>
        </DialogActions>
      </EbsDialog>

      <Dialog open={openSave} handleClose={handleCloseSave} maxWidth="sm">
        <DialogTitle id="alert-dialog-title">
          {method === "PUT"
            ? "Would you like to edit more information in this record?"
            : "Do you want to add another Organization?"}
        </DialogTitle>
        <DialogActions>
          <Button
            onClick={() => navigate("/cs/organization-units")}
            color="primary"
          >
            <FormattedMessage id="none" defaultMessage="NO" />
          </Button>
          <Button
            onClick={method === "PUT" ? handleCloseSave : resetAction}
            color="primary"
            variant="contained"
            className=" bg-ebs-brand-default"
          >
            <FormattedMessage id="none" defaultMessage="YES" />
          </Button>
        </DialogActions>
      </Dialog>
      <FormProvider {...formMethods}>
        <form
          onSubmit={handleSubmit(onSubmit)}
          name="OrganizationUnit"
          data-testid="OrganizationUnit"
        >
          <FormHeader buttonRef={buttonRef} handleClickOpen={handleClickOpen} />
          <UnitInformation
            method={method}
            setvalueAddresses={setvalueAddresses}
            valueAddresses={valueAddresses}
            setInstitutionAdresss={setInstitutionAdresss}
            setInstitutionData={setInstitutionData}
          />
          <Grid container spacing={2}>
            <Grid item xs={6}>
              <UnitContacts
                method={method}
                reloading={reloading}
                percentage={percentage}
                dataUser={dataUser}
                total={total}
              />
            </Grid>
            <Grid item xs={6}>
              <br />
              <Typography variant="h6">
                <FormattedMessage
                  id="none"
                  defaultMessage="Services Provided"
                />
              </Typography>
              <br />
              <Paper style={{ padding: 20 }} elevation={4} square>
                <UnitServices
                  defaultValues={defaultValues}
                  method={method}
                  setValueWorkflow={setValueWorkflow}
                  valueWorkflow={valueWorkflow}
                  setGenotypingView={setGenotypingView}
                />
                <br />
                <br />
                {genotypingView && (
                  <UnitReferenceGenotyping
                    defaultValues={defaultValues}
                    method={method}
                  />
                )}
              </Paper>
            </Grid>
          </Grid>
          {method === "PUT" ? (
            <UnitMembers
              method={method}
              rolesUser={rolesUser}
              defaultAddreses={defaultAddreses}
              reloading={reloading}
              percentage={percentage}
              dataUser={dataUser}
              reloadingRol={reloadingRol}
              percentageRol={percentageRol}
              process="OrganizationUnit"
              institutionData={institutionData}
            />
          ) : (
            <UnitMemberPost
              key={refreshKey}
              method={method}
              rolesUser={rolesUser}
              reloading={reloading}
              percentage={percentage}
              dataUser={dataUser}
              reloadingRol={reloadingRol}
              percentageRol={percentageRol}
              process="OrganizationUnit"
              institutionData={institutionData}
              setMemberUser={setMemberUser}
            />
          )}
        </form>
      </FormProvider>
    </div>
  );
});
// Type and required properties
OrganizationUnitsDetail.propTypes = {};

export default memo(OrganizationUnitsDetail);
