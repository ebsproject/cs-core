import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import OrganizationUnitsTable from "./OrganizationUnitsTable";

afterEach(cleanup);

test("OrganizationsTable is in the DOM", () => {
  render(<OrganizationUnitsTable></OrganizationUnitsTable>, {
    wrapper: Wrapper,
  });
});
