import React, { useEffect, useState } from "react";
import { Core, Icons } from "@ebs/styleguide";
const { PostAdd, KeyboardArrowLeft } = Icons;
const { Tooltip, Button, Typography, DialogActions } = Core;
import { FormattedMessage } from "react-intl";
import UnitForm from "components/molecule/UnitForm";
import { useNavigate } from "react-router-dom";
import { RBAC } from "@ebs/layout";
import { setMethod } from "store/modules/OrganizationUnit";
import { useSelector, useDispatch } from "react-redux";

const toolbarOrganizationUnit = (selectedRows, refresh) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleClickOpen = () => {
    dispatch(setMethod("POST"));
    navigate("/cs/organization-units-detail");
  };

  return (
    <div>
      <DialogActions>
        <RBAC allowedAction={"Create"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"New Unit"} />
              </Typography>
            }
          >
            <Button
              className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
              aria-label="add-unit"
              startIcon={<PostAdd className="fill-current text-white" />}
              onClick={() => handleClickOpen()}
            >
              <Typography className="text-white text-sm font-ebs">
                <FormattedMessage id="none" defaultMessage="New Unit" />
              </Typography>
            </Button>
          </Tooltip>
        </RBAC>
      </DialogActions>
    </div>
  );
};

export default toolbarOrganizationUnit;
