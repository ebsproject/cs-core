import React, { Fragment, memo, useEffect, useState } from "react";
import { Core, Icons, Lab } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
const { Typography, Grid, Paper } = Core;
import FormSelect from "components/atom/FormSelect";
import ServiceTypeSelect from "components/atom/ServiceTypeSelect";
import { useFormContext } from "react-hook-form";
const UnitReferenceGenotyping = React.forwardRef((props, ref) => {
  const {
    defaultValues,
    method
  } = props;
  const { register } = useFormContext();
  return (
    <div>
      <Typography variant="h6">
        <FormattedMessage
          id="none"
          defaultMessage="Reference Genotyping Request"
        />
      </Typography>
      <br />
      <Grid container direction="row" spacing={3}>
        <Grid item xs={12} md={12} sm={4} lg={6} xl={6}>
          <ServiceTypeSelect
            {...register("serviceTypeSelector")}
            method={method}
            defaultValues={defaultValues}
          />
        </Grid>
        <Grid item xs={12} md={12} sm={4} lg={6} xl={6}>
          <FormSelect
            {...register("formsSelector")}
            method={method}
            defaultValues={defaultValues}
          />
        </Grid>
      </Grid>
    </div>
  );
});

export default memo(UnitReferenceGenotyping);
