import React, { memo } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
import { FIND_ORG_UNITS_LIST } from "utils/apollo/gql/crm";

import { Core } from "@ebs/styleguide";
import toolbarOrganizationUnit from "./toolbarOrganizationUnit";
import { rowActionsOrganizationUnit } from "./rowActionsOrganizationUnit";

const { Typography } = Core;
//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */

const OrganizationUnitsTable = React.forwardRef(({}, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "parents[0].institution.institution.legalName",
      csvHeader: "Organization",
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Unit name" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.legalName",
      csvHeader: "Unit Name",
      width: 400,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Unit Abbreviation" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.commonName",
      csvHeader: "Abbreviation",
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Unit Type" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "institution.unitType.name",
      csvHeader: "Unit Type",
      width: 250,
    },
  ];

  const fetch = async ({ page, sort, filters }) => {    
    return new Promise((resolve, reject) => {
      filters.map((item) => {
        if (item.col === "parents[0].institution.institution.legalName") {
          item.col = "parents.institution.institution.legalName";
        }
      });
      sort.map((item) => {
        if (item.col === "parents[0].institution.institution.legalName") {
          item.col = "parents.institution.institution.legalName";
        }
      });
      if (sort.length === 0) {
        sort.push(
          { col: "updatedOn", mod: "DES" },
          { col: "createdOn", mod: "DES" }
        );
      }
      sort.map((item) => {
        if (item.col === "createdBy.userName") {
          item.col = "createdBy";
        }
        if (item.col === "updatedBy.userName") {
          item.col = "updatedBy";
        }
      });
      try {
        client
          .query({
            query: FIND_ORG_UNITS_LIST,
            variables: {
              page: page,
              sort: sort,
              filters: [
                ...filters,
                { col: "category.name", mod: "EQ", val: "Internal Unit" },
              ],
              disjunctionFilters: false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              resolve({
                pages: data.findContactList.totalPages,
                elements: data.findContactList.totalElements,
                data: data.findContactList.content,
              });
            }
          });
      } catch (e) {
        console.log(e);
      }
    });
  };

  return (
    <EbsGrid
      id="OrganizationalUnitsTable"
      toolbar={true}
      data-testid={"OrganizationalUnitsTable"}
      columns={columns}
      toolbaractions={toolbarOrganizationUnit}
      rowactions={rowActionsOrganizationUnit}
      csvfilename="OrganizationalUnitsTable"
      fetch={fetch}
      height="85vh"
      select="multi"
      auditColumns
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organizational Units" />
        </Typography>
      }
    />
  );
});
// Type and required properties
OrganizationUnitsTable.propTypes = {};

export default memo(OrganizationUnitsTable);
