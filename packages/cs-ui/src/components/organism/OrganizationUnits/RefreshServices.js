import { useEffect } from "react";

const RefreshServices = ({ refresh, refreshGrid, setRefreshGrid }) => {
  useEffect(() => {
    if (refreshGrid) {
      refresh();
    }
    return () => setRefreshGrid(false);
  });
  return null;
};
export default RefreshServices;
