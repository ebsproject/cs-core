import React, { useState, useEffect } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
import { setDefaultValues, setMethod } from "store/modules/OrganizationUnit";
import { useSelector, useDispatch } from "react-redux";
import { client } from "utils/apollo";
import {
  DELETE_ORGANIZATION_UNIT,
  FIND_ORG_UNITS_ID,
} from "utils/apollo/gql/crm";
import { deleteOrganizationalUnit } from "./fuctionsOrganizations";
import OrganizationUnitView from "./OrganizationUnitView";
import { findServiceByIdOrgUnit } from "store/modules/SM";
const { Box, Tooltip, Typography, IconButton, CircularProgress } = Core;
const { Edit, Delete, Visibility } = Icons;
/**
 * @param rowData: Object with row data.
 * @param refresh: Function to refresh Grid data.
 */
export const rowActionsOrganizationUnit = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const dispatch = useDispatch();
    const type = "INSTITUTION";
    const navigate = useNavigate();
    const [open, setOpen] = useState(false);
    const [dataUnit, setDataUnit] = useState(null);
    const [serviceP, setServiceP] = useState(null);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
      const fetchServicesP = async () => {
        const result = await findServiceByIdOrgUnit(dataUnit?.id);
        setServiceP(result.findServiceProvider);
      };
      fetchServicesP();
    }, [dataUnit]);

    const handleGoTo = async () => {
      setLoading(true);
      const { data } = await client.query({
        query: FIND_ORG_UNITS_ID,
        variables: {
          id: Number(rowData.id),
        },
        fetchPolicy: "no-cache",
      });
      dispatch(setDefaultValues(data.findContact));
      dispatch(setMethod("PUT"));
      navigate("/cs/organization-units-detail");
      setLoading(false);
    };

    const handleClose = () => {
      setOpen(false);
    };
    const handleOpen = async () => {
      const { data } = await client.query({
        query: FIND_ORG_UNITS_ID,
        variables: {
          id: Number(rowData.id),
        },
        fetchPolicy: "no-cache",
      });
      setDataUnit(data.findContact);
      setOpen(true);
    };
    const deleteUnit = async () => {
      await deleteOrganizationalUnit(
        {
          id: rowData.id,
          name: rowData.institution.legalName,
          type: "Unit",
          refresh: refresh,
        },
        dispatch
      );
    };

    return (
      <div>
        <Box component="div" ref={ref} className="-ml-2 flex flex-auto">
          <Tooltip
            arrow
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"View Unit"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              onClick={handleOpen}
              color="primary"
              className="text-green-600"
            >
              <Visibility />
            </IconButton>
          </Tooltip>
          <Tooltip
            arrow
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Edit Unit"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              onClick={handleGoTo}
              color="primary"
              className="text-green-600"
            >
              {loading === true ? <CircularProgress /> : <Edit />}
            </IconButton>
          </Tooltip>

          <Tooltip
            arrow
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Delete Unit"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              onClick={() => {
                deleteUnit();
              }}
              color="primary"
              className="text-green-600"
            >
              <Delete />
            </IconButton>
          </Tooltip>
        </Box>
        <OrganizationUnitView
          dataUnit={dataUnit}
          open={open}
          handleClose={handleClose}
          serviceP={serviceP}
        />
      </div>
    );
  }
);
