import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import Organizations from "./Organizations";

afterEach(cleanup);

test("Organizations is in the DOM", () => {
  render(<Organizations></Organizations>, { wrapper: Wrapper });
});
