import React, { memo } from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import {Core} from "@ebs/styleguide";
const { Checkbox, Typography, Box } = Core;
import NewOrganizationsMenuButton from "components/molecule/NewOrganizationMenuButton";
import { getCoreSystemContext, RBAC } from "@ebs/layout";
import ModifyOrganizationButton from "components/atom/ModifyOrganizationButton/ModifyOrganizationButton";
import DeleteOrganizationButton from "components/atom/DeleteOrganizationButton/DeleteOrganizationButton";

//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const Organizations = React.forwardRef((props, ref) => {
  const { graphqlUri } = getCoreSystemContext();

  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Organization",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Legal Name" />
        </Typography>
      ),
      accessor: "legalName",
      csvHeader: "Legal Name",
      filter: true,
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Slogan" />
        </Typography>
      ),
      accessor: "slogan",
      csvHeader: "Slogan",
      filter: true,
      width: 350,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="WebPage" />
        </Typography>
      ),
      accessor: "webPage",
      csvHeader: "WebPage",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Logo" />
        </Typography>
      ),
      accessor: "logo",
      csvHeader: "Logo",
      disableGlobalFilter: true,
      disableFilters: true,
      width: 200,
      Cell: ({ value }) => {
        if (value === null || undefined || "") {
          return null;
        } else {
          return (
            <img
              src={"data:image/png;base64," + value}
              alt="unsupported format"
              width="50%"
              height="25%"
            />
          );
        }
      },
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization Code" />
        </Typography>
      ),
      accessor: "code",
      csvHeader: "Organization Code",
      filter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization Phone" />
        </Typography>
      ),
      accessor: "phone",
      csvHeader: "Organization Phone",
      filter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organization Tax Id" />
        </Typography>
      ),
      accessor: "taxId",
      csvHeader: "Organization Tax Id",
      hidden: true,
      disableGlobalFilter: true,
      width: 200,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage
            id="none"
            defaultMessage="Organization Default Authentication Id"
          />
        </Typography>
      ),
      accessor: "defaultAuthenticationId",
      csvHeader: "Organization Organization Default Authentication Id",
      hidden: true,
      disableGlobalFilter: true,
      width: 400,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage
            id="none"
            defaultMessage="Organization Default Theme Id"
          />
        </Typography>
      ),
      accessor: "defaultThemeId",
      csvHeader: "Organization Default Theme Id",
      hidden: true,
      disableGlobalFilter: true,
      width: 400,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Active" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Checkbox disabled checked={value} />;
      },
      accessor: "active",
      csvHeader: "Active",
      disableFilters: true,
      disableResizing: true,
      hidden: true,
      disableGlobalFilter: true,
      width: 100,
    },
  ];

  /**
   * @param dataSelection: Array object with data rows selected.
   * @param refresh: Function to refresh Grid data.
   */

  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box>
        <RBAC
          allowedAction={"Create"}
        >
          <NewOrganizationsMenuButton
            selectedRows={selectedRows}
            refresh={refresh}
          />
        </RBAC>
      </Box>
    );
  };

  /**
   * @param rowData: Object with row data.
   * @param refresh: Function to refresh Grid data.
   */
  const rowActions = (rowData, refresh) => {
    return (
      <div
        data-testid={"CustomerRowActionsTestId"}
        ref={ref}
        className="flex flex-row flex-nowrap gap-2"
      >
        <RBAC
          allowedAction={"Modify"}
        >
          <ModifyOrganizationButton rowSelected={rowData} refresh={refresh} />
        </RBAC>
        <RBAC
          allowedAction={"Delete"}
        >
          <DeleteOrganizationButton rowSelected={rowData} refresh={refresh} />
        </RBAC>
      </div>
    );
  };

  /**
   * @prop data-testid: Id to use inside grid.test.js file.
   */
  return (
    <EbsGrid
      id="Organizations"
      toolbar={true}
      columns={columns}
      uri={graphqlUri}
      entity="Organization"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Organizations" />
        </Typography>
      }
      toolbaractions={toolbarActions}
      rowactions={rowActions}
      callstandard="graphql"
      select="multi"
      height="60vh"
    />
  );
});

// Type and required properties
Organizations.propTypes = {};
// Default properties
Organizations.defaultProps = {};

export default memo(Organizations);
