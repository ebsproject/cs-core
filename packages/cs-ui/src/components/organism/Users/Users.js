import React, { memo, useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  contactProcess,
  process,
  CLEAN_CONTACT,
} from "store/modules/DataPersist";
// CORE COMPONENTS AND ATOMS TO USE
import UserRowActions from "components/molecule/UserRowActions";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { getCoreSystemContext } from "@ebs/layout";
import { Core, Icons, Styles, Lab } from "@ebs/styleguide";
import { useQuery } from "@apollo/client";
import { FIND_ORGANIZATION_LIST } from "utils/apollo/gql/tenantManagement";
import { FIND_ROLE_LIST } from "utils/apollo/gql/userManagement";
import { FIND_PRODUCT_LIST } from "utils/apollo/gql/tenant";
const {
  Box,
  Button,
  Tooltip,
  Typography,
  Chip,
  FormControl,
  MenuItem,
  Select,
  OutlinedInput,
  Stack,
} = Core;
const { PostAdd } = Icons;
import { RBAC } from "@ebs/layout";
import Cookies from "js-cookie";
import { EbsFilter } from "@ebs/components";
//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const Users = React.forwardRef(({}, ref) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { graphqlUri } = getCoreSystemContext();

  const cookie = Cookies.get("filters");
  let filterItem;
  if (cookie) filterItem = JSON.parse(cookie);

  const { data, error, loading } = useQuery(FIND_ORGANIZATION_LIST, {
    variables: { data },
    fetchPolicy: "no-cache",
  });
  const { data: dataRole, loading: loadingRole } = useQuery(FIND_ROLE_LIST, {
    variables: { page: { number: 1, size: 100 } },
    fetchPolicy: "no-cache",
  });
  const { data: dataProduct, loading: loadingProduct } = useQuery(
    FIND_PRODUCT_LIST,
    {
      variables: {
        page: { number: 1, size: 100 },
      },
      fetchPolicy: "no-cache",
    }
  );

  const productList = dataProduct?.findProductList?.content;
  const rolesList = dataRole?.findRoleList?.content;
  const organizationList = data?.findOrganizationList?.content;

  const [width, setWidth] = useState(window.innerWidth);
  useEffect(() => {
    const handleResize = () => {
      setWidth(window.innerWidth || 1300);
    };
    window.addEventListener("resize", handleResize);
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, [window.innerWidth]);

  const filterGlobalCookie = () => {
    return [];
  };

  const SelectColumnFilter = ({ column: { filterValue, setFilter } }) => {
    const classes = {
      formControl: {
        minWidth: 110,
      },
      input: {
        padding: "5px 7px",
      },
    };

    return (
      <RBAC
        allowedAction={"Modify"}
        product={"User Management"}
        domain={"Core System"}
      >
        <FormControl variant="outlined" sx={classes.formControl}>
          <Select
            value={filterValue || ""}
            onChange={(e) => {
              let filterData = e.target.value;
              setFilter(filterData.toString());
            }}
            input={<OutlinedInput style={{ width: 110, height: 30 }} />}
            displayEmpty
          >
            <MenuItem value="">All</MenuItem>
            <MenuItem value="true">Active</MenuItem>
            <MenuItem value="false">Disabled</MenuItem>
          </Select>
        </FormControl>
      </RBAC>
    );
  };

  const SelectColumnFilterType = ({ column: { filterValue, setFilter } }) => {
    const classes = {
      formControl: {
        minWidth: 110,
      },
      input: {
        padding: "5px 7px",
      },
    };

    return (
      <RBAC
        allowedAction={"Modify"}
        product={"User Management"}
        domain={"Core System"}
      >
        <FormControl variant="outlined" sx={classes.formControl}>
          <Select
            value={filterValue || ""}
            onChange={(e) => {
              let filterData = e.target.value;
              setFilter(filterData.toString());
            }}
            input={<OutlinedInput style={{ width: 110, height: 30 }} />}
            displayEmpty
          >
            <MenuItem value="">All</MenuItem>
            <MenuItem value="true">Admin</MenuItem>
            <MenuItem value="false">User</MenuItem>
          </Select>
        </FormControl>
      </RBAC>
    );
  };

  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true },
    {
      Header: (
        <RBAC
          allowedAction={"Modify"}
          product={"User Management"}
          domain={"Core System"}
        >
          <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Status" />
          </Typography>
        </RBAC>
      ),
      Cell: ({ value }) => {
        return (
          <RBAC
            allowedAction={"Modify"}
            product={"User Management"}
            domain={"Core System"}
          >
            <div style={{ paddingLeft: "25px" }}>
              <Chip
                size="small"
                color={value ? "primary" : "error"}
                label={value ? "Active" : "Disabled"}
              />
            </div>
          </RBAC>
        );
      },
      Filter: SelectColumnFilter,
      accessor: "isActive",
      csvHeader: "User status",
      width: 150,
      sticky: "left",
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Account Type" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return (
          <RBAC
            allowedAction={"Modify"}
            product={"User Management"}
            domain={"Core System"}
          >
            <div style={{ paddingLeft: "25px" }}>
              <Chip
                size="small"
                color={value ? "primary" : "secondary"}
                label={value ? "Admin" : "User"}
              />
            </div>
          </RBAC>
        );
      },
      Filter: SelectColumnFilterType,
      accessor: "isAdmin",
      csvHeader: "Account Type",
      width: 160,
      sticky: "left",
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="User" />
        </Typography>
      ),
      accessor: "userName",
      csvHeader: "User",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Name" />
        </Typography>
      ),
      accessor: "contact.person.givenName",
      csvHeader: "Name",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Family Name" />
        </Typography>
      ),
      accessor: "contact.person.familyName",
      csvHeader: "Family Name",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Title" />
        </Typography>
      ),
      accessor: "contact.person.jobTitle",
      csvHeader: "Title",
      width: 250,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Knows About" />
        </Typography>
      ),
      accessor: "contact.person.knowsAbout",
      csvHeader: "Knows About",
      width: 250,
      disableGlobalFilter: true,
    },
    {
      Header: "",
      accessor: "contact.id",
      disableGlobalFilter: true,
      hidden: true,
    },
  ];
  /**
   * @param {String} type
   * @param {String} method
   * @param {ID} id: optional
   */
  const handleGoTo = ({ type, method, path }) => {
    dispatch(process({ type: CLEAN_CONTACT }));
    contactProcess({ type, method })(dispatch);
    navigate(`/cs${path}`);
  };
  /**
   * @param dataSelection: Array object with data rows selected.
   * @param refresh: Function to refresh Grid data.
   */
  const toolbarActions = (selectedRows, refresh) => {
    const componentsFilter = [
      {
        id: "organizations",
        nameFilter: "organizations",
        options: organizationList === undefined ? [] : organizationList,
        product: "User Management",
      },
      {
        id: "roles",
        nameFilter: "Roles",
        options: rolesList === undefined ? [] : rolesList,
        product: "User Management",
      },
      {
        id: "product",
        nameFilter: "Product",
        options: productList === undefined ? [] : productList,
        product: "User Management",
      },
    ];

    return (
      <>
        {width > 1300 ? (
          <Box
            sx={{ display: "flex", flexDirection: "row", alignItems: "center" }}
          >
            <RBAC
              allowedAction={"Filter_Toolbar"}
              product={"CRM"}
              domain={"Core System"}
            >
              <EbsFilter componentsFilter={componentsFilter} />
            </RBAC>
            <RBAC
              allowedAction={"Create"}
              product={"User Management"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                title={
                  <Typography className="text-xl font-ebs">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"Add new user"}
                    />
                  </Typography>
                }
              >
                <Button
                  className="w-1/6 bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                  startIcon={<PostAdd className="fill-current text-white" />}
                  onClick={() =>
                    handleGoTo({
                      type: "USER",
                      method: "POST",
                      path: "/contact",
                    })
                  }
                >
                  <Typography className="text-sm font-ebs">
                    <FormattedMessage id={"none"} defaultMessage={"New"} />
                  </Typography>
                </Button>
              </Tooltip>
            </RBAC>
          </Box>
        ) : (
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <RBAC
              allowedAction={"Create"}
              product={"User Management"}
              domain={"Core System"}
            >
              <Tooltip
                arrow
                title={
                  <Typography className="text-xl font-ebs">
                    <FormattedMessage
                      id={"none"}
                      defaultMessage={"Add new user"}
                    />
                  </Typography>
                }
              >
                <Button
                  startIcon={<PostAdd className="fill-current text-white" />}
                  onClick={() =>
                    handleGoTo({
                      type: "USER",
                      method: "POST",
                      path: "/contact",
                    })
                  }
                ></Button>
              </Tooltip>
            </RBAC>
            <RBAC
              allowedAction={"Filter_Toolbar"}
              product={"CRM"}
              domain={"Core System"}
            >
              <EbsFilter componentsFilter={componentsFilter} />
            </RBAC>
          </Box>
        )}
      </>
    );
  };

  /**
   * @prop data-testid: Id to use inside grid.test.js file.
   */
  if (!organizationList || !rolesList || !productList) return null;
  return (
    <EbsGrid
      id="Users"
      toolbar={true}
      columns={columns}
      entity="User"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registers" />
        </Typography>
      }
      uri={graphqlUri}
      toolbaractions={toolbarActions}
      rowactions={UserRowActions}
      csvfilename="userList"
      callstandard="graphql"
      select="multi"
      height="85vh"
      raWidth={110}
      defaultFilters={[{ col: "deleted", mod: "EQ", val: "false" }]}
      globalFilter={filterGlobalCookie()}
      auditColumns
    />
  );
});

// Type and required properties
Users.propTypes = {};
// Default properties
Users.defaultProps = {};

export default memo(Users);
