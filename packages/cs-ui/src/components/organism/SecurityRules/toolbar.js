import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import { securityRuleProcess } from "store/modules/Rules";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS
import { Core, Icons } from "@ebs/styleguide";
const { Box, Button, Tooltip, Typography } = Core;
const { PostAdd } = Icons;
import { RBAC } from "@ebs/layout";

//MAIN FUNCTION
/**
 * @param { }: component properties
 * @param ref: reference made by React.forward
 */
const SecurityRulesToolbar = ({ selectedRows, refresh }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleGo = () => {
    dispatch(securityRuleProcess({ method: "POST" }));
    navigate("/cs/usermanagement/security-rule");
  };
  return (
    <div data-testid={"SecurityRulesToolbarTestId"} className="ml-3">
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          flexDirection: "column",
        }}
      >
        <RBAC
          allowedAction={"Create_Security_Rule"}
          product={"User Management"}
          domain={"Core System"}
        >
          <Tooltip
            arrow
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Add new security rule"}
                />
              </Typography>
            }
          >
            <Button
              onClick={handleGo}
              startIcon={<PostAdd />}
              className="bg-ebs-brand-default hover:bg-ebs-brand-900 border-l-8 text-white"
            >
              <Typography className="text-sm font-ebs">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"New Security Rule"}
                />
              </Typography>
            </Button>
          </Tooltip>
        </RBAC>
      </Box>
    </div>
  );
};

// Type and required properties
SecurityRulesToolbar.propTypes = {
  selectedRows: PropTypes.array,
  refresh: PropTypes.func,
};
// Default properties
SecurityRulesToolbar.defaultProps = {};

export default SecurityRulesToolbar;
