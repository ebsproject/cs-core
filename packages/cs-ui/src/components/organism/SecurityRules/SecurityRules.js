import React, { memo } from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import { getCoreSystemContext } from "@ebs/layout";

import { columns } from "./columns";
import rowActions from "./rowactions";
import SecurityRulesToolbar from "./toolbar";
//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const SecurityRules = React.forwardRef(({}, ref) => {
  const { graphqlUri } = getCoreSystemContext();

  return (
    <EbsGrid
      id="SecurityRules"
      toolbar={true}
      columns={columns}
      uri={graphqlUri}
      entity="SecurityRule"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Security Rules" />
        </Typography>
      }
      rowactions={rowActions}
      toolbaractions={SecurityRulesToolbar}
      csvfilename="SecurityRuleList"
      callstandard="graphql"
      raWidth={110}
      select="multi"
      height="85vh"
      auditColumns
    />
  );
});

// Type and required properties
SecurityRules.propTypes = {};
// Default properties
SecurityRules.defaultProps = {};

export default memo(SecurityRules);
