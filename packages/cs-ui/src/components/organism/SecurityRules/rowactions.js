import React from "react";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
const { Box, IconButton, Tooltip, Typography } = Core;
const { Edit } = Icons;
import { securityRuleProcess } from "store/modules/Rules";
import { RBAC } from "@ebs/layout";
import DeleteSecurityRule from "components/atom/DeleteSecurityRule";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const rowActions = (rowData, refresh) => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const defaultValues = {
    id: rowData.id,
    name: rowData.name,
    description: rowData.description,
    usedByWorkflow: rowData.usedByWorkflow,
  };
  const handleGoTo = () => {
    dispatch(securityRuleProcess({ method: "PUT", defaultValues }));
    navigate("/cs/usermanagement/security-rule");
  };
  return (
    <div>
      {rowData.isSystem === true ? null : (
        <Box className="flex flex-row flex-nowrap gap-2">
          <RBAC
            allowedAction={"Modify_Security_Rule"}
            product={"User Management"}
            domain={"Core System"}
          >
            <Tooltip
              arrow
              title={
                <Typography className="font-ebs text-xl">
                  <FormattedMessage
                    id={"none"}
                    defaultMessage={"Update Security Rule"}
                  />
                </Typography>
              }
            >
              <IconButton onClick={handleGoTo} color="primary">
                <Edit />
              </IconButton>
            </Tooltip>
          </RBAC>
          <RBAC
            allowedAction={"Delete_Security_Rule"}
            product={"User Management"}
            domain={"Core System"}
          >
            <DeleteSecurityRule
              id={rowData.id}
              name={rowData.name}
              refresh={refresh}
            />
          </RBAC>
        </Box>
      )}
    </div>
  );
};
export default rowActions;
