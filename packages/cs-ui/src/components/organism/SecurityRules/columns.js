import React from "react";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
const { Typography } = Core;
const {  Cancel, CheckCircle } = Icons;

export const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Security Rule" />
        </Typography>
      ),
      csvHeader: "Security Rule",
      accessor: "name",
      width: 250,
    },
    {
        Header: (
          <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Description" />
          </Typography>
        ),
        csvHeader: "Description",
        accessor: "description",
        width: 300,
      },
    {
        Header: (
          <Typography variant="h5" color="primary">
            <FormattedMessage id="none" defaultMessage="Used By Workflow" />
          </Typography>
        ),
        accessor: "usedByWorkflow",
        csvHeader: "Used By Workflow",
        width: 200,
        Cell: ({ value }) => {
          return value ? (
            <div style={{ paddingLeft: "150%" }}>
              <CheckCircle color="primary" fontSize="medium" />
            </div>
          ) : (
            <div style={{ paddingLeft: "150%" }}>
              <Cancel color="error" />
            </div>
          );
        },
      },
  ];