import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { client } from "utils/apollo";
import { EbsGrid } from "@ebs/components";
import FieldDetailMolecule from "components/molecule/FieldDetail/FieldDetail";
import { FIND_FACILITY_LIST } from "utils/apollo/gql/placeManager";
import FacilityToolbarActions from "components/molecule/FacilityToolbarActions/FacilityToolbarActions";
import FacilityRowActionsMolecule from "components/molecule/FacilityRowActions/FacilityRowActions";
import { clientCb } from "utils/apollo/apollo";
import FieldDetailFacilityMolecule from "components/molecule/DetailSubFacility/FieldDetailFacility";
const { Typography } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const FacilitiesOrganism = React.forwardRef(({}, ref) => {
  const columns = [
    { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "facilityName",
      csvHeader: "Facility",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "facilityCode",
      csvHeader: "Facility Code",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Facility Type" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "facilityType",
      csvHeader: "Facility Type",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Site" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "geospatialObject.geospatialObjectName",
      csvHeader: "Site",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Sub Facility" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "subFacility",
      csvHeader: "subFacility",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Sub Facility Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "subFacilityCode",
      csvHeader: "subFacilityCode",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Sub Facility Type" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "subFacilityType",
      csvHeader: "subFacilityType",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Container" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "container",
      csvHeader: "container",
      width: 200,
      filter: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Container Code" />
        </Typography>
      ),
      Cell: ({ value }) => {
        return <Typography variant="body1">{value}</Typography>;
      },
      accessor: "containerCode",
      csvHeader: "containerCode",
      width: 200,
      filter: true,
    },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Date Created" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "dateCreated",
    //   csvHeader: "Data Created",
    //   width: 200,
    //   filter: true,
    // },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Coordinates" />
    //     </Typography>
    //   ),
    //   Cell: ({ value }) => {
    //     return <Typography variant="body1">{value}</Typography>;
    //   },
    //   accessor: "coordinates.features[0].geometry.coordinates",
    //   csvHeader: "Coordinates",
    //   width: 200,
    //   filter: true,
    // },
  ];

  const fetch = async ({ page, sort, filters }) => {
    if (filters.length === 0) {
      return new Promise((resolve, reject) => {
        try {
          clientCb
            .query({
              query: FIND_FACILITY_LIST,
              variables: {
                sort: [
                  { col: "updatedOn", mod: "DES" },
                  { col: "createdOn", mod: "DES" },
                ],
                page: page,
                filters: [
                  ...filters,
                  {
                    col: "parentFacility",
                    mod: "NULL",
                    val: "",
                  },
                ],
                disjunctionFilters: false,
              },
              fetchPolicy: "no-cache",
            })
            .then(({ data, errors }) => {
              if (errors) {
                reject(errors);
              } else {
                resolve({
                  data: data.findFacilityList.content,
                  pages: data.findFacilityList.totalPages,
                  elements: data.findFacilityList.totalElements,
                });
              }
            });
        } catch (error) {
          reject(error);
        }
      });
    } else {
      switch (filters[0].col) {
        case "subFacility":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "facilityName",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findFacilityList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.facilityType,
                        subFacilityType: item.facilityType,
                        subFacility: item.facilityName,
                        subFacilityCode: item.facilityCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        // altitude: item.altitude,
                      })
                    );
                    resolve({
                      // data: data.findFacilityList.content,
                      data: arrayList,
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "subFacilityCode":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "facilityCode",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findFacilityList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.facilityType,
                        subFacilityType: item.facilityType,
                        subFacility: item.facilityName,
                        subFacilityCode: item.facilityCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        // altitude: item.altitude,
                      })
                    );
                    resolve({
                      data: arrayList,
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "subFacilityType":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "facilityType",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findFacilityList.content.map(
                      (item) => ({
                        id: item.id,
                        geospatialObjectType: item.facilityType,
                        subFacilityType: item.facilityType,
                        subFacility: item.facilityName,
                        subFacilityCode: item.facilityCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        // altitude: item.altitude,
                      })
                    );
                    resolve({
                      data: arrayList,
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "container":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "facilityClass",
                        mod: "EQ",
                        val: "container",
                      },
                      {
                        col: "facilityName",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findFacilityList.content.map(
                      (item) => ({
                        id: item.id,
                        // geospatialObjectType: item.facilityType,
                        // subFacilityType: item.facilityType,
                        container: item.facilityName,
                        containerCode: item.facilityCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        // altitude: item.altitude,
                      })
                    );
                    resolve({
                      data: arrayList,
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        case "containerCode":
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    page: page,
                    filters: [
                      {
                        col: "facilityClass",
                        mod: "EQ",
                        val: "container",
                      },
                      {
                        col: "facilityCode",
                        mod: "LK",
                        val: filters[0].val,
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    let arrayList = data.findFacilityList.content.map(
                      (item) => ({
                        id: item.id,
                        // geospatialObjectType: item.facilityType,
                        // subFacilityType: item.facilityType,
                        container: item.facilityName,
                        containerCode: item.facilityCode,
                        description: item.description,
                        dateCreated: item.dateCreated,
                        coordinates: item.coordinates,
                        // altitude: item.altitude,
                      })
                    );
                    resolve({
                      data: arrayList,
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
        default:
          return new Promise((resolve, reject) => {
            try {
              clientCb
                .query({
                  query: FIND_FACILITY_LIST,
                  variables: {
                    sort: [
                      { col: "updatedOn", mod: "DES" },
                      { col: "createdOn", mod: "DES" },
                    ],
                    page: page,
                    filters: [
                      ...filters,
                      {
                        col: "parentFacility",
                        mod: "NULL",
                        val: "",
                      },
                    ],
                    disjunctionFilters: false,
                  },
                  fetchPolicy: "no-cache",
                })
                .then(({ data, errors }) => {
                  if (errors) {
                    reject(errors);
                  } else {
                    resolve({
                      data: data.findFacilityList.content,
                      pages: data.findFacilityList.totalPages,
                      elements: data.findFacilityList.totalElements,
                    });
                  }
                });
            } catch (error) {
              reject(error);
            }
          });
      }
    }
  };

  const DetailComponent = ({ rowData }) => {
    return <FieldDetailFacilityMolecule rowData={rowData} />;
  };

  return (
    /* 
     @prop data-testid: Id to use inside facilities.test.js file.
     */
    <EbsGrid
      id="Facilities"
      toolbar={true}
      data-testid={"FacilitiesTestId"}
      columns={columns}
      toolbaractions={FacilityToolbarActions}
      rowactions={FacilityRowActionsMolecule}
      csvfilename="FacilitiesList"
      detailcomponent={DetailComponent}
      fetch={fetch}
      height="85vh"
      select="multi"
    />
  );
});
// Type and required properties
FacilitiesOrganism.propTypes = {};
// Default properties
FacilitiesOrganism.defaultProps = {};

export default FacilitiesOrganism;
