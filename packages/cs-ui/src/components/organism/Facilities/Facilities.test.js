import Facilities from './Facilities';
import React from 'react'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Facilities is in the DOM', () => {
  render(<Facilities></Facilities>)
  expect(screen.getByTestId('FacilitiesTestId')).toBeInTheDocument();
})
