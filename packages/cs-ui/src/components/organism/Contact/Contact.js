import React from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import { EbsTabsLayout, Core } from "@ebs/styleguide";
const { Box } = Core;
import ContactForm from "components/molecule/ContactForm";
// import { ContactContextProvider } from "components/molecule/ContactForm/contact-context";
// import { useLayoutContext } from "@ebs/layout";

//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const ContactOrganism = React.forwardRef(({}, ref) => {
  // const { setFilterGridToApply} = useLayoutContext();
  const { title, type, method } = useSelector(({ persist }) => persist);

  // setFilterGridToApply([]);

  return (
    /**
     * @prop data-testid: Id to use inside contact.test.js file.
     */
    <div ref={ref} data-testid={"ContactTestId"}>
      <EbsTabsLayout
        orientation={"horizontal"}
        tabs={[
          {
            label: <FormattedMessage id={"none"} defaultMessage={title} />,
            component: <ContactForm type={type} method={method} />,
          },
        ]}
      />
    </div>
  );
});
// Type and required properties
ContactOrganism.propTypes = {};
// Default properties
ContactOrganism.defaultProps = {};

export default ContactOrganism;
