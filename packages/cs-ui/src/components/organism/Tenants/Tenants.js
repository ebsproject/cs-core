import React from "react";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Box, Typography } = Core;
import ModifyTenant from "components/molecule/ModifyTenantButton";
import { getCoreSystemContext, RBAC } from "@ebs/layout";
import NewTenantMenuButton from "components/molecule/NewTenantMenuButton";

const Tenants = () => {
  const { graphqlUri } = getCoreSystemContext();
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    { Header: "sync", accessor: "sync", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Registration ID" />
        </Typography>
      ),
      csvHeader: "Registration ID",
      accessor: "name",
      width: 450,
    },
    {
      Header: "CustomerId",
      accessor: "customer.id",
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Center" />
        </Typography>
      ),
      csvHeader: "Customer",
      accessor: "customer.name",
      disableGlobalFilter: true,
      width: 450,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Member of" />
        </Typography>
      ),
      csvHeader: "Organization",
      accessor: "organization.name",
      width: 450,
    },
  ];


  const rowActions = (rowData, refresh) => {
    return (
      <Box>
        <Box>
          <RBAC
            allowedAction={"Modify"}
          >
            <ModifyTenant rowData={rowData} refresh={refresh} />
          </RBAC>
        </Box>
      </Box>
    );
  };

  return (
    <EbsGrid
      id="Tenants"
      toolbar={true}
      columns={columns}
      entity="Tenant"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage
            id="none"
            defaultMessage="System Instances Managed by "
          />
        </Typography>
      }
      uri={graphqlUri}
      select="multi"
      //toolbaractions={toolbarActions}
      rowactions={rowActions}
      raWidth={55}
      callstandard="graphql"
      height="65vh"
      auditColumns
    />
  );
};

export default Tenants;
