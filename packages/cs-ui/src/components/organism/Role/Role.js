import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { EbsTabsLayout } from "@ebs/styleguide";
import { useSelector } from "react-redux";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
// CORE COMPONENTS AND MOLECULES TO USE
import RoleForm from "components/atom/RoleForm";
import {Core} from "@ebs/styleguide";
const { Box}= Core;

//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const RoleOrganism = React.forwardRef(({}, ref) => {
  const { title, method, defaultValues } = useSelector(
    ({ persist }) => persist
  );
  return (
    /**
     * @prop data-testid: Id to use inside role.test.js file.
     */
    <div data-testid={"RoleTestId"} ref={ref}>
      <EbsTabsLayout
        orientation={"horizontal"}
        tabs={[
          {
            label: <FormattedMessage id={"none"} defaultMessage={title} />,
            component: (
              <RoleForm method={method} defaultValues={defaultValues} />
            ),
          },
        ]}
      />
    </div>
  );
});
// Type and required properties
RoleOrganism.propTypes = {};

export default RoleOrganism;
