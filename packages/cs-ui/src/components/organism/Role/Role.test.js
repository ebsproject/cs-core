import Role from "./Role";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";

let translation={
  none:"test"
}

afterEach(cleanup);
test('Role is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <Role></Role>
       </IntlProvider>
    </Provider>
  )
  expect(screen.getByTestId('RoleTestId')).toBeInTheDocument();
})