import React, { Fragment, memo } from "react";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
const { Box, IconButton, Tooltip, Typography } = Core;
const { Edit, AccountCircle, Computer } = Icons;
import { getCoreSystemContext } from "@ebs/layout";
import RolesToolbar from "components/molecule/RolesToolbar";
import { roleProcess } from "store/modules/DataPersist";
import DeleteRole from "components/atom/DeleteRole";
import { RBAC } from "@ebs/layout";
//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const Roles = React.forwardRef(({}, ref) => {
  const { graphqlUri } = getCoreSystemContext();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="System" />
        </Typography>
      ),
      accessor: "isSystem",
      csvHeader: "isSystem",
      width: 100,
      Cell: ({ value }) => {
        return value === true ? (
          <div style={{ paddingLeft: "150%" }}>
            <Computer color="primary" />
          </div>
        ) : (
          <div style={{ paddingLeft: "150%" }}>
            <AccountCircle color="primary" />
          </div>
        );
      },
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Role" />
        </Typography>
      ),
      csvHeader: "Role",
      accessor: "name",
      width: 500,
    },
    {
      Header: "prodFn",
      accessor: "productfunctions.id",
      disableGlobalFilter: true,
      hidden: true,
      Cell: ({ value }) => <Fragment />,
    },
    {
      Header: "Rules",
      accessor: "rules.id",
      disableGlobalFilter: true,
      hidden: true,
      Cell: ({ value }) => <Fragment />,
    },
    {
      Header: "prodFn",
      accessor: "description",
      disableGlobalFilter: true,
      hidden: true,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="AD Security Group" />
        </Typography>
      ),
      csvHeader: "AD Security Group",
      accessor: "securityGroup",
      filter: true,
      width: 500,
    },
  ];

  /**
   * @param rowData: Object with row data.
   * @param refresh: Function to refresh Grid data.
   */
  const rowActions = (rowData, refresh) => {
    const defaultValues = {
      id: rowData.id,
      name: rowData.name,
      description: rowData.description,
      securityGroup: rowData.securityGroup,
      isSystem: rowData.isSystem,
      rules: rowData.rules || [],
      productFunction: {
        checked: [],
        expanded: [],
      },
    };
    rowData?.productfunctions?.map((item) =>
      defaultValues.productFunction.checked.push(item.id)
    );
    const handleGoTo = () => {
      dispatch(roleProcess({ method: "PUT", defaultValues }));
      navigate("/cs/usermanagement/role");
    };
    return (
      <div>
        <Box className="flex flex-row flex-nowrap gap-2">
          <RBAC
            allowedAction={"Modify_Role"}
            product={"User Management"}
            domain={"Core System"}
          >
            <Tooltip
              arrow
              title={
                <Typography className="font-ebs text-xl">
                  <FormattedMessage
                    id={"none"}
                    defaultMessage={"Update Role"}
                  />
                </Typography>
              }
            >
              <IconButton
                onClick={handleGoTo}
                color="primary"
                //className="text-green-600"
              >
                <Edit />
              </IconButton>
            </Tooltip>
          </RBAC>
          <RBAC
            allowedAction={"Delete_Role"}
            product={"User Management"}
            domain={"Core System"}
          >
            <DeleteRole
              id={rowData.id}
              name={rowData.name}
              refresh={refresh}
              isSystem={rowData.isSystem}
            />
          </RBAC>
        </Box>
      </div>
    );
  };

  /**
   * @prop data-testid: Id to use inside grid.test.js file.
   */
  return (
    <EbsGrid
      id="Roles"
      toolbar={true}
      columns={columns}
      uri={graphqlUri}
      entity="Role"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Roles" />
        </Typography>
      }
      rowactions={rowActions}
      toolbaractions={RolesToolbar}
      csvfilename="roleList"
      callstandard="graphql"
      raWidth={110}
      select="multi"
      height="85vh"
      auditColumns
    />
  );
});

// Type and required properties
Roles.propTypes = {};
// Default properties
Roles.defaultProps = {};

export default memo(Roles);
