import React, { memo } from "react";
import PropTypes from "prop-types";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Typography, Box } = Core;
import NewCustomerMenuButton from "components/molecule/NewCustomerMenuButton";
import { getCoreSystemContext, RBAC } from "@ebs/layout";
import DeleteCustomerButton from "components/atom/DeleteCustomerButton";
import ModifyCustomerButton from "components/atom/ModifyCustomerButton";

//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const Customers = React.forwardRef((props, ref) => {
  const { graphqlUri } = getCoreSystemContext();

  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Center" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Name",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Contact Email" />
        </Typography>
      ),
      accessor: "officialEmail",
      csvHeader: "Contact Mail",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Alternate Email" />
        </Typography>
      ),
      hidden: true,
      accessor: "alternateEmail",
      csvHeader: "Alternate Email",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Contact Title" />
        </Typography>
      ),
      accessor: "jobTitle",
      csvHeader: "Title",
      filter: true,
      width: 300,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Logo" />
        </Typography>
      ),
      accessor: "logo",
      csvHeader: "Logo",
      disableGlobalFilter: true,
      disableFilters: true,
      width: 300,
      Cell: ({ value }) => {
        if (value === null || undefined || "") {
          return null;
        } else {
          return (
            <img
              src={"data:image/png;base64," + value}
              alt="unsupported format"
              width="50%"
              height="25%"
            />
          );
        }
      },
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Phone" />
        </Typography>
      ),
      accessor: "phone",
      csvHeader: "Phone",
      disableGlobalFilter: true,
      disableFilters: true,
      filter: false,
      width: 300,
    },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Phone Extension" />
    //     </Typography>
    //   ),
    //   accessor: "phoneExtension",
    //   csvHeader: "phoneExtension",
    //   disableGlobalFilter: true,
    //   disableFilters: true,
    //   width: 300,
    // },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Owning Organization Id" />
    //     </Typography>
    //   ),
    //   hidden: true,
    //   accessor: "owningOrganizationId",
    //   csvHeader: "owningOrganizationId",
    //   filter: false,
    //   width: 300,
    // },
    // {
    //   Header: (
    //     <Typography variant="h5" color="primary">
    //       <FormattedMessage id="none" defaultMessage="Language Preference" />
    //     </Typography>
    //   ),
    //   accessor: "languagePreference",
    //   csvHeader: "languagePreference",
    //   disableGlobalFilter: true,
    //   disableFilters: true,
    //   width: 300,
    // },
  ];

  /*
    @param dataSelection: Array object with data rows selected.
    @param refresh: Function to refresh Grid data.
*/
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box>
        <RBAC
          allowedAction={"Create"}
        >
          <NewCustomerMenuButton
            selectedRows={selectedRows}
            refresh={refresh}
          />
        </RBAC>
      </Box>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <div
        data-testid={"CustomerRowActionsTestId"}
        ref={ref}
        className="flex flex-row flex-nowrap gap-2"
      >
        <RBAC
          allowedAction={"Modify"}
        >
          <ModifyCustomerButton rowSelected={rowData} refresh={refresh} />
        </RBAC>
        <RBAC
          allowedAction={"Delete"}
        >
          <DeleteCustomerButton rowSelected={rowData} refresh={refresh} />
        </RBAC>
      </div>
    );
  };

  /* 
    @prop data-testid: Id to use inside grid.test.js file.
*/
  return (
    <div data-testid={"CustomerTestId"}>
      <EbsGrid
        id="Customers"
        toolbar={true}
        columns={columns}
        uri={graphqlUri}
        entity="Customer"
        title={
          <Typography variant="h5" color="primary">
            <FormattedMessage
              id="none"
              defaultMessage="Centers (CG and Non-CG)"
            />
          </Typography>
        }
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        select="multi"
        callstandard="graphql"
        height="60vh"
        auditColumns
      />
    </div>
  );
});

// Type and required properties
Customers.propTypes = {};
// Default properties
Customers.defaultProps = {};

export default Customers;
