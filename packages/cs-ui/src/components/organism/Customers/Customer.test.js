import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import Customer from "./Customers";

afterEach(cleanup);

test("Customer is in the DOM", () => {
  render(<Customer></Customer>, { wrapper: Wrapper });
  expect(screen.getByTestId("CustomerTestId")).toBeInTheDocument();
});
