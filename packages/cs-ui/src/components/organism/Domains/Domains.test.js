import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import Domains from "./Domains";

afterEach(cleanup);

test("Domains is in the DOM", () => {
  render(<Domains></Domains>, { wrapper: Wrapper });
});
