import React from "react";
// CORE COMPONENTS AND ATOMS TO USE
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Box, Typography } = Core;
import { getCoreSystemContext, RBAC } from "@ebs/layout";
import ProductList from "components/molecule/ProductList";
import NewDomainsMenuButton from "components/molecule/NewDomainsMenuButton";

const Domains = () => {
  const { graphqlUri } = getCoreSystemContext();
  // Columns
  const columns = [
    { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Module" />
        </Typography>
      ),
      accessor: "name",
      csvHeader: "Name",
      filter: true,
      width: 650,
    },
    {
      Header: (
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Description" />
        </Typography>
      ),
      accessor: "info",
      csvHeader: "Description",
      filter: true,
      width: 650,
    }
  ];

  /*
     @param dataSelection: Array object with data rows selected.
     @param refresh: Function to refresh Grid data.
 */
  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box>
        <Box>
          <RBAC allowedAction={"Create"} >
            <NewDomainsMenuButton
              rowSelected={(selectedRows.length > 0 && selectedRows[0]) || null}
              refresh={refresh}
            />
          </RBAC>

        </Box>
      </Box>
    );
  };

  /*
    @param rowData: Object with row data.
    @param refresh: Function to refresh Grid data.
*/
  const rowActions = (rowData, refresh) => {
    return (
      <IconButton
        title="title"
        onClick={() => {
          alert(JSON.stringify(rowData));
          refresh();
        }}
        color="inherit"
      >
        <AddIcon />
      </IconButton>
    );
  };

  const ProductDetail = ({ rowData }) => {
    return <ProductList rowData={rowData} />;
  };

  return (
    <EbsGrid
      id="Domains"
      data-testid="DomainsTestId"
      toolbar={true}
      columns={columns}
      uri={graphqlUri}
      entity="Domain"
      title={
        <Typography variant="h5" color="primary">
          <FormattedMessage id="none" defaultMessage="Modules" />
        </Typography>
      }
      toolbaractions={toolbarActions}
      detailcomponent={ProductDetail}
      callstandard="graphql"
      select="multi"
      height="60vh"
      auditColumns
    />
  );
};

export default Domains;
