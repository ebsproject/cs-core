import React from "react";
import { Core } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
const { Typography } = Core;
import { FIND_UNITS_LIST } from "utils/apollo/gql/crm";
import { EbsGrid } from "@ebs/components";
import columns from "./colDefinition";
import { useSelector } from "react-redux";
import toolbarActions from "./toolbar";
import rowActionsUnits from "./rowActions";
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const UnitOrganism = React.forwardRef((props, ref) => {
  // Properties of the organism
  // const { label, children, ...rest } = props
  const { title, type, method, institutionId } = useSelector(
    ({ persist }) => persist
  );
  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        client
          .query({
            query: FIND_UNITS_LIST,
            variables: {
              page: page,
              sort: sort,
              filters: [
                ...filters,
                { col: "category.id", mod: "EQ", val: "4" },
                {
                  col: "contactHierarchies.institution.id",
                  mod: "EQ",
                  val: institutionId?.id,
                },
              ],
              disjunctionFilters: filters.length > 0 ? true : false,
            },
            fetchPolicy: "no-cache",
          })
          .then(({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              resolve({
                pages: data.findContactList.totalPages,
                elements: data.findContactList.totalElements,
                data: data.findContactList.content,
              });
            }
          });
      } catch (e) {
        console.log(e);
      } finally {
      }
    });
  };

  return (
    <div data-testid="unitTestId">
      <EbsGrid
        toolbar={true}
        toolbaractions={toolbarActions}
        rowactions={rowActionsUnits}
        columns={columns}
        title={
          <Typography className="font-ebs text-ebs-green-default text-lg">
            <FormattedMessage
              id={"none"}
              defaultMessage={`Units of ${institutionId?.legalName}`}
            />
          </Typography>
        }
        fetch={fetch}
        csvfilename="UnitList"
        select="single"
        height="100vh"
      />
    </div>
  );
});
// Type and required properties
UnitOrganism.propTypes = {};
// Default properties
UnitOrganism.defaultProps = {};

export default UnitOrganism;
