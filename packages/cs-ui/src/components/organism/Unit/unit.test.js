import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import Unit from './unit'
// Test Library
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
import { Provider } from 'react-redux';
import { store } from '../../../store';
import { IntlProvider } from "react-intl";

let translation={
  none:"test"
}
afterEach(cleanup)


test('Unit is in the DOM', () => {
  render(
    <Provider store={store}>
       <IntlProvider locale="es" messages={translation} defaultLocale="en">
       <Unit></Unit>
       </IntlProvider>
    </Provider>


  )
  expect(screen.getByTestId('unitTestId')).toBeInTheDocument();
})
