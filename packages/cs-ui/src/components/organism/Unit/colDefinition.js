
import { Core, DropZone, Icons } from "@ebs/styleguide";
const { Typography } = Core;
import { FormattedMessage } from "react-intl";
const columns = [
  { Header: "Id", accessor: "id", hidden: true, disableGlobalFilter: true },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Abbreviation" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return <Typography variant="body1">{value}</Typography>;
    },
    accessor: "institution.commonName",
    csvHeader: "Abbreviation",
    width: 500,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Unit" />
      </Typography>
    ),
    Cell: ({ value }) => {
      return <Typography variant="body1">{value}</Typography>;
    },
    accessor: "institution.legalName",
    csvHeader: "Unit Name",
    width: 500,
  }
];

export default columns;