import React, { useEffect, useState } from "react";
import { Core, Icons } from "@ebs/styleguide";
const { PostAdd, KeyboardArrowLeft } = Icons;
const { Tooltip, Button, Typography, DialogActions } = Core;
import { FormattedMessage } from "react-intl";
import UnitForm from "components/molecule/UnitForm";
import { useNavigate } from "react-router-dom";
import { RBAC } from "@ebs/layout";

const toolbarActions = (selectedRows, refresh) => {
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = (value) => {
    setOpen(false);
    refresh();
    navigate("/cs/units");
  };

  return (
    <div>
      <DialogActions>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id={"none"} defaultMessage={"Back to CRM"} />
            </Typography>
          }
        >
          <Button
            className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
            startIcon={
              <KeyboardArrowLeft className="fill-current text-white" />
            }
            onClick={() => navigate("/cs/crm")}
          >
            <Typography className="text-white text-sm font-ebs">
              <FormattedMessage id="none" defaultMessage="Back" />
            </Typography>
          </Button>
        </Tooltip>
        <RBAC allowedAction={"New_Institution"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"New Unit"} />
              </Typography>
            }
          >
            <Button
              className="bg-ebs-brand-default hover:bg-ebs-brand-900  text-white"
              aria-label="add-unit"
              startIcon={<PostAdd className="fill-current text-white" />}
              onClick={() => handleClickOpen()}
            >
              <Typography className="text-white text-sm font-ebs">
                <FormattedMessage id="none" defaultMessage="New Unit" />
              </Typography>
            </Button>
          </Tooltip>
        </RBAC>

        <UnitForm open={open} onClose={handleClose} />
      </DialogActions>
    </div>
  );
};

export default toolbarActions;
