import React,{ useState} from 'react'
import {RBAC} from "@ebs/layout";
import { Core, Icons } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch } from 'react-redux';
import DeleteContact from "components/atom/DeleteContact";
import UnitForm from "components/molecule/UnitForm";
import { findServiceProviderById } from 'store/modules/SM';

const {
  IconButton,
  Tooltip,
  Typography,
  } = Core;
const { Edit} = Icons;

  const rowActionsUnits = React.forwardRef(({rowData, refresh},ref) => {
    const { type } = useSelector(({ persist }) => persist);
    const [open, setOpen] = React.useState(false);
    const dispatch = useDispatch();
    const handleClickOpen = async () => {
     setOpen(true);
    };

    const handleClose = (value) => {
      setOpen(false);
    };

    return (
    <div className="ml-1 flex flex-auto">  
    <RBAC allowedAction={"Modify"}>    
      <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Edit"} />
              </Typography>
            }
          >
            <IconButton
              size="small"
              color="primary"
              onClick={handleClickOpen}
            >
              <Edit />
            </IconButton>
      </Tooltip> 
      </RBAC>  
        <UnitForm open={open} onClose={handleClose} rowData={rowData} metodo={"PUT"} refresh={refresh}/>
    <RBAC allowedAction={"Delete"}>
      <Tooltip
        arrow
        placement="top"       
      >    
        <DeleteContact contactId={rowData.id} refresh={refresh} type={type}/>
      </Tooltip>
    </RBAC>      
    </div>          
    );
  });

  export default rowActionsUnits;