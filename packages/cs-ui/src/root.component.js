import React, { useState } from "react";
import { Provider } from "react-redux";
import { ApolloProvider } from "@apollo/client";
import {
  BrowserRouter as Router,
  Route,
  Routes,
  Outlet,
} from "react-router-dom";
import { IntlProvider } from "react-intl";
import { PersistGate } from "redux-persist/integration/react";
import { client } from "utils/apollo";
import Message from "components/atom/Message";
import TenantManagement from "pages/TenantManagementView";
import ContactRM from "pages/CRM";
import Contact from "components/organism/Contact";
import Role from "components/organism/Role";
import Units from "components/organism/Unit";
import EmailTemplates from "pages/EmailTemplates";
import OrganizationUnits from "pages/OrganizationUnits";
import OrganizationUnitsDetail from "components/organism/OrganizationUnits/OrganizationUnitsDetail";
// Routes
import {
  BASE_PATH,
  CONTACT,
  TENANT_MANAGEMENT,
  CRM,
  USER_MANAGEMENT,
  ROLE,
  UNIT,
  EMAIL_TEMPLATE,
  TEMPLATE_EDITOR,
  PLACE_MANAGER,
  ORGANIZATION_UNITS,
  ORGANIZATION_UNITS_DETAIL,
  CREATE_PLACE_MANAGER,
  VIEW_FORM_PLACES,
  CREATE_FACILITY,
  VIEW_FORM_FACILITIES,
  SECURITY_RULE,
} from "./routes";
import UserManagementView from "pages/UserManagement";
import TemplateEditor from "components/EmailTemplate/templateEditor";
import PlaceManager from "pages/PlaceManager";
import PlaceManagerCreate from "components/molecule/PlaceManagerCreate";
import PlaceManagerModifyView from "components/molecule/PlaceManagerModify/PlaceManagerModify";
import FacilityModuleCreateView from "components/molecule/FacilityModuleCreate/FacilityModuleCreate";
import FacilityModuleModifyView from "components/molecule/FacilityModuleModify/FacilityModuleModify";
import SecurityRule from "components/organism/SecurityRule";
import store, { persister } from "store";
import { ContactContextProvider } from "corecontext";

function MainCoreSystem() {
  return (
    <div>
      <Outlet />
    </div>
  );
}
const App = React.forwardRef(({}, ref) => {
  const [locale, setLocale] = useState("en");
  const [messages, setMessages] = useState(null);

  return (
    <Provider store={store}>
      <PersistGate loading={<>Loading...</>} persistor={persister}>
        <ApolloProvider client={client}>
          <IntlProvider locale={locale} messages={messages} defaultLocale="en">
            <Message />
            <ContactContextProvider>
              <Router>
                <Routes>
                  <Route path={BASE_PATH} element={<MainCoreSystem />}>
                    <Route
                      path={ORGANIZATION_UNITS}
                      element={<OrganizationUnits />}
                    />
                    <Route
                      path={ORGANIZATION_UNITS_DETAIL}
                      element={<OrganizationUnitsDetail />}
                    />
                    <Route path={EMAIL_TEMPLATE} element={<EmailTemplates />} />
                    <Route
                      path={TEMPLATE_EDITOR}
                      element={<TemplateEditor />}
                    />
                    <Route
                      path={TENANT_MANAGEMENT}
                      element={<TenantManagement />}
                    />
                    <Route
                      path={USER_MANAGEMENT}
                      element={<UserManagementView />}
                    />
                    <Route path={CRM} element={<ContactRM />} />
                    <Route path={CONTACT} element={<Contact />} />
                    <Route path={UNIT} element={<Units />} />
                    <Route path={ROLE} element={<Role />} />
                    <Route path={SECURITY_RULE} element={<SecurityRule />} />
                    <Route path={PLACE_MANAGER} element={<PlaceManager />} />
                    <Route
                      path={CREATE_PLACE_MANAGER}
                      element={<PlaceManagerCreate />}
                    />
                    <Route
                      path={VIEW_FORM_PLACES}
                      element={<PlaceManagerModifyView />}
                    />
                    <Route
                      path={CREATE_FACILITY}
                      element={<FacilityModuleCreateView />}
                    />
                    <Route
                      path={VIEW_FORM_FACILITIES}
                      element={<FacilityModuleModifyView />}
                    />
                  </Route>
                </Routes>
              </Router>
            </ContactContextProvider>
          </IntlProvider>
        </ApolloProvider>
      </PersistGate>
    </Provider>
  );
});

export default App;
