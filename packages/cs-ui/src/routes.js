export const BASE_PATH = "/cs";
export const TENANT_MANAGEMENT = "global-settings";
export const CRM = "crm";
export const COMING_SOON = "coming-soon";
export const CONTACT = "contact";
export const USER_MANAGEMENT = "usermanagement";
export const ROLE = "usermanagement/role";
export const PRODUCTS = "products";
export const UNIT = "units"
export const EMAIL_TEMPLATE = "email-template";
export const TEMPLATE_EDITOR = "template-editor";
export const ORGANIZATION_UNITS = "organization-units";
export const ORGANIZATION_UNITS_DETAIL = "organization-units-detail"
export const ORGANIZATION_UNITS_TEAMS = "organization-units-teams"
export const PLACE_MANAGER = "place-manager";
export const CREATE_PLACE_MANAGER = "place-manager/create";
export const VIEW_FORM_PLACES = "place-manager/view";
export const VIEW_FORM_FACILITIES = "place-manager/view-facility";
export const CREATE_FACILITY = "place-manager/create-facility";
export const SECURITY_RULE = "usermanagement/security-rule";