import { createContext, useState, useContext } from "react";

const ContactContext = createContext();

export function ContactContextProvider({ children }) {
  const [refresh, setRefresh] = useState(false);

  return (
    <ContactContext.Provider value={{ refresh, setRefresh }}>
      {children}
    </ContactContext.Provider>
  );
}

export function useContactContext() {
  return useContext(ContactContext);
}
