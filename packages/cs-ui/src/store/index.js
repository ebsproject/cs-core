import { configureStore } from "@reduxjs/toolkit";
import {thunk} from "redux-thunk";
import { persistStore, persistReducer } from "redux-persist";
import sessionStorage from "redux-persist/lib/storage/session"; // defaults to localStorage for web
import reducers from "./reducers";
import {
  findContactTypeList,
  findCountryList,
  findProductFunctionList,
} from "./modules/DataPersist";


const persistConfig = {
  key: "core",
  whitelist: ["persist", "emailTemplate"],
  storage: sessionStorage,
};

const persistedReducer = persistReducer(persistConfig, reducers);

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ['persist/PERSIST', 'persist/REHYDRATE'],
      },
    }).concat(thunk),
  devTools: process.env.NODE_ENV !== 'production',

});


store.dispatch(findContactTypeList());
store.dispatch(findCountryList());
store.dispatch(findProductFunctionList());
export default store;
export const persister = persistStore(store);
