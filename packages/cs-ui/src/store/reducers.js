import { combineReducers } from "redux";
import tenant from "store/modules/TenantManagement";
import um from "store/modules/UserManagement";
import crm from "store/modules/CRM";
import message from "store/modules/message";
import persist from "store/modules/DataPersist";
import sm from "store/modules/SM";
import placemanager from "store/modules/PlaceManager";
import emailTemplate from "store/modules/EmailTemplate";
import orgUnit from "store/modules/OrganizationUnit";
import rules from "./modules/Rules";
export default combineReducers({
  rules,
  message,
  tenant,
  crm,
  persist,
  um,
  sm,
  emailTemplate,
  orgUnit,
  placemanager,
});
