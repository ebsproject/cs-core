import { client } from "utils/apollo";
import {
  MODIFY_GEOSPATIAL_OBJECT,
  DELETE_GEOSPATIAL_OBJECT,
  CREATE_GEOSPATIAL_OBJECT,
  DELETE_FACILITY,
  MODIFY_FACILITY,
  CREATE_FACILITY,
} from "utils/apollo/gql/placeManager";
import { showMessage } from "store/modules/message";
import { clientCb } from "utils/apollo/apollo";

// reducer.js
const initialState = {
  setDataPlace: null,
  loading: false,
  success: false,
};

const SET_DATA_PLACE_MANAGEMENT =
  "[PLACE_MANAGEMENT] SET_DATA_PLACE_MANAGEMENT";
const CREATE_SITE_START = "[PLACE MANAGER] CREATE_SITE_START";
const CREATE_SITE_SUCCESS = "[PLACE MANAGER] CREATE_SITE_SUCCESS";
const CREATE_GEOSPATIAL_OBJECT_START =
  "[PLACE_MANAGER] CREATE_GEOSPATIAL_OBJECT_START";
const CREATE_GEOSPATIAL_OBJECT_SUCCESS =
  "[PLACE_MANAGER] CREATE_GEOSPATIAL_OBJECT_SUCCESS";
const MODIFY_GEOSPATIAL_OBJECT_START =
  "[PLACE_MANAGER] MODIFY_GEOSPATIAL_OBJECT_START";
const MODIFY_GEOSPATIAL_OBJECT_SUCCES =
  "[PLACE_MANAGER] MODIFY_GEOSPATIAL_OBJECT_SUCCES";
const DELETE_GEOSPATIAL_OBJECT_START =
  "[PLACE_MANAGER] DELETE_GEOSPATIAL_OBJECT_START";
const DELETE_GEOSPATIAL_OBJECT_SUCCESS =
  "[PLACE_MANAGER] DELETE_GEOSPATIAL_OBJECT_SUCCESS";
const CREATE_FACILITY_SUCCESS = "[PLACE_MANAGER] CREATE_FACILITY_SUCCESS";
const CREATE_FACILITY_START = "[PLACE_MANAGER] CREATE_FACILITY_START";
const MODIFY_FACILITY_SUCCESS = "[PLACE_MANAGER] MODIFY_FACILITY_SUCCESS";
const MODIFY_FACILITY_START = "[PLACE_MANAGER] MODIFY_FACILITY_START";
const DELETE_FACILITY_SUCCESS = "[PLACE_MANAGER] DELETE_FACILITY_SUCCESS";
const DELETE_FACILITY_START = "[PLACE_MANAGER] DELETE_FACILITY_START";
const RESET_SUCCESS = "[PLACE_MANAGEMENT]RESET_SUCCESS";

// actions.js
const process = (type) => ({
  type,
});

const setDataPlaceManagement = (setDataPlace) => ({
  type: SET_DATA_PLACE_MANAGEMENT,
  payload: setDataPlace,
});

export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_DATA_PLACE_MANAGEMENT:
      return {
        ...state,
        setDataPlace: payload,
      };
    case CREATE_SITE_START:
      return { ...state, loading: true };
    case CREATE_SITE_SUCCESS:
      return { ...state, success: true, loading: false };
    case CREATE_GEOSPATIAL_OBJECT_START:
      return { ...state, loading: true };
    case CREATE_GEOSPATIAL_OBJECT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MODIFY_GEOSPATIAL_OBJECT_START:
      return { ...state, loading: true };
    case MODIFY_GEOSPATIAL_OBJECT_SUCCES:
      return { ...state, success: true, loading: false };
    case DELETE_GEOSPATIAL_OBJECT_START:
      return { ...state, loading: true };
    case DELETE_GEOSPATIAL_OBJECT_SUCCESS:
      return { ...state, success: true, loading: false };
    case CREATE_FACILITY_START:
      return { ...state, loading: true };
    case CREATE_FACILITY_SUCCESS:
      return { ...state, success: true, loading: false };
    case MODIFY_FACILITY_START:
      return { ...state, loading: true };
    case MODIFY_FACILITY_SUCCESS:
      return { ...state, success: true, loading: false };
    case DELETE_FACILITY_START:
      return { ...state, loading: true };
    case DELETE_FACILITY_SUCCESS:
      return { ...state, success: true, loading: false };
    case RESET_SUCCESS:
      return { ...state, success: false };
    default:
      return state;
  }
}

export const setFindDataPlace = (data) => async (dispatch) => {
  dispatch(setDataPlaceManagement(data));
};

export const createGeospatialObject = async ({ data, setIdSite }, dispatch) => {
  try {
    dispatch(process(CREATE_GEOSPATIAL_OBJECT_START));
    const { data: dataCreated, errors } = await clientCb.mutate({
      mutation: CREATE_GEOSPATIAL_OBJECT,
      variables: { input: data },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    setIdSite(dataCreated.createGeospatialObject.id);
    dispatch(process(CREATE_GEOSPATIAL_OBJECT_SUCCESS));
    dispatch(
      showMessage({
        message: "Record created successfully",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message:
          "An error occurred while creating the record: You have entered an invalid Site Code, Field Code or Planting Area Code. The code must be unique. ",
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    console.log(error);
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};

export const modifyGeospatialObject = async ({ data }, dispatch) => {
  try {
    dispatch(process(MODIFY_GEOSPATIAL_OBJECT_START));
    const { errors } = await clientCb.mutate({
      mutation: MODIFY_GEOSPATIAL_OBJECT,
      variables: { input: data },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MODIFY_GEOSPATIAL_OBJECT_SUCCES));
    dispatch(
      showMessage({
        message: "The data was successfully updated",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: `An error occurred while updating:` + error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    console.log(error);
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};

export const deleteGeospatialObject = async ({ id }, dispatch) => {
  try {
    dispatch(process(DELETE_GEOSPATIAL_OBJECT_START));
    const { errors } = await clientCb.mutate({
      mutation: DELETE_GEOSPATIAL_OBJECT,
      variables: { int: id },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(DELETE_GEOSPATIAL_OBJECT_SUCCESS));
    dispatch(
      showMessage({
        message: "Removed successfully",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: `An error occurred while deleting:` + error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    console.log(error);
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};

export const createFacility = async ({ data }, dispatch) => {
  try {
    dispatch(process(CREATE_FACILITY_START));
    const { errors } = await clientCb.mutate({
      mutation: CREATE_FACILITY,
      variables: { input: data },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(CREATE_FACILITY_SUCCESS));
    dispatch(
      showMessage({
        message: "Record created successfully",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message:
          `An error occurred while creating the record:` + error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    console.log(error);
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};

export const modifyFacility = async ({ data }, dispatch) => {
  try {
    dispatch(process(MODIFY_FACILITY_START));
    const { errors } = await clientCb.mutate({
      mutation: MODIFY_FACILITY,
      variables: { input: data },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MODIFY_FACILITY_SUCCESS));
    dispatch(
      showMessage({
        message: "The data was successfully updated",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: `An error occurred while updating:` + error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    console.log(error);
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};

export const deleteFacility = async ({ id }, dispatch) => {
  try {
    dispatch(process(DELETE_FACILITY_START));
    const { errors } = await clientCb.mutate({
      mutation: DELETE_FACILITY,
      variables: { id: id },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(DELETE_FACILITY_SUCCESS));
    dispatch(
      showMessage({
        message: "Removed successfully",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: `An error occurred while deleting:` + error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    console.log(error);
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};
