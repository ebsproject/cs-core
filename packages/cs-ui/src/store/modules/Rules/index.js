import { CREATE_RULE, DELETE_SECURITY_RULE, FIND_RULES_LIST, MODIFY_RULE } from "utils/apollo/gql/rules";
import { client } from "utils/apollo";
import { showMessage } from "../message";

/*
 Initial state and properties
 */
export const initialState = {
  rulesList: null
}

/*
 Action types
 */
export const SET_RULES_LIST = '[rules] SET_RULES_LIST';
const NEW_RULE = "[rules] NEW_RULE";

/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_RULES_LIST:
      return {
        ...state,
        rulesList: payload,
      }
    case NEW_RULE:
      return {
        ...state, ...payload,
      }
    default:
      return state
  }
}
const process = ({ type, payload }) => ({
  type,
  payload,
});

export const findRulesList = () => async (dispatch) => {
  try {
    const { errors, data } = await client.query({
      query: FIND_RULES_LIST,
      variables: {
        page: { number: 1, size: 100 },
      },
      fetchPolicy: "no-cache",
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(
      process({ type: SET_RULES_LIST, payload: data.findSecurityRuleList.content })
    );
  } catch (error) {
    dispatch(
      process({ type: SET_RULES_LIST, payload: [] })
    );
  }
};

export const assignRuleRole =
  ({ data, lastRoles }) =>
    async (dispatch) => {
      try {

        const newRoles = new Set();
        const toDelete = new Set();
        // * New roles
        data.role.map((role) => {
          // ? Doesn't exist a role already assigned?
          if (lastRoles.findIndex((e) => e.id === role.id) === -1) {
            newRoles.add(role);
          }
        });
        // * Removed roles
        lastRoles.map((rol) => {
          const { role } = data;
          // ? Are there roles assigned to delete?
          if (role.findIndex((e) => e.id === rol.id) === -1) {
            toDelete.add(rol);
          }
        });
        // * Delete roles
        for (let role of toDelete) {
          (async function (item) {
            // * here the value of role was passed into as the argument item
            try {
              const { errors } = await client.mutate({
                mutation: DELETE_USER_ROLE,
                variables: {
                  userRole: {
                    role: { id: Number(item.id), name: "" },
                    user: {
                      id: Number(data.userId),
                      userName: "",
                      contactId: Number(data.id),
                      active: true,
                    },
                  },
                },
              });
              if (errors) {
                throw new Error(`${errors[0].message}`);
              }
            } catch (error) {
              dispatch(setError(error));
              dispatch(
                showMessage({
                  message: error.toString(),
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
            }
          })(role);
        }
        // * Add new roles
        for (let role of newRoles) {
          (async function (item) {
            try {
              // * here the value of role was passed into as the argument item
              const { errors } = await client.mutate({
                mutation: CREATE_USER_ROLE,
                variables: {
                  userRole: {
                    user: {
                      id: Number(data.userId),
                      userName: "",
                      contactId: Number(data.id),
                      active: true,
                    },
                    role: {
                      id: Number(item.id),
                      name: "",
                    },
                  },
                },
              });
              if (errors) {
                throw new Error(`${errors[0].message}`);
              }
            } catch (error) {
              dispatch(setError(error));
              dispatch(
                showMessage({
                  message: error.toString(),
                  variant: "error",
                  anchorOrigin: {
                    vertical: "top",
                    horizontal: "right",
                  },
                })
              );
            }
          })(role);
        }

        dispatch(
          showMessage({
            message: "Contact created and assigned successfully",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } catch (error) {

      }
    };

export const securityRuleProcess =
  ({ method, defaultValues }) =>
    async (dispatch) => {
      switch (method) {
        case "POST":
          dispatch(
            process({
              type: NEW_RULE,
              payload: {
                title: "New Security Rule",
                method,
              },
            })
          );
          break;
        case "PUT":
          dispatch(
            process({
              type: NEW_RULE,
              payload: {
                title: "Update Security Rule",
                method,
                defaultValues,
              },
            })
          );
        default:
          break;
      }
    };

/**
* @param {Object} data
* @param {String} method
* @returns void
*/
export const mutationSecurityRule =
  ({ data, method, id, refresh }) =>
    async (dispatch) => {
      switch (method) {
        case "POST":
          createSecurityRule(data)(dispatch);
          break;
        case "PUT":
          updateSecurityRule({ dataForm: data })(dispatch);
          break;
        case "DELETE":
          deleteSecurityRule(id, refresh)(dispatch);
        default:
          break;
      }
    };

/**
 * @param {Object} data
 * @returns void
 */
const createSecurityRule = (data) => async (dispatch) => {
  try {
    const { name, description, usedByWorkflow } = data;
    const { errors } = await client.mutate({
      mutation: CREATE_RULE,
      variables: {
        securityRule: { id: 0, name, description, usedByWorkflow },
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }

    dispatch(
      showMessage({
        message: "Successfully added new security rule",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
/**
 * @param {Object} dataForm
 * @returns void
 */
const updateSecurityRule =
  ({ dataForm }) =>
    async (dispatch) => {
      try {
        const { id, name, description, usedByWorkflow } = dataForm;
        const { errors } = await client.mutate({
          mutation: MODIFY_RULE,
          variables: {
            securityRule: { id: Number(id), name, description, usedByWorkflow },
          },
        });
        if (errors) {
          throw new Error(`${errors[0].message}`);
        }
        dispatch(
          showMessage({
            message: "Successfully updated security rule",
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } catch (error) {
        console.log(error)
        dispatch(
          showMessage({
            message: error.toString(),
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      }
    };
/**
 * @param {String} id
 * @returns void
 */
const deleteSecurityRule = (id, refresh) => async (dispatch) => {
  try {

    const { errors } = await client.mutate({
      mutation: DELETE_SECURITY_RULE,
      variables: {
        securityRuleId: Number(id),
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }

    dispatch(
      showMessage({
        message: "Successfully deleted security rule",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    refresh();
  }
};