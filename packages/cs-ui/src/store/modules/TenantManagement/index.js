import { showMessage } from "store/modules/message";
import { client } from "utils/apollo";
import {
  CREATE_ORGANIZATION,
  DELETE_ORGANIZATION,
  MODIFY_ORGANIZATION,
  CREATE_CUSTOMER,
  MODIFY_CUSTOMER,
  DELETE_CUSTOMER,
} from "utils/apollo/gql/tenantManagement";
/**
 * Initial state and properties
 */
export const initialState = {
  loading: false,
  success: false,
};

/*
 Action types
 */
const CREATE_CUSTOMER_START = "[TENANT_MANAGEMENT]CREATE_CUSTOMER_START";
const CREATE_CUSTOMER_SUCCESS = "[TENANT_MANAGEMENT]CREATE_CUSTOMER_SUCCESS";
const MODIFY_CUSTOMER_START = "[TENANT_MANAGEMENT]MODIFY_CUSTOMER_START";
const MODIFY_CUSTOMER_SUCCESS = "[TENANT_MANAGEMENT]MODIFY_CUSTOMER_SUCCESS";
const DELETE_CUSTOMER_START = "[TENANT_MANAGEMENT]DELETE_CUSTOMER_START";
const DELETE_CUSTOMER_SUCCESS = "[TENANT_MANAGEMENT]DELETE_CUSTOMER_SUCCESS";
const CREATE_ORGANIZATION_START =
  "[TENANT_MANAGEMENT]CREATE_ORGANIZATION_START";
const CREATE_ORGANIZATION_SUCCESS =
  "[TENANT_MANAGEMENT]CREATE_ORGANIZATION_SUCCESS";
const MODIFY_ORGANIZATION_START =
  "[TENANT_MANAGEMENT]MODIFY_ORGANIZATION_START";
const MODIFY_ORGANIZATION_SUCCESS =
  "[TENANT_MANAGEMENT]MODIFY_ORGANIZATION_SUCCESS";
const DELETE_ORGANIZATION_START =
  "[TENANT_MANAGEMENT]DELETE_ORGANIZATION_START";
const DELETE_ORGANIZATION_SUCCESS =
  "[TENANT_MANAGEMENT]DELETE_ORGANIZATION_SUCCESS";
const RESET_SUCCESS = "[TENANT_MANAGEMENT]RESET_SUCCESS";
const TENANT_ERROR = "[TENANT_MANAGEMENT]PROCESS_ERROR";
/*
 Arrow function for change state
 */

const process = (type) => ({
  type,
});
const tenantFailure = (payload) => ({
  type: TENANT_ERROR,
  payload,
});

/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case CREATE_CUSTOMER_START:
      return { ...state, loading: true };
    case CREATE_CUSTOMER_SUCCESS:
      return { ...state, success: true, loading: false };
    case MODIFY_CUSTOMER_START:
      return { ...state, loading: true };
    case MODIFY_CUSTOMER_SUCCESS:
      return { ...state, success: true, loading: false };
    case DELETE_CUSTOMER_START:
      return { ...state, loading: true };
    case DELETE_CUSTOMER_SUCCESS:
      return { ...state, success: true, loading: false };
    case CREATE_ORGANIZATION_START:
      return { ...state, loading: true };
    case MODIFY_ORGANIZATION_START:
      return { ...state, loading: true };
    case DELETE_ORGANIZATION_START:
      return { ...state, loading: true };
    case CREATE_ORGANIZATION_SUCCESS:
      return { ...state, loading: false, success: true, error: null };
    case MODIFY_ORGANIZATION_SUCCESS:
      return { ...state, loading: false, success: true, error: null };
    case DELETE_ORGANIZATION_SUCCESS:
      return { ...state, loading: false, success: true, error: null };
    case RESET_SUCCESS:
      return { ...state, success: false };
    case TENANT_ERROR:
      return { ...state, error: payload };
    default:
      return state;
  }
}

// ENTITY CUSTOMER
/**
 * @param {CustomerInput} customer
 * @returns void
 */
export const createCustomer = (customer) => async (dispatch) => {
  try {
    dispatch(process(CREATE_CUSTOMER_START));
    const { errors } = await client.mutate({
      mutation: CREATE_CUSTOMER,
      variables: { customer },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(CREATE_CUSTOMER_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully added new customer",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(tenantFailure(error));
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};
/**
 * @param {CustomerInput} customerInput
 * @returns void
 */
export const modifyCustomer = (customer) => async (dispatch) => {
  try {
    dispatch(process(MODIFY_CUSTOMER_START));
    const { errors } = await client.mutate({
      mutation: MODIFY_CUSTOMER,
      variables: { customer },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MODIFY_CUSTOMER_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully updated customer",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(tenantFailure(error));
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};
/**
 * @param {ID} customerId
 * @returns void
 */
export const deleteCustomer = (customerId) => async (dispatch) => {
  try {
    dispatch(process(DELETE_CUSTOMER_START));
    const { errors } = await client.mutate({
      mutation: DELETE_CUSTOMER,
      variables: { customerId },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(DELETE_CUSTOMER_SUCCESS));
    dispatch(
      showMessage({
        message: "Deleted Customer",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(tenantFailure(error));
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};

/**
 * @param {OrganizationInput} organization
 * @returns void
 */
export const createOrganization = (organization) => async (dispatch) => {
  try {
    dispatch(process(CREATE_ORGANIZATION_START));
    const { errors } = await client.mutate({
      mutation: CREATE_ORGANIZATION,
      variables: { organization },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(CREATE_ORGANIZATION_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully added new organization",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    if (
      error.toString() === "Error: Given code already exists" ||
      "Error: could not execute statement; SQL [n/a]; constraint [UQ_code]; nested exception is org.hibernate.exception.ConstraintViolationException: could not execute statement"
    ) {
      const name = organization.name;
      dispatch(
        showMessage({
          message:
            "The organization " + name + " already exists in the database",
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } else {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
    dispatch(tenantFailure(error));
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};
/**
 * @param {OrganizationInput} organizationInput
 * @returns void
 */
export const modifyOrganization = (organization) => async (dispatch) => {
  try {
    dispatch(process(MODIFY_ORGANIZATION_START));
    const { errors } = await client.mutate({
      mutation: MODIFY_ORGANIZATION,
      variables: { organization },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MODIFY_ORGANIZATION_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully update organization",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(tenantFailure(error));
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};
/**
 * @param {ID} organizationId
 * @returns void
 */
export const deleteOrganization = (organizationId) => async (dispatch) => {
  try {
    dispatch(process(DELETE_ORGANIZATION_START));
    const { errors } = await client.mutate({
      mutation: DELETE_ORGANIZATION,
      variables: { organizationId },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(DELETE_ORGANIZATION_SUCCESS));
    dispatch(
      showMessage({
        message: "Deleted Organization",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(tenantFailure(error));
  } finally {
    dispatch(process(RESET_SUCCESS));
  }
};
