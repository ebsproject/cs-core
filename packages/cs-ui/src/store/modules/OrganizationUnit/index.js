/*
 Initial state and properties
 */
export const initialState = {
  defaultValues: null,
  method: null,
};

/*
  Action types
  */
export const SET_DEFAULTVALUES = "[orgUnit] SET_DEFAULTVALUES";
export const SET_METHOD = "[orgUnit] SET_METHOD";
export const SET_TEAM_ID = "[orgUnit] SET_TEAM_ID";
export const SET_TEAM_DEFAULTVALUES = "[orgUnit] SET_TEAM_DEFAULTVALUES";
/*
  Arrow function for change state
  */
export const setDefaultValues = (payload) => ({
  type: SET_DEFAULTVALUES,
  payload,
});
export const setMethod = (payload) => ({
  type: SET_METHOD,
  payload,
});
export const setTeamId = (payload) => ({
  type: SET_TEAM_ID,
  payload,
});
export const setTeamDefaultValues = (payload) => ({
  type: SET_TEAM_DEFAULTVALUES,
  payload,
});
/*
  Reducer to describe how the state changed
  */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_DEFAULTVALUES:
      return {
        ...state,
        defaultValues: payload,
      };
    case SET_METHOD:
      return {
        ...state,
        method: payload,
      };
    case SET_TEAM_ID:
      return {
        ...state,
        teamId: payload,
      };
    case SET_TEAM_DEFAULTVALUES:
      return {
        ...state,
        teamDefaultValues: payload,
      };
    default:
      return state;
  }
}
