/*
 Initial state and properties
 */
 export const initialState = {
    defaultValues:null,
    method: "POST"
 }

 /*
  Action types
  */
 export const SET_DEFAULTVALUES = '[emailTemplate] SET_DEFAULTVALUES'
 export const SET_METHOD = '[emailTemplate] SET_METHOD'
 /*
  Arrow function for change state
  */
 export const setDefaultValues = (payload) =>(
{
   type: SET_DEFAULTVALUES,
   payload,
 })
 export const setMethod = (payload) =>(
    {
       type: SET_METHOD,
       payload,
     })
 /*
  Reducer to describe how the state changed
  */
 export default function Reducer(state = initialState, { type, payload }) {
   switch (type) {
     case SET_DEFAULTVALUES:
       return {
         ...state,
         defaultValues: payload,
       }
       case SET_METHOD:
       return {
         ...state,
         method: payload,
       }
     default:
       return state
   }
 }  