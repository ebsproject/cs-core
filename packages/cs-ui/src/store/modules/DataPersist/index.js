/**
 * !This module is used to Persist Data for different forms and it's stored into Local Storage
 */
import { client } from "utils/apollo";
import { showMessage } from "store/modules/message";
import {
  FIND_CONTACT_TYPE_LIST,
  FIND_TENANT_LIST,
  FIND_COUNTRY_LIST,
  FIND_PURPOSE_LIST,
  FIND_CONTACT,
  FIND_CONTACT_LIST,
  FIND_HIERARCHY_LIST,
  FIND_HIERARCHY_TREE_LIST,
  FIND_CONTACT_INSTITUTIONS,
} from "utils/apollo/gql/crm";
import {
  FIND_ROLE_LIST,
  FIND_USER,
  FIND_USER_LIST,
} from "utils/apollo/gql/userManagement";
import { FIND_PRODUCT_FUNCTION_LIST } from "utils/apollo/gql/tenantManagement";
import { Icons } from "@ebs/styleguide";
const { ErrorTwoTone } = Icons;
import { FIND_INSTANCE } from "utils/apollo/gql/tenant";
import { getContext } from "@ebs/layout";
import { reject } from "lodash";
/*
  Initial state and properties
  */
export const initialState = {
  title: "",
  serviceProviderList: [],
  contactTypeList: [],
  countryList: [],
  purposeList: [],
  tenantList: [],
  hierarchyList: [],
  contactList: [],
  loading: false,
  error: null,
  contact: {},
  defaultAddress: {},
};
/*
  Action types
  */
const NEW_PERSON = "[PERSIST] NEW_PERSON";
const ASSIGN_PERSON = "[PERSIST] ASSIGN_PERSON";
const UPDATE_PERSON = "[PERSIST] UPDATE_PERSON";
const UPDATE_DEFAULT_VALUES = "[PERSIST] UPDATE_DEFAULT_VALUES";
const NEW_INSTITUTION = "[PERSIST] NEW_INSTITUTION";
const UPDATE_INSTITUTION = "[PERSIST] UPDATE_INSTITUTION";
const NEW_ROLE = "[PERSIST] NEW_ROLE";
const UPDATE_ROLE = "[PERSIST] UPDATE_ROLE";
const GET_TENANT_LIST = "[PERSIST] GET_TENANT_LIST";
const SET_TENANT_LIST = "[PERSIST] SET_TENANT_LIST";
const GET_DEFAULT_ADDRESS = "[PERSIST] GET_DEFAULT_ADDRESS";
const SET_DEFAULT_ADDRESS = "[PERSIST] SET_DEFAULT_ADDRESS";
const GET_CONTACT_LIST = "[PERSIST] GET_CONTACT_LIST";
const SET_CONTACT_LIST = "[PERSIST] SET_CONTACT_LIST";
const GET_CONTACT_TYPE_LIST = "[PERSIST] GET_CONTACT_TYPE_LIST";
const SET_CONTACT_TYPE_LIST = "[PERSIST] SET_CONTACT_TYPE_LIST";
const GET_COUNTRY_LIST = "[PERSIST] GET_COUNTRY_LIST";
const SET_COUNTRY_LIST = "[PERSIST] SET_COUNTRY_LIST";
const GET_PURPOSE_LIST = "[PERSIST] GET_PURPOSE_LIST";
const SET_PURPOSE_LIST = "[PERSIST] SET_PURPOSE_LIST";
const GET_HIERARCHY_LIST = "[PERSIST] GET_HIERARCHY_LIST";
const SET_HIERARCHY_LIST = "[PERSIST] SET_HIERARCHY_LIST";
const GET_FIND_CONTACT = "[PERSIST] GET_FIND_CONTACT";
const SET_FIND_CONTACT = "[PERSIST] SET_FIND_CONTACT";
const SET_ROLE_LIST = "[PERSIST] SET_ROLE_LIST";
const SET_PRODUCT_FUNCTION_LIST = "[PERSIST] SET_PRODUCT_FUNCTION_LIST";
const NEW_USER = "[PERSIST] NEW_USER";
const UPDATE_USER = "[PERSIST] UPDATE_USER";
const VIEW_UNITS = "[PERSIST] VIEW UNITS";
export const CLEAN_CONTACT = "[PERSIST] CLEAN_CONTACT";
export const SET_TYPE_CONTACT = "[PERSIST] SET_TYPE_CONTACT";
const ERROR = "[PERSIST] ERROR";
/*
  Arrow function for change state
  */
export const process = ({ type, payload }) => ({
  type,
  payload,
});
const setData = (type, payload) => ({
  type,
  payload,
});
const setError = (payload) => ({
  type: ERROR,
  payload,
});
const getTenants = () => ({
  type: GET_TENANT_LIST,
});
const setTenants = (tenantList) => ({
  type: SET_TENANT_LIST,
  payload: tenantList,
});
/*
  Reducer to describe how the state changed
  */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case UPDATE_DEFAULT_VALUES:
      return { ...state, defaultValues: payload };
    case NEW_PERSON:
      return { ...state, ...payload, defaultValues: null };
    case ASSIGN_PERSON:
      return { ...state, ...payload, defaultValues: null };
    case NEW_INSTITUTION:
      return { ...state, ...payload, defaultValues: null };
    case UPDATE_PERSON:
      return { ...state, ...payload };
    case UPDATE_INSTITUTION:
      return { ...state, ...payload };
    case NEW_ROLE:
      return { ...state, ...payload };
    case UPDATE_ROLE:
      return { ...state, ...payload };
    case GET_TENANT_LIST:
      return { ...state, loading: true };
    case SET_TENANT_LIST:
      return { ...state, tenantList: payload, loading: false };
    case GET_DEFAULT_ADDRESS:
      return { ...state, loading: true };
    case SET_DEFAULT_ADDRESS:
      return { ...state, defaultAddress: payload, loading: false };
    case GET_CONTACT_LIST:
      return { ...state, loading: true };
    case SET_CONTACT_LIST:
      return { ...state, serviceProviderList: payload, loading: false };
    case GET_CONTACT_TYPE_LIST:
      return { ...state, loading: true };
    case SET_CONTACT_TYPE_LIST:
      return { ...state, contactTypeList: payload, loading: false };
    case GET_COUNTRY_LIST:
      return { ...state, loading: true };
    case SET_COUNTRY_LIST:
      return { ...state, countryList: payload, loading: false };
    case GET_PURPOSE_LIST:
      return { ...state, loading: true };
    case SET_PURPOSE_LIST:
      return { ...state, purposeList: payload, loading: false };
    case GET_HIERARCHY_LIST:
      return { ...state, loading: true };
    case SET_HIERARCHY_LIST:
      return { ...state, hierarchyList: payload, loading: false };
    case SET_ROLE_LIST:
      return { ...state, roleList: payload, loading: false };
    case GET_FIND_CONTACT:
      return { ...state, loading: true };
    case SET_FIND_CONTACT:
      return { ...state, contactList: payload, loading: false };
    case CLEAN_CONTACT:
      return { ...state, defaultValues: null };
    case SET_TYPE_CONTACT:
      return { ...state, type: payload };
    case SET_PRODUCT_FUNCTION_LIST:
      return { ...state, productFunctionList: payload };
    case NEW_USER:
      return { ...state, ...payload, defaultValues: null };
    case UPDATE_USER:
      return { ...state, ...payload };
    case VIEW_UNITS:
      return { ...state, ...payload };
    case ERROR:
      return { ...state, error: payload };
    default:
      return state;
  }
}

export const findDefaultAddress = (instanceId) => async (dispatch) => {
  try {
    dispatch(process({ type: GET_DEFAULT_ADDRESS }));
    const { data, errors } = await client.query({
      query: FIND_INSTANCE,
      variables: {
        id: instanceId,
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(setData(SET_DEFAULT_ADDRESS, data.findInstance.address));
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};

/**
 * @returns {void}
 */
export const findServiceProviderList = () => async (dispatch) => {
  try {
    dispatch(process({ type: GET_CONTACT_LIST }));
    const { data, errors } = await client.query({
      query: FIND_CONTACT_LIST,
      variables: {
        page: { number: 1, size: 100 },
        filters: [{ col: "category.name", mod: "EQ", val: "Internal Unit" }],
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(setData(SET_CONTACT_LIST, data.findContactList.content));
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
export const findTenantList = () => async (dispatch, getState) => {
  dispatch(getTenants());
  client
    .query({
      query: FIND_TENANT_LIST,
    })
    .then(({ data }) => {
      dispatch(setTenants(data.findTenantList));
    })
    .catch(({ message }) => {
      dispatch(
        showMessage({
          message: message,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(tenantFailure(message));
    });
};
/**
 * @returns {void}
 */
export const findContactTypeList = () => async (dispatch) => {
  try {
    dispatch(process({ type: GET_CONTACT_TYPE_LIST }));
    const { data, errors } = await client.query({
      query: FIND_CONTACT_TYPE_LIST,
      variables: {
        page: { number: 1, size: 100 },
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(setData(SET_CONTACT_TYPE_LIST, data.findContactTypeList.content));
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.String(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
/**
 * @returns {void}
 */
export const findCountryList = () => async (dispatch) => {
  try {
    dispatch(process({ type: GET_COUNTRY_LIST }));
    const { data, errors } = await client.query({
      query: FIND_COUNTRY_LIST,
      variables: {
        page: { number: 1, size: 300 },
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(setData(SET_COUNTRY_LIST, data.findCountryList.content));
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.String(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
/**
 * @returns void
 */
export const findProductFunctionList = () => async (dispatch) => {
  try {
    let number = 1;
    let totalData = [];
    const { data , errors } = await client.mutate({
      mutation: FIND_PRODUCT_FUNCTION_LIST,
      variables: {
        page: { number: 1, size: 500 },
      },
      fetchPolicy: "no-cache",
    });
    if (errors) {
      //throw new ErrorTwoTone(`${errors[0].message}`);
    }
    number = data.findProductFunctionList.totalPages;

    totalData = [...totalData, ...data.findProductFunctionList.content];
    if (number > 1) {
      for (let i = 2; i <= number; i++) {
        const { data } = await client.query({
          query: FIND_PRODUCT_FUNCTION_LIST,
          variables: {
            page: { number: i, size: 300 },
            fetchPolicy: "no-cache",
          }
        });
        totalData = [...totalData, ...data.findProductFunctionList.content];
      }
    }

    const tree = [{ value: "0", label: "Domains", children: [] }];
    totalData.map((prodFn) => {
      // * Find domain
      const index = tree[0].children.findIndex(
        (item) => item.value === `${prodFn.product.domain.id}-id`
      );
      if (index != -1) {
        // * Domain already exist
        const products = tree[0].children[index].children;
        const prodIndex = products.findIndex(
          (item) => item.value === `${prodFn.product.id}-d`
        );
        if (prodIndex != -1) {
          // * Product already exist
          tree[0].children[index].children[prodIndex].children.push({
            value: prodFn.id,
            label: prodFn.action,
          });
        } else {
          // * Adding new product
          tree[0].children[index].children.push({
            value: `${prodFn.product.id}-d`,
            label: prodFn.product.name,
            children: [{ value: prodFn.id, label: prodFn.action }],
          });
        }
      } else {
        // * Adding new Domain
        tree[0].children.push({
          value: `${prodFn.product.domain.id}-id`,
          label: prodFn.product.domain.name,
          children: [
            {
              value: `${prodFn.product.id}-d`,
              label: prodFn.product.name,
              children: [
                {
                  value: prodFn.id,
                  label: prodFn.action,
                },
              ],
            },
          ],
        });
      }
    });
    dispatch(
      process({
        type: SET_PRODUCT_FUNCTION_LIST,
        payload: tree,
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.String(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
/**
 * @param {String} type
 * @returns {void}
 */
export const findPurposeList =
  ({ type }) =>
  async (dispatch) => {
    try {
      dispatch(process({ type: GET_PURPOSE_LIST }));
      const { data, errors } = await client.query({
        query: FIND_PURPOSE_LIST,
        variables: {
          page: { number: 1, size: 10 },
          filters: [
            { col: "category.name", mod: "LK", val: type },
            { col: "category.name", mod: "LK", val: "Address" },
          ],
          disjunctionFilters: true,
        },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(setData(SET_PURPOSE_LIST, data.findPurposeList.content));
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
  };
/**
 * @param {ID} id
 * @param {String} type
 * @returns {void}
 */
export const findContact = async ({ id, type }) => {
  if (type === "INSTITUTION") {
    return new Promise(async (resolve, reject) => {
      try {
        const { errors, data } = await client.query({
          query: FIND_CONTACT_INSTITUTIONS,
          variables: { id: Number(id) },
          fetchPolicy: "no-cache",
        });
        if (errors) {
          throw new Error(`${errors[0].message}`);
        }
        const defaultValues = {};
        const phones = [];
        data.findContact.contactInfos.map((item) => {
          switch (item.contactInfoType.id) {
            // ? Phone
            case 1:
              phones.push({ id: item.id, value: item.value });
              break;
            // ? Email
            case 2:
              Object.assign(defaultValues, { officialEmail: item.value });
              break;
            // ? Web page
            case 3:
              break;
            default:
              break;
          }
        });
        Object.assign(defaultValues, { id: Number(id) });
        Object.assign(defaultValues, {
          addresses: data.findContact.addresses,
          phones: phones,
          category: data.findContact.category,
          purposes: data.findContact.purposes,
          contactType: data.findContact.contactTypes,
          officialEmail: data.findContact.email,
          tenants: data.findContact?.tenants || [],
        });
        Object.assign(defaultValues, {
          crop: data.findContact.institution.crops,
          isCgiar: data.findContact.institution.isCgiar,
          commonName: data.findContact.institution.commonName,
          legalName: data.findContact.institution.legalName,
          principalContact:
            data.findContact?.institution?.principalContact || [],
        });

        resolve({ data: defaultValues });
      } catch (error) {
        reject(error);
      }
    });
  } else {
    return new Promise(async (resolve, reject) => {
      try {
        const { errors, data } = await client.query({
          query: FIND_CONTACT,
          variables: { id: Number(id) },
          fetchPolicy: "no-cache",
        });
        if (errors) {
          throw new Error(`${errors[0].message}`);
        }
        const dataInstitutionList = await client.query({
          query: FIND_CONTACT_LIST,
          variables: {
            page: { number: 1, size: 100 },
            filters: [{ col: "category.name", mod: "EQ", val: "Institution" }],
          },
        });
        const defaultValues = {};
        const phones = [];
        data.findContact.contactInfos.map((item) => {
          switch (item.contactInfoType.id) {
            // ? Phone
            case 1:
              phones.push({ id: item.id, value: item.value });
              break;
            // ? Email
            case 2:
              Object.assign(defaultValues, { officialEmail: item.value });
              break;
            // ? Web page
            case 3:
              break;
            default:
              break;
          }
        });
        let dataInstitution = data.findContact.parents;
        const newContactListHerarchy = [];
        let newDefaultValuesAddress = [];
        let newArrayAddress = [];
        let newArrayInstitutionAddress = [];
        // InstitutionInput
        newContactListHerarchy.push(
          dataInstitution.map((item) => ({
            id: item.institution.id,
            name: item.institution.institution.commonName,
            addresses: item.institution.addresses,
            category: item.institution.category.name,
          }))
        );
        // InstitutionAddress
        newArrayInstitutionAddress.push(
          dataInstitutionList.data.findContactList.content.map((item) => ({
            id: item.institution.id,
            name: item.institution.commonName,
            addresses: item.addresses,
          }))
        );
        newArrayInstitutionAddress[0].map((item) => {
          newArrayAddress.push(
            item.addresses.map((i) => ({
              name: item.name,
              id: i.id,
              country: i.country,
              default: i.default,
              location: i.location,
              purposes: i.purposes,
              region: i.region,
              streetAddress: i.streetAddress,
              zipCode: i.zipCode,
            }))
          );
        });

        let idAddressContact = data.findContact.addresses.map(
          (item) => item.id
        );
        idAddressContact.map((idContactAddress) => {
          newArrayAddress.map((addressIntitution) => {
            let dataContact = addressIntitution?.filter(
              (i) => i.id === idContactAddress
            );
            if (dataContact?.length > 0) {
              newDefaultValuesAddress.push(dataContact?.[0]);
            }
          });
        });
        for (
          let element = 0;
          element < data?.findContact.addresses.length;
          element++
        ) {
          for (let value = 0; value < newDefaultValuesAddress.length; value++) {
            if (
              data?.findContact.addresses?.[element].id ===
              newDefaultValuesAddress?.[value].id
            ) {
              Object.assign(data.findContact.addresses[element], {
                name: newDefaultValuesAddress[value].name,
                disabled: true,
              });
            }
          }
        }

        Object.assign(defaultValues, { id: Number(id) });
        Object.assign(defaultValues, {
          addresses: data.findContact.addresses,
          phones: phones,
          purposes: data.findContact.purposes,
          category: data.findContact.category,
          contactType: data.findContact.contactTypes,
          officialEmail: data.findContact.email,
          tenants: data.findContact?.tenants || [],
        });
        Object.assign(defaultValues, {
          familyName: data.findContact.person.familyName,
          givenName: data.findContact.person.givenName,
          additionalName: data.findContact.person.additionalName,
          gender: {
            id:data.findContact.person.gender ? data.findContact.person.gender.toLowerCase() : "", 
            name: data.findContact.person.gender ? data.findContact.person.gender: "" },
          salutation: {
            id:data.findContact.person.salutation ? data.findContact.person.salutation.toLowerCase() : "", 
            name: data.findContact.person.salutation ? data.findContact.person.salutation : "" },
          contacts: data.findContact?.person?.institutions || [],
          institutionInput:
            newContactListHerarchy[0].filter(
              (item) => item.category === "Institution"
            ) || [],
        });
        resolve({ data: defaultValues });
      } catch (error) {
        reject(error);
      }
    });
  }
};

/**
 * @param {ID} id
 * @param {String} type
 * @returns {void}
 */
export const findContactOU = ({ id, type }) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { errors, data } = await client.query({
        query: FIND_CONTACT,
        variables: { id: Number(id) },
        fetchPolicy: "no-cache",
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      resolve({ data: data.findContact });
    } catch (error) {
      reject(error);
    }
  });
};
/**
 * @param {ID} id
 * @returns
 */
export const findUser = ({ id }) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { errors, data } = await client.query({
        query: FIND_USER,
        variables: { id: Number(id) },
        fetchPolicy: "no-cache",
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }

      const { errors: errorsHierarchy, data: dataHierarchy } =
        await client.query({
          query: FIND_HIERARCHY_LIST,
          variables: {
            page: { number: 1, size: 10 },
            filters: [
              { col: "contact.id", mod: "EQ", val: data.findUser.contact.id },
              {
                col: "institution.category.name",
                mod: "EQ",
                val: "Internal Unit",
              },
            ],
            disjunctionFilters: false,
          },
          fetchPolicy: "no-cache",
        });
      if (errorsHierarchy) {
        throw new Error(`${errorsHierarchy[0].message}`);
      }

      const dataInstitutionList = await client.query({
        query: FIND_CONTACT_LIST,
        variables: {
          page: { number: 1, size: 100 },
          filters: [{ col: "category.name", mod: "EQ", val: "Institution" }],
        },
      });

      const serviceProviders = [];
      dataHierarchy.findHierarchyList.content.map((item) =>
        serviceProviders.push(item.institution)
      );

      let dataInstitution = data.findUser.contact.parents;
      const newContactListHerarchy = [];
      let newDefaultValuesAddress = [];
      let newArrayAddress = [];
      let newArrayInstitutionAddress = [];

      newContactListHerarchy.push(
        dataInstitution.map((item) => ({
          id: item.institution.id,
          name: item.institution.institution.commonName,
          addresses: item.institution.addresses,
          category: item.institution.category.name,
        }))
      );
      // InstitutionAddress
      newArrayInstitutionAddress.push(
        dataInstitutionList.data.findContactList.content.map((item) => ({
          id: item.institution.id,
          name: item.institution.commonName,
          addresses: item.addresses,
        }))
      );
      newArrayInstitutionAddress[0].map((item) => {
        newArrayAddress.push(
          item.addresses.map((i) => ({
            name: item.name,
            id: i.id,
            country: i.country,
            default: i.default,
            location: i.location,
            purposes: i.purposes,
            region: i.region,
            streetAddress: i.streetAddress,
            zipCode: i.zipCode,
          }))
        );
      });

      let idAddressContact = data.findUser.contact.addresses.map(
        (item) => item.id
      );
      idAddressContact.map((idContactAddress) => {
        newArrayAddress.map((addressIntitution) => {
          let dataContact = addressIntitution?.filter(
            (i) => i.id === idContactAddress
          );
          if (dataContact?.length > 0) {
            newDefaultValuesAddress.push(dataContact?.[0]);
          }
        });
      });
      for (
        let element = 0;
        element < data?.findUser.contact.addresses.length;
        element++
      ) {
        for (let value = 0; value < newDefaultValuesAddress.length; value++) {
          if (
            data?.findUser.contact.addresses?.[element].id ===
            newDefaultValuesAddress?.[value].id
          ) {
            Object.assign(data.findUser.contact.addresses[element], {
              name: newDefaultValuesAddress[value].name,
              disabled: true,
            });
          }
        }
      }
      const defaultValues = {
        userId: Number(data.findUser.id),
        userName: data.findUser.userName,
        //role: data.findUser.roles,
        id: Number(data.findUser.contact.id),
        familyName: data.findUser.contact.person.familyName,
        givenName: data.findUser.contact.person.givenName,
        additionalName: data.findUser.contact.person.additionalName,
        gender: { name: data.findUser.contact.person.gender },
        contactType: data.findUser.contact.contactTypes,
        purposes: data.findUser.contact.purposes,
        addresses: data.findUser.contact.addresses,
        serviceProvider: serviceProviders,
        tenants: data.findUser.tenants,
        salutation: { name: data.findUser.contact.person.salutation },
        institutionInput:
          newContactListHerarchy?.[0].filter(
            (item) => item.category === "Institution"
          ) || [],
        active: data.findUser.isActive,
        isAdmin: data.findUser.isAdmin ? "admin" : "user",
      };

      const phones = [];
      data.findUser.contact.contactInfos.map((item) => {
        switch (item.contactInfoType.id) {
          // ? Phone
          case 1:
            phones.push({ id: item.id, value: item.value });
            break;
          // ? Email
          case 2:
            Object.assign(defaultValues, { officialEmail: item.value });
            break;
          // ? Web page
          case 3:
            break;
          default:
            break;
        }
      });
      Object.assign(defaultValues, { phones: phones });
      resolve({ data: defaultValues });
    } catch (error) {
      reject(error);
    }
  });
};
/**
 * @returns void
 */
const findRoleList = () => async (dispatch) => {
  try {
    const { errors, data } = await client.query({
      query: FIND_ROLE_LIST,
      variables: {
        page: { number: 1, size: 100 },
      },
      fetchPolicy: "no-cache",
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(
      process({ type: SET_ROLE_LIST, payload: data.findRoleList.content })
    );
  } catch (error) {
    dispatch(process({ type: ERROR, payload: error }));
  }
};
/**
 * @param {String} userName
 * @returns void
 */
export const findContactByAccount =
  ({ userName, type }) =>
  async (dispatch) => {
    try {
      const { data, errors } = await client.query({
        query: FIND_CONTACT_LIST,
        variables: {
          page: { number: 1, size: 1 },
          filters: { col: "email", mod: "EQ", val: userName },
        },
        fetchPolicy: "no-cache",
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      const defaultValues = {};
      if (data.findContactList.content.length == 0) {
        Object.assign(defaultValues, {
          addresses: [{ id: 0, default: true }],
        });
      }
      const { data: user, errors: err } = await client.query({
        query: FIND_USER_LIST,
        variables: {
          filters: [
            {
              col: "contact.id",
              mod: "EQ",
              val: data.findContactList.content[0]?.id.toString() || "-1",
            },
          ],
        },
        fetchPolicy: "no-cache",
      });
      if (user.findUserList.content.length > 0 || err) {
        throw new Error(`User Already Exist ${err}`);
      }

      const contact = data.findContactList?.content[0];
      const phones = [];
      contact?.contactInfos?.map((item) => {
        switch (item.contactInfoType.id) {
          // ? Phone
          case 1:
            phones.push({ id: item.id, value: item.value });
            break;
          // ? Email
          case 2:
            Object.assign(defaultValues, { officialEmail: item.value });
            break;
          // ? Web page
          case 3:
            break;
          default:
            break;
        }
      });
      Object.assign(defaultValues, {
        id: Number(contact?.id) || 0,
        userName,
      });
      Object.assign(defaultValues, {
        addresses:
          contact?.addresses.length > 0
            ? contact.addresses
            : [],
        phones: phones.length > 0 ? phones : [{ id: 0, value: "" }],
        purposes: contact?.purposes || [],
        contactType: contact?.contactTypes || [
          {
            id: 9,
            name: "Ebs member",
            category: { name: "Person" },
          },
        ],
      });
      Object.assign(defaultValues, {
        familyName: contact?.person?.familyName || "",
        givenName: contact?.person?.givenName || "",
        additionalName: contact?.person.additionalName || "",
        gender: { name: contact?.person.gender } || {},
        contacts: contact?.person?.institutions || [],
      });
      dispatch(
        process({ type: UPDATE_DEFAULT_VALUES, payload: defaultValues })
      );
    } catch (error) {
      dispatch(process({ type: ERROR, payload: error }));
    }
  };
/**
 * @param {String} type
 * @param {String} method
 * @param {ID} id: optional
 * @returns {void}
 */
export const contactProcess =
  ({ type, method, defaultValues, institutionId }) =>
  async (dispatch) => {
    switch (type) {
      case "PERSON":
        switch (method) {
          case "POST":
           // findTenantList()(dispatch);
            findContactTypeList()(dispatch);
            findCountryList()(dispatch);
            findDefaultAddress(getContext().instanceId)(dispatch);
            findContactList()(dispatch);
            dispatch(
              process({
                type: NEW_PERSON,
                payload: { title: "New Person", type, method },
              })
            );
            break;
          case "PUT":
            findTenantList()(dispatch);
            findContactTypeList()(dispatch);
            findCountryList()(dispatch);
            findContactList()(dispatch);
            dispatch(
              process({
                type: UPDATE_PERSON,
                payload: {
                  title: "Update Person",
                  type,
                  method,
                  defaultValues,
                },
              })
            );
            break;
          case "ASSIGN":
            //findTenantList()(dispatch);
            findContactTypeList()(dispatch);
            findCountryList()(dispatch);
            findContactList()(dispatch);
            dispatch(
              process({
                type: ASSIGN_PERSON,
                payload: {
                  title: "Assign New Person",
                  type,
                  method,
                  institutionId,
                },
              })
            );
            break;
          default:
            break;
        }
        break;
      case "INSTITUTION":
        switch (method) {
          case "POST":
            //findTenantList()(dispatch);
            findDefaultAddress(getContext().instanceId)(dispatch);
            findContactTypeList()(dispatch);
            findCountryList()(dispatch);
            dispatch(
              process({
                type: NEW_INSTITUTION,
                payload: { title: "New Institution", type, method },
              })
            );
            break;
          case "ASSIGN":
            dispatch(
              process({
                type: VIEW_UNITS,
                payload: { title: "View Units", type, method, institutionId },
              })
            );
            break;
          case "PUT":
            //findTenantList()(dispatch);
            findContactTypeList()(dispatch);
            findCountryList()(dispatch);
            dispatch(
              process({
                type: UPDATE_INSTITUTION,
                payload: {
                  title: "Update Institution",
                  type,
                  method,
                  defaultValues,
                },
              })
            );
            break;
          default:
            break;
        }
        break;
      case "USER":
        //findTenantList()(dispatch);
        findContactTypeList()(dispatch);
        findCountryList()(dispatch);
        findContactList()(dispatch);
        findRoleList()(dispatch);
        findServiceProviderList()(dispatch);
        if (method === "POST") {
          findDefaultAddress(getContext().instanceId)(dispatch);
          findContactList()(dispatch);
          dispatch(
            process({
              type: NEW_USER,
              payload: {
                title: "New USER",
                type,
                method,
                defaultValues: {
                  id: 0,
                  contactType: [
                    {
                      id: 9,
                      name: "Ebs member",
                      category: { name: "Person" },
                    },
                  ],
                  phones: [{ id: 0 }],
                  addresses: [{ id: 0, default: true }],
                },
              },
            })
          );
        } else {
          dispatch(
            process({
              type: UPDATE_USER,
              payload: {
                title: "Update User",
                type,
                method,
                defaultValues,
              },
            })
          );
        }
        break;
      default:
        break;
    }
  };
/**
 * @param {String} method
 * @params {Object} defaultValues
 * @returns void
 */
export const roleProcess =
  ({ method, defaultValues }) =>
  async (dispatch) => {
    switch (method) {
      case "POST":
        dispatch(
          process({
            type: NEW_ROLE,
            payload: {
              title: "New Role",
              method,
            },
          })
        );
        break;
      case "PUT":
        dispatch(
          process({
            type: NEW_ROLE,
            payload: {
              title: "Update Role",
              method,
              defaultValues,
            },
          })
        );
      default:
        break;
    }
  };

export const findContactList = () => async (dispatch) => {
  try {
    dispatch(process({ type: GET_FIND_CONTACT }));
    const { data, errors } = await client.query({
      query: FIND_CONTACT_LIST,
      variables: {
        page: { number: 1, size: 100 },
        filters: [{ col: "category.name", mod: "EQ", val: "Institution" }],
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(setData(SET_FIND_CONTACT, data.findContactList.content));
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};
