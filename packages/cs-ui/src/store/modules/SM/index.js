
import { FIND_SERVICE_PROVIDER, FIND_SERVICE_LIST } from "utils/apollo/gql/sm";
import { clientSM } from '../../..//utils/apollo'

export const initialState = {
    error: null,
    loading: false,
    success: false,
    deleteSuccess: false,
    serviceProviderList:null,
    serviceProvider:null
};

const GET_SERVICE_LIST = "[SM] GET_SERVICE_LIST";
const SET_SERVICE_LIST = "[SM] SET_SERVICE_LIST";
const GET_SERVICE_PROVIDER = "[SM] GET_SERVICE_PROVIDER";
const SET_SERVICE_PROVIDER = "[SM] SET_SERVICE_PROVIDER";

const process = (type) => ({
    type,
});
const setData = (data) => ({
    type:SET_SERVICE_LIST,
    payload :data,
});
const setDataProvider = (data) => ({
  type:SET_SERVICE_PROVIDER,
  payload :data,
});

export default function Reducer(state = initialState, { type, payload }) {
    switch (type) {

        case GET_SERVICE_LIST:
            return { ...state, loading: true };
        case SET_SERVICE_LIST:
            return { ...state, serviceProviderList: payload, loading: false };
        case SET_SERVICE_PROVIDER:
            return { ...state, serviceProvider: payload, loading: false };
        default:
            return state;
    }
}


export const findServiceProviderById = (id) => async (dispatch)=>{
try { 
              clientSM
                .query({
                  query: FIND_SERVICE_PROVIDER,
                  variables: {
                    id: id,
                  },
                  fetchPolicy: "network-only",
                })
                .then(({ data, errors }) => {
                  
                  if (errors) {
                    reject(errors);
                  } else {
                dispatch(setDataProvider(data.findServiceProvider))
                  }
                });    
} catch (error) {
    console.log(error)
}
}

export const findServiceProviderList = (code) => async (dispatch)=>{
  try {
                clientSM
                  .query({
                    query: FIND_SERVICE_LIST,
                    variables: {
                      code: code,
                    },
                    fetchPolicy: "network-only",
                  })
                  .then(({ data, errors }) => {
                    
                    if (errors) {
                      reject(errors);
                    } else {
                      
                  dispatch(setData(data.findServiceProviderList.content[0]))
                    }
                  });    
  } catch (error) {
      console.log(error)
  }
  }  

 /**
 * @param {ID} id
 * @param {String} type
 * @returns {void}
 */
 export const findServiceById = async ({ id }) =>
 new Promise(async (resolve, reject) => {
   try {    
     const { errors, data } = await clientSM.query({
       query: FIND_SERVICE_PROVIDER,
       variables: { id:id},
       fetchPolicy: "no-cache",
     });
    
     if (errors) {
       throw new Error(`${errors[0].message}`);
     }
       resolve({data:data});
   } catch (error) {
     reject(error);
   }
 });

  export async function findServiceByIdOrgUnit ( id ) {
    try {    
      const { errors, data } = await clientSM.query({
        query: FIND_SERVICE_PROVIDER,
        variables: { id:id},
        fetchPolicy: "no-cache",
      });
     return data.findServiceProvider ? data : false;
    } catch (error) {
      return {};
    }
  };