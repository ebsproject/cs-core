import { client } from "utils/apollo";
import { showMessage } from "store/modules/message";
//import _ from "lodash";
import {
  CREATE_CONTACT,
  MODIFY_CONTACT,
  DELETE_CONTACT,
  REMOVE_ADDRESS,
  DELETE_CONTACT_INFO,
  CREATE_CONTACT_INFO,
  MODIFY_CONTACT_INFO,
  CREATE_HIERARCHY,
  CREATE_CONTACT_INFO_TYPE,
  FIND_HIERARCHY_LIST,
  DELETE_HIERARCHY,
} from "utils/apollo/gql/crm";
import { CREATE_USER, CREATE_USER_ROLE } from "utils/apollo/gql/userManagement";
import { clientRest } from "utils/axios/axios";
/*
 Initial state and properties
 */
export const initialState = {
  error: null,
  loading: false,
  success: false,
  primaryAddress: false,
  contactInfoSuccess: false,
};
/*
 Action types
 */
const ERROR = "[CRM] ERROR";
const MUTATION_MODIFY_CGIAR_START = "[CRM] MUTATION_MODIFY_CGIAR_START";
const MUTATION_MODIFY_CGIAR_SUCCESS = "[CRM] MUTATION_MODIFY_CGIAR_SUCCESS";
const MUTATION_CREATE_CONTACT_START = "[CRM] MUTATION_CREATE_CONTACT_START";
const MUTATION_CREATE_CONTACT_SUCCESS = "[CRM] MUTATION_CREATE_CONTACT_SUCCESS";
const MUTATION_MODIFY_CONTACT_START = "[CRM] MUTATION_MODIFY_CONTACT_START";
const MUTATION_MODIFY_CONTACT_SUCCESS = "[CRM] MUTATION_MODIFY_CONTACT_SUCCESS";
const MUTATION_MODIFY_ADDRESS_START = "[CRM] MUTATION_MODIFY_ADDRESS_START";
const MUTATION_MODIFY_ADDRESS_SUCCESS = "[CRM] MUTATION_MODIFY_ADDRESS_SUCCESS";
const MUTATION_DELETE_CONTACT_START = "[CRM] MUTATION_DELETE_CONTACT_START";
const MUTATION_DELETE_CONTACT_SUCCESS = "[CRM] MUTATION_DELETE_CONTACT_SUCCESS";
const MUTATION_CREATE_HIERARCHY_START = "[CRM] MUTATION_CREATE_HIERARCHY_START";
const MUTATION_CREATE_HIERARCHY_SUCCESS =
  "[CRM] MUTATION_CREATE_HIERARCHY_SUCCESS";
const MUTATION_ASSIGN_NEW_CONTACT_START =
  "[CRM] MUTATION_ASSIGN_NEW_CONTACT_START";
const MUTATION_ASSIGN_NEW_CONTACT_SUCCESS =
  "[CRM] MUTATION_ASSIGN_NEW_CONTACT_SUCCESS";
const MUTATION_CREATE_CONTACT_INFO_START =
  "[CRM] MUTATION_CREATE_CONTACT_INFO_START";
const MUTATION_CREATE_CONTACT_INFO_SUCCESS =
  "[CRM] MUTATION_CREATE_CONTACT_INFO_SUCCESS";
const MUTATION_MODIFY_CONTACT_INFO_START =
  "[CRM] MUTATION_MODIFY_CONTACT_INFO_START";
const MUTATION_MODIFY_CONTACT_INFO_SUCCESS =
  "[CRM] MUTATION_MODIFY_CONTACT_INFO_SUCCESS";
const MUTATION_DELETE_CONTACT_INFO_START =
  "[CRM] MUTATION_DELETE_CONTACT_INFO_START";
const MUTATION_DELETE_CONTACT_INFO_SUCCESS =
  "[CRM] MUTATION_DELETE_CONTACT_INFO_SUCCESS";
export const MUTATION_CREATE_INFO_TYPE_START =
  "[CRM] MUTATION_CREATE_INFO_TYPE_START";
export const MUTATION_CREATE_INFO_TYPE_SUCCESS =
  "[CRM] MUTATION_CREATE_INFO_TYPE_SUCCESS";
const CLEAN_CONTACT_SUCCESS = "[CRM] CLEAN_CONTACT_SUCCESS";
const MUTATION_REMOVE_ADDRESS_START = "[CRM] MUTATION_REMOVE_ADDRESS_START";
const MUTATION_REMOVE_ADDRESS_SUCCESS = "[CRM] MUTATION_REMOVE_ADDRESS_SUCCESS";
const CLEAN_CONTACT_INFO_SUCCESS = "[CRM] CLEAN_CONTACT_INFO_SUCCESS";
/*
 Arrow function for change state
 */
const process = (type) => ({
  type,
});
const setError = (payload) => ({
  type: ERROR,
  payload,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case MUTATION_CREATE_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_MODIFY_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_DELETE_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_ADDRESS_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_ADDRESS_SUCCESS:
      return { ...state, success: true, loading: false, primaryAddress: true };
    case MUTATION_CREATE_HIERARCHY_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_HIERARCHY_SUCCESS:
    case MUTATION_MODIFY_CGIAR_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CGIAR_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_DELETE_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_ASSIGN_NEW_CONTACT_START:
      return { ...state, loading: true };
    case MUTATION_ASSIGN_NEW_CONTACT_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_CREATE_CONTACT_INFO_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: true, loading: false };
    case MUTATION_MODIFY_CONTACT_INFO_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: true, loading: false };
    case MUTATION_DELETE_CONTACT_INFO_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CGIAR_START:
      return { ...state, loading: true };
    case MUTATION_MODIFY_CGIAR_SUCCESS:
      return { ...state, success: true, loading: false };
    case MUTATION_DELETE_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: true, loading: false };
    case MUTATION_CREATE_INFO_TYPE_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_INFO_TYPE_SUCCESS:
      return { ...state, loading: false, contactInfoSuccess: true };
    case MUTATION_REMOVE_ADDRESS_START:
      return { ...state, loading: true };
    case MUTATION_REMOVE_ADDRESS_SUCCESS:
      return { ...state, success: true, loading: false, primaryAddress: true };
    case CLEAN_CONTACT_SUCCESS:
      return { ...state, success: false, primaryAddress: false };
    case CLEAN_CONTACT_INFO_SUCCESS:
      return { ...state, contactInfoSuccess: false };
    case ERROR:
      return { ...state, loading: false, error: payload };
    default:
      return state;
  }
}
/**
 * @param {String} type
 * @param {Object} data
 * @returns void
 */
export const assignNewContact =
  ({ type, data }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_ASSIGN_NEW_CONTACT_START));

      // * Contact Input
      let contactInput = new Object({
        id: 0,
        contactTypeIds: data.contactType.map((item) => item.id),
        addresses: data.addresses.map((item) => ({
          default: item.default || false,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.country.id,
          purposeIds: [item.purposeIds],
          id: 0,
        })),
        // contactInfos: contactInfos,
        person: {
          familyName: data.familyName,
          givenName: data.givenName,
          additionalName: data.additionalName,
          gender: data.gender.name,
          salutation: data.salutation === undefined ? "" : data.salutation.name,
        },
        institution: null,
        email: data.officialEmail,
        purposeIds: data.purposes.map((data) => Number(data.id)),
        tenantIds: data.tenants.map((item) => Number(item.id)),
      });
      // * Category
      switch (type) {
        case "PERSON":
          Object.assign(contactInput, {
            category: { id: 1, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        case "INSTITUTION":
          Object.assign(contactInput, {
            category: { id: 2, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        default:
          break;
      }
      const { errors, data: contactData } = await client.mutate({
        mutation: CREATE_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      const { errors: Assign } = await client.mutate({
        mutation: CREATE_HIERARCHY,
        variables: {
          hierarchy: {
            contactId: contactData.createContact.id,
            institutionId: Number(data.institutionId),
            principalContact: false,
          },
        },
      });
      if (Assign) {
        throw new Error(`${Assign[0].message}`);
      }
      dispatch(process(MUTATION_ASSIGN_NEW_CONTACT_SUCCESS));
      dispatch(
        showMessage({
          message: "Contact created and assigned successfully",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAN_CONTACT_SUCCESS));
    }
  };
/**
 * @param {String} type
 * @param {Object} data
 * @returns void
 */
export const createContact =
  ({ type, dataContact, createUser }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_CREATE_CONTACT_START));
      const tenants = [1];
      // * Contact Input
      let contactInput = new Object({
        id: 0,
        contactTypeIds: dataContact.contactType.map((item) => item.id),
        addresses: dataContact.addresses.map((item) => ({
          default: item.default || false,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.countryId,
          purposeIds: item.purposeIds,
          id: item.id ? item.id : 0,
        })),
        //  contactInfos: contactInfos,
        person:
          (type === "PERSON" && {
            familyName: dataContact.familyName,
            givenName: dataContact.givenName,
            additionalName: dataContact.additionalName,
            gender: dataContact.gender ? dataContact.gender.name : "",
            salutation: dataContact.salutation
              ? dataContact.salutation.name
              : "",
          }) ||
          null,
        institution:
          (type === "INSTITUTION" && {
            commonName: dataContact.commonName,
            legalName: dataContact.legalName,
            cgiar: dataContact.isCgiar || false,
            cropIds: dataContact.crop.map((item) => item.id),
          }) ||
          null,
        email: dataContact.officialEmail,
        purposeIds: dataContact.purposes.map((data) => Number(data.id)),
        tenantIds: tenants,
      });
      // * Category
      switch (type) {
        case "PERSON":
          Object.assign(contactInput, {
            category: { id: 1, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        case "INSTITUTION":
          Object.assign(contactInput, {
            category: { id: 2, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        default:
          break;
      }
      const { data, errors } = await client.mutate({
        mutation: CREATE_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      if (type === "PERSON") {
        dataContact.institutionInput.map((item) =>
          createHierarchy({
            contactId: data.createContact.id,
            institutionId: item.id,
            principalContact: false,
          })(dispatch)
        );
      }

      if (type === "PERSON" && createUser === "YES") {
        createNewUserForContact(data, dataContact.officialEmail);
      }
      dispatch(process(MUTATION_CREATE_CONTACT_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully created",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAN_CONTACT_SUCCESS));
    }
  };
/**
 * @param {Number/String} id
 * @returns void
 */
const createNewUserForContact = async (data, email) => {
  const { errors: usrErr, data: user } = await client.mutate({
    mutation: CREATE_USER,
    variables: {
      user: {
        id: 0,
        userName: email,
        contactId: Number(data?.createContact?.id) || 0,
        active: true,
        admin: false,
        tenantIds: [1],
      },
    },
  });
  const { errors: errorsSync } = await clientRest.get(
    `/sync/user/${user.createUser.id}`
  );
  if (errorsSync) {
    throw new Error(`${errorsSync[0].message}`);
  }
  if (usrErr) {
    throw new Error(`${usrErr[0].message}`);
  }
};

export const removeAddress = (data) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_REMOVE_ADDRESS_START));
    const { errors } = await client.mutate({
      mutation: REMOVE_ADDRESS,
      variables: { addressId: data.id, contactId: data.idUser },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_REMOVE_ADDRESS_SUCCESS));
    dispatch(
      showMessage({
        message: "Deleted Address",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};

export const modifyAddress = async (contactInput, dispatch) => {
  try {
    dispatch(process(MUTATION_MODIFY_ADDRESS_START));
    const { errors } = await client.mutate({
      mutation: MODIFY_CONTACT,
      variables: { contact: contactInput },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_MODIFY_ADDRESS_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully updated address",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};

export const modifyCgiar = (contactInput) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_MODIFY_CGIAR_START));
    const { errors } = await client.mutate({
      mutation: MODIFY_CONTACT,
      variables: { contact: contactInput },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_MODIFY_CGIAR_SUCCESS));
    // dispatch(
    //   showMessage({
    //     message: "Upgraded to Cgiar",
    //     variant: "success",
    //     anchorOrigin: {
    //       vertical: "top",
    //       horizontal: "right",
    //     },
    //   })
    // );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};
/**
 * @param {String} type
 * @param {Object} data
 * @returns void
 */
export const modifyContact =
  ({ type, dataContact }) =>
  async (dispatch) => {
    try {
      let contactInput = new Object({
        id: dataContact.id,
        contactTypeIds: dataContact.contactType.map((item) => item.id),
        tenantIds: dataContact.tenants.map((item) => item.id),
        addresses: dataContact.addresses.map((item, index) => ({
          default: item.default === undefined ? false : item.default,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.country.id,
          purposeIds: item.purposes.map((i) => Number(i.id)),
          id: Number(item.id) || 0,
        })),
        //contactInfos: contactInfos,
        person:
          (type === "PERSON" && {
            familyName: dataContact.familyName,
            givenName: dataContact.givenName,
            additionalName: dataContact.additionalName,
            gender: dataContact.gender ? dataContact.gender.name : "",
            salutation: dataContact.salutation
              ? dataContact.salutation.name
              : "",
          }) ||
          null,
        institution:
          (type === "INSTITUTION" && {
            commonName: dataContact.commonName,
            legalName: dataContact.legalName,
            cgiar: dataContact.isCgiar,
            cropIds: dataContact.crop.map((item) => item.id),
          }) ||
          null,
        email: dataContact.officialEmail,
        purposeIds: dataContact.purposes.map((data) => Number(data.id)),
        tenantIds: dataContact.tenants.map((item) => Number(item.id)),
      });
      // * Category
      switch (type) {
        case "PERSON":
          Object.assign(contactInput, {
            category: { id: 1, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        case "INSTITUTION":
          Object.assign(contactInput, {
            category: { id: 2, name: _.upperFirst(type.toLowerCase()) },
          });
          break;
        default:
          break;
      }
      dispatch(process(MUTATION_MODIFY_CONTACT_START));
      const { errors } = await client.mutate({
        mutation: MODIFY_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(process(MUTATION_MODIFY_CONTACT_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully updated information",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAN_CONTACT_SUCCESS));
    }
  };
/**
 * @param {Number} contactId
 * @returns void
 */
export const deleteContact = (contactId) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_DELETE_CONTACT_START));
    const { errors } = await client.mutate({
      mutation: DELETE_CONTACT,
      variables: { contactId: contactId },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_DELETE_CONTACT_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully deleted",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};
/**
 * @param {HierarchyInput} hierarchy
 * @returns void
 */
export const createHierarchy = (hierarchy) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_CREATE_HIERARCHY_START));
    const { errors } = await client.mutate({
      mutation: CREATE_HIERARCHY,
      variables: { hierarchy },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_HIERARCHY_SUCCESS));
    dispatch(
      showMessage({
        message: "Contact Assigned",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_SUCCESS));
  }
};
/**
 * @param {Object} contactInfo
 * @returns void
 */
const createContactInfo = (contactInfo) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_CREATE_CONTACT_INFO_START));
    delete contactInfo.undefined;
    Object.assign(contactInfo, {
      contactInfoTypeId: contactInfo.contactInfoType.id,
    });
    delete contactInfo.contactInfoType;
    const { errors } = await client.mutate({
      mutation: CREATE_CONTACT_INFO,
      variables: { contactInfo },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_CONTACT_INFO_SUCCESS));
    dispatch(
      showMessage({
        message: "Additional Information Added",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
  }
};
/**
 * @param {Object} contactInfo
 * @returns void
 */
const modifyContactInfo = (contactInfo) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_MODIFY_CONTACT_INFO_START));
    delete contactInfo.undefined;
    Object.assign(contactInfo, {
      contactInfoTypeId: contactInfo.contactInfoType.id,
    });
    delete contactInfo.contactInfoType;
    const { errors } = client.mutate({
      mutation: MODIFY_CONTACT_INFO,
      variables: { contactInfo },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_CONTACT_INFO_SUCCESS));
    dispatch(
      showMessage({
        message: "Additional Information Modified",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error,
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
  }
};
/**
 * @param {Int} contactInfoId
 * @returns void
 */
export const deleteContactInfo = (contactInfoId) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_DELETE_CONTACT_INFO_START));
    const { errors } = await client.mutate({
      mutation: DELETE_CONTACT_INFO,
      variables: { contactInfoId },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_DELETE_CONTACT_INFO_SUCCESS));
    dispatch(
      showMessage({
        message: "Additional Information Deleted",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
    dispatch(setError(error));
  } finally {
    dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
  }
};
/**
 * @param {String} type
 * @param {String} method
 * @param {Object} data
 * @returns void
 */
export const assignInstitution =
  ({ institutionData, contactId, previousInstitution }) =>
  async (dispatch) => {
    if (institutionData !== undefined) {
      const newInstitution = [];
      const toDeleteInstitution = [];
      if (
        previousInstitution === undefined ||
        previousInstitution.length === 0
      ) {
        Object.assign(newInstitution, institutionData);
      } else {
        // * New Institution
        institutionData.map((newI) => {
          if (
            previousInstitution.findIndex(
              (previousI) => previousI.id === newI.id
            ) === -1
          ) {
            newInstitution.push(newI);
          }
        });
        // * To delete Instution
        previousInstitution.map((previusI) => {
          if (
            institutionData.findIndex((newI) => newI.id === previusI.id) === -1
          ) {
            toDeleteInstitution.push(previusI);
          }
        });
      }
      try {
        dispatch(process(MUTATION_CREATE_HIERARCHY_START));
        toDeleteInstitution.map(async (item) => {
          const { errors: errorsDeleteSP } = await client.mutate({
            mutation: DELETE_HIERARCHY,
            variables: {
              contactId: contactId,
              institutionId: item.id,
            },
          });
          if (errorsDeleteSP) {
            throw new Error(`${errorsDeleteSP[0].message}`);
          }
        });
        newInstitution.map(async (item) => {
          const { errors: errorsCreateSP } = await client.mutate({
            mutation: CREATE_HIERARCHY,
            variables: {
              hierarchy: {
                contactId: contactId,
                institutionId: Number(item.id),
                principalContact: false,
              },
            },
          });
          if (errorsCreateSP) {
            throw new Error(`${errorsCreateSP[0].message}`);
          }
        });
        dispatch(process(MUTATION_CREATE_HIERARCHY_SUCCESS));
      } catch (error) {
        dispatch(setError(error));
        dispatch(
          showMessage({
            message: error,
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
      } finally {
        dispatch(process(CLEAN_CONTACT_SUCCESS));
      }
    }
  };
export const mutationContact =
  ({ type, method, data, addresses, lastInst, createUser }) =>
  async (dispatch) => {
    let dataContact = data;
    switch (method) {
      case "POST":
        createContact({ type, dataContact, createUser })(dispatch);
        break;
      case "PUT":
        modifyContact({ type, dataContact, addresses })(dispatch);
        assignInstitution({
          institutionData: dataContact.institutionInput,
          contactId: dataContact.id,
          previousInstitution: lastInst,
        })(dispatch);
        break;
      case "ASSIGN":
        assignNewContact({ type, data })(dispatch);
        break;
      default:
        break;
    }
  };
/**
 * @param {String} method
 * @param {Object} data
 * @returns void
 */
export const mutationContactInfo =
  ({ method, data }) =>
  async (dispatch) => {
    switch (method) {
      case "POST":
        createContactInfo(data)(dispatch);
        break;
      case "PUT":
        modifyContactInfo(data)(dispatch);
        break;
      default:
        break;
    }
  };
/**
 * @param {Object} data
 * @returns void
 */
export const createInfoType =
  ({ data }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_CREATE_INFO_TYPE_START));
      const { errors } = await client.mutate({
        mutation: CREATE_CONTACT_INFO_TYPE,
        variables: {
          contactInfoType: data,
        },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(process(MUTATION_CREATE_INFO_TYPE_SUCCESS));
      dispatch(
        showMessage({
          message: "New Info Type Created!",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      dispatch(setError(error));
    } finally {
      dispatch(process(CLEAN_CONTACT_INFO_SUCCESS));
    }
  };
