import { client } from "utils/apollo";
import { showMessage } from "store/modules/message";
import {
  CREATE_ROLE,
  CREATE_ROLE_PRODUCT,
  MODIFY_ROLE,
  DELETE_ROLE,
  DELETE_ROLE_PRODUCT,
  CREATE_USER,
  DELETE_USER,
  MODIFY_USER,
} from "utils/apollo/gql/userManagement";
import { CREATE_CONTACT, MODIFY_CONTACT } from "utils/apollo/gql/crm";
import { assignInstitution, createHierarchy, modifyContact } from "../CRM";
import { clientRest } from "utils/axios/axios";
import { CREATE_ROLE_RULE, DELETE_ROLE_RULE } from "utils/apollo/gql/rules";
import { createHierarchyUMPost } from "components/organism/OrganizationUnits/fuctionsOrganizations";
/*
 Initial state and properties
 */
export const initialState = {
  loading: false,
  success: false,
  error: null,
};
/*
 Action types
 */
const MUTATION_CREATE_USER_ROLE_START = "[UM] MUTATION_CREATE_USER_ROLE_START";
const MUTATION_CREATE_USER_ROLE_SUCCESS =
  "[UM] MUTATION_CREATE_USER_ROLE_SUCCESS";
const MUTATION_CREATE_ROLE_START = "[UM] MUTATION_CREATE_ROLE_START";
const MUTATION_CREATE_ROLE_SUCCESS = "[UM] MUTATION_CREATE_ROLE_SUCCESS";
const MUTATION_UPDATE_ROLE_START = "[UM] MUTATION_UPDATE_ROLE_START";
const MUTATION_UPDATE_ROLE_SUCCESS = "[UM] MUTATION_UPDATE_ROLE_SUCCESS";
const MUTATION_DELETE_ROLE_START = "[UM] MUTATION_DELETE_ROLE_START";
const MUTATION_DELETE_ROLE_SUCCESS = "[UM] MUTATION_DELETE_ROLE_SUCCESS";
const MUTATION_CREATE_USER_START = "[UM] MUTATION_CREATE_USER_START";
const MUTATION_CREATE_USER_SUCCESS = "[UM] MUTATION_CREATE_USER_SUCCESS";
const MUTATION_DELETE_USER_START = "[UM] MUTATION_DELETE_USER_START";
const MUTATION_DELETE_USER_SUCCESS = "[UM] MUTATION_DELETE_USER_SUCCESS";
const MUTATION_CREATE_HIERARCHY_START = "[UM] MUTATION_CREATE_HIERARCHY_START";
const MUTATION_CREATE_HIERARCHY_SUCCESS =
  "[UM] MUTATION_CREATE_HIERARCHY_SUCCESS";
const MUTATTION_UPDATE_ISACTIVE_START = "[UM] MUTATTION_UPDATE_ISACTIVE_START";
const MUTATTION_UPDATE_ISACTIVE_SUCCESS =
  "[UM] MUTATTION_UPDATE_ISACTIVE_SUCCESS";
const MUTATTION_UPDATE_ISADMIN_START = "[UM] MUTATTION_UPDATE_ISADMIN_START";
const MUTATTION_UPDATE_ISADMIN_SUCCESS =
  "[UM] MUTATTION_UPDATE_ISADMIN_SUCCESS";
const CLEAR_SUCCESS = "[UM] CLEAR_SUCCESS";
const ERROR = "[UM] ERROR";
/*
 Arrow function for change state
 */
const process = (type) => ({ type });
const setError = (payload) => ({
  type: ERROR,
  payload,
});
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case MUTATION_CREATE_USER_ROLE_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_USER_ROLE_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATION_CREATE_ROLE_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_ROLE_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATION_UPDATE_ROLE_START:
      return { ...state, loading: true };
    case MUTATION_UPDATE_ROLE_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATION_DELETE_ROLE_START:
      return { ...state, loading: true };
    case MUTATION_DELETE_ROLE_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATION_CREATE_USER_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_USER_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATION_DELETE_USER_START:
      return { ...state, loading: true };
    case MUTATION_DELETE_USER_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATION_CREATE_HIERARCHY_START:
      return { ...state, loading: true };
    case MUTATION_CREATE_HIERARCHY_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATTION_UPDATE_ISACTIVE_START:
      return { ...state, loading: true };
    case MUTATTION_UPDATE_ISACTIVE_SUCCESS:
      return { ...state, loading: false, success: true };
    case MUTATTION_UPDATE_ISADMIN_START:
      return { ...state, loading: true };
    case MUTATTION_UPDATE_ISADMIN_SUCCESS:
      return { ...state, loading: false, success: true };
    case CLEAR_SUCCESS:
      return { ...state, success: false };
    case ERROR:
      return { ...state, error: payload, loading: false };
    default:
      return state;
  }
}
// /**
//  * @param {Object} data
//  * @param {Object[]} lastRoles
//  * @returns
//  */
// export const assignRole =
//   ({ data, lastRoles }) =>
//   async (dispatch) => {
//     try {
//       dispatch(process(MUTATION_CREATE_USER_ROLE_START));
//       const newRoles = new Set();
//       const toDelete = new Set();
//       // * New roles
//       data.role.map((role) => {
//         // ? Doesn't exist a role already assigned?
//         if (lastRoles.findIndex((e) => e.id === role.id) === -1) {
//           newRoles.add(role);
//         }
//       });
//       // * Removed roles
//       lastRoles.map((rol) => {
//         const { role } = data;
//         // ? Are there roles assigned to delete?
//         if (role.findIndex((e) => e.id === rol.id) === -1) {
//           toDelete.add(rol);
//         }
//       });
//       // * Delete roles
//       for (let role of toDelete) {
//         (async function (item) {
//           // * here the value of role was passed into as the argument item
//           try {
//             const { errors } = await client.mutate({
//               mutation: DELETE_USER_ROLE,
//               variables: {
//                 userRole: {
//                   role: { id: Number(item.id), name: "" },
//                   user: {
//                     id: Number(data.userId),
//                     userName: "",
//                     contactId: Number(data.id),
//                     active: true,
//                   },
//                 },
//               },
//             });
//             if (errors) {
//               throw new Error(`${errors[0].message}`);
//             }
//           } catch (error) {
//             dispatch(setError(error));
//             dispatch(
//               showMessage({
//                 message: error.toString(),
//                 variant: "error",
//                 anchorOrigin: {
//                   vertical: "top",
//                   horizontal: "right",
//                 },
//               })
//             );
//           }
//         })(role);
//       }
//       // * Add new roles
//       for (let role of newRoles) {
//         (async function (item) {
//           try {
//             // * here the value of role was passed into as the argument item
//             const { errors } = await client.mutate({
//               mutation: CREATE_USER_ROLE,
//               variables: {
//                 userRole: {
//                   user: {
//                     id: Number(data.userId),
//                     userName: "",
//                     contactId: Number(data.id),
//                     active: true,
//                   },
//                   role: {
//                     id: Number(item.id),
//                     name: "",
//                   },
//                 },
//               },
//             });
//             if (errors) {
//               throw new Error(`${errors[0].message}`);
//             }
//           } catch (error) {
//             dispatch(setError(error));
//             dispatch(
//               showMessage({
//                 message: error.toString(),
//                 variant: "error",
//                 anchorOrigin: {
//                   vertical: "top",
//                   horizontal: "right",
//                 },
//               })
//             );
//           }
//         })(role);
//       }
//       dispatch(process(MUTATION_CREATE_USER_ROLE_SUCCESS));
//       dispatch(
//         showMessage({
//           message: "Contact created and assigned successfully",
//           variant: "success",
//           anchorOrigin: {
//             vertical: "top",
//             horizontal: "right",
//           },
//         })
//       );
//     } catch (error) {
//       dispatch(setError(error));
//       dispatch(
//         showMessage({
//           message: error.toString(),
//           variant: "error",
//           anchorOrigin: {
//             vertical: "top",
//             horizontal: "right",
//           },
//         })
//       );
//     } finally {
//       dispatch(process(CLEAR_SUCCESS));
//     }
//   };
/**
 * @param {Object} data
 * @returns void
 */
const createRole = (dataForm, setProcess) => async (dispatch) => {
  try {
    const { name, description, securityGroup, productFunction } = dataForm;

    if (!productFunction || productFunction["checked"].length === 0) {
      dispatch(
        showMessage({
          message: "Please select at least one permission for this role.",
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
      return;
    }

    dispatch(process(MUTATION_CREATE_ROLE_START));
    const { data, errors } = await client.mutate({
      mutation: CREATE_ROLE,
      variables: {
        role: { id: 0, name, description, securityGroup: "" },
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    const { errors: errRP } = await client.mutate({
      mutation: CREATE_ROLE_PRODUCT,
      variables: {
        roleProduct: {
          role: {
            id: Number(data.createRole.id),
            name: name,
          },
          productFunction: productFunction.checked.map((item) => ({
            id: item,
          })),
        },
      },
    });
    if (dataForm.rules && dataForm.rules.length > 0) {
      for (let i = 0; i < dataForm.rules.length; i++) {
        const _rules = dataForm.rules[i];
        await client.mutate({
          mutation: CREATE_ROLE_RULE,
          variables: {
            securityRuleRole: {
              id: 0,
              role: { id: Number(data.createRole.id), name: "" },
              rule: { id: Number(_rules.id), usedByWorkflow: false, name: "" },
            },
          },
        });
      }
    }

    if (errRP) {
      throw new Error(`${errRP[0].message}`);
    }
    dispatch(process(MUTATION_CREATE_ROLE_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully added new role",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(process(CLEAR_SUCCESS));
    setProcess(true);
  }
};
/**
 * @param {Object} dataForm
 * @returns void
 */
const updateRole =
  ({ dataForm, lastPFs, setProcess }) =>
  async (dispatch) => {
    try {
      const {
        id,
        name,
        description,
        securityGroup,
        productFunction,
        isSystem,
      } = dataForm;

      if (
        // !productFunction["expanded"] ||
        productFunction["checked"].length === 0
      ) {
        dispatch(
          showMessage({
            message: "Please select at least one permission for this role.",
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        );
        return;
      }
      dispatch(process(MUTATION_UPDATE_ROLE_START));
      const toDelete = [];
      const toAdd = [];
      lastPFs.map((item) => {
        if (productFunction.checked.indexOf(item) === -1) {
          toDelete.push(Number(item));
        }
      });
      productFunction.checked.map((item) => {
        if (lastPFs.indexOf(item) === -1) {
          toAdd.push(Number(item));
        }
      });
      const { errors } = await client.mutate({
        mutation: MODIFY_ROLE,
        variables: {
          role: {
            id: Number(id),
            name: name,
            description: description,
            securityGroup: securityGroup,
            system: isSystem,
          },
        },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      if (toDelete.length > 0) {
        const { errors: deleteRoles } = await client.mutate({
          mutation: DELETE_ROLE_PRODUCT,
          variables: {
            roleId: Number(id),
            productFunctionID: toDelete,
          },
        });
        if (deleteRoles) {
          throw new Error(`${deleteRoles[0].message}`);
        }
      }
      if (toAdd.length > 0) {
        const { errors: createRoleProduct } = await client.mutate({
          mutation: CREATE_ROLE_PRODUCT,
          variables: {
            roleProduct: {
              role: {
                id: Number(id),
                name: name,
              },
              productFunction: toAdd.map((item) => ({ id: item })),
            },
          },
        });
        if (createRoleProduct) {
          throw new Error(`${createRoleProduct[0].message}`);
        }
      }
      // first delete all existing relationships
      await client.mutate({
        mutation: DELETE_ROLE_RULE,
        variables: {
          roleId: Number(id),
        },
      });
      // insert the new ones if exist
      if (dataForm.rules && dataForm.rules.length > 0) {
        for (let i = 0; i < dataForm.rules.length; i++) {
          const _rules = dataForm.rules[i];
          await client.mutate({
            mutation: CREATE_ROLE_RULE,
            variables: {
              securityRuleRole: {
                id: 0,
                role: { id: Number(id), name: "" },
                rule: {
                  id: Number(_rules.id),
                  usedByWorkflow: false,
                  name: "",
                },
              },
            },
          });
        }
      }
      dispatch(process(MUTATION_UPDATE_ROLE_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully updated role",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAR_SUCCESS));
      setProcess(true);
    }
  };
/**
 * @param {String} id
 * @returns void
 */
const deleteRole = (id) => async (dispatch) => {
  try {
    dispatch(process(MUTATION_DELETE_ROLE_START));
    // delete all role-rule relationships
    await client.mutate({
      mutation: DELETE_ROLE_RULE,
      variables: {
        roleId: Number(id),
      },
    });

    const { errors } = await client.mutate({
      mutation: DELETE_ROLE,
      variables: {
        id: Number(id),
      },
    });
    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(process(MUTATION_DELETE_ROLE_SUCCESS));
    dispatch(
      showMessage({
        message: "Successfully deleted role",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(setError(error));
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } finally {
    dispatch(process(CLEAR_SUCCESS));
  }
};
/**
 * @param {Object} data
 * @returns void
 */
const createUser =
  ({ dataForm, memberOu }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_CREATE_USER_START));
      // * Contact Infos
      const contactInfos = [];
      const tenants = [1];
      // * Contact Input
      const contactInput = {
        id: dataForm.id ? dataForm.id : 0,
        category: { id: 1, name: "Person" },
        tenantIds: tenants,
        contactTypeIds: [...dataForm.contactType.map((item) => item.id), 9],
        addresses: dataForm.addresses.map((item) => ({
          default: item.default || false,
          location: item.location,
          region: item.region,
          streetAddress: item.streetAddress,
          zipCode: item.zipCode,
          countryId: item.countryId,
          purposeIds: item.purposeIds,
          id: item.id || 0,
        })),
        contactInfos: contactInfos,
        person: {
          familyName: dataForm.familyName,
          givenName: dataForm.givenName,
          additionalName: dataForm.additionalName,
          gender: dataForm.gender.name,
          salutation:
            dataForm.salutation === undefined ? "" : dataForm.salutation.name,
        },
        email: dataForm.userName,
        purposeIds: dataForm.purposes.map((data) => Number(data.id)),
      };
      // * Create or modify a Contact
      const { errors, data } = await client.mutate({
        mutation:
          dataForm.id === 0 || dataForm.id === undefined
            ? CREATE_CONTACT
            : MODIFY_CONTACT,
        variables: { contact: contactInput },
      });
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      // * Create a new User
      const { errors: usrErr, data: user } = await client.mutate({
        mutation: CREATE_USER,
        variables: {
          user: {
            id: 0,
            userName: dataForm.userName,
            contactId: Number(data?.createContact?.id) || Number(dataForm.id),
            active: true,
            admin: dataForm.isAdmin === "admin" ? true : false,
            tenantIds: tenants,
          },
        },
      });
      
      if (usrErr) {
        throw new Error(`${usrErr[0].message}`);
      }

      for (let i = 0; i < dataForm.institutionInput.length; i++) {
        const item = dataForm.institutionInput[i];
        await createHierarchy({
          contactId: data.createContact.id,
          institutionId: item.id,
          principalContact: false,
        })(dispatch);
      }

      //Save Units or Programs from User Management "POST"
      if (memberOu.length !== 0) {
        memberOu.map(async (item) => {
          const dataItem = {
            contactId: data.createContact.id,
            institutionId: item.institution.id,
            principalContact: false,
          };
          await createHierarchyUMPost({
            createHierarchy: dataItem,
            idRol: Number(item.rol.id),
          });
        });
      }

      // User synchronization with CB
      if (window.location.hostname !== "localhost") {
        try {
          await clientRest.get(`/sync/user/${user.createUser.id}`);
        } catch (error) {
          console.error(error);
        }
      }

      dispatch(process(MUTATION_CREATE_USER_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully created new user",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error.toString()));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAR_SUCCESS));
    }
  };

const modifyUser = async ({ data }) => {
  try {
    const tenants = [1];
    const { errors: usrErr, data: user } = await client.mutate({
      mutation: MODIFY_USER,
      variables: {
        user: {
          id: data.userId,
          userName: data.userName,
          contactId: Number(data.id),
          active: data.active,
          admin: data.isAdmin === "admin" ? true : false,
          tenantIds: tenants,
        },
      },
    });
    if (usrErr) {
      throw new Error(`${usrErr[0].message}`);
    }
  } catch {}
};

export const modifyIsActive = async ({ data, dispatch }) => {
  try {
    const { errors } = await client.mutate({
      mutation: MODIFY_USER,
      variables: {
        user: data,
      },
    });
    if (window.location.hostname !== "localhost") {
      const { errors: errorsSync } = await clientRest.get(
        `/sync/user/${data.id}`
      );
      if (errorsSync) {
        throw new Error(`${errorsSync[0].message}`);
      }
    }

    if (errors) {
      throw new Error(`${errors[0].message}`);
    }
    dispatch(
      showMessage({
        message: "User status changed successfully",
        variant: "success",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  } catch (error) {
    dispatch(
      showMessage({
        message: error.toString(),
        variant: "error",
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
};

/**
 * @param {Int} id
 * @returns void
 */
export const deleteUser =
  ({ id }) =>
  async (dispatch) => {
    try {
      dispatch(process(MUTATION_DELETE_USER_START));
      const { errors } = await client.mutate({
        mutation: DELETE_USER,
        variables: { id },
      });
      const { errors: errorsSync } = await clientRest.get(`/sync/user/${id}`);
      if (errorsSync) {
        throw new Error(`${errorsSync[0].message}`);
      }
      if (errors) {
        throw new Error(`${errors[0].message}`);
      }
      dispatch(process(MUTATION_DELETE_USER_SUCCESS));
      dispatch(
        showMessage({
          message: "Successfully deleted user",
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } catch (error) {
      dispatch(setError(error));
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } finally {
      dispatch(process(CLEAR_SUCCESS));
    }
  };
/**
 * @param {Object} data
 * @param {String} method
 * @returns void
 */
export const mutationRole =
  ({ data, method, id, lastPFs, setProcess }) =>
  async (dispatch) => {
    switch (method) {
      case "POST":
        createRole(data, setProcess)(dispatch);
        break;
      case "PUT":
        updateRole({ dataForm: data, lastPFs, setProcess })(dispatch);
        break;
      case "DELETE":
        deleteRole(id)(dispatch);
      default:
        break;
    }
  };
/**
 * @param {Object} data
 * @param {String} method
 * @returns void
 */
export const mutationUser =
  ({ data, method, lastInst, memberOu }) =>
  async (dispatch) => {
    switch (method) {
      case "POST":
        await createUser({ dataForm: data, memberOu })(dispatch);
        break;
      case "PUT":
        await modifyContact({ type: "PERSON", dataContact: data })(dispatch);
        await assignInstitution({
          institutionData: data.institutionInput,
          contactId: data.id,
          previousInstitution: lastInst,
        })(dispatch);
        await modifyUser({ data });
        if (window.location.hostname !== "localhost") {
          console.log("SYNC");
          const { errors: errorsSync } = await clientRest.get(
            `/sync/user/${data.userId}`
          );
          if (errorsSync) {
            throw new Error(`${errorsSync[0].message}`);
          }
        }

        break;
      default:
        break;
    }
  };
