import fetch from "cross-fetch";
import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  ApolloLink,
  from,
} from "@apollo/client";
import {
  getTokenId,
  getContext,
  getCoreSystemContext,
  getDomainContext,
} from "@ebs/layout";

const { graphqlUri, cbGraphqlUri } = getCoreSystemContext();

const httpLink = new HttpLink({
  uri: graphqlUri,
  fetch,
});

const httpLinkSM = new HttpLink({
  uri: `${getDomainContext("sm").sgContext}graphql`,
  fetch,
});

const httpLinkCb = new HttpLink({
  uri: cbGraphqlUri,
  fetch,
});

const authLink = new ApolloLink((operation, forward) => {
  operation.setContext(({ headers }) => ({
    headers: {
      ...headers,
      authorization: `Bearer ${getTokenId()}`,
    },
  }));
  return forward(operation);
});

export const client = new ApolloClient({
  link: from([authLink, httpLink]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});

export const clientSM = new ApolloClient({
  link: from([authLink, httpLinkSM]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});

export const clientCb = new ApolloClient({
  link: from([authLink, httpLinkCb]),
  cache: new InMemoryCache({
    addTypename: false,
  }),
});
