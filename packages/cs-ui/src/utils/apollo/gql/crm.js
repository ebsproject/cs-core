import { gql } from "@apollo/client";
// TODO: QUERIES
/**
 * @graph FIND_CONTACT_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_LIST = gql`
  query FIND_CONTACT_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        tenants {
          id
          name
          organization {
            id
            name
          }
        }
        id
        users {
          id
          userName
        }
        purposes {
          id
          name
        }
        category {
          id
          name
        }
        institution {
          id
          commonName
          legalName
          isCgiar
          crops {
            id
            name
          }
          principalContact {
            person {
              givenName
              familyName
            }
          }
        }
        addresses {
          id
          country {
            id
            name
          }
          purposes {
            id
            name
            category {
              name
            }
          }
          location
          region
          zipCode
          fullAddress
          additionalStreetAddress
          streetAddress
          default
        }
        contactTypes {
          id
          name
          category {
            id
            name
          }
        }
        contactInfos {
          id
          value
          default
          contactInfoType {
            id
            name
          }
        }
        email
        person {
          familyName
          givenName
          additionalName
          jobTitle
          gender
          salutation
        }
        parents {
          id
          institution {
            category {
              id
              name
            }
            institution {
              legalName
            }
          }
        }
        createdBy {
          id
          userName
        }
        updatedBy {
          id
          userName
        }
        createdOn
        updatedOn
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT_INFO_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_INFO_TYPE_LIST = gql`
  query FIND_CONTACT_INFO_TYPE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactInfoTypeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
      }
    }
  }
`;
// Find tenant list to create or modify contact and users tenant relationship
export const FIND_TENANT_LIST = gql`
  query {
    findTenantList {
      content {
        id
        name
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_TYPE_LIST = gql`
  query FIND_CONTACT_TYPE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactTypeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        category {
          id
          name
        }
      }
    }
  }
`;
/**
 * @graph FIND_PURPOSE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_PURPOSE_LIST = gql`
  query FIND_PURPOSE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findPurposeList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        category {
          name
        }
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT_INFO_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_HIERARCHY_LIST = gql`
  query FIND_HIERARCHY_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findHierarchyList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        contact {
          id
          person {
            givenName
            familyName
          }
        }
        institution {
          id
          institution {
            legalName
            commonName
          }
        }
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT
 * @param id: contact id
 */
export const FIND_CONTACT = gql`
  query FIND_CONTACT($id: ID!) {
    findContact(id: $id) {
      id
      category {
        id
      }
      parents {
        id
        principalContact
        institution {
          id
          category {
            id
            name
          }
          addresses {
            id
            country {
              id
              name
            }
            purposes {
              id
              name
              category {
                name
              }
            }
            location
            region
            zipCode
            streetAddress
            default
          }
          institution {
            id
            commonName
            isCgiar
            legalName
          }
        }
      }
      tenants {
        id
        name
        organization {
          id
          name
        }
      }
      category {
        name
        id
      }
      addresses {
        id
        country {
          id
          name
        }
        purposes {
          id
          name
          category {
            name
          }
        }
        fullAddress
        location
        region
        zipCode
        streetAddress
        default
      }
      contactTypes {
        id
        name
        category {
          id
          name
        }
      }
      contactInfos {
        id
        value
        default
        contactInfoType {
          id
          name
        }
      }
      person {
        familyName
        givenName
        additionalName
        gender
        salutation
        institutions {
          commonName
          legalName
        }
      }
      institution {
        isCgiar
        commonName
        legalName
        contacts {
          id
          email
          person {
            givenName
            familyName
          }
        }
        principalContact {
          id
          email
          person {
            givenName
          }
        }
      }
      email
      purposes {
        id
        name
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT_NAME
 * @param id: contact id
 */
export const FIND_CONTACT_NAME = gql`
  query FIND_CONTACT($id: ID!) {
    findContact(id: $id) {
      id
      person {
        familyName
        givenName
        additionalName
      }
    }
  }
`;
/**
 * @graph FIND_CONTACT
 * @param id: contact id
 */
export const FIND_CONTACT_INSTITUTIONS = gql`
  query FIND_CONTACT($id: ID!) {
    findContact(id: $id) {
      id
      tenants {
        id
        name
        organization {
          id
          name
        }
      }
      category {
        name
        id
      }
      addresses {
        id
        country {
          id
          name
        }
        purposes {
          id
          name
          category {
            name
          }
        }
        location
        region
        fullAddress
        zipCode
        streetAddress
        default
      }
      contactTypes {
        id
        name
        category {
          id
          name
        }
      }
      contactInfos {
        id
        value
        default
        contactInfoType {
          id
          name
        }
      }
      person {
        familyName
        givenName
        additionalName
        gender
        salutation
        institutions {
          commonName
          legalName
        }
      }
      institution {
        isCgiar
        commonName
        legalName
        crops {
          id
          name
        }
        contacts {
          id
          email
          person {
            givenName
            familyName
          }
        }
        principalContact {
          id
          email
          person {
            givenName
          }
        }
      }
      email
      purposes {
        id
        name
      }
    }
  }
`;
/**
 * @graph FIND_COUNTRY_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_COUNTRY_LIST = gql`
  query FIND_COUNTRY_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findCountryList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
      }
    }
  }
`;

/**
 * @graph FIND_UNITS_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_UNITS_LIST = gql`
  query FIND_UNITS_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        institution {
          legalName
          commonName
        }
      }
    }
  }
`;
/**
 * @graph FIND_ORG_UNITS_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_ORG_UNITS_LIST = gql`
  query FIND_ORG_UNITS_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      size
      number
      numberOfElements
      content {
        id
        createdBy {
          id
          userName
        }
        updatedBy {
          id
          userName
        }
        createdOn
        updatedOn
        category {
          id
          name
        }
        institution {
          legalName
          commonName
          unitType {
            name
          }
          crops {
            id
            name
          }
        }
        parents {
          institution {
            institution {
              legalName
              commonName
            }
          }
        }
      }
    }
  }
`;
/**
 * @graph FIND_ORG_UNITS_ID
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_ORG_UNITS_ID = gql`
  query FIND_ORG_UNITS_ID($id: ID!) {
    findContact(id: $id) {
      id
      category {
        id
        name
      }
      institution {
        legalName
        unitType {
          id
          name
        }
        commonName
        crops {
          id
          name
        }
      }
      parents {
        institution {
          id
          addresses {
            id
            country {
              id
              name
            }
            purposes {
              id
              name
              category {
                name
              }
            }
            fullAddress
            additionalStreetAddress
            location
            region
            zipCode
            streetAddress
            default
          }
          institution {
            id
            legalName
            commonName
          }
        }
      }
      members {
        id
        contact {
          id
          person {
            familyName
            givenName
          }
        }
        roles {
          id
          name
        }
      }
      workflows {
        provide
        id
        workflow {
          id
          name
        }
      }
      addresses {
        id
        country {
          id
          name
        }
        purposes {
          id
          name
          category {
            name
          }
        }
        fullAddress
        additionalStreetAddress
        location
        region
        zipCode
        streetAddress
        default
      }
    }
  }
`;

// TODO: CREATE
/**
 * @graph CREATE_CONTACT
 * @param contact: {
 * * id: Int!,
 * * category:
 * * ContactTypeCategory!,
 * * contactTypeIds: [Int!],
 * * addresses: [AddressesInput!],
 * * ContactInfos: [ContactInfoInput!],
 * * person: PersonInput,
 * * tenants:[Int!],
 * * institution: InstitutionInput
 * }
 */
export const CREATE_CONTACT = gql`
  mutation CREATE_CONTACT($contact: ContactInput!) {
    createContact(contact: $contact) {
      id
    }
  }
`;
/**
 * @param {HierarchyInput!} hierarchy
 */
export const CREATE_HIERARCHY = gql`
  mutation CREATE_HIERARCHY($hierarchy: HierarchyInput!) {
    createHierarchy(hierarchy: $hierarchy) {
      id
      contact {
        id
      }
    }
  }
`;
/**
 * @param {roleId} roleId
 * @param {contactHierarchyId} contactHierarchyId
 */
export const CREATE_ROLE_CONTACT_HIERARCHY = gql`
  mutation CREATE_ROLE_CONTACT_HIERARCHY(
    $roleId: Int!
    $contactHierarchyId: Int!
  ) {
    createRoleContactHierarchy(
      roleId: $roleId
      contactHierarchyId: $contactHierarchyId
    )
  }
`;
/**
 * @param {ContactInfoInput!} contactInfo
 */
export const CREATE_CONTACT_INFO = gql`
  mutation CREATE_CONTACT_INFO($contactInfo: ContactInfoInput!) {
    createContactInfo(contactInfo: $contactInfo) {
      id
    }
  }
`;
/**
 * @graphql CREATE_CONTACT_INFO_TYPE
 * @param {ContactInfoTypeInput} contactInfoType
 */
export const CREATE_CONTACT_INFO_TYPE = gql`
  mutation CREATE_CONTACT_INFO_TYPE($contactInfoType: ContactInfoTypeInput!) {
    createContactInfoType(contactInfoType: $contactInfoType) {
      id
    }
  }
`;

export const CREATE_ORGANIZATION_UNIT = gql`
  mutation CREATE_ORGANIZATION_UNIT($input: OrganizationalUnitInput!) {
    createOrganizationalUnit(input: $input) {
      id
    }
  }
`;
// TODO: MODIFY
/**
  * @graph MODIFY_CONTACT
  * @param contact: { 
  *
  }
*/
export const MODIFY_CONTACT = gql`
  mutation MODIFY_CONTACT($contact: ContactInput!) {
    modifyContact(contact: $contact) {
      id
    }
  }
`;

export const MODIFY_ORGANIZATION_UNIT = gql`
  mutation MODIFY_ORGANIZATION_UNIT($input: OrganizationalUnitInput!) {
    modifyOrganizationalUnit(input: $input) {
      id
    }
  }
`;
/**
 * @param {ContactInfoInput!} contactInfo
 */
export const MODIFY_CONTACT_INFO = gql`
  mutation MODIFY_CONTACT_INFO($contactInfo: ContactInfoInput!) {
    modifyContactInfo(contactInfo: $contactInfo) {
      id
    }
  }
`;
/**
 * @param {HierarchyInput!} hierarchy
 */
export const MODIFY_HIERARCHY = gql`
  mutation MODIFY_HIERARCHY($hierarchy: HierarchyInput!) {
    modifyHierarchy(hierarchy: $hierarchy) {
      contact {
        id
      }
    }
  }
`;
// TODO: DELETE
/**
@graph DELETE_CONTACT
@param contactId: Contact id
*/
export const DELETE_CONTACT = gql`
  mutation DELETE_CONTACT($contactId: Int!) {
    deleteContact(contactId: $contactId)
  }
`;
/**
@graph DELETE_CONTACT_INFO
@param contactId: Contact Info id
*/
export const DELETE_CONTACT_INFO = gql`
  mutation REMOVE_CONTACT_INFO($contactInfoId: Int!) {
    removeContactInfo(contactInfoId: $contactInfoId)
  }
`;
/**
 * @graphql REMOVE_ADDRESS
 * @param {Int} id
 */
export const REMOVE_ADDRESS = gql`
  mutation REMOVE_ADDRESS($addressId: Int, $contactId: Int) {
    removeAddress(addressId: $addressId, contactId: $contactId)
  }
`;

/**
 * @graphql DELETE_ORGANIZATION_UNIT
 * @param {Int} id
 */
export const DELETE_ORGANIZATION_UNIT = gql`
  mutation DELETE_ORGANIZATION_UNIT($id: Int!) {
    deleteOrganizationalUnit(id: $id)
  }
`;

/**
 * @graphql DELETE_ADRESSS
 * @param {Int} id
 */
export const DELETE_ADDRESS = gql`
  mutation DELETE_ADDRESS($addressId: Int!) {
    deleteAddress(addressId: $addressId)
  }
`;

/**
 * @graphql DELETE_HIERARCHY
 * @variable {Int} contactId
 * @variable {Int} institutionId
 */
export const DELETE_HIERARCHY = gql`
  mutation DELETE_HIERARCHY($contactId: Int!, $institutionId: Int!) {
    deleteHierarchy(contactId: $contactId, institutionId: $institutionId)
  }
`;

/**
 * @graph FIND_CONTACT_INFO_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CROP_LIST = gql`
  query FIND_CROP_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findCropList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        name
        code
        description
        contacts {
          institution {
            id
            commonName
            legalName
          }
        }
      }
    }
  }
`;

/**
 * @graph FIND_CONTACT_INFO_TYPE_LIST
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_CONTACT_LIST_OU = gql`
  query FIND_CONTACT_LIST_OU(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        category {
          id
          name
        }
        users {
          id
          userName
        }
        person {
          familyName
          givenName
        }
        parents {
          id
          roles {
            id
            name
          }
          institution {
            category {
              id
              name
            }
            institution {
              legalName
            }
          }
        }
      }
    }
  }
`;

/**
 * @graph FIND_USER_LIST_OU
 * @param page: Page number and registers
 * @param sort: [Column and sort mode]
 * @param filters: [Filter mode, column and filter value]
 * @param disjunctionFilter: There are more than one filter applied?
 */
export const FIND_USER_LIST_OU = gql`
  query FIND_USER_LISTT_OU(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findUserList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        userName
        contact {
          id
          parents {
            id
            institution {
              institution {
                legalName
                commonName
              }
            }
          }
          person {
            fullName
          }
        }
      }
    }
  }
`;

export const FIND_HIERARCHY_LIST_ROLES = gql`
  query FIND_HIERARCHY_LIST_ROLES(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findHierarchyList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        contact {
          id
          email
          category {
            id
            name
          }
          users {
            id
            userName
          }
          person {
            givenName
            familyName
            fullName
          }
          parents {
            id
            institution {
              id
              category {
                name
              }
              institution {
                legalName
                commonName
                unitType {
                  name
                }
              }
            }
          }
        }
        institution {
          id
          institution {
            legalName
            commonName
            unitType {
              name
            }
          }
        }
        roles {
          id
          name
        }
      }
    }
  }
`;
