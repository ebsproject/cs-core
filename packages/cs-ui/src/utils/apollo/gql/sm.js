import { gql } from "@apollo/client";

export const FIND_SERVICE_TYPE_LIST = gql`
  query findServiceTypeList($tenantId: String!) {
    findServiceTypeList(filters: [{ col: "tenant", mod: EQ, val: $tenantId }]) {
      content {
        id
        name
        tenant
      }
    }
  }
`;

export const FIND_FORMS_LIST = gql`
  query findEbsFormList($tenantId: String!) {
    findEbsFormList(filters: [{ col: "tenant", mod: EQ, val: $tenantId }]) {
      content {
        id
        name
        tenant
      }
    }
  }
`;
export const FIND_SERVICE_PROVIDER = gql`
  query FIND_SERVICE_PROVIDER($id: ID!) {
    findServiceProvider(id: $id) {
      id
      name
      tenant
      servicetypes {
        id
        name
        tenant
      }
      ebsForms {
        id
        name
        tenant
      }
    }
  }
`;
export const FIND_SERVICE_LIST = gql`
  query findServiceList($code: String!) {
    findServiceProviderList(filters: [{ col: "code", mod: EQ, val: $code }]) {
      content {
        id
        name
        tenant
        code
        servicetypes {
          id
          name
          tenant
        }
        ebsForms {
          id
          name
          tenant
        }
      }
    }
  }
`;

export const FIND_SERVICE_PROVIDER_LIST = gql`
  query findServiceList($contactid: String!) {
    findServiceProviderList(
      filters: [{ col: "id", mod: EQ, val: $contactid }]
    ) {
      content {
        id
        name
        tenant
        code
        servicetypes {
          id
          name
          tenant
        }
        ebsForms {
          id
          name
          tenant
        }
      }
    }
  }
`;
export const CREATE_SERVICE_PROVIDER = gql`
  mutation CREATE_SERVICE_PROVIDER($ServiceProviderTo: ServiceProviderInput!) {
    createServiceProvider(ServiceProviderTo: $ServiceProviderTo) {
      id
    }
  }
`;

export const MODIFY_SERVICE_PROVIDER = gql`
  mutation MODIFY_SERVICE_PROVIDER($ServiceProviderTo: ServiceProviderInput!) {
    modifyServiceProvider(ServiceProviderTo: $ServiceProviderTo) {
      code
    }
  }
`;
