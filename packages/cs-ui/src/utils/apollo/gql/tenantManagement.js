import { gql } from "@apollo/client";
// QUERIES

export const FIND_ORGANIZATION_LIST = gql`
  query findOrganizationList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findOrganizationList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
      }
    }
  }
`;

// TODO: Delete findDomain, findProductList
// * FIND DOMAIN
export const FIND_DOMAIN = gql`
  query findDomain($id: ID!) {
    findDomain(id: $id) {
      id
      name
      icon
      products {
        id
        name
        icon
        menuOrder
        path
        description
        help
        productfunctions {
          id
          action
          description
        }
      }
    }
  }
`;
// * FIND_PRODUCT_LIST
export const FIND_PRODUCT_LIST = gql`
  query findProductList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findProductList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        description
        path
        help
        icon
        menuOrder
      }
    }
  }
`;
// * FIND DOMAINS LIST
export const FIND_DOMAIN_LIST = gql`
  query findDomainList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findDomainList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
      }
    }
  }
`;
export const FIND_PRODUCT_FUNCTION_LIST = gql`
  query FIND_PRODUCT_FUNCTION_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findProductFunctionList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalElements
      totalPages
      content {
        id
        action
        product {
          id
          name
          domain {
            id
            name
          }
        }
      }
    }
  }
`;
// CREATE
// * CREATE NEW PRODUCT
export const CREATE_PRODUCT = gql`
  mutation createProduct($ProductInput: ProductInput!) {
    createProduct(product: $ProductInput) {
      id
    }
  }
`;
// * CREATE DOMAIN INSTANCE
export const CREATE_DOMAIN_INSTANCE = gql`
  mutation createDomainInstance($DomainInstance: DomainInstanceInput!) {
    createDomainInstance(domainInstance: $DomainInstance) {
      id
    }
  }
`;
// * CREATE CUSTOMER
export const CREATE_CUSTOMER = gql`
  mutation CREATE_CUSTOMER($customer: CustomerInput!) {
    createCustomer(customer: $customer) {
      id
    }
  }
`;
/**
 * @query Create Organization
 */
export const CREATE_ORGANIZATION = gql`
  mutation CREATE_ORGANIZATION($organization: OrganizationInput!) {
    createOrganization(organization: $organization) {
      id
    }
  }
`;
// MODIFY
// * MODIFY PRODUCT
export const MODIFY_PRODUCT = gql`
  mutation modifyProduct($ProductInput: ProductInput!) {
    modifyProduct(product: $ProductInput) {
      id
    }
  }
`;
// * MODIFY CUSTOMER
export const MODIFY_CUSTOMER = gql`
  mutation MODIFY_CUSTOMER($customer: CustomerInput!) {
    modifyCustomer(customer: $customer) {
      id
    }
  }
`;
// * MODIFY DOMAIN INSTANCE
export const MODIFY_DOMAIN_INSTANCE = gql`
  mutation modifyDomainInstance($DomainInstance: DomainInstanceInput!) {
    modifyDomainInstance(domainInstance: $DomainInstance) {
      id
    }
  }
`;
/**
 * @query Modify Organization
 */
export const MODIFY_ORGANIZATION = gql`
  mutation MODIFY_ORGANIZATION($organization: OrganizationInput!) {
    modifyOrganization(organization: $organization) {
      id
    }
  }
`;
/**
 * @query Modify Organization
 */
 export const MODIFY_INSTANCE = gql`
 mutation MODIFY_INSTANCE($Instance: InstanceInput!) {
   modifyInstance(instance: $Instance) {
     id
   }
 }
`;
/**
 * @query Modify Organization
 */
 export const MODIFY_TENANT = gql`
 mutation MODIFY_TENANT($Tenant: TenantInput!) {
   modifyTenant(tenant: $Tenant) {
     id
   }
 }
`;
// DELETE
// * DELETE PRODUCT
export const DELETE_PRODUCT = gql`
  mutation deleteProduct($productId: Int!) {
    deleteProduct(productId: $productId)
  }
`;
// * DELETE DOMAIN INSTANCE
export const DELETE_DOMAIN_INSTANCE = gql`
  mutation deleteDomainInstance($id: Int!) {
    deleteDomainInstance(domainInstanceId: $id)
  }
`;
// * DELETE CUSTOMER
export const DELETE_CUSTOMER = gql`
  mutation deleteCustomer($customerId: Int!) {
    deleteCustomer(customerId: $customerId)
  }
`;
/**
 * @query Delete Organization
 */
export const DELETE_ORGANIZATION = gql`
  mutation deleteOrganization($organizationId: Int!) {
    deleteOrganization(organizationId: $organizationId)
  }
`;
