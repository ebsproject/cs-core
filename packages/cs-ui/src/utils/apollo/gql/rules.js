import { gql } from "@apollo/client";

export const FIND_RULES_LIST = gql`
  query FIND_RULES_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findSecurityRuleList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalElements
      totalPages
      number
      size
      content {
        id
        name
      }
    }
  }
`;

/**
 * @graphql CREATE_ROLE_RULE
 * @variable {RoleRuleInput} userRole
 */
export const CREATE_ROLE_RULE = gql`
  mutation CREATE_ROLE_RULE($securityRuleRole: SecurityRuleRoleInput!) {
    createSecurityRuleRole(securityRuleRole: $securityRuleRole) {
id
    }
  }
`;

/**
 * @graphql CREATE_ROLE
 * @variable {RuleInput} rule
 */
export const CREATE_RULE = gql`
  mutation CREATE_RULE($securityRule: SecurityRuleInput!) {
    createSecurityRule(securityRule: $securityRule) {
id
    }
  }
`;

/**
 * @graphql MODIFY_RULE
 * @variable {RuleInput} rule
 */
export const MODIFY_RULE = gql`
  mutation MODIFY_RULE($securityRule: SecurityRuleInput!) {
    modifySecurityRule(securityRule: $securityRule) {
id
    }
  }
`;

/**
 * @graphql DELETE_RULE
 * @variable {Int} securityRuleId
 */
export const DELETE_SECURITY_RULE = gql`
  mutation DELETE_SECURITY_RULE($securityRuleId: Int!) {
    deleteSecurityRule(securityRuleId: $securityRuleId)
  }
`;

/**
 * @graphql DELETE_RULE
 * @variable {Int} securityRuleId
 */
export const DELETE_ROLE_RULE = gql`
  mutation DELETE_ROLE_RULE($roleId: Int!) {
    deleteSecurityRuleRoleByRoleId(roleId: $roleId)
  }
`;