import { gql } from "@apollo/client";

export const FIND_HIERARCHIES = gql`
  query getHierarchies(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findCoreHierarchyList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        description
      }
    }
  }
`;

export const FIND_WORKFLOW_CONTACT_LIST = gql`
  query FIND_WORKFLOW_CONTACT_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findContactList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      content {
        id
        workflows {
          id
          workflow {
            id
            name
            title
            description
          }
        }
      }
    }
  }
`;

export const FIND_WORKFLOW_LIST = gql`
  query findWorkflowList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findWorkflowList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        title
        description
        sortNo
        showMenu
        navigationName
        securityDefinition
        product {
          name
          id
          path
          description
          menuOrder
          hasWorkflow
          help
          icon
        }
      }
    }
  }
`;

export const CREATE_CONTACT_WORKFLOW = gql`
  mutation CREATE_CONTACT_WORKFLOW($input: ContactWorkflowInput!) {
    createContactWorkflow(input: $input) {
      id
    }
  }
`;

export const DELETE_CONTACT_WORKFLOW = gql`
  mutation DELETE_CONTACT_WORKFLOW($id: Int!) {
    deleteContactWorkflowById(id: $id)
  }
`;

export const FIND_WORKFLOW_CONTACT_BY_ID = gql`
  query FIND_WORKFLOW_CONTACT_BY_ID($id: ID!) {
    findContact(id: $id) {
      id
      institution {
        id
        commonName
        legalName
        isCgiar
      }
      workflows {
        id
        workflow {
          id
          name
          title
          description
        }
      }
    }
  }
`;
