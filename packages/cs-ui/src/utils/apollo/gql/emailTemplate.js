import { gql } from "@apollo/client";
// Find tenant list to create or modify contact and users tenant relationship
export const FIND_EMAIL_TEMPLATE_LIST = gql`
  query{findEmailTemplateList{
    content{
      id
      name
      subject
      template
      tenants{
        id
      }
    }
  }
}
`;
export const CREATE_EMAIL_TEMPLATE = gql`
mutation CREATE_EMAIL_TEMPLATE($emailTemplate: EmailTemplateInput!) {
    createEmailTemplate(emailTemplate: $emailTemplate) {
      id
    }
  }
`;
export const MODIFY_EMAIL_TEMPLATE = gql`
mutation MODIFY_EMAIL_TEMPLATE($emailTemplate: EmailTemplateInput!) {
    modifyEmailTemplate(emailTemplate: $emailTemplate) {
      id
    }
  }
`;
export const DELETE_EMAIL_TEMPLATE = gql`
mutation DELETE_EMAIL_TEMPLATE($id: Int!) {
    deleteEmailTemplate(id: $id)
  }
`;
export const FIND_JOB_WORKFLOW_LIST = gql`
query findJobWorkflowList(
    $filters:[FilterInput]    
    $page:PageInput
    $disjunctionFilters: Boolean = false
  ) {
  findJobWorkflowList(
    page:$page
    filters:$filters
    disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content{
          id
          name
          description
          emailTemplate
          {id
           name
          }
          tenant{
            id
            name
          }
          jobType {
            id
            name
          }
          productFunction{
            id
            description                      
          }
        }
      }
}
`;
export const MODIFY_JOBWF = gql`
mutation MODIFY_JOBWF($jobWorkflow: JobWorkflowInput!) {
    modifyJobWorkflow(jobWorkflow: $jobWorkflow) {
      id
    }
  }
`;