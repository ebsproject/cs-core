import { gql } from "@apollo/client";

export const FIND_COUNTRY_LIST = gql`
  query FIND_COUNTRY_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findCountryList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        name
        iso
      }
    }
  }
`;

export const FIND_FACILITY_LIST = gql`
  query FIND_FACILITY_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findFacilityList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        facilityCode
        facilityName
        facilityClass
        facilityType
        geospatialCoordinates
        description
        parentFacility {
          id
          facilityCode
          facilityName
          facilityClass
          facilityType
          geospatialCoordinates
          description
        }
        geospatialObject {
          id
          geospatialObjectName
          geospatialObjectName
          geospatialObjectType
        }
        dateCreated
        notes
      }
    }
  }
`;

export const CREATE_GEOSPATIAL_OBJECT = gql`
  mutation createGeospatialObject($input: GeospatialObjectInput!) {
    createGeospatialObject(input: $input) {
      id
    }
  }
`;

export const MODIFY_GEOSPATIAL_OBJECT = gql`
  mutation modifyGeospatialObject($input: GeospatialObjectInput!) {
    modifyGeospatialObject(input: $input) {
      id
    }
  }
`;

export const DELETE_GEOSPATIAL_OBJECT = gql`
  mutation deleteGeospatialObject($int: Int!) {
    deleteGeospatialObject(int: $int)
  }
`;

export const CREATE_FACILITY = gql`
  mutation createFacility($input: FacilityInput!) {
    createFacility(input: $input) {
      id
    }
  }
`;

export const MODIFY_FACILITY = gql`
  mutation modifyFacility($input: FacilityInput!) {
    modifyFacility(input: $input) {
      id
    }
  }
`;

export const DELETE_FACILITY = gql`
  mutation deleteFacility($id: Int!) {
    deleteFacility(id: $id)
  }
`;

export const FIND_PLACES_LIST = gql`
  query FIND_PLACES_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findGeospatialObjectList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        geospatialObjectName
        geospatialObjectCode
        geospatialObjectType
        geospatialObjectSubtype
        altitude
        description
        coordinates
        dateCreated
        parentGeospatialObject {
          id
          geospatialObjectName
          geospatialObjectCode
          geospatialObjectType
          geospatialObjectSubtype
          altitude
          description
          coordinates
          dateCreated
          parentGeospatialObject {
            id
            geospatialObjectName
            geospatialObjectCode
            geospatialObjectType
            geospatialObjectSubtype
            altitude
            description
            coordinates
            dateCreated
          }
        }
      }
    }
  }
`;
