import { gql } from "@apollo/client";
// TODO: QUERY
/**
 * @graphql FIND_USER
 * @variable {ID} id
 */
export const FIND_USER = gql`
  query FIND_USER($id: ID!) {
    findUser(id: $id) {
      id
      isActive
      isAdmin
      userName
      tenants {
        id
        name
      }
      contact {
        id
        parents {
          id
          principalContact
          institution {
            id
            addresses {
              id
              country {
                id
                name
              }
              purposes {
                id
                name
                category {
                  name
                }
              }
              location
              region
              zipCode
              streetAddress
              default
            }
            category {
              id
              name
            }
            institution {
              id
              commonName
              isCgiar
              legalName
            }
          }
        }
        contactTypes {
          id
          name
        }
        addresses {
          id
          location
          region
          zipCode
          streetAddress
          default
          country {
            id
            name
          }
          purposes {
            id
            name
          }
        }
        contactInfos {
          id
          value
          default
          contactInfoType {
            id
            name
          }
        }
        person {
          familyName
          givenName
          gender
          additionalName
          gender
          salutation
        }
        purposes {
          id
          name
        }
      }
    }
  }
`;
/**
 * @graphql FIND_USER_NAME
 * @variable {ID} id
 */
export const FIND_USER_NAME = gql`
  query FIND_USER($id: ID!) {
    findUser(id: $id) {
      id
      isActive
      userName
      contact {
        id
        person {
          familyName
          givenName
          additionalName
        }
      }
    }
  }
`;
/**
 * @graph FIND_ROLE_LIST
 * @variable page: Page number and registers
 * @variable sort: [Column and sort mode]
 * @variable filters: [Filter mode, column and filter value]
 * @variable disjunctionFilter: There are more than one filter applied?
 */
export const FIND_ROLE_LIST = gql`
  query FIND_ROLE_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findRoleList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalElements
      totalPages
      number
      size
      content {
        id
        name
        isSystem
        rules{
          id
          name
        }
      }
    }
  }
`;
/**
 * @graphql FIND_USER_LIST
 * @variable page: Page number and registers
 * @variable sort: [Column and sort mode]
 * @variable filters: [Filter mode, column and filter value]
 * @variable disjunctionFilter: There are more than one filter applied?
 */
// export const FIND_USER_LIST = gql`
//   query FIND_USER_LIST(
//     $page: PageInput
//     $sort: [SortInput]
//     $filters: [FilterInput]
//     $disjunctionFilters: Boolean = false
//   ) {
//     findUserList(
//       page: $page
//       sort: $sort
//       filters: $filters
//       disjunctionFilters: $disjunctionFilters
//     ) {
//       content {
//         id
//       }
//     }
//   }
// `;


export const FIND_USER_LIST = gql`
  # Write your query or mutation here
  query findUserList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findUserList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        externalId
        userName
        isActive
        tenants {
          id
          name
          instances {
            id
            name
            domaininstances {
              domain {
                name
                info
                icon
              }
              context
              sgContext
            }
          }
        }
        contact {
          id
          contactInfos {
            id
            value
            default
            contactInfoType {
              id
              name
            }
          }
          person {
            familyName
            additionalName
            givenName
            jobTitle
            knowsAbout
          }
        }        
      }
    }
  }
`;
// TODO: CREATE
/**
 * @graphql CREATE_USER
 * @variable {UserInput} user
 */
export const CREATE_USER = gql`
  mutation CREATE_USER($user: UserInput!) {
    createUser(user: $user) {
      id
    }
  }
`;
// TODO: MODIFY
/**
 * @graphql MODIFY_USER
 * @variable {UserInput} user
 */
export const MODIFY_USER = gql`
  mutation MODIFY_USER($user: UserInput!) {
    modifyUser(user: $user) {
      id
    }
  }
`;
/**
 * @graphql CREATE_USER_ROLE
 * @variable {UserRoleInput} userRole
 */
export const CREATE_USER_ROLE = gql`
  mutation CREATE_USER_ROLE($userRole: UserRoleInput!) {
    createUserRole(userRole: $userRole) {
      role {
        id
      }
    }
  }
`;
/**
 * @graphql CREATE_ROLE
 * @variable {RoleInput} role
 */
export const CREATE_ROLE = gql`
  mutation CREATE_ROLE($role: RoleInput!) {
    createRole(role: $role) {
      id
    }
  }
`;
/**
 * @graphql CREATE_ROLE_PRODUCT
 * @variable {RoleProductInput} roleProduct
 */
export const CREATE_ROLE_PRODUCT = gql`
  mutation CREATE_ROLE_PRODUCT($roleProduct: RoleProductInput!) {
    createRoleProduct(roleProduct: $roleProduct) {
      role {
        id
      }
    }
  }
`;
// TODO: MODIFY
/**
 * @graphql MODIFY_ROLE
 * @variable {RoleInput} role
 */
export const MODIFY_ROLE = gql`
  mutation MODIFY_ROLE($role: RoleInput!) {
    modifyRole(role: $role) {
      id
    }
  }
`;
// TODO: DELETE
/**
 * @graphql DELETE_USER_ROLE
 * @variable {UserRoleInput} userRole
 */
export const DELETE_USER_ROLE = gql`
  mutation DELETE_USER_ROLE($userRole: UserRoleInput!) {
    deleteUserRole(userRole: $userRole)
  }
`;
/**
 * @graphql DELETE_ROLE
 * @variable {Int} id
 */
export const DELETE_ROLE = gql`
  mutation DELETE_ROLE($id: Int!) {
    deleteRole(id: $id)
  }
`;
/**
 * @graphql DELETE_ROLE_PRODUCT
 */
export const DELETE_ROLE_PRODUCT = gql`
  mutation DELETE_ROLE_PRODUCT($roleId: Int!, $productFunctionID: [Int!]) {
    deleteRoleProduct(roleId: $roleId, productFunctionID: $productFunctionID)
  }
`;
/**
 * @graphql DELETE_USER
 * @variable {Int!} id
 */
export const DELETE_USER = gql`
  mutation DELETE_USER($id: Int!) {
    deleteUser(userId: $id)
  }
`;
