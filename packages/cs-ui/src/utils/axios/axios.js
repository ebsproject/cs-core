import axios from "axios";
import { getTokenId, getCoreSystemContext } from "@ebs/layout";
const { restUri, printoutUri, graphqlUri } = getCoreSystemContext();
export const client = axios.create({
  baseURL: restUri,
  withCredentials: false,
});

client.interceptors.request.use(function (config) {
  config.headers["Content-Type"] = "text/plain";
  config.headers["Accept"] = "*/*";
  // config.headers[ "Access-Control-Allow-Origin"] = "*";
  return config;
});

export const printoutClient = axios.create({
  baseURL: printoutUri,
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

printoutClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});

export const clientRest = axios.create({
  baseURL: graphqlUri.replace("/graphql", ""),
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

clientRest.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});