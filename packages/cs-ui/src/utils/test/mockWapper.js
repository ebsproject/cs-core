import React from "react";
import { render as rtlRender } from "@testing-library/react";
import { configureStore, combineReducers } from "@reduxjs/toolkit";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { IntlProvider } from "react-intl";
import { Provider } from "react-redux";
import { MockedProvider } from "@apollo/client/testing";
import crm from "utils/test/modules/crm";
import persist from "utils/test/modules/crm";
import { mocks } from "utils/test/mockCalls";

export default function render(
  ui,
  {
    preloadedState,
    store = configureStore({
      reducer: combineReducers({ crm, persist }),
      preloadedState,
    }),
    ...renderOptions
  } = {}
) {
  function Wrapper({ children }) {
    return (
      <Provider store={store}>
        <IntlProvider messages={{}} locale="en" defaultLocale="en">
          <MockedProvider mocks={mocks} addTypename={false}>
            {children}
          </MockedProvider>
        </IntlProvider>
      </Provider>
    );
  }
  return rtlRender(ui, { wrapper: Wrapper, ...renderOptions });
}
