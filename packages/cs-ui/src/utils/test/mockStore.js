import { configureStore } from "@reduxjs/toolkit";
import crmReducer from "store/modules/CRM";
import messageReducer from "store/modules/message";
import tenantReducer from "store/modules/TenantManagement";
import userReducer from "store/modules/UserManagement";
import dataPersistReducer from "store/modules/DataPersist";

export const store = configureStore({
  reducer: {
    crm: crmReducer,
    message: messageReducer,
    tenant: tenantReducer,
    um: userReducer,
    persist: dataPersistReducer,
  },
  preloadedState: {
    crm: {
      contactTypeList: [{ id: 1, name: "mock", category: "Person" }],
      countryList: [],
      contactInfoSuccess:true
    },
    um: {
      success: false,
      userInfo: { contact: { person: {} }, tenants: [{}] },
    },
    persist: {
      defaultValues: { id: 1 },
      id: 1,
      countryList: [],
      type: "",
      contactTypeList: [],
      purposeList: [],
    },
    tenant:{
      success:true
    }
  },
});
