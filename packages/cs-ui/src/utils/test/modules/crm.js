import { createSlice } from "@reduxjs/toolkit";

const crmSlice = createSlice({
  name: "crm",
  initialState: {
    success: false,
    contactInfoSuccess:true
  },
  reducers: {},
  extraReducers: (builder) => {},
});

export default crmSlice.reducer;
