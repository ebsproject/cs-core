import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";

export const fetchUser = createAsyncThunk("persist/findPurposeList", async () =>
  jest.fn()
);

const persistSlice = createSlice({
  name: "persist",
  initialState: {
    id:1,
    contactTypeList: [],
    defaultValues: null,
    purposeList: [
      { id: 1, name: "mail", category: { name: "Institution" } },
      { id: 2, name: "shipment", category: { name: "Institution" } },
    ],
    institutionId: null,
    countryList: [{ id: 1, name: "Mexico" }],
  },
  reducers: {},
});

export default persistSlice.reducer;
