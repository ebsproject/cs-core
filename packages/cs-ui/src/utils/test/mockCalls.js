import { FIND_PRODUCT_LIST } from "utils/apollo/gql/tenant";
import { FIND_ROLE_LIST } from "utils/apollo/gql/userManagement";
import { FIND_PURPOSE_LIST } from "utils/apollo/gql/crm";
export const mocks = [
  {
    request: {
      query: FIND_PRODUCT_LIST,
      variables: {
        id: 1,
      },
    },
    result: {
      data: {},
    },
  },
  {
    request: {
      query: FIND_ROLE_LIST,
      variables: {
        page: { number: 1, size: 10 },
      },
    },
    result: {
      data: {
        findRoleList: {
          content: [{ id: 1, name: "default" }],
        },
      },
    },
  },
  {
    request: {
      query: FIND_PURPOSE_LIST,
      variables: {
        page: { number: 1, size: 10 },
        filters: [
          { col: "category.name", mod: "LK", val: "Person" },
          { col: "category.name", mod: "LK", val: "Address" },
        ],
        disjunctionFilters: true,
      },
    },
    result: {
      data: {
        findPurposeList: {
          content: [
            { id: 1, name: "mail", category: { name: "Institution" } },
            { id: 2, name: "shipment", category: { name: "Institution" } },
          ],
        },
      },
    },
  },
];
