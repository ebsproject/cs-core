import { MockedProvider } from "@apollo/client/testing";
import { Provider } from "react-redux";
import { IntlProvider } from "react-intl";
import { mocks } from "./mockCalls";
import { store } from "./mockStore";
import { useForm } from "react-hook-form";

export const Wrapper = ({ children }) => {
  const institutionId = 1;
  const onSubmit = jest.fn();
  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
    getValues,
  } = useForm({
    defaultValues: {
      institutionId: institutionId,
      phones: [{ id: 0, value: "" }],
      addresses: [{ default: true }],
    },
  });
  return (
    <Provider store={store}>
      <IntlProvider messages={{}} locale="en" defaultLocale="en">
        <MockedProvider mocks={mocks}>
          <form onSubmit={handleSubmit(onSubmit)} name="ContactForm">
            {children}
          </form>
        </MockedProvider>
      </IntlProvider>
    </Provider>
  );
};
