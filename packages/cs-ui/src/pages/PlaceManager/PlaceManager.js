import React, { memo } from "react";
import PropTypes from "prop-types";
import { EbsTabsLayout } from "@ebs/styleguide";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { useLocation } from "react-router-dom";
import { ProductDescriptionTitle } from "@ebs/layout";
import Places from "components/organism/Places/Places";
import Facilities from "components/organism/Facilities/Facilities";

/**
 * @param { }: page props,
 */
function PlaceManager({}) {
  const location = useLocation();

  /**
   * @prop data-testid: Id to use inside crm.test.js file.
   */
  return (
    <div data-testid={"PlaceManagerTestId"}>
      <ProductDescriptionTitle />
      <EbsTabsLayout
        orientation="horizontal"
        index={location?.state?.index || 0} //TODO:: Review this
        tabs={[
          {
            label: <FormattedMessage id="none" defaultMessage="Places" />,
            component: <Places />,
          },
          // {
          //   label: <FormattedMessage id="none" defaultMessage="Facilities" />,
          //   component: <Facilities />,
          // },
        ]}
      />
    </div>
  );
}
// Type and required properties
PlaceManager.propTypes = {};
// Default properties
PlaceManager.defaultProps = {};
export default memo(PlaceManager);
