import React, { memo } from "react";
// GLOBALIZATION COMPONENT
import OrganizationUnitsTable from "components/organism/OrganizationUnits/OrganizationUnitsTable";
/**
 * @param { }: page props,
 */
function OrganizationUnitsView() {

  /**
   * @prop data-testid: Id to use inside crm.test.js file.
   */
  return (
    <div data-testid={"OrganizationUnitsTestId"}>
      <OrganizationUnitsTable/>
    </div>
  );
}
// Type and required properties
OrganizationUnitsView.propTypes = {};
export default memo(OrganizationUnitsView);
