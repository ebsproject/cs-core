import OrganizationUnits from "./OrganizationUnits";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("OrganizationUnits is in the DOM", () => {
  render(<OrganizationUnits></OrganizationUnits>, { wrapper: Wrapper });
  expect(screen.getByTestId("OrganizationUnitsTestId")).toBeInTheDocument();
});
