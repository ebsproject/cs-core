import React, { memo } from "react";
import PropTypes from "prop-types";
import { EbsTabsLayout } from "@ebs/styleguide";
import People from "components/organism/People";
import Institutions from "components/organism/Institutions";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { useLocation} from "react-router-dom";
import { ProductDescriptionTitle } from "@ebs/layout";

/**
 * @param { }: page props,
 */
const  CRMView = () => {
  const location = useLocation();

  /**
   * @prop data-testid: Id to use inside crm.test.js file.
   */
  return (
    <div data-testid={"CRMTestId"}>
      <ProductDescriptionTitle />
      <EbsTabsLayout
        index={location?.state?.index || 0} //TODO:: Review this
        tabs={[
          {
            label: <FormattedMessage id="none" defaultMessage="Person" />,
            component: <People />,
          },
          {
            label: <FormattedMessage id="none" defaultMessage="Institutions" />,
            component: <Institutions />,
          },
        ]}
      />
    </div>
  );
}
export default memo(CRMView);
