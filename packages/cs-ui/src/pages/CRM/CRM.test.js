import CRM from "./CRM";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("CRM is in the DOM", () => {
  render(<CRM></CRM>, { wrapper: Wrapper });
  expect(screen.getByTestId("CRMTestId")).toBeInTheDocument();
});
