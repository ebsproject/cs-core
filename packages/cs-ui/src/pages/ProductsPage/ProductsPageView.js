import React from "react";
import { Core } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
const {
  Typography,
  Card,
  CardContent,
  CardHeader,
  CardMedia,
  Box,
  CardActionArea,
  Grid,
} = Core;
import { FormattedMessage } from "react-intl";
import { Products } from "@ebs/layout";
import productLogo from "assets/images/logos/cimmyt.png";
import EbsLogo from "assets/images/logos/EBS.png";
import { getContext } from "utils/other/CrossMFE/CrossContext";
import { setProductSelected } from "store/ducks/product";
import { useDispatch } from "react-redux";

const ProductsPageView = React.forwardRef(({ children, ...rest }, ref) => {
  const dispatch = useDispatch();
  const products = Products();
  const product = products[0];
  const navigate = useNavigate();
  const handleClickProduct = (productPath, item) => {
    setProductSelected(item)(dispatch);
    navigate(`/cs/${productPath}`);
  };
  return (
    <div className="grid-cols-1 gap-14 sm:gap-16">
      <Card>
        <CardMedia className="flex flex-row justify-center flex-wrap">
          <div>
            <img src={EbsLogo} style={{ height: "80px" }} />
          </div>

        </CardMedia>
        <CardHeader
          title={
            <Typography
              variant="h5"
              color="inherit"
              paragraph={true}
              className="font-ebs text-ebs-green-900 text-center"
            >
              <FormattedMessage
                id="tnt.comp.comsoon.welcome"
                defaultMessage={`Select one of the following products`}
              />
            </Typography>
          }
        />
        <Box className="grid grid-cols-3 justify-center justify-items-center">
          {product
            //.slice()
            .sort(function (a, b) {
              return a.menu_order - b.menu_order;
            })
            .map((p, index) => (
              <CardActionArea
                onClick={() => handleClickProduct(p.path, p)}
                key={index}
                className="sm:w-4/5 sm:h-5/6 transition duration-200 ease-in-out transform hover:-translate-y-1 rounded sm:rounded-fully  md:rounded-lg"
              >
                <Card className="rounded-sm">
                  {/*<CardMedia className="flex flex-row justify-center flex-wrap" title={"EBS product"}>
                    <div>
                      <img src={productLogo} style={{ height: "40px" }} />
            </div>

            </CardMedia>*/}
                  <CardContent>
                    <Typography
                      variant="h4"
                      className="font-ebs text-ebs-green-900 text-center"
                    >
                      {p.name}
                    </Typography>
                    <Typography
                      variant="h6"
                      className="font-ebs text-ebs-black-900 text-center"
                    >
                      {p.description}
                    </Typography>
                  </CardContent>
                </Card>
                <br />
              </CardActionArea>
            ))}
        </Box>
      </Card>
    </div>
  );
});
export default ProductsPageView;
