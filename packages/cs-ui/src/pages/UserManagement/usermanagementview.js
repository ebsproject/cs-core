import React, { memo, useContext, useEffect } from "react";
import Users from "components/organism/Users";
import Roles from "components/organism/Roles";
import { EbsTabsLayout } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { useLocation } from "react-router-dom";
import { ProductDescriptionTitle, RBAC, userContext } from "@ebs/layout";
import SecurityRules from "components/organism/SecurityRules";
const  UserManagementView = () => {
  const { userProfile } = useContext(userContext);
  const location = useLocation();
  let newCombinedArray = [];
  let tabOption = [];

  const filterUserProfileTabs = () => {
    userProfile.permissions.applications.map((itemDomain) => {
      if (itemDomain.domain === "Core System") {
        itemDomain.products.map((itemProduct) => {
          if (itemProduct.name === "User Management") {
            itemProduct.dataActions.map((product, index) => {
              let dataProduct = Object.values(product);
              dataProduct.map((action) => {
                newCombinedArray = [...newCombinedArray, ...action];
              });
            });
          }
        });
      }
    });

    const filterActionsUser = newCombinedArray.some(
      (item) => item === "Modify" || item === "Create" || item === "Delete"
    );
    const filterActionsRoles = newCombinedArray.some(
      (item) =>
        item === "Modify_Role" ||
        item === "Create_Role" ||
        item === "Delete_Role"
    );
    const filterActionsRule = newCombinedArray.some(
      (item) =>
        item === "Modify_Security_Rule" ||
        item === "Create_Security_Rule" ||
        item === "Delete_Security_Rule"
    );

    if (filterActionsUser) {
      tabOption.push({
        label: <FormattedMessage id="none" defaultMessage="Users" />,
        component: <Users />,
      });
    }
    if (filterActionsRoles) {
      tabOption.push({
        label: <FormattedMessage id="none" defaultMessage="Roles" />,
        component: <Roles />,
      });
    }
    if (filterActionsRule) {
      tabOption.push({
        label: <FormattedMessage id="none" defaultMessage="Security Rules" />,
        component: <SecurityRules />,
      });
    }
    return tabOption;
  };

  /**
   * This will be rendered in View
   * @prop data-testid: Id to use inside usermanagement.test.js file.
   */
  return (
    <div>
      <ProductDescriptionTitle />
      <EbsTabsLayout
        data-testid="userManagementTestid"
        index={location?.state?.index || 0} //TODO:: Review this
        tabs={filterUserProfileTabs()}
      />
    </div>
  );
}
export default memo(UserManagementView);
