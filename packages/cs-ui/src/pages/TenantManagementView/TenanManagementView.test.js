import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { prettyDOM } from "@testing-library/dom";
import { Wrapper } from "utils/test/mockWapper";
import TenantManagement from "./TenantManagementView";

afterEach(cleanup);

test("TenanManagementView is in the DOM", () => {
  render(
    <TenantManagement
      rowSelected={[{}]}
      refresh={() => {}}
      handleMenuClose={() => {}}
    ></TenantManagement>,
    { wrapper: Wrapper }
  );
  expect(screen.getByTestId("TenantManagementViewTestId")).toBeInTheDocument();
});
