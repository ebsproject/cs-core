import React from "react";
import { EbsTabsLayout } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { Tenants } from "components/organism/Tenants";
import { Domains } from "components/organism/Domains";
import { Customers } from "components/organism/Customers";
import { Organizations } from "components/organism/Organizations";
import { ProductDescriptionTitle } from "@ebs/layout"

const TenantManagementView = () => {
  return (
    <div>
      <ProductDescriptionTitle />
      <EbsTabsLayout
        data-testid={"TenantManagementViewTestId"}
        tabs={[
          {
            label: <FormattedMessage id="none" defaultMessage="Settings " />,
            component: <Tenants />,
          },
          {
            label: <FormattedMessage id="none" defaultMessage="Modules" />,
            component: <Domains />,
          },
          {
            label: <FormattedMessage id="none" defaultMessage="Centers" />,
            component: <Customers />,
          }
        ]}
      />
    </div>
  );
};

export default TenantManagementView;
