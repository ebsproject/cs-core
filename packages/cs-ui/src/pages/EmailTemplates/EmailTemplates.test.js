import EmailTemplates from "./EmailTemplates";
import React from "react";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import "@testing-library/jest-dom/extend-expect";
import { Wrapper } from "utils/test/mockWapper";

afterEach(cleanup);

test("EmailTemplates is in the DOM", () => {
  render(<EmailTemplates></EmailTemplates>, { wrapper: Wrapper });
  expect(screen.getByTestId("EmailTemplatesTestId")).toBeInTheDocument();
});
