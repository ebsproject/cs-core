import React, { memo } from "react";
// GLOBALIZATION COMPONENT
import Templates from "components/EmailTemplate/templates";
/**
 * @param { }: page props,
 */
function EmailTemplateView() {

  /**
   * @prop data-testid: Id to use inside crm.test.js file.
   */
  return (
    <div data-testid={"EmailTemplatesTestId"}>
      <Templates/>
    </div>
  );
}
// Type and required properties
EmailTemplateView.propTypes = {};
// Default properties
// `EmailTemplateView.defaultProps = {};`
export default memo(EmailTemplateView);
