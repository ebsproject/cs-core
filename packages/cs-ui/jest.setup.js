// import Mocks for SystemJS mock below
import {
  EbsDialog,
  EbsTabsLayout,
  EBSMaterialUIProvider,
  DropZone,
  Icons,
  Core,
  Lab
} from "@ebs/styleguide";
import {
Designer
} from "@ebs/po";
import { EbsGrid, EbsForm } from "@ebs/components";
import {
  getDomainContext,
  getContext,
  getAuthState,
  getTokenId,
  getCoreSystemContext,
} from "@ebs/layout";
// Mock SystemJS
global.System = {
  import: jest.fn(mockImport),
};

function mockImport(importName) {
  // What you do in mock import will depend a lot on how you use SystemJS in the project and components you wish to test

  /* If I had already created a mock for `@react-mf/people` and I wanted to test this component:
   *  https://github.com/react-microfrontends/planets/blob/main/src/planets-page/selected-planet/selected-planet.component.js#L5
   * I would want `System.import('@react-mf/people')` to resovle to my mock one way to accomplish this would be the following
   */
  switch (importName) {
    case "@ebs/styleguide":
      return Promise.resolve({
        EbsDialog,
        EbsTabsLayout,
        EBSMaterialUIProvider,
        DropZone,
        Core,
        Icons,
        Lab
      });
    case "@ebs/components":
      return Promise.resolve({ EbsGrid,EbsForm });
      case "@ebs/po":
        return Promise.resolve({ Designer });
    case "@ebs/layout":
      return Promise.resolve({
        getDomainContext,
        getContext,
        getAuthState,
        getTokenId,
        getCoreSystemContext,
      });
    default:
      return Promise.resolve({});
  }
}
