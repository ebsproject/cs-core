import React, { createContext, useState, useContext } from 'react';
const userContext = createContext();

export const UserContextProvider = ({ children }) => {
  const [userProfile, setUserProfile] = useState({
    "account": "j.ramirez@cimmyt.org",
    "dbId": 114,
    "externalId": 580,
    "fullName": "Ramirez Aldama, Juan Manuel",
    "full_name": "Ramirez Aldama, Juan Manuel",
  })
  return (
    <userContext.Provider value={{ userProfile, setUserProfile }}>
      {children}
    </userContext.Provider>
  );
};
export function useUserContext() {
    return useContext(UserContextProvider)
  }