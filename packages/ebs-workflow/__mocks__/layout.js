
import { useContext } from "react";
import React, { createContext, useState } from 'react';

export const InstanceContext = {};
export const InstanceProvider = {};

export const userContext = createContext();
export const WorkflowContext = createContext();

export const UserContextProvider = ({ children }) => {
  const [userProfile, setUserProfile] = useState({
    "account": "j.ramirez@cimmyt.org",
    "dbId": 114,
    "externalId": 580,
    "fullName": "Ramirez Aldama, Juan Manuel",
    "full_name": "Ramirez Aldama, Juan Manuel",
  })
  return (
    <userContext.Provider value={{ userProfile, setUserProfile }}>
      {children}
    </userContext.Provider>
  );
};
export function useUserContext() {
  return useContext(UserContextProvider)
}
export const getDomainContext = (instance) => {
  return { sgContext: "" };
};

export const getTokenId = () => {
  return "123";
};

export const getAuthState = () => {
  return {};
};

export const getCoreSystemContext = () => {
  return {
    markerDbUrl: "https://test-org.org",
    graphqlUri: "https://csapi-dev.ebsproject.org/graphql",
    printoutUri: "https://csps-dev.ebsproject.org",
    fileAPIUrl: "https://fileapi-dev.ebsproject.org",
    cbGraphqlUri: "https://cbgraphql-dev.ebsproject.org/graphql"
  };
};
export const RBAC = ({ allowedAction, children }) => {
  return allowedAction && children
}
