import { forwardRef } from "react";
export const EbsGrid = ({ toolbaractions, children, ...rest }) => {
  const {title} = rest
  return(
  <>
    {toolbaractions && toolbaractions()}
    <table>
      <tbody>
        <tr>
          <td> {title}</td>
        </tr>
      </tbody>
    </table>
  </>
)};

export const EbsForm = forwardRef((props, ref) => {
  const { children } = props;
  return (
  <div>{children}</div>)
});