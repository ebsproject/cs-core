import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import PrintRounded from "@mui/icons-material/PrintRounded";
import Visibility from "@mui/icons-material/Visibility";
import ArrowDropDown from "@mui/icons-material/ArrowDropDown";
import Delete from  "@mui/icons-material/Delete";
import Edit  from "@mui/icons-material/Edit";
import Cancel from "@mui/icons-material/Cancel";
import CheckCircle from "@mui/icons-material/CheckCircle";
import CloudDownload  from "@mui/icons-material/CloudDownload";
import Lock  from "@mui/icons-material/Lock";
import GetApp from "@mui/icons-material/GetApp";
import Message from "@mui/icons-material/Message";
import SettingsApplications from "@mui/icons-material/SettingsApplications";
import { forwardRef, createElement } from "react";
import { PostAdd, KeyboardArrowLeft } from "@mui/icons-material";
import {
  Divider,
  Alert,
  Autocomplete,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Button,
  ButtonGroup,
  CircularProgress,
  ClickAwayListener,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  Grow,
  IconButton,
  InputLabel,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Select,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  ListItemAvatar,
  Avatar,
  ListItemIcon,
  LinearProgress,
  Icon,
  Skeleton,
  RadioGroup,
  FormControlLabel,
  Radio,
  Checkbox,
  SwipeableDrawer,
  Stepper, Step, StepLabel, Chip
} from "@mui/material";
import { Warning } from "@mui/icons-material";

export const Core = {
  Alert,
  Autocomplete,
  DialogTitle,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Button,
  ButtonGroup,
  CircularProgress,
  ClickAwayListener,
  FormControl,
  FormHelperText,
  FormLabel,
  Grid,
  Grow,
  IconButton,
  InputLabel,
  MenuItem,
  MenuList,
  Paper,
  Popper,
  Select,
  Snackbar,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  TextField,
  Tooltip,
  Typography,
  EbsDialog : Dialog,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  ListItemSecondaryAction,
  LinearProgress,
  Icon,
  RadioGroup,
  Radio,
  FormControlLabel,
  Divider,
  Checkbox,  
  ListItemAvatar,
  Avatar,
  SwipeableDrawer,
  Stepper, Step, StepLabel, Chip
};

export const Lab = { Autocomplete, Skeleton };

export const Styles = {useTheme:()=>{ return{spacing:()=>{}}}};
export const Icons = {
  ExpandLess,
  ExpandMore,
  PrintRounded,
  Visibility,
  ArrowDropDown,
  Delete,
  Cancel,
  CheckCircle,
  Warning, Message, PostAdd, KeyboardArrowLeft,
  CloudDownload, Lock, GetApp, SettingsApplications, Edit
};
export const Colors = {};
export const EBSMaterialUIProvider = ({ children }) => <>{children}</>;
const test = () => {
  return (
    <div>
      <Box></Box>
    </div>
  );
};
export const DropZone = {
  DropzoneDialog: forwardRef(({ children, ...rest }, ref) => <>{children}</>),
};
export const EbsDialog = forwardRef(({open, handleClose, children, title, ...rest }, ref) => {
  const {actions} = rest;
  return (
  open ? <>
  {actions && actions}
   <button onClick={handleClose}>Close</button>
    <label>{title}</label>
    {children}
  </> : null
)});

export const EbsTabsLayout = forwardRef(({ tabs, ...rest }, ref) => (
  <div {...rest} ref={ref}>
    {tabs.map((tab, key) => (
      <div key={key}>
        <label>{tab.label}</label>
        {tab.component}
      </div>
    ))}
  </div>
));

