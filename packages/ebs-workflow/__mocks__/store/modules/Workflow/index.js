/*
 Initial state and properties
 */
 export const initialState = {
    openSideBar:false,
    workflowValues:{id:1, name:"Test Workflow", phases:[{id:0,name:"Phase Name", stages:[{id:0, sequence:1, name:"Stage Name",workflowViewType:{name:"Some Type"}, nodes:[{id:0, name:"Node Name", nodeType:{name:"Some Name"}}]}]}]},
    entityColumns:null,
    workflowPhase:{
      id:0, name:"Phase Name",
      stages:[{id:1, name:"Stage One",nodes:[],workflowViewType:{name:"Some Type"}}]
    },
    phases:null,
    customFields:null,
    workflowVariables: {
      entity:"service", url:"https://csapi-dev.ebsprject.org"
    },
    workflowId:0,
    dependOnNodes:null,
    recordId: null,
    ppNodes: null,
    statusNodes:[]
 }
 /*
  Action types
  */
 export const OPEN_ACTION = '[workflow] OPEN_ACTION';
 export const SET_WORKFLOW_VALUES = '[workflow] SET_WORKFLOW_VALUES'; 
 export const SET_WORKFLOW_VARIABLES = '[workflow] SET_WORKFLOW_VARIABLES'; 
 export const SET_ENTITY_COLUMNS = '[workflow] SET_ENTITY_COLUMNS'; 
 export const SET_WORKFLOW_PHASE = '[workflow] SET_WORKFLOW_PHASE';
 export const SET_PHASE = '[workflow] SET_PHASE';
 export const SET_CUSTOM_FIELDS = '[workflow] SET_CUSTOM_FIELDS';
 export const SET_WORKFLOW_ID = '[workflow] SET_WORKFLOW_ID';
 export const SET_DEPEND_NODES = '[workflow] SET_DEPEND_NODES';
 export const SET_RECORD_ID = '[workflow] SET_RECORD_ID';
 export const SET_PRINT_PREVIEW_NODES = '[workflow] SET_PRINT_PREVIEW_NODES';
 export const SET_STATUS_NODES = '[workflow] SET_STATUS_NODES';

 /*
  Arrow function for change state
  */

  export const setRecordId = (payload) =>(
    {
       type: SET_RECORD_ID,
       payload,
     });
 export const setOpenSideBar = (payload) =>(
{
   type: OPEN_ACTION,
   payload,
 });
 export const setWorkflowValues = (payload) =>(
  {
     type: SET_WORKFLOW_VALUES,
     payload,
   });
   export const setDependOnNodes = (payload) =>(
    {
       type: SET_DEPEND_NODES,
       payload,
     });
   export const setWorkflowVariables = (payload) =>(
    {
       type: SET_WORKFLOW_VARIABLES,
       payload,
     });
   export const setCustomFields = (payload) =>(
    {
       type: SET_CUSTOM_FIELDS,
       payload,
     });
   export const setEntityColumns = (payload) =>(
    {
       type: SET_ENTITY_COLUMNS,
       payload,
     });
   export const setWorkflowPhase = (payload) =>(
    {
       type: SET_WORKFLOW_PHASE,
       payload,
     });
     export const setPhases = (payload) =>(
      {
         type: SET_WORKFLOW_PHASE,
         payload,
       });
    export const setWorkflowId = (payload) =>(
      {
        type: SET_WORKFLOW_ID,
        payload,
     });
     export const setPrintPreviewNodes = (payload) =>(
      {
        type:SET_PRINT_PREVIEW_NODES,
        payload
      }
     )
     export const setStatusNodes = (payload) =>(
      {
        type:SET_STATUS_NODES,
        payload
      }
     )
  
 /*
  Reducer to describe how the state changed
  */
 export default function Reducer(state = initialState, { type, payload }) {
   switch (type) {
    case SET_RECORD_ID:
      return {
        ...state,
        recordId: payload,
      }
     case OPEN_ACTION:
       return {
         ...state,
         openSideBar: payload,
       }
       case SET_WORKFLOW_VALUES:
        return {
          ...state,
          workflowValues: payload,
        }
        case SET_WORKFLOW_VARIABLES:
          return {
            ...state,
            workflowVariables: payload,
          }
          case SET_DEPEND_NODES:
            return {
              ...state,
              dependOnNodes: payload,
            }
        case SET_CUSTOM_FIELDS:
          return {
            ...state,
            customFields: payload,
          }
        case SET_ENTITY_COLUMNS:
          return {
            ...state,
            entityColumns: payload,
          }
        case SET_WORKFLOW_PHASE:
          return {
            ...state,
            workflowPhase: payload,
          }
          case SET_PHASE:
            return {
              ...state,
              phases: payload,
            }
            case SET_WORKFLOW_ID:
            return {
              ...state,
              workflowId: payload,
            }
            case SET_PRINT_PREVIEW_NODES:
              return {
                ...state,
                ppNodes: payload,
              }
              case SET_STATUS_NODES:
                return {
                  ...state,
                  statusNodes: payload,
                }
     default:
       return state
   }
 }
 