/*
 Initial state and properties
 */
 export const initialState = {
    open:false,
    template:"",
    params: null
 }
 /*
  Action types
  */
 export const SHOW_DESIGNER = 'SHOW_DESIGNER';
 export const HIDE_DESIGNER = 'HIDE_DESIGNER';
 export const SET_TEMPLATE= "SET_TEMPLATE";
 export const SET_PARAMS= "SET_PARAMS";
 /*
  Arrow function for change state
  */
 export const showDesigner = (payload) => ({
   type: SHOW_DESIGNER,
   payload,
 })
 export const hideDesigner = (payload) => ({
    type: HIDE_DESIGNER,
    payload,
  })
  export const setTemplate = (payload) => ({
    type: SET_TEMPLATE,
    payload,
  })
  export const setParams = (payload) => ({
    type: SET_PARAMS,
    payload,
  })
 /*
  Reducer to describe how the state changed
  */
 export default function Reducer(state = initialState, { type, payload }) {
   switch (type) {
     case HIDE_DESIGNER:
       return {
         ...state,
         open: payload,
       }
       case SHOW_DESIGNER:
        return {
          ...state,
          open: payload,
        }
        case SET_TEMPLATE:
          return {
            ...state,
            template: payload,
          }
          case SET_PARAMS:
            return {
              ...state,
              params: payload,
            }
     default:
       return state
   }
 }
 