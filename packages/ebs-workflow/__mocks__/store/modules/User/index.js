/*
 Initial state and properties
 */
 export const initialState = {
userPermissions: {},
 }
 /*
  Action types
  */
 export const SET_USER_PERMISSIONS = 'SET_USER_PERMISSIONS';
 /*
  Arrow function for change state
  */
 export const setUserPermissions = (payload) => ({
   type: SET_USER_PERMISSIONS,
   payload,
 });
 /*
  Reducer to describe how the state changed
  */
 export default function Reducer(state = initialState, { type, payload }) {
   switch (type) {
     case SET_USER_PERMISSIONS:
       return {
         ...state,
         userPermissions: payload,
       }
     default:
       return state
   }
 }
 