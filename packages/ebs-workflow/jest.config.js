module.exports = {
  testEnvironment: "jsdom",
  collectCoverageFrom: ["src/**/*.js"],
  modulePathIgnorePatterns: ["/cache", "/dist"],
  transform: {
    "^.+\\.(j|t)sx?$": "babel-jest",
    ".+\\.(css|scss|png|jpg|svg)$": "jest-transform-stub",
  },
  coverageDirectory: "coverage/",
  coverageReporters: ["json","html"],
  moduleNameMapper: {
    "\\.(css)$": "identity-obj-proxy",
    "single-spa-react/parcel": "single-spa-react/lib/cjs/parcel.cjs",
    "@ebs/cs": "<rootDir>/__mocks__/core.js",
    "@ebs/layout": "<rootDir>/__mocks__/layout.js",
    "@ebs/po": "<rootDir>/__mocks__/po.js",
    "@ebs/styleguide": "<rootDir>/__mocks__/styleguide.js",
    "@ebs/messaging": "<rootDir>/__mocks__/messaging.js",
    "@ebs/components": "<rootDir>/__mocks__/components.js",
    "\\.(jpg|jpeg|png|gif|svg)$": "<rootDir>/__mocks__/imageMock.js",
  },
  moduleDirectories: ["node_modules", "src"],
  setupFilesAfterEnv: ["./jest.setup.js"],
};
