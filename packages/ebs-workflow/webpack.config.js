const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');
const DotenvWebpackPlugin = require('dotenv-webpack');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'ebs',
    projectName: 'workflow',
    webpackConfigEnv,
    argv,
  });

  // add to defaultConfig externals
  defaultConfig.externals.push(
    "@ebs/po",
    "@ebs/styleguide",
    "@ebs/components",
  );

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|svg|ttf|eot|woff|woff2)$/i,
          type: "asset/resource",
        },
      ],
    },
    resolve: {
      alias: {
        assets: path.resolve(__dirname, './src/assets/'),
        components: path.resolve(__dirname, './src/components/'),
        "custom-components": path.resolve(__dirname, './src/custom-components/'),
        helpers: path.resolve(__dirname, './src/helpers/'),
        mock: path.resolve(__dirname, './src/mock/'),
        pages: path.resolve(__dirname, './src/pages/'),
        store: path.resolve(__dirname, './src/store/'),
        utils: path.resolve(__dirname, './src/utils/'),
        hooks: path.resolve(__dirname, './src/hooks/'),
        validations: path.resolve(__dirname, './src/validations/'),
        __mocks__: path.resolve(__dirname, './__mocks__/'),
      },
    },
    plugins: [new DotenvWebpackPlugin()],
  });
};
