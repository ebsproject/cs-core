## EBS WORKFLOW
#### JSON PROPS:

##### TextField
````
  {
    id: 1,
    sort:1,
    sizes: [12, 12, 12, 12, 12],
    name: '',
    inputProps: {
      disabled: false,
      label: '',
      multiline: false,
      fullWidth: true,
      rows: 1,
      variant: 'outlined'
    },
    helper: {
      title: '',
      placement: 'top'
    },
    rules: {
      required: ''
    },
    defaultValue: ''
  }
  ````
  ###### CheckBox
  ````
  {
    id: 2,
    sort:1,
    helper: {
      title: "Check a value",
      placement: 'top'
    },
    checked: true
  }
  ````
  ##### DatePicker
  ````
  {
    id: 3,
    sort:1,
    sizes: [12, 12, 12, 12, 12],
    name: '',
    inputProps: {

    },
    helper: {
      title: '',
      placement: 'top'
    },
    rules: {
      required: ''
    },
    defaultValue: '' // the ebs form set by default the current date
  }
  ````
  ##### RadioGroup
  ````
  {
    id: 4,
    sort:1,
    sizes: [12, 12, 12, 12, 12],
    helper: {
      title: "Please select the type",
      placement: 'top'
    },
    options: [
      {
        label: "Propagative",
        value: "propagative"
      },
      {
        label: "Non Propagative",
        value: "non-propagative"
      }
    ],
    defaultValue: "propagative",
    inputProps: {label:""},
    rules:{required:""},
    row: false
  }
  ````
  ##### Select
  ````
  {
    id: 5,
    sort:1,
    sizes: [12, 12, 12, 12, 12],
    entity: "Valid Entity Name",
    columns:[], //{name:'Name',accessor :'requestor.person.givenName', hide:true} columns defined for EBS-Grid, for contact must have givenName and FamilyName, you can use hide: true to hide from ebs grid
    controller:{}, //{name:"recipientId", type:"Integer"} mapped to cs-api input object
    helper: {
      title: "",
      placement: "top"
    },
    filters: [],
    apiContent: [
      "id",
      "name"
    ],
    inputProps: {
      color: "primary",
      label: ""
    },
rules:{required:""}
  }
  ````
  ##### Switch
  ````
  {
    id: 6,
    sort:1,
    sizes: [12, 12, 12, 12, 12],
    helper: {
      title: "Mark if you want",
      placement: 'top'
    },
    inputProps: {
      color: "primary",
      disabled: false
    },
    rules:{required:""},
    defaultValue: true
  },
  ````
  ##### File
  ````
  {
    id: 8,
    sort:1,
    sizes: [12, 12, 12, 12, 12],
    name: '',
    customProps: {
      classes: "",
      color: 'primary',
      disabled: false,
      fullWidth: true,
      size: 'large',
      acceptedFiles: ['image/*'],
      cancelButtonText: "cancel",
      submitButtonText: "submit",
      maxFileSize: 5000000,
      showPreviews: true,
      showFileNamesInPreview: true
    },
    helper: {title:"",placement:"top"},
    rules: {
required:""
    },
    defaultValue: ''
  },
````
#### Node Definitions


``````
const nodeBase = {
  after:
  {
    executeNode: '',
    sendNotification: {
      send: false,
      message: ""
    }
  },
  before: {
    validate: {
      valid: false,
      type: "javascript",
      code: "",
      function: ""
    }
  },
  inputProps: {
    sourceNodes: []
  },
  outputProps: {
    targetNodes: []
  }
}

export const nodeDefinition = [
  {
    id: 1,
    title: '',
    ...nodeBase
  },
  {
    id: 3,
    template: '',
    ...nodeBase
  },
  {
    id: 4,
    email: '',
    ...nodeBase
  },
  {
    id: 5,
    status: '',
    ...nodeBase
  },
  {
    id: 7,
    title: '',
    message:'',
    ...nodeBase
  },
  {
    id: 6,
    node: '',
    action:'back | next | reset',
    ...nodeBase
  },
  {
    id: 8,
    component: '',
    ...nodeBase
  }

] 
```````
