// import Mocks for SystemJS mock below
import {
  Core,
  Styles,
  Icons,
  Colors,
  EbsDialog,
  EbsTabsLayout,
  Lab,
} from "@ebs/styleguide";
import { EbsGrid } from "@ebs/components";
import {
  getDomainContext,
  getContext,
  getAuthState,
  getTokenId,
} from "@ebs/layout";
import {
  POReportDesigner
} from "@ebs/po";
import * as Messaging from "@ebs/messaging"
import "@testing-library/jest-dom";
// Mock SystemJS
global.System = {
  import: jest.fn(mockImport),
};

function mockImport(importName) {
  // What you do in mock import will depend a lot on how you use SystemJS in the project and components you wish to test

  /* If I had already created a mock for `@react-mf/people` and I wanted to test this component:
   *  https://github.com/react-microfrontends/planets/blob/main/src/planets-page/selected-planet/selected-planet.component.js#L5
   * I would want `System.import('@react-mf/people')` to resolve to my mock one way to accomplish this would be the following
   */
  switch (importName) {
    case "@ebs/styleguide":
      return Promise.resolve({
        Core,
        Styles,
        Icons,
        Colors,
        EbsDialog,
        EbsTabsLayout,
        Lab,
      });
    case "@ebs/components":
      return Promise.resolve({ EbsGrid });
    case "@ebs/layout":
      return Promise.resolve({
        getDomainContext,
        getContext,
        getAuthState,
        getTokenId,
      });
      case "@ebs/po":
        return Promise.resolve({
          POReportDesigner
        });
        case "@ebs/messaging":
          return Promise.resolve({
            Messaging
          });
    default:
      return Promise.resolve({});
  }
}
