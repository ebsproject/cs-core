import MainBrowser from "components/runtime/MainBrowser/main-browser";
import { Suspense } from "react";

const Runtime = () => {

    return (
        <Suspense fallback={<>{"Loading workflow services..."}</>}>
            <MainBrowser />
        </Suspense>
    )
}
export default Runtime;