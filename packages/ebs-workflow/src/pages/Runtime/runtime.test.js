import { render, screen } from "@testing-library/react";
import Runtime from "./runtime";
import MainBrowser from "components/runtime/MainBrowser/main-browser";

jest.mock("components/runtime/MainBrowser/main-browser", () => () => (
  <div>MainBrowser Loaded</div>
));

describe("Runtime Component", () => {
//   it("displays the fallback loading message while loading MainBrowser", () => {
//     render(<Runtime />);
//     expect(screen.getByText("Loading workflow services...")).toBeInTheDocument();
//   });

  it("renders MainBrowser after it is loaded", async () => {
    render(<Runtime />);

    expect(await screen.findByText("MainBrowser Loaded")).toBeInTheDocument();
  });
});
