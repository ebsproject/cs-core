import WorkflowList from "components/designer/WorkflowList"

import { ProductDescriptionTitle } from "@ebs/layout"
import { Suspense } from "react";

const WorkflowDesigner = () => {
    return (
        <Suspense fallback={<>{"Loading workflow services..."}</>}>

            <ProductDescriptionTitle />
            <WorkflowList />

        </Suspense>

    )
}


export default WorkflowDesigner;