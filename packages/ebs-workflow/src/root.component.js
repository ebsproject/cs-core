import React, { lazy, useState } from 'react';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter as Router, Route, Routes, Outlet } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import store from './store';
import { persister } from './store';
import { client } from 'utils/apollo';
//import AlertMessage from 'components/message'
//import FormLoader from 'components/runtime/formLoader/FormLoader';
import { WorkflowContextProvider } from "components/designer/context/workflow-context"
//components
import WorkflowAddForm from "components/designer/Workflow"
import Workflows from 'pages/Workflows';
import WorkflowStatus from 'components/designer/WorkflowStatusList';
import StatusForm from 'components/designer/WorkflowStatus/statusForm';
import DocumentForm from 'components/designer/WorkflowDocuments/documentForm';
import Service from 'components/service';
import Runtime from 'pages/Runtime';
//const Runtime = lazy(()=> import("pages/Runtime"));
import WorkflowDocuments from 'components/designer/WorkflowDocumentList';

import { PersistGate } from 'redux-persist/integration/react';
// Routes
import { BASE_PATH, WORKFLOW, REQUEST, SERVICE, ADDFORM, STATUS, ADDSTATUS, ADD_DOCUMENTS, DOCUMENTS } from './routes';
//import LaunchDesigner from 'components/runtime/launchDesigner';
import { ServiceContextProvider } from 'custom-components/context/service-context';
//import AlertMessages from 'components/alerts/alert-component';
import { AlertContextProvider } from 'components/alerts/alert-context';
import AlertMessage from 'components/runtime/RowAction/alertmessage';
import AlertMessages from 'components/alerts/alert-component';
import LaunchDesigner from 'components/runtime/launchDesigner/LaunchDesigner';
import FormLoader from 'components/runtime/formLoader/FormLoader';
// const AlertMessages = lazy(()=> import("components/alerts/alert-component"));
// const AlertMessage =  lazy(()=> import("components/message"));
// const LaunchDesigner = lazy(()=> import("components/runtime/launchDesigner"));
// const FormLoader = lazy(()=> import("components/runtime/formLoader/FormLoader"));
function MainWorkflow() {
  return (
    <div>
      <Outlet />
    </div>
  );
};

export default function App() {
  const [locale, setLocale] = useState('en');
  const [messages, setMessages] = useState(null);
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persister}>
        <ApolloProvider client={client}>
          <IntlProvider locale={locale} messages={messages} defaultLocale='en'>
            <WorkflowContextProvider>
              <ServiceContextProvider>
                <AlertContextProvider>
                <Router>
                  <AlertMessages />
                  <AlertMessage />
                  <LaunchDesigner />
                  <FormLoader />
                  <Routes data-testid ={"RootComponentTestId"}>
                    <Route path={BASE_PATH} element={<MainWorkflow />}>
                      <Route path={WORKFLOW} element={<Workflows />} />
                      <Route path={ADDFORM} element={<WorkflowAddForm />} />
                      <Route path={SERVICE} element={<Service />} />
                      <Route path={REQUEST} element={<Runtime />} />
                      <Route path={STATUS} element={<WorkflowStatus />} />
                      <Route path={ADDSTATUS} element={<StatusForm />} />
                      <Route path={DOCUMENTS} element={<WorkflowDocuments />} />
                      <Route path={ADD_DOCUMENTS} element={<DocumentForm />} />
                    </Route>
                  </Routes>
                </Router>
                </AlertContextProvider>
              </ServiceContextProvider>
            </WorkflowContextProvider>
          </IntlProvider>
        </ApolloProvider>
      </PersistGate>
    </Provider>
  );
}
