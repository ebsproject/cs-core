import { getCoreSystemContext, getTokenId } from '@ebs/layout';
import axios from 'axios';
import PropTypes from 'prop-types';

export const useNotification = () => {
   const { graphqlUri, restUri } = getCoreSystemContext();
   const serviceUrl = graphqlUri.replace("graphql", "");

   const sendNotification = async ({ payload, path }) => {
      const headers = {
         Accept: "application/json",
         "Access-Control-Allow-Origin": "*",
         Authorization: `Bearer ${getTokenId()}`
      }
      try {
         const { data } = await axios.request({
            url: `${serviceUrl}${path}`,
            method: 'post',
            headers: headers,
            data: payload
         });
         return data.success
      } catch (error) {
        console.log(error)
         return false;
      }
   };

   sendNotification.prototype = {
      payload: PropTypes.object.isRequired,
      path: PropTypes.string.isRequired
    };
  
   return { sendNotification }

}
useNotification.propTypes = {
 };