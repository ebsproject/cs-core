import { createDefinitionCF, createDefinitionFromEntity } from "components/runtime/DialogBox/functions";
import { FIND_TOTAL_ITEMS } from "custom-components/Items/item-components/gql";
import { client } from "utils/apollo";
import { FIND_SERVICE_BY_RECORD_ID, FIND_SERVICE_DATA, FIND_SERVICE_LIST, FIND_UNIT_MEMBERS } from "utils/apollo/gql/request";
import { FIND_CF_VALUES_BY_EVENT_ID, FIND_NODE } from "utils/apollo/gql/workflow";
import { FIND_DOMAIN_LIST } from "utils/apollo/gql/catalogs";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/catalogs";
import { fetchDefinition } from "components/service/functions";

export async function getNode(nodeId) {
  return await client.query({
    query: FIND_NODE,
    variables: { id: nodeId }
  })
}

export function getNodesToExecute(node, stages) {
  let nodes = [];
  stages.forEach(item => {
    item.nodes.forEach(nodeChild => {
      if (nodeChild.dependOn.edges.includes(node.designRef)) {// check to change for designRef 
        nodes = [...nodes, nodeChild];
      }
    })
  });
  return nodes;
}
export async function getContactIds(recordId, node, rowData) {
  let ids = [];
  try {
    const { data } = await client.query({
      query: FIND_SERVICE_BY_RECORD_ID,
      variables: { filters: [{ col: 'id', mod: 'EQ', val: recordId }] },
      fetchPolicy: "no-cache"
    })
    const result = data.findServiceList.content[0];
    if (result.requestor && node.define.sendToRequestor)
      ids = [...ids, result.requestor.id];
    if (result.sender && node.define.sendToSender)
      ids = [...ids, result.sender.id];
    if (result.recipient && node.define.sendToRecipient)
      ids = [...ids, result.recipient.id];
    if (result.serviceProvider && node.define.sendToSHU) {
      let response = await getSHUEmails(Number(result.serviceProvider.id));
      ids = [...ids, ...response.map(item => item.id)];
    }
    if (node.define.sendToCreator) {
      let creator = rowData["createdBy"];
      const { data } = await client.query({
        query: FIND_CONTACT_LIST,
        variables: { filters: [{ col: "users.id", mod: "EQ", val: Number(creator['value']) }] },
        fetchPolicy: "no-cache"
      })
      let contactId = data.findContactList.content.length > 0 ? data.findContactList.content[0]["id"] : 0;
      ids = [...ids, contactId]
    }
    return ids;
  } catch (error) {
    console.log(error);
    return [0];
  }

}

export async function getContactEmails(recordId, node, rowData) {
  let emails = [];
  try {
    const { data } = await client.query({
      query: FIND_SERVICE_BY_RECORD_ID,
      variables: { filters: [{ col: 'id', mod: 'EQ', val: recordId }] },
      fetchPolicy: "no-cache"
    })
    const result = data.findServiceList.content[0];
  
    if (result.requestor && node.define.sendToRequestor)
      emails = [...emails, result.requestor.email];
    if (result.sender && node.define.sendToSender)
      emails = [...emails, result.sender.email];
    if (result.recipient && node.define.sendToRecipient)
      emails = [...emails, result.recipient.email];
    if (result.serviceProvider && node.define.sendToSHU) {
      let response = await getSHUEmails(Number(result.serviceProvider.id))
      emails = [...emails, ...response.map(item => item.email)];
    }
    if (node.define.sendToCreator) {
      let creator = rowData["createdBy"];
      const { data } = await client.query({
        query: FIND_CONTACT_LIST,
        variables: { filters: [{ col: "users.id", mod: "EQ", val: Number(creator['value']) }] },
        fetchPolicy: "no-cache"
      })
      let email = data.findContactList.content.length > 0 ? data.findContactList.content[0]["email"] : null;
      if (email)
        emails = [...emails, email]
    }
  
  
    if (node.define.contacts && node.define.contacts.length > 0) {
      for (let i = 0; i < node.define.contacts.length; i++) {
        const email = node.define.contacts[i];
        if (email.contact && email.contact.length > 0) {
          emails = [...emails, email.contact];
        }
      }
    }
    return emails;
  } catch (error) {
    console.log(error)
    return [];
  }

}
async function getSHUEmails(serviceProviderId) {

  let unitMembersEmail = [];

  try {
    const { data } = await client.query({
      query: FIND_UNIT_MEMBERS,
      variables: { filters: [{ col: "id", mod: "EQ", val: serviceProviderId }] },
      fetchPolicy: "no-cache"
    })

    const result = data.findContactList.content;
    if (result.length > 0) {
      const members = result[0].members
      for (let i = 0; i < members.length; i++) {
        let roles = members[i].roles || [];
        roles.forEach(item => {
          let productFunction = item.productfunctions || [];
          let isServiceProcessor = productFunction.find(item => item.action === "Service Processor")
          if (isServiceProcessor)
            unitMembersEmail = [...unitMembersEmail, { id: members[i].contact.id, email: members[i].contact.email }];
        })
      }
    }
    return unitMembersEmail;
  } catch (error) {
    console.log(error)
    return unitMembersEmail;
  }
}
export async function getServiceData(filters) {

  const { data } = await client.query({
    query: FIND_SERVICE_DATA,
    variables: { filters: filters },
    fetchPolicy: "no-cache"
  })
  const result = data.findWorkflowCustomFieldList.content[0];
  return result;
}
export async function checkItemValues(filter) {
  try {
    const { data } = await client.query({
      query: FIND_TOTAL_ITEMS,
      variables: {
        filters: filter,
      }, fetchPolicy: "no-cache"
    });
    if(data.findServiceItemList.content.length == 0) return 0;
    let totalElements =  data.findServiceItemList.content.filter(item => item.status.toLowerCase() !== 'rejected')
    return totalElements.length;
  } catch (error) {
    console.error(error)
    return 0;
  }
}

export async function loadDefinitions(rowData, workflowVariables, node) {
  let definition = { components: [] };
  let filters = [{ col:"event.recordId", mod: "EQ", val: Number(rowData?.id || 0) }]
  if(node){
     // filters.push({ col:"nodeCF.node.id", mod:"EQ", val: Number(node.id)})
  }
  const { data } = await client.query({
    query: FIND_CF_VALUES_BY_EVENT_ID,
    variables: {
      page: { number: 1, size: 100 },
      filters: filters,
      sort: [{ col: "nodeCF.id", mod: "ASC" }],
    },
    fetchPolicy: "no-cache",
  });
  const cfValues = data.findCFValueList.content;
  if (cfValues.length > 0) {
    definition = await createDefinitionCF(cfValues);
  }else{
    if(node && node['completed'] === false){
      try {
        let newNode = {...node}
        if(!newNode['nodeCFs']){
          let cf = workflowVariables.allAttributesV2.filter(item => Number(item.node.id) === Number(node.id))
          newNode = {...newNode, nodeCFs : cf}
        }
        const { formNodes } = await fetchDefinition([newNode], true, 'create', workflowVariables, null, rowData);
        definition = formNodes[0].formDefinition
      } catch (error) {
        console.error(error)
      }
    }
  }
  let result = await createDefinitionFromEntity(workflowVariables, rowData, node?.id || 0);
  result.components.forEach((item) => {
    definition.components.push(item);
  });
  definition.components = definition.components.sort((a, b) => {
    return a.sort - b.sort;
  });
  return definition;
}
export async function getSgContext(context) {
  const { data } = await client.query({
    query: FIND_DOMAIN_LIST,
    variables: { filters: [{ col: "prefix", val: context, mod: "EQ" }] }
  })
  let sgContext = data.findDomainList.content[0]
  return sgContext.domaininstances[0].sgContext;
}