import { useChangeStatus } from "../useChangeStatus";
import { useNotification } from "../useNotification";
import { useEmail } from "../useEmail";
import { useMoveNode } from "../useMoveNode";
import { getContext } from "@ebs/layout";
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from "react-redux";
import { getNodesToExecute, getNode, getContactIds, getContactEmails, getServiceData } from "hooks/utils/functions";
import { showForm, setNodeId } from "store/modules/generate";
import { showDesigner,setTemplate, setParams } from "store/modules/PrintOut";
import { showMessage } from "store/modules/message";
import validateFunctions from "validations";
import { getNodeToExecute } from "components/runtime/StepperBar/functions";
import { client } from "utils/apollo";
import { FIND_SERVICE, MODIFY_SERVICE } from "utils/apollo/gql/workflow";
import { useContext } from "react";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { userContext, useUserContext } from "@ebs/layout";
import { po_client } from "utils/axios";
import { axiosClient } from "utils/axios/axios";
import { useCallApi } from "hooks/useCallApi";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context";

export const useExecuteProcess = () => {
  let notificationsMessages = "";
 // const { rowData } = useContext(WorkflowContext);
  const { userProfile } = useUserContext() || {};//useContext(userContext);
  const dispatch = useDispatch();
  const { changeStatus } = useChangeStatus();
  const { sendEmail } = useEmail();
  const { sendNotification } = useNotification();
  const { moveNode } = useMoveNode();
  const { callApi } = useCallApi();
  const { dependOnNodes, workflowValues, allWorkflowNodes } = useSelector(({ wf_workflow }) => wf_workflow);
  const {setBlockButtons, rowData} = useServiceContext();
  const {setAlerts} = useAlertContext();
  const executeProcess = async ({ node, createdObj, target, form_data }) => {
    let failed = false;
    const _createdObj = createdObj;
    const afterNodeId = Number(node?.define?.after?.executeNode || 0);
    const processCode = node.process.code;
    const beforeAction = node.define.before;
    if ( beforeAction && beforeAction.validate.functions.length > 0) {
      let response;
      let message = "";
      let functionName = beforeAction.validate.functions
      const fn = validateFunctions[functionName];
      const res = await fn(createdObj.id, workflowValues.id, userProfile?.dbId);
      if (Object.prototype.toString.call(res) === '[object Object]') {
        response = res.result;
        message = res.message;
      }else{
        response = res;
      }
      if(!response){
        const errorMessage = `${message}`
      let errors = errorMessage.split(",")
        for (let i = 0; i < errors.length; i++)
          setAlerts((prev) => [...prev, { id: `${i}-validation-error`, message: errors[i], severity: 'error' }])
      setBlockButtons(false);
      return `Error!!: process validations have failed. ${errorMessage}`;
       }
    }
    if (node.nodeType && node.nodeType.name === "submit") {
      try {
        const { data: service } = await client.query({
          query: FIND_SERVICE,
          variables: { id: createdObj.id }
        })
        if (!service.findService) return;
        const obj = service.findService;
        let date = new Date();
        let input = {
          id: obj.id,
          programId: obj.program.id,
          submitionDate: date.toISOString().split("T")[0],
          tenantId: getContext().tenantId,
          byOccurrence: obj.byOccurrence
        }
       await client.mutate({
          mutation: MODIFY_SERVICE,
          variables: {
            service: input
          }
        })
      } catch (error) {
        return `Error!: ${error.toString()}` ;
      }
      return;
    }

    let path = node.process.path;
    let date = new Date();
    switch (processCode) {
      case "POPUP":
        dispatch(setNodeId(Number(node.define?.form)));
        dispatch(showForm(true));
        break;
      case "FORM":
        dispatch(setNodeId(node.id));
        dispatch(showForm(true));
        break;
      case "MOVE":
        payload = {
          recordId: Number(_createdObj.id),
          workflowInstanceId: Number(_createdObj.workflowInstanceId),
        }
        if (node.define.action.trim().length === 0 && node.define.node.trim().length === 0) break;
        if (node.define.action.trim().length > 1)
          payload.action = node.define.action;
        else
          payload.targetNodeId = Number(node.define.node);

        await moveNode({ payload, path });
        break;
      case "PP":
        if (node.define.globalAction)
          dispatch(setParams(null));
        else
          dispatch(setParams({ name: 'id', value: Number(_createdObj.id) }))
        dispatch(setTemplate(node?.define?.template))
        dispatch(showDesigner(true));
        break;
      case "CHANGE":
        payload = {
          recordId: Number(_createdObj.id),
          workflowInstanceId: Number(_createdObj.workflowInstanceId),
          statusTypeId: Number(node.define.status),
          tenantId: getContext().tenantId
        }
        await changeStatus({ payload, path });
        break;
      case "EMAIL":
        let _rowData = {};
          const defaultFilters = [
            { col: "workflowId", mod: "EQ", val: workflowValues.id },
            { col: "userId", mod: "EQ", val: userProfile?.dbId },
            { col: "id", mod: "EQ", val: _createdObj.id },
            { col: "wfInstance", mod: "EQ", val: _createdObj.workflowInstanceId },
          ];
          _rowData = {...await getServiceData(defaultFilters)};
        let emails = await getContactEmails(_createdObj.id, node, _rowData);
        const contactEmails = emails.length > 0 ? emails.join(","): null;
        if(!contactEmails) break;
        _rowData = {..._rowData , status : _rowData.status ? _rowData.status.toUpperCase() : "CREATED"}
        _rowData = {..._rowData, currentUser: userProfile.full_name, date: date.toISOString().split("T")[0] }
        payload = {
          to: contactEmails,
          from: "no-reply@ebsproject.org",
          subject: '',
          body: '',
          template: {
            id: Number(node.define.email),
            data: _rowData
          }
        }
        const formData = new FormData();
        if (node.define.attachments && node.define.attachments.attach && node.define.attachments.templates && node.define.attachments.templates.length > 0) {
          for(let i = 0; i < node.define.attachments.templates.length; i++) {
            const template = node.define.attachments.templates[i];
            const { status: poStatus, data: poData } = await po_client.get(`/api/report/export?name=${template.template}&format=${template.format}&parameter[includeItems]=true&parameter[id]=${_rowData.id}`, {
              responseType: "blob",
            });
            if(poStatus !== 200)
                 continue;
            let fileName = template.template;
            const documentName = `[${fileName}]-${_rowData.requestCode}.${template.format}`;
            const file = new File([poData],documentName,{
                type: template.format === 'pdf' ? "application/pdf" : 'text/csvcharset=utf-8' ,
              }
            );
            formData.append("file", file);
          }
          formData.append("payload", JSON.stringify(payload));
          try {
            await axiosClient.post(`send/email`, formData);
          } catch (error) {
            console.error(error);
            setAlerts((prev) => [...prev, { id: `email-error`, message: JSON.stringify(error), severity: 'error' }])
            break;
          }
        }
        else {
          formData.append("payload", JSON.stringify(payload));
          try {
            await axiosClient.post(`send/email`, formData);
          } catch (error) {
            console.error(error)
            setAlerts((prev) => [...prev, { id: `email-error`, message: JSON.stringify(error), severity: 'error' }])
            break;
          }
        }
        break;
      case "NOTIFICATION":
        let _row;
        const _defaultFilters = [
          { col: "workflowId", mod: "EQ", val: workflowValues.id }, 
          { col: "userId", mod: "EQ", val: userProfile?.dbId },
          { col: "id", mod: "EQ", val: _createdObj.id },
          { col: "wfInstance", mod: "EQ", val: _createdObj.workflowInstanceId },
        ];
        _row = await getServiceData(_defaultFilters);
        let ids = await getContactIds(_createdObj.id, node, _row);
        let notificationMessage = node.define.message;
         notificationMessage = notificationMessage.replace("[date]", `[${date.toISOString().split("T")[0]}]`)
                                                  .replace("[currentUser]",`<strong>${userProfile.full_name}</strong>`)
                                                  .replace("[requestCode]",`<strong>${_row.requestCode}</strong>`);
         payload = {
          recordId: Number(_createdObj.id),
          jobWorkflowId: 3,
          message: { status: "submitted", message: { "status": "submitted", "title": node.define.title, "description": notificationMessage } },
          contacts: ids
        }

        await sendNotification({ payload, path })
        break;
      case "COND":
        let definition = { ...node.define };
        let nodeId = 0;
        let field = ""
        let expectedValue = "";
        field = definition.field;
        expectedValue = definition.expectedValue;
        let formDataValue = form_data[field];
        if (formDataValue === null || formDataValue === undefined) break;
        if (String(formDataValue) === expectedValue) {
          nodeId = definition.executeNodeIfTrue;
        } else {
          nodeId = definition.executeNodeIfFalse;
        }
        let cond_node = allWorkflowNodes.find(item => Number(item.id) === Number(nodeId))
        if (cond_node) {
          await executeProcess({ node: cond_node, createdObj: _createdObj, form_data });
        }
        break;
      case "API":
            let payload = {
              definition: node.define,
              formData : form_data,
              serviceId: _createdObj.id
            }
          const res =  await callApi({payload})
             if(res.error === true)
               failed = true;
            break;
      default:
        break;
    }
    if (node.define.after.sendNotification.send && !failed) {
       // notificationsMessages += `${node.define.after.sendNotification.message}. ` +"\n";
        setAlerts((prev) => [...prev, {id:`${node.id}-success-message`, message: node.define.after.sendNotification.message, severity:'success'}]);
      //   dispatch(showMessage({
      //     variant: "success",
      //     message: notificationsMessages,
      //     anchorOrigin: { vertical: "top", horizontal: "right" },
      // }))

  }
    if (afterNodeId && afterNodeId > 1) {
      const { data } = await getNode(afterNodeId);
      if (data.findNode) {
        let node = data.findNode;
        await executeProcess({ node, createdObj: _createdObj, form_data });
      }
    }
    if (node.stages) {
      const edgesNodes = getNodesToExecute(node, [node.stages]);
      for (let i = 0; i < edgesNodes.length; i++) {
        let node = edgesNodes[i];
        await executeProcess({ node, createdObj: _createdObj, form_data });
      }
    }
    if (dependOnNodes.length > 0) {
      let _dependOnNodes = [...dependOnNodes];
      const nodes = _dependOnNodes.sort((a, b) => { return a.sort - b.sort });
      for (let i = 0; i < nodes.length; i++) {
        let nd = nodes[i];
        if (nd.target==node.designRef) {
          let node = allWorkflowNodes.find(item => Number(item.id) === Number(nd.id))//  await getNodeToExecute(nd.id);
          await executeProcess({ node, createdObj: _createdObj, form_data });
        }
      }
    }

  }

  executeProcess.prototype = {
    node: PropTypes.object.isRequired,
    createdObj: PropTypes.object.isRequired
  };


  return { executeProcess }

}

useExecuteProcess.propTypes = {
};