import { getTokenId } from '@ebs/layout';
import axios from 'axios';
import { useAlertContext } from 'components/alerts/alert-context';
import { getSgContext } from 'hooks/utils/functions';
import PropTypes from 'prop-types';

export const useCallApi = () => {
    const { setAlerts } = useAlertContext();

    const callApi = async ({ payload }) => {
        const context = payload.definition.sgcontext;
        const sgContext = await getSgContext(context);
        let headers = {
            Accept: "application/json",
            "Access-Control-Allow-Origin": "*",
            Authorization: `Bearer ${getTokenId()}`
        }
        let formData = new FormData();
        try {
            const _data = payload.formData;
            switch (payload.definition.bodyType) {
                case "binary":
                    break;
                case "form-data":
                    let _file = _data[payload.definition.field];
                    headers = { ...headers, 'Content-Type': 'multipart/form-data', }
                    formData.append('file', _file[0])
                    formData.append('serviceId', payload.serviceId)
                    break;
                default:
                    break;
            }
            const { data } = await axios.request({
                url: new URL(payload.definition.path, sgContext).toString(),
                method: payload.definition.method,
                headers: headers,
                data: formData
            });
            return { error: false }
        } catch (error) {
            setAlerts((prevItems) => [...prevItems, { id: 'upload-file-error', message: error.response.data, severity: 'error' }]);
            return { error: true, message: error.response };
        }
    };
    callApi.prototype = {
        payload: PropTypes.object.isRequired
    };

    return { callApi }

}
