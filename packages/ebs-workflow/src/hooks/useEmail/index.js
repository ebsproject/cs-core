import { getCoreSystemContext, getTokenId } from '@ebs/layout';
import axios from 'axios';
import PropTypes from 'prop-types';

export const useEmail = () => {
   const { graphqlUri } = getCoreSystemContext();
   const serviceUrl = graphqlUri.replace("graphql", "");

   const sendEmail = async ({ payload, path }) => {
      const headers = {
         Accept: "application/json",
         "Access-Control-Allow-Origin": "*",
         Authorization: `Bearer ${getTokenId()}`
      }
      try {
         const { data } = await axios.request({
            url: `${serviceUrl}${path}`,
            method: 'post',
            headers: headers,
            data: payload
         });
         return data.success
      } catch (error) {
         return false;
      }
   };
   sendEmail.prototype = {
      payload: PropTypes.object.isRequired,
      path: PropTypes.string.isRequired
    };
  
   return { sendEmail }

}
useEmail.propTypes = {
 };