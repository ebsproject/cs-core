import { combineReducers } from 'redux';
import wf_workflow from "store/modules/Workflow";
import wf_message from "store/modules/message";
import wf_user from "store/modules/User"
import wf_generate from "store/modules/generate";
import wf_printout from "store/modules/PrintOut";

export default combineReducers({ wf_workflow, wf_message, wf_user, wf_generate, wf_printout });
