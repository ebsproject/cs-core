import reducer, {
    initialState,
    showDesigner,
    hideDesigner,
    setTemplate,
    setParams,
    SHOW_DESIGNER,
    HIDE_DESIGNER,
    SET_TEMPLATE,
    SET_PARAMS,
  } from ".";
  
  describe('Designer PrintOut Redux Store', () => {

    test('should create an action to show the designer', () => {
      const payload = true;
      const expectedAction = { type: SHOW_DESIGNER, payload };
      expect(showDesigner(payload)).toEqual(expectedAction);
    });
  
    test('should create an action to hide the designer', () => {
      const payload = false;
      const expectedAction = { type: HIDE_DESIGNER, payload };
      expect(hideDesigner(payload)).toEqual(expectedAction);
    });
  
    test('should create an action to set a template', () => {
      const payload = "New Template";
      const expectedAction = { type: SET_TEMPLATE, payload };
      expect(setTemplate(payload)).toEqual(expectedAction);
    });
  
    test('should create an action to set params', () => {
      const payload = { key: "value" };
      const expectedAction = { type: SET_PARAMS, payload };
      expect(setParams(payload)).toEqual(expectedAction);
    });
  
    test('should return the initial state', () => {
      expect(reducer(undefined, {})).toEqual(initialState);
    });
  
    test('should handle SHOW_DESIGNER', () => {
      const action = showDesigner(true);
      const expectedState = { ...initialState, open: true };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  
    test('should handle HIDE_DESIGNER', () => {
      const action = hideDesigner(false);
      const expectedState = { ...initialState, open: false };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  
    test('should handle SET_TEMPLATE', () => {
      const action = setTemplate("New Template");
      const expectedState = { ...initialState, template: "New Template" };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  
    test('should handle SET_PARAMS', () => {
      const action = setParams({ key: "value" });
      const expectedState = { ...initialState, params: { key: "value" } };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });
  