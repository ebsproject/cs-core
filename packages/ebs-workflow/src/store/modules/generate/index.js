/*
 Initial state and properties
 */
 export const initialState = {
    show:false,
    nodeId:null
 }
 /*
  Action types
  */
 export const SHOW_FORM = 'SHOW_FORM';
 export const HIDE_FORM = 'HIDE_FORM';
 export const SET_NODE_ID= "SET_NODE_ID";
 /*
  Arrow function for change state
  */
 export const showForm = (payload) => ({
   type: SHOW_FORM,
   payload,
 })
 export const hideForm = (payload) => ({
    type: HIDE_FORM,
    payload,
  })
  export const setNodeId = (payload) => ({
    type: SET_NODE_ID,
    payload,
  })
 /*
  Reducer to describe how the state changed
  */
 export default function Reducer(state = initialState, { type, payload }) {
   switch (type) {
     case HIDE_FORM:
       return {
         ...state,
         show: payload,
       }
       case SHOW_FORM:
        return {
          ...state,
          show: payload,
        }
        case SET_NODE_ID:
          return {
            ...state,
            nodeId: payload,
          }
     default:
       return state
   }
 }
 