import { modifyItems } from "custom-components/Items/item-components/functions";
import { FIND_TOTAL_FILES, FIND_TOTAL_ITEMS } from "custom-components/Items/item-components/gql";
import { checkItemValues } from "hooks/utils/functions";
import { client } from "utils/apollo";
import { FIND_SERVICE_DATA, FIND_SERVICE_LIST } from "utils/apollo/gql/request";
import { FIND_FILE_TYPE_LIST, FIND_SERVICE, MODIFY_SERVICE, FIND_CF_VALUES_BY_EVENT_ID, MODIFY_CF_VALUES_BULK } from "utils/apollo/gql/workflow";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/catalogs";

const validateFunctions = {
    checkItems: async (id) => {
        try {
            const { data } = await client.query({
                query: FIND_TOTAL_ITEMS,
                variables: { filters: [{ col: 'service.id', mod: 'EQ', val: Number(id) }] },
                fetchPolicy: "no-cache"
            })
            if (data.findServiceItemList.totalElements === 0) return {result:false, message:"Please import a package list before continue."};
            if (data.findServiceItemList.totalElements > 0) return true;
        } catch (error) {
            return false;
        }
    },
    checkDocuments: async (id) => {
        try {
            const { data } = await client.query({
                query: FIND_TOTAL_FILES,
                variables: { filters: [{ col: 'service.id', mod: 'EQ', val: Number(id) }] },
                fetchPolicy: "no-cache"
            })
            if (data.findServiceFileList.totalElements === 0) return false;
            if (data.findServiceFileList.totalElements > 0) return true;
        } catch (error) {
            return false;
        }
    },
    validateItems: async (id, wfId, userId) => {
        try {
            const { data } = await client.query({
                query: FIND_SERVICE,
                variables: { id: id },
                fetchPolicy: "no-cache"
            })
            if (data.findService.byOccurrence === true) return true;
            let messages = ["Please review the following: "];
            let result = true;
            let response = {
                result: true,
                message: ""
            }
            let filter = [{ col: "service.id", val: Number(id), mod: "EQ" }];
            const totalElements = await checkItemValues(filter);
            const totalQuantity = await checkItemValues([...filter, { col: "weight", mod: "EQ", val: 0 }]);
            const totalPackage = await checkItemValues([...filter, { col: "packageCount", mod: "EQ", val: 0 }]);
            const totalStatus = await checkItemValues([...filter, { col: "mtaStatus", mod: "EQ", val: "NA" }]);

            if (totalElements === 0) {
                result = false;
                messages.push(`There are no items to check, please upload/import items first.`);
            }
            if (totalPackage > 0) {
                result = false;
                messages.push("Enter all package count values");
            }
            if (totalQuantity > 0) {
                result = false;
                messages.push("Enter all weight values");
            }
            const resp = await validateMTAIsRequired(id, wfId, userId);

            if (!resp) {
                if (totalStatus > 0) {
                    result = false;
                    messages.push('The MTA values in all items must be provided.');
                }
            }
            response.message = messages.join(", ");
            response.result = result;
            return response;

        } catch (error) {
            console.log(error)
            return true;
        }
    },
    validateRequiredDocuments: async (id, wfId) => {
        try {
            await setReceivedDateToProcessing({id,field:"receivedDate"})
            const { data: files_uploaded } = await client.query({
                query: FIND_TOTAL_FILES,
                variables: { filters: [{ col: 'service.id', mod: 'EQ', val: Number(id) }] },
                fetchPolicy: "no-cache"
            });
            const filesUploaded = files_uploaded.findServiceFileList.content.map(item => { return { id: item.id, name: item.fileType?.description } });
            const allFiles = await getAllDocumentsByWorkflowId(id);
            let missingFiles = "The following documents are not present:";
            let failed = false;
            for (let i = 0; i < allFiles.length; i++) {
                const item = allFiles[i]
                let findResult = filesUploaded.find(i => i.name === item.name);
                if (!findResult) {
                    failed = true
                    missingFiles += ` "${item.name}".`;
                }
            }
            if (failed) {
                missingFiles += " Please review it and attach it.\n"
                let response = {
                    result: false,
                    message: missingFiles
                }
                return response;
            }
            return true;
        } catch (error) {
            console.log(error)
            return false;
        }
    },
    markAllItemsAsRejected: async (id) => {
        let input = {
            id: 0,
            status: "Rejected"
        }
        await modifyItems({ input, service: { entity: "Service", id: id }, id: id });
        await modifyService(id);
        return true;
    },
    validateMTA: async (id, wfId, userId) => {
        try {
            let messages = ["Please review the following: "];
            let result = true;
            let response = {
                result: true,
                message: ""
            }
            const { data: service } = await client.query({
                query: FIND_SERVICE_DATA,
                variables: {
                    filters: [
                        { col: "id", mod: "EQ", val: Number(id) },
                        { col: "workflowId", mod: "EQ", val: wfId },
                        { col: "userId", mod: "EQ", val: userId },
                    ],
                },
                fetchPolicy: "no-cache"
            });
            let data = service.findWorkflowCustomFieldList.content[0]
            if (!data || data.length === 0) {
                return true;
            }
            let shipmentType = data["shipmentType"];
            let materialType = data["materialType"];
            let shipmentPurpose = data["shipmentPurposeDd"];

            if (shipmentType === "external" && materialType === "non-propagative" && shipmentPurpose === "Commercial analysis") {
                return true;
            } else {
                let filter = [{ col: "service.id", val: Number(id), mod: "EQ" }];
                const totalMTA = await checkItemValues([...filter, { col: "mtaStatus", mod: "EQ", val: "NA" }]);
                if (totalMTA > 0) {
                    result = false;
                    messages.push(`The MTA values in all items must be provided.`);
                }
            }
            response.message = messages.join(", ");
            response.result = result;
            return response;
        } catch (error) {
            console.error(error);
            return true;
        }
    },
    validateTemplateShow: async (id, wfId, userId, rules) => {
        try {
            let response = {
                result: true,
                message: ""
            }
            const { data: service } = await client.query({
                query: FIND_SERVICE_DATA,
                variables: {
                    filters: [
                        { col: "id", mod: "EQ", val: Number(id) },
                        { col: "workflowId", mod: "EQ", val: wfId },
                        { col: "userId", mod: "EQ", val: userId },
                    ],
                },
                fetchPolicy: "no-cache"
            });
            let data = service.findWorkflowCustomFieldList.content[0]
            if (!data || data.length === 0) {
                return true;
            }
            rules.parameters && rules.parameters.forEach(item => {
                const value = data[item.columnName];
                if (value !== item.columnValue) {
                    response.result = false;
                }
            })
            return response;
        } catch (error) {
            console.error(error);
            return {
                result: true,
                message: ""
            };
        }
    },
    setRecipientSameAsRequestor: (e, { setValue, getValues }) => {
        const flagValue = e.target.checked;
        if (flagValue) {
            setValue("recipientId", getValues("requestorId"));
            setValue("recipientEmail", getValues("requestorEmail"));
            setValue("recipientAddress", getValues("requestorAddress"));
            setValue("recipientInstitution", getValues("requestorInstitution"));
        } else {
            setValue("recipientId", null);
            setValue("recipientEmail", "");
            setValue("recipientAddress", null);
            setValue("recipientInstitution", "");
        }
    }
};
async function getAllDocumentsByWorkflowId(id) {
    try {
        const { data: service } = await client.query({
            query: FIND_SERVICE,
            variables: { id: id }
        });
        const { data: files } = await client.query({
            query: FIND_FILE_TYPE_LIST,
            variables: {
                filters: [{ col: "workflow.id", mod: "EQ", val: Number(service.findService.workflowInstance.workflow.id) }]
            }
        });
        const obj = files.findFileTypeList.content.map(item => { return { id: item.id, name: item.description } })
        return obj

    } catch (error) {
        return []
    }
}
export default validateFunctions;

export function checkPermissions(userPermissions, nsd) {
    let nodeSecurityDefinition = Object.assign({}, nsd)
    let access = false;
    if (!nsd)
        return access;
    if (!Object.keys(nodeSecurityDefinition).includes('roles') || nodeSecurityDefinition.roles === null)
        nodeSecurityDefinition.roles = [];
    if (!Object.keys(nodeSecurityDefinition).includes('programs') || nodeSecurityDefinition.programs === null)
        nodeSecurityDefinition.programs = [];
    if (!Object.keys(nodeSecurityDefinition).includes('users') || nodeSecurityDefinition.users === null)
        nodeSecurityDefinition.users = [];



    userPermissions.roles.forEach(role => {
        if (nodeSecurityDefinition.roles.map(role => role.name).includes(role))
            access = true;
    });

    userPermissions.programs.forEach(program => {
        if (nodeSecurityDefinition.programs.map(item => Number(item.id)).includes(Number(program.id)))
            access = true;
    });

    if (nodeSecurityDefinition.users.map(item => Number(item.id)).includes(Number(userPermissions.userId)))
        access = true;

    return access;
}
async function modifyService(id) {
    try {
        const { data: service } = await client.query({
            query: FIND_SERVICE,
            variables: { id: id }
        })
        if (!service.findService) return;
        const obj = service.findService

        let input = {
            id: obj.id,
            programId: obj.program.id,
            submitionDate: null,
            tenantId: 1
        }
        await client.mutate({
            mutation: MODIFY_SERVICE,
            variables: {
                service: input
            }
        })
    } catch (error) {
        console.error(error)
        return `Error!: ${error.toString()}`;
    }
}
async function validateMTAIsRequired(id, wfId, userId) {
    try {
        const { data: service } = await client.query({
            query: FIND_SERVICE_DATA,
            variables: {
                filters: [
                    { col: "id", mod: "EQ", val: Number(id) },
                    { col: "workflowId", mod: "EQ", val: wfId },
                    { col: "userId", mod: "EQ", val: userId },
                ],
            },
            fetchPolicy: "no-cache"
        });
        let data = service.findWorkflowCustomFieldList.content[0]
        if (!data || data.length === 0) {
            return true;
        }
        let shipmentType = data["shipmentType"];
        let materialType = data["materialType"];
        let shipmentPurpose = data["shipmentPurposeDd"];

        if (shipmentType === "external" && materialType === "non-propagative" && shipmentPurpose === "Commercial analysis") {
            return true;
        }
        else {
            return false;
        }

    } catch (error) {
        console.error(error);
        return false;
    }
}
export async function validateActionsAllowed(rowData, nodes, userProfile) {
    let defaultRules = { allowEdit: true, allowDelete: true, allowView: true, allowViewProgress: true, allowAddNotes: true };
    let status = rowData["statusid"];
    let creator = rowData['createdBy'];
    if (!status) return defaultRules;
    let nodeStatus = nodes.filter(item => Number(item.define.status) === Number(status));
    let user_Id = userProfile["dbId"];
    let isSender = false;
    let isRecipient = false;
    let isRequestor = false;
    let isCreator = false;
    let sender = rowData["sender"];
    let recipient = rowData["recipient"];
    let requestor = rowData["requestor"];
    let contactId = 0;
    try {
        const { data } = await client.query({
            query: FIND_CONTACT_LIST,
            variables: { filters: [{ col: "users.id", mod: "EQ", val: user_Id}] },
        })
        contactId = data.findContactList.content.length > 0 ? data.findContactList.content[0]["id"] : 0;
    } catch (error) {
       console.error(error)
       return  { allowEdit: false, allowDelete: false, allowView: true, allowViewProgress: true, allowAddNotes: true };
    }
    if(creator){
        if(Number(user_Id) === Number(creator['value']))
            isCreator = true;
    }
    if (sender) {
        if (Number(contactId) === Number(sender["value"]))
            isSender = true;
    }
    if (recipient) {
        if (Number(contactId) === Number(recipient["value"]))
            isRecipient = true;
    }
    if (requestor) {
        if (Number(contactId) === Number(requestor["value"]))
            isRequestor = true;
    }
    let isSHU = extractProductFunctionFromUserProfile("Service Processor", userProfile, rowData);
    if (nodeStatus.length > 0) {
        let node = nodeStatus[0];
        let definition = node.define;
        if (!definition["customSecurityRules"]) {
            return defaultRules;
        } else {
            let rules = definition.customSecurityRules
            if (!rules || !Array.isArray(rules) || rules.length === 0) return defaultRules;
            let rulesArray = [];
            let rulesObj = { allowEdit: false, allowDelete: false, allowView: false, allowViewProgress: false, allowAddNotes: false };
            if (isCreator) {
                const item = rules.find(item => item.actor.toLowerCase() === "creator")
                if(item){
                    rulesArray.push(item);
                }else{
                    rulesArray.push(defaultRules);
                }
            }
            if (isSender) {
                const item = rules.find(item => item.actor.toLowerCase() === "sender")
                if(item){
                    rulesArray.push(item);
                }else{
                    rulesArray.push(defaultRules);
                }
            }
            if (isRequestor) {
                const item = rules.find(item => item.actor.toLowerCase() === "requestor")
                if(item){
                    rulesArray.push(item)
                }else{
                    rulesArray.push(defaultRules)
                }
            }
            if (isRecipient) {
                const item = rules.find(item => item.actor.toLowerCase() === "recipient")
                if(item){
                    rulesArray.push(item)
                }else{
                    rulesArray.push(defaultRules)
                }
            }
            if (isSHU) {
                const item = rules.find(item => item.actor.toLowerCase() === "shu")
                if(item){
                    rulesArray.push(item)
                }else{
                    rulesArray.push(defaultRules)
                }
            }
            rulesArray.forEach(item => {
                rulesObj["allowEdit"] = item["allowEdit"] || rulesObj["allowEdit"];
                rulesObj["allowDelete"] = item["allowDelete"] || rulesObj["allowDelete"];
                rulesObj["allowView"] = item["allowView"] || rulesObj["allowView"];
                rulesObj["allowViewProgress"] = item["allowViewProgress"] || rulesObj["allowViewProgress"];
                rulesObj["allowAddNotes"] = item["allowAddNotes"] || rulesObj["allowAddNotes"];
            })
                return rulesObj;
        }
    } else {
        return { allowEdit: true, allowDelete: true, allowView: true, allowViewProgress: true, allowAddNotes: true };
    }
}
function extractProductFunctionFromUserProfile(productFunction, _userProfile, rowData) {
    const memberOf = _userProfile.permissions['memberOf'];
    const roles = memberOf['roles'];
    let exitFlag = false;
    for(let i = 0; i < roles.length; i++){
        const item = roles[i]
        if(item === 'Admin') exitFlag = true;
    }
    if(exitFlag) return true;
    const serviceProcessor = rowData['serviceProvider']
    const program = rowData['program']
    let userProfile = { ..._userProfile };
    let permission = userProfile["permissions"];
    if (!permission) return false;
    let applications = permission["applications"];
    if (!applications || applications.length === 0) return false;
    let SMProduct = applications.find(item => item.domain === "Service Management");
    if (!SMProduct) return false;
    let products = SMProduct["products"];
    if (!products || products.length === 0) return false;
    let requestNoGSR = products.find(item => item.name === "Request No-GSR");
    if (!requestNoGSR) return false;
    let actions = requestNoGSR["actions"];
    if (!actions || actions.length === 0) return false;

    let isPresent = false;
    for (let i = 0; i < actions.length; i++) {
        let a = actions[i];
        let keys = Object.keys(a)
        keys.forEach(k => {
             if((serviceProcessor['label'] && serviceProcessor['label'].includes(k)) || k === program['label'] ){
            let array = a[k];
            if (Array.isArray(array) && array.length > 0) {
                for (let j = 0; j < array.length; j++) {
                    let item = array[j];
                    if (item === productFunction)
                        isPresent = true;
                }
            }
         }
        })
    }
    return isPresent;
}
export async function setReceivedDateToProcessing(props){
    const { data } = await client.query({
        query: FIND_CF_VALUES_BY_EVENT_ID,
        variables: {
          page: { number: 1, size: 300 },
          filters: [{ col: "event.recordId", mod: "EQ", val: Number(props.id) }],
          sort: [{ col: "nodeCF.id", mod: "ASC" }],
        },
        fetchPolicy: "no-cache",
      });

    const response = data.findCFValueList.content;
    if (response.length === 0) return;
    const cf = response.find(item => item.nodeCF.name === props.field)
    if(!cf) return;
     await modifyCustomFieldValues(cf)
}
const modifyCustomFieldValues = async (cf) => {
    let date = new Date();
    let input = [];
        const cfValueInput = {
          id: cf.id,
          textValue:date.toISOString().split("T")[0],
          codeValue: 0,
          numValue: 0,
          flagValue:false,
          nodeCFId: cf.nodeCF.id,
          eventId: cf.event.id,
          dateValue: date.toISOString().split("T")[0],
          tenantId: 1,
        };
        input = [...input, cfValueInput];
    try {
      const { errors } = await client.mutate({
        mutation: MODIFY_CF_VALUES_BULK,
        variables: { cfValues: input },
      });
  
      if (errors) return `Error! ${errors}`;
    } catch (error) {
      console.log(error);
    }
  };