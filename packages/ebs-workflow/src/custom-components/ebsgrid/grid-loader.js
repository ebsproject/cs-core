import { Core } from "@ebs/styleguide";
import { EbsGrid } from "@ebs/components";
const {Typography, Button} = Core;
import { FormattedMessage } from "react-intl";

const GridLoader = ({parentControl,handleClose}) => {

    const columns = [
        { Header: "Id", accessor: "id", hidden: false, disableGlobalFilter: true },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    <FormattedMessage id="none" defaultMessage="Name" />
                </Typography>
            ),
            accessor: "name",
            width: 200,
            filter: true,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    <FormattedMessage id="none" defaultMessage="Experiment" />
                </Typography>
            ),
            accessor: "experiment",
            width: 200,
            filter: true,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    <FormattedMessage id="none" defaultMessage="Place" />
                </Typography>
            ),
            accessor: "place",
            width: 200,
            filter: true,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    <FormattedMessage id="none" defaultMessage="Status" />
                </Typography>
            ),
            accessor: "status",
            width: 200,
            filter: true,
        }
    ];
    const data = [
    { id: 1, name: "Occurrence 0001", experiment: "Experiment 0001", place: "Texcoco" , status:"OK"},
    { id: 2, name: "Occurrence 0002", experiment: "Experiment 0001", place: "Experiment place", status:"OK" },
    { id: 3, name: "Occurrence 0003", experiment: "Experiment 0001", place: "San Vicho", status:"OK" },
    { id: 4, name: "Occurrence 0004", experiment: "Experiment 0001", place: "Chinconcuac", status:"OK" },
    { id: 5, name: "Occurrence 0005", experiment: "Experiment 0001", place: "Rusia" , status:"OK"}
    ];
    const toolbar = () =>{
        return(
            <>
                        <Button  color="secondary" onClick={handleClose}>Cancel</Button>
                        <Button  color="primary" onClick={handleClose}>Continue</Button>
            </>

        )
    }
    const fetch = ({ page, sort, filters }) => {
        return new Promise((resolve, reject) => {
            try {
                resolve({
                    elements: data.length,
                    data: data,
                    pages: 1,
                });
            } catch (error) { }
        });
    };
    return (
        <EbsGrid
            id={"grid_loader"}
            toolbar={true}
            toolbaractions={toolbar}
            fetch={fetch}
            columns={columns}
            height="60vh"
            disabledViewConfiguration
            disabledViewDownloadCSV
            disabledViewPrintOunt
        />
    )
}
export default GridLoader;