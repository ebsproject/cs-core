import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { FormattedMessage } from "react-intl";
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { fileClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { actionDeleteDocument, deleteFile } from "./functions";
import { useAlertContext } from "components/alerts/alert-context";

// CORE COMPONENTS
const {
    IconButton,
    Tooltip,
    DialogContent,
    Button,
    Typography,
    DialogActions,
    CircularProgress
} = Core;
const { Delete } = Icons;

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const DeleteDocument = React.forwardRef(
    ({ refresh, rowData }, ref) => {
        const [open, setOpen] = useState(false);
        const [loading, setLoading] = useState(false)
        const handleClickOpen = () => setOpen(true);
        const handleClose = () => {setOpen(false)};
        const { setAlerts } = useAlertContext();

        const submit = async () => {
            try {
                setLoading(true);
                await actionDeleteDocument(rowData);
                await deleteFile(rowData?.id);
                setAlerts((prev) => [...prev, { id: 'file-delete-message-success-' + rowData.id, message: "File deleted", severity: 'success' }]);
                handleClose();
                refresh();
                setLoading(false)
            } catch ( error ) {
                const message = error.response.data.metadata.status[0].message;
                setAlerts((prev) => [...prev, { id: 'file-delete-message-error-' + rowData.id, message: message, severity: 'error' }]);
                setLoading(false)
            }
        };

        return (
            <div ref={ref} data-testid={"DeleteDocumentId"}>
                <Tooltip
                    arrow
                    title={
                        <Typography className="font-ebs text-xl">
                            <FormattedMessage id="none" defaultMessage="Delete File" />
                        </Typography>
                    }
                >
                    <IconButton
                        onClick={handleClickOpen}
                        aria-label="delete-document"
                        color="primary"
                        data-testid={"DeleteSingleDocumentTestId"}
                    >
                        <Delete />
                    </IconButton>
                </Tooltip>
                <EbsDialog
                    open={open}
                    handleClose={handleClose}
                    maxWidth="sm"
                    title={<FormattedMessage id="none" defaultMessage="Delete File" />}
                >
                    <DialogContent dividers>
                        <Typography className="text-ebs text-bold">
                            <FormattedMessage
                                id="none"
                                defaultMessage="Do you want to delete this file permanently?"
                            />
                        </Typography>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} data-testid={"DeleteButtonCancelTestId"}>
                            <FormattedMessage id="none" defaultMessage="Cancel" />
                        </Button>
                        {
                            loading ? (<CircularProgress />) : (
                                <Button onClick={submit} data-testid={"DeleteButtonConfirmTestId"}>
                                    <FormattedMessage id="none" defaultMessage="Delete" />
                                </Button>
                            )
                        }
                    </DialogActions>
                </EbsDialog>
            </div>
        );
    }
);
// Type and required properties
DeleteDocument.propTypes = {};
// Default properties


export default DeleteDocument;
