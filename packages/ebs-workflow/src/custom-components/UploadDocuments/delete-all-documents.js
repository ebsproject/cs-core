import { Core } from "@ebs/styleguide"
const { Button, Dialog, DialogActions, DialogContent, Typography, LinearProgress } = Core;
import { deleteFile, deleteSingleDocument } from "./functions";
import { useAlertContext } from "components/alerts/alert-context";
import { Fragment, useState } from "react";
const DeleteAllDocuments = ({ selection, refresh }) => {

    const [open, setOpen] = useState(false);
    const handleClose = () => setOpen(false)
    const { setAlerts } = useAlertContext();
    const [loading, setLoading] = useState(false)

    const deleteDocuments = async () => {
        setLoading(true)
        for (let i = 0; i < selection.length; i++) {
            const item = selection[i];
            try {

                await deleteSingleDocument(item);
                await deleteFile(item.id);
                setAlerts((prev) => [...prev, { id: 'file-delete-message-success-' + item.file.id, message: `File ${item.file.name} deleted successfully`, severity: 'success' }]);
            } catch (error) {
                const message = error.response.data.metadata.status[0].message;
                setAlerts((prev) => [...prev, { id: 'file-delete-message-error-'+ item.file.id, message: `Error trying to delete file '${item.file.name}. Reason:'${message}`, severity: 'error' }]);
            }
        }
        setLoading(false);
        refresh();
    }

    return (
        <Fragment>
            <Dialog open={open} onClose={handleClose} maxWidth={"md"}>
                <DialogContent>
                    <Typography variant={"h5"}>{"The following documents will be deleted permanently: "}</Typography>
                    {selection && selection.map(item => (
                        <Typography key={item.file.name} color={"error"} variant={"h6"}>{item.file.name}</Typography>
                    ))}
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} data-testid={"CancelButtonDeleteTestId"}>
                        Cancel
                    </Button>
                    <Button disabled={loading} onClick={deleteDocuments} data-testid={"DeletePermanentlyTestId"}>
                        Delete Permanently
                    </Button>
                </DialogActions>
                {loading && <LinearProgress /> }
            </Dialog>
            <Button data-testid={"BulkDeleteButtonTestId"} onClick={() => setOpen(true)}>
                {'Bulk delete'}
            </Button>
        </Fragment>
    )
}
export default DeleteAllDocuments;