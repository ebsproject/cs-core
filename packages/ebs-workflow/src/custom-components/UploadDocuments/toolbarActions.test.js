import { ToolbarActions } from "./toolbarActions";
import { cleanup, render } from "@testing-library/react";
import { useServiceContext } from "custom-components/context/service-context";
jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));


jest.mock("./uploadFile", () => () => <div data-testid="upload-file-test">Upload File Component</div>);
jest.mock("./document-generator-button", () => () => <div data-testid="document-generator-button-test">Document Button Component</div>);
jest.mock("./delete-all-documents", () => () => <div data-testid="delete-all-documents-test">Delete All Documents Component</div>);




describe("Toolbar Action component", () =>{
    const rowDataMock ={
        id: 1,
        statusid:500,
    }
   const customComponentMock = {
    define:{rules:{legalDocuments:{security:{allowedStatus:[{statusId:500}, {statusId:501}, {statusId:502}]}}}}
   }
    beforeEach(() => {
        useServiceContext.mockReturnValue({
            customComponent: customComponentMock,
            rowData: rowDataMock,
        });
     
    });

    test("Load all buttons in tool bar action", ()=>{
        const selectionMock =[{id:1}];
        const refreshMock = jest.fn();
        const {getByTestId} = render(<ToolbarActions selection={selectionMock} refresh={refreshMock} />)
        expect(getByTestId("upload-file-test")).toBeInTheDocument();
        expect(getByTestId("document-generator-button-test")).toBeInTheDocument();

    })

})