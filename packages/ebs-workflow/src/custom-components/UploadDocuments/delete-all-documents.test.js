import DeleteAllDocuments from "./delete-all-documents";
import React from "react";
import { render, cleanup, screen, fireEvent, waitFor } from "@testing-library/react";
import { useAlertContext } from "components/alerts/alert-context.js";
import { deleteSingleDocument, deleteFile } from "./functions";

jest.mock("components/alerts/alert-context", () => ({
    useAlertContext: jest.fn(),
}));

jest.mock("./functions", () => ({
    deleteSingleDocument: jest.fn(),
    deleteFile: jest.fn()
}));


jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

const setAlertMock = jest.fn()
const mockSelection = [
    { file: { id: 1, name: "File 1" } },
    { file: { id: 2, name: "File 2" } }
];
const refreshMock = jest.fn();

describe("Delete all documents component", () => {

    beforeEach(() => {
        jest.clearAllMocks();
        useAlertContext.mockReturnValue({
            setAlerts: setAlertMock,
        });
    });
    const renderComponent = () => render(<DeleteAllDocuments selection={mockSelection} refresh={refreshMock} />);

    test("Load delete button", () => {
        const { getByTestId } = renderComponent();
        expect(getByTestId("BulkDeleteButtonTestId")).toBeInTheDocument();

    })
    test('opens dialog and renders document names', () => {
        renderComponent();
        fireEvent.click(screen.getByTestId('BulkDeleteButtonTestId'));
        expect(screen.getByText(/The following documents will be deleted permanently:/)).toBeInTheDocument();
        expect(screen.getByText('File 1')).toBeInTheDocument();
        expect(screen.getByText('File 2')).toBeInTheDocument();
    });
    test('clicks cancel button to close dialog', async() => {
        renderComponent();
        fireEvent.click(screen.getByTestId('BulkDeleteButtonTestId'));
        const buttonCancel =  screen.getByText(/cancel/i);
        fireEvent.click(buttonCancel);
        await waitFor(() => {
        expect(screen.queryByText(/The following documents will be deleted permanently:/)).not.toBeInTheDocument();
        })
     });
     test("displays success alert for each file deleted successfully", async () => {
        deleteSingleDocument.mockResolvedValue();
        deleteFile.mockResolvedValue();
        renderComponent();
        fireEvent.click(screen.getByTestId("BulkDeleteButtonTestId")); 
        fireEvent.click(screen.getByTestId("DeletePermanentlyTestId"));
        await waitFor(() => {
            expect(deleteSingleDocument).toHaveBeenCalledTimes(mockSelection.length);
            expect(deleteFile).toHaveBeenCalledTimes(mockSelection.length);
            expect(setAlertMock).toHaveBeenCalledTimes(mockSelection.length);
        });
        mockSelection.forEach((item) => {
            const setAlertCall = setAlertMock.mock.calls.find((call) => {
                const result = call[0]([]);
                return result.some(alert => alert.id === `file-delete-message-success-${item.file.id}`);
            });
            const alerts = setAlertCall[0]([]);
            expect(alerts).toEqual(
                expect.arrayContaining([
                    expect.objectContaining({
                        id: `file-delete-message-success-${item.file.id}`,
                        message: `File ${item.file.name} deleted successfully`,
                        severity: 'success'
                    })
                ])
            );
        });
    
        expect(refreshMock).toHaveBeenCalled();
    });
    test("displays error alert if file deletion fails", async () => {
        const errorMessage = "Deletion failed";
        deleteSingleDocument.mockRejectedValue({
            response: { data: { metadata: { status: [{ message: errorMessage }] } } }
        });
        renderComponent();
        fireEvent.click(screen.getByTestId("BulkDeleteButtonTestId"));
        fireEvent.click(screen.getByTestId("DeletePermanentlyTestId")); 

        await waitFor(() => {
            expect(deleteSingleDocument).toHaveBeenCalledTimes(mockSelection.length); 
            expect(setAlertMock).toHaveBeenCalledWith(expect.any(Function));
        });
        const setAlertsCall = setAlertMock.mock.calls[0][0];
        const alerts = setAlertsCall([]);
        expect(alerts).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    id: `file-delete-message-error-${mockSelection[0].file.id}`,
                    message: `Error trying to delete file '${mockSelection[0].file.name}. Reason:'${errorMessage}`,
                    severity: 'error'
                })
            ])
        );
        expect(refreshMock).toHaveBeenCalled();
    });

})