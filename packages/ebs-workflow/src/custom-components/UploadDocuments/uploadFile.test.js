import { render } from "@testing-library/react";
import UploadFile from "./uploadFile";
import { useFormContext, Controller, useForm } from "react-hook-form";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context.js";
import { useSelector } from "react-redux";

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useSelector: jest.fn(),
  }));

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));

jest.mock("components/alerts/alert-context", () => ({
    useAlertContext: jest.fn(),
}));

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    useForm:jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));


describe("Upload file component", () => {

    const serviceMock = { entity: "test-service" };
    const rowDataMock = { id: 1, statusCode:"Test", byOccurrence:true };
    const setAlertMock = jest.fn()
    beforeEach(() => {
        useForm.mockReturnValue({
            handleSubmit:jest.fn(),
            reset: jest.fn(),
            setValue: jest.fn(),
            control: jest.fn(),
            formState: { errors:jest.fn() },
        })
        useServiceContext.mockReturnValue({
            service: serviceMock,
            rowData: rowDataMock,
        });
        useAlertContext.mockReturnValue({
            setAlert: setAlertMock,
        });
        useSelector.mockImplementation((selectorFn) => {
            return selectorFn({
                wf_workflow: {
                    workflowValues: {id:0}, 
                  },
            });
          });
    });

    test("Upload file button loaded", () => {
        const { getByText } = render(<UploadFile />);
        expect(getByText(/upload/i)).toBeInTheDocument();
    });

});
