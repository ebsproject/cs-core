import LegalDocumentButton from "./DocumentGeneratorButton.js";
import React from "react";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import { Provider } from "react-redux";
import store from "store";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context.js";

// Mock service context
jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));
// Mock service context
jest.mock("components/alerts/alert-context", () => ({
  useAlertContext: jest.fn(),
}));


jest.mock('react-intl', () => ({
  FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));


describe("Legal Document Component", () =>{
  const refreshMock = jest.fn();
  const serviceMock = { entity: "test-service" };
  const rowDataMock = { id: 1, statusCode:"Test", byOccurrence:true };
  const selectionMock = [{ id: 1 }, { id: 2 }];
  const setAlertMock = jest.fn()

  beforeEach(() => {
    useServiceContext.mockReturnValue({
      service: serviceMock,
      rowData: rowDataMock,
    });
    useAlertContext.mockReturnValue({
      setAlert: setAlertMock,
    });
  });
  test("Legal Document Button is in the DOM", () => {
 const {getByRole, getByText} = render(
      <Provider store={store}>
        <LegalDocumentButton></LegalDocumentButton>
      </Provider>
    );
    expect(getByText("Legal Documents")).toBeInTheDocument();
  });
  // test("Click Legal Document Button to open the modal", () => {
  //   const setOpenMock = jest.fn();
  //   const handleClose = jest.fn();
  //   const refreshMock = jest.fn();
  //       const {getByRole, getByText} = render(
  //        <Provider store={store}>
  //          <LegalDocumentButton refresh ={refreshMock}></LegalDocumentButton>
  //        </Provider>
  //      );
  //      const button = getByText("Legal Documents");
  //      fireEvent.click(button);
  //      expect(setOpenMock).toHaveBeenCalled()
  //    });
})

