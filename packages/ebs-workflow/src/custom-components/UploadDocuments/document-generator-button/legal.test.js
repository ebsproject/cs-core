import Legal from "./legal";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context.js";

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));
jest.mock("components/alerts/alert-context", () => ({
    useAlertContext: jest.fn(),
}));

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));


describe("Legal Modal component", () => {
    const serviceMock = { entity: "test-service" };
    const rowDataMock = { id: 1, statusCode: "Test", byOccurrence: true };
    const setAlertMock = jest.fn();

    beforeEach(() => {
        useServiceContext.mockReturnValue({
            service: serviceMock,
            rowData: rowDataMock,
        });
        useAlertContext.mockReturnValue({
            setAlert: setAlertMock,
        });
    });
    test("Loading the Modal", () => {
        const handleCloseMock = jest.fn();
        const refreshMock = jest.fn();
        const { getByTestId } = render(
            <Legal refresh={refreshMock} handleClose={handleCloseMock} />
        )
        expect(getByTestId("LegalFormTestId")).toBeInTheDocument();
    })
})