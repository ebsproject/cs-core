import React, { useState, useEffect } from "react";
import { Core, Styles } from "@ebs/styleguide";
import { restClient } from "utils/axios/axios";
import { FormattedMessage } from "react-intl";
import { useForm, Controller } from "react-hook-form";
import { getSmtaCode } from "./utils";
import { useServiceContext } from "custom-components/context/service-context";
import { getItemMTAStatus } from "../functions";
import { fileClient, po_client as poClient } from "utils/axios/axios";
import { createFileData } from "../functions";
import { useAlertContext } from "components/alerts/alert-context";
import { setReceivedDateToProcessing } from "validations";
const {
  Grid,
  Button,
  TextField,
  Typography,
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
  Autocomplete,
  LinearProgress,
  Checkbox
} = Core;

const Legal = React.forwardRef(({ refresh, handleClose }, ref) => {
  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    reset,
    setValue,
  } = useForm({
    defaultValues: {
      mtaDocToGenerate: null,
      smtaIdMlsId: "",
      smtaIdPud1: "",
      smtaIdPud2: "",
    },
  });

  const [smtaTypeRadio, setSmtaTypeRadio] = React.useState(null);
  const [smtaOptionValue, setSmtaOptionValue] = React.useState(null);
  const [docTypeOptionValue, setDocTypeOptionValue] = React.useState(null);
  const [smtaValue, setSmtaValue] = React.useState("");
  const [smtaIdMls, setSmtaIdMls] = React.useState("");
  const [smtaIdPud1, setSmtaIdPud1] = React.useState("");
  const [smtaIdPud2, setSmtaIdPud2] = React.useState("");
  const [otherMta, setOtherMta] = React.useState("");
  const [smtaIdMlsCurrent, setSmtaIdMlsCurrent] = React.useState("");
  const [smtaIdPud1Current, setSmtaIdPud1Current] = React.useState("");
  const [smtaIdPud2Current, setSmtaIdPud2Current] = React.useState("");
  const [otherMtaCurrent, setOtherMtaCurrent] = React.useState("");
  const [smtaIdMlsExist, setSmtaIdMlsExist] = React.useState(false);
  const [smtaIdPud1Exist, setSmtaIdPud1Exist] = React.useState(false);
  const [smtaIdPud2Exist, setSmtaIdPud2Exist] = React.useState(false);
  const [selectedSmtaId, setSelectedSmtaId] = React.useState("");
  const [selectedDocStatus, setSelectedDocStatus] = React.useState();
  const [pud1OptionHidden, setPud1OptionHidden] = React.useState(true);
  const [isOtherMtaSelected, setIsOtherMtaSelected] = useState(false);
  const [mtaStatuses, setMTAStatuses] = useState(null);
  const [valuesArray, setValuesArray] = useState(null);
  const [mlsData, setMlsData] = useState('');
  const { rowData, customComponent } = useServiceContext();
  const [documentList, setDocumentList] = useState([]);
  const [uploading, setUploading] = useState(false);
  const { setAlerts } = useAlertContext();
  const [value, setValues] = useState([]);
  const [allDocuments, setAllDocuments] = useState([]);
  useEffect(() => {
    if (customComponent) {
      try {
        let legalDocuments = customComponent.define.rules.legalDocuments.documents;
        setAllDocuments(legalDocuments.filter(item => item.active));
      } catch (error) {
        console.log(error)
      }
    }

  }, []);
  useEffect(() => {
    const fetchData = async () => {
      if (rowData.byOccurrence) {
        setMTAStatuses([])
      } else {
        const filters = [
          { col: "service.id", mod: "EQ", val: Number(rowData.id || 0) }
        ]
        const { data, mlsAncestors, valuesArray } = await getItemMTAStatus(filters);
        setMlsData(mlsAncestors && mlsAncestors || "");
        setMTAStatuses(data && data.map(item => { return item.toLowerCase() }) || []);
        setValuesArray(valuesArray)
      }
    }
    fetchData()
  }, [rowData]);

  useEffect(() => {
    if (valuesArray && valuesArray.length > 0) {
      let mls = valuesArray.find(item => item.mtaStatus.toLowerCase() === "mls");
      let fao = valuesArray.find(item => item.mtaStatus.toLowerCase() === "fao");
      let rel1 = valuesArray.find(item => item.mtaStatus.toLowerCase() === "rel1");
      let smta = valuesArray.find(item => item.mtaStatus.toLowerCase() === "smta");
      let pud = valuesArray.find(item => item.mtaStatus.toLowerCase() === "pud");
      let pud1 = valuesArray.find(item => item.mtaStatus.toLowerCase() === "pud1");
      let pud2 = valuesArray.find(item => item.mtaStatus.toLowerCase() === "pud2");
      if (fao || mls || rel1 || smta) {
        setSmtaIdMlsExist(true);
        if (fao) {
          setValue("smtaIdMlsId", fao.smtaId);
          setSmtaIdMlsCurrent(fao.smtaId);
          setSmtaIdMls(fao.smtaId)
        }
        else if (mls) {
          setValue("smtaIdMlsId", mls.smtaId);
          setSmtaIdMlsCurrent(mls.smtaId);
          setSmtaIdMls(mls.smtaId)
        }
        else if (rel1) {
          setValue("smtaIdMlsId", rel1.smtaId);
          setSmtaIdMlsCurrent(rel1.smtaId);
          setSmtaIdMls(rel1.smtaId)
        }
        else if (smta) {
          setValue("smtaIdMlsId", smta.smtaId);
          setSmtaIdMlsCurrent(smta.smtaId);
          setSmtaIdMls(smta.smtaId)
        }
      } if (pud || pud1) {
        setSmtaIdPud1Exist(true);
        if (pud) {
          setValue("smtaIdPud1", pud.smtaId);
          setSmtaIdPud1Current(pud.smtaId);
          setSmtaIdPud1(pud.smtaId)
        } else if (pud1) {
          setValue("smtaIdPud1", pud1.smtaId);
          setSmtaIdPud1Current(pud1.smtaId);
          setSmtaIdPud1(pud1.smtaId)
        }

      } if (pud2) {
        setSmtaIdPud2Exist(true);
        setValue("smtaIdPud2", pud2.smtaId);
        setSmtaIdPud2Current(pud2.smtaId);
        setSmtaIdPud2(pud2.smtaId)
      }
    }
  }, [mtaStatuses]);

  useEffect(() => {
    switch (smtaValue) {
      case 'mls': setDocumentList(allDocuments.filter(item => item.type === 'mls'));
        break;
      case 'pud1': setDocumentList(allDocuments.filter(item => item.type === 'pud1'));
        break;
      case 'pud2': setDocumentList(allDocuments.filter(item => item.type === 'pud2'));
        break;
      default: setDocumentList([])
        break;
    }

  }, [smtaValue])

  function handleClick(event) {
    if (event.target.value === smtaTypeRadio) {
      setSmtaTypeRadio("");
    } else {
      setSmtaTypeRadio(event.target.value);
      setSmtaValue("");
    }
    if (event.target.value == "otherMta") {
      setIsOtherMtaSelected(true);
    } else {
      setIsOtherMtaSelected(false);
    }
  }

  function handleSmtaMlsChanges(e) {
    setValue("smtaIdMls", e.target.value);
    setSmtaIdMls(e.target.value);
    setSmtaIdMlsCurrent(e.target.value);
  }

  function handleSmtaPud1Changes(e) {
    setValue("smtaIdPud1", e.target.value);
    setSmtaIdPud1Current(e.target.value);
    setSmtaIdPud1(e.target.value);
  }

  function handleSmtaPud2Changes(e) {
    setValue("smtaIdPud2", e.target.value);
    setSmtaIdPud2Current(e.target.value);
    setSmtaIdPud2(e.target.value);
  }

  function handleOtherMtaChanges(e) {
    setValue("otherMta", e.target.value);
    setOtherMta(e.target.value);
  }

  function handleChangeDocType(e, options) {
    if (!options) {
      setDocTypeOptionValue(null);
      setValues([])
      return;
    }
    e.preventDefault();
    setValues(options)
    setDocTypeOptionValue(options);
  }

  function handleChangeRadioGroupSMTA(newValue) {
    setSmtaOptionValue(newValue.target.value);

    if (newValue.target.value == "pud1") {
      setPud1OptionHidden(false);
    } else {
      setPud1OptionHidden(true);
    }
    setSelectedDocStatus(newValue.target.value);
  }

  async function handleClickSmtaRadio(event) {
    setValues([])
    setDocTypeOptionValue(null);
    setDocumentList([]);
    setValue('documentType', null)
    if (event.target.value === smtaValue) {
      setSmtaValue("");
    } else {
      setSmtaValue(event.target.value);
      setSmtaTypeRadio("");
    }

    if (event.target.value == "pud1") {
      if (smtaIdPud1.length === 0) {
        const code = await getSmtaCode();
        await restClient.get(`/assign-smta/${code}/pud/${rowData.id}`);
        setSmtaIdPud1Current(code);
        setSmtaIdPud1(code);
      }
      setSelectedSmtaId(smtaIdPud1Current);
      setDocumentList(allDocuments.filter(item => item.type === 'pud1'))
    } else if (event.target.value == "mls") {
      if (smtaIdMls.length === 0) {
        const code = await getSmtaCode();
        setSmtaIdMlsCurrent(code);
        await restClient.get(`/assign-smta/${code}/mls/${rowData.id}`);
        setSmtaIdMls(code)
        setSmtaIdMlsCurrent(code)
      }
      setDocumentList(allDocuments.filter(item => item.type === 'mls'))
      setSelectedSmtaId(smtaIdMlsCurrent);
    } else if (event.target.value == "pud2") {
      if (smtaIdPud2.length === 0) {
        const code = await getSmtaCode();
        setSmtaIdPud2Current(code)
        await restClient.get(`/assign-smta/${code}/pud2/${rowData.id}`)
        setSelectedSmtaId(code);
        setSmtaIdPud2(code);
      }

      setDocumentList(allDocuments.filter(item => item.type === 'pud2'))
    }
  }

  const renderOption = (props, option, { selected }) => {
    const { key, ...restProps } = props;
    return (
      <li key={key} {...restProps}>
        <Checkbox checked={selected} />
        {option.label}
      </li>
    );
  };
  const createAndUploadLegalDocuments = async () => {
    if (!docTypeOptionValue) return;
    setUploading(true)
    const docList = docTypeOptionValue;
    for (let i = 0; i < docList.length; i++) {
      const doc = docList[i];
      let documentName = "";
      switch (smtaValue) {
        case 'mls':
          if (smtaIdMlsCurrent.length > 0)
            documentName = `${smtaIdMlsCurrent}-${doc.value}`;
          break;
        case 'pud1':
          if (smtaIdPud1Current.length > 0)
            documentName = `${smtaIdPud1Current}-${doc.value}`;
          break;
        case 'pud2':
          if (smtaIdPud2Current.length > 0)
            documentName = `${smtaIdPud2Current}-${doc.value}`;
          break;

        default: documentName = "Other";
          break;
      }

      const { status: poStatus, data: poData } =
        await poClient
          .get(`/api/report/export?name=${doc.template}&format=pdf&parameter[includeUniqueAncestors]=true&parameter[includeItems]=true&parameter[id]=${rowData.id}`,
            {
              responseType: "blob",
            })
      if (poStatus !== 200) {
        uploading(false);
        setAlerts((prev) => [...prev, { id: 'file-upload-message-error', message: `There was an error creating Document ${documentName} `, severity: 'error' }]);
        return;
      }

      const file = new File(
        [poData],
        `${documentName}.pdf`,
        {
          type: "application/pdf",
        }
      );
      const formData = new FormData();
      formData.append("file", file);
      formData.append("description", `LEGAL DOCUMENT- ${rowData.requestCode}`);
      try {
        const fileResponse = await fileClient.post(`${1}/objects`, formData, {
          headers: { "Content-Type": "multipart/form-data" },
        });
        let input = {
          id: 0,
          serviceId: rowData.id,
          fileId: fileResponse.data.result.data[0].key,
          remarks: "",
          tenantId: 1
        }
        const rep = await createFileData(input);
        if (rep.errors) {
          return;
        }
        await setReceivedDateToProcessing({id: rowData.id,field:"documentGeneratedDate"});
        setAlerts((prev) => [...prev, { id: `file-upload-message-success-${documentName}`, message: `Document ${documentName} created successfully `, severity: 'success' }]);
      } catch (error) {
        const { response } = error;
        let message = response.data.metadata.status[0].message;
        let _message = message.replace(" for tenant 1", ".")
        if (_message.includes("already exists")) {
          _message = `A file with the same name "${documentName}" has been uploaded earlier. Please update the file name to make it unique and try again`;
          setAlerts((prev) => [...prev, { id: `error-uploading-message-${documentName}`, message: _message, severity: 'error' }]);
        }
        else
          setAlerts((prev) => [...prev, { id: `file-upload-message-error-${documentName}`, message: `Error trying to created the document ${documentName}`, severity: 'error' }]);
      }
    }


    handleClose();
    refresh();
    setUploading(false)
  };
  const onSubmit = () => { }

  return (
    <div >
      <form
        onSubmit={handleSubmit(onSubmit)}
        data-testid="LegalFormTestId"
      >
        <FormControl component="fieldset">
          <RadioGroup
            aria-label="mtaDocToGenerate"
            name="mtaDocToGenerate"
            value={smtaValue}
            onChange={handleChangeRadioGroupSMTA}
          >
            <Grid container spacing={2} direction="row">
              {smtaIdMlsExist && (
                <>
                  <Grid item xs={2}>
                    <FormControlLabel
                      value="mls"
                      label={<Typography variant="h6" color="primary">
                        <FormattedMessage id="none" defaultMessage="MLS" />
                      </Typography>}
                      control={<Radio onClick={handleClickSmtaRadio} />}
                    />
                  </Grid>
                  <Grid item xs={10}>
                    <Controller
                      name="smtaIdMlsId"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          fullWidth
                          {...field}
                          variant="outlined"
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="SMTA ID"
                            />
                          }
                          value={smtaIdMls}
                          data-testid={"smtaIdMlsId"}
                          onChange={handleSmtaMlsChanges}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      )}
                      defaultValue={smtaIdMlsCurrent}
                    />
                  </Grid>
                </>
              )}
              {smtaIdPud1Exist && (
                <>
                  <Grid item xs={2}>
                    <FormControlLabel
                      value="pud1"
                      label={<Typography variant="h6" color="primary">
                        <FormattedMessage
                          id="none"
                          defaultMessage="PUD and PUD1"
                        />
                      </Typography>}
                      control={<Radio onClick={handleClickSmtaRadio} />}
                    />
                  </Grid>
                  <Grid item xs={10}>
                    <Controller
                      name="smtaIdPud1"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          fullWidth
                          {...field}
                          variant="outlined"
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="SMTA ID"
                            />
                          }
                          value={smtaIdPud1}
                          data-testid={"smtaIdPud1"}
                          onChange={handleSmtaPud1Changes}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      )}
                      defaultValue={smtaIdPud1Current}
                    />
                  </Grid>
                </>
              )}
              {smtaIdPud2Exist && (
                <>
                  <Grid item xs={2}>
                    <FormControlLabel
                      label={<Typography variant="h6" color="primary">
                        <FormattedMessage id="none" defaultMessage="PUD2" />
                      </Typography>}
                      value="pud2"
                      control={<Radio onClick={handleClickSmtaRadio} />}
                    />
                  </Grid>
                  <Grid item xs={10}>
                    <Controller
                      name="smtaIdPud2"
                      control={control}
                      render={({ field }) => (
                        <TextField
                          fullWidth
                          {...field}
                          variant="outlined"
                          label={
                            <FormattedMessage
                              id="none"
                              defaultMessage="SMTA ID"
                            />
                          }
                          value={smtaIdPud2}
                          data-testid={"smtaIdPud2"}
                          onChange={handleSmtaPud2Changes}
                          InputLabelProps={{
                            shrink: true,
                          }}
                        />
                      )}
                      defaultValue={smtaIdPud2Current}
                    />
                  </Grid>
                </>
              )}
            </Grid>
          </RadioGroup>
        </FormControl>
        <Grid container spacing={2} direction="row">
          <Grid item xs={12} >
            Document Type
          </Grid>
          <Grid item xs={12}>
            <Controller
              name="documentType"
              control={control}
              render={({ field }) => (
                <Autocomplete
                  multiple
                  value={value}
                  limitTags={6}
                  onChange={handleChangeDocType}
                  options={documentList}
                  disableCloseOnSelect
                  getOptionLabel={(item) => (item ? item.label : "")}
                  isOptionEqualToValue={(option, value) =>
                    value === undefined ||
                    value === "" ||
                    option.value === value.value
                  }
                  renderOption={renderOption}
                  renderInput={(params) => (
                    <TextField
                      {...field}
                      {...params}
                      label="Select a document"
                      variant="outlined"
                      size="normal"
                      fullWidth
                      error={Boolean(errors["documentType"])}
                      helperText={errors["documentType"]?.message}
                    />
                  )}
                />
              )}
            />
          </Grid>
          {/* <Grid item xs={12}>
            {documentName.length > 0 &&
              <Typography variant="h6" color="error">
                {`Document to be generated: ${documentName}`}
              </Typography>
            }
          </Grid> */}
          <Grid item xs={12}>
            {uploading && <LinearProgress />}
            <Button
              fullWidth
              variant="contained"
              color="primary"
              onClick={createAndUploadLegalDocuments}
              disabled={smtaValue === "" || !smtaValue || smtaValue.length === 0 || !docTypeOptionValue || uploading}
            >
              Generate
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={3} direction="row">
          <Grid item xs={6} justifyContent="flex-end">
            <FormControl component="fieldset">
              <RadioGroup
                aria-label="otherDoc"
                name="otherDocType"
                value={smtaTypeRadio}
              >
                <FormControlLabel
                  value="noMta"
                  control={<Radio onClick={handleClick} />}
                  label="No MTA"
                />
                <div>
                  <FormControlLabel
                    value="otherMta"
                    control={<Radio onClick={handleClick} />}
                    label="Other MTA"
                  />
                </div>
              </RadioGroup>
            </FormControl>
          </Grid>
          <Grid item xs={6}></Grid>
        </Grid>
        <Grid container>
          {isOtherMtaSelected && (
            <Controller
              name="otherMta"
              control={control}
              render={({ field }) => (
                <TextField
                  {...field}
                  fullWidth="true"
                  variant="outlined"
                  label={
                    <FormattedMessage
                      id="none"
                      defaultMessage="Other MTA"
                    />
                  }
                  data-testid={"otherMta"}
                  onChange={handleOtherMtaChanges}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              )}
              defaultValue={otherMtaCurrent}
            />
          )}
        </Grid>
      </form>
    </div>
  );
});
// Type and required properties
Legal.propTypes = {};

export default Legal;
