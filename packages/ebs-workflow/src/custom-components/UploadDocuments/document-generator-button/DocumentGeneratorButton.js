import React, { Fragment, useState } from "react";
import { Core } from "@ebs/styleguide";
import { FormattedMessage } from "react-intl";
import { RBAC } from "@ebs/layout";
import Legal from "./legal";

const { Button, Typography, Tooltip, Dialog, DialogContent, DialogActions, DialogTitle } = Core;

const DocumentGeneratorButton = React.forwardRef(({ refresh }, ref) => {

  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(false);

  return (
    <Fragment>
      <Dialog open={open} handleClose={handleClose} maxWidth={"md"} fullWidth>
        <DialogTitle>
        <Typography variant={"h6"} color={"primary"}>
              <FormattedMessage id="none" defaultMessage="Please select the SMTA-ID and the document type." />
            </Typography>
        </DialogTitle>
        <DialogContent>
          <Legal refresh= {refresh} handleClose={handleClose} />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)}>
            Cancel
          </Button>
        </DialogActions>
      </Dialog>
      <RBAC allowedAction="Service Processor" >
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Generate and attach documents" />
            </Typography>
          }
        >
          <Button
            onClick={() => setOpen(true)}
            color="primary"
          >
            Legal Documents
          </Button>
        </Tooltip>
      </RBAC>
    </Fragment>

  );
});
// Type and required properties
DocumentGeneratorButton.propTypes = {};
// Default properties


export default DocumentGeneratorButton;
