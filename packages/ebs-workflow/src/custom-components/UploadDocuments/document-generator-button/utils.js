import { restClient } from "utils/axios/axios"
export const mlsDocumentList =[
    {label:"Shrink Wrap", value:"SMTA_MLS_Shrink-Wrap", template:"sys_legal_smta_mls"},
    {label:"Signed", value:"SMTA_MLS_Signed", template:"sys_standard_material_transfer_agreement_signed_rel1"},
]

export const pud1DocumentList =[
    {label:"Shrink Wrap", value:"SMTA_PUD_Shrink-Wrap", template:"sys_legal_smta_pud"},
    {label:"Signed", value:"SMTA_PUD_Signed", template:"sys_standard_material_transfer_agreement_signed_pud1"},
    {label:"HRDC_OMTA", value:"HRDC_OMTA", template:"sys_hrdc_omta"},
    {label:"JIRCAS_OMTA", value:"JISRCAS_OMTA", template:"sys_jircas_omta"},
    {label:"NARVI_OMTA", value:"NARVI_OMTA", template:"sys_narvi_omta"},
    {label:"VRAP_OMTA", value:"VRAP_OMTA", template:"sys_vrap_omta"},
    {label:"IRRI_INGER_OMTA", value:"IRRI_INGER_OMTA", template:"sys_irri_inger_omta"},
    {label:"ASEAN RiceNet-OMTA", value:"ASEAN_RICENET_OMTA", template:"sys_asean_omta"},
    {label:"IRRI_OMTA", value:"IRRI_OMTA", template:"sys_irri_omta"},
]

export const pud2DocumentList =[
    {label:"Shrink Wrap", value:"SMTA_PUD2_Shrink-Wrap", template:"sys_legal_smta_pud2"},
    {label:"Signed", value:"SMTA_PUD2_Signed", template:"sys_standard_material_transfer_agreement_signed_pud2"},
    {label:"Biological MTA", value:"BIOLOGICAL_MTA_PUD2_Signed", template:"sys_biological_mta"},
    {label:"Global MTA", value:"GLOBAL_MTA_PUD2_Signed", template:"sys_global_mta_pud2"},
]
export async function getSmtaCode(){
   const segment = "cs-smta"
    try {
        const { data } = await restClient.get(`sequence`);
        const sequence = data.content.find(item => item.name === segment);
        if(!sequence) return `Error! sequence rule not found: ${segment} `
        const { data: next } = await restClient.post(`sequence/next/${sequence['id']}`, {"":""});
        return next;
    } catch (error) {
        return error.toString()
    }
}