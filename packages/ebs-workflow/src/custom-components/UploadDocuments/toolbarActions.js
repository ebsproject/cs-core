import { Core } from "@ebs/styleguide";
const { Grid } = Core;
import UploadFile from "./uploadFile";
import DocumentGeneratorButton from "./document-generator-button";
import DeleteAllDocuments from "./delete-all-documents";
import { RBAC } from "@ebs/layout";
import { useServiceContext } from "custom-components/context/service-context";
import { useEffect, useState } from "react";


export const ToolbarActions = (selection, refresh) => {
  const { rowData, customComponent } = useServiceContext();
  const [show, setShow] = useState(false);

  useEffect(() => {
    if (rowData && customComponent) {
      const currentStatus = rowData['status'];
      let allowed = false;
      const define = customComponent.define;
      const rules = define["rules"]
      if (rules) {
        const legalDocuments = rules["legalDocuments"];
        if (legalDocuments) {
          const security = legalDocuments["security"];
          if (security) {
            const allowedStatus = security["allowedStatus"];
            if (allowedStatus && allowedStatus.length) {
              allowedStatus.forEach(item => {
                if (item.status.toLowerCase() === currentStatus.toLowerCase())
                  allowed = true;
              })
              setShow(allowed);
            }
          }
        }
      }
    }
  }, [customComponent]);
  
  return (

    <Grid container spacing={2}>
      {/* <Grid item>
        <LegalDocumentButton refresh={refresh} />
      </Grid> */ }
      {show && <RBAC allowedAction="Service Processor">
        <Grid item>
          <DocumentGeneratorButton refresh={refresh} />
        </Grid>
      </RBAC>}

      <Grid item>
        <UploadFile refresh={refresh} />
      </Grid>
      {
        selection.length > 1 && (
          <Grid item>
            <DeleteAllDocuments selection={selection} refresh={refresh} />
          </Grid>
        )
      }
    </Grid>
  );
};