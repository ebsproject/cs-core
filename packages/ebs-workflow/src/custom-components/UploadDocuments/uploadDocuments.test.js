import { UploadDocuments } from "./uploadDocuments";
import { render } from "@testing-library/react";
import { useServiceContext } from "custom-components/context/service-context";
import { ToolbarActions } from "./toolbarActions";
import { RowActions } from "./rowActions";
jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));

jest.mock("./toolbarActions", () => () => <div data-testid="toolbar-action">ToolBar Action Component</div>);
jest.mock("./rowActions", () => () => <div data-testid="row-action">Row Action Component</div>);

describe("Upload Documents Grid", ()=>{
    const setCustomComponentMock = jest.fn()
    const rowDataMock = { id: 1, statusCode:"Test", byOccurrence:true };
    const modeMock = "view"
    beforeEach(() => {
        useServiceContext.mockReturnValue({
            setCustomComponent: setCustomComponentMock,
            rowData: rowDataMock,
            mode:modeMock
        });
  
    });
    test("Upload Documents loaded", ()=>{

        const {getByText} = render(<UploadDocuments />)
        expect(getByText(/documents/i)).toBeInTheDocument();
    });
    test("Upload Documents with node props", ()=>{
        const {getByText} = render(<UploadDocuments node={{id:0, name:"Test Node"}} />)
        expect(getByText(/documents/i)).toBeInTheDocument();
    })



})