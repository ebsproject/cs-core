import { RowActions } from "./rowActions";
import { render } from "@testing-library/react";


jest.mock("./downloadDocument", () => () => <div data-testid="download-document-test">Download Document Component</div>);
jest.mock("./deleteDocument", () => () => <div data-testid="delete-document-button-test">Delete Button Component</div>);

describe("Row Action Component", () =>{
    const rowDataMock ={
        id: 1,
        statusid:500,
    }
   const  refreshMock= jest.fn();

    test("Row Action button loaded", ()=>{
        const {getByTestId} = render(<RowActions rowData={rowDataMock} refresh={refreshMock}/>)
        expect (getByTestId("RowActionDocumentsTestId")). toBeInTheDocument();
        expect (getByTestId("download-document-test")). toBeInTheDocument();
        expect (getByTestId("delete-document-button-test")). toBeInTheDocument();

    }) 
})