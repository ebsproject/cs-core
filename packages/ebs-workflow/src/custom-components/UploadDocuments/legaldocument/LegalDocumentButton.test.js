import React from "react";
import { render, screen, fireEvent, waitFor, act } from "@testing-library/react";
import LegalDocumentButton from "./LegalDocumentButton";
import { createFileData, getItemMTAStatus } from "../functions";
import { useServiceContext } from "custom-components/context/service-context";
import { po_client as poClient } from "utils/axios";
import { fileClient } from "utils/axios";
jest.mock("")
jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));
jest.mock("utils/axios", () => ({
  poClient: { get: jest.fn() },
  po_client: { get: jest.fn() },
  fileClient: { post: jest.fn() },
}))

import { useSelector, useDispatch } from "react-redux";
jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("store/modules/message");
jest.mock("custom-components/context/service-context");
jest.mock("../functions", () => ({
  getItemMTAStatus: jest.fn(),
  createFileData: jest.fn()
}));
jest.mock('react-intl', () => ({
  FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Legal Document Button Component", () => {
  const mockDispatch = jest.fn();
  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useSelector.mockImplementation((selectorFn) => {
      return selectorFn({
        wf_workflow: {
          workflowPhase: { stages: [] },
          workflowValues: { phases: [] },
          workflowVariables: { entity: "someEntity", customFields: [] },
        },
      });
    });

    useServiceContext.mockReturnValue({
      rowData: { id: 1, requestCode: "REQ123", byOccurrence: false },
    });
  });

  const renderComponent = (props) => render(
    <LegalDocumentButton refresh={props.refresh} />
  )
  test("renders when byOccurrence is true", async () => {
    useServiceContext.mockReturnValue({
      rowData: { id: 1, requestCode: "REQ123", byOccurrence: true },
    });
    const props = { refresh: jest.fn() }
    act(() => {
      renderComponent(props)
    })
    await waitFor(() => {
      expect(screen.queryByTestId("LegalDocumentButtonTestId")).not.toBeInTheDocument();
    })

  });
  test("renders without crashing and displays the button when there are MTA statuses", async () => {
    getItemMTAStatus.mockResolvedValue({
      data: ["PUD", "PUD2", "FAO", "MLS"]
    });
    const props = { refresh: jest.fn() }
    act(() => {
      renderComponent(props)
    })
    await waitFor(() => {
      expect(screen.getByTestId("LegalDocumentButtonTestId")).toBeInTheDocument();
    })

  });

  test("shows a loading spinner and hides button when loading is true", async () => {
    const props = {
      refresh: jest.fn()
    }
    getItemMTAStatus.mockResolvedValue({
      data: ["PUD", "PUD2", "FAO", "MLS"]
    });
    createFileData.mockResolvedValue({
      errors: true
    })
    poClient.get.mockResolvedValue({
      data: {},
      status: 200
    })
    fileClient.post.mockResolvedValue({
      data: {
        result: {
          data: [{ key: "some key" }]
        }
      }
    })
    act(() => {
      renderComponent(props)
    });
    await waitFor(() => {
      const button = screen.getByText(/Legal documents/i);
      fireEvent.click(button)
      expect(screen.getByText(/Creating legal documents.../i)).toBeInTheDocument();
    })
  })
  test("shows an error when api fails", async () => {
    const props = {
      refresh: jest.fn()
    }
    getItemMTAStatus.mockResolvedValue({
      data: ["PUD", "PUD2", "FAO", "MLS", "pud1",]
    });
    poClient.get.mockResolvedValue({
      data: {},
      status: 500
    })
    act(() => {
      renderComponent(props)
    });
    await waitFor(() => {
      const button = screen.getByText(/Legal documents/i);
      fireEvent.click(button)
      expect(screen.getByText(/Creating legal documents.../i)).toBeInTheDocument();
    })
  })

  test("fails and enter in catch segment", async () => {
    const props = {
      refresh: jest.fn()
    }
    getItemMTAStatus.mockResolvedValue({
      data: ["PUD", "PUD2", "FAO", "MLS", "pud1",]
    });
    poClient.get.mockResolvedValue({
      data: {},
      status: 200
    })
    fileClient.post.mockResolvedValue(Error)
    act(() => {
      renderComponent(props)
    });
    await waitFor(() => {
      const button = screen.getByText(/Legal documents/i);
      fireEvent.click(button)
      expect(screen.getByText(/Creating legal documents.../i)).toBeInTheDocument();
    })
  })

});
