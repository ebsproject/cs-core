import React, { useState, useContext, useEffect } from "react";
import { Core } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { po_client as poClient } from "utils/axios";
import { fileClient } from "utils/axios";
import { showMessage } from "store/modules/message";
import { FormattedMessage } from "react-intl";
import { RBAC } from "@ebs/layout";
import { createFileData, getItemMTAStatus } from "../functions";
const { Grid, Button, Typography, Tooltip } = Core;
import loader from "assets/images/time_loader.gif"
import { useServiceContext } from "custom-components/context/service-context";
/// Component deprecated
const LegalDocumentButton = React.forwardRef(({ refresh }, ref) => {
  const dispatch = useDispatch();
  const { rowData } = useServiceContext();
  const [loading, setLoading] = useState(false);
  const [show, setShow] = useState(false);
  const [mtaStatuses, setMTAStatuses] = useState(null);
//  const [mlsData, setMlsData] = useState('');

  useEffect(() => {
    const fetchData = async () => {
      if(rowData.byOccurrence){
        setMTAStatuses([])
      }else{
        const filters = [
          { col: "service.id", mod: "EQ", val: Number(rowData.id || 0) }
        ]
        const { data } = await getItemMTAStatus(filters);
      //  setMlsData(mlsAncestors && mlsAncestors || "");
        setMTAStatuses(data && data.map(item => {return item.toLowerCase()}) || []);
        setShow(data && data.length > 0 ? true : false);
      }
    }
    fetchData()
  }, [rowData])
  const showPopupMessage = (message, variant) => {
    dispatch(
      showMessage({
        message: message,
        variant: variant,
        anchorOrigin: {
          vertical: "top",
          horizontal: "right",
        },
      })
    );
  }
  const createAndUploadLegalDocuments = async () => {

    let mtaStatusesName = mtaStatuses;
    let templates = [];

    if (mtaStatusesName.includes("pud") || mtaStatusesName.includes("pud1"))
      templates.push({ name: "sys_legal_smta_pud", mta: "PUD, PUD1" });
    if (mtaStatusesName.includes("pud2"))
      templates.push({ name: "sys_legal_smta_pud2", mta: "PUD2" });
    if (mtaStatusesName.includes("rel1") || mtaStatusesName.includes("mls") || mtaStatusesName.includes("smta") || mtaStatusesName.includes("fao"))
      templates.push({ name: "sys_legal_smta_mls", mta: "Rel1,MLS,SMTA,FAO" });
        
     for(let i = 0; i < templates.length; i ++) {
      const template = templates[i];
      setLoading(true);
      const { status: poStatus, data: poData } = await poClient.get(`/api/report/export?name=${template.name}&format=pdf&parameter[includeUniqueAncestors]=true&parameter[includeItems]=true&parameter[id]=${rowData.id}`, {
        responseType: "blob",
      })
      if (poStatus !== 200) {
        setLoading(false);
        showPopupMessage("There was an error trying to generate the Legal Document. Status error: " + poStatus, "error");
        return;
      }
      const documentName = `${template.mta}-${rowData.requestCode}.pdf`;
      const file = new File(
        [poData],
        documentName,
        {
          type: "application/pdf",
        }
      );

      const formData = new FormData();
      formData.append("file", file);
      formData.append("description", `LEGAL DOCUMENT FOR ${rowData.requestCode}, MTA Status : ${template.mta}`);
      try {
        const fileResponse = await fileClient.post(`${1}/objects`, formData, {
          headers: { "Content-Type": "multipart/form-data" },
        });
        let input = {
          id: 0,
          serviceId: rowData.id,
          fileId: fileResponse.data.result.data[0].key,
          remarks: "",
          tenantId: 1
        }
        const rep = await createFileData(input);
        if (rep.errors) {
          setLoading(false);
          showPopupMessage("There was an error trying to create the Legal Document metadata.", "error");
          return;
        }
      } catch (error) {
        setLoading(false);
        showPopupMessage(`There was an error trying to upload the Legal Document. Error: Document with name ${documentName} already exists.`+ error,"error");
      }
    }
    refresh();
    setLoading(false);
  };
  if (!show) return null;

  return (
    /* 
     @prop data-testid: Id to use inside LegalDocumentForm.test.js file.
     */
    <div data-testid={"LegalDocumentButtonTestId"}>
      {loading ? (
        <Grid container flexDirection={"row"}>
          <Grid item xs={10}><Typography color="primary" variant={"caption"}>{"Creating legal documents..."}</Typography></Grid>
          <Grid item xs={2}> <img src={loader} width={25} height={25} alt="Loading..." /></Grid>
        </Grid>
       
      ) : (
        <RBAC allowedAction="Service Processor" >
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Generate and attach legal document" />
            </Typography>
          }
        >
          <Button
            onClick={createAndUploadLegalDocuments}
            variant="contained"
            color="primary"
            size="medium"
            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
          >
            Legal documents
          </Button>
        </Tooltip>
        </RBAC>

      )}
    </div>

  );
});
// Type and required properties
LegalDocumentButton.propTypes = {};
// Default properties


export default LegalDocumentButton;
