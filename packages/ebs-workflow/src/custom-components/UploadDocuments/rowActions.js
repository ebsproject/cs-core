import { Core, Icons} from '@ebs/styleguide';
import DownloadDocument from './downloadDocument';
import DeleteDocument from './deleteDocument';
const { IconButton, Box } = Core;
export const RowActions =(rowData, refresh) =>{

    return(
        <Box display="flex" data-testid={"RowActionDocumentsTestId"}>
            <DownloadDocument rowData={rowData}/>
            <DeleteDocument refresh={refresh} rowData = {rowData}/>
        </Box>

    )
}