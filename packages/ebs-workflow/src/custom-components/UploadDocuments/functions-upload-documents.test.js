
import { deleteFile, deleteSingleDocument } from './functions';
import { fileClient } from 'utils/axios';
jest.mock('utils/axios', () => ({
    fileClient: {
        delete: jest.fn(),
    },
}));

describe('deleteSingleDocument', () => {
    beforeEach(() => {
        jest.clearAllMocks();
    });

    it('should delete the file with the specified item file id', async () => {
        const mockItem = { file: { id: 456 } };
        fileClient.delete.mockResolvedValue({ data: { success: true } });

        await deleteSingleDocument(mockItem);

        expect(fileClient.delete).toHaveBeenCalledWith(`1/objects/${mockItem.file.id}`);
    });

    it('should handle errors when deleting a file', async () => {
        const mockItem = { file: { id: 456 } };
        const errorMessage = 'Error deleting document';
        fileClient.delete.mockRejectedValue(new Error(errorMessage));

        await expect(deleteSingleDocument(mockItem)).rejects.toThrow(errorMessage);
    });
});