import { EbsGrid } from "@ebs/components";
import { getCoreSystemContext } from "@ebs/layout";
import { Core } from '@ebs/styleguide';
import { columns } from "./columns";
const { Typography } = Core;
import { ToolbarActions } from "./toolbarActions";
import { RowActions } from "./rowActions";
import { useServiceContext } from "custom-components/context/service-context";
import { useEffect } from "react";

export const UploadDocuments = ({node}) => {
  const { graphqlUri } = getCoreSystemContext();
  const { rowData, setCustomComponent } = useServiceContext();
  const { mode } = useServiceContext();

  useEffect(()=>{
    if(node){
      setCustomComponent(node);
    }
  },[]);
  return (
    <EbsGrid
      title={
        <>
          <Typography variant="h4" color="primary" className="text-ebs text-bold" >
            {"Documents"}
          </Typography>
        </>
      }
      id={`upload-documents-id`}
      toolbar={true}
      toolbaractions={mode === "view" ? null : ToolbarActions}
      columns={columns}
      rowactions={mode === "view" ? null : RowActions}
      csvfilename={`DocumentList`}
      height="65vh"
      raWidth={100}
      select="multi"
      entity={"ServiceFile"}
      callstandard="graphql"
      disabledViewPrintOunt={true}
      uri={graphqlUri}
      defaultFilters={[{ col: "service.id", mod: "EQ", val: rowData?.id || 0 }]}
      auditColumns
    />
  )
}