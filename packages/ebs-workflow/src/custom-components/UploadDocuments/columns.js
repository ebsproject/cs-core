import { Core } from "@ebs/styleguide";
const { Typography } = Core;

export const columns = [
    {
      Header: "id",
      accessor: "id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "File Id",
      accessor: "file.id",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
         {"File Name"} 
        </Typography>
      ),

      csvHeader: "File Name",
      accessor: "file.name",
      width: 400,
    },
  ];