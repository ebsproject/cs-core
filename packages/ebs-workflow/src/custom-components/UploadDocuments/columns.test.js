import { render } from "@testing-library/react";
import { columns } from "./columns";

describe("columns configuration/definition", () => {
  test("verifies id column properties", () => {
    const idColumn = columns.find(column => column.accessor === "id");
    expect(idColumn).toBeDefined();
    expect(idColumn.hidden).toBe(true);
    expect(idColumn.disableGlobalFilter).toBe(true);
  });

  test("verifies file.id column properties", () => {
    const fileIdColumn = columns.find(column => column.accessor === "file.id");
    expect(fileIdColumn).toBeDefined();
    expect(fileIdColumn.hidden).toBe(true);
    expect(fileIdColumn.disableGlobalFilter).toBe(true);
  });

  test("verifies File Name column Header", () => {
    const fileNameColumn = columns.find(column => column.accessor === "file.name");
    expect(fileNameColumn).toBeDefined();
    expect(fileNameColumn.width).toBe(400);
    
    const { getByText } = render(fileNameColumn.Header);
    const headerText = getByText("File Name");
    expect(headerText).toBeInTheDocument();
  });
});
