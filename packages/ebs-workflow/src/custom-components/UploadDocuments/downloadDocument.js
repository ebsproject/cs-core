import React from "react";
import { fileClient } from "utils/axios";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { actionButtonDownload } from "./functions";

const { IconButton, Tooltip, Typography } = Core;
const { CloudDownload } = Icons;

const DownloadDocument = React.forwardRef(
  ({ rowData }, ref) => {
    
const handleClick = async () =>{
 await actionButtonDownload(rowData);
}

    return (
      <div data-testid="ShipmentDocumentBrowserButtonDownloadTestId">
        <Tooltip
          arrow
          title={
            <Typography className="font-ebs text-xl">
              <FormattedMessage id="none" defaultMessage="Download Document" />
            </Typography>
          }
        >
          <IconButton
            onClick={handleClick}
            aria-label="download-shipment-document"
            color="primary"
            data-testid={"ButtonDownloadDocumentTestId"}
          >
            <CloudDownload />
          </IconButton>
        </Tooltip>
      </div>
    );
  }
);
// Type and required properties
DownloadDocument.propTypes = {};
// Default properties

export default DownloadDocument;
