import {getContext} from "@ebs/layout";
import { client } from "utils/apollo";
import { gql } from "@apollo/client";
import { FIND_TOTAL_ITEMS_BY_MTA_STATUS } from "custom-components/Items/item-components/gql";
import { fileClient } from "utils/axios";

export async function createFileData(metadata){
    try {
      let ServiceFileInput = {
        id: 0,
        serviceId: metadata.serviceId,
        fileTypeId:metadata.fileTypeId,
        fileId: metadata.fileId,
        remarks: "",
        tenantId: getContext().tenantId
      };
  
      const { errors , data } = await client.mutate({
        mutation: CREATE_FILE_DATA,
        variables: { ServiceFile: ServiceFileInput },
      });
      if (errors) {
        return {errors: errors};
      }
      return { data : data };
    } catch (error) {
      console.log(error)
      return {errors : error}
  
    }
  };

  export const CREATE_FILE_DATA = gql`
  mutation CREATE_FILE_DATA($ServiceFile: ServiceFileInput!) {
    createServiceFile(ServiceFile: $ServiceFile) {
      id
    }
  }
`;
export const deleteFile = async (id)=> {
  try {
    await client.mutate({
      mutation: DELETE_FILE,
      variables: { id: id },
    });
  } catch (error) {
    console.log(error);
  } finally {
  }
};
export const DELETE_FILE = gql`
  mutation DELETE_FILE($id: Int!) {
    deleteServiceFile(id: $id)
  }
`;

export async function getItemMTAStatus(filter) {
  try {
    let totalData = [];
    let number = 1;
    let _totalElements = 0;
    const { data: totalEle } = await client.query({
      query: FIND_TOTAL_ITEMS_BY_MTA_STATUS,
      variables: {
        page: { size: 1, number: number },
        filters: filter,
      }, fetchPolicy: "no-cache"
    });
_totalElements = totalEle.findServiceItemList.totalElements

    const { data } = await client.query({
      query: FIND_TOTAL_ITEMS_BY_MTA_STATUS,
      variables: {
        page: { size: 1, number: number },
        filters: [...filter, { col: "mtaStatus", val: "NA", mod: "EQ" }],
      }, fetchPolicy: "no-cache"
    });
    let elementsFiltered = data.findServiceItemList.totalElements;
    let elementsWithMTA = _totalElements - elementsFiltered;


    if (elementsWithMTA > 0) {
      const { data } = await client.query({
        query: FIND_TOTAL_ITEMS_BY_MTA_STATUS,
        variables: {
          page: { size: 300, number: number },
          filters: filter,
        }, fetchPolicy: "no-cache"
      });
      number = data.findServiceItemList.totalPages;
      totalData = [...totalData, ...data.findServiceItemList.content]
      if (number > 1) {
        for (let i = 2; i <= number; i++) {
          const { data } = await client.query({
            query: FIND_TOTAL_ITEMS_BY_MTA_STATUS,
            variables: {
              page: { size: 300, number: number },
              filters: filter,
            }, fetchPolicy: "no-cache"
          });
          totalData = [...totalData, ...data.findServiceItemList.content]
        }
      }
      const uniqueValues = Array.from(
        new Set(totalData.map(item => JSON.stringify(item)))
      ).map(item => JSON.parse(item));
      let mtaStatusArray = totalData.map(item => item.mtaStatus)
      let unique_mtaStatuses = new Set(mtaStatusArray);
      let arrayMTA = Array.from(unique_mtaStatuses);
    //  let mlsAncestors =  extractUniqueMlsAncestors(totalData);
      return {data :arrayMTA, valuesArray:uniqueValues}
    }
    else return []


  } catch (error) {
    console.error(error)
    return [];
  }
}
function extractUniqueMlsAncestors(data) {
  let splitMlsAncestors = [];
  for (let i = 0; i < data.length; i++) {
    let item = data[i].mlsAncestors;
    if (item !== 'NA')
      splitMlsAncestors = [...splitMlsAncestors, ...item.split(",")]
  }
  let uniqueMlsAncestorsArray = [...new Set(splitMlsAncestors)];
  const mlsAncestors = uniqueMlsAncestorsArray.join(",")
  return mlsAncestors;
}

export async function actionButtonDownload (rowData){
  try {
 const {data} =  await fileClient
      .get(`${1}/objects/${rowData.file.id}/download`, {
        responseType: "blob",
      });
        const url = window.URL.createObjectURL(new Blob([data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", rowData.file.name); //
        document.body.appendChild(link);
        link.click();
    
  } catch ({ message }) {
    console.log(message);
  } 
};

export async function actionDeleteDocument(rowData) {

  await fileClient.delete(`${1}/objects/${rowData.file.id}`)

};
export async function deleteSingleDocument(item) {
  await fileClient.delete(`${1}/objects/${item.file.id}`);

}