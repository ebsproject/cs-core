import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import DownloadDocument from './downloadDocument';
import { actionButtonDownload } from './functions';

jest.mock('./functions', () => ({
    actionButtonDownload: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe('Download Document Component', () => {
    const rowDataMock = {
        file: {
            id: '1234',
            name: 'sample.pdf',
        },
    };
    const setup = () =>
        render(
            <DownloadDocument rowData={rowDataMock} />
        );

    beforeEach(() => {
        jest.clearAllMocks();
    });
    test('renders download button in DOM', () => {
        setup();
        const downloadButton = screen.getByTestId('ButtonDownloadDocumentTestId');
        expect(downloadButton).toBeInTheDocument();
    });

    test('calls action Button Download with rowData when button is clicked', async () => {
        setup();
        const downloadButton = screen.getByTestId('ButtonDownloadDocumentTestId');
        fireEvent.click(downloadButton);
        expect(actionButtonDownload).toHaveBeenCalledWith(rowDataMock);
        expect(actionButtonDownload).toHaveBeenCalledTimes(1);
    });
});
