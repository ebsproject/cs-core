import DeleteDocument from "./deleteDocument";
import { findAllByTestId, fireEvent, render, screen, waitFor } from "@testing-library/react";
import { actionDeleteDocument, deleteFile } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context";

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));

jest.mock('./functions', () => ({
    actionDeleteDocument: jest.fn(),
    deleteFile: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));
jest.mock("components/alerts/alert-context", () => ({
    useAlertContext: jest.fn(),
}));
const rowDataMock = { id: 1, name: "Test Code" }
const refreshMock = jest.fn();
const setAlertMock = jest.fn();

describe("Delete Single Document component", () => {

    beforeEach(() => {
        jest.clearAllMocks();
        useAlertContext.mockReturnValue({
            setAlerts: setAlertMock,
        });

    });

    const renderComponent = () => render(
        <DeleteDocument rowData={rowDataMock} refresh={refreshMock} />
    );
    
    test("Render de Icon Button in DOM", () => {
        const { getByTestId } = renderComponent()
        expect(getByTestId("DeleteSingleDocumentTestId")).toBeInTheDocument();
    });

    test("open dialog modal when click in button", () => {
        const { getByTestId } = renderComponent();
        const button = getByTestId("DeleteSingleDocumentTestId");
        fireEvent.click(button);
        const modal = screen.getByTestId("DeleteDocumentId");
        expect(getByTestId("DeleteButtonCancelTestId")).toBeInTheDocument();
        expect(getByTestId("DeleteButtonConfirmTestId")).toBeInTheDocument();
        expect(modal).toBeInTheDocument();
    });

    test('closes dialog on Cancel button click', async () => {
        const { getByTestId, queryByText } = renderComponent();
        fireEvent.click(getByTestId('DeleteSingleDocumentTestId'));
        fireEvent.click(getByTestId('DeleteButtonCancelTestId'));
        await waitFor(() => {
            expect(queryByText(/Do you want to delete this file permanently/i)).not.toBeInTheDocument();
        })
    });
    test('calls setAlerts with success message after successful deletion', async () => {
        actionDeleteDocument.mockResolvedValue();
        deleteFile.mockResolvedValue();
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId('DeleteSingleDocumentTestId'));
        fireEvent.click(getByTestId('DeleteButtonConfirmTestId'));
        await waitFor(() => {
            expect(actionDeleteDocument).toHaveBeenCalledWith(rowDataMock);
            expect(deleteFile).toHaveBeenCalledWith(rowDataMock.id);
            expect(setAlertMock).toHaveBeenCalledWith(expect.any(Function));
            expect(refreshMock).toHaveBeenCalled();
        });
        const setAlertsCall = setAlertMock.mock.calls[0][0];
        const alerts = setAlertsCall([]);
        expect(alerts).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    id: `file-delete-message-success-${rowDataMock.id}`,
                    message: 'File deleted',
                    severity: 'success'
                })
            ])
        );
    });
    test('calls setAlerts with error message on deletion failure', async () => {
        const errorMessage = 'Deletion failed';
        actionDeleteDocument.mockRejectedValue({
            response: { data: { metadata: { status: [{ message: errorMessage }] } } }
        });
        const { getByTestId } = renderComponent();
        fireEvent.click(getByTestId('DeleteSingleDocumentTestId'));
        fireEvent.click(getByTestId('DeleteButtonConfirmTestId'));
        await waitFor(() => {
            expect(actionDeleteDocument).toHaveBeenCalledWith(rowDataMock);
            expect(setAlertMock).toHaveBeenCalledWith(expect.any(Function));
        });
        const setAlertsCall = setAlertMock.mock.calls[0][0];
        const alerts = setAlertsCall([]);

        expect(alerts).toEqual(
            expect.arrayContaining([
                expect.objectContaining({
                    id: `file-delete-message-error-${rowDataMock.id}`,
                    message: errorMessage,
                    severity: 'error'
                })
            ])
        );
    });

})