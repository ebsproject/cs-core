import React, { useState, useEffect } from "react";
import { Core, Lab, DropzoneDialog } from "@ebs/styleguide";
const { Button, TextField, Dialog, DialogTitle, DialogActions, DialogContent, CircularProgress, Grid, Typography } = Core;
const { Autocomplete } = Lab;
import { useForm } from "react-hook-form";
import { getContext } from "@ebs/layout";
import { createFileData } from "./functions";
import { fileClient } from "utils/axios";
import { client } from "utils/apollo";
import { FIND_FILE_TYPE_LIST } from "utils/apollo/gql/workflow";
import { useSelector } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context";
import { Controller } from "react-hook-form";
export const UploadFile = React.forwardRef(({ refresh }, ref) => {

  const { rowData } = useServiceContext();
  const { setAlerts } = useAlertContext();
  const { workflowValues } = useSelector(({ wf_workflow }) => wf_workflow);
  const [open, setOpen] = useState(false);
  const [openF, setOpenF] = useState(false);
  const [loading, setLoading] = useState(false);
  const [fileTypes, setFileTypes] = useState(null);
  const [fileNames, setFileNames] = useState("");

  const handleClose = () => {
    reset({ values: { fileDescription: "", fileType: "", files: null } });
    setFileNames('');
    setOpen(false);
  };
  const { handleSubmit,
    reset,
    setValue,
    control,
    formState: { errors }, } = useForm({ defaultValues: { fileDescription: "", fileType: "", files: null } });

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await client.query({
        query: FIND_FILE_TYPE_LIST,
        variables: {
          filters: [{ col: "workflow.id", mod: "EQ", val: workflowValues.id }]
        }
      })
      setFileTypes(data.findFileTypeList.content)
    }
    fetchData();
  }, [workflowValues])

  const submit = async (data, event) => {

    event.preventDefault();
    if (data.flies !== null) {
      setLoading(true);
      for (let i = 0; i < data.files.length; i++) {
        const formData = new FormData();
        formData.append("file", data.files[i]);
        formData.append("description", data.fileDescription || "NA");
        try {
          const response = await fileClient.post(`${Number(getContext().tenantId)}/objects`, formData, {
            headers: { "Content-Type": "multipart/form-data" },
          });
          let metadata = {
            serviceId: rowData.id,
            fileId: response.data.result.data[0].key,
            fileTypeId: data.fileType?.id || 0
          }
          const resp = await createFileData(metadata);
          if (resp.data) {
            setAlerts((prev) => [...prev, { id: `success-uploading-message-${data.files[i].name}`, message: `The document ${data.files[i].name} was uploaded successfully.`, severity: 'success' }]);
          }
        } catch (error) {
          const { response } = error;
          let message = response.data.metadata.status[0].message;
          let _message = message.replace(" for tenant 1", ".")
          if (_message.includes("already exists")) {
            _message = `A file with the same name "${data.files[i].name}" has been uploaded earlier. Please update the file name to make it unique and try again`;
            setAlerts((prev) => [...prev, { id: `error-uploading-message-${data.files[i].name}`, message: _message, severity: 'error' }]);
          }
          else
            setAlerts((prev) => [...prev, { id: `error-uploading-message-${data.files[i].name}`, message: `Error trying to upload ${data.files[i].name}, Reason: ${_message}`, severity: 'error' }]);
        }
      }
      setLoading(false);
      handleClose();
      refresh();
    }
  }
  const handleChangeFileType = (e, option) => {
    setValue('fileType', option)
  }
  const onClick = (files) => {
    setValue('files', files)
    setOpenF(!openF);
    let _names = []
    if (files && files.length > 0)
      _names = files.map(file => file.name);
    setFileNames(_names.join(","));
    setOpenF(!openF);
  };

  return (
    <div data-testid="ShipmentDocumentBrowserUploadFileTestId">
      <Button
        variant="contained"
        color="primary"
        className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
        size="medium"
        onClick={() => setOpen(true)}>
        Upload
      </Button>
      <Dialog
        onClose={handleClose}
        open={open}
        aria-label="uploadFileDialog"
        fullWidth
        maxWidth={"sm"}
      >
        <DialogTitle>Upload document</DialogTitle>
        <DialogContent>
          <form onSubmit={handleSubmit(submit)} name="ContactInfoForm" >
            <Grid container spacing={2}>
              <Controller
                control={control}
                name={"files"}
                render={({ field: { onChange, value, ref } }) => (
                  <DropzoneDialog
                    error={""}
                    open={openF}
                    onClose={onClick}
                    value={value}
                    onSave={(files) => {
                      onChange(files);
                      onClick(files);
                    }}
                    acceptedFiles={[".csv", ".pdf", ".jpg", ".png", ".jpeg"]}
                    cancelButtonText={"cancel"}
                    submitButtonText={"ok"}
                    maxFileSize={5000000}
                    showPreviews={true}
                    showFileNamesInPreview={true}
                  />
                )}
                rules={{ required: "Please select the document(s) to upload" }}
              />

              {
                workflowValues.id !== 562 && (
                  <Grid item xs={12}>
                    <Controller
                      control={control}
                      name={'fileDescription'}
                      render={({ field }) => (
                        <TextField
                          fullWidth
                          variant={"outlined"}
                          multiline
                          rows={5}
                          label="Document Description"
                          name="fileDescription"
                          onChange={(e) => setValue('fileDescription', e.target.value)}
                        />
                      )}
                      rules={{ required: "Please enter file description" }}
                    />
                    <Typography
                      component={'span'}
                      color={"error"}
                    >
                      {errors['fileDescription'] && errors['fileDescription'].message}
                    </Typography>
                  </Grid>
                )
              }


              {workflowValues.id !== 562 && fileTypes && fileTypes.length > 0 && (
                <Grid item xs={12}>
                  <Controller
                    control={control}
                    name={'fileType'}
                    render={({ field }) => (
                      <Autocomplete
                        id="file_type_options"
                        options={fileTypes || []}
                        getOptionLabel={(option) => option.description}
                        fullWidth
                        onChange={handleChangeFileType}
                        renderInput={(params) => <TextField {...params} label="Select a document type" variant="outlined" />}
                      />
                    )}
                  />
                </Grid>
              )}

              <Grid item xs={12}>
                <Button fullWidth onClick={() => setOpenF(true)}>
                  {'Select the document(s)'}
                </Button>
                <Typography
                  component={'span'}
                  color={"error"}
                >
                  {errors['files'] && errors['files'].message}
                </Typography>
                <Grid item xs={6}>
                  <Typography variant="body1"> {fileNames} </Typography>
                </Grid>
              </Grid>

            </Grid>

            <DialogActions>
              <Button onClick={handleClose} color="secondary" className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white">
                Close
              </Button>
              {loading ? (<CircularProgress />) : (
                <Button
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                  type="submit" >
                  Save
                </Button>)}
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}
);

export default UploadFile;