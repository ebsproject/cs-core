import React, { useState } from "react";
import { Core, Lab } from "@ebs/styleguide";
const {
  Button,
  Grid,
  TextField,
  Typography,
  LinearProgress,
  Checkbox,
  Chip,
} = Core;
const { Autocomplete } = Lab;
import { FormattedMessage } from "react-intl";
import { FIND_HIERARCHIES_BY_CONTACT } from "./queries";
import { useQuery } from "@apollo/client";
import { useForm, Controller } from "react-hook-form";
import { addAddressContact } from "./functions";


const AddAddressForm = React.forwardRef(
  ({ parentControl, handleClose }, ref) => {
    const countryList = [];
    const purposeList = [];
    const institutionList = [];

    const [disableAddressFields, setDisabledAddressFields] = useState(false);
    const [load, setLoad] = useState(false);
    const contactName = parentControl?.label;

    const {
      control,
      handleSubmit,
      formState: { errors },
      reset,
      setValue,
      getValues,
    } = useForm({
      defaultValues: {
        id: 0,
        country: { id: 0, name: "" },
        location: "",
        region: "",
        zipCode: "",
        streetAddress: "",
        purposes: [],
        institution: { institution: { institution: { commonName: "" } } },
      },
    });
    const { loading, error, data, refetch } = useQuery(FIND_HIERARCHIES_BY_CONTACT, {
      variables: {
        id: Number(parentControl?.value) || 0
      }
    });

    if (loading) return null;
    if (error) return `Error! ${error}`;
    const contactInstitutions = data.findContact?.parents || [];
    const onSubmit = async () => {

      setLoad(true);
      const input = {
        id: Number(parentControl?.value),
        addresses:
        {
          id: getValues("id"),//addressData.id,
          country: getValues("country"),//addressData.country,
          location: getValues("location"),//addressData.location,
          region: getValues("region"),// addressData.region,
          zipCode: getValues("zipCode"),//addressData.zipCode,
          street: getValues("streetAddress"),//addressData.streetAddress,
          purposes: getValues("purposes"),//addressData.purposes,
        },
      };
      await addAddressContact(input);
      setLoad(false);
      handleClose();

    };

    const setDataToForm = (option) => {

      setDisabledAddressFields(false);
      if (option.institution.addresses.length > 0) {
        setDisabledAddressFields(true);
        setValue("id", option.institution.addresses[0].id);
        setValue("streetAddress", option.institution.addresses[0].streetAddress);
        setValue("location", option.institution.addresses[0].location);
        setValue("region", option.institution.addresses[0].region);
        setValue("zipCode", option.institution.addresses[0].zipCode);
        setValue("purposes", option.institution.addresses[0].purposes);
        setValue("country", option.institution.addresses[0].country);
      } else
        reset({ institution: option });
    }
    const renderInputPurpose = (params, field, options) => {
      const newInputProps = new Object();
      if (!getValues(field)) {
        Object.assign(newInputProps, { value: "" });
      }
      return (
        <TextField
          {...params}
          variant="outlined"
          inputProps={{
            ...params.inputProps,
            ...newInputProps,
          }}
          InputLabelProps={{ shrink: true }}
          focused={Boolean(errors[field])}
          label={
            <Typography className="text-ebs">
              <FormattedMessage id={"none"} defaultMessage={"Purposes"} />
            </Typography>
          }
          helperText={errors[field]?.message}
        />
      );
    };
    return (
      <form name={"addAddressForm"} >
        <Grid container spacing={2} direction={"row"}>
          <Typography>
            {`Adding address for: ${contactName ? contactName : "Please select a contact first."}`}
          </Typography>
          <Grid item xs={12} md={12} sm={12}>
            <Controller
              name="institution"
              control={control}
              render={({ field }) => (
                <Autocomplete
                  {...field}
                  id="institution"
                  options={contactInstitutions.length > 0 ? contactInstitutions.filter(item => item.institution.category.name === "Institution") : institutionList}
                  getOptionLabel={(option) => option.institution.institution.commonName}
                  renderInput={(params) => (
                    <TextField required {...params} label="Institution" variant="outlined" />
                  )}
                  onChange={(event, option) => {
                    setValue("institution", option)
                    setDataToForm(option)
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={6} md={6} sm={6}>
            <Controller
              name="country"
              control={control}
              render={({ field }) => (
                <Autocomplete
                  {...field}
                  disabled={true}
                  id="country"
                  options={countryList || []}
                  getOptionLabel={(option) => option.name}
                  renderInput={(params) => (
                    <TextField required {...params} label="Country" variant="outlined" />
                  )}
                  onChange={(event, option) => {
                    setValue("country", option);
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={6} md={6} sm={6}>
            <Controller
              name="location"
              control={control}
              render={({ field }) => (
                <TextField
                  required
                  disabled={true}
                  {...field}
                  fullWidth
                  label={<FormattedMessage id="none" defaultMessage="Location" />}
                  onChange={(event) => {
                    setValue("location", event.target.value);
                  }}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={6} md={6} sm={6}>
            <Controller
              name="region"
              control={control}
              render={({ field }) => (
                <TextField
                  required
                  disabled={true}
                  {...field}
                  fullWidth
                  label={<FormattedMessage id="none" defaultMessage="Region" />}
                  onChange={(event) => {
                    setValue("region", event.target.value);
                  }}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={6} md={6} sm={6}>
            <Controller
              name="zipCode"
              control={control}
              render={({ field }) => (
                <TextField
                  required
                  disabled={true}
                  {...field}
                  fullWidth
                  label={
                    <FormattedMessage id="none" defaultMessage="Postal Code" />
                  }
                  onChange={(event) => {
                    setValue("zipCode", event.target.value);
                  }}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={6} md={6} sm={6}>
            <Controller
              name="streetAddress"
              control={control}
              render={({ field }) => (
                <TextField
                  required
                  disabled={true}
                  {...field}
                  fullWidth
                  label={<FormattedMessage id="none" defaultMessage="Street Address" />}
                  onChange={(event) => {
                    setValue("streetAddress", event.target.value);
                  }}
                  variant="outlined"
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              )}
            />
          </Grid>
          <Grid item xs={6} md={6} sm={6}>
            <Controller
              name={"purposes"}
              control={control}
              render={({ field }) => (
                <Autocomplete
                  disabled={true}
                  {...field}
                  multiple
                  limitTags={4}
                  id={"purposes"}
                  data-testid={"purposes"}
                  options={purposeList || []}
                  value={field?.value || []}
                  renderTags={(tagValue, getTagProps) =>
                    tagValue.map((option, index) => 
                      {
                        const { key, ...restProps } = getTagProps({ index });
                        return <Chip key={key} label={option.name} {...restProps} />
                      }
                  )
                  }
                  inputValue={
                    getValues("purposes")?.name || ""
                  }
                  disableCloseOnSelect
                  onChange={(e, options) => {
                    setValue("purposes", options);
                  }}
                  getOptionLabel={(option) => option.name}
                  renderOption={(option, { selected }) => {
                    return (<>
                      <Checkbox checked={selected} />
                      {option.name}
                    </>
                    );
                  }}
                  renderInput={(params) =>
                    renderInputPurpose(
                      params,
                      "purposes",
                      field.value
                    )
                  }
                  isOptionEqualToValue={(option, value) => option.id === value.id}
                />
              )}
            />
          </Grid>
        </Grid>

        <Grid
          container
          spacing={2}
          direction={"row"}
          justifyContent="flex-end"
        >
          <Grid item xs={3}>
          <Button
                  fullWidth
                  onClick={handleClose}
                  variant="contained"
                  color="primary"
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white btn-success"
                >
                  Cancel
                </Button>
          </Grid>
          <Grid item xs={3}>
            {load ? (
              <div sx={{width:200}}>
                <LinearProgress />
              </div>
            ) : (
              <>
                <Button
                  disabled={!disableAddressFields}
                  fullWidth
                  onClick={onSubmit}
                  variant="contained"
                  color="primary"
                  className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white btn-success"
                >
                  Save
                </Button>
              </>
            )}
          </Grid>
        </Grid>
      </form>
    );
  }
);
export default AddAddressForm;