import { FIND_CONTACT_BY_ID, MODIFY_CONTACT } from "./queries";
import { client } from "utils/apollo";
export const addAddressContact = async (input) => {
    
    try {
        let flagDefault = false;
        const { data } = await client.query({
            query: FIND_CONTACT_BY_ID,
            variables: { id: Number(input.id) }
        });
        const contact = data.findContact;
       if(contact.addresses.length === 0)
          flagDefault = true;
   
        const contactInput = new Object({
            id: contact.id,
            contactTypeIds: contact.contactTypes.map((item) => item.id),
            addresses: [{
                id: input.addresses.id,
                default: flagDefault,
                location: input.addresses.locality,
                region: input.addresses.region,
                streetAddress: input.addresses.street,
                zipCode: input.addresses.zipCode,
                countryId: input.addresses.country.id,
                purposeIds: input.addresses.purposes.map(item => item.id),
            }],
            purposeIds: contact.purposes.map((data) => Number(data.id)),
            tenantIds: contact.tenants.length === 0 ? [1] : contact.tenants.map((item) => Number(item.id)),
            category: {id:contact.category.id},
        });


        const { errors } = await client.mutate({
            mutation: MODIFY_CONTACT,
            variables: {
                contact: contactInput,
            },
        });

    } catch (error) {
        console.log(error);
    }
};