import { gql } from "@apollo/client"

export const MODIFY_CONTACT = gql`
  mutation MODIFY_CONTACT($contact: ContactInput!) {
    modifyContact(contact: $contact) {
      id
    }
  }
`;

export const FIND_HIERARCHIES_BY_CONTACT = gql`
query FIND_HIERARCHIES_BY_CONTACT($id:ID!){
  findContact(id: $id){
    id
    parents{
      contact {
          id
        }
        institution {
          id
          category{
               id
               name
               }
        contactTypes{
                    id
                  }
        tenants{
              id
              }
        purposes{
                id
                name               
              }
          addresses {
            id
            country {
              id
              name
            }
            purposes {
              id
              name
              category {
                name
              }
            }
            location
            region
            zipCode
            streetAddress
          }
          institution {
            commonName
            isCgiar
          }
        }
    }
  }
}
`;
export const FIND_CONTACT_BY_ID = gql`
  query FIND_CONTACT_BY_ID($id: ID!) {
    findContact(id: $id) {
      addresses{
        default
        id
      }
      id
      category {
        id
      }
      tenants {
        id
      }
      contactTypes {
        id
      }
      email
      purposes {
        id
      }
    }
  }
`;
