import { gql } from "@apollo/client";

export const buildDeleteOCC = (entity) => {
    return gql`
      mutation DELETE_OCC($id: Int!){
          delete${entity}(id:$id)
      }`;
  };

  export const MODIFY_OCCURRENCES = gql`
mutation MODIFY_OCCURRENCES($occurrenceShipment: OccurrenceShipmentInput!){
  modifyOccurrenceShipment(occurrenceShipment: $occurrenceShipment){
    id
  }
}
`
export const GET_ALL_OCCURRENCES_BY_SERVICE_ID = gql`
query GET_ALL_OCCURRENCES_BY_SERVICE_ID(
  $filters:[FilterInput]
){
  findOccurrenceShipmentList(filters: $filters){
    content{
    id
    occurrenceDbId
    occurrenceNumber
    occurrenceName
    experimentCode
    experimentYear
    experimentSeason
    experimentName
    coopkey
    weight
    status
    testCode
    validated
    }

  }
}
`