
import { Core } from "@ebs/styleguide";
import RecipientDropdown from "./components/recipient-contact";
import Rsht from "./components/Rsht";
import Volume from "./components/Volume";
import SelectStatus from "./components/Status";
import Unit from "./components/Unit";
const { Typography, Chip } = Core;
export function columnDefinition(setRefreshTable, entity, node) {
  const definition = node.define;
  const extractRules = (name) => {
    const rules = definition.rules
    if (!rules || !rules.columns || !rules.columns.length)
      return {
        hidden: false,
        header: name,
      }
    let header = rules.columns.find(item => item.name === name)?.alias || name;
    let hidden = rules.columns.find(item => item.name === name)?.hidden || false;
    return {
      hidden,
      header
    }
  }

  return [
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"ID"}
        </Typography>
      ),
      csvHeader: "ID",
      accessor: "id",
      width: 10,
      hidden: true
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Request ID"}
        </Typography>
      ),
      csvHeader: "Request ID",
      accessor: "service.id",
      width: 10,
      hidden: true
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"GHT"}
        </Typography>
      ),
      Cell: ({ row }) => <Rsht value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "GHT",
      accessor: "testCode",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Total Quantity to Send"}
        </Typography>
      ),
      Cell: ({ row }) => <Volume value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "Total Quantity to Send",
      accessor: "weight",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Unit"}
        </Typography>
      ),
      Cell: ({ row }) => <Unit value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "Unit",
      accessor: "unit",
      width: 100,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Status"}
        </Typography>
      ),
      Cell: ({ row }) =>
        <SelectStatus value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "Item Status",
      accessor: "status",
      width: 200,
      hidden: false
    },

    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Occurrence ID"}
        </Typography>
      ),
      csvHeader: "OccurrenceDbID",
      accessor: "occurrenceDbId",
      width: 180,
      hidden: extractRules("isDangerous")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Occurrence Number"}
        </Typography>
      ),
      csvHeader: extractRules("testCode")["header"],
      accessor: "occurrenceNumber",
      width: 200,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {`Occurrence Name`}
        </Typography>
      ),
      csvHeader: "Occurrence Name",
      accessor: "occurrenceName",
      width: 200,
      hidden: extractRules("occurrenceName")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Experiment Name"}
        </Typography>
      ),
      csvHeader: "Experiment Name",
      accessor: "experimentName",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Experiment Code"}
        </Typography>
      ),
      csvHeader: "Experiment Code",
      accessor: "experimentCode",
      width: 180,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Experiment Year"}
        </Typography>
      ),
      csvHeader: "Experiment Year",
      accessor: "experimentYear",
      width: 180,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Experiment Season"}
        </Typography>
      ),
      csvHeader: "Experiment Season",
      accessor: "experimentSeason",
      width: 200,
      hidden: extractRules("experimentSeason")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Recipient"}
        </Typography>
      ),
      csvHeader: "Recipient",
      accessor: "recipient.person.fullName",
    //  Cell: ({ row }) => <RecipientDropdown value={row} setRefreshTable={setRefreshTable} />,
      width: 300,
      hidden:false
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Coop Key"}
        </Typography>
      ),
      csvHeader: "Coop Key",
      accessor: "coopkey",
      width: 200,
      hidden: false
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Validated"}
        </Typography>
      ),
      Cell: ({ value }) =><Chip
      size="small"
      color={value ? "primary" : "error"}
      label={value ? "OK" : "FAILED"}
    />     ,
      csvHeader: "Validated",
      accessor: "validated",
      width: 200,
      hidden: false
    },
    
  ];
}

