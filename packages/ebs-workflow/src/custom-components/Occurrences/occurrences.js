import { EbsGrid } from "@ebs/components";
import { getCoreSystemContext } from "@ebs/layout";
import { Core } from '@ebs/styleguide';
import React, { useState, useEffect, useContext } from "react";
const { Typography } = Core;
import ToolbarActions from "./toolbarActions";
import { columnDefinition } from "./columns";
import RowActions from "./rowActions";
import { getAllContacts } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";

const Occurrences = ({ entity, id, node }) => {
  const { graphqlUri } = getCoreSystemContext();
  const { setContactList, service, setService, rowData } = useServiceContext();
  const [refreshTable, setRefreshTable] = useState(false);
  const columns = columnDefinition(setRefreshTable, entity, node);

  useEffect(()=>{
    if(!service){
      setService({entity:entity,id:id});
    }
  },[entity, id]);

//   useEffect(()=>{
// const fetchContacts = async() =>{
//   const data = await getAllContacts();
//   setContactList(data || []);
// }
// fetchContacts();
//   },[])

  const toolbar = (selection, refresh) => {
    return (
      <ToolbarActions columns={columns} refresh={refresh} refreshTable={refreshTable} setRefreshTable={setRefreshTable} selection={selection} node={node} />
    )
  }
  return (
    <EbsGrid
      title={
        <>
          <Typography variant="h6" color="primary" className="text-ebs text-bold" >
            {"Occurrences"}
          </Typography>
        </>
      }
      id={`items-id`}
      toolbar={true}
      disabledViewDownloadCSV={true}
      toolbaractions={toolbar}
      columns={columns}
      rowactions={RowActions}
      csvfilename={`Occurrences Shipment List`}
      height="65vh"
      raWidth={120}
      select="multi"
      entity={`OccurrenceShipment`}
      callstandard="graphql"
      uri={graphqlUri}
      disabledViewPrintOunt={true}
      defaultSort={{ col: "id", mod: "ASC" }}
      defaultFilters={[{ col: `${entity.toLowerCase()}.id`, mod: "EQ", val: rowData.id }]}
    />

  )
}
export default Occurrences;