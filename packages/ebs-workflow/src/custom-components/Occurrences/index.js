import Occurrences from "./occurrences";

const CustomOccurrences = ({ entity, id, node, mode }) => {
    return (
            <Occurrences entity={entity} id={id} node={node} mode={mode} />
    )
}
export default CustomOccurrences;