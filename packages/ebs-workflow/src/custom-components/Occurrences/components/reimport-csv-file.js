import React, { useState, useEffect } from "react";
import { Core, DropzoneDialog } from "@ebs/styleguide";
import { useForm } from "react-hook-form";
import { uploadFile } from "./functions";

import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context";

const { Button } = Core;

const ReImportCSVFile =
    ({ refresh, refreshTable, setRefreshTable }, ref) => {
        const [open, setOpen] = useState(false);
        const { setAlerts } = useAlertContext();
        const [errorMessage, setErrorMessage] = useState(null);
        useEffect(() => {
            return () => setTimeout(() => {
                setErrorMessage(null)
            }, 3000)
        }, [errorMessage])
        const {
            control,
            formState: { errors },
        } = useForm();
        const { rowData } = useServiceContext();
        useEffect(() => {
            if (refreshTable) {
                refresh();
            }
            return () => setRefreshTable(false);
        }, [refreshTable]);

        const onClick = () => {
            setErrorMessage(null);
            setOpen(!open);
           // setRefreshTable(true)
        };
        const onSubmitAction = async (files) => {
            if (!files || files.length === 0) {
                setErrorMessage("Please drag and drop or select a csv file from your computer!!");
                return;
            }
           const res = await uploadFile(files, rowData);
            if (res.failed)
                setAlerts((prev) => [...prev, { id: 'error-upload-message', message: res.error.response.data, severity: 'error' }]);
            else
                setAlerts((prev) => [...prev, { id: 'file-upload-message-successful', message: "The CSV file was imported successfully", severity: 'success' }]);
            onClick();
            refresh();
        }
        return (
            <div data-testid={"ButtonImportCSVTestId"}>
                <Button onClick={onClick} >
                    Import CSV file
                </Button>
                <DropzoneDialog
                    error={errorMessage}
                    acceptedFiles={[".csv"]}
                    cancelButtonText={"cancel"}
                    submitButtonText={"submit"}
                    maxFileSize={5000000}
                    showPreviews={true}
                    showFileNamesInPreview={true}
                    open={open}
                    onClose={onClick}
                    onSave={onSubmitAction}
                />
            </div>
        );
    };

export default ReImportCSVFile;
