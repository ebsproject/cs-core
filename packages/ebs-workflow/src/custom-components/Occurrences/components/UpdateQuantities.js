import React, { useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
import { useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { modifyOccurrences } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { client } from "utils/apollo";
import { GET_ALL_OCCURRENCES_BY_SERVICE_ID } from "../gql";
import { sendMessage } from "custom-components/Items/item-components/functions";
const {
    Grid,
    Button,
    TextField,
    Typography,
    Box,
    CircularProgress,
} = Core;
const UpdateQuantities = React.forwardRef(({ refresh, selection, handleClose }, ref) => {

    const [selectionOption, setSelectionOption] = useState("Selected");
    const dispatch = useDispatch();
    const { rowData } = useServiceContext();
    const [loading, setLoading] = useState(false);
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
    } = useForm({
        defaultValues: {
            weight: null,
            packageCount: null,
        },
    });
    const onSubmit = async (data) => {
        if(!rowData) return;
        setLoading(true);
        if (selectionOption === "All") {
            let occurrences = [];
            try {
                const { data } = await client.query({
                    query: GET_ALL_OCCURRENCES_BY_SERVICE_ID,
                    variables: {
                        filters: [{ col: "service.id", mod: "EQ", val: rowData.id }]
                    }
                })
                occurrences = [...data.findOccurrenceShipmentList.content]
            } catch (error) {
                console.log(error);
                return;
            }
            for (let i = 0; i < occurrences.length; i++) {
                let newInput = { ...occurrences[i] }
                newInput = { ...newInput, serviceId: rowData.id, weight: data.weight }
                await modifyOccurrences(newInput);
            }
            sendMessage("success", "The total quantity was updated for all occurrences",dispatch)
            refresh();
            handleClose();
            reset();
            setLoading(false);
        } else {
            if (selection.length > 0) {
                for (let i =0; i < selection.length; i ++){
                    let newInput = {...selection[i]};
                    delete newInput['service'];
                    delete newInput['recipient']
                    newInput = {...newInput, weight: data.weight, serviceId: rowData.id}
                    await modifyOccurrences(newInput);
                }
                sendMessage("success", "The total quantity was updated for selected occurrences",dispatch)
                handleClose();
                refresh();
                setLoading(false);
                reset();
            } else {
                sendMessage("error", "No occurrences selected for updating",dispatch);
                setLoading(false);
            }
        }
    };
    return (
        <div ref={ref} data-testid={"BulkUpdateShipmentTestId"}>
            <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateForm">
                <Grid container spacing={2}>

                    <Grid item xs={6}>
                        <Typography variant={"h5"}>
                            {"Total quantity to send"}
                        </Typography>
                        <Controller
                            name="weight"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    {...field}
                                    variant="outlined"
                                    type="number"
                                    label={""}
                                    data-testid={"weight"}
                                    error={Boolean(errors["weight"])}
                                    helperText={errors["weight"]?.message}
                                />
                            )}
                            rules={{
                                required: (
                                    <Typography>
                                        {"Please enter a value for total quantity"}
                                    </Typography>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box display="flex" justifyContent="flex-end" >
                            <Button onClick={handleClose}>
                                {"Cancel"}
                            </Button>
                            {loading ? (<CircularProgress />) : (
                                <div>
                                    <Grid container spacing={2}>
                                        <Grid item>
                                            <Button
                                                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                                onClick={() => setSelectionOption("Selected")} type="submit">
                                                {"Apply to selected"}
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                                onClick={() => setSelectionOption("All")} type="submit">
                                                {"Apply to all"}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </div>
                            )}
                        </Box>
                    </Grid>
                </Grid>
            </form>
        </div>
    );
}
);
// Type and required properties
UpdateQuantities.propTypes = {};
// Default properties


export default UpdateQuantities;
