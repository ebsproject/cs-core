import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
// CORE COMPONENTS
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
// import { deleteBulk, deleteItem, sendMessage } from "./functions";
import { restClient } from "utils/axios/axios";
import { useServiceContext } from "custom-components/context/service-context";
import { modifyItems, reorderItems } from "./functions";
import { sendMessage } from "custom-components/Items/item-components/functions";
import { client } from "utils/apollo";
import { GET_ALL_OCCURRENCES_BY_SERVICE_ID } from "../gql";
import { deleteOccurrence } from "../functions";
const {
    Grid,
    Button,
    DialogActions,
    DialogContent,
    Tooltip,
    Typography,
    FormControl,
    FormLabel,
    FormControlLabel,
    Radio,
    RadioGroup,
    CircularProgress,
} = Core;
const { Delete } = Icons;

const DeleteOccurrences = React.forwardRef(
    ({ refresh, selection }, ref) => {
        const [open, setOpen] = useState(false);
        const dispatch = useDispatch();
        const [loading, setLoading] = useState(false);
        const { rowData } = useServiceContext();
        const [deleteOption, setDeleteOption] = useState("all");

        const handleClickOpen = () => setOpen(true);
        const handleClose = () => setOpen(false);

        const {
            control,
            handleSubmit,
            formState: { errors },
            reset,
            setValue,
        } = useForm({
            defaultValues: {},
        });

        const handleClose1 = (event, reason) => {
            if (reason === "backdropClick")
                return
            handleClose()
        }
        const onSubmit = async () => {
            if (!rowData) return;
            setLoading(true);
            switch (deleteOption) {
                case "all":
                    let occurrences = [];
                    try {
                        const { data } = await client.query({
                            query: GET_ALL_OCCURRENCES_BY_SERVICE_ID,
                            variables: { filters: [{ col: "service.id", mod: "EQ", val: rowData.id }] },
                            fetchPolicy: "no-cache"
                        })
                        occurrences = data.findOccurrenceShipmentList.content;

                    } catch (error) {
                        return;
                    }
                    for (let i = 0; i < occurrences.length; i++) {
                        let occurrenceId = occurrences[i].id;
                        await deleteOccurrence({ id: occurrenceId, entity: "OccurrenceShipment" });
                    }
                    sendMessage("success", "All occurrences were deleted successfully", dispatch);
                    setLoading(false);
                    handleClose();
                    refresh();
                    break;
                case "selected":
                    if (selection.length === 0) {
                        sendMessage('error', 'Please select at least one occurrence to delete', dispatch);
                        setLoading(false);
                        handleClose();
                        break;
                    }
                    for (let i = 0; i < selection.length; i++) {
                        let id = selection[i].id;
                        await deleteOccurrence({ id: id, entity: "OccurrenceShipment" });
                    }
                    sendMessage('success', 'The selected occurrences were deleted successfully', dispatch);
                    setLoading(false);
                    handleClose();
                    refresh();
                    break;
                case "failed":
                    let allOccurrences = [];
                    try {
                        const { data } = await client.query({
                            query: GET_ALL_OCCURRENCES_BY_SERVICE_ID,
                            variables: { filters: [{ col: "service.id", mod: "EQ", val: rowData.id }] },
                            fetchPolicy: "no-cache"
                        })
                        allOccurrences = data.findOccurrenceShipmentList.content;

                    } catch (error) {
                        console.log(error)
                        return;
                    }
                    let failedOccurrences = allOccurrences.filter(item => !item.validated);
                    for (let i = 0; i < failedOccurrences.length; i++) {
                        let failedOccurrenceId = failedOccurrences[i].id;
                        await deleteOccurrence({ id: failedOccurrenceId, entity: "OccurrenceShipment" });
                    }
                    sendMessage('success', 'All failed occurrences were deleted successfully', dispatch);
                    setLoading(false);
                    handleClose();
                    refresh();
                    break;
                default:
                    break;
            }
        };


        const handleClickDeleteOption = (event) => {
            setDeleteOption(event.target.value);
        };
        return (
            <div ref={ref} data-testid={"BulkUpdateShipmentTestId"}>
                <Tooltip
                    arrow
                    title={
                        <Typography className="font-ebs text-xl">
                            {"Occurrences Bulk Delete"}
                        </Typography>
                    }
                >
                    <Button
                        onClick={handleClickOpen}
                        aria-label="occurrence-bulk-update"

                    >
                        <Delete />
                    </Button>
                </Tooltip>
                <EbsDialog
                    open={open}
                    handleClose={handleClose1}
                    maxWidth="sm"
                    title={"Bulk Delete"}
                >
                    <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkDeleteForm">
                        <DialogContent dividers>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <div>
                                        <FormControl component="fieldset">
                                            <FormLabel component="legend">Options</FormLabel>
                                            <RadioGroup
                                                arial-label="occurrenceDeleteOptionLabel"
                                                name="occurrenceDeleteOption"
                                                value={deleteOption}
                                                row={false}
                                            >
                                                <FormControlLabel
                                                    value="all"
                                                    control={<Radio onClick={handleClickDeleteOption} />}
                                                    label="Remove all occurrences."
                                                />
                                                <FormControlLabel
                                                    value="selected"
                                                    control={<Radio onClick={handleClickDeleteOption} />}
                                                    label="Remove all selected occurrences."
                                                />
                                                <FormControlLabel
                                                    value="failed"
                                                    control={<Radio onClick={handleClickDeleteOption} />}
                                                    label="Remove all occurrences with failed validation."
                                                />
                                            </RadioGroup>
                                        </FormControl>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogContent>

                        <DialogActions>
                            <Button onClick={handleClose}>
                                {"Cancel"}
                            </Button>
                            {loading ? (<CircularProgress />) : (<Button type="submit">
                                {"Apply"}
                            </Button>)}
                        </DialogActions>
                    </form>
                </EbsDialog>
            </div>
        );
    }
);

export default DeleteOccurrences;
