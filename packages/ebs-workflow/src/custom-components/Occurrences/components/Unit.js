import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyOccurrences } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const Unit = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(value?.values["unit"]);
    const [newValue, setNewValue] = useState(value?.values["unit"]);
    const [rowDataId, setRowDataId] = useState();

    useEffect(() => {
      setNewValue(value?.values["unit"]);
      setCurrentValue(value?.values["unit"]);
      setRowDataId(value?.values["id"]);
    }, [value]);

    useEffect(() => {
      if (newValue !== currentValue && newValue !== "") {
        const delayDebounceFn = setTimeout(async () => {
          let input = {...value.values}
          try {
            let _serviceId = value.values["service.id"];
            delete input["service.id"];
            delete input["recipient.person.fullName"];
            input = {...input, unit: newValue, serviceId: _serviceId },
            await modifyOccurrences(input);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 1000);

        return () => clearTimeout(delayDebounceFn);
      }
    }, [newValue]);

    const handleChangeValue = (e) => {
        setNewValue(e.target.value);
    };

    return (
      <TextField
        data-testid={"TextFieldUnitId"}
        ref={ref}
        onChange={handleChangeValue}
        value={newValue}
        variant="standard"
        inputProps={{ maxLength: 3 }}
      />
    );
  }
);
// Type and required properties
Unit.propTypes = {};
// Default properties


export default Unit;
