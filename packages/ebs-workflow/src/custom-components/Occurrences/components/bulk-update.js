import { useState, useRef } from "react";
import { Core, Icons } from "@ebs/styleguide";
const { ArrowDropDown } = Icons;
const { Tooltip, Button, Popper, ClickAwayListener, Typography,
    Grow, Paper, List, ListItem } = Core;
import RequestMessage from "components/service/alert";
import OccurrencesModalBulkUpdate from "./modal-bulk-update";

const OccurrencesBulkUpdate = ({ refresh, selection, node, columns }) => {
    const [open, setOpen] = useState(false);
    const [openMessage, setOpenMessage] = useState(false);
    const [openMenu, setOpenMenu] = useState(false);

    const handleClose = (event, reason) => {
        if (reason === "backdropClick")
            return;
        setOpen(false)
    }
    const handleCloseMenu = () => {
        setOpenMenu(false)
    }
    const anchorRef = useRef(null);
    const handleToggle = () => {
        setOpenMenu((prevOpen) => !prevOpen);
    };
    const handleBulkUpdate = () => {
        setOpen(true);
        handleToggle();
    }

    return (
        <>
            <RequestMessage open={openMessage} />
            <OccurrencesModalBulkUpdate open={open} handleClose={handleClose} refresh={refresh} selection={selection} node={node} />
            <Tooltip arrow top title={
                <Typography className="font-ebs text-xl">
                    {"Bulk update"}
                </Typography>
            }>
                <Button
                    ref={anchorRef}
                    onClick={handleToggle}
                >
                    <Typography className='text-white text-sm font-ebs'>
                        {"Update"}
                    </Typography>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <ArrowDropDown />
                </Button>
            </Tooltip>
            <Popper open={openMenu} anchorEl={anchorRef.current} transition>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleCloseMenu}>
                                <List id='split-button-menu-template'>
                                    <ListItem
                                        onClick={handleBulkUpdate}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        {"Bulk update"}
                                    </ListItem>
                                </List>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </>

    )
}
export default OccurrencesBulkUpdate;