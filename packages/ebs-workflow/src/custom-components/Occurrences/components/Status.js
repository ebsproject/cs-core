import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { Select, MenuItem, Typography } = Core;
import { modifyOccurrences } from "./functions";
// import { sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const SelectStatus = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(value?.values["status"]);
    const [newValue, setNewValue] = useState(value?.values["status"]);
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    const {service} = useServiceContext();
    useEffect(() => {
      setNewValue(value?.values["status"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["status"]);
    }, [value]);

    useEffect(() => {
      if (newValue != currentValue) {
        const delayDebounceFn = setTimeout(async () => {
          let input = {...value.values}
          try {
            let _serviceId = value.values["service.id"];
            delete input["service.id"];
            delete input["recipient.person.fullName"];
            input = {...input, status: newValue, serviceId: _serviceId },
            await modifyOccurrences(input);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 500);
        return () => {
          clearTimeout(delayDebounceFn);
        };
      }

    }, [newValue]);
    const handleChangeValue = (e) => {
      setNewValue(e.target.value);
    };


    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserSelectVolumeUnit.test.js file.
     */ <div data-testid="SelectStatusId">
        <Select
          sx={{width:'220px'}}
          labelId="select-status"
          id="select-status-id"
          value={newValue}
          label=""
          onChange={handleChangeValue}
          variant="standard"
        >
          <MenuItem value="Released">
            <Typography style={{ color: "#1609" }}>Released</Typography>
          </MenuItem>
          <MenuItem value="Rejected">
            <Typography style={{ color: "#f50" }}>Rejected</Typography>
          </MenuItem>
          <MenuItem value="Unverified">
            <Typography style={{ color: "#f50049" }}>Unverified</Typography>
          </MenuItem>
        </Select>
      </div>
    );
  }
);
SelectStatus.propTypes = {};


export default SelectStatus;
