import React, { useState, useEffect } from "react";
import { Core, EbsDialog, Lab } from "@ebs/styleguide";
const { Autocomplete } = Lab;
const { DialogContent, Box, Typography, TextField } = Core;
import UpdateRSHT from "./UpdateRSHT";
import UpdateStatus from "./UpdateStatus";
import UpdateQuantities from "./UpdateQuantities";
import { useServiceContext } from "custom-components/context/service-context";
import UpdateUnit from "./UpdateUnit";

const OccurrencesModalBulkUpdate = ({ open, handleClose, refresh, selection }) => {
    const [mode, setMode] = useState("");
    const [numberSelected, setNumberSelected] = useState(selection.length);
    const [options, setOptions] = useState(['GHT','Status','Quantities', 'Unit']);
    const { service } = useServiceContext();


    useEffect(() => {
        setMode("")
    }, [open]);

    useEffect(() => {
        setNumberSelected(selection.length)
    }, [selection]);

    const selectComponent = () => {
        switch (mode) {
            case "GHT": return (<UpdateRSHT refresh={refresh} selection={selection} handleClose={handleClose} option={"GHT"} />);
            case "Status": return (
                <UpdateStatus refresh={refresh} selection={selection} handleClose={handleClose} />
            );
            case "Quantities": return (<UpdateQuantities refresh={refresh} selection={selection} handleClose={handleClose} />)
            case "Unit": return (<UpdateUnit refresh={refresh} selection={selection} handleClose={handleClose} />)
            default: <></>
                break;
        }
    }
    return (
        <>
            <EbsDialog
                open={open}
                handleClose={handleClose}
                title={
                    <>
                        <Typography variant={"h4"}>
                            {"Bulk Update"}
                        </Typography>
                        <Typography variant={"subtitle"}>
                            {`${numberSelected} selected items`}
                        </Typography>
                    </>
                }
                maxWidth={"sm"}
            >
                <DialogContent>
                    <Typography variant={"title"}>
                        {"Select variable for bulk update"}

                    </Typography>
                    <Box>&nbsp;</Box>
                    <Autocomplete
                        id="combo-box-Options"
                        options={options || []}
                        getOptionLabel={(option) => option}
                        fullWidth
                        onChange={(e, option) => setMode(option)}
                        renderInput={(params) => <TextField {...params} label="Select a value" variant="outlined" />}
                    />
                    <Box>&nbsp;</Box>
                    <Box>
                        {
                            selectComponent()
                        }
                    </Box>
                </DialogContent>
            </EbsDialog>
        </>
    )
}
export default OccurrencesModalBulkUpdate;