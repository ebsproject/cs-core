import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyOccurrences } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const Volume = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(value?.values["weight"]);
    const [newValue, setNewValue] = useState(value?.values["weight"]);
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    const {service} = useServiceContext();
    useEffect(() => {
      setNewValue(value?.values["weight"]);
      setCurrentValue(value?.values["weight"]);
      setRowDataId(value?.values["id"]);
    }, [value]);

    useEffect(() => {
      if (newValue !== currentValue && newValue !== "") {
        const delayDebounceFn = setTimeout(async () => {
          let input = {...value.values}
          try {
            let _serviceId = value.values["service.id"];
            delete input["service.id"];
            delete input["recipient.person.fullName"];
            input = {...input, weight: newValue, serviceId: _serviceId },
            await modifyOccurrences(input);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 1000);

        return () => clearTimeout(delayDebounceFn);
      }
    }, [newValue]);

    const handleChangeValue = (e) => {
      if (e.target.value > 0)
        setNewValue(e.target.value);
    };

    return (
      <TextField
        data-testid={"TextFieldVolumeId"}
        ref={ref}
        onChange={handleChangeValue}
        value={newValue}
        variant="standard"
        type="number"
      />
    );
  }
);
// Type and required properties
Volume.propTypes = {};
// Default properties


export default Volume;
