import DeleteOccurrence from "./delete-occurrence";
import { cleanup, findAllByTestId, fireEvent, render, screen, waitFor } from "@testing-library/react";
import { actionDeleteDocument, deleteFile } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context";
import { useDispatch } from "react-redux";
import { deleteOccurrence } from "../functions";
import { sendMessage } from "custom-components/Items/item-components/functions";


jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useDispatch: jest.fn(),
}));

jest.mock('../functions', () => ({
    deleteOccurrence: jest.fn(),
}));
jest.mock('custom-components/Items/item-components/functions', () => ({
    sendMessage: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));
jest.mock("components/alerts/alert-context", () => ({
    useAlertContext: jest.fn(),
}));
const rowDataMock = { id: 1, name: "Test Code" }
const mockId = 1;
const setAlertMock = jest.fn();

describe("Delete Single Document component", () => {

afterEach(cleanup)
    beforeEach(() => {
        jest.clearAllMocks();
        useAlertContext.mockReturnValue({
            setAlerts: setAlertMock,
        });

    });

    const renderComponent = () => render(
        <DeleteOccurrence rowData={rowDataMock} id={mockId} />
    );
    
    test("Render de Icon Button in DOM", () => {
        renderComponent();
        expect(screen.getByTestId("DeleteOccurrenceItemTestId")).toBeInTheDocument();
    });

    test("open dialog modal when click in button", () => {
       renderComponent();
       const button = screen.getByTestId("DeleteIconButtonTestId");
       fireEvent.click(button);
       expect(screen.getByText(/The occurrence will be deleted permanently/i)).toBeInTheDocument();

    });

    test('closes dialog on Cancel button click', async () => {
        renderComponent();
        fireEvent.click(screen.getByTestId("DeleteIconButtonTestId"));
        fireEvent.click(screen.getByTestId('DeleteOccurrenceButtonCancelTestId'));
        await waitFor(() => {
            expect(screen.queryByText(/The occurrence will be deleted permanently/i)).not.toBeInTheDocument();
        })
    });
    test("call submit when click save button", async ()=>{
        deleteOccurrence.mockResolvedValue();
        sendMessage.mockResolvedValue();
        renderComponent();
        fireEvent.click(screen.getByTestId("DeleteIconButtonTestId"));
        fireEvent.click(screen.getByTestId("DeleteOccurrenceButtonSaveTestId"));
        
        await waitFor(() => {
            expect(screen.queryByText(/The occurrence will be deleted permanently/i)).not.toBeInTheDocument();
        })
    })
   

})