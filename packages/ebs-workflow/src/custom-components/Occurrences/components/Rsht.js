import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyOccurrences } from "./functions";
// import { sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const Rsht = React.forwardRef(({ value, setRefreshTable }, ref) => {
  const [currentValue, setCurrentValue] = useState(value?.values["testCode"]);
  const [newValue, setNewValue] = useState(value?.values["testCode"]);
  const [rowDataId, setRowDataId] = useState();
  const dispatch = useDispatch();
  const {service} = useServiceContext();
  useEffect(() => {
      setNewValue(value?.values["testCode"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["testCode"]);
  }, [value]);

  useEffect(() => {
    const delayDebounceFn = setTimeout(async () => {
      if (newValue != undefined && newValue != currentValue) {
        let input = {...value.values}
        try {
          let _serviceId = value.values["service.id"];
          delete input["service.id"];
          delete input["recipient.person.fullName"];
          input = {...input, testCode: newValue, serviceId: _serviceId },
          await modifyOccurrences(input);
          setRefreshTable(true);
        } catch (error) {
          console.log(error)
        }
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [newValue]);

  const handleChangeValue = (e) => {
    setNewValue(e.target.value);
  };
  return (
    /* 
   @prop data-testid: Id to use inside ShipmentItemBrowserTextFieldRshtNumber.test.js file.
   */
    <TextField
      data-testid={"TextFieldRshtNumberId"}
      ref={ref}
      value={newValue}
      onChange={handleChangeValue}
      type="text"
      variant="standard"
    />
  );
});
// Type and required properties
Rsht.propTypes = {};
// Default properties


export default Rsht;
