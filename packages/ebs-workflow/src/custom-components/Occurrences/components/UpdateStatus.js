import React, { useState } from "react";
import { Core, Lab } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
import { modifyOccurrences } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
import { GET_ALL_OCCURRENCES_BY_SERVICE_ID } from "../gql";
import { client } from "utils/apollo";
import { sendMessage } from "custom-components/Items/item-components/functions";
const {
    Grid,
    Button,
    TextField,
    Typography,
    Box,
    CircularProgress,
} = Core;

const { Autocomplete } = Lab;

const UpdateStatus = React.forwardRef(
    ({ refresh, selection, handleClose }, ref) => {
        const [selectionOption, setSelectionOption] = useState("Selected");
        const { rowData } = useServiceContext();
        const [loading, setLoading] = useState(false);
        const dispatch = useDispatch()
        const [status] = useState([
            "Unverified",
            "Released",
            "Rejected",
        ]);

        const {
            control,
            handleSubmit,
            formState: { errors },
            reset,
            setValue,
        } = useForm({
            defaultValues: {
                status: "Released",
            },
        });
        const onSubmit = async (data) => {
            if(!rowData) return;
            setLoading(true);
            if (selectionOption === "All") {
                let occurrences = [];
                try {
                    const { data } = await client.query({
                        query: GET_ALL_OCCURRENCES_BY_SERVICE_ID,
                        variables: {
                            filters: [{ col: "service.id", mod: "EQ", val: rowData.id }]
                        }
                    })
                    occurrences = [...data.findOccurrenceShipmentList.content]
                } catch (error) {
                    console.log(error);
                    return;
                }
                for (let i = 0; i < occurrences.length; i++) {
                    let newInput = { ...occurrences[i] }
                    newInput = { ...newInput, serviceId: rowData.id, status: data.status }
                    await modifyOccurrences(newInput);
                }
                sendMessage("success", "The status was updated for all occurrences",dispatch);
                refresh();
                handleClose();
                reset();
                setLoading(false);
            } else {
                if (selection.length > 0) {
                    for (let i =0; i < selection.length; i ++){
                        let newInput = {...selection[i]};
                        delete newInput['service'];
                        delete newInput['recipient']
                        newInput = {...newInput, status: data.status, serviceId: rowData.id}
                        await modifyOccurrences(newInput);
                    }
                    sendMessage("success", "The status was updated for selected occurrences",dispatch);
                    handleClose();
                    refresh();
                    setLoading(false);
                    reset();
                } else {
                    sendMessage("error", "No occurrences selected for updating",dispatch);
                    setLoading(false);
                }
            }
        };
        return (
            /* 
           @prop data-testid: Id to use inside ShipmentItemBrowserButtonBulkUpdate.test.js file.
           */
            <div ref={ref} data-testid={"ItemStatusTestId"}>
                <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateForm">
                    <Grid container>
                        <Grid item xs={12} style={{ paddingBottom: "20px" }}>
                            <Typography variant={"h5"}>
                                {`What status would you like to apply`}
                            </Typography>
                            <Controller
                                control={control}
                                name="status"
                                render={({ field: { value } }) => (
                                    <Autocomplete
                                        onChange={(event, options) => {
                                            setValue("status", options)
                                        }}
                                        value={value}
                                        options={status}
                                        autoHighlight
                                        getOptionLabel={(item) => (item ? item : "")}
                                        isOptionEqualToValue={(option, value) =>
                                            value === undefined ||
                                            value === "" ||
                                            option === value
                                        }
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                label="Select a value"
                                                variant="outlined"
                                                size="normal"
                                                fullWidth
                                                error={Boolean(errors["status"])}
                                                helperText={errors["status"]?.message}
                                            />
                                        )}
                                    />
                                )}
                                rules={{
                                    required: (
                                        <Typography>
                                            {"Please select a status"}
                                        </Typography>
                                    ),
                                }}
                            />
                        </Grid>
                    </Grid>
                    <Box display="flex" justifyContent="flex-end">
                        <Button onClick={handleClose}>
                            {"Cancel"}
                        </Button>
                        {loading ? (<CircularProgress />) : (
                            <div>
                                <Grid container spacing={2}>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => setSelectionOption("Selected")} type="submit">
                                            {"Apply to selected"}
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => setSelectionOption("All")} type="submit">
                                            {"Apply to all"}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                        )}
                    </Box>
                </form>
            </div>
        );
    }
);
// Type and required properties
UpdateStatus.propTypes = {};
// Default properties


export default UpdateStatus;
