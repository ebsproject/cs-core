import React, { useState } from "react";
// GLOBALIZATION COMPONENT
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { sendMessage } from "custom-components/Items/item-components/functions";
import { deleteOccurrence } from "../functions";
// CORE COMPONENTS
const {
    IconButton,
    Tooltip,
    Button,
    Typography,
    DialogActions,
    DialogContent
} = Core;
const { Delete } = Icons;

const DeleteOccurrence = ({ refresh, id }) => {

        const [open, setOpen] = useState(false);
        const dispatch = useDispatch();
        const handleClickOpen = () => setOpen(true);
        const handleClose = () => setOpen(false);


        const submit = async () => {
            try {
                await deleteOccurrence({ id, entity: "OccurrenceShipment" });
                sendMessage('success', 'The occurrence was deleted successfully', dispatch);
                handleClose();
                refresh();

            } catch (error) { handleClose(); }
        };

        return (
            /* 
           @prop data-testid: Id to use inside ShipmentItemBrowserButtonDelete.test.js file.
           */
            <div data-testid={"DeleteOccurrenceItemTestId"}>
                <Tooltip
                    arrow
                    title={
                        <Typography className="font-ebs text-xl">
                            {"Delete Occurrence"}
                        </Typography>
                    }
                >
                    <IconButton
                    data-testid={"DeleteIconButtonTestId"}
                        onClick={handleClickOpen}
                        aria-label="delete-shipment-occurrence"
                        color="primary"
                    >
                        <Delete />
                    </IconButton>
                </Tooltip>
                <EbsDialog
                    open={open}
                    handleClose={handleClose}
                    maxWidth="sm"
                    title={
                        "Delete Occurrence"
                    }
                >
                    <DialogContent>
                        {"The occurrence will be deleted permanently"}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} data-testid={"DeleteOccurrenceButtonCancelTestId"}>
                            {"Cancel"}
                        </Button>
                        <Button onClick={submit} data-testid={"DeleteOccurrenceButtonSaveTestId"}>
                           {"Delete"}
                        </Button>
                    </DialogActions>
                </EbsDialog>
            </div>
        );
    };

export default DeleteOccurrence;
