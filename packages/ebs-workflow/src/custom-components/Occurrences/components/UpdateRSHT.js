import React, { useState } from "react";
import { Core } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { modifyOccurrences } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { GET_ALL_OCCURRENCES_BY_SERVICE_ID } from "../gql";
import { client } from "utils/apollo";
import { sendMessage } from "custom-components/Items/item-components/functions";
// CORE COMPONENTS
const {
  Radio,
  RadioGroup,
  Button,
  Grid,
  FormControl,
  FormControlLabel,
  TextField,
  Typography,
  CircularProgress,
  Box,
} = Core;
//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const UpdateRSHT = React.forwardRef(
  ({ refresh, handleClose, selection,option }, ref) => {
    const [loading, setLoading] = useState(false);
    const [selectionOption, setSelectionOption] = useState("Selected");
    const [showLabel, setShowLabel] = useState(false);
    const dispatch = useDispatch();
    const { rowData } = useServiceContext();
    const initValue = null;
    const { control, handleSubmit, formState: { errors },reset} = useForm();
    const [action, setAction] = React.useState("manually");
    const handleChange = (event) => {
      setAction(event.target.value);
    };
    const onSubmit = async (data) => {
      if(!rowData) return;
      setLoading(true);
      if (selectionOption === "All") {
          let occurrences = [];
          try {
              const { data } = await client.query({
                  query: GET_ALL_OCCURRENCES_BY_SERVICE_ID,
                  variables: {
                      filters: [{ col: "service.id", mod: "EQ", val: rowData.id }]
                  }
              })
              occurrences = [...data.findOccurrenceShipmentList.content]
          } catch (error) {
              console.log(error);
              return;
          }
          for (let i = 0; i < occurrences.length; i++) {
              let newInput = { ...occurrences[i] }
              newInput = { ...newInput, serviceId: rowData.id, testCode: data.rsht }
              await modifyOccurrences(newInput);
          }
          sendMessage("success", "The GHT was updated for all occurrences",dispatch);
          refresh();
          handleClose();
          reset();
          setLoading(false);
      } else {
          if (selection.length > 0) {
              for (let i =0; i < selection.length; i ++){
                  let newInput = {...selection[i]};
                  delete newInput['service'];
                  delete newInput['recipient']
                  newInput = {...newInput, testCode: data.rsht, serviceId: rowData.id}
                  await modifyOccurrences(newInput);
              }
              sendMessage("success", "The GHT was updated for selected occurrences",dispatch)
              handleClose();
              refresh();
              setLoading(false);
              reset();
          } else {
            sendMessage("error", "No occurrences selected for updating",dispatch);
              setLoading(false);
          }
      }
  };
    return (
      <div data-testid="ShipmentBrowserButtonRefreshTestId">
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid container>
            <Grid item xs={12}>
              <FormControl componet="fieldset">
                <RadioGroup
                  name="row-radio-buttons-group"
                  value={action}
                  onChange={handleChange}
                >
                  <FormControlLabel
                    value="manually"
                    control={<Radio />}
                    label="Update manually"
                  />
                  <Controller
                    name="rsht"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        variant={"outlined"}
                        disabled={action !== "api" ? false : true}
                        {...field}
                        value={initValue}
                        label={"Enter value"}
                        data-testid={"rsht"}
                      />
                    )}
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Box display="flex" justifyContent="flex-end">
                <Button onClick={handleClose}>
                  {"Cancel"}
                </Button>
                {loading ? (<CircularProgress />) : (
                  <div>
                    <Grid container spacing={2}>
                      <Grid item>
                        <Button
                          disabled={action === "api" ? true : false}
                          onClick={() => setSelectionOption("Selected")} type="submit">
                          {"Apply to selected"}
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                         
                          onClick={() => setSelectionOption("All")} type="submit">
                          {action === "api" ? "start process" : "Apply to all"}
                        </Button>
                      </Grid>
                    </Grid>
                  </div>
                )}
              </Box>
            </Grid>
          </Grid>
          <br />
        </form>
      </div>
    );
  }
);
// Type and required properties
UpdateRSHT.propTypes = {};
// Default properties


export default UpdateRSHT;
