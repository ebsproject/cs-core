import { getTokenId, getCoreSystemContext } from '@ebs/layout';
import { client } from 'utils/apollo';
import axios from 'axios';
import { MODIFY_OCCURRENCES } from '../gql';
export async function uploadFile(files, service) {
    const { graphqlUri } = getCoreSystemContext();
    const serviceUrl = graphqlUri.replace("graphql", "");
    let headers = {
        Accept: "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${getTokenId()}`
    }
    let formData = new FormData();
    let path = 'importFiles/occurrenceShipment';
    try {
        let _file = files[0];
        headers = { ...headers, 'Content-Type': 'multipart/form-data', };
        formData.append('file', _file);
        formData.append('serviceId', service.id);

        const { data } = await axios.request({
            url: new URL(path, serviceUrl).toString(),
            method: 'post',
            headers: headers,
            data: formData
        });
        return {failed:false }
    } catch(error) {
      console.error(error)
      return{failed:true, error:error}
    }
}
export async function modifyOccurrences(input) {
try {
  const {data} =  await client.mutate({
    mutation: MODIFY_OCCURRENCES,
    variables:{occurrenceShipment: input}
    })
} catch (error) {
    console.log(error)
}

    
}