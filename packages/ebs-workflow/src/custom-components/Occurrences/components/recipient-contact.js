import React, { useState, useEffect, useContext } from "react";
import { Core } from "@ebs/styleguide";
const { Autocomplete, TextField, Checkbox } = Core;
import { sendMessage } from "custom-components/Items/item-components/functions";
import { useDispatch } from "react-redux";
import { modifyOccurrences } from "../functions";
import { useServiceContext } from "custom-components/context/service-context";

const RecipientDropdown = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const recipient =value && value.original["recipient"]
    const { contactList } = useServiceContext();
    const [currentValue, setCurrentValue] = useState(recipient);
    const [newValue, setNewValue] = useState(recipient);
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    useEffect(() => {
      setNewValue(recipient);
      setRowDataId(value?.values["id"]);
      setCurrentValue(recipient);
    }, [value]);

    useEffect(() => {
      if (newValue.person.fullName != currentValue.person.fullName) {
        const delayDebounceFn = setTimeout(async () => {
          try {
            let input = 
              {
                id: rowDataId,
                serviceId:value.values["service.id"],
                occurrenceId:value.values["occurrenceDbId"],
                occurrenceNumber:value.values["occurrenceNumber"],
                occurrenceName:value.values["occurrenceName"],
                experimentCode: value.values["experimentCode"],
                experimentYear: value.values["experimentYear"],
                experimentSeason: value.values["experimentSeason"],
                experimentName: value.values["experimentName"],
                coopkey: value.values["coopkey"],                
                recipientId: newValue.id,
              }
           
             await modifyOccurrences(input);
            sendMessage("success", "The selected occurrence was updated with a new recipient", dispatch);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 500);
        return () => {
          clearTimeout(delayDebounceFn);
        };
      }

    }, [newValue]);
    const renderInput = (params) => (
      <TextField
      {...params}
        label={""}
        variant="standard"
      />
    );

    const optionLabel = (option) => `${option.person.fullName}`;
  
    const onChange = (e, option) => {
      return;
      if(!option) return;
      setNewValue(option);
    };
    const renderOption = (props,option, { selected }) => {
      const { key, ...restProps  } = props;
      return (
        <li key={key} {...restProps}>
          <Checkbox checked={selected} />
          {option.person.fullName}
        </li>
      );
    };
   if(!contactList) return "Loading...."
    return (
       <div data-testid="RecipientDropdownId">
        <Autocomplete
          value={newValue}
          isOptionEqualToValue={(option, value) => option.person.fullName === value.person.fullName}
          options={contactList}
          onChange={onChange}
          getOptionLabel={optionLabel}
          renderInput={renderInput}
          renderOption={renderOption}
          sx={{width: 250}}
        />
      </div>
    );
  }
);
RecipientDropdown.propTypes = {};


export default RecipientDropdown;
