import { Core } from "@ebs/styleguide";
const { Box, Grid } = Core;
import { RBAC } from "@ebs/layout";
import ReImportCSVFile from "./components/reimport-csv-file";
import OccurrencesBulkUpdate from "./components/bulk-update";
import DeleteOccurrences from "./components/DeleteOccurrences";



const ToolbarActions = ({ refresh, refreshTable, setRefreshTable, selection, node, columns }) => {
  return (
    <Grid container direction="row">
      <Grid item>
        <ReImportCSVFile refresh={refresh} refreshTable={refreshTable} setRefreshTable={setRefreshTable} />
      </Grid>
      <Grid item>
        <OccurrencesBulkUpdate refresh={refresh} selection={selection} />
      </Grid>
      <Grid item>
        <DeleteOccurrences  refresh={refresh} selection={selection} />
      </Grid>
    </Grid>

  );
};
export default ToolbarActions