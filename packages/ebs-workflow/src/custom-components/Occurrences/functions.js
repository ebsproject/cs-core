import { FIND_CONTACT_LIST } from "utils/apollo/gql/catalogs";
import { buildDeleteOCC, MODIFY_OCCURRENCES } from "./gql";
import { client } from "utils/apollo";

export const deleteOccurrence = async (props) => {
    const { id, entity } = props;
    try {
      const MUTATION = buildDeleteOCC(entity);

      await client.mutate({
        mutation: MUTATION,
        variables: { id: id },
      });
    } catch (error) {
      console.error(error);
    }
  };


  export async function getAllContacts() {

    let totalData = [];
    let number = 1;
    const { data } = await client.query({
      query: FIND_CONTACT_LIST,
      variables: {
        filters:[{col:"category.name", val:"Person", mod:"EQ"}],
        page: { number: 1, size: 100 }
      },
    });
    number = data.findContactList.totalPages;
  
    totalData = [...totalData, ...data.findContactList.content];
    if (number > 1) {
      for (let i = 2; i <= number; i++) {
        const { data } = await client.query({
          query: FIND_CONTACT_LIST,
          variables: {
            filters:[{col:"category.name", val:"Person", mod:"EQ"}],
            page: { number: i, size: 100 }
          },
        });
        totalData = [...totalData, ...data.findContactList.content];
      }
    }
    return totalData;
  }
  export const modifyOccurrences = async (input)=>{
    try {
      await client.mutate({
        mutation:MODIFY_OCCURRENCES,
        variables:{occurrenceShipment: input } ,
      });
    } catch (error) {
      console.error(error);
    }
  }