
import { Core } from "@ebs/styleguide";
const { Box } = Core;
import { RBAC } from "@ebs/layout";
import DeleteOccurrence from "./components/delete-occurrence";

const RowActions = (rowData, refresh) => {
  return (
    <Box display="flex" flexDirection="row">
      <RBAC allowedAction={"Delete"}>
        <Box>
          <DeleteOccurrence id={rowData.id} refresh={refresh} />
        </Box>
      </RBAC>

    </Box>
  );
};
export default RowActions;