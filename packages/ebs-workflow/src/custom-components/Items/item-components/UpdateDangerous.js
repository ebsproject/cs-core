import React, { useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { modifyItems, sendMessage } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
const {
    Grid,
    Button,
    Box,
    CircularProgress,
    FormControlLabel,
    FormControl,
    Radio,
    RadioGroup,
} = Core;
const UpdateDangerous = React.forwardRef(({ refresh, selection, handleClose }, ref) => {

    const [selectionOption, setSelectionOption] = useState("Selected");
    const dispatch = useDispatch();
    const { service } = useServiceContext();
    const [isDangerous, setIsDangerous] = useState(false);

    const [loading, setLoading] = useState(false);

    const handleChangeValue = (e) => {
        const value = e.target.value === "Yes" ? true : false;
        setIsDangerous(value);
    };
    const { handleSubmit, } = useForm();
    const onSubmit = async (data) => {
        setLoading(true);
        if (selectionOption === "All") {
            let input = [{
                id: 0,
                isDangerous: isDangerous
            }];
            await modifyItems({ input, service, id: service.id })
            refresh();
            handleClose();
            sendMessage("success", "All items were updated successfully", dispatch)
            setLoading(false);
        } else {
            if (selection.length > 0) {
                let input = [];
                selection.forEach((item) => {
                    input = [...input, {
                        id: item.id,
                        isDangerous: isDangerous

                    }];
                });
                await modifyItems({ input, service });
                sendMessage("success", "Selection was updated successfully", dispatch)
                handleClose();
                refresh();
                setLoading(false);
            } else {
                // showMessage("error", "No items selected for updating", dispatch)
                setLoading(false);
            }
        }
    };
    return (
        <div ref={ref} data-testid={"BulkUpdateShipmentTestId"}>
              <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateForm">
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                    <FormControl component="fieldset">
                <RadioGroup
                  name="row-radio-buttons-group"
                  value={isDangerous ? "Yes" : "No"}
                  onChange={handleChangeValue}
                >
                  <FormControlLabel
                    value="Yes"
                    control={<Radio />}
                    label="Mark as Dangerous"
                  />
                  <FormControlLabel
                    value="No"
                    control={<Radio />}
                    label="Mark as Not Dangerous"
                  />

                </RadioGroup>
              </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <Box display="flex" justifyContent="flex-end" >
                            <Button onClick={handleClose}>
                                {"Cancel"}
                            </Button>
                            {loading ? (<CircularProgress />) : (
                                <div>
                                    <Grid container spacing={2}>
                                        <Grid item>
                                            <Button
                                                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                                onClick={() => setSelectionOption("Selected")} type="submit">
                                                {"Apply to selected"}
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                                onClick={() => setSelectionOption("All")} type="submit">
                                                {"Apply to all"}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </div>
                            )}
                        </Box>
                    </Grid>
                </Grid>
                </form>
        </div>
    );
}
);
// Type and required properties
UpdateDangerous.propTypes = {};
// Default properties


export default UpdateDangerous;
