import React, { useState, useEffect } from "react";

import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyItems, sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const PackageQty = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(
      value?.values["packageCount"]
    );
    const [newValue, setNewValue] = useState(value?.values["packageCount"]);
    const [rowDataId, setRowDataId] = useState();
    const dispatch =  useDispatch();
    const{service} = useServiceContext();
    useEffect(() => {
      setNewValue(value?.values["packageCount"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["packageCount"]);
    }, [value]);
    useEffect(() => {
      if(newValue !== currentValue && newValue !== ""){
        const delayDebounceFn = setTimeout( async () => {
          try {
            let input = [
              {
                id: rowDataId,
                packageCount: newValue,
              },
            ];
            await modifyItems({input, service});
            sendMessage("success", "The selected item was updated", dispatch);
                   setRefreshTable(true)
          } catch ({ message }) {
          } finally {
          }
        }, 1000);
        return () => clearTimeout(delayDebounceFn);
      }
    }, [newValue]);

    const handleChangeValue = (e) => {
      if(e.target.value > 0)
         setNewValue(e.target.value);
    };

    return (
      <TextField
        data-testid={"PackageQtyId"}
        ref={ref}
        value={newValue}
        autoComplete="off"
        type="number"
        onChange={handleChangeValue}
        variant="outlined"
      />
    );
  }
);

PackageQty.propTypes = {};

export default PackageQty;
