import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyItems } from "./functions";
import { sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const ReferenceNumber = React.forwardRef(({ value, setRefreshTable }, ref) => {
  const [currentValue, setCurrentValue] = useState(value?.values["referenceNumber"]);
  const [newValue, setNewValue] = useState(value?.values["referenceNumber"]);
  const [rowDataId, setRowDataId] = useState();
  const dispatch = useDispatch();
  const {service} = useServiceContext();
  useEffect(() => {
      setNewValue(value?.values["referenceNumber"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["referenceNumber"]);
  }, [value]);

  useEffect(() => {
    const delayDebounceFn = setTimeout(async () => {
      if (newValue != undefined && newValue != currentValue) {
        try {
          let input = [{
            id: rowDataId,
            referenceNumber: newValue,
          }];
          await modifyItems({input, service});
          sendMessage("success", "The selected item was updated", dispatch);
          setRefreshTable(true);
        } catch (error) {
          console.error(error);
        }
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [newValue]);

  const handleChangeValue = (e) => {
    setNewValue(e.target.value);
  };
  return (
    <TextField
      data-testid={"TextFieldReferenceNumberId"}
      ref={ref}
      value={newValue}
      onChange={handleChangeValue}
      type="text"
      variant="outlined"
    />
  );
});
// Type and required properties
ReferenceNumber.propTypes = {};
// Default properties


export default ReferenceNumber;
