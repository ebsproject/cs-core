import { useState, useRef, useContext } from "react";
import { Core, Icons, DropzoneDialog } from "@ebs/styleguide";
const { ArrowDropDown } = Icons;
const { Tooltip, Button, Popper, ClickAwayListener, Typography,
    Grow, Paper, Grid, List, ListItem, ButtonGroup } = Core;
import ModalBulkUpdate from "./ModalBulkUpdate";
import PackageReplace from "./PackageReplace";
import { downloadCSVData, downloadItemsCSVData, formattedDate, sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { reorderItems } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import convertXLSXFile from "./functions";
import { modifyItems } from "./functions";
import RequestMessage from "components/service/alert";
import { useAlertContext } from "components/alerts/alert-context";

const BulkUpdate = ({ refresh, selection, node, columns }) => {
    const [open, setOpen] = useState(false);
    const [openMessage, setOpenMessage] = useState(false);
    const [openMenu, setOpenMenu] = useState(false);
    const [openReplace, setOpenReplace] = useState(false);
    const {setAlerts} = useAlertContext();
    const dispatch = useDispatch();
    const { service, rowData } = useServiceContext();
    const [openFileUpload, setOpenFileUpload] = useState(false);
    const handleClose = (event, reason) => {
        if (reason === "backdropClick")
            return;
        setOpen(false)
    }
    const handleCloseMenu = () => {
        setOpenMenu(false)
    }
    const anchorRef = useRef(null);
    const handleToggle = () => {
        setOpenMenu((prevOpen) => !prevOpen);
    };
    const handleBulkUpdate = () => {
        setOpen(true);
        handleToggle();
    }

    const handleReorder = async () => {
        await reorderItems(service.id, service.entity);
        refresh();
        handleToggle()
    }
    const handleReplace = () => {
        if (selection.length > 0) {
            setOpenReplace(true)
            handleToggle()
        } else {
            setAlerts((prev) => [...prev, {id:'error-no-item-selected', message: "Please select an item first.", severity:'error'}]);
        }
    }
    const handleCloseReplace = () => {
        setOpenReplace(false);
    }
    const handleCloseFileModal = () => {
        setOpenFileUpload(false);
    }
    const onSubmitFileAction = async (files) => {
        handleCloseFileModal(false);
        setOpenMessage(true);
        const file = files[0];
        convertXLSXFile(columns, file, async (data) => {
            await modifyItems({ input: data, service });
            setOpenMessage(false);
            refresh();
        });
    }
    const handleDownload = async () => {
        let date = formattedDate();
        const fileName = `${rowData.requestCode}-${date}`;
        const obj = {
            id: service.id,
            selectedRows: selection,
            columns,
            fileName
        }

        setOpenMessage(true);
        handleToggle();
        await downloadItemsCSVData(obj)
        setOpenMessage(false);
    }
    const handleUpload = () => {
        handleToggle();
        setOpenFileUpload(true);
    }
    return (
        <>
               <DropzoneDialog
                acceptedFiles={[".csv",".xlsx"]}
                cancelButtonText={"cancel"}
                submitButtonText={"submit"}
                maxFileSize={5000000}
                open={openFileUpload}
                onClose={handleCloseFileModal}
                onSave={onSubmitFileAction}
                showPreviews={true}
                showFileNamesInPreview={true}
            />

            <RequestMessage open={openMessage} />
            <PackageReplace open={openReplace} handleClose={handleCloseReplace} row={selection[0]} refresh={refresh} />
            <ModalBulkUpdate open={open} handleClose={handleClose} refresh={refresh} selection={selection} node={node} />
            <Tooltip arrow placement={"top"} title={
                <Typography >
                    {"Bulk update"}
                </Typography>
            }>

                <Button
                    ref={anchorRef}
                    onClick={handleToggle}
                >
                    <Typography className='text-white text-sm font-ebs'>
                        {"Update"}
                    </Typography>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <ArrowDropDown />
                </Button>
            </Tooltip>
            <Popper open={openMenu} anchorEl={anchorRef.current} transition>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleCloseMenu}>
                                <List id='split-button-menu-template'>
                                    <ListItem
                                        onClick={handleBulkUpdate}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        {"Bulk update"}
                                    </ListItem>
                                    <ListItem
                                        onClick={handleReorder}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        {"Reorder Items"}

                                    </ListItem>
                                    <ListItem
                                        onClick={handleReplace}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        {"Replace Package"}
                                    </ListItem>
                                    <ListItem
                                        onClick={handleDownload}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        {"Download CSV Items template"}
                                    </ListItem>
                                    <ListItem
                                        onClick={handleUpload}
                                        style={{ cursor: 'pointer' }}
                                    >
                                        {"Upload CSV Items Template"}
                                    </ListItem>
                                </List>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </>

    )
}
export default BulkUpdate;