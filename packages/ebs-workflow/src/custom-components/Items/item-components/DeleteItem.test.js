import DeleteItem from "./DeleteItem";
import { render, cleanup, screen, fireEvent, act, waitFor } from "@testing-library/react";
import "@testing-library/dom";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
import { modifyItems, reorderItems, sendMessage, deleteItem } from "./functions";


jest.mock("./functions", () => ({
  modifyItems: jest.fn(),
  reorderItems: jest.fn(),
  sendMessage: jest.fn(),
  deleteItem: jest.fn()
}))

jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));
jest.mock('react-intl', () => ({
  FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));
jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn((service) => service),
}));

describe("Delete Item component", () => {
  const refreshMock = jest.fn();;
  const mockId = 1;

  beforeEach(() => {
    useServiceContext.mockReturnValue({ service: { id: 1 } });
    jest.clearAllMocks();
  });

  const renderComponent = () => render(
    <DeleteItem refresh={refreshMock} id={mockId} />
  )

  test("delete icon button is in the DOM", () => {
    renderComponent();
    expect(screen.getByTestId("DeleteShipmentItemTestId")).toBeInTheDocument();
  });

  test("open modal when click button", () => {
    renderComponent();
    const button = screen.getByTestId("ButtonOpenDialogTestId");
    fireEvent.click(button);
    expect(screen.getByText(/Delete Shipment Item/i)).toBeInTheDocument();
    expect(screen.getByText(/Mark item only as rejected/i)).toBeInTheDocument();
    expect(screen.getByText(/Delete item permanently/i)).toBeInTheDocument();
    expect(screen.getByText(/Mark as rejected/i)).toBeInTheDocument();
  });

  test("click in removed mode", async() => {
    act(()=>{
      renderComponent();
    })
await waitFor(()=>{
  const button = screen.getByTestId("ButtonOpenDialogTestId");
  fireEvent.click(button);
  const radioButtonRemoved = screen.getByTestId("RemovedRadioTestId");
  fireEvent.click(radioButtonRemoved);
  const buttonSubmit = screen.getByTestId("SubmitDeleteButtonTestId");
  fireEvent.click(buttonSubmit);
  expect(modifyItems).toHaveBeenCalled();
})


  });
  
  test("click in delete mode", async() => {
    
    act(()=>{
      renderComponent();
    })
    await waitFor(()=>{
      const button = screen.getByTestId("ButtonOpenDialogTestId");
      fireEvent.click(button);
      const radioButtonDelete = screen.getByTestId("DeleteRadioTestId");
      fireEvent.click(radioButtonDelete);
      const buttonSubmit = screen.getByTestId("SubmitDeleteButtonTestId");
      fireEvent.click(buttonSubmit);
      expect(deleteItem).toHaveBeenCalled();
    })

  });


})
