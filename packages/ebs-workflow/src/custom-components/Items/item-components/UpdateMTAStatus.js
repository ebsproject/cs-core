import React, { useState } from "react";
import { Core, Lab } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
import { modifyItems, sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
const {
    Grid,
    Button,
    TextField,
    Typography,
    Box,
    CircularProgress,
} = Core;

const { Autocomplete } = Lab;

const UpdateMTAStatus = React.forwardRef(
    ({ refresh, selection, handleClose }, ref) => {

        const [selectionOption, setSelectionOption] = useState("Selected");
        const { service } = useServiceContext();
        const [loading, setLoading] = useState(false);
        const dispatch = useDispatch()
        const [status] = useState([
            {label:"PUD1",value:"PUD1"},
            {label:"PUD2",value:"PUD2"},
            {label:"Rel1",value:"Rel1"},
            {label:"MLS",value:"MLS"},
            {label:"FAO",value:"FAO"},
            {label:"SMTA",value:"SMTA"},
            {label:"None",value:""},
            {label:"NA",value:"NA"}
        ]);

        const {
            control,
            handleSubmit,
            formState: { errors },
            reset,
            setValue,
        } = useForm({
            defaultValues: {
                volume: null,
                packageCount: null,
            },
        });

        const onSubmit = async (data) => {
            setLoading(true)
            if (selectionOption === "All") {
                let input = {
                    id: 0,
                    mtaStatus: data.mtaStatus
                }
                await modifyItems({ input, service, id: service.id })
                sendMessage('success', "Bulk updating MTA status successfully", dispatch);
                setLoading(false)
                handleClose();
                refresh();
                reset();
            } else {
                if (selection.length > 0) {
                    let input = [];
                    for (let i = 0; i < selection.length; i++) {
                        input = [...input, { id: selection[i].id, mtaSatus: data.mtaSatus }];
                    }
                    await modifyItems({ input, service });
                    sendMessage('success', "Selection updating MTA status successfully", dispatch);
                    setLoading(false)
                    handleClose();
                    refresh();
                    reset();
                } else {
                    sendMessage('error', "No shipment item selected for updating", dispatch);
                    setLoading(false)
                }
            }

        };
        return (
            /* 
           @prop data-testid: Id to use inside ShipmentItemBrowserButtonBulkUpdate.test.js file.
           */
            <div ref={ref} data-testid={"ItemMTAStatusTestId"}>
                <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateFormMTA">
                    <Grid container>
                        <Grid item xs={12} style={{ paddingBottom: "20px" }}>
                            <Typography variant={"h5"}>
                                {`What MTA status would you like to apply`}
                            </Typography>
                            <Controller
                                control={control}
                                name="mtaStatus"
                                render={({ field: { onChange, value } }) => (
                                    <Autocomplete
                                        onChange={(event, options) => {
                                            if(!options){
                                                setValue("mtaStatus", "");
                                                onChange("");
                                            }else{
                                                setValue("mtaStatus", options.value);
                                                onChange(options.value);
                                            }
                                        }}
                                        value={value}
                                        options={status}
                                        autoHighlight
                                        getOptionLabel={(item) => (item ? item.label : "")}
                                        isOptionEqualToValue={(option, value) =>
                                            value === undefined ||
                                            value === "" ||
                                            option.label === value
                                        }
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                label="Select a value"
                                                variant="outlined"
                                                size="normal"
                                                fullWidth
                                                error={Boolean(errors["mtaStatus"])}
                                                helperText={errors["mtaStatus"]?.message}
                                            />
                                        )}
                                    />
                                )}
                                rules={{
                                    required: (
                                        <Typography>
                                            {"Please select a MTA status"}
                                        </Typography>
                                    ),
                                }}
                            />
                        </Grid>
                    </Grid>
                    <Box display="flex" justifyContent="flex-end">
                        <Button onClick={handleClose}>
                            {"Cancel"}
                        </Button>
                        {loading ? (<CircularProgress />) : (
                            <div>
                                <Grid container spacing={2}>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => setSelectionOption("Selected")} type="submit">
                                            {"Apply to selected"}
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => setSelectionOption("All")} type="submit">
                                            {"Apply to all"}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                        )}
                    </Box>
                </form>
            </div>
        );
    }
);
// Type and required properties
UpdateMTAStatus.propTypes = {};
// Default properties


export default UpdateMTAStatus;
