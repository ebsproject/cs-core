import UpdateRSHT from "./UpdateRSHT";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
import { useServiceContext } from "custom-components/context/service-context";

afterEach(cleanup)
const refresh = () => { };
const handleClose = () => { }
const selection = [];

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn((service) => service),
}));
const mockServiceContext = { service: {} };

beforeEach(() => {
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
});

test("UpdateRSHT is in the DOM", () => {
    render(
        <Provider store={store}>
            <IntlProvider locale="en">
                <UpdateRSHT refresh={refresh} selection={selection} handleClose={handleClose}  ></UpdateRSHT>
            </IntlProvider>
        </Provider>
    );
    expect(screen.getByTestId("UpdateRSHTTestId")).toBeInTheDocument();
});
