import React, { useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
import { useForm, Controller } from "react-hook-form";
import { modifyItems } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { useAlertContext } from "components/alerts/alert-context";

const {
    Grid,
    Button,
    TextField,
    Typography,
    Box,
    CircularProgress,
} = Core;
const UpdateQuantities = React.forwardRef(({ refresh, selection, handleClose }, ref) => {

    const [selectionOption, setSelectionOption] = useState("Selected");
    const {service, rowData} = useServiceContext();
    const {setAlerts } = useAlertContext();

    const [loading, setLoading] = useState(false);
    const {
        control,
        handleSubmit,
        formState: { errors },
        reset,
        setValue,
    } = useForm({
        defaultValues: {
            volume: null,
            packageCount: null,
        },
    });
    const onSubmit = async (data) => {
        setLoading(true);
        if (selectionOption === "All") {
            let input = [{
                id:0,
                weight: data.volume,
                packageCount: data.packageCount,
            }];
            await modifyItems({input, service, id: rowData.id})
            refresh();
            handleClose();
            reset();
            setAlerts((prev) => [...prev, {id:'success-all-items-updated', message: 'All items were updated successfully', severity:'success'}]);
            setLoading(false);
        } else {
            if (selection.length > 0) {
                let input = [];
                selection.forEach((item) => {
                    input = [...input,{
                        id: item.id,
                        weight: data.volume,
                        packageCount: data.packageCount,
                    }];
                });
                await modifyItems({input,service});
                setAlerts((prev) => [...prev, {id:'success-selected-items-updated', message: 'Selection was updated successfully', severity:'success'}]);
                handleClose();
                refresh();
                setLoading(false);
                reset();
            } else {
                setAlerts((prev) => [...prev, {id:'error-no-items-selected', message: 'No items selected for updating', severity:'error'}]);
                setLoading(false);
            }
        }
    };
    return (
        <div ref={ref} data-testid={"UpdateQuantitiesTestId"}>
            <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateForm">
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <Typography variant={"h5"} className={"bold"}>
                            {"Seed count"}
                        </Typography>
                        <Controller
                            name="volume"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    {...field}
                                    variant="outlined"
                                    type="number"
                                    label={"Total Quantity"}
                                    data-testid={"volume"}
                                    error={Boolean(errors["volume"])}
                                    helperText={errors["volume"]?.message}
                                />
                            )}
                            rules={{
                                required: (
                                    <Typography>
                                        {"Please enter a value for quantity"}
                                    </Typography>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={6}>
                        <Typography variant={"h5"}>
                            {"Package count"}
                        </Typography>
                        <Controller
                            name="packageCount"
                            control={control}
                            render={({ field }) => (
                                <TextField
                                    {...field}
                                    variant="outlined"
                                    type="number"
                                    label={"Package Count"}
                                    data-testid={"packageCount"}
                                    error={Boolean(errors["packageCount"])}
                                    helperText={errors["packageCount"]?.message}
                                />
                            )}
                            rules={{
                                required: (
                                    <Typography>
                                        {"Please enter a value for count"}
                                    </Typography>
                                ),
                            }}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <Box display="flex" justifyContent="flex-end" >
                            <Button onClick={handleClose}>
                                {"Cancel"}
                            </Button>
                            {loading ? (<CircularProgress />) : (
                                <div>
                                    <Grid container spacing={2}>
                                        <Grid item>
                                            <Button
                                                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                                onClick={() => setSelectionOption("Selected")} type="submit">
                                                {"Apply to selected"}
                                            </Button>
                                        </Grid>
                                        <Grid item>
                                            <Button
                                                className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                                onClick={() => setSelectionOption("All")} type="submit">
                                                {"Apply to all"}
                                            </Button>
                                        </Grid>
                                    </Grid>
                                </div>
                            )}
                        </Box>
                    </Grid>
                </Grid>
            </form>
        </div>
    );
}
);
// Type and required properties
UpdateQuantities.propTypes = {};
// Default properties


export default UpdateQuantities;
