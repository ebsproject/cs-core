import { Core } from "@ebs/styleguide";
import PackageData from "./PackageData";
import React, { useEffect, useState } from "react";
import PackageSelect from "./PackageSelect";
import { modifyItems } from "./functions";
const { DialogContent, Button, DialogActions, Grid, Typography, Box, Dialog } = Core;
import { useServiceContext } from "custom-components/context/service-context";
import { useDispatch } from "react-redux";
import { sendMessage } from "./functions";
import { restClient } from "utils/axios/axios";

const PackageReplace = ({ open, handleClose, row, refresh }) => {
    const [step, setStep] = useState(1);
    const [packageData, setPackageData] = useState(null);
    const [dataSelected, setDataSelected] = useState(null);
    const { service, rowData } = useServiceContext();
    const dispatch = useDispatch();
    useEffect(() => {
        if (packageData) {
            setDataSelected({
                number: row.number,
                packageName: packageData.packageCode,
                packageLabel: packageData.packageLabel,
                weight: packageData.packageQuantity,
                packageCount: 0,
                packageUnit: packageData.packageUnit,
                seedName: packageData.seed.seedName,
                seedCode: packageData.seed.seedCode,
                code: packageData.seed.germplasm.germplasmCode
            })
        } else { setDataSelected(null) }
    }, [packageData])
    if (!row) return null;
    const handleNext = () => {
        setStep((step) => step + 1);
    }
    const handleSave = async () => {
        let input = {
            id: row.id,
            code: packageData.seed.germplasm.germplasmCode,
            packageName: packageData.packageCode,
            packageLabel: packageData.packageLabel,
            seedName: packageData.seed.seedName,
            seedCode: packageData.seed.seedCode,
            weight: packageData.packageQuantity,
            packageCount: 0,
            packageUnit: packageData.packageUnit,
            status:"Unverified"
        }
        await modifyItems({ input, service });
        await restClient.get(`/shipment/${rowData.id}/items/all-data`);
        sendMessage("success", `You have successfully replaced 1 package in item ${row.number}`, dispatch);
        setPackageData(null);
        setStep(1);
        refresh();
        handleClose();

    }
    const selectComponent = () => {
        switch (step) {
            case 1:
                return (<PackageData row={row} type={"Original Package"} />)
            case 2:
                return (<PackageSelect setPackageData={setPackageData} row={row} />)
            case 3:
                return (
                    <Grid container flexDirection={"row"} spacing={2}>
                        <Grid item xs={6}>
                            <PackageData row={row} type={"Original Package"} />
                        </Grid>
                        <Grid item xs={6}>
                            {dataSelected ? (<PackageData row={dataSelected || row} type={"Replacement Package"} />) : (
                                <Typography variant={"title"}>
                                    {"Please select first a package to be replaced"}
                                </Typography>
                            )}
                        </Grid>
                    </Grid>
                )
        }
    }
    return (
        <>
            <Dialog
                fullWidth
                maxWidth={step === 2  ? "lg" : "md" }
                open={open}
                onClose={handleClose}
            >
                <Box  
                sx={{padding:1}}>
                <Typography variant={"h5"}>
                    {step === 3 ? "Replacement Summary" : "Replace Package"}
                </Typography>
                <Typography variant={"subtitle"}>
                    {`You are replacing 1 package in entry number ${row.number}`}
                </Typography>
                <DialogContent>
                    {selectComponent()}
                </DialogContent>
                <DialogActions>
                    <Button variant={"outlined"} onClick={handleClose}>
                        {"Cancel"}
                    </Button>
                    {
                        step < 3 ? (
                            <>
                                <Button
                                    disabled={step === 1}
                                    onClick={() => setStep((step) => step - 1)}
                                    className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900" >
                                    {"Back"}
                                </Button>
                                <Button
                                    onClick={handleNext}
                                    className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900" >
                                    {"Next"}
                                </Button>
                            </>

                        ) : (
                            <>
                                <Button
                                    onClick={() => setStep((step) => step - 1)}
                                    className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900" >
                                    {"Back"}
                                </Button>
                                <Button
                                    disabled={!packageData}
                                    onClick={handleSave}
                                    className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900" >
                                    {"Confirm"}
                                </Button>
                            </>
                        )
                    }
                </DialogActions>
                </Box>
            </Dialog>
        </>
    )
}
export default PackageReplace;