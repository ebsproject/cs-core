import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import ReceivedDate from "./ReceivedDate";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn((service)=> service ),
}));

describe("ReceivedDate component...", () => {
  const mockDispatch = jest.fn();
  const mockServiceContext = { service: {} };

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
  });

  test("renders the ReceivedDate component with initial value", () => {
    const { getByTestId } = render(
      <ReceivedDate value={{ values: { receivedDate: "2020-12-31", id: 1 } }} />
    );
    const textField = getByTestId("ReceivedDatePickerTestId");
    const input = textField.querySelector("input");

    expect(textField).toBeInTheDocument();
    expect(input).toHaveValue("2020-12-31");
  });

  test("updates the value when user types in the control", () => {
    const { getByTestId } = render(
      <ReceivedDate value={{ values: { receivedDate: "test 2", id: 1 } }} />
    );
    const textField = getByTestId("ReceivedDatePickerTestId");
    const input = textField.querySelector("input");

    fireEvent.change(input, { target: { value: "2024-12-31" } });
    expect(input).toHaveValue("2024-12-31");
  });


});
