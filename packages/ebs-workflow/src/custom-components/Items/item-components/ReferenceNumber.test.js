import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import ReferenceNumber from "./ReferenceNumber";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
import { modifyItems } from "./functions";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));

jest.mock("./functions", () => ({
  modifyItems: jest.fn(),
  sendMessage: jest.fn()
}))

describe("ReferenceNumber component...", () => {
  const mockDispatch = jest.fn();
  const mockServiceContext = { userProfile: {} };

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    jest.clearAllMocks();
    useServiceContext.mockReturnValue({
      service: { id: 1 }
  });
  });

  test("renders the ReferenceNumber component with initial value", () => {
    const { getByTestId } = render(
      <ReferenceNumber value={{ values: { referenceNumber: "test 1", id: 1 } }} />
    );
    const textField = getByTestId("TextFieldReferenceNumberId");
    const input = textField.querySelector("input");

    expect(textField).toBeInTheDocument();
    expect(input).toHaveValue("test 1");
  });

  test("updates the value when user types in the TextField", () => {
    const { getByTestId } = render(
      <ReferenceNumber value={{ values: { referenceNumber: "test 2", id: 1 } }} />
    );
    const textField = getByTestId("TextFieldReferenceNumberId");
    const input = textField.querySelector("input");

    fireEvent.change(input, { target: { value: "test 2" } });
    expect(input).toHaveValue("test 2");
  });

  test("updates the value when user types in the TextField twice", () => {
    const { getByTestId } = render(
      <ReferenceNumber value={{ values: { referenceNumber: "test 2", id: 1 } }} />
    );
    const textField = getByTestId("TextFieldReferenceNumberId");
    const input = textField.querySelector("input");

    fireEvent.change(input, { target: { value: "New value" } });
    expect(input).toHaveValue("New value");
  });


});
