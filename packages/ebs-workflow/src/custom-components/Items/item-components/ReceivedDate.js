import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyItems, sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const ReceivedDate = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState("");
    const [newValue, setNewValue] = useState("");
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    const {service} = useServiceContext();
    useEffect(() => {
        let date = value?.values["receivedDate"];
        const dateValue = date ? date : "yyyy-mm-dd"
      setNewValue(dateValue);
      setCurrentValue(dateValue);
      setRowDataId(value?.values["id"]);
    }, [value]);

    useEffect(() => {
      if (newValue !== currentValue && newValue !== "") {
        const delayDebounceFn = setTimeout(async () => {
          try {
            let input = [
              {
                id: rowDataId,
                receivedDate: newValue,
              },
            ];
            await modifyItems({input, service});
            sendMessage("success", "The selected item was updated", dispatch);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 500);

        return () => clearTimeout(delayDebounceFn);
      }
    }, [newValue]);

    const handleChange = (e) => {
        setNewValue(e.target.value)
    };

    return (
        <TextField
        data-testid={`ReceivedDatePickerTestId`}
        type="date"
        value={newValue}
        fullWidth
        onChange={handleChange}
        label={""}
      />
    );
  }
);
// Type and required properties
ReceivedDate.propTypes = {};
// Default properties


export default ReceivedDate;
