import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { Select, MenuItem, Typography } = Core;
import { modifyItems } from "./functions";
import { sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const MTAStatus = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(value?.values["mtaStatus"]);
    const [newValue, setNewValue] = useState(value?.values["mtaStatus"]);
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    const {service} = useServiceContext();
    useEffect(() => {
      setNewValue(value?.values["mtaStatus"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["mtaStatus"]);
    }, [value]);

    useEffect(() => {
      if (newValue != currentValue) {
        const delayDebounceFn = setTimeout(async () => {
          try {
            let input = [
              {
                id: rowDataId,
                mtaStatus: newValue,
              },
            ];
            await modifyItems({input, service});
            sendMessage("success", "The selected item was updated", dispatch);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 500);
        return () => {
          clearTimeout(delayDebounceFn);
        };
      }

    }, [newValue]);

    const handleChangeValue = (e) => {
      setNewValue(e.target.value);
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserSelectVolumeUnit.test.js file.
     */ <div data-testid="SelectMTAStatusId">
        <Select
          labelId="select-mtastatus"
          id="select-mtastatus-id"
          value={newValue}
          onChange={handleChangeValue}
          variant="outlined"
        >
          <MenuItem value="">
            <Typography style={{ color: "#f5123340" }}>None</Typography>
          </MenuItem>
          <MenuItem value="PUD1">
            <Typography style={{ color: "#1609" }}>PUD1</Typography>
          </MenuItem>
          <MenuItem value="PUD2">
            <Typography style={{ color: "#f50" }}>PUD2</Typography>
          </MenuItem>
          <MenuItem value="Rel1">
            <Typography style={{ color: "#f50049" }}>Rel1</Typography>
          </MenuItem>
          <MenuItem value="MLS">
            <Typography style={{ color: "#999453" }}>MLS</Typography>
          </MenuItem>
          <MenuItem value="FAO">
            <Typography style={{ color: "#195009" }}>FAO</Typography>
          </MenuItem>
          <MenuItem value="SMTA">
            <Typography style={{ color: "#f5123340" }}>SMTA</Typography>
          </MenuItem>
          <MenuItem value="NA">
            <Typography style={{ color: "#f5123340" }}>NA</Typography>
          </MenuItem>
        </Select>
      </div>
    );
  }
);
MTAStatus.propTypes = {};


export default MTAStatus;
