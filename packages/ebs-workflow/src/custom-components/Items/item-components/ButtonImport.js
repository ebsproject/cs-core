import React, { useState, useEffect, useRef } from "react";
import { EbsDialog, Core, Lab, DropzoneDialog } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
const { Autocomplete } = Lab;

import { cbClient } from "./axios";
import { useWorkflow } from "@ebs/components";
// import { showMessage } from "store/modules/message";
import { onSubmitTemplate, sendMessage, submitToServer } from "./functions";
import { useDispatch } from "react-redux";
import { createItem } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import ImportOptions from "./ImportOptions";

import loader from "assets/images/time_loader.gif";
import { restClient } from "utils/axios/axios";
import { useAlertContext } from "components/alerts/alert-context";
import message from "components/message";
const { Grid, FormControl, FormControlLabel, Radio, RadioGroup, Button,
    DialogContent, TextField, Typography, LinearProgress, CircularProgress, Box, Checkbox } = Core;

const ButtonImport = React.forwardRef(
    ({ refresh, refreshTable, setRefreshTable }, ref) => {

        const [open, setOpen] = useState(false);
        const [openFileUpload, setOpenFileUpload] = useState(false);
        const [list, setList] = useState({});
        const [option, setOption] = useState('append');
        const buttonImport = useRef();
        const { productList, sendNotification } = useWorkflow();
        const [isDangerous, setIsDangerous] = useState(false);
        const dispatch = useDispatch();
        const {
            control,
            handleSubmit,
            formState: { errors },
            reset,
            setValue,
        } = useForm();
        const [importStart, setImportStart] = useState(false);
        const [listSearchStart, setListSearchStart] = useState(false);
        const { service, rowData } = useServiceContext();
        const [listValue, setListValue] = useState(null)
        const {setAlerts}= useAlertContext();

        useEffect(() => {
            if (refreshTable) {
                refresh();
            }
            return () => setRefreshTable(false);
        }, [refreshTable]);


        useEffect(() => {
            var bodyFormData = new FormData();
            bodyFormData.append("type", "package");

            cbClient
                .post("lists-search?sort=creationTimestamp:DESC", bodyFormData)
                .then((response) => {
                    setList(response.data.result.data);
                    setListSearchStart(true);
                })
                .catch(({ message }) => { });
        }, [open]);

        const handleClose = () => {
            setListValue(null);
            setOpen(false);
            refresh();
        };
        const handleClose1 = (event, reason) => {
            if (reason === "backdropClick")
                return
            handleClose()
        }
        const handleClickOpen = () => {
            reset();

            if (list.length == 0) {
                setAlerts((prev) => [...prev, {id:'error-no-package', message: 'No Package list to import.', severity:'error'}]);
            } else {
                setOpen(true);
            }
        };
        const handleClickOption = (event) => {
            setOption(event.target.value);
        };

        const handleCloseFileModal = () => {
            setOpenFileUpload(false);
        }
        const onSubmitFileAction = async (files) => {
            console.log(files)
            return
            await submitToServer(files, service, dispatch, refresh);
            handleCloseFileModal(false);
        }
        const onSubmit = async (data) => {
            if(!rowData) return;
            setImportStart(true);
            const result = await cbClient.get(`lists/${data.listId}/members`);
            //     ,{
            //     "isBasic":"true", 
            //     "hasData":"true"
            //   });
            if (result) {
                if( result.data.result.data[0].members.length === 0){
                    setAlerts((prev) => [...prev, {id:'error-no-no-items', message: 'Please note that Import package list contains NO Items in it, please navigate to the package list, add items and import again.', severity:'error'}]);
                    setImportStart(false);
                    handleClose();
                    return;
                }

                let shipmentItem = {
                    listId: data.listId,
                    method: "POST",
                    members: result.data.result.data[0].members,
                    service: service,
                    sortMode: option,
                    isDangerous: isDangerous,
                    serviceId: rowData.id
                }
             const res =  await createItem(shipmentItem);
                if (res.error) {
                    setAlerts((prev) => [...prev, { id: 'error-create-items', message: 'There was an error trying to import the package list. ' + res.message, severity: 'error' }]);
                    setImportStart(false);
                    handleClose();
                    return;
                }

                // try {
                //     if (service.entity === "Shipment") {
                //         const jobWorkflowId =
                //             productList
                //                 .find((product) => product.productName === "Shipment Manager")
                //                 ?.actions.find((action) => action.actionName === "Submit")
                //                 ?.workflowJobId || 0;

                //         await sendNotification({
                //             recordId: service.id,
                //             jobWorkflowId: jobWorkflowId,
                //             otherParameters: { new_status: "created" },
                //         });
                //     }
                //     // sendMessage('success', 'Shipment items added successfully', dispatch);
                //     // handleClose();
                // } catch (error) {
                //     sendMessage('error', 'Network error, the notification cannot be sent.', dispatch);
                //     setImportStart(false);
                // }

                await restClient.get(`/shipment/${rowData.id}/items/all-data`);
                setTimeout(() => {
                    setAlerts((prev) => [...prev, {id:'items-added', message: 'Shipment items added successfully', severity:'success'}]);
                    setImportStart(false);
                    handleClose();
                }, 7500)
                
            } else {
                setAlerts((prev) => [...prev, {id:'items-added-error', message: 'There was an error trying to import the packages', severity:'error'}]);
                setImportStart(false);
            }
        };
        const handleChangeValue = (e) => {
            setIsDangerous(e.target.checked);
        };
        return (
            <div data-testid={"ButtonImportTestId"} ref={ref}>
                <DropzoneDialog
                    acceptedFiles={[".csv"]}
                    cancelButtonText={"cancel"}
                    submitButtonText={"submit"}
                    maxFileSize={5000000}
                    open={openFileUpload}
                    onClose={handleCloseFileModal}
                    onSave={onSubmitFileAction}
                    showPreviews={true}
                    showFileNamesInPreview={true}
                />
                <ImportOptions handleClickOpen={handleClickOpen} setOpenFileUpload={setOpenFileUpload} />
                <EbsDialog
                    open={open}
                    handleClose={handleClose1}
                    title={
                        <Typography variant={"title"}>
                            {"Import Package List"}
                        </Typography>
                    }
                    maxWidth={"sm"}
                >
                    <form
                        onSubmit={handleSubmit(onSubmit)}
                        data-testid="packageListImportForm"
                    >
                        <DialogContent
                            className="grid grid-cols-1 sm:grid-cols-2 gap-2 md:gap-6"
                        >
                            <Typography variant={"h5"}>
                                {`Select  shipment package list`}
                            </Typography>
                            <Box>&nbsp;</Box>
                            {!listSearchStart ? (<LinearProgress />) : (
                                <Controller
                                    name="listId"
                                    control={control}
                                    render={({ field }) => (
                                        <Autocomplete
                                            {...field}
                                            id="listId"
                                            data-testid={"List-Id"}
                                            options={list || []}
                                            onChange={(e, options) => {
                                                if(!options) return;
                                                setValue("listId", options.listDbId);
                                                setListValue(options)
                                            }
                                            }
                                            disabled={!listSearchStart}
                                            defaultValue={list?.listDbId}
                                            getOptionLabel={(option) => option.abbrev}
                                            value={listValue}
                                            renderInput={(params) => (
                                                <TextField
                                                    {...params}
                                                    error={errors["listId"] ? true : false}
                                                    variant="outlined"
                                                    helperText={
                                                        errors["listId"] && (
                                                            <Typography className="text-red-600 text-ebs">
                                                                {"Please select a list"}
                                                            </Typography>
                                                        )
                                                    }
                                                    label={"List Name"}
                                                />
                                            )}
                                        />
                                    )}
                                    rules={{ required: true }}
                                />
                            )}
                            <Grid container direction="row" spacing={2}>
                                <Grid item xs={12}>
                                    <FormControl component="fieldset">
                                        <RadioGroup
                                            arial-label="itemsLabel"
                                            name="shipmentItemModeOption"
                                            value={option}
                                        >
                                            <FormControlLabel
                                                value="append"
                                                control={<Radio onClick={handleClickOption} />}
                                                label="Append items at the end of the list (default)."
                                            />
                                            <FormControlLabel
                                                value="add"
                                                control={<Radio onClick={handleClickOption} />}
                                                label="Add items at the beginning of the list."
                                            />
                                        </RadioGroup>
                                    </FormControl>
                                </Grid>
                                {/* {
                                    showIsDangerousOption && (
                                        <Grid item xs={12}>
                                            <FormControlLabel
                                                label="Check if items are dangerous"
                                                onChange={handleChangeValue}
                                                control={<Checkbox checked={isDangerous} color="primary" />}
                                                labelPlacement="end"
                                            />
                                        </Grid>
                                    )
                                } */}
        
                            </Grid>
                        </DialogContent>
                        <Box className="flex flex-row" justifyContent="flex-end">
                            <Button onClick={handleClose} disabled={importStart}>
                                {"Cancel"}
                            </Button>
                            {importStart ? (<img src={loader} width={20} height={20} alt="Loading..." />) : (
                                <Button ref={buttonImport} type="submit" disabled={importStart}>
                                    <Typography>
                                        {"Import"}
                                    </Typography>
                                </Button>
                            )}
                        </Box>
                    </form>
                </EbsDialog>
            </div>
        );
    }
);

// Type and required properties
ButtonImport.propTypes = {};
// Default properties


export default ButtonImport;
