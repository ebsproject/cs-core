import Notes from "./Notes";
import { fireEvent, render, screen } from "@testing-library/react";
import { useServiceContext } from "custom-components/context/service-context";
import { useUserContext } from "@ebs/layout";
import { useDispatch } from "react-redux";
import { modifyItems, sendMessage } from "./functions";
import { rowDataMock } from "components/service/mock_data";

jest.mock("./functions", () => ({
    modifyItems: jest.fn(),
    sendMessage: jest.fn()
}))
jest.mock("react-redux", () => ({
    useDispatch: jest.fn(),
}));


jest.mock("@ebs/layout", () => ({
    useUserContext: jest.fn(),
}));
jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn((service) => service),
}));

describe("Notes component", () => {

    beforeEach(() => {
        jest.clearAllMocks()
        useServiceContext.mockReturnValue({
            service: { id: 1 }
        })
        useUserContext.mockReturnValue({
            userProfile: { dbId: 233, userName: "j.ramirez@irri.org" }
        })
    })

    const renderComponent = (props) => render(<Notes rowData={props.rowData} refresh={props.refresh} />)
    test("notes button loaded in DOM", () => {
        const props = {
            refresh: jest.fn(),
            rowData: rowDataMock
        }
        renderComponent(props);
        expect(screen.getByTestId("ButtonOpenModalMessageTestId"))

    });
    test("notes button loaded in DOM", () => {
        const props = {
            refresh: jest.fn(),
            rowData: rowDataMock
        }
        renderComponent(props);
        const button = screen.getByTestId("ButtonOpenModalMessageTestId")
        fireEvent.click(button);

    });

})
