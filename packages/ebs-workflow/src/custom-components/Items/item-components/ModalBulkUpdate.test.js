import ModalBulkUpdate from "./ModalBulkUpdate";
import { render, cleanup, screen, fireEvent, getByTestId, act, waitFor, getByRole } from "@testing-library/react";
import "@testing-library/dom";
import { useServiceContext } from "custom-components/context/service-context";
import { reorderItems } from "./functions";

jest.mock("./functions", ()=> ({
    reorderItems: jest.fn()
}));
jest.mock("./UpdateRSHT", ()=> ()=> <div>Update RSHT Component</div>);
jest.mock("./UpdateStatus", ()=> ()=> <div>Update Status Component</div>);
jest.mock("./UpdateQuantities", ()=> ()=> <div>Update Quantities Component</div>);
jest.mock("./UpdateReceivedDate", ()=> ()=> <div>Update Received Date Component</div>);
jest.mock("./SyncComponent", ()=> ()=> <div>Sync CB Component</div>);

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn((service) => service),
}));

describe("ModalBulk Update", () => {
    const mockServiceContext = { service: {bulkOptions:["Status", "Quantities", "Seed Received Date", "Sync from CB", "GHT"] }, };
    const handleClose = jest.fn();
    const refresh = jest.fn();

    beforeEach(() => {
        useServiceContext.mockReturnValue(mockServiceContext);
        jest.clearAllMocks();
    });
    const renderComponent = (props) => render(<ModalBulkUpdate refresh={refresh} selection={props.selection} open={props.open} node={props.node} handleClose={handleClose}  ></ModalBulkUpdate>

    );
    test("ModalBulkUpdate is in the DOM", () => {
        const props = {
            selection: [{id:1}],
            node: { node: { define: { rules: { columns: [{ name: "test", alias: "test" } ]} } } },
            open: true
        }
        renderComponent(props);
       expect(screen.getByText("Bulk Update")).toBeInTheDocument();
       expect(screen.getByText(/selected items/i)).toBeInTheDocument();
    });

    test("option select with status", async () => {
        const props = {
            selection: [{id:1}],
            node: { node: { define: { rules: { columns: [{ name: "test", alias: "test" }] } } } },
            open: true
        }
            renderComponent(props);
            const dropdown = screen.getByRole('combobox');
            fireEvent.mouseDown(dropdown);
            fireEvent.click(screen.getByText('Status'));
             expect(screen.getByText("Update Status Component")).toBeInTheDocument();
      
    });

    test("option select with Quantities", async () => {
        const props = {
            selection: [{id:1}],
            node: { node: { define: { rules: { columns: [{ name: "test", alias: "RSHT" }] } } } },
            open: true
        }
            renderComponent(props);
            const dropdown = screen.getByRole('combobox');
            fireEvent.mouseDown(dropdown);
            fireEvent.click(screen.getByText('Quantities'));
             expect(screen.getByText("Update Quantities Component")).toBeInTheDocument();
      
    });
    test("option select with Seed Received Date", async () => {
        const props = {
            selection: [{id:1}],
            node: { node: { define: { rules: { columns: [{ name: "test", alias: "RSHT" }] } } } },
            open: true
        }
            renderComponent(props);
            const dropdown = screen.getByRole('combobox');
            fireEvent.mouseDown(dropdown);
            fireEvent.click(screen.getByText('Seed Received Date'));
             expect(screen.getByText("Update Received Date Component")).toBeInTheDocument();
      
    });
    test("option select with Sync from CB", async () => {
        const props = {
            selection: [{id:1}],
            node: { node: { define: { rules: { columns: [{ name: "test", alias: "RSHT" }] } } } },
            open: true
        }
            renderComponent(props);
            const dropdown = screen.getByRole('combobox');
            fireEvent.mouseDown(dropdown);
            fireEvent.click(screen.getByText('Sync from CB'));
             expect(screen.getByText("Sync CB Component")).toBeInTheDocument();
      
    });
    test("option select with RSHT", async () => {
        const props = {
            selection: [{id:1}],
            node: { define: { rules: { columns: [{ name: "testCode", alias: "RSHT" }] } } } ,
            open: true
        }
            renderComponent(props);
            const dropdown = screen.getByRole('combobox');
            fireEvent.mouseDown(dropdown);
            fireEvent.click(screen.getByText('RSHT'));
             expect(screen.getByText("Update RSHT Component")).toBeInTheDocument();
      
    });
     
})

