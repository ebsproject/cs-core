import PackageSelect from "./PackageSelect";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import { useServiceContext } from "custom-components/context/service-context";

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));

jest.mock("./search-components", () => jest.fn(() => <div data-testid={"searchComponentTestId"}>Search Component</div>));
jest.mock("./search-components/resultGrid", () => jest.fn(() => <div data-testid={"resultGridComponentTestId"}>ResultGrid Component</div>));


describe("Package Select component", () => {
    beforeEach(() => {
        useServiceContext.mockReturnValue({
            service: { id: 1 }
        });
    })
    const renderComponent = (props) => render(
        <PackageSelect row={props.row} setPackageData={props.setPackageData} />
    )
    test("PackageSelect is in the DOM", () => {
        const setValueMock= jest.fn()
        const props = {
            row: { id: 1, code:"some code" },
            setPackageData: jest.fn()
        }
        renderComponent(props);
        expect(screen.getByTestId("PackageSelectTestId")).toBeInTheDocument();
        const textField = screen.getByTestId("SearchBoxTestId");
        const input = textField.querySelector("input");
        fireEvent.change(input, { target: { value: "Some Input" } });
        fireEvent.focusIn(input);
        const button = screen.getByTestId("searchButtonTestId");
        fireEvent.click(button)

    });
})


