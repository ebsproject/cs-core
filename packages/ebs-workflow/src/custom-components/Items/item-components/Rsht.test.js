import React from "react";
import { render, fireEvent, waitFor, act, screen } from "@testing-library/react";
import Rsht from "./Rsht";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn((service) => service),
}));

jest.mock("./functions", () => ({
  modifyItems: jest.fn(),
  sendMessage: jest.fn(),
}));

describe("RSHT component", () => {
  const mockDispatch = jest.fn();
  const mockServiceContext = { service: {} };

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
  });

  test("renders the Rsht component with initial value", () => {
    const { getByTestId } = render(
      <Rsht value={{ values: { status: "rejected", testCode: "Test Code 1", id: 1 } }} />
    );
    const textField = getByTestId("TextFieldRshtNumberId");
    const input = textField.querySelector("input");

    expect(textField).toBeInTheDocument();
    expect(input).toHaveValue("Test Code 1");
  });

  test("updates the value when user types in the TextField", () => {
    const { getByTestId } = render(
      <Rsht value={{ values: { status: "some status", testCode: "Test code 2", id: 1 } }} />
    );
    const textField = getByTestId("TextFieldRshtNumberId");
    const input = textField.querySelector("input");

    fireEvent.change(input, { target: { value: "Test code 2" } });
    expect(input).toHaveValue("Test code 2");
  });

  test("user types new value", async () => {
    const { getByTestId } = render(
      <Rsht value={{ values: { status: "some status", testCode: "Test code 2", id: 1 } }} />
    );
    const textField = getByTestId("TextFieldRshtNumberId");
    const input = textField.querySelector("input");
    act(() => {
      fireEvent.change(input, { target: { value: "Test code changed" } });
    })
    await waitFor(() => {
      expect(input).toHaveValue("Test code changed");
    })

  });


});
