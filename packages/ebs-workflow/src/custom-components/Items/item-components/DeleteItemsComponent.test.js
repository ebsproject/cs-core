import React from "react";
import { render, fireEvent, screen, waitFor } from "@testing-library/react";
import DeleteItems from "./DeleteItems";
import { useServiceContext } from "custom-components/context/service-context";
import { deleteBulk, deleteItem, sendMessage, modifyItems, reorderItems } from "./functions";
import '@testing-library/dom';


jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));

jest.mock("./functions", () => ({
  deleteBulk: jest.fn(),
  deleteItem: jest.fn(),
  sendMessage: jest.fn(),
  modifyItems: jest.fn(),
  reorderItems: jest.fn(),
}));

jest.mock('react-intl', () => ({
  FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Delete Items Component", () => {
  const serviceMock = { entity: "test-service" };
  const rowDataMock = { id: 1 };


  beforeEach(() => {
    jest.clearAllMocks();
    useServiceContext.mockReturnValue({
      service: serviceMock,
      rowData: rowDataMock,
    });
  });

  const renderComponent = (props) => render(
    <DeleteItems refresh={props.refreshMock} selection={props.selectionMock} />
  );


  test("renders the button in the DOM", () => {
    const props = {
      refreshMock: jest.fn(),
      selectionMock: [{ id: 1 }, { id: 2 }]
    };
    renderComponent(props);
    expect(screen.getByTestId("DeleteItemTestId")).toBeInTheDocument();
  });


  test("submits form with 'delete' option for selected items", async () => {
    const props = {
      refreshMock: jest.fn(),
      selectionMock: [{ id: 1 }, { id: 2 }]
    };
    renderComponent(props);
    fireEvent.click(screen.getByTestId("BulkUpdateOpenModalTestId"));
    fireEvent.click(screen.getByLabelText("Delete item permanently"));
    fireEvent.click(screen.getByLabelText("Selected Item"));
    fireEvent.click(screen.getByText("Apply"));

    await waitFor(() => {
      expect(deleteItem).toHaveBeenCalledTimes(props.selectionMock.length);
      expect(reorderItems).toHaveBeenCalledWith(rowDataMock.id, serviceMock.entity);
      expect(props.refreshMock).toHaveBeenCalled();
    });
  });

  test("submits form with 'removed' option for selected items", async () => {
    const props = {
      refreshMock: jest.fn(),
      selectionMock: [{ id: 1 }, { id: 2 }]
    };
    renderComponent(props);
    fireEvent.click(screen.getByTestId("BulkUpdateOpenModalTestId"));
    fireEvent.click(screen.getByLabelText("Mark item only as rejected"));
    fireEvent.click(screen.getByLabelText("Selected Item"));
    fireEvent.click(screen.getByText("Apply"));
    await waitFor(() => {
      expect(modifyItems).toHaveBeenCalledTimes(props.selectionMock.length);
      expect(props.refreshMock).toHaveBeenCalled();
    });
  });
  test("submits form with 'removed' option for all items", async () => {
    const props = {
      refreshMock: jest.fn(),
      selectionMock: [{ id: 1 }, { id: 2 }]
    };
    renderComponent(props);
    fireEvent.click(screen.getByTestId("BulkUpdateOpenModalTestId"));
    fireEvent.click(screen.getByLabelText("Mark item only as rejected"));
    fireEvent.click(screen.getByLabelText("All Items"));
    fireEvent.click(screen.getByText("Apply"));
    await waitFor(() => {
      expect(modifyItems).toHaveBeenCalledTimes(1);
      expect(props.refreshMock).toHaveBeenCalled();
    });
  });
  test("submits form with 'delete' option for all items", async () => {
    const props = {
      refreshMock: jest.fn(),
      selectionMock: [{ id: 1 }, { id: 2 }]
    };
    renderComponent(props);
    fireEvent.click(screen.getByTestId("BulkUpdateOpenModalTestId"));
    fireEvent.click(screen.getByLabelText("Delete item permanently"));
    fireEvent.click(screen.getByLabelText("All Items"));
    fireEvent.click(screen.getByText("Apply"));
    await waitFor(() => {
      expect(deleteBulk).toHaveBeenCalled();
      expect(props.refreshMock).toHaveBeenCalled();
    });
  });
  test("submits form with 'removed' option for selected items with no selection", async () => {
    const props = {
      refreshMock: jest.fn(),
      selectionMock: []
    };
    renderComponent(props);
    fireEvent.click(screen.getByTestId("BulkUpdateOpenModalTestId"));
    fireEvent.click(screen.getByLabelText("Mark item only as rejected"));
    fireEvent.click(screen.getByLabelText("Selected Item"));
    fireEvent.click(screen.getByText("Apply"));
    await waitFor(() => {
      expect(sendMessage).toHaveBeenCalled();
    });
  });

});
