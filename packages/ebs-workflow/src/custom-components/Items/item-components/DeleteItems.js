import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
// CORE COMPONENTS
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";

import { deleteBulk, deleteItem, sendMessage } from "./functions";
import { restClient } from "utils/axios/axios";
import { useServiceContext } from "custom-components/context/service-context";
import { modifyItems, reorderItems } from "./functions";
const {
    Grid,
    Button,
    DialogActions,
    DialogContent,
    IconButton,
    Tooltip,
    Typography,
    FormControl,
    FormLabel,
    FormControlLabel,
    Radio,
    RadioGroup,
    CircularProgress,
} = Core;
const { Delete } = Icons;

const DeleteItems = React.forwardRef(
    ({ refresh, selection }, ref) => {
        const [open, setOpen] = useState(false);
        const [selectionOption, setSelectionOption] = useState("Selected");
        const dispatch = useDispatch();
        const [loading, setLoading] = useState(false);
        const { service, rowData } = useServiceContext();
        const [deleteOption, setDeleteOption] = useState("removed");

        const handleClickOpen = () => setOpen(true);
        const handleClose = () => setOpen(false);

        const {
            control,
            handleSubmit,
            formState: { errors },
            reset,
            setValue,
        } = useForm({
            defaultValues: {},
        });

        const handleClose1 = (event, reason) => {
            if (reason === "backdropClick")
                return
            handleClose()
        }
        const onSubmit = async (data) => {
            setLoading(true);
            switch (deleteOption) {
                case "delete":
                    if (data.selectedItem == "Selected") {
                        if (selection.length > 0) {
                            for(let i = 0; i < selection.length; i++){
                                const value = selection[i];
                                await deleteItem({ id: value.id, service });
                            }
                            sendMessage("success", "Selected items successfully deleted", dispatch);
                        } else {
                            sendMessage("error", "No shipment item selected for deleting", dispatch);
                        }
                    } else {
                        await deleteBulk({ id: rowData.id, service });
                        sendMessage("success", "All shipment items were deleted successfully", dispatch);
                    }
                    await reorderItems(rowData.id,service.entity);
                    setLoading(false);
                    handleClose();
                    refresh();
                    break;
                case "removed":
                    if (data.selectedItem == "Selected"){
                        let input = [];
                        for(let i = 0; i < selection.length; i++){
                            const value = selection[i];
                            input = [...input,{id:value.id,status:"Rejected"}]
                        await modifyItems({input, service});
                        }
                    }else{
                        let input = {
                            id: 0,
                            status: "Rejected"
                        }
                        await modifyItems({ input, service, id: rowData.id })
                    }
                    sendMessage('success', 'The items were marked as removed successfully', dispatch);
                    setLoading(false);
                    handleClose();
                    refresh();
                    break;
                default:
                    break;
            }
        };

        const handleClickSelection = (event) => {
            if (event.target.value === "Selected") {
                setSelectionOption("Selected");
            } else {
                setSelectionOption("All");
            }
        };

        useEffect(() => {
            setValue("selectedItem", selectionOption);
        }, [selectionOption]);

        const handleClickDeleteOption = (event) => {
            setDeleteOption(event.target.value);
        };
        return (
            <div ref={ref} data-testid={"DeleteItemTestId"}>
                <Tooltip
                    arrow
                    title={
                        <Typography className="font-ebs text-xl">
                            {"Shipment Item Bulk Delete"}
                        </Typography>
                    }
                >
                    <Button
                        data-testid={"BulkUpdateOpenModalTestId"}
                        onClick={handleClickOpen}
                        aria-label="bulk-update"
                        
                    >
                        <Delete />
                    </Button>
                </Tooltip>
                <EbsDialog
                    open={open}
                    handleClose={handleClose1}
                    maxWidth="sm"
                    title={"Bulk Delete"}
                >
                    <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkDeleteForm">
                        <DialogContent dividers>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <div>
                                    <FormControl component="fieldset">
                                    <FormLabel component="legend">Options</FormLabel>
                                    <RadioGroup
                                        arial-label="shipmentTypeLabel"
                                        name="shipmentItemDeleteOption"
                                        value={deleteOption}
                                        row={true}
                                    >
                                        <FormControlLabel
                                            value="removed"
                                            control={<Radio onClick={handleClickDeleteOption} />}
                                            label="Mark item only as rejected"
                                        />
                                        <FormControlLabel
                                            value="delete"
                                            control={<Radio onClick={handleClickDeleteOption} />}
                                            label="Delete item permanently"
                                        />
                                    </RadioGroup>
                                </FormControl>
                                    </div>
                                    <div>
                                        <FormControl component="fieldset">
                                            <FormLabel component="legend">Apply to</FormLabel>
                                            <RadioGroup
                                                aria-label="selectedItem"
                                                name="selectedItem"
                                                value={selectionOption}
                                                row={true}
                                            >
                                                <FormControlLabel
                                                    value="Selected"
                                                    control={<Radio onClick={handleClickSelection} />}
                                                    label="Selected Item"
                                                />
                                                <FormControlLabel
                                                    value="All"
                                                    control={<Radio onClick={handleClickSelection} />}
                                                    label="All Items"
                                                />
                                            </RadioGroup>
                                        </FormControl>
                                    </div>
                                </Grid>
                            </Grid>
                        </DialogContent>

                        <DialogActions>
                            <Button onClick={handleClose}>
                                {"Cancel"}
                            </Button>
                            {loading ? (<CircularProgress data-testid={"CircularProgressTestId"} />) : (<Button type="submit">
                                {"Apply"}
                            </Button>)}
                        </DialogActions>
                    </form>
                </EbsDialog>
            </div>
        );
    }
);
// Type and required properties
DeleteItems.propTypes = {};
// Default properties


export default DeleteItems;
