import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { FormControlLabel, Checkbox } = Core;
import { modifyItems, sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const DangerousMark = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(value?.values["isDangerous"]);
    const [newValue, setNewValue] = useState(value?.values["isDangerous"]);
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    const {service} = useServiceContext();
    useEffect(() => {
      setNewValue(value?.values["isDangerous"]);
      setCurrentValue(value?.values["isDangerous"]);
      setRowDataId(value?.values["id"]);
    }, [value]);

    useEffect(() => {
      if (newValue !== currentValue && newValue !== "") {
        const delayDebounceFn = setTimeout(async () => {
          try {
            let input = [
              {
                id: rowDataId,
                isDangerous: newValue,
              },
            ];
            await modifyItems({input, service});
            sendMessage("success", "The selected item was updated", dispatch);
            setRefreshTable(true);
          } catch ({ message }) {
          } finally {
          }
        }, 500);

        return () => clearTimeout(delayDebounceFn);
      }
    }, [newValue]);

    const handleChangeValue = (e) => {
        setNewValue(e.target.checked);
    };

    return (
        <FormControlLabel
        onChange={handleChangeValue}
        value={currentValue}
        control={<Checkbox checked={currentValue} color="primary" />}
        labelPlacement="top"
      />
    );
  }
);
// Type and required properties
DangerousMark.propTypes = {};
// Default properties


export default DangerousMark;
