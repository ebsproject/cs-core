import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
const { Select, MenuItem, Typography } = Core;
import { modifyItems } from "./functions";
import { sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

const SelectStatus = React.forwardRef(
  ({ value, setRefreshTable }, ref) => {
    const [currentValue, setCurrentValue] = useState(value?.values["status"]);
    const [newValue, setNewValue] = useState(value?.values["status"]);
    const [rowDataId, setRowDataId] = useState();
    const dispatch = useDispatch();
    const {service} = useServiceContext();
    useEffect(() => {
      setNewValue(value?.values["status"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["status"]);
    }, [value]);

    useEffect(() => {
      if (newValue != currentValue) {
        const delayDebounceFn = setTimeout(async () => {
          try {
            let input = [
              {
                id: rowDataId,
                status: newValue,
              },
            ];
            await modifyItems({input, service});
            sendMessage("success", "The selected item was updated", dispatch);
            setRefreshTable(true)
          } catch ({ message }) {
          } finally {
          }
        }, 500);
        return () => {
          clearTimeout(delayDebounceFn);
        };
      }

    }, [newValue]);

    const handleChangeValue = (e) => {
      setNewValue(e.target.value);
    };

    return (
      /* 
     @prop data-testid: Id to use inside ShipmentItemBrowserSelectVolumeUnit.test.js file.
     */ <div data-testid="SelectStatusId">
        <Select
          labelId="select-status"
          id="select-status-id"
          value={newValue}
          label="Volume Unit"
          onChange={handleChangeValue}
          variant="outlined"
        >
          {/* <MenuItem value="notliberated">
            <Typography style={{ color: "#189" }}>Not Liberated</Typography>
          </MenuItem> */}
          <MenuItem value="Released">
            <Typography style={{ color: "#1609" }}>Released</Typography>
          </MenuItem>
          <MenuItem value="Rejected">
            <Typography style={{ color: "#f50" }}>Rejected</Typography>
          </MenuItem>
          <MenuItem value="Unverified">
            <Typography style={{ color: "#f50049" }}>Unverified</Typography>
          </MenuItem>
          {/* <MenuItem value="removed">
            <Typography style={{ color: "#999453" }}> Removed</Typography>
          </MenuItem>
          <MenuItem value="liberated">
            <Typography style={{ color: "#195009" }}> Liberated</Typography>
          </MenuItem>
          <MenuItem value="unliberated">
            <Typography style={{ color: "#f5123340" }}> Unliberated</Typography>
          </MenuItem> */}
        </Select>
      </div>
    );
  }
);
SelectStatus.propTypes = {};


export default SelectStatus;
