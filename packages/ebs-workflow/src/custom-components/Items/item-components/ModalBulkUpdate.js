import React, { useState, useEffect } from "react";
import { Core, EbsDialog, Lab } from "@ebs/styleguide";
const { Autocomplete } = Lab;
const { DialogContent, Box, Typography, TextField } = Core;
import UpdateRSHT from "./UpdateRSHT";
import UpdateStatus from "./UpdateStatus";
import UpdateQuantities from "./UpdateQuantities";
import { useServiceContext } from "custom-components/context/service-context";
import { reorderItems } from "./functions";
import UpdateDangerous from "./UpdateDangerous";
import SyncComponent from "./SyncComponent";
import UpdateReceivedDate from "./UpdateReceivedDate";

const ModalBulkUpdate = ({ open, handleClose, refresh, selection, node }) => {
    const [mode, setMode] = useState("");
    const [options, setOptions] = useState([]);
    const { service } = useServiceContext();
    const headerValue = node.define?.rules?.columns?.find(item => item.name === "testCode")?.alias;
    const headerName = headerValue ? headerValue : "Test Code";
    useEffect(() => {
        if (service)
            setOptions(service.bulkOptions || [])
    }, [service])

    useEffect(() => {
        setMode("")
    }, [open]);

    const selectComponent = () => {

        let option = headerName;
        switch (mode) {
            case option: return (<UpdateRSHT refresh={refresh} selection={selection} handleClose={handleClose} option={option} />);
            case "Status": return (
                <UpdateStatus refresh={refresh} selection={selection} handleClose={handleClose} />
            );
            // case "MTA Status": return (
            //     <UpdateMTAStatus refresh={refresh} selection={selection} handleClose={handleClose} />
            // );
            case "Quantities": return (<UpdateQuantities refresh={refresh} selection={selection} handleClose={handleClose} />)
            // case "Dangerous": return (<UpdateDangerous refresh={refresh} selection={selection} handleClose={handleClose} />)
            case "Seed Received Date": return (<UpdateReceivedDate refresh={refresh} selection={selection} handleClose={handleClose} />)
            case "Sync from CB": return (<SyncComponent refresh={refresh} selection={selection} handleClose={handleClose} option={option} />)
            default: <></>
                break;
        }
    }
    // const reorderFnc = async () => {
    //     await reorderItems(service.id, service.entity);
    //     refresh();
    //     handleClose()
    // }

    return (
        <div data-testid={"ModalBulkUpdateTestId"}>
            <EbsDialog
                open={open}
                handleClose={handleClose}
                title={
                    <>
                        <Typography variant={"h4"}>
                            {"Bulk Update"}
                        </Typography>
                        <Typography variant={"subtitle"}>
                            {`${selection.length} selected items`}
                        </Typography>
                    </>
                }
                maxWidth={"sm"}
            >
                <DialogContent>
                    <Typography variant={"title"}>
                        {"Select variable for bulk update"}

                    </Typography>
                    <Box>&nbsp;</Box>
                    <Autocomplete
                        data-testid={"OptionsDropDownTestId"}
                        id="combo-box-Options"
                        options={[...options, headerName]}
                        getOptionLabel={(option) => option}
                        fullWidth
                        onChange={(e, option) => setMode(option)}
                        renderInput={(params) => <TextField {...params} label="Select a value" variant="outlined" />}
                    />
                    <Box>&nbsp;</Box>
                    <Box>
                        {
                            selectComponent()
                        }
                    </Box>
                </DialogContent>
            </EbsDialog>
        </div>
    )
}
export default ModalBulkUpdate;