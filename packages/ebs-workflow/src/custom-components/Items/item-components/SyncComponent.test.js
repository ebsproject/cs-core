import SyncComponent from "./SyncComponent";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
import { useServiceContext } from "custom-components/context/service-context";

afterEach(cleanup)
const refresh = () => { };
const handleClose = () => { }
const option = "Test";

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn((service) => service),
}));
const mockServiceContext = { service: {}, rowData:{} };

beforeEach(() => {
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
});

test("SyncComponent is in the DOM", () => {
    render(
        <Provider store={store}>
            <IntlProvider locale="en">
                <SyncComponent refresh={refresh} option={option} handleClose={handleClose}  ></SyncComponent>
            </IntlProvider>
        </Provider>
    );
    expect(screen.getByTestId("SyncComponentTestId")).toBeInTheDocument();
});
