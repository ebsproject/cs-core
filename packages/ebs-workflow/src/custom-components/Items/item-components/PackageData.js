import { Core, Icons, EbsDialog } from "@ebs/styleguide";
const { Box, Grid, List, ListItem, Typography, Divider } = Core;

const PackageData = ({ row, type }) => {
    return (
        <Box style={{ backgroundColor: "#d3d3d3", paddingLeft: 10 }} data-testid={"PackageDataTestId"}>
            <List >
                <Typography variant={"h6"} >
                    {type}
                </Typography>
                <ListItem >
                    <Grid container spacing={2}>
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Entry no:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.number}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem>
                    <Grid container spacing={2} >
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Package code:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.packageName}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem>
                    <Grid container spacing={2} >
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Package label:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.packageLabel}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
                <Divider />
                <Typography variant={"h6"} >
                    {"QUANTITY"}
                </Typography>
                <ListItem>
                    <Grid container spacing={2} flexDirection={"row"}>
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Quantity:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {`${row.weight} ${row.packageUnit}`}
                            </Typography>
                        </Grid>
                    </Grid>

                </ListItem>
                <ListItem>
                    <Grid container spacing={2} >
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Package count:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.packageCount}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
                <Divider />
                <Typography variant={"h6"} >
                    {"SEED"}
                </Typography>
                <ListItem>
                    <Grid container spacing={2} >
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Seed Name:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.seedName}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem>
                    <Grid container spacing={2} >
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Seed code:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.seedCode}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
                <ListItem>
                    <Grid container spacing={2} >
                        <Grid item xs={4}>
                            <Typography variant={"title"}>
                                {"Germplasm code:"}
                            </Typography>
                        </Grid>
                        <Grid item xs={8}>
                            <Typography variant={"title"} style={{ fontWeight: 600 }}>
                                {row.code}
                            </Typography>
                        </Grid>
                    </Grid>
                </ListItem>
            </List>
        </Box>
    )
}
export default PackageData;