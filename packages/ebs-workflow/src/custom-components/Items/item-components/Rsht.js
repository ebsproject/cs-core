import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
// CORE COMPONENTS
import { Core } from "@ebs/styleguide";
const { TextField } = Core;
import { modifyItems } from "./functions";
import { sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";



//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const Rsht = React.forwardRef(({ value, setRefreshTable }, ref) => {
  const [currentValue, setCurrentValue] = useState(value?.values["testCode"]);
  const [newValue, setNewValue] = useState(value?.values["testCode"]);
  const [rowDataId, setRowDataId] = useState();
  const dispatch = useDispatch();
  const {service} = useServiceContext();
  const [errorState, setErrorState] = useState(false)
  useEffect(()=>{
    const status = value.values['status']
    if(status.toLowerCase() === 'rejected'){
      setErrorState(true)
    }
return () => setErrorState(false)
  },[value.values['status']])
  useEffect(() => {
      setNewValue(value?.values["testCode"]);
      setRowDataId(value?.values["id"]);
      setCurrentValue(value?.values["testCode"]);
  }, [value]);

  useEffect(() => {
    const delayDebounceFn = setTimeout(async () => {
      if (newValue != undefined && newValue != currentValue) {
        try {
          let input = [{
            id: rowDataId,
            testCode: newValue,
          }];
          await modifyItems({input, service});
          sendMessage("success", "The selected item was updated", dispatch);
          setRefreshTable(true);
        } catch (error) {
        }
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [newValue]);

  const handleChangeValue = (e) => {
    setNewValue(e.target.value);
  };
  return (
    /* 
   @prop data-testid: Id to use inside ShipmentItemBrowserTextFieldRshtNumber.test.js file.
   */
    <TextField
      data-testid={"TextFieldRshtNumberId"}
      ref={ref}
      value={newValue}
      onChange={handleChangeValue}
      type="text"
      variant="outlined"
      error={errorState}
      helperText={errorState && "Item rejected"}
    />
  );
});
// Type and required properties
Rsht.propTypes = {};
// Default properties


export default Rsht;
