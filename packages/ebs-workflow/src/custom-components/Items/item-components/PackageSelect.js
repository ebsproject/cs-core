import { Core, Lab } from "@ebs/styleguide";
import { useState } from "react";
import SearchComponent from "./search-components";
import ResultGrid from "./search-components/resultGrid";
const { Box, Grid, Typography, TextField, Button, Chip } = Core;

const PackageSelect = ({ setPackageData,row }) => {

    const [filter, setFilter] = useState([]);
    const [searchValue, setSearchValue] = useState("");
    const [germplasmCode, setGermplasmCode] = useState(row.code);
    const [reload, setReload] = useState(false);

    const setValue = () => {
        setGermplasmCode(searchValue);
        setReload(true);
    }
    return (
        <Box data-testid={"PackageSelectTestId"}>
            <Typography variant={"caption"}>
                {"Search parameters:"}
            </Typography>
            <Grid container spacing={2}>
                <SearchComponent entity={"Program"} setFilter={setFilter} filter={filter} disabled={false} row={row} />
                <SearchComponent entity={"Season"} setFilter={setFilter} filter={filter} disabled={true} row={row}   />
                <SearchComponent entity={"ExperimentYear"} setFilter={setFilter} filter={filter} disabled={false} row={row}   />
                <SearchComponent entity={"ExperimentType"} setFilter={setFilter} filter={filter} disabled={false} row={row}   />
                <SearchComponent entity={"Occurrence"} setFilter={setFilter} filter={filter}  disabled={true} row={row}  />
                <SearchComponent entity={"Facility"} setFilter={setFilter} filter={filter}  disabled={true} row={row}  />
                <SearchComponent entity={"SubFacility"} setFilter={setFilter} filter={filter} disabled={true} row={row}   />
            </Grid>

            <Typography variant={'caption'}>
                {"Search for germplasm code similar to:"}
            </Typography>
            <Grid container spacing={2} >
                <Grid item xs={8}>
                    <TextField
                        data-testid={"SearchBoxTestId"}
                        variant="outlined"
                        fullWidth
                        placeholder={germplasmCode === "" ? "": row.code }
                        onChange={(e) => setSearchValue(e.target.value)}
                        onFocus={()=>setGermplasmCode("")}
                    />
                </Grid>
                <Grid item xs={4}>
                    <Button
                        data-testid={"searchButtonTestId"}
                        className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
                        onClick={setValue}>
                        {"search"}
                    </Button>
                </Grid>
            </Grid>
            <Grid item xs={12} style={{ paddingTop: 5 }}>
            <Typography variant={'subtitle'}>
                {`Filters selected:`} {filter.length > 0 ? filter.map(item => (<Chip style= {{backgroundColor:"#"+(0x1000000 + ( item.val.length )*30965*0xffff).toString(16).substr(1,6)}} key={item.val} label={item.val}/>)): (<>No filters selected</>)}
            </Typography>
            </Grid>
            <Grid container spacing={2} style={{ paddingTop: 5 }}>
                <ResultGrid filter={filter} germplamsCode={germplasmCode} reload={reload} setReload={setReload} setPackageData={setPackageData} />
            </Grid>
        </Box>
    )
}
export default PackageSelect;