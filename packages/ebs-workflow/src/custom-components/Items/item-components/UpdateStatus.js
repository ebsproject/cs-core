import React, { useState } from "react";
import { Core, Lab } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
import { modifyItems, sendMessage } from "./functions";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
const {
    Grid,
    Button,
    TextField,
    Typography,
    Box,
    CircularProgress,
} = Core;

const { Autocomplete } = Lab;

const UpdateStatus = React.forwardRef(
    ({ refresh, selection, handleClose }, ref) => {

        const [selectionOption, setSelectionOption] = useState("Selected");
        const { service } = useServiceContext();
        const [loading, setLoading] = useState(false);
        const dispatch = useDispatch()
        const [status] = useState([
            "Unverified",
            "Released",
            "Rejected",
        ]);

        const {
            control,
            handleSubmit,
            formState: { errors },
            reset,
            setValue,
        } = useForm({
            defaultValues: {
                volume: null,
                packageCount: null,
            },
        });

        const onSubmit = async (data) => {
            setLoading(true)
            if (selectionOption === "All") {
                let input = {
                    id: 0,
                    status: data.status
                }
                await modifyItems({ input, service, id: service.id })
                sendMessage('success', "Bulk updating status successfully", dispatch);
                setLoading(false)
                handleClose();
                refresh();
                reset();
            } else {
                if (selection.length > 0) {
                    let input = [];
                    for (let i = 0; i < selection.length; i++) {
                        input = [...input, { id: selection[i].id, status: data.status }];
                    }
                    await modifyItems({ input, service });
                    sendMessage('success', "Selection updating status successfully", dispatch);
                    setLoading(false)
                    handleClose();
                    refresh();
                    reset();
                } else {
                    sendMessage('error', "No shipment item selected for updating", dispatch);
                    setLoading(false)
                }
            }

        };
        return (
            /* 
           @prop data-testid: Id to use inside ShipmentItemBrowserButtonBulkUpdate.test.js file.
           */
            <div ref={ref} data-testid={"ItemStatusTestId"}>
                <form onSubmit={handleSubmit(onSubmit)} data-testid="BulkUpdateForm">
                    <Grid container>
                        <Grid item xs={12} style={{ paddingBottom: "20px" }}>
                            <Typography variant={"h5"}>
                                {`What status would you like to apply`}
                            </Typography>
                            <Controller
                                control={control}
                                name="status"
                                render={({ field: { onChange, value } }) => (
                                    <Autocomplete
                                        onChange={(event, options) => {
                                            setValue("status", options);
                                            onChange(options);
                                        }}
                                        value={value}
                                        options={status}
                                        autoHighlight
                                        getOptionLabel={(item) => (item ? item : "")}
                                        isOptionEqualToValue={(option, value) =>
                                            value === undefined ||
                                            value === "" ||
                                            option === value
                                        }
                                        renderInput={(params) => (
                                            <TextField
                                                {...params}
                                                label="Select a value"
                                                variant="outlined"
                                                size="normal"
                                                fullWidth
                                                error={Boolean(errors["status"])}
                                                helperText={errors["status"]?.message}
                                            />
                                        )}
                                    />
                                )}
                                rules={{
                                    required: (
                                        <Typography>
                                            {"Please select a status"}

                                        </Typography>
                                    ),
                                }}
                            />
                        </Grid>
                    </Grid>
                    <Box display="flex" justifyContent="flex-end">
                        <Button onClick={handleClose}>
                            {"Cancel"}
                        </Button>
                        {loading ? (<CircularProgress />) : (
                            <div>
                                <Grid container spacing={2}>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => setSelectionOption("Selected")} type="submit">
                                            {"Apply to selected"}
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => setSelectionOption("All")} type="submit">
                                            {"Apply to all"}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                        )}
                    </Box>
                </form>
            </div>
        );
    }
);
// Type and required properties
UpdateStatus.propTypes = {};
// Default properties


export default UpdateStatus;
