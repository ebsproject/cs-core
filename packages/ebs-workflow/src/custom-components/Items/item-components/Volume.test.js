import React from "react";
import { render, fireEvent, waitFor } from "@testing-library/react";
import Volume from "./Volume";
import { useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";

jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn((service)=> service ),
}));

jest.mock("./functions", () => ({
  modifyItems: jest.fn(),
  sendMessage: jest.fn(),
}));

describe("Volume component", () => {
  const mockDispatch = jest.fn();
  const mockServiceContext = { service: {} };

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
  });

  test("renders the Volume component with initial value", () => {
    const { getByTestId } = render(
      <Volume value={{ values: { weight: 10, id: 1 } }} />
    );
    const textField = getByTestId("TextFieldVolumeId");
    const input = textField.querySelector("input");

    expect(textField).toBeInTheDocument();
    expect(input).toHaveValue(10);
  });

  test("updates the value when user types in the TextField", () => {
    const { getByTestId } = render(
      <Volume value={{ values: { weight: 10, id: 1 } }} />
    );
    const textField = getByTestId("TextFieldVolumeId");
    const input = textField.querySelector("input");

    fireEvent.change(input, { target: { value: 15 } });
    expect(input).toHaveValue(15);
  });


});
