import PackageData from "./PackageData";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";

afterEach(cleanup)
const row =  {packageName:"test name", code:"test code", rowNumber:1, packageLabel:"test label", weight : 0, packageUnit:"g", packageCount:0, seedName:"test seed name" };
const type = "Test";

test("PackageData is in the DOM", () => {
    render(
        <Provider store={store}>
            <IntlProvider locale="en">
                <PackageData type={type} row={row}  ></PackageData>
            </IntlProvider>
        </Provider>
    );
    expect(screen.getByTestId("PackageDataTestId")).toBeInTheDocument();
});
