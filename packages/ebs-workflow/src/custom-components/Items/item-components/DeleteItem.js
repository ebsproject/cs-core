import React, { useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { modifyItems, reorderItems, sendMessage } from "./functions";
import { deleteItem } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
// CORE COMPONENTS
const {
    IconButton,
    Tooltip,
    Button,
    Typography,
    DialogActions,
    Radio,
    RadioGroup,
    FormControlLabel,
    FormControl,
    FormLabel,
    Grid,
} = Core;
const { Delete } = Icons;

const DeleteItem = React.forwardRef(
    ({ refresh, id }, ref) => {
        const [open, setOpen] = useState(false);
        const [deleteOption, setDeleteOption] = useState("removed");
        const dispatch = useDispatch();
        const {service} = useServiceContext();

        const handleClickOpen = () => setOpen(true);
        const handleClose = () => setOpen(false);

        const handleClickDeleteOption = (event) => {
            setDeleteOption(event.target.value);

        };

        const submit = async () => {
            try {
                switch (deleteOption) {
                    case "delete":
                        await deleteItem({id,service});
                        await reorderItems(service.id,service.entity);
                        sendMessage('success', 'The item was deleted successfully, the reorder process was executed', dispatch);
                        handleClose();
                        refresh();
                        break;
                    case "removed":
                        let input = [
                            {
                                id: id,
                                status: "Rejected",
                            },
                        ];
                        await modifyItems({input, service});
                        sendMessage('success', 'The item was marked as removed successfully', dispatch);
                        handleClose();
                        refresh();
                        break;

                    default:
                        break;
                }
            } catch (error) { handleClose(); }
        };

        return (
            /* 
           @prop data-testid: Id to use inside ShipmentItemBrowserButtonDelete.test.js file.
           */
            <div ref={ref} data-testid={"DeleteShipmentItemTestId"}>
                <Tooltip
                    arrow
                    title={
                        <Typography className="font-ebs text-xl">
                            {"Delete Shipment Item"}
                        </Typography>
                    }
                >
                    <IconButton
                        data-testid={"ButtonOpenDialogTestId"}
                        onClick={handleClickOpen}
                        aria-label="delete-shipment-item"
                        color="primary"
                    >
                        <Delete />
                    </IconButton>
                </Tooltip>
                <EbsDialog
                    open={open}
                    handleClose={handleClose}
                    maxWidth="sm"
                    title={
                        "Delete Shipment Item"
                    }
                >
                    <div >
                        <Grid container direction="row" spacing={2}>
                            <Grid item xs={12}>
                                <FormControl component="fieldset">
                                    <FormLabel component="legend">Options</FormLabel>
                                    <RadioGroup
                                        arial-label="shipmentTypeLabel"
                                        name="shipmentItemDeleteOption"
                                        value={deleteOption}
                                    >
                                        <FormControlLabel
                                            value="removed"
                                            control={<Radio onClick={handleClickDeleteOption} data-testid={"RemovedRadioTestId"} />}
                                            label="Mark item only as rejected"
                                        />
                                        <FormControlLabel
                                            value="delete"
                                            control={<Radio onClick={handleClickDeleteOption} data-testid={"DeleteRadioTestId"} />}
                                            label="Delete item permanently"
                                        />
                                    </RadioGroup>
                                </FormControl>
                            </Grid>
                        </Grid>
                    </div>
                    <DialogActions>
                        <Button onClick={handleClose}>
                            {"Cancel"}
                        </Button>
                        <Button onClick={submit} data-testid={"SubmitDeleteButtonTestId"}>
                            {deleteOption === "delete" ? "Delete" : "Mark as rejected"}
                        </Button>
                    </DialogActions>
                </EbsDialog>
            </div>
        );
    }
);
// Type and required properties
DeleteItem.propTypes = {};
// Default properties


export default DeleteItem;
