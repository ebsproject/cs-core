import { useEffect, useContext, useState, Fragment } from "react";
import { useSelector } from "react-redux";
import { Core, Icons } from "@ebs/styleguide";
const { Box, Button, Typography, Tooltip } = Core;
const { GetApp } = Icons;
import { RBAC } from "@ebs/layout";
import { userContext } from "@ebs/layout";
import { downloadItemsCSVData, formattedDate } from "../functions";
import { useServiceContext } from "custom-components/context/service-context";
import loader from "assets/images/time_loader.gif";
import { WorkflowContext } from "components/designer/context/workflow-context";

const DownloadItemsCSV = ({ selectedRows, columns }) => {
    const [loading, setLoading] = useState(false);
    const { rowData } = useServiceContext();

    const csvExport = async () => {
        const _formattedDate = formattedDate();
        const fileName = `${rowData.requestCode}-${_formattedDate}`;
        setLoading(true)
        const obj = {
            id: rowData.id,
            selectedRows,
            columns,
            fileName
        };
        try {
            await downloadItemsCSVData(obj);
            setLoading(false);
        } catch (error) {
            console.log(error);
            setLoading(false);
        }


    }
    return (
        <Fragment>
            <RBAC allowedAction={'Export'}>
                <Box>
                    <Tooltip
                        arrow
                        title={
                            <Typography className='font-ebs text-lg' placement='top'>
                                Download CSV
                            </Typography>
                        }
                    >
                        {

                            <Button
                                disabled={loading}
                                onClick={csvExport}
                                aria-label='download file'
                                startIcon={
                                    loading ? (
                                        <img className='fill-current text-white ml-3' src={loader} width={20} height={20} alt="Loading..." />
                                    ) : (
                                        <GetApp className='fill-current text-white ml-3' />)
                                }
                            ></Button>

                        }

                    </Tooltip>
                </Box>
            </RBAC>
        </Fragment>)
}
export default DownloadItemsCSV;