import React, { useState } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core } from "@ebs/styleguide";
// CORE COMPONENTS
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { modifyItems, sendMessage } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import loader from "assets/images/time_loader.gif";
const {
    Grid,
    Button,
    Box,
    TextField,
    Typography,
    LinearProgress
} = Core;
const UpdateReceivedDate = React.forwardRef(({ refresh, selection, handleClose }, ref) => {

    const dispatch = useDispatch();
    const { service } = useServiceContext();
    const [receivedDate, setReceivedDate] = useState("");
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const handleChange = (e) => {
        const value = e.target.value;
        setReceivedDate(value);
    };

    const submit = async (option) => {
        const selectionOption = option;
        if (receivedDate.length === 0) {
            setError(true);
            return;
        } else
            setError(false);
        setLoading(true);
        if (selectionOption === "All") {
            let input = [{
                id: 0,
                receivedDate: receivedDate
            }];
            await modifyItems({ input, service, id: service.id })
            refresh();
            handleClose();
            sendMessage("success", "All items were updated successfully", dispatch)
            setLoading(false);
        } else {
            if (selection.length > 0) {
                let input = [];
                selection.forEach((item) => {
                    input = [...input, {
                        id: item.id,
                        receivedDate: receivedDate

                    }];
                });
                await modifyItems({ input, service });
                sendMessage("success", "Selection was updated successfully", dispatch)
                handleClose();
                refresh();
                setLoading(false);
            } else {
                sendMessage("error", "No items selected for updating", dispatch)
                setLoading(false);
            }
        }
    };

    const handleClickSave = async (option) => {
         await submit(option);
    }
    return (
        <div ref={ref} data-testid={"UpdateReceivedDateTestId"}>

            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <TextField
                        type="date"
                        value={receivedDate}
                        fullWidth
                        onChange={handleChange}
                        label={""}
                        data-testid={`updateReceivedDateTestId`}
                    />
                    {error && <Typography style={{color:"red"}} variant="body1">{"Please select a date first."}</Typography>}
                </Grid>
                <Grid item xs={12}>
                    <Box className="flex flex-row" justifyContent="flex-end" >
                        <Button onClick={handleClose}>
                            {"Cancel"}
                        </Button>
                        {loading ? ( <img src={loader} width={20} height={20} alt="Loading..." />) : (
                            <div>
                                <Grid container spacing={2}>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => handleClickSave("Selected")} >
                                            {"Apply to selected"}
                                        </Button>
                                    </Grid>
                                    <Grid item>
                                        <Button
                                            className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                            onClick={() => handleClickSave("All")}>
                                            {"Apply to all"}
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                        )}
                    </Box>
                </Grid>
            </Grid>

        </div>
    );
}
);
// Type and required properties
UpdateReceivedDate.propTypes = {};
// Default properties


export default UpdateReceivedDate;
