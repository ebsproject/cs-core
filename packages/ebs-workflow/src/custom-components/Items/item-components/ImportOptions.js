import { Core, Icons } from "@ebs/styleguide";
import { useState, useRef } from "react";
import { useDispatch } from "react-redux";
import { templateDownload } from "./functions";
const { ArrowDropDown } = Icons;
const { ButtonGroup, Button, Popper, ClickAwayListener, Typography,
    Grow, Paper, Grid, List, ListItem, } = Core;

const ImportOptions = ({ handleClickOpen, setOpenFileUpload }) => {

    const [open, setOpen] = useState(false);
    const anchorRef = useRef(null);
    const dispatch = useDispatch();

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };
    const launchForm = () => {
        handleClickOpen();
        setOpen(false);
    }

    return (
        <Grid container direction='column' alignItems='center' data-testid={"ImportOptionsTestId"}>
            <Grid item xs={12}>
                <Button
                    data-testid={"ButtonInToolbarTestId"}
                    onClick={handleToggle}
                    ref={anchorRef}
                >
                    <Typography className='text-white text-sm font-ebs'>
                        {"Import Package List"}
                    </Typography>
                    &nbsp;
                    &nbsp;
                    &nbsp;
                    <ArrowDropDown />
                </Button>

                <Popper open={open} anchorEl={anchorRef.current} transition>
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{
                                transformOrigin:
                                    placement === 'bottom' ? 'center top' : 'center bottom',
                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={handleClose}>
                                    <List id='split-button-menu-template'>
                                        <ListItem
                                            onClick={launchForm}
                                            style={{ cursor: 'pointer' }}
                                        >
                                            {"From Core Breeding"}
                                        </ListItem>
                                        <ListItem
                                            onClick={() => { setOpenFileUpload(true); handleToggle() }}
                                            style={{ cursor: 'pointer' }}
                                        >
                                            {"From CSV file "}
                                        </ListItem>
                                        <ListItem
                                            onClick={() => templateDownload(dispatch)}
                                            style={{ cursor: 'pointer' }}
                                        >
                                            {"Download CSV file template"}
                                        </ListItem>
                                    </List>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </Grid>
        </Grid>
    )
}
export default ImportOptions;