import { MODIFY_ITEM, MODIFY_ITEMS, buildModifyItems, buildDeleteItem, buildDeleteItems, FIND_ITEMS_LIST_BY_SERVICE_ID, FIND_ITEMS_BY_SERVICE_ID } from "./gql";
import { getContext } from "@ebs/layout";
import { client } from "utils/apollo";
import { showMessage } from "store/modules/message";
import { buildQuery, buildCreateMutation } from "./gql";
import { restClient } from "utils/axios/axios";
import { printoutClient } from "./axios";
import Papa from 'papaparse';
import * as XLSX from 'xlsx';

export const modifyItem = async (values) => {
  if (values.id) {
    try {
      let ShipmentItemInput = {
        id: values.id,
        weight: Number(values.weight),
        tenantId: 1,
        shipmentId: values.shipmentId,
        germplasmId: values.germplasmId,
        seedId: values.seedId,
        number: values.number,
        code: values.code,
        status: values.status,
        packageUnit: values.packageUnit,
        packageCount: values.packageCount,
        testCode: values.testCode,
        remarks: values.remarks,
      };
      const { data } = await client.mutate({
        mutation: MODIFY_ITEM,
        variables: { shipmentItem: ShipmentItemInput },
      });

    } catch (error) {
      console.log(error);
    }
  }
};
export const updateRow = async (Input) => {
  if (!Input[0].id) return;
  try {
    await client.mutate({
      mutation: MODIFY_ITEMS,
      variables: {
        shipmentItems: ShipItemInput,
      },
    });
  } catch (error) {
    console.log(error);
  }
};
export const modifyItems = async (data) => {
  const { input, id, service } = data;
  let entity = service.entity;
  const MUTATE = buildModifyItems(entity)
  let variables = { [`${entity.toLowerCase()}Items`]: input }
  if (id)
    variables[`${entity.toLowerCase()}Id`] = id
  try {
    await client.mutate({
      mutation: MUTATE,
      variables,
    });
  } catch (error) {
    console.log(error);
  }
};
async function findItems(service) {
  try {
    const apiContent = [{
      accessor: "id"
    }]
    const entity = `ServiceItem`;
    const QUERY = buildQuery(entity, apiContent);
    const { data } = await client.query({
      query: QUERY,
      variables: {
        filters: [{ col: `service.id`, mod: "EQ", val: Number(service) }]
      },
      fetchPolicy: "no-cache"
    })
    return data[`find${entity}List`].totalElements;
  } catch (error) {
    console.error(error);
    return 0
  }
}
export async function findItemsByServiceId(service) {
  try {
    const apiContent = [{
      accessor: "packageId"
    }]
    const entity = `ServiceItem`;
    const QUERY = buildQuery(entity, apiContent);
    const { data } = await client.query({
      query: QUERY,
      variables: {
        page: { number: 1, size: 1 },
        filters: [{ col: `service.id`, mod: "EQ", val: Number(service.id) }]
      },
      fetchPolicy: "no-cache"
    })
    return data[`find${entity}List`].content;
  } catch (error) {
    console.error(error);
    return [];
  }
}
export const createItem = async (items) => {
  try {
    let mode = items.sortMode;
    const isDangerous = items.isDangerous;
    const MUTATION = buildCreateMutation(`ServiceItems`)
    let totalItems = await findItems(items.serviceId);
    let checkTotal = totalItems;
    let input = [];
    items.members.forEach(async (item) => {
      totalItems++;
      input.push({
        id: 0,
        serviceId: Number(items.serviceId),
        germplasmId: item.germplasmDbId,
        seedId: item.seedDbId,
        packageId: item.packageDbId,
        number: mode === "add" && checkTotal > 0 ? 0 : totalItems,
        code: item.germplasmCode,
        status: "Unverified",
        weight: 0,
        packageUnit: item.unit,
        packageCount: 0,
        testCode: item.rsht || "NA",
        mtaStatus: item.mtaStatus || "NA",
        availability: "",
        use: "",
        smtaId: "",
        mlsAncestors: "NA",
        remarks: "",
        packageName: item.packageCode || "NA",
        packageLabel: item.label || "NA",
        seedName: item.seedName || "NA",
        seedCode: item.GID || "NA",
        designation: item.designation,
        parentage: item.parentage,
        taxonomyName: item.taxonomyName || "NA",
        isDangerous: isDangerous,
        tenantId: getContext().tenantId,
        origin: item.origin || "NA",
        geneticStock: item.geneticStock || "NA",
        listId: items.listId,
        germplasmName: item.germplasmName,
        otherNames:item.otherNames,
        sourceStudy: item.sourceOccurrenceName || "NA"
      });

    });

    await client.mutate({
      mutation: MUTATION,
      variables: { [`${items.service.entity.toLowerCase()}Items`]: input },
    });
    if (mode === "add") {
      await reorderItems(items.serviceId, items.service.entity)
    }
    return { error: false }
  } catch (error) {
    console.error(error)
    return { error: true, message: JSON.stringify(error) }
  }
};
export const reorderItems = async (id, entity) => {
  try {
    return await restClient.get(`reorder-items/${entity}/${id}`);
  } catch (error) {
    console.error(error)
  }

}

export const sendMessage = (variant, message, dispatch) => {
  dispatch(
    showMessage({
      message: message,
      variant: variant,
      anchorOrigin: {
        vertical: "top",
        horizontal: "right",
      },
    })
  );
}
export const deleteBulk = async (data) => {
  const { id, service } = data;
  let entity = service.entity;
  const MUTATION = buildDeleteItems(entity)
  try {
    await client.mutate({
      mutation: MUTATION,
      variables: {
        [`${entity.toLowerCase()}Id`]: id,
        ids: [0],
      },
    });
  } catch (error) {
    console.error(error);
  }
};
export const deleteItem = async (data) => {
  const { id, service } = data
  try {
    const MUTATION = buildDeleteItem(service.entity)
    await client.mutate({
      mutation: MUTATION,
      variables: { id: id },
    });
  } catch (error) {
    console.error(error);
  }
};

async function uploadTxtFile(file, service, dispatch) {
  try {
    let entityName = service.entity;
    entityName = entityName.toLowerCase() === "service" ? "workflow" : "shipment";
    var bodyFormData = new FormData();
    bodyFormData.append("shipmentId", service.id);
    bodyFormData.append("entity", entityName);
    bodyFormData.append("file", file);
    await restClient.post(`/importFiles/shipmentItems`, bodyFormData);
  } catch (error) {
    sendMessage("error", `${error.toString()}!. Please review the data format in the items template.`, dispatch)
  }

};
export async function templateDownload(dispatch) {
  try {
    const { data, status } = await printoutClient.get(
      `/api/FileManager/downloadDocument?file_name=items_import_template.csv`
    );
    if (status === 200) {
      const blob = new Blob([data], { type: "text/csvcharset=utf-8" });
      if (navigator.msSaveBlob) {
        navigator.msSaveBlob(blob, "items-import.csv");
      } else {
        var link = document.createElement("a");
        if (link.download !== undefined) {
          var url = URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", "items-import.csv");
          link.style.visibility = "hidden";
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
        }
      }
    } else {
      throw new Error(`Server Error`);
    }
  } catch (error) {
    sendMessage("error", `Server error: ${error.toString()}`, dispatch);
  }
};
export const submitToServer = async (files, service, dispatch, refresh) => {
  const file = files[0];
  Papa.parse(file, {
    header: false,
    skipEmptyLines: true,
    complete: async function (results) {
      results.data.shift();
      const txtData = results.data.map((row) => row.join(',')).join('\n');
      const newFile = new Blob([txtData], { type: 'text/plain' });
      await uploadTxtFile(newFile, service, dispatch);
      refresh();
    },
  });
}

function convertToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = '';
  return array.reduce((str, next) => {
    str +=
      `${Object.values(next)
        .map((value) => `"${value}"`)
        .join(',')}` + '\r\n';
    return str;
  }, str);
}

export function exportCSVFile(headers, data, fileTitle) {
  if (headers) {
    data.unshift(headers);
  }
  console.log(data)
  var jsonObject = JSON.stringify(data);
  var csv = convertToCSV(jsonObject);
  var exportedFilename = fileTitle + '.csv' || 'Items.csv';
  var blob = new Blob([csv], { type: 'text/csvcharset=utf-8' });
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, exportedFilename);
  } else {
    var link = document.createElement('a');
    if (link.download !== undefined) {
      // feature detection
      // Browsers that support HTML5 download attribute
      var url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', exportedFilename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

export async function downloadCSVData(serviceId,fileName) {

  try {

    const data = await getData(serviceId);
    let headers = {
      id: "Item ID",
      number: "Item Number",
      testCode: "RSHT/GHT",
      referenceNumber: "GH Reference Number",
      seedName: "Seed Name",
      seedCode: "Seed Code",
      germplasmName: "Germplasm Name",
      packageName: "Package Code",
      status: "Status"
    };
    let map = {
      id: "id",
      number: "number",
      testCode: "testCode",
      referenceNumber: "referenceNumber",
      seedName: "seedName",
      seedCode: "seedCode",
      germplasmName: "germplasmName",
      packageName: "packageName",
      status: "status"
    };
    let formattedData = [];
    // * Building formattedData
    for (let i = 0; i < data.length; i++) {
      const item = data[i]
      let mapKeys = Object.keys(map)
      let objTransformed = {}
      for (let i = 0; i < mapKeys.length; i++) {
        const _k = mapKeys[i]
        if (!item[_k]) {
          objTransformed[_k] = " "
        } else {
          let _item = item[_k]
          if (Object.prototype.toString.call(_item) === '[object Object]') {
            let obj = _item['name']
            if (obj) {
              objTransformed[_k] = obj
            } else {
              objTransformed[_k] = _item
            }
          } else {
            objTransformed[_k] = _item
          }
        }
      }
      formattedData = [...formattedData, objTransformed];
    };

    // * Export Data to CSV
    for (let i = 0; i < formattedData.length; i++) {
      let item = formattedData[i];
      let obj = item;
      let objKeys = Object.keys(obj);
      for (let i = 0; i < objKeys.length; i++) {
        let key = objKeys[i];
        if (Object.prototype.toString.call(item[key]) === '[object Object]') {
          let _obj = item[key];
          let _objKeys = Object.keys(_obj);
          let stringValue = [];
          _objKeys.forEach(_key => {
            if (typeof _obj[_key] === "string")
              stringValue = [...stringValue, _obj[_key]]
          })
          item[key] = stringValue.join(",")
        }
        if (item[key] === null) {
          item[key] = " "
        }
        if (Array.isArray(item[key])) {
          item[key] = item[key].length
        }
      }
    }
console.log(headers)
    exportCSVFile(headers, formattedData, fileName);

  } catch (error) {
    console.log(error)
  }


}


async function getData(id) {

  let totalData = [];
  let number = 1;
  const { data } = await client.query({
    query: FIND_ITEMS_BY_SERVICE_ID,
    variables: {
      sort: { col: "number", mod: "ASC" },
      page: { number: number, size: 300 },
      filters: [{ col: "service.id", mod: "EQ", val: id }],
    },
    fetchPolicy: "no-cache",
  });
  number = data.findServiceItemList.totalPages;

  totalData = [...totalData, ...data.findServiceItemList.content];
  if (number > 1) {
    for (let i = 2; i <= number; i++) {
      const { data } = await client.query({
        query: FIND_ITEMS_BY_SERVICE_ID,
        variables: {
          sort: { col: "number", mod: "ASC" },
          page: { number: i, size: 300 },
          filters: [{ col: "service.id", mod: "EQ", val: id }],
        },
        fetchPolicy: "no-cache",
      });
      totalData = [...totalData, ...data.findServiceItemList.content];
    }
  }
  return totalData;
}



function convertXLSXFile(columns,file, callback) {
  const reader = new FileReader();

  reader.onload = (e) => {
    const data = e.target.result;
    const workbook = XLSX.read(data, { type: 'binary' });

    const sheetName = workbook.SheetNames[0];
    const sheet = workbook.Sheets[sheetName];

    const jsonData = XLSX.utils.sheet_to_json(sheet, { header: 1 });
let headersName = jsonData[0];
    jsonData.shift();
    const parsedData = jsonData.map(row => {
      const parsedRow = {};

      row.forEach((cell, index) => {
        let _header = headersName[index];
        let columnName = columns.find(item => item.csvHeader === _header).accessor;    
        if (columnName && columnName.length > 0)
          parsedRow[columnName] = cell !== undefined ? cell : "";
        if(columnName === "isDangerous")
            parsedRow[columnName] = (typeof cell === "boolean") ? cell : null;
      });
      return parsedRow;
    });
    callback(parsedData);
  };

  reader.readAsArrayBuffer(file);
}

export default convertXLSXFile;


export async function downloadItemsCSVData(props) {
  const { id, selectedRows, columns, fileName } = props;
  let data = [];
  let includeMlsAncestor = false;
  if (selectedRows.length > 0)
    data = selectedRows;
  else
    data = await getData(id);
  let headers = {};
  let map = {};
  for (let i = 0; i < columns.length; i++) {
    const column = columns[i];
    if (column.csvHeader === 'RSHT') {
      includeMlsAncestor = true;
    }
    if (column.accessor === "id") {
      map[column.accessor.split('.')[0]] = column.accessor;
      headers[column.accessor.split('.')[0]] = column.csvHeader;
    }
    else if (!column.hidden && column.accessor !== "receivedDate") {
      map[column.accessor.split('.')[0]] = column.accessor;
      headers[column.accessor.split('.')[0]] = column.csvHeader;
    }
  }
  if (includeMlsAncestor) {
    map['mlsAncestors'] = 'mlsAncestors';
    headers['mlsAncestors'] = 'MLS Ancestors';
  }
  let formattedData = [];
  for (let i = 0; i < data.length; i++) {
    const item = data[i]
    let mapKeys = Object.keys(map)
    let objTransformed = {}
    for (let i = 0; i < mapKeys.length; i++) {
      const _k = mapKeys[i]
      if (!item[_k]) {
        objTransformed[_k] = ""
      } else {
        let _item = item[_k]
        if (Object.prototype.toString.call(_item) === '[object Object]') {
          let obj = _item['label']
          if (obj) {
            objTransformed[_k] = obj
          } else {
            objTransformed[_k] = _item
          }
        } else {
          objTransformed[_k] = _item
        }
      }
    }
    formattedData = [...formattedData, objTransformed];
  };

  for (let i = 0; i < formattedData.length; i++) {
    let item = formattedData[i];
    let obj = item;
    let objKeys = Object.keys(obj);
    for (let i = 0; i < objKeys.length; i++) {
      let key = objKeys[i];
      if (Object.prototype.toString.call(item[key]) === '[object Object]') {
        let _obj = item[key];
        let _objKeys = Object.keys(_obj);
        let stringValue = [];
        _objKeys.forEach(_key => {
          if (typeof _obj[_key] === "string")
            stringValue = [...stringValue, _obj[_key]]
        })
        item[key] = stringValue.join(",")
      }
      if (item[key] === null) {
        item[key] = "";
      }
      if (key === "isDangerous") {
        if(item[key] === ""){
          item[key] = "FALSE"
        }
      }
      if (Array.isArray(item[key])) {
        item[key] = item[key].length
      }
    }
  }
  console.log(headers)
  exportCSVFile(headers, formattedData, fileName);

}

export function formattedDate(){
  const currentDate = new Date();

const year = currentDate.getFullYear();
const month = String(currentDate.getMonth() + 1).padStart(2, '0');
const day = String(currentDate.getDate()).padStart(2, '0');

const hour = String(currentDate.getHours()).padStart(2, '0');
const minute = String(currentDate.getMinutes()).padStart(2, '0');
const period = currentDate.getHours() >= 12 ? 'PM' : 'AM';

const formattedDate = `${year}${month}${day}-${hour}:${minute} ${period}`;
return formattedDate;
}