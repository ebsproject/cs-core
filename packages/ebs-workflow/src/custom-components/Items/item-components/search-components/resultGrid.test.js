import ResultGrid from "./resultGrid";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
import { useServiceContext } from "custom-components/context/service-context";

afterEach(cleanup)
const filter = [] ;
const germplamsCode = "Test"
const setReload= () =>{}
const reload = false
const setPackageData= () =>{} 

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn((service)=> service ),
  }));
  const mockServiceContext = { service: {} };

  beforeEach(() => {
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
  });

test("ResultGrid is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <ResultGrid germplamsCode={germplamsCode} setReload={setReload} filter={filter} reload={reload} setPackageData={setPackageData} ></ResultGrid>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("ResultGridTestId")).toBeInTheDocument();
});
