import { useQuery } from "@apollo/client";
import { Core, Lab } from "@ebs/styleguide";
import { buildQuery } from "../gql";
import { clientCB } from "utils/apollo/apollo";
import { userContext } from "@ebs/layout";
import { useContext, useEffect } from "react";
import { useServiceContext } from "custom-components/context/service-context";

const { Grid, TextField } = Core;
const { Autocomplete } = Lab;
const SearchComponent = ({ entity, setFilter, filter, disabled, row }) => {
    const { service } = useServiceContext();

    useEffect(() => {
        if (entity === "Program") {
            let ent = service.entity;
            let defaultProgram = row[`${ent.toLowerCase()}`].program;
            setFilter((filter) => [...filter, { col: "program.programName", val: defaultProgram.institution.legalName, mod: "EQ" }])
        }
    }, []);

    const handleYear = (e, value) => {
        if (!value){
            setFilter(filter.filter(item => item.col !== "seed.experiment.experimentYear"));
            return;
        }
        let a = filter.filter(item => Number(item.val) === Number(value));
        if (a.length > 0) return;
        setFilter((filter) => [...filter, { col: "seed.experiment.experimentYear", val: `${value}`, mod: "EQ" }]);
    }
    const handleType = (e, value) => {
        if (!value){
            setFilter(filter.filter(item => item.col !== "seed.experiment.experimentType"));
            return;
        }
        let a = filter.filter(item => item.val === value);
        if (a.length > 0) return;
        setFilter((filter) => [...filter, { col: "seed.experiment.experimentType", val: `${value}`, mod: "EQ" }])
    }
    const handleChangeFilter = (e, value) => {
        switch (entity) {
            case "Program":
                if (!value) {
                    setFilter(filter.filter(item => item.col !== "program.programName"));
                    break;
                }
                let a = filter.filter(item => item.val === value.name);
                if (a.length > 0) break;
                setFilter((filter) => [...filter, { col: "program.programName", val: value.name, mod: "EQ" }])
                break;
            case "Season":
                if (!value) {
                    setFilter(filter.filter(item => item.col !== "seed.experiment.season.seasonName"));
                    break;
                }
                let b = filter.filter(item => item.val === value.seasonName);
                if (b.length > 0) break;
                setFilter((filter) => [...filter, { col: "seed.experiment.season.seasonName", val: value.seasonName, mod: "EQ" }])
                break;
            case "Occurrence":
                if (!value) {
                    setFilter(filter.filter(item => item.col !== "seed.experiment.occurrences.occurrenceName"));
                    break;
                }
                let c = filter.filter(item => item.val === value.occurrenceName);
                if (c.length > 0) break;
                setFilter((filter) => [...filter, { col: "seed.experiment.occurrences.occurrenceName", val: value.occurrenceName, mod: "EQ" }])
                break;
            default:
                break;
        }

    }
    switch (entity) {
        case "ExperimentYear":
            const currentYear = new Date().getFullYear();
            const years = Array.from({ length: 50 }, (_, index) => currentYear - index);
            return (
                <Grid item xs={3} data-testid={"SearchComponentTestId"}>
                    <Autocomplete
                        disabled={disabled}
                        id={`${entity}-id`}
                        options={years}
                        getOptionLabel={(option) => `${option}`}
                        renderInput={(params) => <TextField {...params} label={entity} variant="outlined" />}
                        onChange={handleYear}
                    />
                </Grid>
            );
        case "Program":
            const { userProfile } = useContext(userContext);
            let programOptions = userProfile.permissions.memberOf.programs
            let ent = service.entity;
            let defaultProgram = row[`${ent.toLowerCase()}`].program;
            return (
                <Grid item xs={3} data-testid={"SearchComponentTestId"}>
                    <Autocomplete
                        disabled={false}
                        id={`${ent}-id`}
                        options={programOptions}
                        getOptionLabel={(option) => option[`name`]}
                        renderInput={(params) => <TextField {...params} label={"Program"} variant="outlined" />}
                        onChange={handleChangeFilter}
                    />
                </Grid>
            )

        case "ExperimentType":
            return (
                <Grid item xs={3} data-testid={"SearchComponentTestId"}>
                    <Autocomplete
                        disabled={disabled}
                        id={`${entity}-id`}
                        options={["Breeding Trial", "Generation Nursery", "Intentional Crossing Nursery"]}
                        getOptionLabel={(option) => `${option}`}
                        renderInput={(params) => <TextField {...params} label={entity} variant="outlined" />}
                        onChange={handleType}
                    />
                </Grid>
            )
        default:
            let entityName = entity === "SubFacility" ? "Facility" : entity;
            const field = entityName.charAt(0).toLowerCase() + entityName.slice(1);
            let content = [{ accessor: "id" }, { accessor: `${field}Name` }];
            let column = entity === "Program" ? "program.programName" : "id";

            if (entity === "SubFacility" || entity === "Facility")
                content.push({ accessor: "parentFacility.id" });

            const BUILD_QUERY = buildQuery(entityName, content);
            let variables = { page: { size: 1000, number: 1 } }
            const { loading: _loading, error: _error, data: _data } = useQuery(BUILD_QUERY, {
                fetchPolicy: "cache-first",
                variables,
                client: clientCB
            });
            if (_loading) return 'Loading Filter...';
            if (_error) return `Error! ${_error.message}`;

            let dataOptions = _data[`find${entityName}List`].content;
            if (entity === "SubFacility")
                dataOptions = dataOptions.filter(item => item.parentFacility !== null);
            if (entity === "Facility")
                dataOptions = dataOptions.filter(item => item.parentFacility === null);

            return (
                <Grid item xs={3} data-testid={"SearchComponentTestId"}>
                    <Autocomplete
                        disabled={disabled}
                        id={`${entityName}-id`}
                        options={dataOptions}
                        getOptionLabel={(option) => option[`${field}Name`]}
                        renderInput={(params) => <TextField {...params} label={entity === "SubFacility" ? entity : entityName} variant="outlined" />}
                        onChange={handleChangeFilter}
                    />
                </Grid>
            )
    }
}
export default SearchComponent;