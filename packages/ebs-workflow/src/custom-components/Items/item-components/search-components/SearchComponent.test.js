import SearchComponent from ".";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";
import { Provider } from "react-redux";
import store from "store";
import { IntlProvider } from "react-intl";
import { useServiceContext } from "custom-components/context/service-context";

afterEach(cleanup)
const entity = "ExperimentYear" ;
const setFilter = () =>{} ;
const filter = [] ;
const disabled = false ;
const row = {service:{program:{institution:{legalName:"TEST"}}}} ;

jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn((service)=> service ),
  }));
  const mockServiceContext = { service: {} };

  beforeEach(() => {
    useServiceContext.mockReturnValue(mockServiceContext);
    jest.clearAllMocks();
  });

test("SearchComponent is in the DOM", () => {
  render(
    <Provider store={store}>
      <IntlProvider locale="en">
        <SearchComponent entity={entity} setFilter={setFilter} filter={filter} disabled={disabled} row={row} ></SearchComponent>
      </IntlProvider>
    </Provider>
  );
  expect(screen.getByTestId("SearchComponentTestId")).toBeInTheDocument();
});
