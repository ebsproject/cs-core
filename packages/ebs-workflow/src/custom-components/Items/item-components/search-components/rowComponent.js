import { useEffect } from "react";

const RowComponent = ({ selection, refresh, reload, setReload, setPackageData }) => {
    useEffect(() => {
        if (reload){
            refresh();
        }
        return () => setReload(false)
    }, [reload]);

    useEffect(() => {
  
        if (selection.length > 0)
            setPackageData(selection[0])
        if (selection.length === 0)
            setPackageData(null)

    }, [selection,setPackageData]);
    
    return null;
}
export default RowComponent;