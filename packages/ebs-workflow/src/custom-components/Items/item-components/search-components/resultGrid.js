import { EbsGrid } from "@ebs/components";
import { columnsPackage } from "./gridColumns";
import RowComponent from "./rowComponent";
import { getCoreSystemContext } from "@ebs/layout"
import {Core} from "@ebs/styleguide"
const {Chip} = Core;

const ResultGrid = ({ germplamsCode, filter, setReload, reload, setPackageData }) => {
    const { cbGraphqlUri } = getCoreSystemContext();

    const refreshComponent = (selection, refresh) => {

        return (
            <RowComponent selection={selection || []} refresh={refresh} setReload={setReload} reload={reload} setPackageData={setPackageData} germplamsCode={germplamsCode} />
        )
    }

    return (
        <div data-testid={'ResultGridTestId'}>
        <EbsGrid
            columns={columnsPackage}
            entity={"Package"}
            toolbar={true}
            toolbaractions={refreshComponent}
            id={`PackageGrid`}
            height="65vh"
            raWidth={85}
            select="single"
            callstandard="graphql"
            uri={cbGraphqlUri}
            defaultFilters={germplamsCode === "" ? filter : [...filter, ...[{ col: `seed.germplasm.germplasmCode`, mod: "EQ", val: germplamsCode }]]}
            disabledViewConfiguration={true}
            disabledViewDownloadCSV={true}
            disabledViewPrintOunt={true}
        />
        </div>

    )
}
export default ResultGrid;