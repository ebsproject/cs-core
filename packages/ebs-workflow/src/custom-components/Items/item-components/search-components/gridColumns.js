
import { Core } from "@ebs/styleguide";
const { Typography } = Core;

export const columnsPackage =
    [
        {
            Header: "id",
            accessor: "id",
            hidden: true,
            disableGlobalFilter: true,
        },
        {
            Header: "pakUni",
            accessor: "packageUnit",
            hidden: true,
            disableGlobalFilter: true,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    {"Package Label"}
                </Typography>
            ),
            csvHeader: "packageLabel",
            accessor: "packageLabel",
            width: 150,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    {"Package Code"}
                </Typography>
            ),
            csvHeader: "packageCode",
            accessor: "packageCode",
            width: 150,
        },
        {
            Header: (
                <Typography variant="h6" color="primary" style={{textAlign:"center"}}>
                    {"Package Quantity"}
                </Typography>
            ),
            csvHeader: "packageQuantity",
            accessor: "packageQuantity",
            width: 180,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    {"Seed Name"}
                </Typography>
            ),
            csvHeader: "SeedName",
            accessor: "seed.seedName",
            width: 120,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    {"Seed Code"}
                </Typography>
            ),
            csvHeader: "SeedCode",
            accessor: "seed.seedCode",
            width: 120,
        },
        {
            Header: (
                <Typography variant="h6" color="primary">
                    {"Germplasm Code"}
                </Typography>
            ),
            csvHeader: "GermplasmCode",
            accessor: "seed.germplasm.germplasmCode",
            width: 160,
        },
    ];





