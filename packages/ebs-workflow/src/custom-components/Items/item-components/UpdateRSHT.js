import React, { useState } from "react";
import { Core } from "@ebs/styleguide";
import { useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { modifyItems } from "./functions";
import { sendMessage } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { restClient } from "utils/axios/axios";
// CORE COMPONENTS
const {
  Radio,
  RadioGroup,
  Button,
  Grid,
  FormControl,
  FormControlLabel,
  TextField,
  Typography,
  CircularProgress,
  Box,
} = Core;
//MAIN FUNCTION
/*
 @param { }: component properties
 @param ref: reference made by React.forward
*/
const UpdateRSHT = React.forwardRef(
  ({ refresh, handleClose, selection }, ref) => {
    const [loading, setLoading] = useState(false);
    const [selectionOption, setSelectionOption] = useState("Selected");
    const [showLabel, setShowLabel] = useState(false);
    const dispatch = useDispatch();
    const { service } = useServiceContext();
   // const initValue = "";
    const { control, handleSubmit, formState: { errors },} = useForm();
    const [action, setAction] = React.useState("manually");
    const handleChange = (event) => {
      setAction(event.target.value);
    };
    const onSubmit = async (value) => {
      setLoading(true);
      if (selectionOption === 'Selected') {
        if (selection.length === 0) {
          sendMessage("error", "No item selected for updating", dispatch);
          setLoading(false);
          return;
        } else {
          let input=[]
          for (let i = 0; i < selection.length; i++) {
              input = [...input,{
              id: selection[i].id,
             // status: "liberated",
              testCode: value.rsht,
            }];
          }
          await modifyItems({ input, service })
          sendMessage("success", "The selected items were updated", dispatch);
          refresh();
          setLoading(false);
          handleClose();
          return;
        }
      }
      else {
        if (action === "api") {
          setLoading(false);
          setShowLabel(true);
          const result = await restClient.get(`/shipment/${Number(service.id)}/items/graph-update`);
          return;
        }
        let input = [{
          id: 0,
          //status: "liberated",
          testCode: value.rsht
        }]

        await modifyItems({ input, service, id: service.id })
        sendMessage("success", "Update completed successfully", dispatch);
        setLoading(false);
        refresh();
        handleClose();
        return
      }
    };
    return (
      /* 
     @prop data-testid: Id to use inside ShipmentBrowserButtonRefresh.test.js file.
     */
      <div data-testid="UpdateRSHTTestId">
        <form onSubmit={handleSubmit(onSubmit)}>
          {/* <Typography variant={"h5"}>
            {`How would you like to update  ${option} values`}
          </Typography> */}

          <Grid container>
            <Grid item xs={12}>
              <FormControl componet="fieldset">
                <RadioGroup
                  name="row-radio-buttons-group"
                  value={action}
                  onChange={handleChange}
                >
                  {/* <FormControlLabel
                    value="api"
                    control={<Radio />}
                    label="Import from Core Breeding"
                  /> */}
                  <FormControlLabel
                    value="manually"
                    control={<Radio />}
                    label="Update manually"
                  />
                  <Controller
                    name="rsht"
                    control={control}
                    render={({ field }) => (
                      <TextField
                        variant={"outlined"}
                        disabled={action !== "api" ? false : true}
                        {...field}
                      //  value={initValue}
                        label={"Enter value"}
                        data-testid={"rsht"}
                      />
                    )}
                  />
                </RadioGroup>
              </FormControl>
            </Grid>
            <Grid item xs={12}>
              <Box display="flex" justifyContent="flex-end">

                <Button onClick={handleClose}>
                  {"Cancel"}
                </Button>
                {loading ? (<CircularProgress />) : (
                  <div>
                    <Grid container spacing={2}>
                      <Grid item>
                        <Button
                          disabled={action === "api" ? true : false}
                          className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                          onClick={() => setSelectionOption("Selected")} type="submit">
                          {"Apply to selected"}
                        </Button>
                      </Grid>
                      <Grid item>
                        <Button
                          disabled={action === "api" && showLabel ? true : false}
                          className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                          onClick={() => setSelectionOption("All")} type="submit">
                          {action === "api" ? "start process" : "Apply to all"}
                        </Button>
                      </Grid>
                    </Grid>
                  </div>
                )}
              </Box>
            </Grid>
          </Grid>
          <br />
          {showLabel && (
            <Typography variant={"title"} style={{ color: "red" }}>
              {"*You can navigate away, the system will notify you when the process is completed."}
            </Typography>
          )}
        </form>
      </div>
    );
  }
);
// Type and required properties
UpdateRSHT.propTypes = {};
// Default properties


export default UpdateRSHT;
