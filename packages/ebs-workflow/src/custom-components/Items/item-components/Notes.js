import React, { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
// GLOBALIZATION COMPONENT
import { Core, EbsDialog, Icons } from "@ebs/styleguide";
// CORE COMPONENTS
import { useUserContext } from "@ebs/layout";
import { useDispatch } from "react-redux";
import { useForm, Controller } from "react-hook-form";
import { modifyItems, sendMessage } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
const { Message } = Icons;
const { Grid, Button, DialogActions, DialogContent, IconButton, TextField, Tooltip, Typography } = Core

const Notes = ({ refresh, rowData }) => {
    const [open, setOpen] = useState(false);
    const initValue = null;
    const dispatch = useDispatch();
    const handleClickOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [hasNote, setHasNote] = useState(false);
    const [note, setNote] = useState("")
    const [oldNotes, setOldNotes] = useState("");
    const { control, handleSubmit, } = useForm();
    const { userProfile } = useUserContext();
    const { service } = useServiceContext();
    useEffect(() => {
        let mounted = true
        if (mounted) {
            if (rowData) {
                setHasNote(true);
                setOldNotes(rowData.remarks);
                setNote(rowData.remarks)
            } else {
                setHasNote(false);
            }
        }
        return () => mounted = false;
    }, [rowData]);
    const onSubmit = async (data, event) => {
        event.preventDefault();
        try {
            let input = [{
                id: rowData.id,
                remarks: note,
            }];
            await modifyItems({ input, service });
            sendMessage('success', 'The note was saved successfully', dispatch);
            setOpen(false);
            refresh();
        } catch (error) {
            console.log(error)
            setOpen(false);
        }
    };
    const onChangeInput = (event) => {
        setNote(`${oldNotes} \n(${userProfile.full_name} :  ${new Date()}): \n ${event.target.value}.`)
    }

    return (
        <div data-testid={"ItemNotesId"}>
            <Tooltip
                arrow
                title={
                    <Typography className="font-ebs text-xl">
                        {"Notes"}
                    </Typography>
                }
            >
                <IconButton
                data-testid={"ButtonOpenModalMessageTestId"}
                    onClick={handleClickOpen}
                    aria-label="package-note"
                    color="primary"
                >
                    <Message />
                </IconButton>
            </Tooltip>
            <EbsDialog
                open={open}
                handleClose={handleClose}
                maxWidth="sm"
                title={"Add Notes"}
            >
                <form onSubmit={handleSubmit(onSubmit)} data-testid="AddPackageNotes">
                    <DialogContent dividers>
                        <Grid container spacing={3}>
                            <Grid item xs={12}>
                                {hasNote && (
                                    <TextField
                                        fullWidth
                                        variant="standard"
                                        multiline
                                        rows={12}
                                        label={
                                            "Notes"
                                        }
                                        value={note}
                                        InputProps={{
                                            readOnly: true,
                                        }}
                                    />
                                )}
                            </Grid>

                            <Grid item xs={12}>
                                <Controller
                                    name="newNote"
                                    control={control}
                                    render={({ field }) => (
                                        <TextField
                                            inputRef={input => input && input.focus()}
                                            {...field}
                                            value={initValue}
                                            fullWidth
                                            multiline
                                            onChange={onChangeInput}
                                            rows={4}
                                            label={
                                                "Add Notes"
                                            }
                                            data-testid={"newNote"}
                                        />
                                    )}
                                />
                            </Grid>
                        </Grid>
                    </DialogContent>

                    <DialogActions>
                        <Button onClick={handleClose}>
                            {"Cancel"}
                        </Button>
                        <Button type="submit">
                            {"Save"}
                        </Button>
                    </DialogActions>
                </form>
            </EbsDialog>
        </div>
    );
}
// Type and required properties
Notes.propTypes = {};
// Default properties


export default Notes;
