import ImportOptions from "./ImportOptions";
import { render, cleanup, screen, fireEvent } from "@testing-library/react";
import "@testing-library/dom";
import { templateDownload } from "./functions";

jest.mock("./functions", () => ({
  templateDownload: jest.fn()
}))
jest.mock("react-redux", () => ({
  useDispatch: jest.fn(),
}));

describe("Import Options component", () => {
  const handleClose = jest.fn()

  beforeEach(() => {
    jest.clearAllMocks();
  })

  const renderComponent = (props) => render(<ImportOptions handleClickOpen={props.handleClickOpen} setOpenFileUpload={props.setOpenFileUpload} />);

  test("ImportOptions is in the DOM", () => {
    const props = {
      handleClickOpen: jest.fn(),
      setOpenFileUpload: jest.fn()
    }
    renderComponent(props);
    expect(screen.getByTestId("ButtonInToolbarTestId")).toBeInTheDocument();
  });

  test("open menu when click", () => {
    const props = {
      handleClickOpen: jest.fn(),
      setOpenFileUpload: jest.fn()
    }
    renderComponent(props);
    const button = screen.getByTestId("ButtonInToolbarTestId")
    fireEvent.click(button);
    expect(screen.getByText(/Import Package List/i)).toBeInTheDocument();
    expect(screen.getByText(/From Core Breeding/i)).toBeInTheDocument();
    expect(screen.getByText(/From CSV file/i)).toBeInTheDocument();
    expect(screen.getByText(/Download CSV file template/i)).toBeInTheDocument();
  });

  test("open menu when click and select  Download CSV file template", () => {
    const props = {
      handleClickOpen: jest.fn(),
      setOpenFileUpload: jest.fn()
    }
    renderComponent(props);
    const button = screen.getByTestId("ButtonInToolbarTestId")
    fireEvent.click(button);
    const buttonList = screen.getByText("Download CSV file template");
    fireEvent.click(buttonList);
    expect(templateDownload).toHaveBeenCalled();
  });

  test("open menu when click and select From CSV file", () => {
    const props = {
      handleClickOpen: jest.fn(),
      setOpenFileUpload: jest.fn()
    }
    renderComponent(props);
    const button = screen.getByTestId("ButtonInToolbarTestId")
    fireEvent.click(button);
    const buttonList = screen.getByText("From CSV file");
    fireEvent.click(buttonList);
    expect(props.setOpenFileUpload).toHaveBeenCalled();
  });

   
  test("open menu when click and then click away", () => {
    const props = {
      handleClickOpen: jest.fn(),
      setOpenFileUpload: jest.fn()
    }
    renderComponent(props);
    const button = screen.getByTestId("ButtonInToolbarTestId")
    fireEvent.click(button);
    screen.debug()
    console.log(document.body)
    fireEvent.mouseDown(document.body);
   //expect(handleClose).toHaveBeenCalled();
  });

})

