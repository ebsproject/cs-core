import React, { useState, useEffect } from "react";
import { Core } from "@ebs/styleguide";
import { findItemsByServiceId, sendMessage } from "./functions";
import { useServiceContext } from "custom-components/context/service-context";
import { restClient } from "utils/axios/axios";
import { useDispatch } from "react-redux";
const { Grid, Box, Button, Typography, LinearProgress } = Core;

const SyncComponent = ({ handleClose, refresh, option }) => {
    const [loading, setLoading] = useState(false);
    const { service } = useServiceContext();
    const dispatch = useDispatch();



    const syncFunction = async () => {
        setLoading(true)
       const response = await restClient.get(`/shipment/${service.id}/items/all-data`);
        sendMessage("success", response["data"] + ". You will receive a notification when the synchronization process finish.", dispatch);
        setLoading(false);
        handleClose();
        refresh();
    }

    return (
        <div data-testid={"SyncComponentTestId"}>
            <Grid item xs={12}>
                <Typography variant={"body1"} >
                    {`${option}, Taxonomy Name, Germplasm data, Package data and Seed data will be updated.`}
                </Typography>
                <Box>&nbsp;</Box>
                <Box display="flex" justifyContent="flex-end">
                    <Button onClick={handleClose}>
                        {"Cancel"}
                    </Button>
                    <div>
                        <Grid container spacing={2}>
                            <Grid item>
                                <Button
                                    disabled={loading}
                                    className="bg-ebs-brand-default hover:bg-ebs-brand-900 text-white"
                                    onClick={syncFunction}>
                                    {"Start"}
                                </Button>
                            </Grid>
                        </Grid>
                    </div>
                    {loading && <LinearProgress />}
                </Box>
            </Grid>
        </div>
    )
}
export default SyncComponent;