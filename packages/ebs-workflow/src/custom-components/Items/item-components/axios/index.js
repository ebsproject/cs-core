import axios from "axios";
import { getDomainContext, getTokenId, getCoreSystemContext } from "@ebs/layout";

const { sgContext } = getDomainContext("cb");
const { printoutUri } = getCoreSystemContext();

export const cbClient = axios.create({
  baseURL: sgContext,
  headers: {
    Accept: "application/json",
  },
});
cbClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});


export const printoutClient = axios.create({
  baseURL: printoutUri,
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

printoutClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});