import { gql } from "@apollo/client";

export const MODIFY_ITEM = gql`
  mutation MODIFY_ITEM($shipmentItem: ShipmentItemInput!) {
    modifyShipmentItem(shipmentItem: $shipmentItem) {
      id
    }
  }
`;
export const MODIFY_ITEMS = gql`
  mutation MODIFY_ITEMS($shipmentId: Int, $shipmentItems: [ShipItemInput!]!) {
    modifyShipmentItems(shipmentId: $shipmentId, shipmentItems: $shipmentItems)
  }
`;

export const buildModifyItems = (entity) => {
  return gql`
  mutation MODIFY_ITEMS(
    $${entity.toLowerCase()}Id: Int
    $${entity.toLowerCase()}Items: [${entity.slice(0, 4)}ItemInput!]!
  ) {
    modify${entity}Items(${entity.toLowerCase()}Id: $${entity.toLowerCase()}Id, ${entity.toLowerCase()}Items: $${entity.toLowerCase()}Items)
  }
`;
};

export const CREATE_ITEMS = gql`
  mutation CREATE_ITEMS($shipmentItems: [ShipmentItemInput!]!) {
    createShipmentItems(shipmentItems: $shipmentItems)
  }
`;
export const DELETE_ITEMS = gql`
  mutation DELETE_ITEMS($shipmentId: Int, $ids: [Int!]!) {
    deleteShipmentItems(shipmentId: $shipmentId, ids: $ids)
  }
`;
export const buildDeleteItems = (entity) => {
  return gql`
  mutation DELETE_ITEMS($${entity.toLowerCase()}Id: Int, $ids: [Int!]!) {
    delete${entity}Items(${entity.toLowerCase()}Id: $${entity.toLowerCase()}Id, ids: $ids)
  }
`;
};
export const DELETE_ITEM = gql`
  mutation DELETE_ITEM($id: Int!) {
    deleteShipmentItem(id: $id)
  }
`;

export const buildDeleteMutation = (entity) => {
  return gql`
    mutation delete${entity}($id: Int!){
        delete${entity}(id:$id)
    }`;
};
export const buildDeleteItem = (entity) => {
  return gql`
    mutation DELETE_ITEM($id: Int!){
        delete${entity}Item(id:$id)
    }`;
};

export const FIND_TOTAL_ITEMS = gql`
  query FIND_TOTAL_ITEMS($filters: [FilterInput], $disjunctionFilters: Boolean = false) {
    findServiceItemList(filters: $filters, disjunctionFilters: $disjunctionFilters) {
      totalElements
      content{
        status
      }
    }
  }
`;
export const FIND_TOTAL_ITEMS_BY_MTA_STATUS = gql`
  query FIND_TOTAL_ITEMS_BY_MTA_STATUS($page: PageInput,$filters: [FilterInput], $disjunctionFilters: Boolean = false) {
    findServiceItemList(page:$page,filters: $filters, disjunctionFilters: $disjunctionFilters) {
      totalPages
      totalElements
      content{
        mtaStatus
        mlsAncestors
        smtaId
      }
    }
  }
`;
export const FIND_ITEMS_LIST_BY_SERVICE_ID = gql`
  query FIND_ITEMS_LIST_BY_SERVICE_ID($page: PageInput,$sort: [SortInput],$filters: [FilterInput], $disjunctionFilters: Boolean = false) {
    findServiceItemList(page:$page, sort:$sort,filters: $filters, disjunctionFilters: $disjunctionFilters) {
      totalElements
      totalPages
      content{
        id
        number
        testCode
        referenceNumber
        status
        seedCode
      }
    }
  }
`;
export const FIND_ITEMS_BY_SERVICE_ID = gql`
  query FIND_ITEMS_BY_SERVICE_ID($page: PageInput,$sort: [SortInput],$filters: [FilterInput], $disjunctionFilters: Boolean = false) {
    findServiceItemList(page:$page, sort:$sort,filters: $filters, disjunctionFilters: $disjunctionFilters) {
      totalElements
      totalPages
      content{
        id
        seedId
        seedName
        seedCode
        number
        testCode
        referenceNumber
        germplasmId
        receivedDate
        mtaStatus
        taxonomyName
        origin
        parentage
        isDangerous
        code
        packageId
        packageLabel
        packageName
        remarks
        mlsAncestors
        smtaId
        availability
        packageUnit
        status
        weight
        use
        geneticStock
        designation
        packageCount
        germplasmName
        otherNames
        sourceStudy 
      }
    }
  }
`;

export const FIND_TOTAL_FILES = gql`
  query FIND_TOTAL_FILES($filters: [FilterInput], $disjunctionFilters: Boolean = false) {
    findServiceFileList(filters: $filters, disjunctionFilters: $disjunctionFilters) {
      totalElements
      content {
        id
        fileType {
          description
        }
      }
    }
  }
`;
export const buildCreateMutation = (entity) => {
  return gql`
    mutation create${entity}($${entity.charAt(0).toLowerCase() + entity.slice(1)}: [${entity.slice(
    0,
    -1
  )}Input!]!){
        create${entity}(${entity.charAt(0).toLowerCase() + entity.slice(1)}:$${
    entity.charAt(0).toLowerCase() + entity.slice(1)
  })
    }`;
};

export const buildModifyMutation = (entity) => {
  return gql`
    mutation modify${entity}($${entity.charAt(0).toLowerCase() + entity.slice(1)}: [${entity.slice(
    0,
    -1
  )}Input!]!){
        modify${entity}(${entity.charAt(0).toLowerCase() + entity.slice(1)}:$${
    entity.charAt(0).toLowerCase() + entity.slice(1)
  })
    }`;
};
export const buildQuery = (entity, apiContent) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: [SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            content {
                ${apiContent.map((item) => {
                  const levels = item.accessor.split(".");
                  // Setting levels
                  return extractLevels(levels);
                })}
            }
        }
    }`;
};
function extractLevels(arr) {
  let levels = arr;
  if (levels.length > 1) {
    return `${levels.shift()}{
        ${extractLevels(levels)}
    }`;
  } else {
    return `${levels[0]}`;
  }
}
