
import { Core } from "@ebs/styleguide";
const { Typography, FormControl,Select, OutlinedInput, MenuItem } = Core;
import Rsht from "./item-components/Rsht";
import SelectStatus from "./item-components/SelectStatus";
import PackageQty from "./item-components/PackageQty";
import Volume from "./item-components/Volume";
import { RBAC } from "@ebs/layout";
import DangerousMark from "./item-components/DangerousMark";
import ReferenceNumber from "./item-components/ReferenceNumber";
import MTAStatus from "./item-components/MTAStatus";
import ReceivedDate from "./item-components/ReceivedDate";
import { useServiceContext } from "custom-components/context/service-context";

export function columnDefinition(setRefreshTable, entity, node) {
  const {mode} = useServiceContext();
  const SelectColumnFilter = ({
    column: { filterValue, setFilter },
  }) => {
    return (
      <FormControl variant="standard">
        <Select
          value={filterValue || ""}
          onChange={(e) => {
            let filterData = e.target.value;
            setFilter(filterData.toString());
          }}
          input={<OutlinedInput style={{ width: 160, height: 30 }} />}
          displayEmpty
        >
          <MenuItem value="">All</MenuItem>
          <MenuItem value="true">Dangerous</MenuItem>
          <MenuItem value="false">Not Dangerous</MenuItem>
        </Select>
      </FormControl>
    );
  };
  const definition = node.define;
  const extractRules = (name) => {
    const rules = definition.rules
    if (!rules || !rules.columns || !rules.columns.length)
      return {
        hidden: false,
        header: name,
      }
    let header = rules.columns.find(item => item.name === name)?.alias || name;
    let hidden = rules.columns.find(item => item.name === name)?.hidden || false;
    return {
      hidden,
      header
    }
  }


  return [
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Item ID"}
        </Typography>
      ),
      csvHeader: "Item ID",
      accessor: "id",
      hidden: extractRules("id")["hidden"],
      width: 150,
    },
    {
      Header: `${entity.toLowerCase()}Id`,
      accessor: `${entity.toLowerCase()}.id`,
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: `${entity.toLowerCase()}`,
      accessor: `${entity.toLowerCase()}.program.institution.legalName`,
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: "Remarks",
      accessor: "remarks",
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Item Number"}
        </Typography>
      ),
      csvHeader: "Item Number",
      accessor: "number",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Seed Received Date"}
        </Typography>
      ),
      Cell: ({ row }) => <ReceivedDate value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "Seed Received Date",
      accessor: "receivedDate",
      width: 180,
      hidden: extractRules("receivedDate")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Mark as dangerous"}
        </Typography>
      ),
      Cell: ({ row }) => <DangerousMark value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "Dangerous",
      accessor: "isDangerous",
      width: 180,
      hidden: extractRules("isDangerous")["hidden"],
      Filter: SelectColumnFilter,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {extractRules("testCode")["header"]}
        </Typography>
      ),
      Cell: ({ row }) => mode === "edit" ? <Rsht value={row} setRefreshTable={setRefreshTable} /> : <>{row.values["testCode"]}</>,
      csvHeader: extractRules("testCode")["header"],
      accessor: "testCode",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {`GH Reference Number`}
        </Typography>
      ),
      Cell: ({ row }) => <ReferenceNumber value={row} setRefreshTable={setRefreshTable} />,
      csvHeader: "Reference Number",
      accessor: "referenceNumber",
      width: 200,
      hidden: extractRules("referenceNumber")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Total Quantity to Send"}
        </Typography>
      ),
      Cell: ({ row }) => mode ==="edit" ? <Volume value={row} setRefreshTable={setRefreshTable} /> : <>{row.values["weight"]}</>,
      csvHeader: "Total Quantity to Send",
      accessor: "weight",
      width: 250,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Unit"}
        </Typography>
      ),
      csvHeader: "Package Unit",
      accessor: "packageUnit",
      width: 100,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Package Count"}
        </Typography>
      ),
      Cell: ({ row }) => mode === "edit" ? <PackageQty value={row} setRefreshTable={setRefreshTable} /> : <>{row.values["packageCount"]}</>,
      csvHeader: "Package Count",
      accessor: "packageCount",
      width: 150,
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Status"}
        </Typography>
      ),
      Cell: ({ row }) =>
        //  <RBAC allowedAction={"Change Status"} showComponent={true}>
        <SelectStatus value={row} setRefreshTable={setRefreshTable} />
      // {/* </RBAC> */}
      ,
      csvHeader: "Item Status",
      accessor: "status",
      width: 200,
      hidden: extractRules("status")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Taxonomy Name"}
        </Typography>
      ),
      csvHeader: "Taxonomy Name",
      accessor: "taxonomyName",
      width: 150,
      hidden: extractRules("taxonomyName")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Germplasm Name"}
        </Typography>
      ),
      csvHeader: "Germplasm Name",
      accessor: "germplasmName",
      width: 200,
      hidden: extractRules("germplasmName")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"MTA"}
        </Typography>
      ),
      csvHeader: "MTA Status",
      accessor: "mtaStatus",
      // Cell: ({ row }) => <MTAStatus value={row} setRefreshTable={setRefreshTable} />,
      width: 100,
      hidden: extractRules("mtaStatus")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Seed Name"}
        </Typography>
      ),
      csvHeader: "Seed Name",
      accessor: "seedName",
      width: "Seed Name".length * 20,
      hidden: extractRules("seedName")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Seed Code"}
        </Typography>
      ),
      csvHeader: "Seed Code",
      accessor: "seedCode",
      width: 150,
      hidden: extractRules("seedCode")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Parentage"}
        </Typography>
      ),
      csvHeader: "Parentage",
      accessor: "parentage",
      width: 150,
      hidden: extractRules("parentage")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Germplasm Code"}
        </Typography>
      ),
      csvHeader: "Germplasm Code",
      accessor: "code",
      width: "Germplasm Code".length * 20,
      hidden: extractRules("code")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Package Code"}
        </Typography>
      ),
      csvHeader: "Package Code",
      accessor: "packageName",
      width: 250,
      hidden: extractRules("packageName")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Package Label"}
        </Typography>
      ),
      csvHeader: "Package Label",
      accessor: "packageLabel",
      width: 150,
      hidden: extractRules("packageLabel")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"MLS Ancestors"}
        </Typography>
      ),
      csvHeader: "MLS Ancestors",
      accessor: "mlsAncestors",
      width: 150,
      hidden: extractRules("mlsAncestors")["hidden"],
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Genetic Stock"}
        </Typography>
      ),
      csvHeader: "Genetic Stock",
      accessor: "geneticStock",
      width: 150,
      hidden: extractRules("geneticStock")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Origin"}
        </Typography>
      ),
      csvHeader: "Origin",
      accessor: "origin",
      width: 150,
      hidden: extractRules("origin")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Other Names"}
        </Typography>
      ),
      csvHeader: "Other Names",
      accessor: "otherNames",
      width: 150,
      hidden: extractRules("otherNames")["hidden"]
    },
    {
      Header: (
        <Typography variant="h6" color="primary">
          {"Source Study"}
        </Typography>
      ),
      csvHeader: "Source Study",
      accessor: "sourceStudy",
      width: 150,
      hidden: extractRules("sourceStudy")["hidden"]
    }
  ];
}

