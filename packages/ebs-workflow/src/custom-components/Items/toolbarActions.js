import { Core } from "@ebs/styleguide";
const { Box } = Core;
import ButtonImport from "./item-components/ButtonImport";
import BulkUpdate from "./item-components/BulkUpdate";
import DeleteItems from "./item-components/DeleteItems";
import { RBAC } from "@ebs/layout";
import DownloadItemsCSV from "./item-components/itemscsv/downloaditemscsv";
import { useServiceContext } from "custom-components/context/service-context";


const ToolbarActions = ({ refresh, refreshTable, setRefreshTable, selection, node, columns }) => {
  const {mode} = useServiceContext();
  return (

    <Box
      component="div"
      data-testid={"ToolbarReportsTestId"}
      className="flex flex-row flex-nowrap ml-3"
      display="flex"
      alignItems="center">
      <Box>
        {mode !== "view" && <ButtonImport refresh={refresh} refreshTable={refreshTable} setRefreshTable={setRefreshTable} />}
      </Box>
      <Box>&nbsp;</Box>
      {mode !== "view" && <BulkUpdate refresh={refresh} selection={selection} node={node} columns={columns} />}
      <Box>&nbsp;</Box>
      {mode !== "view" && <RBAC allowedAction={"Delete"}>
        <Box>
          <DeleteItems
            refresh={refresh}
            selection={selection}
          />
        </Box>
      </RBAC>}
      <DownloadItemsCSV selectedRows={selection} columns={columns} />
    </Box>

  );
};
export default ToolbarActions