
import { Core } from "@ebs/styleguide";
const { Box } = Core;
import {RBAC}  from "@ebs/layout";

import DeleteItem from "./item-components/DeleteItem";
import Notes from "./item-components/Notes";

const RowActions = ({rowData, refresh}) => {
    return (
      <Box display="flex" flexDirection="row">
          <RBAC allowedAction={"Delete"}>
            <Box>
              <DeleteItem
                id={rowData.id}
                refresh={refresh}
              />
            </Box>
          </RBAC>
        <RBAC allowedAction={"Create"}>
          <Box>
            <Notes rowData={rowData} refresh={refresh} />
          </Box>
        </RBAC>
      </Box>
    );
  };
export default RowActions;