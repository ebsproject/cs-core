import { Items } from "./items";

export const CustomItems = ({ entity, id, node, mode }) => {
    return (
            <Items entity={entity} id={id} node={node} mode={mode}/>
    )
}
