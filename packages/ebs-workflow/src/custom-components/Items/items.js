import { EbsGrid } from "@ebs/components";
import { getCoreSystemContext, useSelector } from "@ebs/layout";
import { Core } from '@ebs/styleguide';
import React, {useContext,useState, useEffect} from "react";
const { Typography } = Core;
import ToolbarActions from "./toolbarActions";
import RowActions from "./rowActions";
import { columnDefinition } from "./columns";
import { useServiceContext } from "custom-components/context/service-context";

export const Items = ({ entity, id, node }) => {
  const { service, setService, mode, rowData } = useServiceContext();
  const { graphqlUri } = getCoreSystemContext();
  const [refreshTable, setRefreshTable] = useState(false);

  const columns = columnDefinition(setRefreshTable, entity, node);
useEffect(()=>{
  if(!service){
    setService({entity:entity,id:id,bulkOptions:node.define.rules.bulkOptions})
  }
}, [entity, id])

  const toolbar = (selection, refresh) => {
    return (
      <ToolbarActions columns={columns} refresh={refresh} refreshTable={refreshTable} setRefreshTable={setRefreshTable} selection={selection} node = {node} />
    )
  }
  const rowaction = (rowData, refresh) => {
    return (
      <RowActions rowData={rowData} refresh={refresh} />
    )
  }
  return (
    <EbsGrid
      title={
        <>
          <Typography variant="h6" color="primary" className="text-ebs text-bold" >
            {"Items"}
          </Typography>
        </>
      }
      id={`items-id`}
      toolbar={true}
      disabledViewDownloadCSV={true}
      toolbaractions={toolbar}
      columns={columns}
      rowactions={mode=== "view" ? null : rowaction}
      csvfilename={`ItemsList`}
      height="65vh"
      raWidth={120}
      select="multi"
      entity={`${entity}Item`}
      callstandard="graphql"
      uri={graphqlUri}
      disabledViewPrintOunt={true}
      defaultSort={{ col: "number", mod: "ASC" }}
      defaultFilters={[{ col: `${entity.toLowerCase()}.id`, mod: "EQ", val: rowData.id }]}
    />

  )
}