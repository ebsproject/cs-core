import { createContext, useState, useContext } from 'react';

const ServiceContext = createContext();

export function ServiceContextProvider({ children }) {

  const [service, setService] = useState(null);
  const [contactList, setContactList] = useState(null);
  const [nodeForm, setNodeForm] = useState(null);
  const [requestId, setRequestId] = useState(null);
  const [reload, setReload] = useState(false);
  const [loading, setLoading] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const [rowData, setRowData] = useState({});
  const [mode, setMode] = useState("create");
  const [blockButtons, setBlockButtons] = useState(false);
  const [customComponent, setCustomComponent] = useState(null);
  const [definition, setDefinition] = useState(null);
  const [refetch, setRefetch] = useState(0);
  return (
    <ServiceContext.Provider value={
      {
        customComponent,
        setCustomComponent,
        rowData, setRowData,
        loading, setLoading,
        refresh, setRefresh,
        mode, setMode,
        service, setService,
        requestId, setRequestId,
        contactList, setContactList,
        nodeForm, setNodeForm,
        reload, setReload,
        blockButtons, setBlockButtons,
        definition, setDefinition,
        refetch, setRefetch
      }
    }>
      {children}
    </ServiceContext.Provider>
  );
};

export function useServiceContext() {
  return useContext(ServiceContext)
}