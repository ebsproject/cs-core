import { useEffect, useContext, useState, Fragment } from "react";
import { useSelector } from "react-redux";
import { Core, Icons } from "@ebs/styleguide";
const { Box, Button, Typography, Tooltip } = Core;
const { GetApp } = Icons;
import { RBAC } from "@ebs/layout";
import { useUserContext } from "@ebs/layout";
import { downloadCSVData } from "components/runtime/ToolbarAction/functions";

//Component used in the Main Grid 

const DownloadCSV = ({ selectedRows }) => {
    const { userProfile } = useUserContext();
    const { workflowVariables, workflowValues } = useSelector(({ wf_workflow }) => wf_workflow);

    const csvExport = async () => {
        const obj = {
            fields: workflowVariables.allAttributesV2,
            workflowId: workflowValues.id,
            selectedRows,
            userId: userProfile.dbId
        };
        await downloadCSVData(obj)
    }
    return (
        <Fragment>
            <RBAC allowedAction={'Export'}>
                <Box>
                    <Tooltip
                        arrow
                        title={
                            <Typography className='font-ebs text-lg' placement='top'>
                                Download CSV
                            </Typography>
                        }
                    >
                        <Button
                            data-testid={"ButtonDownloadCSVTestId"}
                            variant='contained'
                             onClick={() => csvExport()}
                             aria-label='download file'
                            startIcon={
                                <GetApp className='fill-current text-white ml-3' />
                            }
                        ></Button>
                    </Tooltip>
                </Box>
            </RBAC>
        </Fragment>)
}
export default DownloadCSV;