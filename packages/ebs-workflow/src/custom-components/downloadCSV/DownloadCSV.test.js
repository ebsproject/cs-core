import DownloadCSV from "./DownloadCSV";
import { fireEvent, render, screen } from "@testing-library/react";
import { useUserContext } from "@ebs/layout";
import { useSelector } from "react-redux";
import { downloadCSVData } from "components/runtime/ToolbarAction/functions";

jest.mock("components/runtime/ToolbarAction/functions", () => ({
    downloadCSVData: jest.fn(),
}));
jest.mock("@ebs/layout", () => ({
    useUserContext: jest.fn(),
    RBAC: ({allowedAction, children}) => <div>{children}</div> 
}));

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useSelector: jest.fn(),
}));

describe("Download CSV component", () => {

    const renderComponent = (props) => render(
        <DownloadCSV selectedRows={props.selectedRows} />
    )

    beforeEach(() => {
        useUserContext.mockReturnValue({
            userProfile: { dbId: 1, userName: "j.ramirez@cimmyt.org" },
        });

        useSelector.mockImplementation((selectorFn) => {
            return selectorFn({
                wf_workflow: {
                    workflowValues: { id: 0 },
                    workflowVariables: { id: 0 }
                },
            });
        });

    });


    test("Download CSV loaded", () => {
        const mockSelectedRows = [{ id: 0 }]
        renderComponent({selectedRows : mockSelectedRows })
        expect(screen.getByTestId("ButtonDownloadCSVTestId")).toBeInTheDocument();
    })
    test("Download CSV click button", () => {
        downloadCSVData.mockReturnValue(true)
        const mockSelectedRows = [{ id: 0 }]
        renderComponent({selectedRows : mockSelectedRows })
        const button = screen.getByTestId("ButtonDownloadCSVTestId");
        fireEvent.click(button)
        expect(downloadCSVData).toHaveBeenCalled();
    })
})