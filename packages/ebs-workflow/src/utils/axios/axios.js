import axios from 'axios';
import {getTokenId, getCoreSystemContext} from "@ebs/layout";
const { printoutUri, fileAPIUrl, graphqlUri } = getCoreSystemContext();

export const client = axios.create({
  baseURL: process.env.REACT_APP_CSAPI_URI_REST,
  headers: {
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

client.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});
export const fileClient = axios.create({
  baseURL: fileAPIUrl,
  headers: {
    Accept: "*/*",
  },
});
fileClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});
export const restClient = axios.create({
  baseURL: graphqlUri.replace("/graphql", ""),
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

restClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});

export const po_client = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: printoutUri,
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

po_client.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});

export const axiosClient = axios.create({
  baseURL: graphqlUri.replace("/graphql", ""),
  headers: {
    Accept: "application/json",
    'content-type': 'multipart/form-data',
    "Access-Control-Allow-Origin": "*",
  },
});

axiosClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});

export const cbClient = axios.create({
  // eslint-disable-next-line no-undef
  baseURL: "https://cbapi-dev.ebsproject.org/v3",
  headers: {
    Accept: "application/json",
    "Access-Control-Allow-Origin": "*",
  },
});

cbClient.interceptors.request.use(function (config) {
  config.headers.Authorization = `Bearer ${getTokenId()}`;
  return config;
});
