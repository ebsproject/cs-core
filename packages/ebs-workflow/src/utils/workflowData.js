// Workflow Design
export const workflowDefinition = {
    "id": 1,
    "name": "Incoming Seed",
    "description": "Incoming shipment is a workflow where a program receives seeds from an external organization, such as a partner institute or a private group.",  
    "productTarget": { 
      "domain": "Service Management",
      "product": "Request No-GRS",
      "mainEntity": {      
        "entity": "Service", 
        "api": "http://localhost:18282/graphql" 
      }
    },  
    "security": { 
      "roles": [
        "admin",
        "labmanager"
      ]
    },
    "navigation": { 
      "showMenu": true, 
      "name": "Incoming Seed",
      "icon": "node",
      "translation": [
        {
          "language": "es",
          "value": "Introduccion de Semilla"
        }
      ]
    },
    "design": {
      "phases": [ 
        {
          "id": 1,
          "sequence": 1,
          "name": "Request",
          "description": "Manage the request preparation and approval process",
          "icon": null, 
          "translation": [
            {
              "language": "es",
              "value": "Introduccion de Semilla"
            }
          ],
          "view": "page" ,
          "stages": [
            {
              "id": 1,
              "sequence": 1,
              "view": "form",
              "name": "Create Request",
              "description": "Create Request",
              "icon": null,
              "translation": [
                {
                  "language": "es",
                  "value": "Introduccion de Semilla"
                }
              ],
              "dependOn": "Node",
              "nodes": [
                {
                  "id": 1,
                  "sequence": 1,
                  "define": {
                    "type": "Actor",
                    "props": {
                      "name": "Requestor",
                      "roles": ["admin"]
                    }
                  },
                  "name": "Define if the user has permission",
                  "description": "Define if the user has permission",
                  "icon": null,
                  "translation": [],
                  "requireInput": ["@UserProfile"],
                  "validation": {
                    "type": "javascript",
                    "code": "(@UserProfile) => { return @UserProfile.permissions.memberOf.roles.some(r=> @nodeProps.roles.indexOf(r) >=0 }"
                  },
                  "message": {
                    "error": "The User is not allowed to create a Request"
                  },
                  "security": {
                    "user": [],
                    "actionName": ""
                  },
                  "dependOn": null
                }
              ]
            }
          ]
        },
        {
          "id": 2,
          "sequence": 2,
          "name": "OnProcessing(Outgoing,Incoming)",
          "description": "OnProcessing the request after be approved",
          "icon": null, 
          "translation": [
            {
              "language": "es",
              "value": "Introduccion de Semilla"
            }
          ]
        }
      ]
    }
  }