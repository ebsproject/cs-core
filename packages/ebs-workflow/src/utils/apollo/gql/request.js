import { gql } from "@apollo/client";
export const FIND_REQUEST_LIST = gql`
query FIND_REQUEST_LIST(
  $page: PageInput
  $sort: SortInput
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findRequestList(
    page: $page
    sort: $sort
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    totalPages
    totalElements
    content{
        id
      submitionDate
      requestCode
      status{name}
    }
  }
}
`
export const FIND_SERVICE_LIST = gql`
query FIND_SERVICE_LIST(
  $page: PageInput
  $sort: SortInput
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findServiceList(
    page: $page
    sort: $sort
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    totalPages
    totalElements
    content{
        id
        name
        code
    }
  }
}
`
export const FIND_CONTACT_LIST = gql`
query FIND_CONTACT_LIST(
  $page: PageInput
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findContactList(
    page: $page
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    content{
        id
        email
        addresses{
          streetAddress
          zipCode
        }
        category{
          name
        }
        institution{
          id
          legalName
          commonName
        }
      person{
        familyName
        givenName
      }
    }
  }
}
`
export const FIND_PROGRAM_LIST = gql`
query FIND_PROGRAM_LIST(
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findProgramList(
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    content{
        id
  name
  code
    }
  }
}
`
export const FIND_SERVICE_BY_RECORD_ID = gql`
query FIND_SERVICE_LIST_BY_RECORD_ID(
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findServiceList(
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    content{
      recipient{
        id
        email
      }
      sender{
        email
        id
      }
      requestor{
        id
        email
      }
      serviceProvider{
        id
        email
      }
    }
  }
}
`;
export const FIND_SERVICE_DATA = gql`
query FIND_SERVICE_DATA(
  $filters: [FilterInput]
  $sort: [SortInput]
  $page: PageInput
  $disjunctionFilters: Boolean = false
){
  findWorkflowCustomFieldList(
    filters: $filters
    page: $page
    sort: $sort
    disjunctionFilters: $disjunctionFilters
  ){
    content
  }
}
`;
export const FIND_UNIT_MEMBERS = gql`
query FIND_UNIT_MEMBERS(
  $filters: [FilterInput]
  $sort: [SortInput]
  $page: PageInput
  $disjunctionFilters: Boolean = false
){
  findContactList(
    filters: $filters
    page: $page
    sort: $sort
    disjunctionFilters: $disjunctionFilters
  ){
    content{
      id
      members{
        id
          roles{
        name
        productfunctions{
          action
        }
      }
          contact{
            id
            email
          }
        
      }
    }
  }
}
`;