import { gql } from "@apollo/client";

export const FIND_WORKFLOW_INSTANCE_LIST = gql`
query FIND_WORKFLOW_INSTANCE_LIST(
  $page: PageInput
  $sort: [SortInput]
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findWorkflowInstanceList(
    page: $page
    sort: $sort
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    totalPages
    totalElements
    content{
      id
      initiated
      complete
      workflow{
        id
        name
        description
        help
        sortNo
        definition
        entityReference{
          id
          entitySchema
        }
      }
    }
  }
}
`