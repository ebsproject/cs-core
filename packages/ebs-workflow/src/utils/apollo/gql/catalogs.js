import { gql } from "@apollo/client";

export const FIND_PRODUCT_LIST = gql`
  query findProductList {
    findProductList(
      page: { size: 300, number: 1 }
      sort: { col: "domain.name", mod: ASC }
    ) {
      content {
        id
        name
        description
        hasWorkflow
        domain {
          id
          name
          domaininstances {
            sgContext
          }
        }
      }
    }
  }
`;

export const FIND_PROGRAM_LIST = gql`
  query findProgramList {
    findProgramList(page: { size: 300, number: 1 }) {
      content {
        id
        code
        name
      }
    }
  }
`;

export const FIND_USER_LIST = gql`
  query findUserList {
    findUserList(page: { size: 300, number: 1 }) {
      content {
        id
        userName
        contact {
          id
          person {
            givenName
            familyName
          }
        }
      }
    }
  }
`;

export const FIND_ROLE_LIST = gql`
  query findRoleList {
    findRoleList(page: { size: 300, number: 1 }) {
      content {
        id
        name
        description
        isSystem
        rules {
          id
          name
        }
      }
    }
  }
`;

export const FIND_VIEWTYPE_LIST = gql`
  query viewTypeList {
    findWorkflowViewTypeList {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_PROCESS_LIST = gql`
  query processList {
    findProcessList {
      content {
        id
        description
        code
        name
        isBackground
        dbFunction
        callReport
      }
    }
  }
`;

export const FIND_FORMTYPE_LIST = gql`
  query FIND_FORMTYPE_LIST {
    findCFTypeList {
      content {
        id
        name
        description
        type
      }
    }
  }
`;

export const FIND_NODETYPE_LIST = gql`
  query FIND_NODETYPE_LIST {
    findNodeTypeList {
      content {
        id
        name
        description
      }
    }
  }
`;

export const FIND_TEMPLATE_LIST = gql`
  query FIND_TEMPLATE_LIST {
    findPrintoutTemplateList {
      content {
        id
        name
        description
      }
    }
  }
`;

export const FIND_EMAIL_LIST = gql`
  query FIND_EMAIL_LIST {
    findEmailTemplateList {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_EMAIl_TEMPLATE_LIST = gql`
  query FIND_EMAIL_LIST(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findEmailTemplateList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        template
        subject
      }
    }
  }
`;

export const FIND_STATUS_LIST = gql`
  query FIND_STATUS_LIST($filters: [FilterInput]) {
    findStatusTypeList(filters: $filters) {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_PRODUCT_BY_ID = gql`
  query FIND_PRODUCT_BY_ID($id: ID!) {
    findProduct(id: $id) {
      id
      name
      entityReference {
        id
        attributess {
          id
          name
          description
          defaultValue
        }
      }
    }
  }
`;
export const FIND_NODE_LIST = gql`
  query findNodeList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findNodeList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        nodeCFs {
          name
        }
        description
        process {
          code
        }
      }
    }
  }
`;
export const FIND_NODE_LIST_DEFINE = gql`
  query findNodeList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findNodeList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        name
        nodeCFs {
          name
        }
        define
        description
        process {
          code
        }
      }
    }
  }
`;
export const FIND_CONTACT_LIST = gql`
  query findContactList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findContactList(page: $page, sort: $sort, filters: $filters) {
      totalPages
      content {
        id
        email
        person {
          fullName
        }
      }
    }
  }
`;
export const FIND_DOMAIN_LIST = gql`
  query findDomainList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findDomainList(page: $page, sort: $sort, filters: $filters) {
      content {
        id
        prefix
        name
        domaininstances {
          sgContext
        }
      }
    }
  }
`;

export const FIND_WORKFLOW_BY_ID = gql`
  query FIND_WORKFLOW_BY_ID($id: ID!) {
    findWorkflow(id: $id) {
      id
      title
      name
      description
      help
      sortNo
      icon
      tenants {
        id
      }
      htmlTag {
        id
      }
      api
      navigationIcon
      navigationName
      product {
        id
        name
      }
      sequence
      showMenu
      isSystem
      securityDefinition
      phases {
        id
        name
        description
        help
        sequence
        tenant {
          id
        }
        htmlTag {
          id
        }
        workflow {
          id
        }
        dependOn
        icon
        designRef
        workflowViewType {
          id
        }
        stages {
          id
          name
          description
          help
          sequence
          parent {
            id
            name
          }
          tenant {
            id
          }
          htmlTag {
            id
          }
          dependOn
          icon
          designRef
          nodes {
            id
            name
            description
            help
            sequence
            requireApproval
            htmlTag {
              id
            }
            product {
              id
            }
            workflow {
              id
            }
            process {
              id
            }
            define
            dependOn
            icon
            message
            requireInput
            securityDefinition
            validationCode
            validationType
            designRef
            workflowViewType {
              id
            }
            nodeCFs {
              id
              name
              description
              help
              required
              fieldAttributes
              apiAttributesName
              attributes {
                name
                description
                help
                sortNo
                attComponent
                isMultiline
                isRequired
                defaultValue
                sm
                md
                lg
                id
                entityreference {
                  id
                }
              }
              cfType {
                id
                name
                description
                help
                type
              }
              htmlTag {
                id
              }
              node {
                id
              }
              cfValue {
                id
                flagValue
                textValue
                numValue
                dateValue
                codeValue
                nodeCF {
                  id
                }
                event {
                  id
                }
                tenant {
                  id
                }
              }
              tenant {
                id
              }
            }
            tenant {
              id
            }
            nodeType {
              id
            }
          }
          workflowViewType {
            id
          }
        }
      }
      statusTypes {
        id
        name
        description
        help
        workflow {
          id
        }
        tenant {
          id
        }
      }
      workflowContacts {
        id
        workflow {
          id
        }
        contact {
          id
        }
        provide
      }
    }
  }
`;
