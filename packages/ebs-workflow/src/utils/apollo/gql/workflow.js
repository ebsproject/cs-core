import { gql } from "@apollo/client";

export const FIND_WORKFLOW_BY_ID = gql`
query findWorkflow($id: ID!) {
  findWorkflow(id: $id) {
    id
    title
    name
    description
    navigationName    
    navigationIcon    
    sortNo
    help
    showMenu
    securityDefinition
    phases{
      designRef
      sequence
      id
      description
      name
      icon
      dependOn
      workflow {
        id
      }
      workflowViewType{
          id
          name
        }
      stages{
        designRef
        phase{
          designRef
          id
        }
        id
        icon
        description
        name
        dependOn
        sequence
        
        workflowViewType{
          id
          name
        }        
        nodes{
          designRef
          id
          dependOn
          define
          name
          description
          help
          icon
          sequence
          securityDefinition
          workflowViewType{
            id
            name
          }          
          nodeType{
            id 
            name 
            description
          }
          process{
              id
              isBackground
              name
            }
       
          nodeCFs{
            id
            fieldAttributes
            apiAttributesName          
            description
            name
            required
          attributes{
              id
              description
              isRequired
              name
              sortNo  
            }
            cfType{
              id
              description
              name
              type
            }
          }
        }
      }
    }
    sequence
    isSystem
    api
  product{
    hasWorkflow
    id
    name
    entityReference{
      entity
    }
    domain{
      domaininstances{
        sgContext
      }
    }
    htmltag{
      id
      tagName
      
    }
    
  }
   
  }
}
`;

export const FIND_PHASE_LIST = gql`
query FIND_PHASE_LIST($filters: [FilterInput]){
  findPhaseList(filters: $filters){
    content{
      id
      workflow{
        id
      }
      stages{
        nodes{
          id
          designRef
          dependOn
          sequence
        }
      }
      }
  }
}
`
export const BUILD_WORKFLOW_CF_LIST = gql`
query findWorkflowCustomFieldList(
  $page: PageInput
  $sort: [SortInput]
  $filters: [FilterInput]
){
  findWorkflowCustomFieldList(page: $page, sort: $sort, filters: $filters) 
}
`
export const FIND_WORKFLOW_LIST = gql`
  query findWorkflowList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findWorkflowList(page: $page, sort: $sort, filters: $filters) {
      content {
    workflowContacts{
      contact{
          id
            institution{
              unitType{
                  name
                 }
              legalName
              commonName
              }
         }
        }
    id
    title
    name
    description
    navigationName    
    navigationIcon    
    sortNo
    help
    showMenu
    securityDefinition
    phases{
      designRef
      sequence
      id
      icon
      description
      name
      dependOn
      workflow {
        id
      }
      workflowViewType{
          id
          name
        }
      stages{
        designRef
        phase{
          id
        }
        id
        description
        name
        dependOn
        sequence
        icon
        workflowViewType{
          id
          name
        }        
        nodes{
          designRef
          id
          dependOn
          define
          name
          description
          help
          sequence
          icon
          securityDefinition
          workflowViewType{
            id
            name
          }          
          nodeType{
            id 
            name 
            description
          }
          process{
              id
              isBackground
              name
              code
              path
            }
       
          nodeCFs{
            node{
              id
            }
            id
            fieldAttributes
            apiAttributesName          
            description
            name
            required
          attributes{
              id
              description
              isRequired
              name
              sortNo  
            }
            cfType{
              id
              description
              name
              type
            }
          }
        }
      }
    }
    sequence
    isSystem
    api
  product{
    hasWorkflow
    id
    name
    entityReference{
      entity
    }
    domain{
      domaininstances{
        sgContext
      }
    }
    htmltag{
      id
      tagName
      
    }
    
  }
    
      }
    }
  }
`;

export const FIND_FILE_TYPE_LIST = gql`
  query findFileTypeList(
    $page: PageInput
    $sort: [SortInput]
    $filters: [FilterInput]
  ) {
    findFileTypeList(page: $page, sort: $sort, filters: $filters) {
      content {
          id
          description
      }
    }
  }
`;

export const CREATE_WORKFLOW = gql`
mutation CREATE_WORKFLOW($workflow: WorkflowInput!){
  createWorkflow(workflow: $workflow){
    id    
  }
}
`

export const MODIFY_WORKFLOW = gql`
mutation MODIFY_WORKFLOW($workflow: WorkflowInput!){
  modifyWorkflow(workflow: $workflow){
    id
  }
}
`
export const MODIFY_SERVICE = gql`
mutation MODIFY_SERVICE($service: ServiceInput!){
  modifyService(service: $service){
    id
  }
}
`
export const FIND_SERVICE = gql`
query FIND_SERVICE($id: ID!){
  findService(id: $id){
    id
    byOccurrence
    workflowInstance{
      workflow{
        id
      }
    }
program{
  id
}
  }
}
`
export const FIND_STAGE = gql`
query FIND_STAGE($id: ID!){
  findStage(id: $id){
    id
    designRef
    dependOn
    workflowViewType{
      id
    }
    nodes{
      id
      designRef
    }
  }
}
`
export const FIND_PHASE = gql`
query FIND_PHASE($id: ID!){
  findPhase(id: $id){
    id
    byOccurrence
    workflowInstance{
      workflow{
        id
      }
    }
program{
  id
}
  }
}
`

export const CREATE_PHASE = gql`
mutation CREATE_PHASE($phase: PhaseInput!){
  createPhase(phase: $phase){
    id
  }
}
`

export const MODIFY_PHASE = gql`
mutation MODIFY_PHASE($phase: PhaseInput!){
  modifyPhase(phase: $phase){
    id
  }
}
`

export const FIND_PHASE_BY_ID = gql`
query FIND_PHASE_BY_ID($filters: [FilterInput]){
  findPhaseList(filters: $filters){
    content {
     designRef
      name
      id
      description
      help
      sequence
      tenant {
        id
      }
      workflow {
        id
      }
    }
    
  }
}
`
export const CREATE_STAGE = gql`
mutation CREATE_STAGE($stage: StageInput!){
  createStage(stage: $stage){
    id
  }
}
`

export const MODIFY_STAGE = gql`
mutation MODIFY_STAGE($stage: StageInput!){
  modifyStage(stage: $stage){
    id
  }
}
`

export const FIND_STAGE_BY_ID = gql`
query FIND_STAGE_BY_ID($filters: [FilterInput]){
  findStageList(filters: $filters){
    content {
      name
      id
      description
      help
      sequence
      tenant {
        id
      }
      phase {
        id
      }
    }
    
  }
}
`
export const CREATE_NODE = gql`
mutation CREATE_NODE($node: NodeInput!){
  createNode(node: $node){
    id
  }
}
`

export const MODIFY_NODE = gql`
mutation MODIFY_NODE($node: NodeInput!){
  modifyNode(node: $node){
    id
  }
}
`

export const FIND_NODE_BY_ID = gql`
query FIND_NODE_BY_ID($filters: [FilterInput]){
  findNodeList(filters: $filters){
    content {
      name
      id
      description
      help
      sequence
      dependOn     
    }
    
  }
}
`
export const CREATE_NODE_CF = gql`
mutation CREATE_NODE_CF($nodeCF: NodeCFInput!){
  createNodeCF(nodeCF: $nodeCF){
    id
  }
}
`

export const MODIFY_NODE_CF = gql`
mutation MODIFY_NODE_CF($nodeCF: NodeCFInput!){
  modifyNodeCF(nodeCF: $nodeCF){
    id
  }
}
`

export const FIND_NODE_CF_BY_ID = gql`
query FIND_NODE_CF_BY_ID(
  $filters: [FilterInput]
  $page: PageInput
  ){
  findNodeCFList(filters: $filters, page: $page){
    content {     
      id
      description
      help
      name
    }
    
  }
}
`


export const buildCreateMutation = (entity) => {
  return gql`
    mutation create${entity}($${entity.toLowerCase()}: ${entity}Input!){
        create${entity}(${entity.toLowerCase()}:$${entity.toLowerCase()}){
          id
          }
    }`;
};
export const buildDeleteMutation = (entity) => {
  return gql`
    mutation delete${entity}($id: Int!){
        delete${entity}(id:$id)
    }`;
};

export const buildModifyMutation = (entity) => {
  return gql`
    mutation modify${entity}($${entity.toLowerCase()}: ${entity}Input!){
        modify${entity}(${entity.toLowerCase()}:$${entity.toLowerCase()}){
          id
          }
    }`;
};

export const buildQuery = (entity, apiContent) => {
  return gql`
    query find${entity}List($page: PageInput, $sort: [SortInput], $filters: [FilterInput], $disjunctionFilters: Boolean){
        find${entity}List (page:$page, sort:$sort, filters:$filters, disjunctionFilters: $disjunctionFilters){
            totalPages
            totalElements
            content {
                ${apiContent.map((item) => {
    const levels = item.accessor.split('.');
    // Setting levels
    return extractLevels(levels);
  })}
            }
        }
    }`;
};

export const CREATE_WORKFLOW_INSTANCE = gql`
  mutation CREATE_WORKFLOW_INSTANCE($workflowInstance: WorkflowInstanceInput!) {
    createWorkflowInstance(workflowInstance: $workflowInstance) {
      id
    }
  }
`;

export const CREATE_EVENT = gql`
  mutation CREATE_EVENT($event: EventInput!) {
    createEvent(event: $event) {
      id
    }
  }
`;

export const CREATE_CF_VALUES = gql`
  mutation CREATE_CF_VALUES($cfValue: CFValueInput!) {
    createCFValue(cfValue: $cfValue) {
      id
    }
  }
`;
export const CREATE_CF_VALUES_BULK = gql`
  mutation CREATE_CF_VALUES_BULK($cfValues: [CFValueInput!]!) {
    createCFValues(cfValues: $cfValues)
  }
`;
export const MODIFY_CF_VALUES = gql`
  mutation MODIFY_CF_VALUES($cfValue: CFValueInput!) {
    modifyCFValue(cfValue: $cfValue) {
      id
    }
  }
`;
export const MODIFY_CF_VALUES_BULK = gql`
  mutation MODIFY_CF_VALUES_BULK($cfValues: [CFValueInput!]!) {
    modifyCFValues(cfValues: $cfValues)
  }
`;
function extractLevels(arr) {
  let levels = arr;
  if (levels.length > 1) {
    return `${levels.shift()}{
        ${extractLevels(levels)}
    }`;
  } else {
    return `${levels[0]}`;
  }
};

export const FIND_EVENT_BY_WFI_ID = gql`
query FIND_EVENT_BY_WFI_ID(
  $page: PageInput
  $sort: [SortInput]
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findEventList(
    page: $page
    sort: $sort
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    totalPages
    totalElements
    content{
        id
        description
        
    }
  }
}
`;
export const FIND_CF_VALUES_BY_EVENT_ID = gql`
query FIND_CF_VALUES_BY_EVENT_ID(
  $page: PageInput
  $sort: [SortInput]
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findCFValueList(
    page: $page
    sort: $sort
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    totalPages
    totalElements
    content{
      event{
        id
      }
      id
      textValue
      dateValue
      numValue
      flagValue
      codeValue
      nodeCF{
            node{
              id
            }
            id
            fieldAttributes
            apiAttributesName          
            description
            name
            required
          attributes{
              id
              description
              isRequired
              name
              sortNo  
            }
            cfType{
              id
              description
              name
              type
            }
          }
    }
  }
}
`;
export const FIND_WORKFLOW_INSTANCE_BY_ID = gql`
query findWorkflowInstance($id: ID!) {
  findWorkflowInstance(id: $id) {
    id
    initiated
    complete
    status{
      statusType{
        name
      }
    }
  }
}
`;
export const FIND_EVENT_LIST = gql`
query FIND_EVENT_LIST(
  $page: PageInput
  $sort: [SortInput]
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findEventList(
    page: $page
    sort: $sort
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    totalPages
    totalElements
    content{
      completed
      id
      workflowInstance{
        id
        status{
          id
          statusType{
            id
            name
          }
        }
      }
      node{
        define
        id
        name
        process{
          code
        }
      }
      stage{
        sequence
        nodes{
          name
          dependOn
          help
          define
          requireInput
          designRef
          securityDefinition
          process{
            name
            code
            id
            description
            path
          }
          sequence
          id
          icon
          requireApproval
          nodeType{
            name
            description
            
          }
        }
        
        id
        name
        phase{
          id
          sequence
          name
          stages{
            nodes{
              designRef
              icon
          requireApproval
          nodeType{
            name
            description
          }
              help
              securityDefinition
              define
              id
              process{
                code
                name
                path
              }
            }
          } 
        }
      }
    }
  }
}
`;

export const MODIFY_STATUS = gql`
mutation MODIFY_STATUS($status: StatusInput!){
  modifyStatus(status: $status){
    id
  }
}
`;
export const CREATE_STATUS = gql`
  mutation CREATE_STATUS($statusInput: StatusInput!) {
    createStatus(status: $statusInput) {
      id
    }
  }
`;
export const MODIFY_STATUS_TYPE = gql`
mutation MODIFY_STATUS_TYPE($statusTypeInput: StatusTypeInput!){
  modifyStatusType(statusType: $statusTypeInput){
    id
  }
}
`;
export const CREATE_STATUS_TYPE = gql`
  mutation CREATE_STATUS_TYPE($statusTypeInput: StatusTypeInput!) {
    createStatusType(statusType: $statusTypeInput) {
      id
    }
  }
`;
export const CREATE_FILE_TYPE = gql`
  mutation CREATE_FILE_TYPE($fileTypeInput: FileTypeInput!) {
    createFileType(fileType: $fileTypeInput) {
      id
    }
  }
`;
export const MODIFY_FILE_TYPE = gql`
mutation MODIFY_FILE_TYPE($fileTypeInput: FileTypeInput!){
  modifyFileType(fileType: $fileTypeInput){
    id
  }
}
`;
export const DELETE_STATUS_TYPE = gql`
  mutation DELETE_STATUS_TYPE($id: Int!) {
    deleteStatusType(id: $id) 
  }
`;
export const DELETE_FILE_TYPE = gql`
  mutation DELETE_FILE_TYPE($id: Int!) {
    deleteFileType(id: $id) 
  }
`;

export const FIND_NODE = gql`
query FIND_NODE($id: ID!){
  findNode(id: $id){
      name
      id
      description
      designRef
      help
      sequence
      define
      process{
        id
        code
        path
      }     
        
  }
}
`
export const FIND_NODE_ID = gql`
query FIND_NODE_ID($id: ID!){
  findNode(id: $id){
      id
      sequence
      define
      dependOn
      message
      requireInput
      securityDefinition
      designRef
      workflowViewType{
            id
          }          
          nodeType{
            id 
          }
        
  }
}
`
export const FIND_CF_VALUE = gql`
query FIND_CF_VALUE(
  $filters: [FilterInput]
  $disjunctionFilters: Boolean = false
){
  findCFValueList(
    filters: $filters
    disjunctionFilters: $disjunctionFilters
  ){
    content{
      textValue
      numValue
    nodeCF{
      id
    }     
    }
  }
}
`;
export const FIND_NODE_LIST_BY_ID = gql`
query FIND_NODE_LIST_BY_ID($filters: [FilterInput]){
  findNodeList(filters: $filters){
    content{
          id
          dependOn
          designRef
          define
          name
          description
          help
          sequence
          icon
          securityDefinition
          workflowViewType{
            id
            name
          }          
          nodeType{
            id 
            name 
            description
          }
          process{
              id
              isBackground
              name
              code
              path
            }
       
          nodeCFs{
            id
            fieldAttributes
            apiAttributesName          
            description
            name
            required
          attributes{
              id
              description
              isRequired
              name
              sortNo  
            }
            cfType{
              id
              description
              name
              type
            }
          }
        }
    
  }
}
`
export const FIND_JOB_LOG_LIST= gql`
query FIND_JOB_LOG_LIST($filters: [FilterInput]){
  findJobLogList(filters: $filters){
    content{
      contacts{
      id
          }
      recordId
      id
      message
      jobWorkflow{
        id
      }
    }
  }
}
`

export const MODIFY_JOB_LOG = gql`
mutation MODIFY_JOB_LOG($jobLog: JobLogInput!){
  modifyJobLog(jobLog: $jobLog){
    id
  }
}
`;

export const NEW_MESSAGE_SUBSCRIPTION = gql`
subscription{
  jobLog{
              id
              contacts{
                id
                person{
                  givenName
                  familyName
                }
              }
              message
              startTime
              endTime
              status
              jobWorkflow{
                id
                jobType{
                  id
                  name
                }
            }                 
  }
}
`;