import { client } from "utils/apollo";
import { FIND_SERVICE_DATA } from "utils/apollo/gql/request";

function convertToCSV(objArray) {
  const array = typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
  let str = '';
  return array.reduce((str, next) => {
    str +=
      `${Object.values(next)
        .map((value) => `"${value}"`)
        .join(',')}` + '\r\n';
    return str;
  }, str);
}

export function exportCSVFile(headers, data, fileTitle) {
  if (headers) {
    data.unshift(headers);
  }
  var jsonObject = JSON.stringify(data);
  var csv = convertToCSV(jsonObject);
  var exportedFilename = fileTitle + '.csv' || 'Items.csv';
  var blob = new Blob([csv], { type: 'text/csvcharset=utf-8' });
  if (navigator.msSaveBlob) {
    // IE 10+
    navigator.msSaveBlob(blob, exportedFilename);
  } else {
    var link = document.createElement('a');
    if (link.download !== undefined) {
      var url = URL.createObjectURL(blob);
      link.setAttribute('href', url);
      link.setAttribute('download', exportedFilename);
      link.style.visibility = 'hidden';
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }
}

export async function downloadCSVData(props) {
  const { fields, workflowId, selectedRows, userId } = props;
  let data = [];
  if (selectedRows.length > 0)
    data = selectedRows;
  else
    data = await getData(workflowId, userId);

  let headers = {};
  let map = {};
  let columns = fields.sort((a, b) => { a })
  for (let i = 0; i < columns.length; i++) {
    const column = columns[i]
    if (column.fieldAttributes.showInGrid) {
      map[column.apiAttributesName] = column.apiAttributesName;
      headers[column.apiAttributesName] = column.description;
    }
  };
  headers = { ...{ status: "Status", totalItems: "Total Items", totalPackages: "Total Packages", totalWeight: "Total Weight" }, ...headers }
  map = { ...{ status: "status", totalItems: "totalItems", totalPackages: "totalPackages", totalWeight: "totalWeight" }, ...map }

  let formattedData = [];
  for (let i = 0; i < data.length; i++) {
    const item = data[i]
    let mapKeys = Object.keys(map)
    let objTransformed = {}
    for (let i = 0; i < mapKeys.length; i++) {
      const _k = mapKeys[i]
      if (!item[_k]) {
        objTransformed[_k] = " "
      } else {
        let _item = item[_k]
        if (Object.prototype.toString.call(_item) === '[object Object]') {
          let obj = _item['label']
          if (obj) {
            objTransformed[_k] = obj
          } else {
            objTransformed[_k] = _item
          }
        } else {
          objTransformed[_k] = _item
        }
      }
    }
    formattedData = [...formattedData, objTransformed];
  };

  for (let i = 0; i < formattedData.length; i++) {
    let item = formattedData[i];
    let obj = item;
    let objKeys = Object.keys(obj);
    for (let i = 0; i < objKeys.length; i++) {
      let key = objKeys[i];
      if (Object.prototype.toString.call(item[key]) === '[object Object]') {
        let _obj = item[key];
        let _objKeys = Object.keys(_obj);
        let stringValue = [];
        _objKeys.forEach(_key => {
          if (typeof _obj[_key] === "string")
            stringValue = [...stringValue, _obj[_key]]
        })
        item[key] = stringValue.join(",")
      }
      if (item[key] === null) {
        item[key] = " "
      }
      if (Array.isArray(item[key])) {
        item[key] = item[key].length
      }
    }
  }
  exportCSVFile(headers, formattedData, `Shipment`);

}

async function getData(wfId, userId) {

  let totalData = [];
  let number = 1;
  const { data } = await client.query({
    query: FIND_SERVICE_DATA,
    variables: {
      page: { number: number, size: 300 },
      sort:[{col:"id", mod:"ASC"}],
      filters: [
        { col: "workflowId", mod: "EQ", val: wfId },
        { col: "userId", mod: "EQ", val: userId },
      ],
    },
    fetchPolicy: "no-cache",
  });
  number = data.findWorkflowCustomFieldList.totalPages;

  totalData = [...totalData, ...data.findWorkflowCustomFieldList.content];
  if (number > 1) {
    for (let i = 2; i <= number; i++) {
      const { data } = await client.query({
        query: FIND_SERVICE_DATA,
        variables: {
          page: { number: i, size: 300 },
          sort:[{col:"id", mod:"ASC"}],
          filters: [
            { col: "workflowId", mod: "EQ", val: wfId },
            { col: "userId", mod: "EQ", val: userId },
          ],
        },
        fetchPolicy: "no-cache",
      });
      totalData = [...totalData, ...data.findWorkflowCustomFieldList.content];
    }
  }
  return totalData;
}
