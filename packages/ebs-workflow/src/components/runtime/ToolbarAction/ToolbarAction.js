import { useState, useEffect, useContext } from "react";
import { Core } from "@ebs/styleguide";
const { Box, Button, Tooltip, Typography, Icon } = Core;
import { useSelector } from "react-redux";
import { FormattedMessage } from "react-intl";
import { useNavigate } from "react-router-dom";
import { FIND_EVENT_LIST } from "utils/apollo/gql/workflow";
import { client } from "utils/apollo";
import { checkPermissions } from "validations";
import { useExecuteProcess } from "hooks/useExecuteProcess";
import DownloadCSV from "custom-components/downloadCSV/DownloadCSV";
import RefreshComponent from "./refreshComponent";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { useServiceContext } from "custom-components/context/service-context";
import {RBAC} from "@ebs/layout";

const ToolbarAction = (selectedRows, refresh) => {
  const { rowData, setRowData, setMode } = useServiceContext();
  const navigate = useNavigate()
  const [globalAction, setGlobalAction] = useState([]);
  const [toolbarActionNode, setToolbarActionNode] = useState([]);
  const { userPermissions } = useSelector(({ wf_user }) => wf_user);
  const { workflowValues } = useSelector(({ wf_workflow }) => wf_workflow);
  const { executeProcess } = useExecuteProcess();
  useEffect(() => {
    let globalActions = [];
    workflowValues.phases.forEach((ph) => {
      ph.stages.forEach((st) => {
        st.nodes.forEach((nd) => {
          if (nd.nodeType.name === 'toolbarAction' && nd.define.globalAction)
            globalActions = [...globalActions, nd]
        });
      });
    });
    setGlobalAction((prevItems) => [...prevItems, ...globalActions]);
    setToolbarActionNode(globalActions);
  }, []);

  useEffect(() => {
    if (selectedRows.length === 1) {
      const fetchData = async () => {
        const { data } = await client.query({
          query: FIND_EVENT_LIST,
          variables: {
            filters: [
              { col: "workflowInstance.id", mod: "EQ", val: Number(selectedRows[0].wfInstance) },
              { col: "recordId", mod: "EQ", val: Number(selectedRows[0].id) },
            ],
          },
        });
        const wf = data.findEventList.content.filter((item) => item.completed === null);
        const _toolbarActionNode =
          wf.length > 0
            ? wf[0].stage.nodes.filter((item) => item.nodeType.name === "toolbarAction" && !item.define.globalAction)
            : [];
        setToolbarActionNode((prevItems) => [...prevItems, ..._toolbarActionNode]);
      };
      fetchData();
    }else{
      setToolbarActionNode(globalAction);
    }
   
  },[selectedRows.length, globalAction]);

  const processToExecute = async (node) => {
    setRowData(selectedRows[0]);
    let createdObj = {
      id: selectedRows[0]?.id || 0,
      workflowInstanceId: selectedRows[0]?.wfInstance || 0,
    };
    await executeProcess({ node, createdObj });
  };

  const startNewRequest = () => {
    if (rowData) setRowData(null);
    setMode("create");
    navigate("/wf/request/service", {
      state: { mode: "create" },
    });
  };
  return (
    <Box
      component="div"
      data-testid={"ToolbarReportsTestId"}
      className="flex flex-row flex-nowrap ml-3"
      display="flex"
      alignItems="center"
    >
      <RefreshComponent refresh={refresh} />
      <RBAC allowedAction="Create" >
      <Tooltip
        arrow
        placement="top"
        title={
          <Typography className="font-ebs text-lg">
            <FormattedMessage id={"none"} defaultMessage={"Create a request"} />
          </Typography>
        }
      >
        <Button
          className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          onClick={startNewRequest}
        >
          <Typography className="font-ebs">
            <FormattedMessage id="none" defaultMessage={"New"} />
          </Typography>
        </Button>
      </Tooltip>
      </RBAC>
      {toolbarActionNode.length > 0 &&
        toolbarActionNode.map((item) => {
          return (
            checkPermissions(userPermissions, item.securityDefinition) && (
              <div key={item.id}>
                <Button
                  className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
                  onClick={() => processToExecute(item)}
                  startIcon={
                    <Icon style={{ cursor: "pointer" }} color={"white"}>
                      {item.icon ? item.icon.toLowerCase() : "warning"}
                    </Icon>
                  }
                >
                  <Typography className="font-ebs">
                    <FormattedMessage id="none" defaultMessage={item.help} />
                  </Typography>
                </Button>
              </div>
            )
          );
        })}
        <RBAC allowedAction={"Export"}>
        <DownloadCSV selectedRows={selectedRows} />
        </RBAC>
    </Box>
  );
};
export default ToolbarAction;
