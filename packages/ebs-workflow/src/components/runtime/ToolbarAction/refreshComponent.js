import { useEffect } from "react";
import { useContext } from "react";
import { WorkflowContext } from "components/designer/context/workflow-context";

const RefreshComponent = ({ refresh }) => {
    const { reload, setReload } = useContext(WorkflowContext);

    useEffect(() => {
        if (reload) {
            refresh();
        }
        return () => setReload(false)
    }, [reload]);

    return null;
}
export default RefreshComponent;