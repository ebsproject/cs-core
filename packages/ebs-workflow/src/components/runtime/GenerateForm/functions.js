import { createCustomFieldValues, createEvent } from "components/service/functions";
import { showMessage } from "store/modules/message";
import { createComponentFromAPI } from "components/service/functions";
import { createComponent } from "components/service/functions";

export async function createCFMutation(data) {
  let cfFields = [];
  let objKeys = Object.keys(data);

  for (let i = 0; i < data.customFields.length; i++) {
    let cf = Object.assign({}, data.customFields[i]);
    if (objKeys.includes(cf.name)) {
      cf.value = data[`${cf.name}`];
      delete data[`${cf.name}`];
    }
    cfFields = [...cfFields, cf];
  }
  let nd_st = {
    nd: Number(data.nodeId),
    st: Number(data.stageId),
  };
  let eventId = await createEvent(Number(data.id), Number(data.workflowInstanceId), null, nd_st);

  await createCustomFieldValues(cfFields, eventId);
}

export const fetchDefinition = async (node,dispatch) => {
  let selectEntComp = [];
  let _node =  Object.assign({},node);
  let ebsComponents = [];
  if (_node.nodeCFs.length === 0) {
    let message = `Please check the ${node.name} form definition in Workflow Designer, there is no custom fields to load`;
    dispatch(
      showMessage({
        variant: "error",
        message: message,
        anchorOrigin: { vertical: "top", horizontal: "right" },
      })
    );
    return;
  }
  let componentsExtracted  =  _node.nodeCFs.map(i => i)
  let components = componentsExtracted.sort((a, b) => {
    return Number(a.fieldAttributes.sort) - Number(b.fieldAttributes.sort);
  });
  for (let i = 0; i < components.length; i++) {
    if (components[i].cfType.description.toLowerCase() === "select") {
      if (components[i].attributes) selectEntComp.push(components[i]);
      let apiComp = await createComponentFromAPI(components[i]);
      if (apiComp.fail) {
        let message = `Please check the ${
          components[i].name
        } field definition in Workflow Designer, ${apiComp.error.toString()}`;
        dispatch(
          showMessage({
            variant: "error",
            message: message,
            anchorOrigin: { vertical: "top", horizontal: "right" },
          })
        );
        return;
      }
      ebsComponents.push(apiComp);
    } else {
      let comp = createComponent(components[i]);
      ebsComponents.push(comp);
    }
  }
  _node.formDefinition = { components: ebsComponents };

  return _node
};