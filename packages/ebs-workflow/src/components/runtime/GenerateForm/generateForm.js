import { Core } from "@ebs/styleguide";
import PropTypes from "prop-types";
import { EbsForm } from "@ebs/components";
import React, { useEffect, useState, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { client } from "utils/apollo";
import { FIND_NODE_LIST_BY_ID } from "utils/apollo/gql/workflow";
const { DialogActions, Button } = Core;
import { createCFMutation, fetchDefinition } from "./functions";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { getCurrentStage } from "../StepperBar/functions";
import { getNodesToExecute } from "components/service/functions";
import { useNavigate } from "react-router-dom";
import { useExecuteProcess } from "hooks/useExecuteProcess";
import { showForm } from "store/modules/generate";
import { useServiceContext } from "custom-components/context/service-context";

const GenerateForm = ({ _nodeId, buttonRef, modal, _rowData }) => {
  const { executeProcess } = useExecuteProcess();
  const { rowData, setBlockButtons,setLoading } = useServiceContext();
  const { setRecord } = useContext(WorkflowContext);
  const { nodeId } = useSelector(({ wf_generate }) => wf_generate);
  const [node, setNode] = useState(null);
  const [nodeForm, setNodeForm] = useState(null);
  const [customFields, setCustomFields] = useState(null);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { workflowValues } = useSelector(({ wf_workflow }) => wf_workflow);
  useEffect(() => {
    if (nodeId || _nodeId) {
      const fetchData = async () => {
        const { data } = await client.query({
          query: FIND_NODE_LIST_BY_ID,
          variables: {
            filters: [{ col: "id", mod: "EQ", val: nodeId || _nodeId }],
          },
        });
        let node = data.findNodeList.content[0];
        let _node = await fetchDefinition(node, dispatch);
        setNodeForm(_node);
        setNode(node);
        setCustomFields(node.nodeCFs);
      };
      fetchData();
    }
  }, [_nodeId, nodeId]);

  const submitAction = async (data) => {
    let row_data;
    if (modal)
      row_data = _rowData
    else
      row_data = rowData
    if (!row_data) return;
    setBlockButtons(true);
    const stage = await getCurrentStage(row_data);
    let newComponents = customFields.map(cf => cf);
    let newObj = {
      customFields: newComponents,
      id: row_data.id,
      workflowInstanceId: row_data.wfInstance,
      stageId: stage.stage.id,
      nodeId: Number(node.id),
      ...data,
    }
    try {
      await createCFMutation(newObj);
      await moveNode(stage);
      if (modal) {
        dispatch(showForm(false));
        exitForm();
      }

    } catch (error) {
      console.error(error)
      exitForm();
    }

  };
  const exitForm = () => {
    setRecord(rowData);
    setBlockButtons(false);
    navigate("/wf/request", {
      state: {
        name: workflowValues.name,
        id: workflowValues.id,
        description: workflowValues.description,
      },
    });
  };
  const moveNode = async (currentStage) => {
    const _node = node;
    const createdObj = { id: rowData.id, workflowInstanceId: rowData.wfInstance };
    let validationResult;
    const edgesNodes = getNodesToExecute(_node, [currentStage.stage]);
    if (edgesNodes.length === 0) {
      let node = _node;
      await executeProcess({ node, createdObj });
      if (!modal)
        setTimeout(() => {
          exitForm();
        }, 1500);
    }
    else {
      for (let i = 0; i < edgesNodes.length; i++) {
        let node = edgesNodes[i];
        if (node.nodeType.name === "process") {
          validationResult = await executeProcess({ node, createdObj });
          if (validationResult) console.error(validationResult);
        }
      }
      if (!modal)
        setTimeout(() => {
          exitForm();
        }, 1500);
    }

  }
  const onError = (errors, e) => {
    setLoading(false);
    setBlockButtons(false);
    console.error(errors)
  };
  if (!nodeForm) return null;
  return (
    <EbsForm
      definition={() => nodeForm.formDefinition || { components: [] }}
      onSubmit={submitAction}
      actionPlacement="bottom"
      onError={onError}
    >
      <DialogActions>
        <Button
          onClick={() => dispatch(showForm(false))}
          style={{ display: modal ? "block" : "none" }}
          variant={"outlined"}
        >
          {"Exit form and fill later"}
        </Button>
        <Button
          ref={buttonRef}
          style={{ display: buttonRef ? "none" : "block" }}
          type="submit"
          variant={"outlined"}
        >
          {"Save"}
        </Button>
      </DialogActions>
    </EbsForm>
  );
};

// Type and required properties
GenerateForm.propTypes = {
  _nodeId: PropTypes.number,
  buttonRef: PropTypes.object,
};

export default GenerateForm;
