import { Core, Icons, EbsDialog } from "@ebs/styleguide";
import PropTypes from "prop-types";
import { deleteRecord } from "./functions";
const { IconButton, DialogContent, DialogActions, Button, Typography, Tooltip } = Core;
const { Delete } = Icons;
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { showMessage } from "store/modules/message";
import { useState } from "react";

const DeleteAction = ({ rowData, refresh }) => {
  const { workflowVariables } = useSelector(({ wf_workflow }) => wf_workflow);
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const handleClose = () => {
    setOpen(false);
  };
  const deleteRow = async () => {
    const data = {
      id: rowData.id,
      entity: workflowVariables.entity,
    };
    let resp = await deleteRecord(data);
    if (resp) {
      dispatch(
        showMessage({
          variant: "success",
          message: "Deleted",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    } else {
      dispatch(
        showMessage({
          variant: "error",
          message: "No Deleted",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
    refresh();
  };
  return (
    <>
      <EbsDialog
        handleClose={handleClose}
        open={open}
        maxWidth="sm"
        title={<FormattedMessage id="none" defaultMessage="Delete Record" />}
      >
        <DialogContent>
          <Typography variant={"subtitle"}>
            <FormattedMessage id="none" defaultMessage="Are you sure?" />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            onClick={handleClose}
          >
            {"Cancel"}
          </Button>
          <Button
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            onClick={deleteRow}
          >
            {"Delete"}
          </Button>
        </DialogActions>
      </EbsDialog>
      <Tooltip
        arrow
        placement="top"
        title={
          <Typography className="font-ebs text-lg">
            <FormattedMessage id={"none"} defaultMessage={"Delete Request"} />
          </Typography>
        }
      >
        <IconButton size={"small"} color={"primary"} onClick={() => setOpen(true)}>
          <Delete />
        </IconButton>
      </Tooltip>
    </>
  );
};
// Type and required properties
DeleteAction.propTypes = {
  refresh: PropTypes.func,
  rowData: PropTypes.object,
};
// Default properties

export default DeleteAction;
