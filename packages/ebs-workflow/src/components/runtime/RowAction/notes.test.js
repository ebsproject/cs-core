import { render, screen,  } from "@testing-library/react";
import MessagingModal from "./notes";

describe("Messaging Component", ()=>{
    const renderComponent  = (props) => render(
        <MessagingModal open={props.open} handleClose ={props.handleClose} data={props.data}/>
    )
    
    test("Load Messaging modal", ()=>{
        const props = {
            data:JSON.stringify({}),
            open: true,
            handleClose: jest.fn()
        }
        renderComponent(props);
        expect(screen.getByTestId("MessagingModalTestId")).toBeInTheDocument();
    })
    test("Not Load Messaging modal", ()=>{
        const props = {
            data:JSON.stringify({}),
            open: false,
            handleClose: jest.fn()
        }
        renderComponent(props);
        expect(screen.queryByTestId("MessagingModalTestId")).not.toBeInTheDocument();
    })
})