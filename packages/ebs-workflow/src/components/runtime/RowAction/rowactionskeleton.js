import React from 'react';
import { Core, Lab } from "@ebs/styleguide";
const {  Grid, Box }  = Core;
const { Skeleton }  = Lab;

const RowActionSkeleton = () => {
    return (
        <Grid container spacing={1}>
        {Array.from(new Array(7)).map((_, index) => (
          <Grid item key={index}>
            <Skeleton variant="circle" width={20} height={20} />
          </Grid>
        ))}
      </Grid>
    );
  };
  
  export default RowActionSkeleton;
