import { useEffect, useState } from "react";
import { validateActionsAllowed } from "validations";

const ExecuteValidation = ({ type, children, rowData, statusNodes, userProfile }) => {
    const [result, setResult] = useState(null);
    const [access, setAccess] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            let result = await validateActionsAllowed(rowData, statusNodes, userProfile);
            setResult(result);
        }
        fetchData();
    }, [type]);

    useEffect(() => {
        if (result) {
            let value = result[type]
            setAccess(value)
        }
    }, [result]);
    if (!result) return null;

    return access && children ;
}
export default ExecuteValidation;