import React from "react";
import PropTypes from "prop-types";
import "@ebs/messaging";
import { Core } from "@ebs/styleguide";
const { Dialog, DialogContent } = Core;
import { getCoreSystemContext, getTokenId } from "@ebs/layout";
const tokenID = getTokenId();
const { graphqlUri } = getCoreSystemContext();
const MessagingModal = ({ open, data, handleClose }) => {
  if(!open) return null;
  return (
    <Dialog open={open} onClose={handleClose}>
      <DialogContent>
        <ebs-messaging
          data-testid={"MessagingModalTestId"}
          endPoint={graphqlUri}
          authorization_token={tokenID}
          data={data}
        ></ebs-messaging>
      </DialogContent>
    </Dialog>
  );
};
// Type and required properties
MessagingModal.propTypes = {
  handleClose: PropTypes.func,
  open: PropTypes.bool,
  data: PropTypes.string,
};
// Default properties

export default MessagingModal;
