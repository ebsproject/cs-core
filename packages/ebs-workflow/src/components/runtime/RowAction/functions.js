import { FIND_CONTACT_BY_ID } from "custom-components/AddressForm/queries";
import { client } from "utils/apollo";
import { FIND_CONTACT_LIST } from "utils/apollo/gql/catalogs";
import { buildDeleteMutation, MODIFY_JOB_LOG } from "utils/apollo/gql/workflow";
import validateFunctions from "validations";
import { getJobLogsList } from "../MainBrowser/functions";

export function checkPermissions(userPermissions, _nodeSecurityDefinition) {
  let nodeSecurityDefinition = { ..._nodeSecurityDefinition }
  let access = false;
  if (!nodeSecurityDefinition)
    return access;
  if (!Object.keys(nodeSecurityDefinition).includes('roles') || nodeSecurityDefinition.roles === null)
    nodeSecurityDefinition = { ...nodeSecurityDefinition, ...{ roles: [] } };
  if (!Object.keys(nodeSecurityDefinition).includes('programs') || nodeSecurityDefinition.programs === null)
    nodeSecurityDefinition = { ...nodeSecurityDefinition, ...{ programs: [] } };
  if (!Object.keys(nodeSecurityDefinition).includes('users') || nodeSecurityDefinition.users === null)
    nodeSecurityDefinition = { ...nodeSecurityDefinition, ...{ users: [] } };

  userPermissions.roles.forEach(role => {
    if (nodeSecurityDefinition.roles.map(role => role.name).includes(role))
      access = true;
  });

  userPermissions.programs.forEach(program => {
    if (nodeSecurityDefinition.programs.map(item => Number(item.id)).includes(Number(program.id)))
      access = true;
  });

  if (nodeSecurityDefinition.users.map(item => Number(item.id)).includes(Number(userPermissions.userId)))
    access = true;

  return access;
}

export async function deleteRecord(data) {
  try {
    const MUTATION = buildDeleteMutation(data.entity);
    const { data: resp } = await client.mutate({
      mutation: MUTATION,
      variables: { id: data.id },
    });
    if (resp) return true;
  } catch (error) {
    console.log(error);
    return false;
  }
}
export const extractPPNodes = async (raNodes, allPPNodes, wf, userPermissions, obj) => {

  let pp = raNodes.filter(item => item.process.code === "PP");
  const all = allPPNodes.filter(item => item.process.code === "PP");
  const allFlowNodes = all.filter(a => a.define.allFlow);

  const phase = wf[0].stage.phase;
  let allPPNodesInPhase = [];
  phase.stages.forEach(item => {
    item.nodes.forEach(n => {
      if (n['process'] && n.process.code === "PP" && n.define.allPhase)
        allPPNodesInPhase = [...allPPNodesInPhase, n]
    })
  })
  pp = [...pp, ...allPPNodesInPhase]
  pp = [...pp, ...allFlowNodes];
  const uniqueArray = removeDuplicatesPP(pp);

  let secureNodes = [];
  uniqueArray.forEach(node => {
    if (checkPermissions(userPermissions, node.securityDefinition))
      secureNodes = [...secureNodes, node];
  })

  let finalNodes = [];
  for (let i = 0; i < secureNodes.length; i++) {
    const node = secureNodes[i];
    // if (node.define.rules) {
    //   let functionName = node.define.rules.validationFunction;
    //   if (functionName.length > 0) {
    //     const fn = validateFunctions[functionName];
    //     const res = await fn(obj.id, obj.wfId, obj.userId, node.define.rules);
    //     if (res.result) {
    //       finalNodes.push(node);
    //     }
    //   } else {
    //     finalNodes.push(node);
    //   }
    // } else {
      finalNodes.push(node);
   // }
  }
  return finalNodes;
}

const removeDuplicatesPP = (array) => {
  const seenIds = new Set();
  return array.reduce((acc, current) => {
    if (!seenIds.has(current.define.template)) {
      seenIds.add(current.define.template);
      acc.push(current);
    }
    return acc;
  }, []);
};

export async function setReadMessages(rowData) {
  let response = await getJobLogsList(rowData.id | 0);
  let result = false;

  if (response.length === 0) return;
  let item = response[0];
  let message = item['message'];
  if (!message) return;
  let messageObject = message['message'];
  if (!messageObject) return;
  let messagingArray = messageObject["direct_messaging"];
  if (!messagingArray) return;
  if (!Array.isArray(messagingArray)) return;
  let newMessagingArray = [];
  for (let i = 0; i < messagingArray.length; i++) {
    let messagingObj = messagingArray[i];
    if (messagingObj.status === 'unread') {
      result = true;
    }
    messagingObj = { ...messagingObj, status: 'read' }
    newMessagingArray.push(messagingObj)
  }
  message.message.direct_messaging = newMessagingArray
  if (result) {
    let newObj = {
      id: item.id,
      jobWorkflowId: item.jobWorkflow.id,
      message: message,
      tenantId: 1,
      contactIds: item.contacts.map(i => i.id)
    }
    await modifyJobLog(newObj);
  }
  return result;
}

async function modifyJobLog(input){

  try {
    await client.mutate({
      mutation: MODIFY_JOB_LOG,
      variables:{jobLog: input}
    })
  } catch (error) {
    console.log(error)
  }

}