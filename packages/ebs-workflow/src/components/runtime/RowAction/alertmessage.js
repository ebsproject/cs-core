import {Core, EbsDialog, Icons} from "@ebs/styleguide";
const { DialogContent, Typography} = Core;
const {Warning} = Icons;
import { FormattedMessage } from "react-intl";
const AlertMessage = ({open}) =>{
    return(
        <EbsDialog
        open={open}
        handleClose={()=>{}}
        maxWidth="sm"
        title={
          <div>
            <Warning className="mr-2" style={{ color: "#efb810" }} />
            <FormattedMessage
              id="none"
              defaultMessage={"Executing actions"}
            />
          </div>
        }
      >
        <DialogContent>
          <Typography className="text-ebs text-black text-base">
            <FormattedMessage
              id="none"
              defaultMessage="Please wait while your request is being processed."
            />
          </Typography>
        </DialogContent>
      </EbsDialog>
    )
}
export default AlertMessage;