import { useState, useRef } from "react";
import { Core, Icons } from "@ebs/styleguide";
const { PrintRounded } = Icons;
const { Tooltip, Popper, ClickAwayListener, Typography,
    Grow, Paper, List, ListItem, IconButton, Icon, ListItemIcon, ListItemText } = Core;


const PrintPreviewButton = ({ executeNodeProcess, ppNodes }) => {

    const [openMenu, setOpenMenu] = useState(false);

    const handleCloseMenu = () => {
        setOpenMenu(false)
    }
    const anchorRef = useRef(null);
    const handleToggle = () => {
        setOpenMenu((prevOpen) => !prevOpen);
    };

    return (
        <>
            <Tooltip arrow placement="top" title={
                <Typography className="font-ebs text-xl">
                    {"Click to print available templates"}
                </Typography>
            }>
                <IconButton
                    ref={anchorRef}
                    size={"small"}
                    onClick={handleToggle}
                    color={"primary"}
                >
                    <Icon>{"PrintRounded".toLowerCase()}</Icon>
                </IconButton>
            </Tooltip>
            <Popper open={openMenu} anchorEl={anchorRef.current} transition>
                {({ TransitionProps, placement }) => (
                    <Grow
                        {...TransitionProps}
                        style={{
                            transformOrigin:
                                placement === 'bottom' ? 'center top' : 'center bottom',
                        }}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleCloseMenu}>
                            <List id='pp-template'>
                            {ppNodes && ppNodes.map(pp => (
                                <ListItem key={pp.id}
                                    onClick={() => { handleCloseMenu(); executeNodeProcess(pp); }}
                                    style={{ cursor: 'pointer' }}
                                >
                                    <ListItemIcon>
                                        <PrintRounded color="primary"/>
                                        {"|"}{pp.help}
                                    </ListItemIcon>
                                 
                                </ListItem>
                            ))}
                        </List>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </>
    )
}
export default PrintPreviewButton;