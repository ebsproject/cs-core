import { Core, Icons } from "@ebs/styleguide";
const { Box, Icon, Tooltip, Typography, IconButton } = Core;
const { Edit, Chat, Visibility, AccountTree } = Icons;
import SideBar from "../SideBar/SideBar";
import { useSelector } from "react-redux";
import { FIND_EVENT_LIST } from "utils/apollo/gql/workflow";
import { checkPermissions } from "validations";
import DeleteAction from "./DeleteAction";
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
import { useNavigate } from "react-router-dom";
import React, { useState, useContext, useEffect } from "react";
import { useExecuteProcess } from "hooks/useExecuteProcess";
import { RBAC } from "@ebs/layout";
import loader from "assets/images/time_loader.gif";
import RowActionSkeleton from "./rowactionskeleton";
import { extractPPNodes, setReadMessages } from "./functions";
import ExecuteValidation from "./executeValidation";
import { userContext } from "@ebs/layout";
import { useServiceContext } from "custom-components/context/service-context";
import DialogBox from "../DialogBox";
import RequestMessage from "components/service/alert";
import MessagingModal from "./notes";
// const RequestMessage = React.lazy(() => import('components/service/alert'));
// const MessagingModal = React.lazy(() => import('./notes'));
import PrintPreviewButton from "./printpreviewbutton";
//const PrintPreviewButton = React.lazy(()=> import("./printpreviewbutton"));
import MessageBadge from "../MainBrowser/message-badge";

const RowAction = (rowData, refresh) => {
  const { setMode, setRowData } = useServiceContext();
  const { userProfile } = useContext(userContext);
  const { userPermissions } = useSelector(({ wf_user }) => wf_user);
  const { workflowVariables, ppNodes, workflowValues, statusNodes } = useSelector(({ wf_workflow }) => wf_workflow);
  const { executeProcess } = useExecuteProcess();
  const navigate = useNavigate();
  const [load, setLoad] = useState(false);
  const [loadView, setLoadView] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [open, setOpen] = useState(false);
  const [dataRow, setData] = useState(null);
  const [rowActionsNode, setRowActionsNode] = useState(null);
  const [printPreviewNodes, setPrintPreviewNodes] = useState(null);
  const [openView, setOpenView] = useState(false);
  const handleCloseView = () => setOpenView(!openView);
  const [_definition, _setDefinition] = useState(null);
  const [openSideBar, setOpenSideBar] = useState(false);
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await client.query({
        query: FIND_EVENT_LIST,
        variables: {
          filters: [
            { col: "workflowInstance.id", mod: "EQ", val: Number(rowData.wfInstance) },
            { col: "recordId", mod: "EQ", val: Number(rowData.id) },
          ],
        },
        fetchPolicy: "no-cache",
      });
      let wf = data.findEventList.content.filter((item) => item.completed === null);
      let actionRowNodes = wf.length > 0 ? wf[0].stage.nodes.filter((item) => item.nodeType.name === "rowAction") : [];
      setRowActionsNode(actionRowNodes.filter(item => item.process.code !== "PP"));
      const obj = {
        id: rowData.id,
        userId: userProfile.dbId,
        wfId: workflowValues["id"]
      }
      let PPNodes = ppNodes ? [...ppNodes] : [];
      const _ppNodes = await extractPPNodes(actionRowNodes, PPNodes, wf, userPermissions, obj);
      setPrintPreviewNodes(_ppNodes.filter(item => item.nodeType.name === "rowAction"))
    };
    fetchData();
  }, []);

  const executeNodeProcess = async (node) => {
    setDisabled(true);
    let createdObj = {
      id: rowData.id,
      workflowInstanceId: rowData.wfInstance,
    };
    await executeProcess({ node, createdObj });
    setTimeout(() => {
      refresh();
      setDisabled(false);
    }, 2700);
  };
  const navigateToEditMode = async (mode) => {
    setRowData(rowData);
    if (mode === "edit") {
      setMode("edit");
      navigate("/wf/request/service",{
        state: { mode: mode, definition: {}, rowData: rowData },
      });
    } else {
      setMode("view");
      setOpenView(true);
    }

  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleOpen = async () => {
    let result = await setReadMessages(rowData);
    if (result) setCounter(0);
    setData(
      JSON.stringify({
        type: "record",
        recordId: rowData?.id,
        name: `${workflowVariables.entity} request`,
        entity: workflowVariables.entity,
      })
    );
    setOpen(true);
  };
  const handleOpenSideBar = () =>{
    setRowData(rowData);
    setOpenSideBar(true);
  }
  if (!rowActionsNode || !statusNodes) return <RowActionSkeleton />;
  return (
    <Box component={"div"} className={"flex flex-row"} data-testid={"RowActionsGirdBrowserTestId"}>
      <SideBar open={openSideBar} rowData={rowData} setOpen={setOpenSideBar}/>
      <DialogBox rowData={rowData} open={openView} handleClose={handleCloseView} />
      <RequestMessage open={disabled} mode={"edit"} />
      <MessagingModal open={open} data={dataRow} handleClose={handleClose} />
      <ExecuteValidation type="allowView" rowData={rowData} statusNodes={statusNodes} userProfile={userProfile}>
        
          {loadView ? (
            <img src={loader} width={20} height={20} alt="Loading..." />
          ) : (
            <Tooltip
              arrow
              placement="top"
              title={
                <Typography className="font-ebs text-lg">
                  <FormattedMessage id={"none"} defaultMessage={"View Request"} />
                </Typography>
              }
            >
              <IconButton size={"small"} onClick={() => navigateToEditMode("view")} color={"primary"}>
                <Visibility />
              </IconButton>
            </Tooltip>
          )}
       
      </ExecuteValidation>
      <ExecuteValidation type="allowEdit" rowData={rowData} statusNodes={statusNodes}userProfile={userProfile}>
      
          {load ? (
            <img src={loader} width={20} height={20} alt="Loading..." />
          ) : (
            <Tooltip
              arrow
              placement="top"
              title={
                <Typography className="font-ebs text-lg">
                  <FormattedMessage id={"none"} defaultMessage={"Edit Request"} />
                </Typography>
              }
            >
              <IconButton size={"small"} onClick={() => navigateToEditMode("edit")} color={"primary"}>
                <Edit />
              </IconButton>
            </Tooltip>
          )}
  
      </ExecuteValidation>
      <ExecuteValidation type="allowDelete" rowData={rowData} statusNodes={statusNodes} userProfile={userProfile}>
    
          <DeleteAction rowData={rowData} refresh={refresh} />
  
      </ExecuteValidation>
      <ExecuteValidation type="allowAddNotes" rowData={rowData} statusNodes={statusNodes} userProfile={userProfile}>
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id={"none"} defaultMessage={"Add notes"} />
            </Typography>
          }
        >
          <IconButton size={"small"} onClick={handleOpen} color="primary">
            <MessageBadge counter={counter} setCounter={setCounter} rowData={rowData}>
              <Chat />
            </MessageBadge>
          </IconButton>
        </Tooltip>
      </ExecuteValidation>
      <ExecuteValidation type="allowViewProgress" rowData={rowData} statusNodes={statusNodes} userProfile={userProfile}>
      <IconButton onClick={handleOpenSideBar}  color="primary" size={"small"}>
            <AccountTree />
          </IconButton>
      </ExecuteValidation>
      {rowActionsNode &&
        rowActionsNode.sort((a, b) => { return Number(a.sequence || 0) - Number(b.sequence || 0) }).map((node) => {
          return (
            checkPermissions(userPermissions, node.securityDefinition) && !disabled && (
              <Tooltip
                key={node.icon}
                arrow
                placement="top"
                title={
                  <Typography className="font-ebs text-xl">
                    <FormattedMessage id="none" defaultMessage={node.help} />
                  </Typography>
                }
              >
                <IconButton
                  size={"small"}
                  onClick={() => executeNodeProcess(node)}
                  color={"primary"}
                >
                  <Icon>{node.icon ? node.icon.toLowerCase() : "error"}</Icon>
                </IconButton>
              </Tooltip>
            )
          );
        })}
      {
        printPreviewNodes && printPreviewNodes.length > 0 && (
          <PrintPreviewButton executeNodeProcess={executeNodeProcess} ppNodes={printPreviewNodes} />
        )
      }
    </Box>
  );
};
export default RowAction;
