export function checkPermissions(userPermissions, nodeSecurityDefinition) {
  let access = false;
  if (!nodeSecurityDefinition) return access;
  if (
    !Object.keys(nodeSecurityDefinition).includes("roles") ||
    nodeSecurityDefinition.roles === null
  )
    nodeSecurityDefinition.roles = [];
  if (
    !Object.keys(nodeSecurityDefinition).includes("programs") ||
    nodeSecurityDefinition.programs === null
  )
    nodeSecurityDefinition.programs = [];
  if (
    !Object.keys(nodeSecurityDefinition).includes("users") ||
    nodeSecurityDefinition.users === null
  )
    nodeSecurityDefinition.users = [];

  userPermissions.roles.forEach((role) => {
    if (nodeSecurityDefinition.roles.map((role) => role.name).includes(role)) access = true;
  });

  userPermissions.programs.forEach((program) => {
    if (nodeSecurityDefinition.programs.map((item) => Number(item.id)).includes(Number(program.id)))
      access = true;
  });

  if (
    nodeSecurityDefinition.users
      .map((item) => Number(item.id))
      .includes(Number(userPermissions.userId))
  )
    access = true;

  return access;
}
