import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { useSelector, useDispatch } from "react-redux";
import { hideForm } from "store/modules/generate";
import FormLoader from "./FormLoader";
import { useServiceContext } from "custom-components/context/service-context";
jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useDispatch: jest.fn(),
    useSelector: jest.fn(),
  }));
jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));
jest.mock("store/modules/generate", () => ({
  hideForm: jest.fn(),
}));
jest.mock("../GenerateForm/generateForm", () => () => <div data-testid="generateForm">Generate Form</div>);

describe("FormLoader Component", () => {
    const mockDispatch = jest.fn();
  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useSelector.mockImplementation((selectorFn) => {
        return selectorFn({
            wf_generate: {
                show: true, 
              },
        });
      });

    useServiceContext.mockReturnValue({
      rowData: { id: 1, name: "Test Row" },
    });

  });
  afterEach(() => {
    jest.clearAllMocks();
  });
//   test("renders Dialog when 'show' is true", () => {
//     render( <FormLoader />);
//     const dialog = screen.getByRole("dialog");
//     expect(dialog).toBeInTheDocument();
//     expect(dialog).toHaveAttribute("aria-hidden", "false");
//   });

  test("does not render Dialog when 'show' is false", () => {
    useSelector.mockImplementation((selectorFn) => {
        return selectorFn({
            wf_generate: {
                show: false, 
              },
        });
      });

    render(<FormLoader /> );
    expect(screen.queryByRole("dialog")).not.toBeInTheDocument();
  });

  test("renders GenerateForm component within DialogContent", () => {
   const {container} = render( <FormLoader />);
    const generateForm = screen.getByTestId("generateForm");
    expect(generateForm).toBeInTheDocument();
  });

//   test("dispatches hideForm action on Dialog close", () => {
//     render( <FormLoader /> );
//     const dialog = screen.getByRole("dialog");
//     fireEvent.click(dialog);
//     expect(mockDispatch).toHaveBeenCalledWith(hideForm(false));
//   });
});
