import { useDispatch, useSelector } from "react-redux";
import { hideForm } from "store/modules/generate";
import GenerateForm from "../GenerateForm/generateForm";
import { Core } from "@ebs/styleguide";
const { DialogContent, Dialog } = Core;
import { useState } from "react";
import { useServiceContext } from "custom-components/context/service-context";
const FormLoader = () => {
  const { rowData } = useServiceContext();
  const dispatch = useDispatch();
  const { show } = useSelector(({ wf_generate }) => wf_generate);
  const [formName, setFormName] = useState("");
  const handleClose = () => {
    dispatch(hideForm(false));
  };
  return (
    <Dialog
      open={show}
      onClose={handleClose}
      maxWidth="lg"
      fullWidth
    >
      <DialogContent>
        <GenerateForm _nodeId={null} buttonRef={null} modal={true} _rowData = {{...rowData}}  />
      </DialogContent>
    </Dialog>
  );
};
export default FormLoader;
