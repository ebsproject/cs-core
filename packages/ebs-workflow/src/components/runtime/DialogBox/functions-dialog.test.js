import { createDefinitionCF, createDefinitionFromEntity } from "./functions";
import { getOptions } from "components/service/functions"
import { client } from "utils/apollo";
import validateFunctions from "validations";

jest.mock("utils/apollo", () => ({
    client: {
        query: jest.fn(),
    },
}));

jest.mock("components/service/functions", ()=>({
    getOptions: jest.fn(),
    validateFunctions: {testFunction:jest.fn()}
}))

describe("call createDefinitionCF function", () => {

    test("empty custom fields array", async () => {
        const cf = [];
        const response = await createDefinitionCF(cf);
        expect(response.components.length).toBe(0);
        expect(response.customFieldsData.length).toBe(0);
    });
    test("custom fields array provided", async () => {

        const cf = [
            { id: 1, name: "A", event: { id: 1 }, nodeCF: { required: true, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: { inputProps:{label:"some label"},rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3],defaultRules:{applyRules:true} } } },
            { id: 2, name: "B", event: { id: 2 }, nodeCF: { required: true, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3],defaultOptions: [], defaultRules:{applyRules:false} } } },
            { id: 3, name: "C", event: { id: 3 }, nodeCF: { required: true, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: true, sizes: [3, 3, 3, 3, 3],defaultRules:{applyRules:false} } } },
            { id: 4, name: "D", event: { id: 4 }, nodeCF: { required: true, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: true, disabled: false, sizes: [3, 3, 3, 3, 3],defaultOptions: [] ,defaultRules:{applyRules:true} } } },
            { id: 5, name: "E", event: { id: 5 }, nodeCF: { required: true, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: true, disabled: true, } } },
            { id: 6, name: "F", event: { id: 6 }, nodeCF: { required: true, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3],defaultRules:{applyRules:true}, defaultOptions: [] } } },
            { id: 7, name: "G", event: { id: 7 }, nodeCF: { required: true, cfType: { name: "textfield" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 8, name: "H", event: { id: 7 }, nodeCF: { required: true, cfType: { name: "datepicker" }, apiAttributesName: "", fieldAttributes: { inputProps:{label:"some label"},rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 9, name: "I", event: { id: 8 }, nodeCF: { required: true, cfType: { name: "checkbox" }, apiAttributesName: "", fieldAttributes: { inputProps:{label:"some label"},rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 10, name: "J", event: { id: 9 }, nodeCF: { required: true, cfType: { name: "radiogroup" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 11, name: "K", event: { id: 10 }, nodeCF: { required: true, cfType: { name: "switch" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 12, name: "L", event: { id: 11 }, nodeCF: { required: true, cfType: { name: "some other no covered" }, apiAttributesName: "", fieldAttributes: { inputProps:{label:"some label"},rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 13, name: "M", event: { id: 11 }, nodeCF: { required: true, cfType: { name: "some other no covered" }, apiAttributesName: "Test with name", fieldAttributes: { inputProps:{label:"some label"},rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },
            { id: 14, name: "N", event: { id: 11 }, nodeCF: { required: true, cfType: { name: "some other no covered" }, apiAttributesName: "", fieldAttributes: { inputProps:{label:"some label"},rules: {}, disabledAfterCreation: false, disabled: false, defaultOptions: [] } } },
            { id: 15, name: "O", event: { id: 2 }, nodeCF: { required: false, cfType: { name: "select" }, apiAttributesName: "", fieldAttributes: {inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3],defaultOptions: [], defaultRules:{applyRules:false} } } },
            { id: 16, name: "K", event: { id: 10 }, nodeCF: { required: true, cfType: { name: "switch" }, apiAttributesName: "", fieldAttributes: {function:"testFunction",inputProps:{label:"some label"}, rules: {}, disabledAfterCreation: false, disabled: false, sizes: [3, 3, 3, 3, 3], defaultOptions: [] } } },

        ];
      
        const response = await createDefinitionCF(cf);
        expect(response.components.length).toBeGreaterThan(0);
        expect(response.customFieldsData.length).toBeGreaterThan(0);
    
    });

})

describe("call createDefinitionFromEntity function", ()=>{

    test("call with empty array", async ()=>{
        const wf = {allAttributes:[]}
        const defaultValues ={}
        const response = await createDefinitionFromEntity(wf, defaultValues);
         expect(response.components.length).toBe(0);
    })
    test("call with data", async ()=>{
        getOptions.mockResolvedValue([]);
        const wf = {allAttributes:
            [
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:false,}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:true,}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],inputProps:{label:"some label"},sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"textfield"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],inputProps:{label:"some label"},sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",required:true,fieldAttributes:{rules:{},disabledAfterCreation:true,sizes:[2,2,2,2,2],inputProps:{label:"some label"},sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"textfield"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:false,stateValues:{name:"some name"}}},
            ]}
        const defaultValues ={"Name in API":"Some value"}
        const response = await createDefinitionFromEntity(wf, defaultValues);
        expect(response.components.length).toBeGreaterThan(0);
    })
    test("call with data and defaultValues no provided ", async ()=>{
        getOptions.mockResolvedValue([]);
        const wf = {allAttributes:
            [
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:false,}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:true,}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],inputProps:{label:"some label"},sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"textfield"},name:"Test_Name",attributes:"Test",fieldAttributes:{sizes:[2,2,2,2,2],inputProps:{label:"some label"},sort:1,disabled:false,stateValues:{name:"some name"}}},
                {apiAttributesName:"Name in API",cfType:{description:"select"},name:"Test_Name",attributes:"Test",required:true,fieldAttributes:{rules:{},disabledAfterCreation:true,sizes:[2,2,2,2,2],inputProps:{label:"some label"},sort:1,disabled:false,stateValues:{name:"some name"}}},
            ]}
        const defaultValues = null
        const response = await createDefinitionFromEntity(wf, defaultValues);
        expect(response.components.length).toBeGreaterThan(0);
    })
})