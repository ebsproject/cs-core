import DialogBox from "./DialogBox";
import { render, screen, act, waitFor } from "@testing-library/react";
import { client } from "utils/apollo";
import { createDefinitionCF, createDefinitionFromEntity } from "./functions";
import { useSelector } from "react-redux";

jest.mock("react-redux", () => ({
    ...jest.requireActual("react-redux"),
    useSelector: jest.fn(),
}));
jest.mock("utils/apollo", () => ({
    client: {
        query: jest.fn(),
    },
}));
jest.mock("./functions", () => ({
    createDefinitionCF: jest.fn(),
    createDefinitionFromEntity: jest.fn()
}))

jest.mock("components/service/request", () => () => <div data-testid={"RequestTestId"}>Request</div>);

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));
describe("Dialog Box component", () => {
    beforeEach(() => {
        jest.clearAllMocks();
        useSelector.mockImplementation((selectorFn) => {
            return selectorFn({
                wf_workflow: {
                    workflowVariables: {
                        allAttributes: [],
                    },
                },
            });
        });
    })
    const renderComponent = (props) => render(<DialogBox rowData={props.rowData} open={props.open} handleClose={props.handleClose} />)

    test("dialog box not loaded", async () => {
        const props = {
            rowData: { id: 1, requestCode: "Test code" },
            open: false,
            handleClose: jest.fn()
        }
        act(() => {
            renderComponent(props);
        })
        await waitFor(() => {
            expect(screen.queryByTestId("DialogBoxTestId")).not.toBeInTheDocument();
        });
    })
    test("dialog box loaded without data in call", async () => {
        client.query.mockResolvedValue({ data: { findCFValueList: { content: [] } } })
        createDefinitionFromEntity.mockReturnValue({ components: [{ id: 1, name: "test name", sort: 1 }] })
        createDefinitionCF.mockResolvedValue({ components: [{ id: 1, name: "test name", sort: 1 }] })
        const props = {
            rowData: { id: 1, requestCode: "Test code" },
            open: true,
            handleClose: jest.fn()
        }
        act(() => {
            renderComponent(props);
        });
        await waitFor(() => {
            expect(screen.getByTestId("DialogBoxTestId")).toBeInTheDocument();
        });
    });
    test("dialog box loaded with data in call", async () => {
        client.query.mockResolvedValue({ data: { findCFValueList: { content: [{ id: 1, name: "some name" }] } } })
        createDefinitionFromEntity.mockReturnValue({ components: [{ id: 1, name: "test name", sort: 1 }] })
        createDefinitionCF.mockResolvedValue({ components: [{ id: 1, name: "test name", sort: 1 }] })
        const props = {
            rowData: { id: 1, requestCode: "Test code" },
            open: true,
            handleClose: jest.fn()
        }
        act(() => {
            renderComponent(props);
        });
        await waitFor(() => {
            expect(screen.getByTestId("DialogBoxTestId")).toBeInTheDocument();
        })
    });
})
