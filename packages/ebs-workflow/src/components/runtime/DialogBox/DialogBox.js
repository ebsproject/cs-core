import { Core } from "@ebs/styleguide";
import PropTypes from "prop-types";
const {
  Typography,
  DialogContent,
  Dialog,
  DialogActions,
  DialogTitle,
  Button,
} = Core;
import { FormattedMessage } from "react-intl";
import React, { useState, useEffect } from "react";
import { createDefinitionCF, createDefinitionFromEntity } from "./functions";
import { useSelector } from "react-redux";
import { FIND_CF_VALUES_BY_EVENT_ID } from "utils/apollo/gql/workflow";
import { client } from "utils/apollo";
import Request from "components/service/request";

const DialogBox = ({ rowData, open, handleClose }) => {

  const { workflowVariables } = useSelector(({ wf_workflow }) => wf_workflow);
  const [_definition, _setDefinition] = useState({});
  useEffect(() => {
    if (open) {
      const fetchDefinition = async () => {
        let definition = { components: [] };
        const { data } = await client.query({
          query: FIND_CF_VALUES_BY_EVENT_ID,
          variables: {
            page: { size: 300, number: 1 },
            filters: [{ col: "event.recordId", mod: "EQ", val: Number(rowData?.id) }],
            sort: [{ col: "nodeCF.id", mod: "ASC" }],
          },
          fetchPolicy: "no-cache",
        });
        const cfValues = data.findCFValueList.content;
        if (cfValues.length > 0) {
          definition = await createDefinitionCF(cfValues);
        }
        let result = await createDefinitionFromEntity(workflowVariables, rowData);
        result.components.forEach((item) => {
          definition.components.push(item);
        });
        definition.components = definition.components.sort((a, b) => {
          return a.sort - b.sort;
        });
        _setDefinition(definition);
      }
      fetchDefinition();
    }


  }, [open]);

  if (!open) return null;
  return (

      <Dialog open={open} onClose={handleClose} maxWidth="xl" fullWidth data-testid={"DialogBoxTestId"}>
        <DialogTitle>
          <Typography className="font-ebs text-xl">
            <FormattedMessage id={"none"} defaultMessage={`Request Code: ${rowData.requestCode}`} />
          </Typography>
        </DialogTitle>
        <DialogContent style={{ height: 800 }}>
          <Request definition={_definition} />
        </DialogContent>
        <DialogActions>
          <Button variant={"outlined"} onClick={handleClose}>
            {"Close"}
          </Button>
        </DialogActions>
      </Dialog>

  );
};
// Type and required properties
DialogBox.propTypes = {
  mode: PropTypes.string,
  rowData: PropTypes.object,
};
// Default properties


export default DialogBox;
