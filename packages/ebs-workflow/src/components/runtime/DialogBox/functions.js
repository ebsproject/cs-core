import { getOptions } from "components/service/functions"
import { client } from "utils/apollo";
import { FIND_CONTACT_LIST, FIND_PROGRAM_LIST } from "utils/apollo/gql/request";
import validateFunctions from "validations";

export const createDefinitionFromEntity = async (wf, defaultValues, nodeId) => {
  try {
    let attributes = [...wf.allAttributes];
    const columns = attributes.filter(item => item.attributes);
    let components = [];
    for (let i = 0; i < columns.length; i++) {
        let item = columns[i];
      if (item.fieldAttributes.disabled || item.node.id !== nodeId)
        continue;
      let label = item.name.replace("_", " ").split(" ").map(item => { return item[0].toUpperCase() + item.substr(1) }).join(" ");
      let obj;
      if (item.cfType.description.toLowerCase() === "select") {
        let options = [] // await getOptions(item);
        if(item.fieldAttributes.stateValues){
          options = wf[item.fieldAttributes.stateValues.name];
        }
        obj = {
          sort: item.fieldAttributes.sort,
          component: "select",
          options: options,
          helper: item.fieldAttributes.helper,
          name: `${item.apiAttributesName}Id`,
          sizes: item.fieldAttributes.sizes,
          defaultRules: {...item.fieldAttributes.defaultRules, fieldAttributes: item.fieldAttributes},
          inputProps: item.fieldAttributes.inputProps ? item.fieldAttributes.inputProps : {
            "data-testid": item.name,
            label: label,
            variant:"outlined"
          }
        }
      }
      else {
        obj = {
          sort: item.fieldAttributes.sort,
          component: item.cfType.description.toLowerCase(),
          name: item.apiAttributesName,
          sizes: item.fieldAttributes.sizes,
          inputProps: item.fieldAttributes.inputProps ? item.fieldAttributes.inputProps : {
            "data-testid": item.name,
            label: label,
            variant: "outlined",
          }
        }
      }
      if (item.fieldAttributes["disabledAfterCreation"] )
        obj.inputProps = {...obj.inputProps, disabled: true}
      if(item.required)
         obj.rules = item.fieldAttributes.rules
      if (defaultValues) {
        if (item.cfType.description.toLowerCase() === "select") {
          let o = defaultValues[`${item.apiAttributesName}`];
        // let defaultValue = await getDefaultValues(o);
          obj.defaultValue = o;
        }
        else
          obj.defaultValue = defaultValues[`${item.apiAttributesName}`];
      }
      components = [...components, obj];
    }
    
    return {
      components: components
    }
  } catch (error) {
    console.log(error)
    return {
      components: []
    }
  }

}
// async function getDefaultValues(obj) {
//   let keys = Object.keys(obj);
//   let defaultValue = { label: "Check the Custom field definition!!", value: 0 };
//   let key = "program"
//   if (keys.includes('person'))
//     key = "person";
//   if (keys.includes('institution'))
//     key = "institution";
//   switch (key) {
//     case 'person':
//       let givenName = obj.person.givenName;
//       let familyName = obj.person.familyName;
//       const { data } = await client.query({
//         query: FIND_CONTACT_LIST,
//         variables: {
//           filters: [
//             { col: "person.familyName", val: familyName, mod: "EQ" },
//             { col: "person.givenName", val: givenName, mod: "EQ" },
//           ]
//         },
//         disjunctionFilters: true
//       })
//       let result = data.findContactList.content[0];
//       defaultValue = { label: `${result.person.familyName}, ${result.person.givenName}`, value: result.id }

//       break;
//       case 'program':
//         let programName = obj.name;
//         const { data :dataP } = await client.query({
//           query: FIND_PROGRAM_LIST,
//           variables: {
//             filters: [
//               { col: "name", val: programName, mod: "EQ" },
//             ]
//           },
//           disjunctionFilters: true
//         })
//         let resultP = dataP.findProgramList.content[0];
//         defaultValue = { label: `${resultP.name}`, value: resultP.id }
//         break;
//     default:
//       break;
//   }

//   return defaultValue;
// }
export const createDefinitionCF = async (_cf) => {
  let cf = [..._cf];
  let components = [];
  let customFieldsData= []
  for (let i = 0; i < cf.length; i++) {
    if (cf[i].nodeCF.apiAttributesName !== "")
      continue;
    if (cf[i].nodeCF.fieldAttributes.disabled || cf[i].nodeCF.fieldAttributes["disabledAfterCreation"] )
      continue;
    let sizes = [];
    if (cf[i].nodeCF.fieldAttributes.sizes)
      sizes = cf[i].nodeCF.fieldAttributes.sizes;
    else
      sizes = [12, 12, 6, 4, 2];
    switch (cf[i].nodeCF.cfType.name.toLowerCase()) {   
      case "select":
        let selectName = null;
        if(cf[i].nodeCF.apiAttributesName !== "" )
        {
           selectName = `${cf[i].nodeCF.apiAttributesName}Id`
        }
        let options = cf[i].nodeCF.fieldAttributes.defaultOptions ? cf[i].nodeCF.fieldAttributes.defaultOptions : cf[i].nodeCF.fieldAttributes.defaultRules.applyRules ? [] : await getOptions(cf[i].nodeCF);
        let selectComp = {
          sort:cf[i].nodeCF.fieldAttributes.sort,
          component: cf[i].nodeCF.cfType.name.toLowerCase(),
          name: selectName === null ? cf[i].nodeCF.name : selectName,
          sizes: sizes,
          options: options,
          inputProps: cf[i].nodeCF.fieldAttributes.inputProps ? cf[i].nodeCF.fieldAttributes.inputProps :  {
            "data-testid": `data-testid ${cf[i].name}`,
            label: cf[i].nodeCF.fieldAttributes.inputProps.label,
            variant:"outlined"
          },
          helper: { title: cf[i].description, placement: "top" },
          defaultRules:cf[i].nodeCF.fieldAttributes.defaultRules
        }
        if (cf[i].nodeCF.required) selectComp.rules = cf[i].nodeCF.fieldAttributes.rules;
        selectComp.defaultValue = { label: cf[i].textValue, value: cf[i].codeValue };
        components.push(selectComp);
        customFieldsData.push({cfId:cf[i].id, eventId:cf[i].event.id, name: cf[i].nodeCF.name,cfNode:cf[i].nodeCF})
        break;
      case "textfield":
        let textComp = {
          sort:cf[i].nodeCF.fieldAttributes.sort,
          component: cf[i].nodeCF.cfType.name.toLowerCase(),
          name: cf[i].nodeCF.name,
          sizes: sizes,
          inputProps: cf[i].nodeCF.fieldAttributes.inputProps ? cf[i].nodeCF.fieldAttributes.inputProps :  {
            "data-testid": cf[i].nodeCF.name,
            label: cf[i].nodeCF.name,
            multiline: true,
            rows: 2,
            variant: "outlined",
          },
          defaultRules:cf[i].nodeCF.fieldAttributes.defaultRules
        }
        if (cf[i].nodeCF.required) textComp.rules = cf[i].nodeCF.fieldAttributes.rules;
        textComp.defaultValue = extractDefaultCFValue(cf[i]);
        components.push(textComp);
        customFieldsData.push({cfId:cf[i].id, eventId:cf[i].event.id, name: cf[i].nodeCF.name,cfNode:cf[i].nodeCF})
        break;
      case "datepicker":
        let dateComp = {
          sort:cf[i].nodeCF.fieldAttributes.sort,
          component: cf[i].nodeCF.cfType.name.toLowerCase(),
          name: cf[i].nodeCF.name,
          sizes: sizes,
          inputProps:  cf[i].nodeCF.fieldAttributes.inputProps ? cf[i].nodeCF.fieldAttributes.inputProps :  {
            "data-testid": cf[i].nodeCF.name,
            label: cf[i].nodeCF.name,
            rows: 2,
            variant: "outlined",
          },
        }
        if (cf[i].nodeCF.required) dateComp.rules = cf[i].nodeCF.fieldAttributes.rules;
        dateComp.defaultValue = extractDefaultCFValue(cf[i]);
        components.push(dateComp);
        customFieldsData.push({cfId:cf[i].id, eventId:cf[i].event.id, name: cf[i].nodeCF.name,cfNode:cf[i].nodeCF})
        break;
        case "checkbox":
          let checkboxComp = {
            sort:cf[i].nodeCF.fieldAttributes.sort,
            component: cf[i].nodeCF.cfType.name.toLowerCase(),
            name: cf[i].nodeCF.name,
            sizes:sizes,
            inputProps: cf[i].nodeCF.fieldAttributes.inputProps,
          }
          checkboxComp.defaultValue = extractDefaultCFValue(cf[i]);
          components.push(checkboxComp);
          customFieldsData.push({cfId:cf[i].id, eventId:cf[i].event.id, name: cf[i].nodeCF.name,cfNode:cf[i].nodeCF})
          break;
        case "radiogroup":
         let radioComp = {
          sort:cf[i].nodeCF.fieldAttributes.sort,
          component: "radio",
          name: cf[i].nodeCF.name,
          row: false,
          options:cf[i].nodeCF.fieldAttributes.options,
          sizes:sizes,
          label: cf[i].nodeCF.fieldAttributes.inputProps.label,
        }
        radioComp.defaultValue = extractDefaultCFValue(cf[i]);
        components.push(radioComp);
        customFieldsData.push({cfId:cf[i].id, eventId:cf[i].event.id, name: cf[i].nodeCF.name,cfNode:cf[i].nodeCF})
        break;
        case "switch":
          let switchComp = {
          sort:cf[i].nodeCF.fieldAttributes.sort,
           component: "switch",
           name: cf[i].nodeCF.name,
           sizes:sizes,
           defaultRules:cf[i].nodeCF.fieldAttributes.defaultRules,
           label: cf[i].nodeCF.fieldAttributes.inputProps.label,
           onChange: cf[i].nodeCF.fieldAttributes.function ? (e, { setValue, getValues }) => validateFunctions[cf[i].nodeCF.fieldAttributes.function](e, { setValue, getValues }) : ()=>{},
         }
         switchComp.defaultValue = extractDefaultCFValue(cf[i]);
         components.push(switchComp);
         customFieldsData.push({cfId:cf[i].id, eventId:cf[i].event.id, name: cf[i].nodeCF.name,cfNode:cf[i].nodeCF})
         break;
      default: break;

    }

  }

  return {
    components: components,
    customFieldsData : customFieldsData
  }
}
function extractDefaultCFValue(item) {
  switch (item.nodeCF.cfType.name.toLowerCase()) {
    case "textfield": return item.textValue;
    case "checkbox": return item.flagValue;
    case "switch": return item.flagValue;
    case "datepicker": return item.dateValue;
    case "radiogroup": return item.textValue;
    case "select": return JSON.parse(item.textValue);
  }

}
