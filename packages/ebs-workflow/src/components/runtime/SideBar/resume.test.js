import { render, cleanup, screen } from "@testing-library/react";
import Resume from "./resume";

describe("Resume Component", () => {

    test("Resume component loaded with data", () => {
        const rowDataMock = {
            requestCode: "Test Code",
            status: "Created",
            sender: { label: "Test sender", value: 0 },
            recipient: { label: "Test recipient", value: 0 },
            requestor: { label: "Test requestor", value: 0 },
            program: { label: "Test Program", value: 0 },
            submitionDate: "2024-12-31"
        }
        const { getByTestId } = render(<Resume rowData={rowDataMock} />);
        expect(getByTestId("ResumeTestId")).toBeInTheDocument();
    })
    test("Resume component loaded without data", () => {
        const rowDataMock = {
            requestCode: null,
            status: null,
            sender: { label: null},
            recipient: { label: null },
            requestor: { label: null},
            program: { label: null},
            submitionDate: null
        }
        const { getByTestId } = render(<Resume rowData={rowDataMock} />);
        expect(getByTestId("ResumeTestId")).toBeInTheDocument();
    })
})