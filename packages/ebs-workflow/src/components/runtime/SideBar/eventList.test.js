import { render, fireEvent } from "@testing-library/react";
import EventList from "./eventlist";
import { useQuery } from '@apollo/client';
import { useServiceContext } from "custom-components/context/service-context";

jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Event List component", () => {
    const rowDataMock = { id: 1, statusCode:"Test", byOccurrence:true };
    beforeEach(() => {
        useServiceContext.mockReturnValue({
            rowData: rowDataMock,
        });
    });

    test("Error in useQuery getting the event List data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<EventList />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });


    test('Renders event list correctly', () => {
        const mockData = { findEventList: { content: [{ id: '1', completed: true, node:{name:"test node name A"} }, { id: '2', completed: false,node:{name:"test node name B"} }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getAllByTestId, getByText } = render(<EventList />);
        const list = getAllByTestId('EventListTestId');
        expect(getByText('test node name A')).toBeInTheDocument();
        expect(getByText('test node name B')).toBeInTheDocument();
    });
    test('show message loading data for...', () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<EventList />);
        expect(getByText(/Loading data for/i)).toBeInTheDocument();
    });

});
