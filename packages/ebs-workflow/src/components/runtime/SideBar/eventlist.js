import { Core, Styles, Icons } from "@ebs/styleguide";

const { CheckCircle, Refresh } = Icons
const { List, ListItem, ListItemAvatar, ListItemText, Avatar, IconButton, Tooltip, Typography } = Core;
import { FIND_EVENT_LIST, FIND_WORKFLOW_LIST } from "utils/apollo/gql/workflow";
import { FormattedMessage } from "react-intl";
import { useQuery } from "@apollo/client";
import { useServiceContext } from "custom-components/context/service-context";
const { useTheme } = Styles;

const EventList = () => {
    const { rowData } = useServiceContext();
    const theme = useTheme();
    const classes = {
        root: {
            width: '100%',
            maxWidth: 360
        },
    };
    const { loading, error, data } = useQuery(FIND_EVENT_LIST, {
        variables: {
            filters: [{ col: "recordId", mod: "EQ", val: Number(rowData.id) }],
            sort: { col: "id", mod: "ASC" }
        },
        fetchPolicy: "no-cache",
    });
    if (loading) return `Loading data for ${rowData.requestCode}`;
    if (error) return `Error! ${error}`;
    const eventList = data.findEventList.content
    return (
        <List sx={classes.root}>
            <Typography color="primary" variant={"h4"}>
                <FormattedMessage id={"none"} defaultMessage={"Workflow history"} />
            </Typography>

            {
                eventList && eventList.map((item, index) => (
                    <ListItem key={index} data-testid={"EventListTestId"}>
                        <ListItemAvatar>
                            <Avatar>
                                <CheckCircle color={item.completed ? "primary" : ""} />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText  primary={`${item.node.name}`} secondary={`Completed on: ${item.completed ? item.completed : ""}`} />
                        {/* {
                            item.completed && (
                                <Tooltip
                                    arrow
                                    placement="top"
                                    title="Reset to this step">
                                    <IconButton>
                                        <Refresh color="primary" />
                                    </IconButton>
                                </Tooltip>
                            )
                        } */}
                    </ListItem>
                ))
            }
        </List>
    );
}
export default EventList;