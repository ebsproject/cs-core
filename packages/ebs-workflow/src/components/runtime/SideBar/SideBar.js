import PropTypes from "prop-types";
import { Core } from "@ebs/styleguide";
import StepperBar from "../StepperBar";
const { Box, SwipeableDrawer, Divider, Typography, Button } = Core;
import { FormattedMessage } from "react-intl";
import { useSelector } from "react-redux";
import EventList from "./eventlist";
import Resume from "./resume";

const SideBar = ({open, rowData, setOpen }) => {
  if(!open) return null;
  const { workflowValues } = useSelector(({ wf_workflow }) => wf_workflow);
  const toggleDrawer = (open) => (event) => {
    if (event && event.type === "keydown" && (event.key === "Tab" || event.key === "Shift")) {
      return;
    }
    if(!open)
      setOpen(false);
  };
  return (
  
        <SwipeableDrawer
          anchor={"right"}
          open={open}
          onClose={toggleDrawer(false)}
          onOpen={toggleDrawer(true)}
          data-testid={"SidebarDrawerTestId"}
        >
          <Box
            sx={{ width: 640, paddingTop: 10, paddingLeft:2 }}
            role="presentation"
            onKeyDown={toggleDrawer(false)}
          >
            <Typography color="primary" className="font-ebs" variant={"h4"}>
              <FormattedMessage id={"none"} defaultMessage={"Workflow Progress"} />
            </Typography>
            <StepperBar hideButtons={true} mode="view" />
            <Divider />
            <Box>&nbsp;</Box>
            <Resume rowData= {rowData}/>
            <EventList />
            {/* <Button  className="bg-ebs-brand-default hover:bg-ebs-brand-900 border-l-8 text-white" fullWidth variant="contained">
              Reset Workflow
            </Button> */}
          </Box>
        </SwipeableDrawer>

  );
};
// Type and required properties
SideBar.propTypes = {
  rowData: PropTypes.object,
};
// Default properties

export default SideBar;
