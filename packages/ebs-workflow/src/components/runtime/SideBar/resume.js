import { Core, Icons } from "@ebs/styleguide";
const { TextField, Grid } = Core;
const Resume = ({ rowData }) => {
    return (
        <>
            <Grid container spacing={2} sx={{ flexDirection: "row" }} data-testid={"ResumeTestId"}>
                <Grid item xs={12}>
                    <TextField fullWidth id="outlined-basic1" label="Shipment Name" variant="standard" value={rowData.requestCode || ""} />
                </Grid>
                <Grid item xs={6}>
                    <TextField fullWidth id="outlined-basic1" label="Status" variant="standard" value={rowData.status || ""} />
                </Grid>
                <Grid item xs={6}>
                    <TextField fullWidth id="outlined-basic2" label="Sender" variant="standard" value={rowData.sender.label || ""} />
                </Grid>
                <Grid item xs={6}>
                    <TextField fullWidth id="outlined-basic3" label="Requestor" variant="standard" value={rowData.requestor.label || ""} />
                </Grid>
                <Grid item xs={6}>
                    <TextField fullWidth id="outlined-basic4" label="Recipient" variant="standard" value={rowData.recipient.label || ""} />
                </Grid>
                <Grid item xs={6}>
                    <TextField fullWidth id="outlined-basic5" label="Program" variant="standard" value={rowData.program.label || ""} />
                </Grid>
                <Grid item xs={6}>
                    <TextField fullWidth id="outlined-basic6" label="Submitted On" variant="standard" value={rowData.submitionDate || ""} />
                </Grid>
            </Grid>
        </>
    )
}
export default Resume;