import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import SideBar from './SideBar';
import { useSelector } from 'react-redux';
import { Core } from "@ebs/styleguide";
import { IntlProvider } from 'react-intl';

jest.mock('react-redux', () => ({
  useSelector: jest.fn(),
}));

jest.mock('../StepperBar', () => () => <div data-testid="stepper-bar">StepperBar Component</div>);
jest.mock('./eventlist', () => () => <div data-testid="event-list">EventList Component</div>);
jest.mock('./resume', () => () => <div data-testid="resume">Resume Component</div>);

const { SwipeableDrawer } = Core;

describe('SideBar Component', () => {
  const mockSetOpen = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    useSelector.mockReturnValue({
      workflowValues: {
        phases: [{ id: 1, name: 'Phase 1' }, { id: 2, name: 'Phase 2' }],
      },
    });
  });

  const renderComponent = (open = true, rowData = {}) => {
    return render(
      <IntlProvider locale="en">
        <SideBar open={open} rowData={rowData} setOpen={mockSetOpen} />
      </IntlProvider>
    );
  };

  test('renders SideBar when open is true', () => {
    renderComponent(true);

    expect(screen.queryByTestId('SidebarDrawerTestId')).toBeInTheDocument();
    expect(screen.getByText('Workflow Progress')).toBeInTheDocument();
    expect(screen.getByTestId('stepper-bar')).toBeInTheDocument();
    expect(screen.getByTestId('resume')).toBeInTheDocument();
    expect(screen.getByTestId('event-list')).toBeInTheDocument();
  });

  test('does not render SideBar when open is false', () => {
    renderComponent(false);
    expect(screen.queryByTestId('SidebarDrawerTestId')).not.toBeInTheDocument();
  });

  test('calls setOpen with false when toggleDrawer is triggered on close', () => {
    renderComponent(true);
    const swipeableDrawer = screen.queryByTestId('SidebarDrawerTestId');
    fireEvent.keyDown(swipeableDrawer, { key: 'Tab', code: 'Tab', charCode: 9 });
    expect(mockSetOpen).not.toHaveBeenCalled();
    fireEvent.keyDown(swipeableDrawer, { key: 'Escape', code: 'Escape', charCode: 27 });
    expect(mockSetOpen).toHaveBeenCalledWith(false);
  });

  test('renders StepperBar with workflowValues phases', () => {
    renderComponent(true);
    expect(screen.getByTestId('stepper-bar')).toBeInTheDocument();
    expect(useSelector).toHaveBeenCalledWith(expect.any(Function));
  });

  test('renders Resume with provided rowData', () => {
    const rowData = { id: 1, name: 'Sample Row' };
    renderComponent(true, rowData);
    expect(screen.getByTestId('resume')).toBeInTheDocument();
  });
});
