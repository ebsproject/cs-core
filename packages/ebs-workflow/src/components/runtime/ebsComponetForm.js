import React from "react";
import { Core } from "@ebs/styleguide";
const { Dialog, DialogContent, Button } = Core;
import { EbsForm } from "@ebs/components";
import { FIND_CONTACT_LIST, FIND_PROGRAM_LIST } from "utils/apollo/gql/request";
import { useQuery } from "@apollo/client";
import { getCoreSystemContext } from "@ebs/layout";



const EbsComponent = () => {
    const { graphqlUri } = getCoreSystemContext();
    const { loading, data, error } = useQuery(FIND_CONTACT_LIST, {
        variables: {
            page: { number: 1, size: 300 },
            filters:[{col:"category.id",val:"2",mod:"EQ"}]
        },
        fetchPolicy: "no-cache",
    });

    const { loading: loa, data: dat, error: err } = useQuery(FIND_PROGRAM_LIST, {
        fetchPolicy: "no-cache",
    });


    if (loading || loa) return "Loading...";
    if (error || err) return `Error vvvv! ${error.message}`;

let dtaPr=[]
data.findContactList.content.forEach(item =>{

})




    const options = data.findContactList.content.filter(item => item.category.name === "Person").map(item => {
        let obj = { label: item.person.familyName + ", " + item.person.givenName, value: item.id }
        return obj;
    });
    const optionsP = dat.findProgramList.content.map(item => {
        let obj = { label: item.name, value: item.id }
        return obj;
    });

    const instOption = data.findContactList.content.filter(item => item.category.name === "Institution").map(item => {
        let obj = { label: item.institution.commonName, value: item.id }
        return obj;
    })

    const onSubmit = (data) => {

        alert("form submited " + JSON.stringify(data))
    }

    const definition = ({ getValues, setValue, reset }) => {
        return {
            name: "Shipment Form",
            components: [
                {
                    sizes: [12, 6, 4, 3, 2],
                    component: "Radio",
                    name: "type",
                    label: "Select Type",
                    row: false,
                    options: [
                        { label: "Person", value: "Person", },
                        { label: "Institution", value: "Institution" },
                    ],
                    defaultValue: "Person"
                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "TextField",
                    name: "test",
                    inputProps: {
                        "data-testid": "name",
                        label: "Test Name",
                        variant: "outlined",
                    },
                    rules: {
                        required: "A Tets Name is required",
                    },

                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "TextField",
                    name: "requestCode",
                    inputProps: {
                        "data-testid": "name",
                        label: "Request Name",
                        variant: "outlined",
                    },
                    rules: {
                        required: "A Facility Name is required",
                    },
                    defaultRules: {
                        applyRules: true,
                        uri: graphqlUri,
                        entity: "Program",
                        apiContent: [{ accessor: "id" }, { accessor: "name" }, { accessor: "code" }],
                        parentControl: "program",
                        field: "name",
                        columnFilter: "id"
                    },
                },

                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "Select",
                    name: "contact",
                    options: options,
                    inputProps: {
                        "data-testid": "chooseCountryId",
                        label: "Select a Contact",
                        variant: 'outlined'
                    },
                    helper: { title: "Choose Site", placement: "bottom" },
                    rules: {
                        required: "It's required",
                    },
                    defaultRules: {
                        applyRules: true,
                        uri: graphqlUri,
                        entity: "Contact",
                        apiContent: [{ accessor: "id" }, { accessor: "person.givenName" }, { accessor: "person.familyName" }],
                        parentControl: "type",
                        field: "person",
                        columnFilter: "category.name",
                        label: ["familyName", "givenName"],
                    },
                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "Select",
                    name: "program",
                    options: optionsP,
                    inputProps: {
                        "data-testid": "chooseCountryId",
                        label: "Select a Program",
                        variant: 'outlined'
                    },
                    helper: { title: "Choose Site", placement: "bottom" },
                    rules: {
                        required: "It's required",
                    }
                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "TextField",
                    name: "email",
                    inputProps: {
                        "data-testid": "name",
                        label: "Email Name",
                        variant: "outlined",
                    },
                    rules: {
                        required: "A Facility Name is required",
                    },
                    defaultRules: {
                        applyRules: true,
                        uri: graphqlUri,
                        entity: "Contact",
                        apiContent: [{ accessor: "email" }],
                        parentControl: "contact",
                        field: "email",
                        columnFilter: "id"
                    },
                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "divider",
                    label:"Additional Information",
                    style:{height:"1px", backgroundColor:"#000"}
        
                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "Select",
                    name: "addresses",
                    options: [],
                    inputProps: {
                        "data-testid": "chooseCountryId",
                        label: "Choose An Address",
                        variant: "outlined"
                    },
                    helper: { title: "Choose Site", placement: "bottom" },
                    rules: {
                        required: "It's required",
                    },
                    defaultRules: {
                        applyRules: true,
                        uri: graphqlUri,
                        entity: "Contact",
                        apiContent: [{ accessor: "addresses.streetAddress" }, { accessor: "addresses.id" }, { accessor: "addresses.zipCode" }, { accessor: "addresses.country.name" }],
                        parentControl: "contact",
                        field: "addresses",
                        label: ["streetAddress", "zipCode", "country.name"],
                        columnFilter: "id"
                    },
                },
                {
                    sizes: [12, 12, 6, 4, 2].reverse(),
                    component: "Select",
                    name: "institution",
                    options: instOption,
                    inputProps: {
                        "data-testid": "chooseCountryId",
                        label: "Choose An Institution",
                        variant: "outlined"
                    },
                    helper: { title: "Choose Site", placement: "bottom" },
                    rules: {
                        required: "It's required",
                    },
                    defaultRules: {
                        applyRules: false,
                        uri: "",
                        entity: "",
                        apiContent: [{ accessor: "id" }],
                        parentControl: "",
                        field: "",
                        label: [],
                        columnFilter: "id"
                    },
                }
            ],
        };
    };
    return (
        <>
            <Dialog
                open={true}
                onClose={null}
                maxWidth={"lg"}
                fullWidth

            >
                <DialogContent>
                    <EbsForm
                        definition={definition}
                        onSubmit={onSubmit}
                        buttonReset={true}

                    >
                        <Button color={"secondary"} variant={"contained"} type="submit">
                            Submit

                        </Button>
                    </EbsForm>
                </DialogContent>


            </Dialog>

        </>
    )
}
export default EbsComponent;