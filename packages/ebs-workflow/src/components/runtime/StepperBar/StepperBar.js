import { Core } from "@ebs/styleguide";
import PropTypes from "prop-types";
const { Box, Stepper, Step, StepLabel, Button, Typography } = Core;
import { setWorkflowPhase } from "store/modules/Workflow";
import { useDispatch } from "react-redux";
import { useEffect, useContext, useState, Fragment } from "react";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { getCurrentStage } from "./functions";
import { useExecuteProcess } from "hooks/useExecuteProcess";
import { getNodesToExecute } from "components/service/functions";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { showMessage } from "store/modules/message";
import { client } from "utils/apollo";
import { FIND_EVENT_LIST } from "utils/apollo/gql/workflow";
import { checkPermissions } from "validations";
import { useServiceContext } from "custom-components/context/service-context";

const StepperBar = ({ buttonRef, refreshAction, hideButtons }) => {
  const { setLoading, mode, rowData, blockButtons, setBlockButtons } = useServiceContext();
  const dispatch = useDispatch();
  const [activeStep, setActiveStep] = useState(0);
  const { setRecord } = useContext(WorkflowContext);
  const { executeProcess } = useExecuteProcess();
  const [currentStage, setCurrentStage] = useState(null);
  const { workflowValues } = useSelector(({ wf_workflow }) => wf_workflow);
  const { userPermissions } = useSelector(({ wf_user }) => wf_user);
  const navigate = useNavigate();
  const [rowActionsNode, setRowActionsNode] = useState(null);


  useEffect(() => {
    let mounted = true;
    if (rowData && mounted) {
      const fetchData = async () => {
        const result = await getCurrentStage(rowData);
        setCurrentStage(result);
        setActiveStep(result.stage.phase.sequence - 1);
      };
      if (mode === "edit" || mode === "view") fetchData();
      else {
        setCurrentStage(null);
        setActiveStep(0);
      }
    }
    return () => (mounted = false);
  }, [rowData, mode]);
  useEffect(() => {
    const fetchData = async () => {
      const { data } = await client.query({
        query: FIND_EVENT_LIST,
        variables: {
          filters: [
            { col: "workflowInstance.id", mod: "EQ", val: Number(rowData?.wfInstance || 0) },
            { col: "recordId", mod: "EQ", val: Number(rowData?.id || 0) },
          ],
        },
        fetchPolicy: "no-cache",
      });
      let wf = data.findEventList.content.filter((item) => item.completed === null);
      setRowActionsNode(
        wf.length > 0
          ? wf[0].stage.nodes.filter((item) => item.nodeType.name === "rowAction")
                                                                                  .filter(item => item.process.code !== "PP")
                                                                                  .filter(item => item.process.code !== "EMAIL")
                                                                                  .filter(item => item.process.code !== "NOTIFICATION")
                                                                                  .filter(item => item.process.code !== "EXTERNAL")
                                                                                  .filter(item => item.process.code !== "POPUP")
          : []
      );
    };
    fetchData();
}, [rowData]);

  const refreshData = () => {
    setLoading(false);
    refreshAction();
  };
  const exitToGrid = () =>{
    let _rowData ={...rowData}
    let _workflowValues = {...workflowValues};
    if (mode === "create")
      setRecord(null);
    else
      setRecord(_rowData);
    navigate( "/wf/request",{
      state: {
        name: _workflowValues.name,
        id: _workflowValues.id,
        description: _workflowValues.description,
      },
    });
  }
  const handleNext = async () => {
    if (mode.includes("edit") && !rowData )  return;
    if (mode.includes("edit")) {
      setLoading(true);
      setBlockButtons(true);
      let createdObj = { id: rowData.id, workflowInstanceId: rowData.wfInstance };
      let nodeId = currentStage.node.id;
      const currentNode = currentStage.stage.nodes.filter((node) => node.id === nodeId);
      let _node = currentNode[0];
      if(rowActionsNode && rowActionsNode.length > 0){
        _node = rowActionsNode[0];
        const access = checkPermissions(userPermissions, _node.securityDefinition);
        if(!access){
          setLoading(false);
          dispatch(
            showMessage({
              variant: "error",
              anchorOrigin: {vertical: "top",horizontal: "right" },
              message: `Error on security validation!!: Your user does not have the privileges to execute this action.`,
            })
          );
          return;
        }
         await moveNode(_node, createdObj);
         return;
      }
      if ( _node.nodeType.name === "toolbarAction" || (_node.process.code === "FORM" && buttonRef.current === null)) {
        setLoading(false);
        dispatch(
          showMessage({
            variant: "error",
            anchorOrigin: {vertical: "top",horizontal: "right" },
            message: _node.process.code === "FORM" ? `Please fill the required information. Error validations on: ${_node.name}` : `Please complete the workflow actions at row level. Error Validations on: ${_node.name}`,
          })
        );
        setBlockButtons(false);
        return;
      }
      if (buttonRef.current !== null && _node.process.code === "FORM") {
        buttonRef.current.click();
        setLoading(false);
      } else {
        await moveNode(_node, createdObj);
      }
    }
    if (mode.includes("create") && buttonRef.current !== null) {
      buttonRef.current.click();
      setLoading(false);
      return;
    }
  };
  const moveNode = async (_node, createdObj) => {
    let validationResult;
    const edgesNodes = getNodesToExecute(_node, [currentStage.stage]);
    if (edgesNodes.length === 0) {
      let node = _node;
      await executeProcess({ node, createdObj });
      setTimeout(() => {
        refreshData();
      }, 1100);
     return;
   }
    if (rowActionsNode && rowActionsNode.length === 1) {
      let node = _node;
      await executeProcess({ node, createdObj });
      setTimeout(() => {
        refreshData();
      }, 1100);
      return;
    }

    for (let i = 0; i < edgesNodes.length; i++) {
      let node = edgesNodes[i];
    //  if (node.nodeType.name === "process") {
        validationResult = await executeProcess({ node, createdObj });
        if (validationResult) console.error(validationResult);
    //  }
    }
    if (!validationResult) {
      setTimeout(() => {
        refreshData();
      }, 1100);
    }else{
      setLoading(false);
    }
  }

  // const handleBack = () => {
  //   setActiveStep((prevActiveStep) => prevActiveStep - 1);
  //   dispatch(setWorkflowPhase(phases[activeStep - 1]));
  // };

  return (
    <Box sx={{ width: "100%" }} data-testid={"StepperBarTestId"}>
      <Stepper activeStep={activeStep}>
        {workflowValues.phases
      //   .sort((a, b) => {
      // return a.sequence - b.sequence;
      //       })
            .map((label, index) => {
          const stepProps = {};
          const labelProps = {};
          labelProps.optional = (
            <Typography variant="caption" color={index === activeStep ? "primary" : ""}>
              {label.description}
            </Typography>
          );
          return (
            <Step key={label.name} {...stepProps}>
              <StepLabel {...labelProps}>
                <Typography variant={"h5"} color={index === activeStep ? "primary" : ""}>
                  {label.name}
                </Typography>
              </StepLabel>
            </Step>
          );
        })}
      </Stepper>
{
  mode !== "view" &&(
    <Fragment>
    <Box sx={{ display: "flex", flexDirection: "row"}}>
      {
        !hideButtons && (
          <>
            <Box sx={{ flex: "1 1 auto" }} />
            <Button
              color="inherit"
              disabled={blockButtons}
              onClick={exitToGrid}
              sx={{ mr: 1 }}>
              Back
            </Button>
            {
            <Button
            disabled={blockButtons}
            style={{ display: `${(rowActionsNode && rowActionsNode.length > 0 && !checkPermissions(userPermissions,rowActionsNode[0].securityDefinition)) || activeStep === (workflowValues.phases.length - 1) || (rowActionsNode && rowActionsNode.length > 1) ? "none" : "block"}` }}
            onClick={handleNext}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            {activeStep === workflowValues.phases.length ? "Finish" : mode === "create" ? "Create" : rowActionsNode && rowActionsNode.length > 0 ? rowActionsNode[0].name : "Next"}
          </Button>
            }
          </>
        )
      } 
    </Box>
  </Fragment>
  )

}
    
    </Box>
  );
};
// Type and required properties
StepperBar.propTypes = {
  phases: PropTypes.array,
  buttonRef: PropTypes.object,
  mode: PropTypes.string,
};

export default StepperBar;
