import { client } from "utils/apollo";
import { FIND_EVENT_LIST, FIND_NODE_LIST_BY_ID } from "utils/apollo/gql/workflow";

export async function getCurrentStage(rowData) {
  try {
    const { data } = await client.query({
      query: FIND_EVENT_LIST,
      variables: {
        filters: [
          { col: "workflowInstance.id", mod: "EQ", val: Number(rowData.wfInstance) },
          { col: "recordId", mod: "EQ", val: Number(rowData.id) },
        ],
      },
      fetchPolicy: "no-cache",
    });
    const result = data.findEventList.content.filter((item) => item.completed === null);
  
    return result[0];
  } catch (error) {
    console.error(error)
    return [];
  }

}

export async function getNodeToExecute(nodeId) {
  try {
    const { data } = await client.query({
      query: FIND_NODE_LIST_BY_ID,
      variables: {
        filters: [{ col: "id", mod: "EQ", val: nodeId }],
      },
      fetchPolicy: "no-cache",
    });
    const result = data.findNodeList.content[0];
    return result || [];
  } catch (error) {
    console.error(error);
    return [];
  }
}
