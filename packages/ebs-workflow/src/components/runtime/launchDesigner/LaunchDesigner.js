import { useDispatch, useSelector } from "react-redux";
import { hideDesigner } from "store/modules/PrintOut";
import { lazy, Suspense } from "react";
import { Core, Icons } from "@ebs/styleguide";
import SplashScreen from "../SplashScreen/splashscreen";
const { DialogContent, Dialog, Toolbar, IconButton, Typography } = Core;
const { Close } = Icons;
const POReportDesigner = lazy(() => import('@ebs/po').then(module => ({ default: module.POReportDesigner })));
const LaunchDesigner = () => {
  const dispatch = useDispatch();
  const { open, template, params } = useSelector(({ wf_printout }) => wf_printout);
  const handleClose = () => {
    dispatch(hideDesigner(false));
  };
if(!open) return null;
  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        fullScreen
      >
        <Toolbar className="bg-ebs-brand-default text-white font-ebs" >
          <IconButton
            edge="start"
            color="inherit"
            onClick={handleClose}
            aria-label="close"
          >
            <Close />
          </IconButton>
          <Typography>
            {"Report Preview"}
          </Typography>
        </Toolbar>
        <DialogContent>
          <Suspense fallback={<SplashScreen />}>
          <POReportDesigner reportName={template} mode={"preview"} paramValues = {params} />
          </Suspense>
        </DialogContent>
      </Dialog>
    </>
  );
};
export default LaunchDesigner;
