import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { useDispatch, useSelector } from "react-redux";
import { hideDesigner } from "store/modules/PrintOut";
import LaunchDesigner from "./LaunchDesigner";
import { Core } from "@ebs/styleguide";

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useDispatch: jest.fn(),
  useSelector: jest.fn(),
}));

jest.mock("store/modules/PrintOut", () => ({
  hideDesigner: jest.fn(),
}));

jest.mock("@ebs/po", () => ({
  POReportDesigner: jest.fn(() => <div data-testid="POReportDesignerTestId">POReportDesigner Component</div>),
}));

describe("LaunchDesigner Component", () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useSelector.mockReturnValue({
      open: true,
      template: "Sample Report",
      params: { id: 1, name: "Sample" },
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  // test("renders the Dialog and its contents when open is true", () => {
  //   render(<LaunchDesigner />);

  //   expect(screen.getByRole("dialog")).toBeInTheDocument();
  //   expect(screen.getByTestId("POReportDesignerTestId")).toBeInTheDocument();
  //   expect(screen.getByText("Report Preview")).toBeInTheDocument();
  // });

  // test("dispatches hideDesigner action when the close button is clicked", () => {
  //   render(<LaunchDesigner />);
  //   const closeButton = screen.getByLabelText("close");
  //   fireEvent.click(closeButton);
  //   expect(mockDispatch).toHaveBeenCalledWith(hideDesigner(false));
  // });

  test("does not render the Dialog if open is false", () => {
    useSelector.mockReturnValue({
      open: false,
      template: "Sample Report",
      params: { id: 1, name: "Sample" },
    });

    render(<LaunchDesigner />);
    expect(screen.queryByRole("dialog")).not.toBeInTheDocument();
  });

  // test("renders POReportDesigner with correct props", () => {
  //   render(<LaunchDesigner />);
  //   expect(screen.getByTestId("POReportDesignerTestId")).toBeInTheDocument();
  // });
});
