/* eslint-disable react/prop-types */
import { Core } from "@ebs/styleguide";
const { Typography, Chip, FormControl, Select,MenuItem, OutlinedInput } = Core;
import { FormattedMessage } from "react-intl";
import { client } from "utils/apollo";
import { FIND_JOB_LOG_LIST } from "utils/apollo/gql/workflow";

// const SelectColumnFilter = ({
//   column: { filterValue, setFilter },
// }) => {
//   return (
//     <FormControl variant="standard">
//       <Select
//         value={filterValue || ""}
//         onChange={(e) => {
//           let filterData = e.target.value;
//           setFilter(filterData);
//         }}
//         input={<OutlinedInput style={{ width: 160, height: 30 }} />}
//         displayEmpty
//       >
//         <MenuItem value="">All</MenuItem>
//         <MenuItem value={true}>Yes</MenuItem>
//         <MenuItem value={false}>No</MenuItem>
//       </Select>
//     </FormControl>
//   );
// };

export function columnsDefinitionV2(columns) {
  let newColumns = [
    {
      Header: "id",
      accessor: "id",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
    {
      Header: "workflowInstance",
      accessor: "wfInstance",
      hidden: true,
      filter: false,
      disableGlobalFilter: true,
    },
  ];
  columns
    .sort((a, b) => {
      return a.fieldAttributes.sort - b.fieldAttributes.sort;
    })
    .forEach((column) => {
      if (!column.fieldAttributes.showInGrid) return;
      let columnComponent;
      if (column.cfType.name === "select" && column.attributes) {
        columnComponent = {
          Header: (
            <Typography variant="h5" color="primary">
              <FormattedMessage
                id="none"
                defaultMessage={column.fieldAttributes.inputProps.label}
              />
            </Typography>
          ),
          csvHeader: column.apiAttributesName,
          accessor: `${column.apiAttributesName}.label`,
          width: column.fieldAttributes.inputProps.label.length * 17,
          hidden: !column.fieldAttributes.showInGrid,
        };
      } else {
        // if (column.cfType.name === "checkbox") {
        //   columnComponent = {
        //     Header: (
        //       <Typography variant="h5" color="primary">
        //         <FormattedMessage
        //           id="none"
        //           defaultMessage={column.fieldAttributes.inputProps.label}
        //         />
        //       </Typography>
        //     ),
        //     csvHeader: column.apiAttributesName,
        //     accessor: column.apiAttributesName,
        //     width: column.fieldAttributes.inputProps.label.length * 19,
        //     hidden: !column.fieldAttributes.showInGrid,
        //     sort: column.fieldAttributes.sort,
        //     //Filter: SelectColumnFilter,
        //     Cell: ({ value }) => {
        //       return <Typography variant="body1">{value ? "YES": "NO"}</Typography>;
        //     },
        //   }
        // } else 
        columnComponent = {
          Header: (
            <Typography variant="h5" color="primary">
              <FormattedMessage
                id="none"
                defaultMessage={column.fieldAttributes.inputProps.label}
              />
            </Typography>
          ),
          csvHeader: column.apiAttributesName,
          accessor: column.apiAttributesName,
          width: column.fieldAttributes.inputProps.label.length * 19,
          hidden: !column.fieldAttributes.showInGrid,
          sort: column.fieldAttributes.sort,
        };
      }
      newColumns = [...newColumns, columnComponent];
    });
  return newColumns.sort((a, b) => { return a.sort - b.sort });
}
export function extractCustomFieldsV2(workflow) {
  let allCF = [];
  for (let a = 0; a < workflow.phases.length; a++) {
    const phase = workflow.phases[a];
    for (let b = 0; b < phase.stages.length; b++) {
      const stage = phase.stages[b];
      for (let c = 0; c < stage.nodes.length; c++) {
        const node = stage.nodes[c];
        if (node.nodeType.name === "form" || (node['process'] && node.process.code === "FORM")) {
          allCF = [...allCF, ...node.nodeCFs];
        }
      }
    }
  }
let newCF =[]
  for (let i = 0; i < allCF.length; i++) {
    let item = {...allCF[i]};
    if (item.fieldAttributes.showInGrid) {
      if (!item.attributes) {
        item.apiAttributesName = item.name;
      } else {
        item.apiAttributesName = item.name;
      }
      newCF =[...newCF,item]
    }
  }
  return newCF;
}
export function extractCustomFields(workflow) {
  let allCF = [];

  for (let a = 0; a < workflow.phases.length; a++) {
    const phase = workflow.phases[a];
    for (let b = 0; b < phase.stages.length; b++) {
      const stage = phase.stages[b];
      for (let c = 0; c < stage.nodes.length; c++) {
        const node = stage.nodes[c];
        if (node.nodeType.name === "form" || (node['process'] && node.process.code === "FORM")) {
          allCF = [...allCF, ...node.nodeCFs];
        }
      }
    }
  }

  try {
    const Cfs = workflow.phases
      .filter((item) => item.sequence === 1)[0]
      .stages.filter((item) => item.sequence === 1)[0]
      .nodes.filter((item) => item.nodeType.name === "form")[0].nodeCFs;

    return Cfs;
  } catch (error) {
    return [];
  }
}
export function wfVariables(workflow) {
  let newUrl = workflow.product.domain.domaininstances[0].sgContext
  let serviceProviderOptions = [];
  const allAttributes = extractCustomFields(workflow);
  const allAttributesV2 = extractCustomFieldsV2(workflow);
  const uri = new URL( "graphql", newUrl).toString();
  let entityName = workflow.product.entityReference.entity;
  let entity = `${entityName.charAt(0).toUpperCase()}${entityName.slice(1)}`;
  let customFields = allAttributes.filter((item) => !item.attributes);
  let serviceProvider = workflow?.workflowContacts;
  if (serviceProvider?.length > 0) {
    let options = serviceProvider?.filter(item=> item.contact.institution.unitType.name === "Service Unit").map((item) => {
      return { label: item?.contact?.institution?.legalName, value: item?.contact?.id };
    });
    serviceProviderOptions = options;
  }
  // const customFields = allAttributes//.filter(item => !item.attributes); // TODO Review how to put all as a custom fields
  let obj = {
    uri: uri,
    entity: entity,
    customFields: customFields,
    allAttributes: allAttributes,
    allAttributesV2: allAttributesV2,
    serviceProvider: serviceProviderOptions,
  };
  return obj;
}

export function getDependOnNodes(phases, wfId) {
  let targetNodes = [];
  phases.forEach((ph) => {
    ph.stages.forEach((st) => {
      st.nodes.forEach((nd) => {
        nd.dependOn.edges.forEach((ed) => {
          targetNodes = [...targetNodes, { id: nd.id, target: ed, sort: nd.sequence, source:nd.designRef, workflowId:wfId }];
        });
      });
    });
  });
  return targetNodes;
}
export function getAllPrintPreviewNodes(workflow) {
  let printPreviewNodes = [];
  workflow.phases.forEach((ph) => {
    ph.stages.forEach((st) => {
      st.nodes.forEach((nd) => {
        if(nd["process"] && nd.process.code === "PP")
            printPreviewNodes = [...printPreviewNodes,nd];
      
      });
    });
  });
  return printPreviewNodes;
}
export function getAllStatusNodes(workflow) {
  let statusNodes = [];
  workflow.phases.forEach((ph) => {
    ph.stages.forEach((st) => {
      st.nodes.forEach((nd) => {
        if(nd["process"] && nd.process.code === "CHANGE")
          statusNodes = [...statusNodes,nd];
      
      });
    });
  });
  return statusNodes;
}
export function getAllWorkflowNodes(workflow) {
  let nodes = [];
  workflow.phases.forEach((ph) => {
    ph.stages.forEach((st) => {
      st.nodes.forEach((nd) => {
        nodes = [...nodes, nd];
      });
    });
  });
  return nodes;
}

export async function  getJobLogsList(recordId){
try {
  const {data} = await client.query({
    query: FIND_JOB_LOG_LIST,
    variables:{
      filters:[{col:"recordId", val:recordId, mod:"EQ"}, {col:"jobWorkflow.id", val:"2", mod:"EQ"} ]
    },
    fetchPolicy:'no-cache'
  });
  return data.findJobLogList.content;
} catch (error) {
  return []
}
}
export const setCounterMessage = (response, setCounter) =>{
  if(response.length === 0 ) return;
  const item = response[0];
  const message = item['message'];
  if(!message) return;
  const messageObject = message['message'];
  if(!messageObject) return;
  const messagingArray = messageObject["direct_messaging"];
  if(!messagingArray) return;
  if(!Array.isArray(messagingArray)) return;
  let counterUnread = 0
  for(let i = 0; i < messagingArray.length; i++) {
      const messagingObj = messagingArray[i];
      if(messagingObj.status === 'unread')
           counterUnread++;
  }
  if(counterUnread > 0)
      setCounter(counterUnread)
  }