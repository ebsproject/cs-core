import { Core } from "@ebs/styleguide";
const { Breadcrumbs, Link, Typography } = Core;
import { useSelector } from "react-redux";
import { useContext, useEffect, memo } from "react";
import { WorkflowContext } from "components/designer/context/workflow-context";

const GridBreadcrumb = ({ record, setRecord, setReload}) => {
    const { workflowValues } = useSelector(({ wf_workflow }) => wf_workflow)
  //  const { record, setRecord, setReload } = useContext(WorkflowContext);

    const handleClick = () => {
        if (record) {
            setRecord(null);
            setReload(true);
        }
    }
    if(!workflowValues) return "Loading..."
    return (
        <>
            <Breadcrumbs aria-label="breadcrumb">
                <Link color="inherit"  >
                    SM
                </Link>
                <Link color="inherit" onClick={handleClick} sx={{cursor:"pointer"}}>
                    {workflowValues.name}
                </Link>
                {
                    record && <Typography color="textPrimary">{record.requestCode}</Typography>
                }

            </Breadcrumbs>
        </>
    )
}
export default memo(GridBreadcrumb);