import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import { FormattedMessage } from "react-intl";
export const columns = [
  {
    Header: "id",
    accessor: "id",
    hidden: true,
    filter: false,
    disableGlobalFilter: true,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Code" />
      </Typography>
    ),
    csvHeader: "Code",
    accessor: "requestCode",
    width: 500,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Description" />
      </Typography>
    ),
    csvHeader: "Name",
    accessor: "description",
    width: 500,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Submition Date" />
      </Typography>
    ),
    csvHeader: "Submitiondate",
    accessor: "submitionDate",
    width: 500,
  },
];
