import React, { useContext, useEffect, useState } from "react";
import { EbsGrid } from "@ebs/components";
import { useLocation, useSearchParams } from "react-router-dom";
import { Core } from "@ebs/styleguide";
const { Typography, LinearProgress } = Core;
import { FormattedMessage } from "react-intl";
import { setWorkflowValues, setWorkflowVariables, setDependOnNodes, setPrintPreviewNodes, setStatusNodes, setWorkflowNodes } from "store/modules/Workflow";
import { useDispatch } from "react-redux";
import { FIND_WORKFLOW_LIST, NEW_MESSAGE_SUBSCRIPTION } from "utils/apollo/gql/workflow";
import { wfVariables, getDependOnNodes, columnsDefinitionV2, getAllPrintPreviewNodes, getAllStatusNodes, getAllWorkflowNodes } from "./functions";
import { getCoreSystemContext, userContext } from "@ebs/layout";
import ToolbarAction from "../ToolbarAction/ToolbarAction";
import RowAction from "../RowAction/RowAction";
import { setUserPermissions } from "store/modules/User";
import { WorkflowContext } from "components/designer/context/workflow-context";
import GridBreadcrumb from "./gridbreadcrumb";
import { client } from "utils/apollo";

import { CreateApolloClient } from "./ws-client";
import { useServiceContext } from "custom-components/context/service-context";

const MainBrowser = () => {
  const { setRefetch } = useServiceContext();
  const [searchParams] = useSearchParams();
  const { record, setRecord, setReload } = useContext(WorkflowContext);
  const [filters, setFilters] = useState(null);
  const { userProfile } = useContext(userContext);
  const dispatch = useDispatch();
  const { graphqlUri } = getCoreSystemContext();
  const [workflowId, setWorkflowId] = useState(null);
  const location = useLocation();
  const [data, setData] = useState(null);
  const clientWS = CreateApolloClient(graphqlUri);

  useEffect(()=>{
    clientWS.subscribe({ query: NEW_MESSAGE_SUBSCRIPTION }).subscribe({
      next: (data) => {
        setRefetch((prev) => prev + 1);
      },
    });
  },[])

  useEffect(() => {
    setWorkflowId(null);
    setData(null);
    let id = searchParams.get("workflowID") || location?.state?.id || 0;
    setWorkflowId(id);
    return () => setRecord(null);
  }, [searchParams]);

  useEffect(() => {
    setFilters(null);
    if (workflowId) {
      if (!record) {
        setFilters([
          { col: "workflowId", mod: "EQ", val: workflowId },
          { col: "userId", mod: "EQ", val: userProfile?.dbId || 0 },
        ]);

      } else {
        setFilters([
          { col: "workflowId", mod: "EQ", val: workflowId },
          { col: "userId", mod: "EQ", val: userProfile?.dbId || 0 },
          { col: "id", mod: "EQ", val: record?.id },
        ]);
      }
    }

  }, [record, workflowId]);

  useEffect(() => {
    if (workflowId) {
      const fetchWorkflowData = async () => {
        const { data: _data } = await client.query({
          query: FIND_WORKFLOW_LIST,
          variables: {
            filters: [{ col: "id", mod: "EQ", val: Number(workflowId) }],
          },
          fetchPolicy: "no-cache",
        })
        setData(_data);
      }
      fetchWorkflowData();
    }
  }, [workflowId]);

  if (!data || !workflowId || !filters) return <LinearProgress />;
  if (data.findWorkflowList.content.length === 0) return `Error! Workflow not found, try again.`

  let workflowPhase = [...data.findWorkflowList.content[0].phases];
  let dependOnNodes = getDependOnNodes(workflowPhase, workflowId);
  let _workflow = { ...data.findWorkflowList.content[0] };
  _workflow = {..._workflow, phases : _workflow.phases.sort((a,b)=> {return Number(a.sequence) - Number(b.sequence)})}
  let printPreviewNodes = getAllPrintPreviewNodes(_workflow);
  let wfVars = wfVariables(_workflow);
  let userPermissions = {
    roles: userProfile?.permissions?.memberOf?.roles,
    programs: userProfile?.permissions?.memberOf?.programs,
    userId: userProfile?.dbId,
  };
  const statusNodes = getAllStatusNodes(_workflow);
  let allWorkflowNodes = getAllWorkflowNodes(_workflow);
  dispatch(setWorkflowNodes(allWorkflowNodes));
  dispatch(setStatusNodes(statusNodes));
  dispatch(setPrintPreviewNodes(printPreviewNodes))
  dispatch(setDependOnNodes(dependOnNodes));
  dispatch(setWorkflowVariables(wfVars));
  dispatch(setWorkflowValues(_workflow));
  dispatch(setUserPermissions(userPermissions));

  let _allAttributesV2 = [...wfVars.allAttributesV2];
  let columnsV2 = columnsDefinitionV2(_allAttributesV2);
  return (
    <>
      <EbsGrid
        title={
          <>
            <Typography variant="h3" className="text-ebs text-bold">
              <FormattedMessage id="none" defaultMessage={_workflow.navigationName} />
            </Typography>
            <GridBreadcrumb record={record} setRecord={setRecord} setReload={setReload} />
          </>
        }
        id={`${wfVars.entity}Id`}
        disabledViewDownloadCSV={true}
        toolbar={true}
        toolbaractions={ToolbarAction}
        columns={columnsV2}
        rowactions={RowAction}
        csvfilename={`${wfVars.entity}List`}
        height="65vh"
        raWidth={250}
        select="multi"
        entity={"WorkflowCustomField"} //{wfVars.entity}
        callstandard="graphql"
        uri={
          window.location.hostname === "localhost"
            ? graphqlUri
            : wfVars.entity === "Service"
              ? wfVars.uri.replace("smapi", "csapi")
              : wfVars.uri
        }
        defaultFilters={filters}
        defaultSort={{ col: "id", mod: "DES" }}
        auditColumns
      />
    </>
  );
};
export default MainBrowser;
