import { Core, Icons } from "@ebs/styleguide";
import { getJobLogsList, setCounterMessage } from "./functions";
const { Badge } = Core;
import { useEffect, useState } from "react";
import { useServiceContext } from "custom-components/context/service-context";

const MessageBadge = ({ children, rowData, counter, setCounter }) => {
    const { refetch } = useServiceContext();
    useEffect(() => {
        const getJobLogMessages = async () => {
            const response = await getJobLogsList(rowData.id || 0);
            setCounterMessage(response, setCounter);
        }
        getJobLogMessages();
    }, [refetch])
    return (
        <Badge badgeContent={counter} color={"error"} size="small">
            {children}
        </Badge>
    )
}
export default MessageBadge