import { render } from "@testing-library/react";
import SplashScreen from "./splashscreen";

describe("SplashScreen Component", () => {
    test('show splashscreen', () => {
        const { getByTestId } = render(<SplashScreen />);
        expect(getByTestId("SplashScreenTestId")).toBeInTheDocument();
    });

});
