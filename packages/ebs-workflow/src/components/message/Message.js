import { Core } from "@ebs/styleguide";
import { memo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { hideMessage } from "store/modules/message";
const {  Snackbar, Typography,Alert } = Core;

function FuseMessage(props) {
  const dispatch = useDispatch();
  const state = useSelector((store) => store.wf_message.state);
  const options = useSelector((store) => store.wf_message.options);

  const handleClose = () =>{
    setTimeout(()=>{
      dispatch(hideMessage())
    },2000)
  }
  return (
    <Snackbar
      {...options}
      open={state}
      onClose={handleClose}
      ContentProps={{
        variant: "body2",
        headlineMapping: {
          body1: "div",
          body2: "div",
        },
      }}
    >
      <Alert
        onClose={handleClose}
        severity={options.variant}
        variant="filled"
        sx={{ width: '100%' }}
      >
        <div className="flex items-center">
            <Typography className="mx-8">{options.message}</Typography>
          </div>
      </Alert>
    </Snackbar>
  );
}

export default memo(FuseMessage)
