import React from 'react';
import { Styles, Core, Icons, Lab } from "@ebs/styleguide";
const { Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} = Core;
const {
  Alert
} = Lab;

import PropTypes from 'prop-types'


const AlertDeletion = React.forwardRef((props, ref) => {


  const { title, open, onChange, onClose, message, ...rest } = props

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div>
      <Dialog
        open={open}
        onClose={onClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{`Are you sure delete the record  ${title}?`}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description" >
            <Alert severity="warning"> {message}</Alert>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => { onChange(false) }}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            Cancel
          </Button>
          <Button onClick={() => { onChange(true) }}
            className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
          >
            Delete
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});


AlertDeletion.propTypes = {
  title: PropTypes.string,
  message: PropTypes.string,
  onChange: PropTypes.func,
  onClose: PropTypes.func,
  open: PropTypes.bool
}


export default AlertDeletion;