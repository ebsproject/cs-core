import React, { useContext } from "react";
import { Core, Icons } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { showMessage } from "store/modules/message";
import { WorkflowContext } from "../context/workflow-context";
import { CREATE_STATUS_TYPE, MODIFY_STATUS_TYPE } from "utils/apollo/gql/workflow";
import { client } from "utils/apollo";
import { getContext } from "@ebs/layout";
const { Typography, Button, TextField, Grid } = Core;
const { CheckCircle, Cancel } = Icons;
//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const StatusForm = () => {
  const { status } = useContext(WorkflowContext);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { workflowId } = useSelector(({ wf_workflow }) => wf_workflow);
  const tenant = getContext();
  const MutationStatusType = (dataInfo, action) =>
    new Promise((resolve, reject) => {
      try {
        client
          .mutate({
            mutation: action == "add" ? CREATE_STATUS_TYPE : MODIFY_STATUS_TYPE,
            variables: {
              statusTypeInput: dataInfo,
            },
          })
          .then((result) => {
            resolve(result);
          })
          .catch((e) => {
            reject(e);
          });
      } catch (error) {
        reject(error);
      }
    });

  const onSubmit = (data) => {
    const dataInfo = {
      id: status.action === "edit" ? data.id : 0,
      name: data.name,
      description: data.description,
      help: data.help,
      workflowId: workflowId,
      tenantId: tenant.tenantId,
    };
    MutationStatusType(dataInfo, status.action).then(() => {
      dispatch(
        showMessage({
          message: `Status was ${status.action == "edit" ? "edited" : "created"} successfully `,
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
    navigate("/wf/designer/status");
  };
  const { control, handleSubmit, setValue } = useForm({
    defaultValues:
      status.action === "edit"
        ? {
            id: status.data.id,
            name: status.data.name,
            description: status.data.description,
            help: status.data.help,
          }
        : {
            name: "",
            description: "",
            help: "",
          },
  });
  const handleCancel = (e) => {
    e.preventDefault();
    navigate("/wf/designer/status");
  };
  return (
    /**
     * @prop data-testid: Id to use inside contact.test.js file.
     */
    <div data-testid={"StatusFormTestId"}>
      <form
        onSubmit={handleSubmit(onSubmit)}
        style={{
          paddingTop: "14px",
          position: "relative",
          paddingLeft: "10px",
          paddingRight: "20px",
        }}
      >
        <Typography variant="h5" color="primary" className="flex-grow font-ebs col-span-2">
          <FormattedMessage id="none" defaultMessage="Status" />
        </Typography>
        <Grid container justifyContent="flex-end">
          <Button
            data-testid="cancel"
            onClick={handleCancel}
            startIcon={<Cancel />}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            data-testid="save"
            type="submit"
            startIcon={<CheckCircle />}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Save"} />
          </Button>
        </Grid>
        <div>
          <div className="grid grid-cols-2 gap-5  ">
            <Controller
              name="name"
              control={control}
              render={({ field }) => (
                <TextField
                  InputLabelProps={{ shrink: true }}
                  {...field}
                  required
                  label={<FormattedMessage id="none" defaultMessage="Name" />}
                  onChange={(e) => {
                    setValue("name", e.target.value);
                  }}
                />
              )}
            />
            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <TextField
                  InputLabelProps={{ shrink: true }}
                  {...field}
                  required
                  label={<FormattedMessage id="none" defaultMessage="description" />}
                  onChange={(e) => {
                    setValue("description", e.target.value);
                  }}
                />
              )}
            />
            <Controller
              name="help"
              control={control}
              render={({ field }) => (
                <TextField
                  InputLabelProps={{ shrink: true }}
                  {...field}
                  required
                  label={<FormattedMessage id="none" defaultMessage="help" />}
                  onChange={(e) => {
                    setValue("help", e.target.value);
                  }}
                />
              )}
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default StatusForm;
