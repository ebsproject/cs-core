import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Icons } from "@ebs/styleguide";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Typography,
  Tooltip,
} = Core;
import { FormattedMessage } from "react-intl";
import { useDispatch } from "react-redux";
import { DELETE_STATUS_TYPE } from "utils/apollo/gql/workflow";
import { showMessage } from "store/modules/message";
import { client } from "utils/apollo";
const { Delete } = Icons;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteStatus = React.forwardRef(({ rowSelected, refresh }, ref) => {
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = () => {
    try {
      client
        .mutate({
          mutation: DELETE_STATUS_TYPE,
          variables: { id: rowSelected.id },
        })
        .then(() => {
          dispatch(
            showMessage({
              message: "Deleted Status",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    } catch (error) {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
    handleClose();
    refresh();
  };

  return (
    /* 
     @prop data-testid: Id to use inside deletecustomerbutton.test.js file.
     */
    <div ref={ref}>
      <Tooltip
        arrow
        title={
          <Typography className="font-ebs text-xl">
            <FormattedMessage id="none" defaultMessage="Delete Status" />
          </Typography>
        }
      >
        <IconButton onClick={handleClickOpen} aria-label="Delete Status" color="primary">
          <Delete />
        </IconButton>
      </Tooltip>
      <Dialog fullWidth keepMounted maxWidth="sm" open={open} onClose={handleClose}>
        <DialogTitle>
          <FormattedMessage id="none" defaultMessage="Delete Status" />
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <FormattedMessage id="none" defaultMessage="Are you sure to delete following Status?" />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <Typography variant="button" color="secondary">
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Typography>
          </Button>
          <Button onClick={handleDelete}>
            <Typography variant="button">
              <FormattedMessage id="none" defaultMessage="Confirm" />
            </Typography>
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
DeleteStatus.propTypes = {
  refresh: PropTypes.func,
  rowSelected: PropTypes.object,
};
// Default properties

export default DeleteStatus;
