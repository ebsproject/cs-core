import { useEffect, useState } from "react";
import { Core, Icons } from "@ebs/styleguide";
const { Typography, Chip, Grid } = Core;
const { Lock } = Icons;

const SecurityRules = ({ rules }) => {
    const [rulesName, setRulesName] = useState([]);
    const [name, setName] = useState([])

    useEffect(() => {
        setRulesName([])
        if (rules) {

            rules.forEach(item => {
                if (item.rules && item.rules.length > 0) {
                    item.rules.forEach(i => {
                        setRulesName((name) => [...name, i.name])
                    })
                }
            });
        }
    }, [rules])

    useEffect(() => {
        if (rulesName.length > 0) {
            let newArray = Array.from(new Set(rulesName));
            setName(newArray);
        } else {
            setName([])
        }

    }, [rulesName])

    return (
        <div style={{paddingTop:10, fontFamily:"sans-serif"}}>
            <Typography variant="body1">
                {"Security rules applied for the role selection:"}
            </Typography>
            <Grid container spacing={1} flexDirection={"row"} style={{paddingTop:10}}>

               {
                    name.map(name => (
                        <Grid key={name} item xs={3} lg={2} md={4}>
                            <Chip
                                size={"small"}
                                key={name}
                                icon={<Lock />}
                                label={name}
                                variant="outlined"
                                color="secondary"
                                />
                                
                        </Grid>
                    ))
                }
            </Grid>
        </div>
    )
}
export default SecurityRules;