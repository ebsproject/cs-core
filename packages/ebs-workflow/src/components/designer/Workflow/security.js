import React from 'react';
import {  Core } from "@ebs/styleguide";
import ProgramSelect from "./helpers/programSelect";
import UserSelect from './helpers/userSelect';
import RoleSelect from './helpers/roleSelect';

const {
  Grid,
  Box
} = Core;





const styles = {
  root: { marginLeft: 20, width: "100%", padding: 10 },
  box: { borderRadius: 5, border: "1px dotted black" },
  legend: { fontWeight: "bold", marginLeft: 20, width: '100px' }
};


const Security = (props) => {


  return (
    <Box component="fieldset" >
      <legend >Security</legend>
      <Grid  container item spacing={3}>
        <Grid item xs={6}>
          <ProgramSelect />
        </Grid>
        <Grid item xs={6}>
          <UserSelect />
        </Grid>

        <Grid item xs={12}>
          <RoleSelect />
        </Grid>
      </Grid>
    </Box>

  )
}


export default Security;