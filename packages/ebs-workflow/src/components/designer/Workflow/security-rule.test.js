import SecurityRules from "./security-rule";
import { render, act, screen, cleanup } from "@testing-library/react";

describe("Security Rules component", () => {
    afterEach(cleanup);
    const rulesMock = [{
        rules: [{
            id: "1",
            name: "@CreatedByMeTest1"
        }, {
            id: "2",
            name: "@CreatedByMeTest2"
        },]
    },
    {
        rules: [{
            id: "3",
            name: "@SubmittedToMeTest3"
        }, {
            id: "4",
            name: "@CreatedByMeTest4"
        },]
    }
    ];

    test("Security Rules loaded in DOM", () => {
        render(<SecurityRules rules={rulesMock} />)
        expect(screen.getByText(/Security rules applied for the role selection:/i)).toBeInTheDocument();
    })

    test("Security Rules loaded with rules in array ", () => {

        act(() => {
            render(<SecurityRules rules={rulesMock} />)
        })
        expect(screen.getByText(/@CreatedByMeTest1/i)).toBeInTheDocument();
        expect(screen.getByText(/@SubmittedToMeTest3/i)).toBeInTheDocument();
    })
    test("Security Rules loaded with no rules in array ", () => {
        act(() => {
            render(<SecurityRules rules={[]} />)
        })
        expect(screen.queryByText(/@CreatedByMeTest1/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/@SubmittedToMeTest3/i)).not.toBeInTheDocument();
    })
    test("Security Rules loaded with nullable rules ", () => {
        act(() => {
            render(<SecurityRules rules={null} />)
        })
        expect(screen.queryByText(/@CreatedByMeTest1/i)).not.toBeInTheDocument();
        expect(screen.queryByText(/@SubmittedToMeTest3/i)).not.toBeInTheDocument();
    })
})