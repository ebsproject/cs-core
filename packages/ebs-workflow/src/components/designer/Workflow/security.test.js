import Security from "./security";
import { render } from "@testing-library/react";

jest.mock('./helpers/programSelect', () => () => <div>Programs Dropdown</div>);
jest.mock('./helpers/userSelect', () => () => <div>Users Dropdown</div>);
jest.mock('./helpers/roleSelect', () => () => <div>Roles Dropdown</div>);

describe("Security component", ()=>{

    const renderComponent = () => render (<Security />)

    test("Security loaded", ()=>{
        const {getByText} = renderComponent();
        expect(getByText("Security")).toBeInTheDocument();
        expect(getByText("Programs Dropdown")).toBeInTheDocument();
        expect(getByText("Users Dropdown")).toBeInTheDocument();
        expect(getByText("Roles Dropdown")).toBeInTheDocument();

    })
})