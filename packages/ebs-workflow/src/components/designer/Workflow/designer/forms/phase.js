import React, { useState, useEffect, useContext } from 'react'
import { useFormContext } from "react-hook-form";
import { Styles, Core, Icons } from "@ebs/styleguide";
import CustomTextField from "../../helpers/texfield";
import ViewTypeSelect from '../../helpers/viewTypeSelect';
import { useNodes } from '@xyflow/react';
import { NodeContext } from 'components/designer/context/node-context';
import { isEmpty } from '../../helpers/functions';
import IconSelect from '../../helpers/iconSelect';
import { WorkflowContext } from 'components/designer/context/workflow-context';
const {
  styled
} = Styles

const styles = {
  root: { marginTop: 10, marginLeft: 10, marginBottom: 10, width: "100%" },
  paper: { borderRadius: 10, border: "1px dotted #000", padding: 50, },
  container: { display: "grid", gridGap: 3 },
  item: { width: "100%" },
  box: { borderRadius: 5, border: "1px dotted black" },
  legend: { fontWeight: "bold", marginLeft: 20 }

};


const {
  Grid,

} = Core;

const PhaseForm = (props) => {

  const [phases, setPhases] = useState([]);
  const [connector, setConnector] = React.useState('');
 
  const { reset, formState: { errors } } = useFormContext();
  //states of reactflow
  // const nodes = useNodes();
  const { node, setNode } = useContext(NodeContext);
  const { nodes } = useContext(WorkflowContext);
  useEffect(() => {

  }, [node])

  useEffect(() => {
    let nodeSelected = nodes.find(nd =>  nd.id == node.id);
    reset({
      name: nodeSelected.data.name,
      description: isEmpty(nodeSelected.data.description) ? '' : nodeSelected.data.description,
      sequence: isEmpty(nodeSelected.data.sequence) ? 0 : nodeSelected.data.sequence,
      nodeViewType: isEmpty(nodeSelected.data.nodeViewType) ? { id: 1, name: 'N/A' } : nodeSelected.data.nodeViewType,
      icon: nodeSelected.data.icon,
    })
  }, [reset]);


  // useEffect(() => {
  //   const filterNodes = nodes.filter(nd => nd.type == "phase" && nd.id != node.id)
  //   setPhases(filterNodes)
  // }, []);



  return (
    <Grid container spacing={3} data-testid={"PhaseFormTestId"} >
      <Grid container item xs={12} spacing={2} >
        <Grid item xs={12} >
          <CustomTextField name="name" label={"Name"} rules={{ required: "The Name is required" }} otherProps={{ fullWidth: true }} />
        </Grid>
        <Grid item xs={12} >
          <CustomTextField name="description" label={"Description"} rules={{ required: "The Description is required" }} otherProps={{ fullWidth: true }} />
        </Grid>
        <Grid item xs={3}>
          <CustomTextField name="sequence" label={"Sort No"} rules={{ required: "The Sort Number is required" }}
            otherProps={{ fullWidth: false, type: "number" }} />
        </Grid>
        <Grid item xs={3}>
          <ViewTypeSelect />
        </Grid>
        <Grid item xs={3}>
          <IconSelect />
        </Grid>
      </Grid>
    </Grid>
  )
}

//export default styled(styles)(PhaseForm)
export default PhaseForm