import React, { useState, useEffect, useContext } from 'react'
import { useFormContext, Controller } from "react-hook-form";
import { Styles, Core, Icons, EbsTabsLayout } from "@ebs/styleguide";
import CustomTextField from "../../helpers/texfield";
import ViewTypeSelect from '../../helpers/viewTypeSelect';
import ProcessSelect from '../../helpers/processSelect';
import NodeTypeSelect from '../../helpers/NodeTypeSelect';
import JsonEditor from '../../helpers/jsonDefine';
import { FormattedMessage } from "react-intl";
import Security from '../../security';
import AttributeForm from '../../helpers/attributeForm'
import { NodeContext } from 'components/designer/context/node-context';
import { useNodes } from '@xyflow/react';
import { isEmpty } from '../../helpers/functions';
import IconSelect from '../../helpers/iconSelect';
import { WorkflowContext } from 'components/designer/context/workflow-context';



const {
  Grid,
} = Core;


const NodeForm = (props) => {

  //const nodes = useNodes();
  const { node, setNode } = useContext(NodeContext);
  const { nodes } = useContext(WorkflowContext);

  const { reset, control, register, watch, formState: { errors } } = useFormContext();


  const [approvalProcess, setApprovalProcess] = useState(false)

  const watchProcess = watch("process");
  const watchNodeType = watch("nodeType");

  useEffect(() => {

  }, [node])


  useEffect(() => {
    if (nodes){
      let nodeSelected = nodes.find(nd => nd.id == node.id);
      reset({
        name: nodeSelected.data.name,
        description: isEmpty(nodeSelected.data.description) ? '' : nodeSelected.data.description,
        sequence: isEmpty(nodeSelected.data.sequence) ? 0 : nodeSelected.data.sequence,
        nodeViewType: isEmpty(nodeSelected.data.nodeViewType) ? { id: 1, name: 'N/A' } : nodeSelected.data.nodeViewType,
        nodeType: isEmpty(nodeSelected.data.nodeType) ? { id: 4, name: 'process' } : nodeSelected.data.nodeType,
        process: isEmpty(nodeSelected.data.process) ? { id: 1, name: 'Generate Form' } : nodeSelected.data.process,
        define: isEmpty(nodeSelected.data.define) ? {} : nodeSelected.data.define,
        roles: isEmpty(nodeSelected.data.roles) ? null : nodeSelected.data.roles,
        users: isEmpty(nodeSelected.data.users) ? null : nodeSelected.data.users,
        programs: isEmpty(nodeSelected.data.programs) ? null : nodeSelected.data.programs,
        icon: nodeSelected.data.icon,
      })
    }

  }, [reset, nodes]);

  const handleApprovalChange = (event) => {
    setApprovalProcess(event.target.checked);
  };



  let tabs = [
    {
      label: <FormattedMessage id="securityTab" defaultMessage="Security" />,
      component: <Security />,
    }
  ];

  if (watchNodeType?.id != 5){
    tabs.push({
      label: <FormattedMessage id="defineTab" defaultMessage="Define" />,
      component: <JsonEditor />,
    },
    {
      label: <FormattedMessage id="fieldTab" defaultMessage="Form Fields" />,
      component: <AttributeForm />,
    })
  }

  if (watchProcess?.id != 1)
    tabs.pop();

  return (
    <Grid container spacing={3}  >
      <Grid container item xs={12} spacing={2} >
        <Grid item xs={3}>
          <ViewTypeSelect />
        </Grid>
        <Grid item xs={3}>
          <NodeTypeSelect />
        </Grid>
        <Grid item xs={3}>
          {
            watchNodeType?.id != 5 && (
              <ProcessSelect />
            )
          }
        </Grid>
        <Grid item xs={3}>
          <CustomTextField name="sequence" label={"Sort No"} rules={{ required: "The Sort Number is required" }}
            otherProps={{ fullWidth: false, type: "number" }} />
        </Grid>
        <Grid item xs={12} >
          <CustomTextField name="name" label={"Name"} rules={{ required: "The Name is required" }} otherProps={{ fullWidth: true }} />
        </Grid>
        <Grid item xs={12} >
          <CustomTextField name="description" label={"Description"} rules={{ required: "The Description is required" }} otherProps={{ fullWidth: true }} />
        </Grid>
        <Grid item xs={3}>
          <IconSelect />
        </Grid>
        <Grid item xs={12} >

          <EbsTabsLayout
            orientation="horizontal"
            data-testid={"nodeFormTabsTestId"}
            tabs={tabs}
          />
        </Grid>

      </Grid>


    </Grid>
  )
}

//export default styled(styles)(NodeForm)
export default NodeForm