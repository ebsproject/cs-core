import StageForm from "./stage";
import { render, screen } from "@testing-library/react";
import { NodeContext } from 'components/designer/context/node-context';
import { WorkflowContext } from "components/designer/context/workflow-context";
import { useFormContext, Controller } from "react-hook-form";
import { isEmpty } from "../../helpers/functions";

jest.mock('../../helpers/functions', () =>({
    isEmpty:jest.fn()
}))

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('../../helpers/viewTypeSelect', () => () => <div>ViewTypeSelect</div>);
jest.mock('../../helpers/iconSelect', () => () => <div>Icon Dropdown</div>);
jest.mock('../../helpers/texfield', () => () => <div>CustomTextField Component</div>);
describe("Stage Form component",()=>{
    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            reset: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });

    const renderComponent = (props) => {
        return render(
            <WorkflowContext.Provider
                value={{ nodes: props.nodesMock }}
            >
                <NodeContext.Provider
                    value={{ node: props.nodeMock }}
                >
                    <StageForm />
                </NodeContext.Provider>
            </WorkflowContext.Provider>
        );
    };

    test("Stage form loaded with proper data", () =>{
        const nodesMock =[
            {id:1, data:{name:"Node Name A", description:"Description A", sequence:1,nodeViewType:{id:1,name:"Some Name A"}, icon:"Some Icon A"},}
            ,{id:2, data:{name:"Node Name B", description:"Description B", sequence:1,nodeViewType:{id:1,name:"Some Name B"}, icon:"Some Icon B"},}

        ];
        const nodeMock = nodesMock[0];
        renderComponent({nodesMock, nodeMock});
        expect(screen.getByTestId("StageFormTestId")).toBeInTheDocument();
    });

    test("Stage Form loaded with missing data", () => {
        isEmpty.mockReturnValue(true);
        const nodesMock =[
            {id:1, data:{name:"Node Name A", description:"Description A", sequence:1,nodeViewType:{id:1,name:"Some Name A"}, icon:"Some Icon A"},}
            ,{id:2, data:{name:"Node Name B", description:"Description B", sequence:1,nodeViewType:{id:1,name:"Some Name B"}, icon:"Some Icon B"},}

        ];
        const nodeMock = nodesMock[0];
        renderComponent({nodesMock, nodeMock});
    });
} )