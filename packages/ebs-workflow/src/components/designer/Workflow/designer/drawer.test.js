import FormDrawer from "./drawer";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { NodeContext } from 'components/designer/context/node-context';
jest.mock('./forms/phase', () => () => <div>Phase Form</div>);
jest.mock('./forms/stage', () => () => <div>Stage Form</div>);
jest.mock('./forms/node', () => () => <div>Process Form</div>);

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
  }));
describe("Form Drawer component", () => {

    const renderComponent = (props) => {
        return render(
            <NodeContext.Provider
                value={{
                    node: props.nodeMock
                }}
            >
                <FormDrawer openDrawer={props.openDrawer} />
            </NodeContext.Provider>
        );
    };

    beforeEach(() => {
        jest.clearAllMocks();
    });

    test("Form Drawer loaded with open modal and Phase Mode", () => {
        const props = {
            nodeMock: {
                id: 1,
                type: "phase",
                data: { name: "Node Name Phase", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props);
        expect(screen.getByText(/Phase Form/i)).toBeInTheDocument();

    })
    test("Form Drawer loaded with open modal and Stage Mode", () => {
        const props = {
            nodeMock: {
                id: 1,
                type: "stage",
                data: { name: "Node Name Stage", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props);
        expect(screen.getByText(/Stage Form/i)).toBeInTheDocument()

    })
    test("Form Drawer loaded with open modal and Process Mode", () => {
        const props = {
            nodeMock: {
                id: 1,
                type: "process",
                data: { name: "Node Name Process", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props);
        expect(screen.getByText(/Process Form/i)).toBeInTheDocument()

    })
    test("Form Drawer closed when click in button cancel", async () => {
        const mockClose = jest.fn();
        const props = {
            nodeMock: {
                id: 1,
                type: "process",
                data: { name: "Node Name Process", onPropertyClick: mockClose }
            }, openDrawer: true
        };
        renderComponent(props);
        screen.debug();
        const button = screen.getByText(/cancel/i);
        fireEvent.click(button);

        await waitFor(()=>{
            expect(mockClose).toHaveBeenCalled();
        })

    })
    test("Form Drawer closed when click in button close", async () => {
        const mockClose = jest.fn();
        const props = {
            nodeMock: {
                id: 1,
                type: "process",
                data: { name: "Node Name Process", onPropertyClick: mockClose }
            }, openDrawer: true
        };
        renderComponent(props);
        screen.debug();
        const button = screen.getByText(/close/i);
        fireEvent.click(button);

        await waitFor(()=>{
            expect(mockClose).toHaveBeenCalled();
        })
    })
})