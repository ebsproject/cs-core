import React, { useContext, useEffect } from 'react';
import { FormattedMessage } from "react-intl";
import { Styles, Core, Icons } from "@ebs/styleguide";

import { NodeContext } from 'components/designer/context/node-context';

const {
  ImageAspectRatioOutlined
} = Icons
const {
  Fab,
  Toolbar,
  ButtonGroup,
  Button,
  Typography,
} = Core;


const styles = {
  fab: {
    margin: 0,
    top: 20,
    right: 20,
    bottom: 20,
    left: "auto",
    zIndex: 20,

  },
  fabIcon: {
    fill: "white"
  },
  title: { fontWeight: "bold" },
  button: { margin: 10 }
};



const ToolbarActions = (props) => {

  const onDragStart = (event, nodeType) => {
    event.dataTransfer.setData('application/reactflow', nodeType);
    event.dataTransfer.effectAllowed = 'move';
  };
  const { node, setNode } = useContext(NodeContext);



  const enabled = (nodeType) => {
    return node && node.type == nodeType ? false : true;
  }

  const textNodeSelected = (nodeType) => {
    return node && node.type == nodeType ? `${node.data.name.substring(0, 15)} >>> ` : ''
  }
  return (
    <ButtonGroup
      orientation="vertical"
      aria-label="vertical contained button group"
      variant="contained"
      data-testid={"ToolbarActionsTestId"}
    >
      <Button onDragStart={(event) => onDragStart(event, 'phase')} draggable data-testid={"PhaseButtonTestId"}>
        <Typography  >
          <FormattedMessage
            id="none"
            defaultMessage="New Phase"
          />
        </Typography>

      </Button>
      <Button disabled={enabled("phase")} onDragStart={(event) => onDragStart(event, 'stage')} draggable data-testid={"StageButtonTestId"}>
        <Typography >
          <FormattedMessage
            id="none"
            defaultMessage={`${textNodeSelected("phase")}New Stage`}
          />
        </Typography>
      </Button>

      <Button disabled={enabled("stage")} onDragStart={(event) => onDragStart(event, 'process')} draggable data-testid={"ProcessButtonTestId"}>
        <Typography  >
          <FormattedMessage
            id="none"
            defaultMessage={`${textNodeSelected("stage")} Process`}
          />
        </Typography>
      </Button>

    </ButtonGroup>

  )
}

export default ToolbarActions;