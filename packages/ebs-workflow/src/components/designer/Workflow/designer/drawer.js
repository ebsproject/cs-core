import React, { useContext, useState, useEffect, Fragment } from 'react'
import { FormattedMessage } from "react-intl";
import { EbsDialog, Styles, Core, Icons } from "@ebs/styleguide";
import { NodeContext } from 'components/designer/context/node-context';



const { Button, Dialog, DialogContent } = Core;


import PhaseForm from './forms/phase'
import StageForm from './forms/stage'
import NodeForm from './forms/node'


const styles = {
  root: { marginTop: 20, width: "100%" },
  drawer: {
    position: "relative",
    marginLeft: "auto",
    width: 300,

    "& .MuiBackdrop-root": {
      display: "none"
    },
    "& .MuiDrawer-paper": {
      width: 200,
      position: "absolute",
      transition: "none !important"
    }
  }
};


const {
  CheckCircle,
  Cancel
} = Icons;

const FormDrawer = (props) => {

  const { openDrawer } = props;
  const { node, setNode } = useContext(NodeContext);

  const actions = (nd) => {
    return (
      <>

        <Button
          data-testid={"CancelButtonTestId"}
          onClick={(e) => nd.data.onPropertyClick(e, nd.id, 'close')}
          startIcon={<Cancel />}
          className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
        >
          <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
        </Button>

        <Button
          type="submit"
          data-testid={"SaveButtonTestId"}
          startIcon={<CheckCircle />}
          form="designerForm"
          className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
        >
          <FormattedMessage id={"none"} defaultMessage={"Save"} />
        </Button>
      </>

    )
  }

  return (
    <Fragment>
      {node &&
        <EbsDialog
          open={openDrawer}
          handleClose={(e) => node.data.onPropertyClick(e, node.id, 'close')}
          title={`Properties of ${node.type}'s ${node.data.name}`}
          actions={actions(node)} >
          {node.type == "phase" && <PhaseForm />}
          {node.type == "stage" && <StageForm />}
          {node.type == "process" && <NodeForm />}
        </EbsDialog>

      }
    </Fragment>


  )
}



export default FormDrawer;