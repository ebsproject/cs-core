import React, { useEffect, useState, useRef, useCallback, useContext, } from 'react';
import { FormattedMessage } from "react-intl";
import { Styles, Core, Icons, EbsDialog } from "@ebs/styleguide";
import { initialNodes, initialEdges } from "./initialization"
import ToolbarActions from "./toolbar"
import PhaseNode from "./nodes/phase"
import StageNode from "./nodes/stage"
import FormNode from "./nodes/form"
import ProcessNode from "./nodes/process"
import Node from "./nodes/node"
import FormDrawer from "./drawer"
import { FormProvider, useForm } from "react-hook-form";
import { v4 as uuidv4 } from 'uuid';
//CONTEXTS
import { NodeContext } from 'components/designer/context/node-context';
import { WorkflowContext } from "components/designer/context/workflow-context";
import { transform } from "node-json-transform";
import { designPhase, designStage, designNode, edgesDefinition } from '../helpers/jsonActions'
import {
  findWorkflowById, onMaintainPhase,
  onMaintainStage, onMaintainNode, onCreateRelationStageNode,
  onDeleteWorkflowObject
} from '../helpers/formFunction';
import seedrandom from 'seedrandom';
import { useDispatch } from "react-redux";
import { showMessage } from "store/modules/message";
import AlertDeletion from 'components/designer/alert-deletion/alert';
import { ReactFlow,applyEdgeChanges,applyNodeChanges,Background, MiniMap,Controls,Panel,ReactFlowProvider,addEdge } from '@xyflow/react';
import '@xyflow/react/dist/style.css';
import '@xyflow/react/dist/base.css';
import './nodes/index.css'
import './main.css';

const {
  Typography, Dialog, DialogContent, Button, DialogActions
} = Core;


//custom nodes list
const nodeTypes = {
  phase: PhaseNode,
  stage: StageNode,
  form: FormNode,
  process: ProcessNode,
  node: Node
}




const DesignerV2 = (props) => {


  const dispatch = useDispatch();
  const {open,handleClose, setOpenDesigner, buttonSave} = props;
  const formMethods = useForm();
  const [openDrawer, setOpenDrawer] = useState(false);
  const [showError, setShowError] = useState({ show: false, message: '' })
  const [node, setNode] = useState({});
  const [nodeDelete, setNodeDelete] = useState({});
  const [openAlertDelete, setOpenAlertDelete] = useState(false);
  const { workflow, setWorkflow, nodes, setNodes, edges, setEdges, reload, setReload } = useContext(WorkflowContext);
  const [nodeSelected, setNodeSelected] = useState(null);
  const reactFlowWrapper = useRef(null);
  const [reactFlowInstance, setReactFlowInstance] = useState(null);
  const [refresh, setRefresh] = useState(false);
  
  const onPropertyClick = useCallback((event, nodeId, action) => {
    setNodeSelected({
      id: nodeId,
      action: action
    })

  }, []);

  const onError = (errors, e) => console.log(errors, e)
  const onSubmit = async (data, event) => {
    //event.preventDefault();
    setNode(
      {
        ...node,
        data: {
          ...node.data,
          ...data
        }
      });

    setNodeSelected({
      id: node.id,
      action: 'cancel'
    })

  };

  useEffect(() => {
    if (workflow.data || refresh) {
      const fetchWorkflowData = async () => {
        const arrayNodes = await refreshDesign();
        const _nodes =  arrayNodes.map((item) => {
            if ("edge" in item == false)
              item = {...item, edge: []};
            return {
              ...item,
              data: {
                ...item.data,
                onPropertyClick,
                onSubmit,
                workflowId: workflow.data.id
              }
            }
          })
        setNodes(_nodes);
        const aryEdges = refreshEdges(_nodes);
        if (Array.isArray(aryEdges)) {
          setEdges(aryEdges)
        }
      }
      fetchWorkflowData();
    }
  }, [workflow.data, refresh])

  const edgeNewObject = (edges) => {
    const aryEdgeObjects = [];
    edges.forEach(edge => {
      edge.sources.forEach(source => {
        const edgeObject = {
          id: uuidv4(),
          source: source,
          target: String(edge.id),
          type: 'smoothstep',
          style: { stroke: '#0f9640', strokeWidth: 2 },
        }
        aryEdgeObjects.push(edgeObject)
      });
    });
    return aryEdgeObjects;

  }

  const refreshEdges = () => {

    //TODO: Improve It
    //phases
    const aryPhases = workflow.data.phases.filter(f => "edges" in f.dependOn);
    const aryEdges = Array.from(transform(aryPhases, edgesDefinition));
    const aryEdgesPhases = edgeNewObject(aryEdges);

    //stages
    let aryEdgesStages = [];
    aryPhases.forEach((phase) => {
      const aryStages = phase.stages.filter(f => "edges" in f.dependOn);
      const aryEdges = Array.from(transform(aryStages, edgesDefinition));
      const aryTemp = edgeNewObject(aryEdges)
      aryEdgesStages = [...aryEdgesStages, ...aryTemp]
    });


    //nodes
    let aryEdgesNodes = [];
    aryPhases.forEach((phase) => {
      phase.stages.forEach((stage) => {
        const aryNodes = stage.nodes.filter(f => "edges" in f.dependOn);
        const aryEdges = Array.from(transform(aryNodes, edgesDefinition));
        const aryTemp = edgeNewObject(aryEdges);
        aryEdgesNodes = [...aryEdgesNodes, ...aryTemp]
      })
    })



    return [...aryEdgesPhases, ...aryEdgesStages, ...aryEdgesNodes];





  }

  const refreshDesign = async () => {
    try {
      const wf = await findWorkflowById(workflow.data.id);
      const workflowObject = wf.data.findWorkflow;

      const arrPhases = Array.from(transform(workflowObject.phases, designPhase));
      let arrStages = [];
      let arrNodesFromStage = [];

      workflowObject.phases.forEach(phase => {
        const conversion = Array.from(transform(phase.stages, designStage));
        arrStages = arrStages.concat(conversion);
        //prepare arr nodes
        const arrStagesNodes = Array.from(phase.stages);
        arrStagesNodes.forEach(stage => {
          if (stage.nodes) {
            //add parent stageid
            let stageNodes = stage.nodes.map(nd => ({ ...nd, stageId: stage.designRef }));
            const conversion = Array.from(transform(stageNodes, designNode));
            arrNodesFromStage = arrNodesFromStage.concat(conversion);
          }
        });
      })

      let arrNodes = [...arrPhases, ...arrStages, ...arrNodesFromStage];
      return arrNodes;
    } catch (error) {
      return initialNodes
    }

    // return findWorkflowById(workflow.data.id).then((wf) => {

    //   const workflowObject = wf.data.findWorkflow;

    //   const arrPhases = Array.from(transform(workflowObject.phases, designPhase));
    //   let arrStages = [];
    //   let arrNodesFromStage = [];

    //   workflowObject.phases.forEach(phase => {
    //     const conversion = Array.from(transform(phase.stages, designStage));
    //     arrStages = arrStages.concat(conversion);
    //     //prepare arr nodes
    //     const arrStagesNodes = Array.from(phase.stages);
    //     arrStagesNodes.forEach(stage => {
    //       if (stage.nodes) {
    //         //add parent stageid
    //         let stageNodes = stage.nodes.map(nd => ({ ...nd, stageId: stage.id }));
    //         const conversion = Array.from(transform(stageNodes, designNode));
    //         arrNodesFromStage = arrNodesFromStage.concat(conversion);
    //       }
    //     });
    //   })

    //   let arrNodes = [...arrPhases, ...arrStages, ...arrNodesFromStage];

    //   return arrNodes
    // }).catch(err => {
    //   return initialNodes
    // });
  }

  //enable for edition
  useEffect(() => {
    if (nodeSelected) {
      setNodes((nds) =>
        nds.map((nd) => {
          if (nd.id === nodeSelected.id) {
            nd.data = {
              ...nd.data,
              mode: nodeSelected.action,
              onPropertyClick,
              onSubmit,
              workflowId: workflow.data.id
            };
            nd = {...nd, selected : nodeSelected.action === 'close' ? false : true};
          }
          return nd
        })
      );
      setOpenDrawer(nodeSelected.action === 'edit');
    }
    return ()=> setNodeSelected(null);
  }, [nodeSelected]);

  useEffect(() => {
    setNodes((nds) =>
      nds.map((nd) => {
        if (nd.id === node.id) {
          nd = node
        }
        return nd
      })
    )
  }, [node]);

  useEffect(() => {
    if (showError.show) {
      dispatch(
        showMessage({
          message: `${showError.message}`,
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      )

      setShowError({
        show: false,
        message: ''
      })
    }

  }, [showError])

  //callbacks

  const onConnect = useCallback((params) => {

    setNodes((nds) =>
      nds.map((item) => {
        let newItem ={...item}
        if (item.id === params.target) {
          let arrEdges = [];
          //if the edges array exists and sourceId is not in array
          if ("edge" in item && !item.edge.includes(params.source)) {
            arrEdges = [...item.edge, params.source]
            newItem = {...item, edge: arrEdges}
          }

          if ("edge" in item == false) {
            arrEdges.push(params.source);
            newItem ={...item, edge : arrEdges}
          }
        }
        return newItem
      })
    );


    setEdges((eds) => addEdge(params, eds))

  }, []);

  const onDragOver = useCallback((event) => {
    event.preventDefault();
    event.dataTransfer.dropEffect = 'move';
  }, []);

  const onDrop = useCallback((event) => {
    event.preventDefault();

    const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
    const type = event.dataTransfer.getData('application/reactflow');

    // check if the dropped element is valid
    if (typeof type === 'undefined' || !type) {
      return;
    }

    const position = reactFlowInstance.screenToFlowPosition({
      x: event.clientX - reactFlowBounds.left,
      y: event.clientY - reactFlowBounds.top,
    });

    const guid = uuidv4();
    const generator = seedrandom(guid);
    const nodeId = Math.floor(generator() * 20000);

    let nodeBase = {
      type,
      position,
      size: type==='process' ? {width:50, height:30} : type === 'stage' ? {width:100, height:60} : {width:150, height:90}  ,
      edge: [],
      data: {
        onPropertyClick: onPropertyClick,
        name: `${type}-${nodeId}`,
        description: 'Fill description',
        help: 'Fill description',
        sequence: 1,
        mode: "new",
        nodeViewType: {
          id: 1
        },
        workflowId: workflow.data.id
      },
      mode: 'create',

    };



    if (type == 'phase') {
      onMaintainPhase(nodeBase).then((res) => {

        const phaseId = res.data.createPhase.id;

        let newNode = {
          id: res.designRef,
          dbId: phaseId.toString(),
          designRef:res.designRef,
          ...nodeBase
        }
        return newNode
      }).then((node) => {
        setNodes((nds) => nds.concat(node));
      })

        .catch((e) => {
          setShowError({
            show: true,
            message: e
          })
        })
    }

    if (type == "stage") {

      nodeBase.data = {
        ...nodeBase.data,
        phaseId: node.dbId
      }
      onMaintainStage(nodeBase).then((res) => {

        const stageId = res.data.createStage.id;

        let newNode = {
          id: res.designRef,
          designRef: res.designRef,
          dbId: stageId.toString(),
          ...nodeBase,
          parentId: node.id,
          extent: 'parent',
          position: { x: 10, y: 50 },

        }
        return newNode
      }).then((node) => {

        setNodes((nds) => nds.concat(node));
      }).catch((e) => {
        console.log(e)
        setShowError({
          show: true,
          message: e
        })
      })
    };

    if (type == "process") {
      nodeBase.data = {
        ...nodeBase.data,
        define: {},
        nodeType: {
          id: 1
        }
      }

      onMaintainNode(nodeBase).then((res) => {
        const nodeId = res.data.createNode.id;
        let newNode = {
          id: res.designRef,
          designRef: res.designRef,
          dbId: nodeId.toString(),
          ...nodeBase,
          parentId: node.id,
          extent: 'parent',
          position: { x: 10, y: 50 }
        };
        return newNode
      }).then((nd) => {
        let _nodes = nodes.filter(item=> item.parentId === node.id);     
        let nodesIds = _nodes.map(item => Number(item.dbId))
        nodesIds = [...nodesIds, Number(nd.dbId)]
         const stageNodeT = {
          ...nd,
          id: node.dbId,
          nodes: nodesIds //new Array(nd.id)
        }
        onCreateRelationStageNode(stageNodeT).then(r => {
          setNodes((nds) => nds.concat(nd));
        })

      }).catch((e) => {
        setShowError({
          show: true,
          message: e
        })
      })
    }

    // setNodes((nds) => nds.concat(newNode));



  }, [reactFlowInstance, node]);

  const onNodesChange = useCallback(
    (changes) => setNodes((nds) => applyNodeChanges(changes, nds)),
    []
  );
  const onEdgesChange = useCallback(
    (changes) => setEdges((eds) => applyEdgeChanges(changes, eds)),
    []
  );

  const onNodeClick = useCallback(
    (evt, node) => {
      setNode(node)
    },
    []
  );

  const onNodesDelete = useCallback(
    (deleted) => {

      setNodeDelete(deleted[0]);
      setOpenAlertDelete(true);
    },
    [nodes, edges]
  );

  const onCloseAlert = () => {
    setOpenAlertDelete(false);

  }
  const onChangeAlert = (action) => {
    if (!action)
      setRefresh(true);
    if (action) {
      //TODO: DELETE CHILDREN
      const str = nodeDelete.type;
      const entityName = str.charAt(0).toUpperCase() + str.slice(1);
      onDeleteWorkflowObject(nodeDelete.dbId, entityName == 'Process' ? 'Node' : entityName).then(res => {
        dispatch(
          showMessage({
            message: `The Object ${nodeDelete.data.name} was deleted successfully`,
            variant: "success",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        )
      }).catch(err => {
        dispatch(
          showMessage({
            message: `Error deleting ${err}`,
            variant: "error",
            anchorOrigin: {
              vertical: "top",
              horizontal: "right",
            },
          })
        )
      })
    }
    setOpenAlertDelete(false);
  }
  
  return (
    <EbsDialog open={open} handleClose={() =>setOpenDesigner(false)} fullWidth maxWidth={"xl"}>
      <DialogActions>
        <Button onClick={() =>setOpenDesigner(false)}>
          Cancel
        </Button>
        <Button onClick={() =>  buttonSave.current.click()}>
          Save
        </Button>
        <Button onClick={handleClose}>
          Save & Close
        </Button>
      </DialogActions>
      <DialogContent>
        <div style={{ marginBottom: "80px" }} >

          <legend >
            <Typography variant="h5" >
              <FormattedMessage
                id="none"
                defaultMessage="Designer"
              />
            </Typography>
          </legend>
          <ReactFlowProvider>
            <div ref={reactFlowWrapper} style={{ width: '83vw', height: '100vh', border: '1px solid gray', paddingBottom: 10 }}>
              <ReactFlow nodes={nodes} edges={edges}
                onEdgesChange={onEdgesChange}
                onNodesChange={onNodesChange}
                onNodeClick={onNodeClick}
                onNodesDelete={onNodesDelete}
                onInit={setReactFlowInstance}
                onConnect={onConnect}
                onDrop={onDrop}
                onDragOver={onDragOver}
                nodeTypes={nodeTypes}
                fitView
              // colorMode={"dark"}
              >
                <MiniMap />
                <Controls />
                <NodeContext.Provider value={{ node, setNode }}>
                  <Panel>
                    <ToolbarActions />
                  </Panel>
                  <Panel position='top-right'>
                    <FormProvider {...formMethods}>
                      <form id='designerForm' onSubmit={formMethods.handleSubmit(onSubmit, onError)} >

                        {openDrawer && <FormDrawer openDrawer={openDrawer} />}

                      </form>
                    </FormProvider>
                  </Panel>
                </NodeContext.Provider>
                <Background variant="dots" />
              </ReactFlow>
            </div>
          </ReactFlowProvider>
          <AlertDeletion open={openAlertDelete} onClose={onCloseAlert} onChange={onChangeAlert} title={node.data?.name} message={'if the node is a stage or phase, the children of the object will be deleted too.'} />

        </div>
       </DialogContent>
     </EbsDialog>
  )
}
export default DesignerV2;