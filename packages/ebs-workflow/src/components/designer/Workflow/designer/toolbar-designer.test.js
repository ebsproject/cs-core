import ToolbarActions from "./toolbar";
import { fireEvent, render, screen } from "@testing-library/react";
import { NodeContext } from 'components/designer/context/node-context';

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Toolbar Action component", () => {

    const renderComponent = (props) => {
        return render(
            <NodeContext.Provider
                value={{
                    node: props.nodeMock
                }}
            >
                <ToolbarActions />
            </NodeContext.Provider>
        );
    };

    test("Toolbar loaded", () => {
        const props = {
            nodeMock: {
                id: 1,
                type: "Process",
                data: { name: "Node Name Process", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props)
        expect(screen.getByTestId("ToolbarActionsTestId")).toBeInTheDocument();
    })
    test("Toolbar loaded and button Phase dragged", () => {
        const dataTransferMock = {
            setData: jest.fn(),
            effectAllowed: '',
        };
        const props = {
            nodeMock: {
                id: 1,
                type: "phase",
                data: { name: "Node Name Phase", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props)
        const buttonPhase = screen.getByTestId("PhaseButtonTestId");
        fireEvent.dragStart(buttonPhase, { dataTransfer: dataTransferMock });
        expect(dataTransferMock.setData).toHaveBeenCalledWith('application/reactflow', 'phase');
        expect(dataTransferMock.effectAllowed).toBe('move');
    });
    test("Toolbar loaded and button Stage dragged", () => {
        const dataTransferMock = {
            setData: jest.fn(),
            effectAllowed: '',
        };
        const props = {
            nodeMock: {
                id: 1,
                type: "stage",
                data: { name: "Node Name Stage", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props)
        const buttonStage = screen.getByTestId("StageButtonTestId");
        fireEvent.dragStart(buttonStage, { dataTransfer: dataTransferMock });
        expect(dataTransferMock.setData).toHaveBeenCalledWith('application/reactflow', 'stage');
        expect(dataTransferMock.effectAllowed).toBe('move');
    })
    test("Toolbar loaded and button Process dragged", () => {
        const dataTransferMock = {
            setData: jest.fn(),
            effectAllowed: '',
        };
        const props = {
            nodeMock: {
                id: 1,
                type: "process",
                data: { name: "Node Name Stage", onPropertyClick: jest.fn() }
            }, openDrawer: true
        };
        renderComponent(props)
        const buttonProcess = screen.getByTestId("ProcessButtonTestId");
        fireEvent.dragStart(buttonProcess, { dataTransfer: dataTransferMock });
        expect(dataTransferMock.setData).toHaveBeenCalledWith('application/reactflow', 'process');
        expect(dataTransferMock.effectAllowed).toBe('move');
    })
})
