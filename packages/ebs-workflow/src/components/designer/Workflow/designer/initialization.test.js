import { initialEdges, initialStatus, initialNodes } from "./initialization";

describe("Check the initial values", ()=>{
    test("initial values of edges", ()=>{
        expect(initialEdges.length).toBe(0)
    });
    test("initial values of status", ()=>{
        expect(initialStatus.length).toBe(0)
    });
    test("initial values of nodes", ()=>{
        expect(initialNodes.length).toBe(0)
    });
})