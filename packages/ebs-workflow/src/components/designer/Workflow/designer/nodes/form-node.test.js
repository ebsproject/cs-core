import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Handle, Position, useNodeId } from '@xyflow/react';
import FormNode from './form';

jest.mock('@xyflow/react', () => ({
  ...jest.requireActual('@xyflow/react'),
  Handle: jest.fn(() => <div data-testid="HandleTestId" />),
  useNodeId: jest.fn(),
}));

describe('Process Node component', () => {
  const mockUseNodeId = 'test-node-id';
  const mockOnPropertyClick = jest.fn();
  const dataMock = {
    name: 'Test Node',
    onPropertyClick: mockOnPropertyClick,
    dependOn: { size: { width: 100, height: 50 } },
  };

  beforeEach(() => {
    jest.clearAllMocks();
    useNodeId.mockReturnValue(mockUseNodeId);
  });
  const renderComponent = () => render(<FormNode data={dataMock} isConnectable={true} />)

  test('renders with default size and updates when data is provided', () => {
    renderComponent();
    const nodeContainer = screen.getByTestId("MainContainerFormTestId");
    expect(nodeContainer).toBeInTheDocument();
    expect(screen.getByTestId('SettingsButtonTestId')).toBeInTheDocument();
    expect(screen.getByTestId('SettingsIconTestId')).toBeInTheDocument();
  });

  test('calls data.onPropertyClick with nodeId and "edit" on button click', () => {
    renderComponent();
    const button = screen.getByTestId('SettingsButtonTestId');
    fireEvent.click(button);
    expect(mockOnPropertyClick).toHaveBeenCalledWith(expect.any(Object), mockUseNodeId, 'edit');
  });

  test('renders handles with connectable prop', () => {
    renderComponent();
    const handles = screen.getAllByTestId('HandleTestId');
    expect(handles.length).toBe(2); 
  });

});
