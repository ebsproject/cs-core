import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Handle, Position, useNodeId, NodeResizer } from '@xyflow/react';
import StageNode from "./stage";

jest.mock('@xyflow/react', () => ({
  ...jest.requireActual('@xyflow/react'),
  Handle: jest.fn(() => <div data-testid="HandleTestId" />),
  NodeResizer: jest.fn(() => <div data-testid="NodeResizerTestId" />),
  useNodeId: jest.fn(),
}));

describe('Stage Node component', () => {
  const mockUseNodeId = 'test-node-id';
  const mockOnPropertyClick = jest.fn();
  const dataMock = {
    name: 'Test Node',
    onPropertyClick: mockOnPropertyClick,
    dependOn: { size: { width: 300, height: 150 } },
  };

  beforeEach(() => {
    jest.clearAllMocks();
    useNodeId.mockReturnValue(mockUseNodeId);
  });
  const renderComponent = () => render(<StageNode data={dataMock} isConnectable={true} />)

  test('renders with default size and updates when data is provided', () => {
    renderComponent();
    const nodeContainer = screen.getByTestId("ContainerTestId");
    expect(nodeContainer).toHaveStyle({ width: '300px', height: '150px' });
    expect(screen.getByTestId('NodeResizerTestId')).toBeInTheDocument();
    expect(screen.getByTestId('SettingsButtonTestId')).toBeInTheDocument();
    expect(screen.getByTestId('SettingsIconTestId')).toBeInTheDocument();
  });

  test('calls data.onPropertyClick with nodeId and "edit" on button click', () => {
    renderComponent();
    const button = screen.getByTestId('SettingsButtonTestId');
    fireEvent.click(button);
    expect(mockOnPropertyClick).toHaveBeenCalledWith(expect.any(Object), mockUseNodeId, 'edit');
  });

  test('renders handles with connectable prop', () => {
    renderComponent();
    const handles = screen.getAllByTestId('HandleTestId');
    expect(handles.length).toBe(2); 
  });

  // TODO: Review this segment in component

//   test('renders with nullable data in props', () => {
//     render(<ProcessNode data={null} isConnectable={true} />);
//     const nodeContainer = screen.getByText(dataMock.name).parentElement;
//     expect(nodeContainer).toHaveStyle({ width: '80px', height: '30px' });
//   });
});
