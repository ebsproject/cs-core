import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Handle, Position, useNodeId, NodeResizer } from '@xyflow/react';
import Node from './node';

jest.mock('@xyflow/react', () => ({
  ...jest.requireActual('@xyflow/react'),
  Handle: jest.fn(() => <div data-testid="HandleTestId" />),
}));

describe('Node component', () => {

  const mockOnPropertyClick = jest.fn();
  const dataMock = {
    name: 'Test Node',
    onPropertyClick: mockOnPropertyClick,
    dependOn: { size: { width: 100, height: 50 } },
  };

  beforeEach(() => {
    jest.clearAllMocks();
  });
 const renderComponent = () => render(<Node data={dataMock} isConnectable={true}  />);

  test('renders component in DOM', () => {
    renderComponent();
    const nodeContainer = screen.getByTestId("MainContainerTestId");
    expect(nodeContainer).toBeInTheDocument();
  });

  test('renders handles with connectable prop', () => {
    renderComponent();
    const handles = screen.getAllByTestId('HandleTestId');
    expect(handles.length).toBe(2); 
  });

});
