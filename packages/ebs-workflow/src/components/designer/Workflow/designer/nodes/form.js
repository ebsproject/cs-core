import { useCallback } from 'react';
import { Handle, Position, useNodeId } from '@xyflow/react';
import { v4 as uuidv4 } from 'uuid';
import { Styles, Core, Icons } from "@ebs/styleguide";
const { IconButton } = Core;
const { SettingsApplications } = Icons;

function FormNode({ data, isConnectable }) {
  const nodeId = useNodeId();

  return (
    <div className="form-node" data-testid={"MainContainerFormTestId"}>
      <Handle type="target" position={Position.Left} isConnectable={isConnectable} />
      <div>
        <label htmlFor="text">{`${data.name.substring(0, 15)}`}</label>
        <IconButton color="primary" className={"button"} onClick={(e) => data.onPropertyClick(e, nodeId, 'edit')} data-testid={"SettingsButtonTestId"}>
          <SettingsApplications data-testid={"SettingsIconTestId"} />
        </IconButton>
      </div>

      <Handle
        type="source"
        position={Position.Right}
        id={uuidv4()}
        isConnectable={isConnectable}
      />

    </div >
  );
}

export default FormNode;