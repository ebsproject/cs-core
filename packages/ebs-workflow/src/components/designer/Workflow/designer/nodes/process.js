import { useEffect, useState } from 'react';
import { Handle, Position, useNodeId, NodeResizer } from '@xyflow/react';
import { Core, Icons } from "@ebs/styleguide";
const { IconButton } = Core;
const { SettingsApplications } = Icons;

function ProcessNode({ data, isConnectable }) {
  const [size, setSize] = useState(null);
  useEffect(() => {
    let _size = {
      width: 80,
      height: 30
    }
    if (data) {

      if (data["dependOn"]) {
        let d = data.dependOn;
        if (d["size"]) {
          _size.width = d.size.width;
          _size.height = d.size.height
        }
      }

    }
    setSize(_size)
  }, [data])
  const nodeId = useNodeId();
  if (!size) return null
  return (
    <div style={{
      ...size,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      textAlign: 'center',
      padding: '1px',
      boxSizing: 'border-box',
    }}>
      <NodeResizer
        color='#000' 
        isVisible={true} minWidth={80} minHeight={30} />
      <IconButton data-testid={"SettingsButtonTestId"} color="primary" className={"button"} onClick={(e) => data.onPropertyClick(e, nodeId, 'edit')}>
        <SettingsApplications fontSize="small" data-testid={"SettingsIconTestId"} />
      </IconButton>
      <span
        style={{
          fontSize: `${Math.min(size.width, size.height) * 0.2}px`,
          lineHeight: 1.2,
        }}
      >
        {data.name}
      </span>
      <Handle type="target" position={Position.Left} isConnectable={isConnectable} />
      <Handle
        type="source"
        position={Position.Right}
        isConnectable={isConnectable}
      />
    </div>
  );
}

export default ProcessNode;