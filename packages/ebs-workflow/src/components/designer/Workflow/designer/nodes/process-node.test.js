import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Handle, Position, useNodeId, NodeResizer } from '@xyflow/react';
import ProcessNode from './process';

jest.mock('@xyflow/react', () => ({
  ...jest.requireActual('@xyflow/react'),
  Handle: jest.fn(() => <div data-testid="HandleTestId" />),
  NodeResizer: jest.fn(() => <div data-testid="NodeResizerTestId" />),
  useNodeId: jest.fn(),
}));

describe('Process Node component', () => {
  const mockUseNodeId = 'test-node-id';
  const mockOnPropertyClick = jest.fn();
  const dataMock = {
    name: 'Test Node',
    onPropertyClick: mockOnPropertyClick,
    dependOn: { size: { width: 100, height: 50 } },
  };

  beforeEach(() => {
    jest.clearAllMocks();
    useNodeId.mockReturnValue(mockUseNodeId);
  });

  test('renders with default size and updates when data is provided', () => {
    render(<ProcessNode data={dataMock} isConnectable={true} />);

    const nodeContainer = screen.getByText(dataMock.name).parentElement;

    expect(nodeContainer).toHaveStyle({ width: '100px', height: '50px' });
    expect(screen.getByTestId('NodeResizerTestId')).toBeInTheDocument();
    expect(screen.getByTestId('SettingsButtonTestId')).toBeInTheDocument();
    expect(screen.getByTestId('SettingsIconTestId')).toBeInTheDocument();
  });

  test('calls data.onPropertyClick with nodeId and "edit" on button click', () => {
    render(<ProcessNode data={dataMock} isConnectable={true} />);

    const button = screen.getByTestId('SettingsButtonTestId');
    fireEvent.click(button);

    expect(mockOnPropertyClick).toHaveBeenCalledWith(expect.any(Object), mockUseNodeId, 'edit');
  });

  test('renders handles with connectable prop', () => {
    render(<ProcessNode data={dataMock} isConnectable={true} />);

    const handles = screen.getAllByTestId('HandleTestId');
    expect(handles.length).toBe(2); 
  });

  // TODO: Review this segment in component

//   test('renders with nullable data in props', () => {
//     render(<ProcessNode data={null} isConnectable={true} />);
//     const nodeContainer = screen.getByText(dataMock.name).parentElement;
//     expect(nodeContainer).toHaveStyle({ width: '80px', height: '30px' });
//   });
});
