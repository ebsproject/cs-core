import { useState, useEffect } from 'react';
import { Handle, Position, useNodeId, NodeResizer } from '@xyflow/react';
import { Core, Icons } from "@ebs/styleguide";
const { IconButton } = Core;
const { SettingsApplications } = Icons;
const controlStyle = {
  background: 'transparent',
  borderRadius: '5px',
};

function TextUpdaterNode({ data, isConnectable }) {
  const nodeId = useNodeId();
  const [size, setSize] = useState(null);
  useEffect(() => {
    let _size = {
      width: 200,
      height: 80
    }
    if (data) {
      if (data["dependOn"]) {
        let d = data.dependOn;
        if (d["size"]) {
          _size.width = d.size.width;
          _size.height = d.size.height
        }
      }
    }
    setSize(_size);
  }, [data])
  if (!size) return null;
  return (
    <div style={size} data-testid={"ContainerTestId"}>
      <NodeResizer color='#000' style={controlStyle} isVisible={true} minWidth={200} minHeight={80} />
      <Handle type="target" position={Position.Top} isConnectable={isConnectable} />
      <div>
        <IconButton color="primary" className={"button"} onClick={(e) => data.onPropertyClick(e, nodeId, 'edit')} data-testid={"SettingsButtonTestId"}>
          <SettingsApplications fontSize="small" data-testid={"SettingsIconTestId"} />
        </IconButton>
        <label htmlFor="text">{`${data.name}`}</label>
      </div>
      <Handle
        type="source"
        position={Position.Bottom}
        isConnectable={isConnectable}
      />
    </div>
  );
}

export default TextUpdaterNode;