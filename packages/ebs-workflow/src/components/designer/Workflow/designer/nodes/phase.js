import React, { useState, useEffect } from 'react';
import { Handle, Position, useNodeId, NodeResizer } from '@xyflow/react';
import { Core, Icons } from "@ebs/styleguide";
const { IconButton } = Core;
const { SettingsApplications } = Icons;

function TextUpdaterNode({ data, isConnectable, ...props }) {

  const nodeId = useNodeId();
  const [size, setSize] = useState(null);
  useEffect(() => {
    let _size = {
      width: 400,
      height: 200
    }
    if (data) {
      if (data["dependOn"]) {
        let d = data.dependOn;
        if (d["size"]) {
          _size.width = d.size.width;
          _size.height = d.size.height
        }
      }
    }
    setSize(_size);
  }, [data])

  if (!size) return null;
  return (
    <div style={size} data-testid={"MainContainerPhaseTestId"}>
      <NodeResizer color='#000' isVisible={true} minWidth={400} minHeight={200} />
      <div>
        <IconButton color="primary" className={"button"} onClick={(e) => data.onPropertyClick(e, nodeId, 'edit')} data-testid={"SettingsButtonTestId"}>
          <SettingsApplications data-testid={"SettingsIconTestId"} />
        </IconButton>
        <label htmlFor="text">{`${data.name}`}</label>
      </div>
      <Handle type="target" position={Position.Top} isConnectable={isConnectable} />
      <Handle
        type="source"
        position={Position.Bottom}
        isConnectable={isConnectable}
      />
    </div>
  );
}

export default TextUpdaterNode