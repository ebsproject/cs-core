import { useCallback, memo } from 'react';
import { Handle, Position } from '@xyflow/react';

const handleStyle = { left: 10 };

function TextUpdaterNode({ data, isConnectable }) {
  // const onChange = useCallback((evt) => {

  // }, []);

  return (
    <div className="standard-node" data-testid={"MainContainerTestId"}>
      <Handle type="target" position={Position.Top} isConnectable={isConnectable} />
      <div>
        <label htmlFor="text">{data.name}</label>

      </div>
      <Handle
        type="source"
        position={Position.Bottom}
        id="a"
        //style={handleStyle}
        isConnectable={isConnectable}
      />

    </div>
  );
}

export default memo(TextUpdaterNode);