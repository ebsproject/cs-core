import React, { useEffect, useContext, useState, Suspense, useRef, Fragment } from 'react'
import { FormProvider, useForm, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { useDispatch } from "react-redux";
import { showMessage } from "store/modules/message";
import { Core } from "@ebs/styleguide";
import GeneralInfo from './general';
import { useWorkflowContext } from '../context/workflow-context';
import { useNavigate } from "react-router-dom";
import {
  findStageByPhase, findNodeById, findFieldByNode, findPhaseById, findWorkflowById,
  onCreateWorkflow, onModifyWorkflow, onMaintainPhase, onMaintainStage, onMaintainNode, onMaintainCF, onCreateRelationStageNode
} from './helpers/formFunction'
import AlertSaving from './alert-saving';
import { useAlertContext } from 'components/alerts/alert-context';
const DesignerV2 = React.lazy(() => import('./designer/designer'));
const { Grid, Typography, Box, Button } = Core;

const WorkflowForm = () => {
  const {setAlerts} = useAlertContext();
  const [openMessage, setOpenMessage] = useState(false);
  const [openDesigner, setOpenDesigner] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const navigate = useNavigate();
  const { workflow, setWorkflow, nodes, edges, setReload } = useWorkflowContext();
  const [workflowId, setWorkflowId] = useState(workflow.data?.id);
  const buttonSave = useRef(null)
  useEffect(() => {
    if (workflow.action === "unknown") {
      onCloseForm();
    }
  }, []);

  useEffect(() => {
    if (refresh) {
      const fetchData = async () => {
        const res = await findWorkflowById(workflowId.toString())//.then((res) => {
        const workflowInfo = res.data.findWorkflow;
        setWorkflow({
          action: "edit",
          data: workflowInfo
        })
      }
      fetchData();
    }
    return () => setRefresh(false);
  }, [refresh]);

  const formMethods = useForm(
    {
      defaultValues: {
        name: workflow.data?.name,
        title: workflow.data?.title,
        description: workflow.data?.description,
        sort: workflow.data?.sortNo,
        isSystem: workflow.data?.isSystem ? true : false,
        showMenu: workflow.data?.showMenu,
        products: workflow.data?.product,
        roles: workflow.data?.securityDefinition?.roles,
        users: workflow.data?.securityDefinition?.users,
        programs: workflow.data?.securityDefinition?.programs
      }
    },
    {
      sort: 1
    }
  );
  const { register, handleSubmit } = formMethods;

  const onCloseForm = () => {
    navigate('/wf/designer');
  }
  const onError = (errors, e) => console.log(errors, e);
  const onSubmit = async data => {
    setOpenMessage(true);
    let recordId = null;
    let res;
    if (workflow.action === 'add') {
      try {
        res = await onCreateWorkflow(data);
      } catch (error) {
        setAlerts((prev) => [...prev, { id: 'workflow-edited-error' + workflow.name, message: `There was an error in the ${workflow.action === 'edit' ? 'update' : 'create'} process. ${error}`, severity: 'error' }]);
       setOpenMessage(false);
        return;
      }
    }
    if (workflow.action === 'edit') {
      res = await onModifyWorkflow(data, workflow.data.id);
    }
    const getWorkflowId = workflow.action === 'add' ? res.data.createWorkflow.id : res.data.modifyWorkflow.id;
    setWorkflowId(getWorkflowId);
    if (workflow.action == 'edit') {
      try {
        await onDesignMaintainV2();
        setOpenMessage(false);
        setRefresh(true);
        setAlerts((prev) => [...prev, { id: 'workflow-edited-success-' + data.name, message: `The workflow ${data.name} was edited successfully`, severity: 'success' }]);
      } catch (error) {
        setOpenMessage(false);
        setRefresh(true);
        setAlerts((prev) => [...prev, { id: 'workflow-edited-error-' + data.name, message: `There was an error in the ${workflow.action === 'edit' ? 'update' : 'create'} process`, severity: 'error' }]);
        console.log(error)
      }
    } else {
      setOpenMessage(false);
      setRefresh(true);
      setAlerts((prev) => [...prev, { id: 'workflow-created-success-' + data.name, message: `The workflow ${data.name} was created successfully`, severity: 'success' }]);

    }
  };
    //TODO: It should be reviewed and improve
  const onDesignMaintainV2 = async () => {
    if (nodes) {
      let arrNodes = Array.from(nodes.filter(item => item.type === 'phase'));
      for (let i = 0; i < arrNodes.length; i++) {
        let nd = { ...arrNodes[i] };
        const res = await findPhaseById(nd)
        let arrResult = Array.from(res.data.findPhaseList.content);
        nd.mode = arrResult.length === 0 ? "create" : "modify";
        const response = await onMaintainPhase(nd)
        let phaseId = nd.dbId;
        if (nd.mode == "create")
          phaseId = response.data.createPhase.id;
        const arrNodesStage = Array.from(nodes.filter(el => el.type == "stage" && el.parentId == nd.designRef));
        for (let j = 0; j < arrNodesStage.length; j++) {
          let stage = { ...arrNodesStage[j] };
          stage.phaseId = nd.dbId;
          const r = await findStageByPhase(stage)
          const lg = r.data.findStageList.content;
          stage.mode = lg.length === 0 ? "create" : "modify";
          stage.data.phaseId = phaseId;
          const stageResponse = await onMaintainStage(stage)
          let stageId = stage.dbId;
          if (stage.mode == "create")
            stageId = stageResponse.data.createStage.id;
          const nodesFromStage = Array.from(nodes.filter(el => el.type == "process" && el.parentId == stage.designRef));
          for (let k = 0; k < nodesFromStage.length; k++) {
            let ndProcess = { ...nodesFromStage[k] };
            const r = await findNodeById(ndProcess);
            const lg = r.data.findNodeList.content;
            ndProcess.mode = lg.length == 0 ? "create" : "modify";
            const nodeResponse = await onMaintainNode(ndProcess)
            let nodeId = ndProcess.dbId;
            if (ndProcess.mode == "create")
              nodeId = nodeResponse.data.createNode.id;
            if (Array.isArray(ndProcess.data.fields)) {
              let arrFields = Array.from(ndProcess.data.fields);
              for (let h = 0; h < arrFields.length; h++) {
                let cf = { ...arrFields[h] }
                cf.nodeId = nodeId;
                if (cf.mode != "deleted") {
                  const r = await findFieldByNode(cf)//.then(r => {
                  const lg = r.data.findNodeCFList.content;
                  cf.mode = lg.length == 0 ? "create" : "modify";
                  await onMaintainCF(cf);
                } else {
                  await onMaintainCF(cf);
                }
              }
            }
          }
          let aryNodes = nodesFromStage.map(r => r.dbId );
          stage.nodes = aryNodes;
          stage.id = stage.dbId
          await onCreateRelationStageNode(stage) //.catch(err => console.log(err))
        }
      }
    }
  }
  const handleCloseDesigner = () =>{
  setOpenDesigner(false);
  buttonSave &&  buttonSave.current.click();
  }
  return (
    <Fragment>
      <AlertSaving open={openMessage} mode={workflow.action === 'add' ? 'created' : 'updated'} />
      <Grid container spacing={2} >
        <Grid item xs={12} >
          <Box >
            <legend >
              <Typography variant="h5" >
                <FormattedMessage
                  id="none"
                  defaultMessage="General Information"
                />
              </Typography>
            </legend>
            <FormProvider {...formMethods}>
              <form id='workflowForm' component="form" onSubmit={handleSubmit(onSubmit, onError)} >
                <GeneralInfo onCloseForm={onCloseForm} buttonSave ={buttonSave} setOpenDesigner={setOpenDesigner} />
              </form>
            </FormProvider>
          </Box>
        </Grid>
        <Grid container item xs={12} >
          {workflow.action == 'edit' &&
            <Suspense fallback={<div>{"..."}</div>}>
              <DesignerV2 buttonSave = {buttonSave} open={openDesigner} setOpenDesigner={setOpenDesigner} handleClose={handleCloseDesigner} />
            </Suspense>
          }
        </Grid>
      </Grid>
    </Fragment>


  )
}


export default WorkflowForm;
