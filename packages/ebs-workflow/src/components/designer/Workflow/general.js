import React, { useContext } from 'react';
import { FormattedMessage } from "react-intl";
import CustomTextField from "./helpers/texfield"
import { Styles, Core, Icons } from "@ebs/styleguide";
import ProductSelect from "./helpers/productSelect"
import Security from './security';
import { useFormContext, Controller } from "react-hook-form";
import { useWorkflowContext } from '../context/workflow-context';

const {
  styled: withStyles
} = Styles

const {
  Paper,
  Typography,
  Grid,
  Button,
  FormControlLabel,
  Checkbox, Box
} = Core;

const {
  CheckCircle,
  Cancel
} = Icons;

const styles = {
  // root: { marginTop: 20, width: "100%" },
  // paper: { borderRadius: 10, border: "1px dotted #000", padding: 50, },
  // container: { display: "grid", gridGap: 3 },
  // item: { width: "100%" },
  // box: { borderRadius: 5, border: "1px dotted black" },
  contain: { marginLeft: 20 }

};

const GeneralInfo = (props) => {

  const { control } = useFormContext();
  const { setOpenDesigner, onCloseForm , buttonSave} = props;
  const { workflow } = useWorkflowContext();
  
  return (

    <Grid container spacing={3} sx={{ padding: 5 }} data-testid={"GeneralInformationContainerTestId"} >
      <Grid container spacing={2} >
        <Grid item xs={9} >
          <CustomTextField name="name" label={"Name"} rules={{ required: "The Name is required" }} otherProps={{ fullWidth: true }} />
        </Grid>
        <Grid item xs={3}>
          <CustomTextField name="sort" label={"Sort No"} rules={{ required: "The Sort Number is required" }}
            otherProps={{ fullWidth: false }} />
        </Grid>
        <Grid item xs={12}>
          <CustomTextField name="title" label={"Title"} rules={{ required: "The Title is required" }}
            otherProps={{ fullWidth: true }} />
        </Grid>

        <Grid item xs={12}>
          <CustomTextField name="description" label={"Description"} rules={{ required: "The Description is required" }}
            otherProps={{ fullWidth: true }} />
        </Grid>

        <Grid item xs={3}>
          <ProductSelect />
        </Grid>
        <Grid item xs={3}>
          <FormControlLabel control={
            <Controller
              name="showMenu"
              control={control}
              defaultValue={true}
              rules={{ required: false }}
              render={({ field: { value, ...field } }) => <Checkbox checked={!!value} {...field} />}
            />
          } label={"Show as a Product"} />
        </Grid>
        <Grid item xs={3}>
          <FormControlLabel control={
            <Controller
              name="isSystem"
              control={control}
              defaultValue={true}
              rules={{ required: false }}
              render={({ field: { value, ...field } }) => <Checkbox checked={!!value} {...field} />}
            />
          } label={"Enable as System Workflow"} />
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{border:'1px solid gray', padding:'10px', margin:'5px'}}>
      <Grid item xs={12}  >
        <Security />
      </Grid>
      </Grid>

      <Grid container item xs={12} spacing={2} justifyContent="flex-end">
      {workflow.action === 'edit' && <Button onClick={()=> setOpenDesigner(true)}>
          Open Designer
        </Button>}
        <Button
          onClick={onCloseForm}
          startIcon={<Cancel />}
        >
          <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
        </Button>
        <Button
        ref={buttonSave}
          type="submit"
          // form="workflowForm"
          startIcon={<CheckCircle />}
        >
          <FormattedMessage id={"none"} defaultMessage={"Save"} />
        </Button>
      </Grid>
    </Grid>


  )
}


export default GeneralInfo;