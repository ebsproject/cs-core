import GeneralInfo from "./general";
import { render, screen } from "@testing-library/react";
import { useFormContext, Controller } from "react-hook-form";
import { useWorkflowContext } from '../context/workflow-context';
jest.mock("../context/workflow-context", () => ({
    useWorkflowContext: jest.fn(),
  }));
jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));

jest.mock('./helpers/texfield', () => () => <div>CustomTextField</div>);
jest.mock('./helpers/productSelect', () => () => <div>Product Dropdown</div>);
jest.mock('./security', () => () => <div>Security Component</div>);

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
  }));
  const workflowMock = {
    action: 'edit',
    data: {
      id: 1,
      name: 'Test Workflow',
      title: 'Test Title',
      description: 'Test Description',
      sortNo: 1,
      isSystem: false,
      showMenu: true,
      product: [],
      securityDefinition: {
        roles: [],
        users: [],
        programs: [],
      },
    },
  };


describe("General Information component", () =>{
    onCloseFormMock = jest.fn()

    beforeEach(() => {
        useWorkflowContext.mockReturnValue({
            workflow: workflowMock,
            setWorkflow: jest.fn(),
            nodes: []
          });
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });

    const renderComponent = () => render(<GeneralInfo  onCloseForm={onCloseFormMock}/>)

    test("General Information loaded", ()=>{
        renderComponent();
        expect(screen.getByTestId("GeneralInformationContainerTestId")).toBeInTheDocument();
    })
})