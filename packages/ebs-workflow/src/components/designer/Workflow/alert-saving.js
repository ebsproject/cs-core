import { Core, Icons } from "@ebs/styleguide";
import PropTypes from "prop-types";
const { Dialog, DialogContent, Typography, Icon, Alert } = Core;
const { Warning } = Icons;
import loader from "assets/images/time_loader.gif"
const AlertSaving = ({ mode, open }) => {
 
  return (
      <Dialog open={open} maxWidth={"md"} data-testid={"WorkflowAlertTestId"}>
        <DialogContent sx={{ padding: 2 }}>
          <Alert severity={"warning"}>
          <Typography variant="title">
            {`Please wait. The workflow definition is being ${mode}.`}
          </Typography>
          </Alert>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "100%"
            }}>
            <img src={loader} alt="loading..." />
          </div>
  
        </DialogContent>
      </Dialog>
  );
};
// Type and required properties
AlertSaving.propTypes = {
  mode: PropTypes.string,
  open: PropTypes.bool,
};
// Default properties

export default AlertSaving;
