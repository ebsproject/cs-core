import React from 'react';
import { render, screen, fireEvent, waitFor, act } from '@testing-library/react';
import WorkflowForm from './form';
import { FormProvider } from 'react-hook-form';
import { WorkflowContext } from '../context/workflow-context';
import { useDispatch } from 'react-redux';
import { showMessage } from 'store/modules/message';
import { useNavigate } from 'react-router-dom';
import { useAlertContext } from 'components/alerts/alert-context';
import { useWorkflowContext } from '../context/workflow-context';
import {
  findStageByPhase, findNodeById, findFieldByNode, findPhaseById, findWorkflowById,
  onCreateWorkflow, onModifyWorkflow, onMaintainPhase, onMaintainStage, onMaintainNode, onMaintainCF, onCreateRelationStageNode
} from './helpers/formFunction'

// jest.mock('@apollo/client', () => ({
//     ...jest.requireActual('@apollo/client'),
//     useQuery: jest.fn(),
// }));
jest.mock("components/alerts/alert-context", () => ({
  useAlertContext: jest.fn(),
}));
jest.mock("../context/workflow-context", () => ({
  useWorkflowContext: jest.fn(),
}));


jest.mock('react-redux', () => ({
  useDispatch: jest.fn(),
}));

jest.mock('store/modules/message', () => ({
  showMessage: jest.fn(),
}));

jest.mock('react-router-dom', () => ({
  useNavigate: jest.fn(),
}));
jest.mock('react-intl', () => ({
  FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

jest.mock('./helpers/formFunction', () => ({
  findStageByPhase: jest.fn().mockResolvedValue({ data: { findStageList: { content: [{ id: 1, name: "stage name" }] } } }),
  findNodeById: jest.fn().mockResolvedValue({ data: { findNodeList: { content: [{ id: 1, name: "node name" }] } } }),
  findFieldByNode: jest.fn().mockResolvedValue({ data: { findNodeCFList: { content: [{ id: 1, name: "some name" }] } } }),
  findPhaseById: jest.fn().mockResolvedValue({ data: { findPhaseList: { content: [{ id: 1, name: "phase name" }] } } }),
  findWorkflowById: jest.fn().mockResolvedValue({ data: { findWorkflow: { name: 'Test Workflow', id: 1 } }, }),
  onCreateWorkflow: jest.fn().mockResolvedValue({ data: { createWorkflow: { id: 1 } } }),
  onModifyWorkflow: jest.fn().mockResolvedValue({ data: { modifyWorkflow: { id: 1 } } }),
  onMaintainPhase: jest.fn().mockResolvedValue({ data: { createPhase: { id: 1 } } }),
  onMaintainStage: jest.fn().mockResolvedValue({ data: { createStage: { id: 1 } } }),
  onMaintainNode: jest.fn().mockResolvedValue({ data: { createNode: { id: 1 } } }),
  onMaintainCF: jest.fn(),
  onCreateRelationStageNode: jest.fn(),
}));

jest.mock('./designer/designer', () => () => <div>Designer</div>);
jest.mock('./general', () => () => <div>General Info<button>Save</button><button onClick={() => jest.fn()}>Cancel</button></div>);

describe('WorkflowForm Component', () => {
  const mockNavigate = jest.fn();
  useNavigate.mockReturnValue({ navigate: jest.fn()});
  const mockDispatch = jest.fn();
  useDispatch.mockReturnValue(mockDispatch);
  const mockNodes = [
    { id: 1, name: "Phase Name", type: "phase", mode: "create", parentId: 1 },
    { id: 2, name: "Phase Name 2", type: "phase", mode: "modify", parentId: 1 },
    { id: 3, name: "Stage Name", data: { phaseId: 1 }, type: "stage", mode: "create", parentId: 1 },
    { id: 4, name: "Stage Name 2", data: { phaseId: 1 }, type: "stage", mode: "modify", parentId: 1 },
    { id: 5, name: "Node Name", data: { fields: [{ id: 1, name: "field name 2" }] }, type: "process", mode: "create", parentId: 3 },
    { id: 6, name: "Node Name 2", data: { fields: [{ id: 1, name: "field name 2" }] }, type: "process", mode: "modify", parentId: 3 }
  ]

  const workflowMock = {
    action: 'edit',
    data: {
      id: 1,
      name: 'Test Workflow',
      title: 'Test Title',
      description: 'Test Description',
      sortNo: 1,
      isSystem: false,
      showMenu: true,
      product: [],
      securityDefinition: {
        roles: [],
        users: [],
        programs: [],
      },
    },
  };

  const setWorkflowMock = jest.fn();
  const setReloadMock = jest.fn();

  const renderComponent = () => {
    return render(
      <WorkflowForm />
    );
  };

  beforeEach(() => {
    jest.clearAllMocks();
    useAlertContext.mockReturnValue({
      setAlerts: jest.fn(),

    });
    useWorkflowContext.mockReturnValue({
      workflow: workflowMock,
      setWorkflow: jest.fn(),
      nodes: mockNodes
    });
  });

  test('renders the WorkflowForm component', async () => {
    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      expect(screen.getByText('General Information')).toBeInTheDocument();
    })

  });

  test('renders the WorkflowForm component', async () => {
    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      expect(screen.getByText('Designer')).toBeInTheDocument();
    })

  });

  test('click in save button', async () => {
    act(() => {
      renderComponent();
    })
    await waitFor(() => {

      expect(screen.getByText('Designer')).toBeInTheDocument();
      const button = screen.getByText(/save/i);
      fireEvent.click(button)
    })

  });
  test('click in save button when is a new definition', async () => {
    useWorkflowContext.mockReturnValueOnce({
      workflow: {
        action: 'add',
        data: {
          id: 1,
          name: 'Test Workflow',
          title: 'Test Title',
          description: 'Test Description',
          sortNo: 1,
          isSystem: false,
          showMenu: true,
          product: [],
          securityDefinition: {
            roles: [],
            users: [],
            programs: [],
          },
        },
      },
      setWorkflow: jest.fn(),
      nodes: mockNodes
    });
    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      //expect(screen.getByText('Designer')).toBeInTheDocument();
      const button = screen.getByText(/save/i);
      fireEvent.click(button)
    })

  });

  test('click in save button when is updated', async () => {
    findStageByPhase.mockResolvedValueOnce({ data: { findStageList: { content: [] } } }),
      findNodeById.mockResolvedValueOnce({ data: { findNodeList: { content: [] } } }),
      findFieldByNode.mockResolvedValue({ data: { findNodeCFList: { content: [] } } }),
      findPhaseById.mockResolvedValueOnce({ data: { findPhaseList: { content: [] } } }),
      onCreateWorkflow.mockResolvedValueOnce({ data: { createWorkflow: { id: 1 } } }),
      useWorkflowContext.mockReturnValueOnce({
        workflow: {
          action: 'edit',
          data: {
            id: 1,
            name: 'Test Workflow',
            title: 'Test Title',
            description: 'Test Description',
            sortNo: 1,
            isSystem: false,
            showMenu: true,
            product: [],
            securityDefinition: {
              roles: [],
              users: [],
              programs: [],
            },
          },
        },
        setWorkflow: jest.fn(),
        nodes: mockNodes
      });
    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      // expect(screen.getByText('Designer')).toBeInTheDocument();
      const button = screen.getByText(/save/i);
      fireEvent.click(button)

    })

  });
  test('click in save button when is a new definition an rise up an error', async () => {
    onCreateWorkflow.mockResolvedValueOnce(Error),

      useWorkflowContext.mockReturnValueOnce({
        workflow: {
          action: 'add',
          data: {
            id: 1,
            name: 'Test Workflow',
            title: 'Test Title',
            description: 'Test Description',
            sortNo: 1,
            isSystem: false,
            showMenu: true,
            product: [],
            securityDefinition: {
              roles: [],
              users: [],
              programs: [],
            },
          },
        },
        setWorkflow: jest.fn(),
        nodes: mockNodes
      });
    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      // expect(screen.getByText('Designer')).toBeInTheDocument();
      const button = screen.getByText(/save/i);
      fireEvent.click(button)
    })

  });
});
