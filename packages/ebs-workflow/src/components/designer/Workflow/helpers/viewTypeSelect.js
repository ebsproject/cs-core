import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_VIEWTYPE_LIST } from "utils/apollo/gql/catalogs"

const {
  TextField,
} = Core;

const { Autocomplete } = Lab;
const viewTypeSelect = () => {

  const { control, setValue, formState: { errors } } = useFormContext();
  const { loading, error, data } = useQuery(FIND_VIEWTYPE_LIST);

  if (loading) return 'Loading View Types...';
  if (error) return `Error! ${error.message}`;

  let viewTypes = data.findWorkflowViewTypeList.content;

  const renderInput = (params) => (
    <TextField
      {...params}
      label={
        <>
          <FormattedMessage id={"none"} defaultMessage={"Link to View Type"} />
          {` *`}{" "}
        </>
      }
      focused={Boolean(errors["nodeViewType"])}
      error={Boolean(errors["nodeViewType"])}
      helperText={errors["nodeViewType"]?.message}
    />
  );

  const optionLabel = (option) => `${option.name}`;

  const onChange = (e, options) => {
    e.preventDefault();
    setValue("nodeViewType", options);
  };

  return (
    <Controller
      control={control}
      name="nodeViewType"
      rules={{ required: true }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
        isOptionEqualToValue={(option, value) => option.name === value.name}
          options={viewTypes}
          onChange={onChange}
          value={value || null}
          getOptionLabel={optionLabel}
          renderInput={renderInput}
        />
      )}
    />
  )
}

export default viewTypeSelect;