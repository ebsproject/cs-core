import React, { useState, useEffect } from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Styles, Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_PROCESS_LIST } from "utils/apollo/gql/catalogs"
const {
  styled
} = Styles

const { TextField } = Core;

const { Autocomplete } = Lab;



const styles = {
  root: { marginTop: 20, width: "100%" },
};


const ProcessSelect = () => {
  const { control, setValue, formState: { errors } } = useFormContext();
  const { loading, error, data } = useQuery(FIND_PROCESS_LIST);

  if (loading) return 'Loading Process...';
  if (error) return `Error! ${error.message}`;

  let processes = data.findProcessList.content;

  const renderInput = (params) => {
    return (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Link to Process"} />
            {` *`}{" "}
          </>
        }
        focused={Boolean(errors["process"])}
        error={Boolean(errors["process"])}
        helperText={errors["process"]?.message}
        inputProps={{
          ...params.inputProps,

        }}
      />
    )

  };

  const optionLabelReports = (option) => `${option.name}`;

  const onProcessChange = (e, options) => {
    e.preventDefault();
    setValue("process", options);

  };

  return (
    <Controller
      control={control}
      name="process"
      rules={{ required: true }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
          value={value || null}
          isOptionEqualToValue={(option, value) => option.name === value.name}
          options={processes}
          onChange={onProcessChange}
          getOptionLabel={optionLabelReports}
          renderInput={renderInput}
        />
      )}
    />
  )
}
export default ProcessSelect