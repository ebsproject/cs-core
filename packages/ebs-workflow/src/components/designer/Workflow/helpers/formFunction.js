import { client as graphQLClient } from 'utils/apollo';
import { transform } from "node-json-transform";
import { mutationWorkflow, mutationPhase, mutationStage, mutationNode, mutationFormFields, mutationStageRelationsNode } from './jsonActions'
import { v4 as uuidv4 } from 'uuid';
import {
  FIND_PHASE_BY_ID,
  FIND_STAGE_BY_ID,
  FIND_NODE_BY_ID,
  FIND_NODE_CF_BY_ID,
  FIND_WORKFLOW_BY_ID,
  CREATE_WORKFLOW, MODIFY_WORKFLOW,
  CREATE_PHASE, MODIFY_PHASE,
  CREATE_STAGE, MODIFY_STAGE,
  CREATE_NODE, MODIFY_NODE,
  CREATE_NODE_CF, MODIFY_NODE_CF,
  buildDeleteMutation
} from "utils/apollo/gql/workflow"


export const onDeleteWorkflowObject = async (id, entity) => {
  return graphQLClient.mutate({
    mutation: buildDeleteMutation(entity),
    variables: {
      id: id
    }
  })
}

export const findPhaseById = async phase => {

  return await graphQLClient.query({
    query: FIND_PHASE_BY_ID,
    variables: {
      filters: [
        { col: "id", mod: "EQ", val: phase.dbId.toString() },
        { col: "workflow.id", mod: "EQ", val: phase.data.workflowId }
      ]
    }
  });
}

export const findStageByPhase = async stage => {

  return await graphQLClient.query({
    query: FIND_STAGE_BY_ID,
    variables: {
      filters: [
        { col: "id", mod: "EQ", val: stage.dbId},
        { col: "phase.id", mod: "EQ", val: stage.phaseId }
      ]
    },fetchPolicy:"no-cache"
  });
}

export const findNodeById = async node => {

  return await graphQLClient.query({
    query: FIND_NODE_BY_ID,
    variables: {
      filters: [
        { col: "id", mod: "EQ", val: node.dbId || node.id }
      ]
    }
  });
}

export const findFieldByNode = async cf => {

  return await graphQLClient.query({
    query: FIND_NODE_CF_BY_ID,
    variables: {
      filters: [
        { col: "id", mod: "EQ", val: cf.id },
        { col: "node.id", mod: "EQ", val: cf.nodeId }
      ]
    }
  });
}


export const findWorkflowById = async id => {

  return await graphQLClient.query({
    query: FIND_WORKFLOW_BY_ID,
    variables: {
      id: id.toString()
    },
    fetchPolicy: 'no-cache'
  });
}

export const refreshWorkflowById = async id => {
  return graphQLClient.refetchQueries({
    include: FIND_NODE_BY_ID
  })
}




export const onCreateWorkflow = async data => {
  const workflowInputObject = transform(data, mutationWorkflow);
  let result = await graphQLClient.mutate({
    mutation: CREATE_WORKFLOW,
    variables: { workflow: workflowInputObject }
  });
  return result;
}

export const onModifyWorkflow = async (data, workflowId) => {

  const workflowInputObject = transform(data, mutationWorkflow);
  workflowInputObject.id = workflowId;

  let result = await graphQLClient.mutate({
    mutation: MODIFY_WORKFLOW,
    variables: { workflow: workflowInputObject }
  })


  return result;
}

export const onMaintainPhase = async data => {

 

  if (data.mode == 'create') {
    let uid = uuidv4()
    data = {...data, designRef:uid}
    const phaseInputObject = transform(data, mutationPhase);
    return {...await graphQLClient.mutate({
      mutation: CREATE_PHASE,
      variables: { phase: phaseInputObject }
    }), designRef: uid };

  }

  if (data.mode == 'modify') {
    const phaseInputObject = transform(data, mutationPhase);
    phaseInputObject.id = data.dbId;
    return await graphQLClient.mutate({
      mutation: MODIFY_PHASE,
      variables: { phase: phaseInputObject }
    });
  }

}

export const onMaintainStage = async data => {
 
  
  if (data.mode == 'create') {
    let uid = uuidv4()
    data = {...data, designRef: uid}
    const stageInputObject = transform(data, mutationStage);
    return {...await graphQLClient.mutate({
      mutation: CREATE_STAGE,
      variables: { stage: stageInputObject }
    }), designRef: uid };
  }

  if (data.mode == 'modify') {
    const stageInputObject = transform(data, mutationStage);
    stageInputObject.id = data.dbId;
    return await graphQLClient.mutate({
      mutation: MODIFY_STAGE,
      variables: { stage: stageInputObject }
    });
  }
}

export const onMaintainNode = async data => {

  if (data.mode == 'create') {
    let uid = uuidv4()
    data ={...data, designRef: uid }
    const nodeInputObject = transform(data, mutationNode);
    return {...await graphQLClient.mutate({
      mutation: CREATE_NODE,
      variables: { node: nodeInputObject }
    }), designRef: uid};
  }

  if (data.mode == 'modify') {
    const nodeInputObject = transform(data, mutationNode);
    nodeInputObject.id = data.dbId || data.id;
   return await graphQLClient.mutate({
      mutation: MODIFY_NODE,
      variables: { node: nodeInputObject }
    });
  }
}


export const onMaintainCF = async data => {
  data = {...data, designRef: uuidv4()};

  const cfInputObject = transform(data, mutationFormFields);
  let result = null;

  if (data.mode == 'create') {
    result = await graphQLClient.mutate({
      mutation: CREATE_NODE_CF,
      variables: { nodeCF: cfInputObject }
    });
  }

  if (data.mode == 'modify') {
    cfInputObject.id = data.id;
    result = await graphQLClient.mutate({
      mutation: MODIFY_NODE_CF,
      variables: { nodeCF: cfInputObject }
    });
  }
  if (data.mode == 'deleted') {
    result = await graphQLClient.mutate({
      mutation: buildDeleteMutation('NodeCF'),
      variables: { id: data.id }
    });
  }
  return result;
}

export const onCreateRelationStageNode = async data => {
 // data.designRef = uuidv4();
  const stageInputObject = transform(data, mutationStageRelationsNode);
  let result = null;
  stageInputObject.id = data.id;
  result = await graphQLClient.mutate({
    mutation: MODIFY_STAGE,
    variables: { stage: stageInputObject }
  });
  return result;
}