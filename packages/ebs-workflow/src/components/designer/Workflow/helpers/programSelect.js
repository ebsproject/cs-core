import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_PROGRAM_LIST } from "utils/apollo/gql/catalogs"

const {
  TextField,
  Checkbox
} = Core;

const { Autocomplete } = Lab;


const ProgramSelect = (props) => {

  const { control, setValue, formState: { errors } } = useFormContext();
  const { loading, error, data } = useQuery(FIND_PROGRAM_LIST);

  if (loading) return 'Loading Program...';
  if (error) return `Error! ${error.message}`;

  let programs = data.findProgramList.content;

  const renderInput = (params) => (
    <TextField
      {...params}
      label={
        <>
          <FormattedMessage id={"none"} defaultMessage={"Link to Program"} />
          {` *`}{" "}
        </>
      }
      focused={Boolean(errors["programs"])}
      error={Boolean(errors["programs"])}
      helperText={errors["programs"]?.message}
    />
  );

  const optionLabel = (option) => `${option.name}`;

  const onChange = (e, options) => {
    e.preventDefault();
    setValue("programs", options || []);
  };
  const renderOption = (props,option, { selected }) => {
    const { key, ...restProps  } = props;
    return (
      <li key={key} {...restProps}>
        <Checkbox checked={selected} />
        {option.name}
      </li>
    );
  };

  return (
    <Controller
      data-testid ={"ProgramSelectTestId"}
      control={control}
      name="programs"
      render={({ field: { value, ...field } }) => (
        <Autocomplete
          multiple
          isOptionEqualToValue={(option, value) => option.id === value.id}
          value={value || []}
          options={programs}
          onChange={onChange}
          getOptionLabel={optionLabel}
          renderInput={renderInput}
          renderOption={renderOption}
          disableCloseOnSelect
        />
      )}
    />
  )
}


export default ProgramSelect