import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_ROLE_LIST } from "utils/apollo/gql/catalogs";
import SecurityRules from '../security-rule';
const {
  TextField,
  Checkbox
} = Core;

const { Autocomplete } = Lab;



const RoleSelect = (props) => {
  const { control, setValue, watch, formState: { errors } } = useFormContext();
  const { loading, error, data } = useQuery(FIND_ROLE_LIST);

  if (loading) return 'Loading Roles...';
  if (error) return `Error! ${error.message}`;

  let roles = data.findRoleList.content;

  let rules = watch("roles");


  const renderInput = (params) => (
    <TextField
      {...params}
      label={
        <>
          <FormattedMessage id={"none"} defaultMessage={"Link to Roles"} />
          {` *`}{" "}
        </>
      }
      focused={Boolean(errors["roles"])}
      error={Boolean(errors["roles"])}
      helperText={errors["roles"]?.message}
    />
  );

  const optionLabel = (option) => `${option.name}`;

  const onChange = (e, options) => {
    e.preventDefault();
    setValue("roles", options || []);
  };
  const renderOption = (props,option, { selected }) => {
    const { key, ...restProps  } = props;
    return (
      <li key={key} {...restProps}>
        <Checkbox checked={selected} />
        {option.name}
      </li>
    );
  };

  return (

    <>
      <Controller
        control={control}
        name="roles"
        render={({ field: { value, ...field } }) => (
          <Autocomplete
            multiple
            isOptionEqualToValue={(option, value) => option.name === value.name}
            value={value || []}
            options={roles}
            onChange={onChange}
            getOptionLabel={optionLabel}
            renderInput={renderInput}
            renderOption={renderOption}
            disableCloseOnSelect
          />
        )}
      />
      <SecurityRules rules={rules} />
    </>


  )
}


export default RoleSelect