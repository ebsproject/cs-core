import React, { useState } from 'react'
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { Styles, Core, Icons } from "@ebs/styleguide";


const {
  styled
} = Styles

const {
  Grid,
  List,
  ListItem,
  ListItemAvatar,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  Box,
  Typography,
  Checkbox,
  IconButton

} = Core;

const {
  Delete
} = Icons;

const styles = {
  root: { marginTop: 20, width: "100%", padding: 10 },
  box: { borderRadius: 5, border: "1px dotted black" },
  message: { fontWeight: "bold", marginLeft: 20 },
  list: { marginLeft: 20 }
};


const FormListAttributes = (props) => {
  const {  fields, itemSelected, onItemRemoved, onItemSelected } = props;





  return (
    <Box >
      {fields.length == 0 ? <p >Not Fields</p> :

        <List dense={true} data-testid={"AttributeListTestId"}>
          {fields.filter((field) => field.mode !== 'deleted').map((field, i) =>
            <ListItem key={field.id} //onClick={() => onItemSelected(field)}
            >
              <ListItemIcon>
                <Checkbox
                  edge="start"
                  checked={itemSelected == field.id}
                  tabIndex={-1}
                  disableRipple
                  onChange={(e) => onItemSelected(field,e)}
                  inputProps={{ 'aria-labelledby': `labelId${i}` }}
                />
              </ListItemIcon>
              <ListItemText id={`labelId${field.id}`} >
                <Typography variant={"h6"}  >
                  <FormattedMessage id={"none"} defaultMessage={`${field.attrName} - ${field.attrType.name} - (${field.mode || "No changes"})`} />
                </Typography>
              </ListItemText>
              <ListItemSecondaryAction>
                <IconButton data-testid={"RemoveButtonTestId"} onClick={() => onItemRemoved(field.id)} edge="end" aria-label="delete">
                  <Delete color="primary" />
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>


          )}
        </List>
      }
    </Box>
  )
}

FormListAttributes.propTypes = {
  fields: PropTypes.array.isRequired,
  itemSelected: PropTypes.number,
  onItemRemoved: PropTypes.func.isRequired,
  onItemSelected: PropTypes.func.isRequired,

};
// Default properties
// FormListAttributes.defaultProps = {
//   fields: [],
//   itemSelected: 0
// };

export default FormListAttributes