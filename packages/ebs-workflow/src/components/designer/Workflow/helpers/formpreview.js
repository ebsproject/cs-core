import { EbsForm } from "@ebs/components"
import { Core } from "@ebs/styleguide";
import { useEffect, useState } from "react";
import { createDefinition } from "./functions";
const { Dialog, DialogContent, DialogActions, Button } = Core;

const FormPreview = ({ open, handleClose, fields }) => {

    const [ebsDefinition, setEbsDefinition] = useState(null);
    useEffect(() => {
        if (fields) {
            let definition = createDefinition(fields);
            setEbsDefinition(definition);
        }
    }, [fields]);

    if (!ebsDefinition) return <>Build form preview....</>
    return (
        <Dialog
            onClose={handleClose}
            open={open}
            maxWidth={"lg"}
            fullWidth
            data-testid={"FormPreviewDialogTestId"}
        >
            <DialogContent>
                <EbsForm
                    definition={() =>ebsDefinition}
                    onSubmit={()=>{}}
                    actionPlacement="top"
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} >
                    Close
                </Button>
            </DialogActions>
        </Dialog>
    )
}
export default FormPreview;