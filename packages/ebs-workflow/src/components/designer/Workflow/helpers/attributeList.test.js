import { render, fireEvent } from "@testing-library/react";
import FormListAttributes from "./attributeList";
import { useFormContext, Controller } from "react-hook-form";
jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Form Attribute List component", () => {

    test("No fields to load", () => {
        const fields = [];
        const itemSelected = 0;
        const onItemRemoved = jest.fn();
        const onItemSelected = jest.fn();
        const { getByText } = render(<FormListAttributes fields={fields} itemSelected={itemSelected} onItemRemoved={onItemRemoved} onItemSelected={onItemSelected} />)
        expect(getByText(/Not Fields/i)).toBeInTheDocument();
    });

    test("Fields loaded", () => {
        const fields = [{id:1, attrName:"Test Name", mode:null, attrType:{name:"Test Attribute Tye Name"}}];
        const itemSelected = 0;
        const onItemRemoved = jest.fn();
        const onItemSelected = jest.fn();
        const { getByTestId } = render(<FormListAttributes fields={fields} itemSelected={itemSelected} onItemRemoved={onItemRemoved} onItemSelected={onItemSelected} />)
        expect(getByTestId("AttributeListTestId")).toBeInTheDocument();
    });

    test("Click to select item ", () => {
        const fields = [{id:1, attrName:"Test Name", mode:"edit", attrType:{name:"Test Attribute Tye Name"}}];
        const itemSelected = 0;
        const onItemRemoved = jest.fn();
        const onItemSelected = jest.fn();

        const { getAllByRole, getByTestId } = render(<FormListAttributes fields={fields} itemSelected={itemSelected} onItemRemoved={onItemRemoved} onItemSelected={onItemSelected} />)
        const inputs = getAllByRole('checkbox');
        inputs.forEach((chk, i) =>{
            fireEvent.click(chk);
            expect(onItemSelected).toHaveBeenCalled()

        })

    });
    test("Click to remove item ", () => {
        const fields = [{id:1, attrName:"Test Name", mode:"edit", attrType:{name:"Test Attribute Tye Name"}}];
        const itemSelected = 0;
        const onItemRemoved = jest.fn();
        const onItemSelected = jest.fn();

        const { getAllByTestId } = render(<FormListAttributes fields={fields} itemSelected={itemSelected} onItemRemoved={onItemRemoved} onItemSelected={onItemSelected} />)
        const inputs = getAllByTestId('RemoveButtonTestId');
        inputs.forEach((chk, i) =>{
            fireEvent.click(chk);
            expect(onItemRemoved).toHaveBeenCalled()
        })
    });
});
