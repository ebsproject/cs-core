import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_PRODUCT_LIST } from "utils/apollo/gql/catalogs"
const { TextField } = Core;
const { Autocomplete } = Lab;

const ProductSelect = () => {

  const { control, setValue, formState: { errors } } = useFormContext();
  const { loading, error, data } = useQuery(FIND_PRODUCT_LIST);

  if (loading) return 'Loading Products...';
  if (error) return `Error! ${error.message}`;

  let products = data.findProductList.content;
  products = products.filter(product => product.hasWorkflow)

  const renderInput = (params) => {
    return (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Link to Product"} />
            {` *`}{" "}
          </>
        }
        focused={Boolean(errors["products"])}
        error={Boolean(errors["products"])}
        helperText={errors["products"]?.message}
        inputProps={{
          ...params.inputProps,

        }}
      />
    )
  };

  const optionLabelReports = (option) => `${option.name}`;

  const onProductChange = (e, options) => {
    e.preventDefault();
    setValue("products", options);

  };

  return (
    <Controller
      control={control}
      name="products"
      rules={{ required: true }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
          groupBy={(option) => option.domain.name}
          isOptionEqualToValue={(option, value) => option.name === value.name}
          options={products}
          value={value || null}
          onChange={onProductChange}
          getOptionLabel={optionLabelReports}
          renderInput={renderInput}
        />
      )}
    />
  )
}


export default ProductSelect