import FormPreview from "./formpreview";
import { render, screen } from "@testing-library/react";
import { createDefinition } from "./functions";

jest.mock('./functions', () => ({
    createDefinition: jest.fn(),
}))

describe("Form Preview component", () => {

const renderComponent = (props) => render(<FormPreview open={props.open} handleClose={props.handleClose} fields={props.fields} />)
    test("form preview loaded with data", () => {
        const mockReturn ={
            components: [{
                sort: 1,
                component: "Test component",
                name: "Test Name",
                sizes: [12,12,12,12,12],
                inputProps: {label:'test label'}
            }]
        }
        const mockProps = {
            open: true,
            handleClose: jest.fn(),
            fields: {}
        }
        createDefinition.mockReturnValue(mockReturn)
        renderComponent(mockProps)
        expect(screen.getByTestId("FormPreviewDialogTestId")).toBeInTheDocument();
    });

    test("form preview loading data when fields are not provided", () => {
        const mockProps = {
            open: true,
            handleClose: jest.fn(),
            fields: null
        }
        renderComponent(mockProps)
        expect(screen.getByText("Build form preview....")).toBeInTheDocument();
    })


})