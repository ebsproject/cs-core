import { FIND_DOMAIN_LIST } from "utils/apollo/gql/catalogs";
import { client as graphQLClient } from "utils/apollo";
import { restClient } from "utils/axios/axios";

export function isEmpty(value) {
  return (
    // null or undefined
    (value == null) ||

    // has length and it's zero
    (value.hasOwnProperty('length') && value.length === 0) ||

    // is an Object and has no keys
    (value.constructor === Object && Object.keys(value).length === 0)
  )
}
export async function getSgContext() {
  const { data } = await graphQLClient.query({
    query: FIND_DOMAIN_LIST,
  })
  const sgContext = data.findDomainList.content.map(item => {
    return { value: item.prefix, label: item.name }
  })
  return sgContext;
}
export async function getSequenceSegments() {
  try {
    const { data } = await restClient.get(`sequence/1`);
    const result = data.segments.map(item => {
      return { label: item.name, value: item.name }
    })
    return result;
  } catch (error) {
    return []
  }

}

export const createDefinition = (fields) => {
  let components = [];
  for (let i = 0; i < fields.length; i++) {
    const field = fields[i];
    if (!field.attrField.disabled) {
      switch (field.attrType.name.toLowerCase()) {
        case "select":
          let selectComp = {
            sort: field.attrField.sort,
            component: field.attrType.name.toLowerCase(),
            name: field.attrName,
            sizes: field.attrField.sizes,
            options: field.attrField.defaultOptions ? field.attrField.defaultOptions : [{ label: "Option", value: 1 }],
            inputProps: field.attrField.inputProps,
          }
          selectComp.defaultValue = { label: "Label here", value: 1 };
          components.push(selectComp);
          break;
        case "textfield":
          let textComp = {
            sort: field.attrField.sort,
            component: field.attrType.name.toLowerCase(),
            name: field.attrName,
            sizes: field.attrField.sizes,
            inputProps: field.attrField.inputProps,
          }
          textComp.defaultValue = "";
          components.push(textComp);
          break;
        case "datepicker":
          let date = new Date();
          let dateComp = {
            sort: field.attrField.sort,
            component: "datepicker",
            name: field.attrName,
            sizes: field.attrField.sizes,
            inputProps: field.attrField.inputProps,
            defaultValue: date.toISOString().split("T")[0],
          };
          dateComp.defaultValue = date.toISOString().split("T")[0];
          components.push(dateComp);
          break;
        case "checkbox":
          let checkboxComp = {
            sort: field.attrField.sort,
            component: field.attrType.name.toLowerCase(),
            name: field.attrName,
            sizes: field.attrField.sizes,
            inputProps: field.attrField.inputProps,
          }
          checkboxComp.defaultValue = true;
          components.push(checkboxComp);
          break;
        case "radiogroup":
          let radioComp = {
            sort: field.attrField.sort,
            name: field.attrName,
            label: field.attrField.inputProps.label,
            sizes: field.attrField.sizes,
            inputProps: field.attrField.inputProps,
            component: "radio",
            row: false,
            options: field.attrField.options,

          }
          radioComp.defaultValue = "";
          components.push(radioComp);
          break;
        case "switchq":
          let switchComp = {
            sort: field.attrField.sort,
            name: field.attrName,
            sizes: field.attrField.sizes,
            component: "switch",
            label: field.attrField.attrName,
          }
          components.push(switchComp);
          break;
        default: break;
      }
    }
  }
  return {
    components: components.sort((a, b) => { return a.sort - b.sort }),
  }
}