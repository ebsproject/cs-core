export const formDefinition = [
  {
    id: 1,
    sort: 1,
    sizes: [12, 12, 12, 12, 12],
    name: '',
    inputProps: {
      disabled: false,
      label: '',
      multiline: false,
      fullWidth: true,
      rows: 1,
      variant: 'outlined'
    },
    helper: {
      title: '',
      placement: 'top'
    },
    rules: {
      required: ''
    },
    defaultRules: {
      uri:"", // end point to query
      applyRules:false,// apply or not the rules, if false the component will work without dependencies
      entity: "Contact", // Entity to query
      apiContent: [{ accessor: "id" }, { accessor: "another.field" }], //query to API [{ accessor: "id" }, { accessor: "person.givenName" }, { accessor: "person.familyName" }]
      parentControl: "control name", //Parent control that triggers the data population
      field: "", // field to access the api result
      columnFilter: "id", // column that will be used to filter the data
      label: ["familyName", "givenName"], // label to show in the dropdown, use only in Select component
      customFilters:[], //add here filters that cannot be calculated in runtime
      sequenceRules:{ // sequence rules apply for textfield 
        applyRule:false,
        ruleName:""
      }
    },
    defaultValue: '',
    showInGrid:true,
    modalPopup:{
      show:false,
      componentUI:"",
    }
  },
  {
    id: 2,
    sizes: [12, 12, 12, 12, 12],
    sort: 1,
    helper: {
      title: "Check a value",
      placement: 'top'
    },
    inputProps: { label: "" },
    checked: true,
    showInGrid:true,
    disabled:false
  },
  {
    id: 3,
    sort: 1,
    sizes: [12, 12, 12, 12, 12],
    name: '',
    inputProps: {

    },
    helper: {
      title: '',
      placement: 'top'
    },
    rules: {
      required: ''
    },
    defaultValue: '',
    showInGrid:true,
    disabled: false
  },
  {
    id: 4,
    sort: 1,
    sizes: [12, 12, 12, 12, 12],
    helper: {
      title: "Please select the type",
      placement: 'top'
    },
    options: [
      {
        label: "Propagative",
        value: "propagative"
      },
      {
        label: "Non Propagative",
        value: "non-propagative"
      }
    ],
    defaultValue: "propagative",
    inputProps: { label: "" },
    rules: { required: "" },
    row: false,
    showInGrid:true,
    disabled:false
  },
  {
    id: 5,
    sort: 1,
    sizes: [12, 12, 12, 12, 12],
    entity: "Contact",
    columns: [{accessor:""}], //{name:'Name',accessor :'requestor.person.givenName', hide:true} columns defined for EBS-Grid
   // controller: {}, //{name:"recipientId", type:"Integer"} mapped to cs-api input object
    helper: {
      title: "",
      placement: "top"
    },
    filters: [{col:"",val:"",mod:"EQ"}],
    apiContent: [
      "id",
      "name"
    ],
    inputProps: {
      color: "primary",
      label: "",
      variant: 'outlined'
    },
    rules: { required: "" },
    defaultRules: {
      uri:"", // end point to query
      applyRules:false,// apply or not the rules, if false the component will work without dependencies
      entity: "Contact", // Entity to query
      apiContent: [{ accessor: "id" },{ accessor: "another.field" }], //query to API [{ accessor: "id" }, { accessor: "person.givenName" }, { accessor: "person.familyName" }]
      parentControl: "control name", //Parent control that triggers the data population
      field: "person", // field to access the api result
      columnFilter: "category.name", // column that will be used to filter the data
      label: ["familyName", "givenName"], // label to show in the dropdown, use only in Select component
      customFilters:[] //add here filters that cannot be calculated in runtime
    },
    showInGrid:true,
    modalPopup:{
      show:false,
      componentUI:"",
    },
    disabled:false
  },
  {
    id: 6,
    sort: 1,
    sizes: [12, 12, 12, 12, 12],
    helper: {
      title: "",
      placement: 'top'
    },
    inputProps: {
      color: "primary",
      disabled: false
    },
    rules: { required: "" },
    defaultValue: true,
    showInGrid:true,
    disabled: false
  },
  {
    id: 8,
    sort: 1,
    sizes: [12, 12, 12, 12, 12],
    name: '',
    customProps: {
      classes: "",
      color: 'primary',
      disabled: false,
      fullWidth: true,
      size: 'large',
      acceptedFiles: ['image/*'],
      cancelButtonText: "cancel",
      submitButtonText: "submit",
      maxFileSize: 5000000,
      showPreviews: true,
      showFileNamesInPreview: true
    },
    helper: { title: "", placement: "top" },
    rules: {
      required: ""
    },
    defaultValue: '',
    showInGrid:false,
    disabled:false
  },
  {
    id: 10,
    sort: 1,
    sizes: [2, 2, 2, 2, 2],
    name: '',
    label:"",
    helper: { title: "", placement: "top" },
    rules: {
      required: ""
    },
    defaultRules:{
      showIfValue:"",
      componentUI:""
    },
    showInGrid:false,
    disabled:false
  },
]


const nodeBase = {
  after:
  {
    executeNode: '',
    sendNotification: {
      send: false,
      message: ""
    }
  },
  before: {
    validate: {
      valid: false,
      type: "javascript",
      code: "",
      functions: "",
      onError:"",
      onSuccess:""

    }
  },
  inputProps: {
    sourceNodes: []
  },
  outputProps: {
    targetNodes: []
  },
  disabled: false
}

export const nodeDefinition = [
  {
    id: 1,
    title: '',
    ...nodeBase
  },
  {
    id: 3,
    template: '',
    allFlow: false,
    allPhase:false,
    rules:{validationFunction:'',parameters:[{columnName:'',columnValue:''}]},
    ...nodeBase
  },
  {
    id: 4,
    contacts:[{contact:''}],
    attachments:{attach:false,templates:[{template:'', format:''}]},
    email: '',
    emailName:'',
    sendToRecipient:false,
    sendToSender:false,
    sendToRequestor:false,
    sendToSHU:false,
    ...nodeBase
  },
  {
    id: 5,
    status: '',
    statusName:'',
    customSecurityRules: [{ actor:"", allowEdit: true, allowDelete: true, allowView: true, allowViewProgress: true, allowAddNotes: true }],
    ...nodeBase
  },
  {
    id: 7,
    title: '',
    message: '',
    ...nodeBase
  },
  {
    id: 6,
    node: '',
    nodeName:'',
    action: '',// back | next | reset
    ...nodeBase
  },
  {
    id: 8,
    component: '',
    rules:{columns:[{name:"",alias:"",hidden:false}],legalDocuments:{security:{allowedStatus:{status:''}}}},
    ...nodeBase
  },
  {
    id: 9,
    form: '',
    formName:'',
    ...nodeBase
  },
  {
    id: 10,
    method: "",//post, get, delete
    sgcontext: '', //base URL
    path: "",      // api 
    bodyType: "",
    queryParameters: "",
    field: "",
    ...nodeBase
  },
  {
    id: 11,
    executeNodeIfTrue: "",
    executeNodeIfFalse: "",
    executeNodeIfTrueName: "",
    executeNodeIfFalseName: "",
    field: "",
    expectedValue: "",
    ...nodeBase
  }
]