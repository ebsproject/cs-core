import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_NODETYPE_LIST } from "utils/apollo/gql/catalogs"
const {
  TextField,
} = Core;

const { Autocomplete } = Lab;

const NodeTypeSelect = () => {
  const { control, setValue, formState: { errors } } = useFormContext();

  const { loading, error, data } = useQuery(FIND_NODETYPE_LIST);

  if (loading) return 'Loading Types...';
  if (error) return `Error! ${error.message}`;

  let nodeTypes = data.findNodeTypeList.content;

  const renderInput = (params) => {

    return (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Link to Types"} />
            {` *`}{" "}
          </>
        }
        focused={Boolean(errors["nodeType"])}
        error={Boolean(errors["nodeType"])}
        helperText={errors["nodeType"]?.message}
        inputProps={{
          ...params.inputProps,

        }}
      />
    )

  };

  const optionLabelReports = (option) => `${option.name}`;

  const onFormTypeChange = (e, options) => {
    e.preventDefault();
    setValue("nodeType", options);

  };

  return (
    <Controller
      control={control}
      name="nodeType"
      rules={{ required: true }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
        isOptionEqualToValue={(option, value) => option.name === value.name}
          options={nodeTypes}
          value={value || null}
          onChange={onFormTypeChange}
          getOptionLabel={optionLabelReports}
          renderInput={renderInput}
        />
      )}
    />
  )
}

export default NodeTypeSelect