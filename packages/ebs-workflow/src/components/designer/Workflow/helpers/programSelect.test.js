import { render, fireEvent } from "@testing-library/react";
import ProgramSelect from "./programSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Program Selected component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("Error in the Query", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<ProgramSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<ProgramSelect />)
        expect(getByText("Loading Program...")).toBeInTheDocument();
    });

    test('renders programs list correctly', () => {
        const mockData = { findProgramList: { content: [{ id: '1', name: 'Program A' }, { id: '2', name: 'Program B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ProgramSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Program A')).toBeInTheDocument();
        expect(getByText('Program B')).toBeInTheDocument();
    });

    test('set the selected program', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findProgramList: { content: [{ id: '1', name: 'Program A' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ProgramSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Program A'));
        expect(mockSetValue).toHaveBeenCalledWith('programs', [{ id: '1', name: 'Program A' }]);
    });

});
