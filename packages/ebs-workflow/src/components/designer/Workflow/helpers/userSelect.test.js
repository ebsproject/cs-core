import { render, fireEvent } from "@testing-library/react";
import UserSelect from "./userSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("User Select component", () => {
  
    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() },
        });
    });


    test("Error in the Query", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<UserSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<UserSelect />)
        expect(getByText("Loading User...")).toBeInTheDocument();
    });

    test('renders users list correctly', () => {
        const mockData = { findUserList: { content: [{ id: '1', userName: 'User A' }, { id: '2', userName: 'User B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<UserSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('User A')).toBeInTheDocument();
        expect(getByText('User B')).toBeInTheDocument();
    });

    test('set the selected user', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });

        const mockData = { findUserList: { content: [{ id: '1', userName: 'User A' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<UserSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('User A'));
        expect(mockSetValue).toHaveBeenCalledWith('users', [{ id: '1', userName: 'User A' }]);
    });

});
