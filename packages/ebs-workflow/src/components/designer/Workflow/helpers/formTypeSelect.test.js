import { render, fireEvent } from "@testing-library/react";
import FormTypeSelect from "./formTypeSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Form Type Select component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("Error in useQuery getting the Form Type data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<FormTypeSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<FormTypeSelect />)
        expect(getByText("Loading Types...")).toBeInTheDocument();
    });

    test('Renders Form Type list correctly', () => {
        const mockData = { findCFTypeList: { content: [{ id: '1', name: 'Form Type A' }, { id: '2', name: 'Form Type B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<FormTypeSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Form Type A')).toBeInTheDocument();
        expect(getByText('Form Type B')).toBeInTheDocument();
    });

    test('Set the selected node type', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findCFTypeList: { content: [{ id: '1', name: 'Form Type A'}]} };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<FormTypeSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Form Type A'));
        expect(mockSetValue).toHaveBeenCalledWith('attrType', { id: '1', name: 'Form Type A' });
    });

});
