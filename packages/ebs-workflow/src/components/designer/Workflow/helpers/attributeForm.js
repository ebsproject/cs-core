import React, { useState, useEffect, useContext } from 'react'
import { FormattedMessage } from "react-intl";
import { Styles, Core, EbsDialog, Icons } from "@ebs/styleguide";
import { Controller, useForm, FormProvider } from "react-hook-form";
import { ReactFlow, useNodes, useReactFlow  } from '@xyflow/react';
import CustomTextField from './texfield'
import FormTypeSelect from './formTypeSelect'
import NodeFieldSelect from './FieldsSelect'
import AttributeList from './attributeList';
import { v4 as uuidv4 } from 'uuid';
import JsonEditor from './jsonEditor';
import { NodeContext } from 'components/designer/context/node-context';
import { getSequenceSegments, getSgContext } from './functions';
import FormPreview from './formpreview';
const {
  styled
} = Styles

const {
  Add,
  Visibility,
  Edit
} = Icons
const {
  Grid,
  FormControlLabel,
  Checkbox,
  Box,
  Modal,
  Button,
  Dialog,
  DialogContent
} = Core;
const initialValues = {
  id: 0,
  mode: 'new',
  attrName: '',
  attrDescription: '',
  attrRequire: false,
  attrIsCustom: true,
  attrType: {
    id: 1,
    name: 'TextField'
  },
  attrNodeField:'',
  attrApiName:'',
  attrField: {}
}


const styles = {
  root: { marginTop: 20, width: "100%", padding: 10 },
  box: { borderRadius: 5, width: "100%", border: "1px dotted black" },
  legend: { fontWeight: "bold", marginLeft: 20, width: '100px' }
};



const FormAttributes = ({ name, label, otherProps, rules, ...props }) => {

  const formMethods = useForm({
    defaultValues: initialValues
  });


  const { register,
    handleSubmit,
    setValue,
    watch,
    reset,
    control,
    formState: { errors, defaultValues } } = formMethods;

  //reactflow
  const { node, setNode } = useContext(NodeContext);
  const { getNodes, setNodes } = useReactFlow();

  const [openAttributes, setOpenAttributes] = useState(false);
  const [openPreview, setOpenPreview] = useState(false);
  const handleClosePreview = () => setOpenPreview(!openPreview);
  const [edit, setEdit] = useState(false);
  const [fields, setFields] = useState(node.data.fields ? node.data.fields : []);

  //attr operation
  const [isSubmitSuccessful, setIsSubmitSuccessful] = useState(false);
  const [itemSelected, setItemSelected] = useState(0);
  const [itemSelectedForDeletion, setItemSelectedForDeletion] = useState(null);



  const [watchIsCustom ,setWatchIsCustom]= useState(true);
  const [options, setOptions]= useState([]);
  const [ruleName, setRuleName]= useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const result = await getSgContext();
      setOptions(result);
      const segments = await getSequenceSegments();
      setRuleName(segments);
    }
    fetchData();
  }, [])

  useEffect(() => {

    setNode({
      ...node,
      data: {
        ...node.data,
        fields: fields
      }
    })

  }, [fields])


  const attributesName = fields && fields.map(item => {
    let obj;
    if (item.attrApiName) {
      if (item.attrType.name.toLowerCase() === "select") {
        obj = { value: `${item.attrApiName}Id`, label: `${item.attrApiName}Id` }
      } else {
        obj = { value: `${item.attrApiName}`, label: `${item.attrApiName}` }
      }
    }else{
      obj = { value: `${item.attrName}`, label: `${item.attrName}` }
    }
    return obj;
  })

  //for reset
  useEffect(() => {
    if (isSubmitSuccessful) {
      reset(initialValues);
      setIsSubmitSuccessful(false);

    }
  }, [isSubmitSuccessful]);

  //for edition
  useEffect(() => {
    if (edit) {
      const fieldSelected = fields.find(field => field.id == itemSelected);

      reset(fieldSelected)

    }
  }, [itemSelected])


  //for deletion
  useEffect(() => {
    if (itemSelectedForDeletion) {
      const fieldFound = fields.find(field => field.id == itemSelectedForDeletion);
      fieldFound.mode = 'deleted';
      maintainFields(fieldFound);
    }
  }, [itemSelectedForDeletion])


  const maintainFields = (updateField) => {
    const fieldIndex = fields.findIndex(field => field.id === updateField.id);
    let newFieldArray = [];


    //add
    if (fieldIndex === -1 && (updateField.mode == 'new' || updateField.mode == 'added')) {
      newFieldArray = fields.slice();
      newFieldArray.push(updateField);
    } else { //edit
      const updatedField = { ...updateField };
      newFieldArray = [
        ...fields.slice(0, fieldIndex),
        updatedField,
        ...fields.slice(fieldIndex + 1)
      ]
      setFields(newFieldArray);
    }
    setFields(newFieldArray);


  }

  const handleOpen = () => {
    setOpenAttributes(true);
  };

  const handleClose = () => {
    setOpenAttributes(false);
  };


  const onSubmit = (data, event) => {

    if (edit) {
      data.mode = 'modified';
      maintainFields(data);
      setEdit(false)
    }

    if (!edit) {
      data.mode = 'added';
      maintainFields(data);
    }

    //reset states
    setItemSelected(0);
    setItemSelectedForDeletion(null);

    setIsSubmitSuccessful(true);
  }

  const onItemRemoved = (item) => {
    setItemSelectedForDeletion(item)
  }

  const onItemSelected = (item, e) => {

    if (!e.target.checked) {
      reset(initialValues);
      setEdit(false);
      if (itemSelected == item.id)
        setItemSelected(0);
      return;
    }

    if (!item.attrApiName)
      setWatchIsCustom(true);
    else setWatchIsCustom(false);
    if (itemSelected == item.id)
      setItemSelected(0);

    if (itemSelected != item.id)
      setItemSelected(item.id);
    setEdit(true);
  }
  const handleOpenPreview = () =>{
       setOpenPreview(true);
  }

if(options.length === 0)return<>Loading...</>
  return (
    <>
    <FormPreview open={openPreview} handleClose={handleClosePreview} fields={fields || []} />
        <FormProvider {...formMethods}>
      <Box id="fieldForm" component="form" onSubmit={handleSubmit(onSubmit)} >
        <legend >Fields</legend>
        <Grid  container spacing={3}  >


          <Grid container item xs={12} spacing={2} >
            <Grid item xs={6} >
              <Controller
                control={control}
                name="id"
                render={({ field }) => (
                  <input type='hidden' {...field} />
                )}
              />
              <input type="hidden" id='mode' {...register("mode")} />
              <CustomTextField name="attrName" label={"Name"} rules={{ required: "The Name is required" }} otherProps={{ fullWidth: true, size: "small" }} />
            </Grid>

            <Grid item xs={3}>
              <FormControlLabel control={
                <Controller
                  name="attrRequire"
                  control={control}
                  defaultValue={false}
                  rules={{ required: false }}
                  render={({ field: { value, ...field } }) => <Checkbox checked={!!value} {...field} />}
                />
              } label={"Is Required"} />

            </Grid>


            <Grid item xs={12} >
              <CustomTextField name="attrDescription" label={"Description"} rules={{ required: "The Description is required" }} otherProps={{ fullWidth: true, size: "small" }} />
            </Grid>
            <Grid item xs={3}>
              <FormControlLabel control={
                <Controller
                  name="attrIsCustom"
                  control={control}
                  defaultValue={true}
                  rules={{ required: false }}
                  render={({ field: { value, ...field } }) => <Checkbox checked={watchIsCustom} {...field} onChange={(e)=> setWatchIsCustom(e.target.checked)}/>}
                />
              } label={"Is a Custom Field"} />

            </Grid>
            {!watchIsCustom && (
              <>
                <Grid item xs={3}>
                  <NodeFieldSelect />
                </Grid>
                <Grid item xs={3} >
                  <CustomTextField name="attrApiName" label={"Api Attribute Name"} rules={{ required: false }} otherProps={{ fullWidth: true, size: "small" }} />
                </Grid>
              </>)
            }

            <Grid item xs={3} >
              <FormTypeSelect />
            </Grid>
            <Grid item xs={3} >
              <Button variant="text" onClick={handleOpen}>
                Setup Field Attributes
              </Button>
              <EbsDialog
                open={openAttributes}
                handleClose={handleClose}
                title={`Properties of the Attribute`}
              >
                <JsonEditor  {...register("attrField")} options={options} attributesName={attributesName} ruleName={ruleName} />
              </EbsDialog>
            </Grid>

          </Grid>
          <Grid container item xs={12} spacing={2} justifyContent="flex-end" >

            <Button
              size="small"
              startIcon={<Visibility />}
              onClick={handleOpenPreview}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={"Preview Form"} />
            </Button>

            <Button
              type="submit"
              form="fieldForm"
              size="small"
              startIcon={edit ? <Edit /> : <Add />}
              className="bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900"
            >
              <FormattedMessage id={"none"} defaultMessage={`${edit ? 'Save changes' : 'Add field'}`} />
            </Button>
          </Grid>


          <Grid container item xs={12} spacing={2} >
            <Grid item xs={12} >
              <AttributeList fields={fields} itemSelected={itemSelected} onItemRemoved={onItemRemoved} onItemSelected={onItemSelected} />
            </Grid>
          </Grid>
        </Grid>
      </Box>
    </FormProvider>
    </>


  )
}



export default FormAttributes