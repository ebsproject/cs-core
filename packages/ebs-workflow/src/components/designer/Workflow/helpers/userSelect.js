import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_USER_LIST } from "utils/apollo/gql/catalogs"

const {
  TextField,
  Checkbox
} = Core;

const { Autocomplete } = Lab;


const UserSelect = () => {
  const { control, setValue, formState: { errors } } = useFormContext();

  const { loading, error, data } = useQuery(FIND_USER_LIST);

  if (loading) return 'Loading User...';
  if (error) return `Error! ${error.message}`;

  let users = data.findUserList.content;

  const renderInput = (params) => (
    <TextField
      {...params}
      label={
        <>
          <FormattedMessage id={"none"} defaultMessage={"Link to Users"} />
          {` *`}{" "}
        </>
      }
      focused={Boolean(errors["users"])}
      error={Boolean(errors["users"])}
      helperText={errors["users"]?.message}
    />
  );

  const optionLabel = (option) => `${option.userName}`;

  const onChange = (e, options) => {
    e.preventDefault();
    setValue("users", options || []);
  };
  const renderOption = (props,option, { selected }) => {
    const { key, ...restProps  } = props;
    return (
      <li key={key} {...restProps}>
        <Checkbox checked={selected} />
        {option.userName}
      </li>
    );
  };

  return (
    <Controller
      control={control}
      name="users"
      render={({ field: { value, ...field } }) => (
        <Autocomplete
          multiple
          value={value || []}
          isOptionEqualToValue={(option, value) => option.userName === value.userName}
          options={users}
          onChange={onChange}
          getOptionLabel={optionLabel}
          renderInput={renderInput}
          renderOption={renderOption}
          disableCloseOnSelect
        />
      )}
    />

  )
}

export default UserSelect