import { Styles, Core } from "@ebs/styleguide";
import { Controller, useFormContext } from "react-hook-form";
const {
} = Styles

const {
  TextField
} = Core;

const CustomTextField = ({ name, label, otherProps, rules }) => {
  const { control, formState: { errors } } = useFormContext();
  return (
    <Controller
   
      name={name}
      control={control}
      rules={rules}
      render={({ field }) => <TextField data-testid={"TextFieldTestId"}
        InputLabelProps={{ shrink: true }}
        focused={Boolean(errors[name])}
        error={Boolean(errors[name])}
        helperText={errors[name]?.message}
        variant="outlined"  {...otherProps}
        label={label} {...field} />}
    />

  )
}



export default CustomTextField;