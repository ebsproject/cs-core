import React, { useState, useEffect } from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Styles, Core, Lab, Icons } from "@ebs/styleguide";

const {
  styled
} = Styles

const {
  Icon,
  TextField,
  Typography
} = Core;

const { Autocomplete } = Lab;

const styles = {
  root: { marginTop: 20, width: "100%" },
};


const FormIconSelect = ({ field }) => {
  const { control, setValue, formState: { errors } } = useFormContext();

  const [aryIcons, setAryIcons] = useState([]);
  const [inputValue, setInputValue] = React.useState('');


  // const iconList = Object.keys(Icons).map((icon) => ({
  //   name: icon,
  //   component: Icons[icon],
  // }));
  
  const iconList = Object.keys(Icons).map((icon) =>  icon);
  


  // useEffect(() => {
  //   let arrIcons = []
  //   Object.keys(Icons).forEach(item => {

  //     arrIcons = [...arrIcons, item]
  //   })
  //   setAryIcons(arrIcons);
  // }, [])


  const renderInput = (params) => {

    return (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Link to Icon"} />
          </>
        }
        focused={Boolean(errors["icon"])}
        error={Boolean(errors["icon"])}
        helperText={errors["icon"]?.message}
        inputProps={{
          ...params.inputProps,

        }}
      />
    )

  };



  const onInputChange = (e, value) => {
    setInputValue(value);
  };



  const optionLabelIcons = (option) => `${option}`;

  const onFormIconChange = (e, options) => {
    e.preventDefault();
    setValue("icon", options);

  };


  return (

    <Controller
      control={control}
      name="icon"
      rules={{ required: false }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
          fullWidth
          options={iconList || []}
          value={value || null}
          autoHighlight
          onChange={onFormIconChange}
          renderOption={(props, option) => (
            <li {...props}>
              <Icon fontSize="large">
                {_.lowerCase(option).replace(" ", "_")}
              </Icon>
              {option}
            </li>
          )}
          renderInput={(params) => <TextField {...params} label="Select Icon" />}
        />
      )}
    />

  )
}


export default FormIconSelect