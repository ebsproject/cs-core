import { render } from "@testing-library/react";
import CustomTextField from "./texfield";
import { useFormContext, Controller } from "react-hook-form";

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));


describe("Custom TextField component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("TextField Component with all props", () => {
        const name = "Some Name";
        const label = "Some Label";
        const otherProps = {};
        const rules = {};
        const { getByTestId } = render(<CustomTextField name={name} label={label} otherProps={otherProps} rules={rules} />);
        expect(getByTestId("TextFieldTestId")).toBeInTheDocument();
    });


});
