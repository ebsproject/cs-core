import { isEmpty, createDefinition, getSgContext, getSequenceSegments } from "./functions";
import { restClient } from "utils/axios/axios";

jest.mock("utils/apollo", () => ({
    client: {
      query: jest.fn(),
    },
  }));
  
  import { client as graphQLClient } from "utils/apollo";
  
  jest.mock('utils/axios/axios', () => ({
    restClient:{
        get: jest.fn()
    },
  }));

describe('isEmpty function', () => {
    test('should return true for null', () => {
        expect(isEmpty(null)).toBe(true);
    });

    test('should return true for undefined', () => {
        expect(isEmpty(undefined)).toBe(true);
    });

    test('should return true for an empty string', () => {
        expect(isEmpty('')).toBe(true);
    });

    test('should return false for a non-empty string', () => {
        expect(isEmpty('hello')).toBe(false);
    });

    test('should return true for an empty array', () => {
        expect(isEmpty([])).toBe(true);
    });

    test('should return false for a non-empty array', () => {
        expect(isEmpty([1, 2, 3])).toBe(false);
    });

    test('should return true for an empty object', () => {
        expect(isEmpty({})).toBe(true);
    });

    test('should return false for a non-empty object', () => {
        expect(isEmpty({ key: 'value' })).toBe(false);
    });

    test('should return false for numbers', () => {
        expect(isEmpty(0)).toBe(false);
        expect(isEmpty(123)).toBe(false);
    });

    test('should return false for boolean values', () => {
        expect(isEmpty(true)).toBe(false);
        expect(isEmpty(false)).toBe(false);
    });
});


describe("createDefinition function", () => {

    test(" return value is custom fields empty", () => {
        const fields = [];
        const result = createDefinition(fields);
        expect(result.components.length).toBe(0)
    })
    test(" return value is custom fields provided", () => {
        const fields = [
            {
                attrField: { disabled: true }
            },
            {
                attrField: { disabled: false },attrType:{name:"select"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            },
            {
                attrField: { disabled: false },attrType:{name:"textfield"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            },
            {
                attrField: { disabled: false },attrType:{name:"datepicker"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            },
            {
                attrField: { disabled: false },attrType:{name:"checkbox"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            },
            {
                attrField: { disabled: false },attrType:{name:"radiogroup"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            },
            {
                attrField: { disabled: false },attrType:{name:"switchq"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            },
            {
                attrField: { disabled: false },attrType:{name:"other"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: [{ label: "Option", value: 1 }], inputProps:{label:"label test"}}, attrName:"test name"
            }
        ];
        const result = createDefinition(fields);
        expect(result.components.length).toBeGreaterThan(0)
    })
    test(" return value is custom fields provided and no defaultOptions", () => {
        const fields = [
            {
                attrField: { disabled: false },attrType:{name:"select"}, attrField:{sort:1, sizes:[4,6,6,6,6], defaultOptions: null, inputProps:{label:"label test"}}, attrName:"test name"
            },
        ];
        const result = createDefinition(fields);
        expect(result.components.length).toBeGreaterThan(0)
    })

})

describe("getSgContext function", ()=>{
    test("call function", async ()=>{
        graphQLClient.query.mockResolvedValue({
            data: {
              findDomainList: {
                content: [
                  { prefix: 'cs', name: 'Domain 1' },
                  { prefix: 'cb', name: 'Domain 2' },
                ],
              },
            },
          });
          const result = await getSgContext();
          expect(result).toEqual([
            { value: 'cs', label: 'Domain 1' },
            { value: 'cb', label: 'Domain 2' },
          ]);

    });
})

describe("getSegmentSequence function", ()=>{
    test('should return an empty array if there is an error', async () => {
        restClient.get.mockRejectedValue(new Error('Network error'));
        const result = await getSequenceSegments();
        expect(result).toEqual([]);
      });
      test('should return data', async () => {
        restClient.get.mockResolvedValue({data:{segments:[{ name:"Test Name"}]}});
        const result = await getSequenceSegments();
        expect(result.length).toBeGreaterThan(0);
        expect(result[0]).toEqual({label: 'Test Name', value: 'Test Name'})
      });
})

