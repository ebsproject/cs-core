import { render, fireEvent } from "@testing-library/react";
import NodeTypeSelect from "./NodeTypeSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Process Select component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("Error in useQuery getting the Node Type data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<NodeTypeSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<NodeTypeSelect />)
        expect(getByText("Loading Types...")).toBeInTheDocument();
    });

    test('Renders Node Type list correctly', () => {
        const mockData = { findNodeTypeList: { content: [{ id: '1', name: 'Node Type A' }, { id: '2', name: 'Node Type B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<NodeTypeSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Node Type A')).toBeInTheDocument();
        expect(getByText('Node Type B')).toBeInTheDocument();
    });

    test('Set the selected node type', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findNodeTypeList: { content: [{ id: '1', name: 'Node Type A'}]} };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<NodeTypeSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Node Type A'));
        expect(mockSetValue).toHaveBeenCalledWith('nodeType', { id: '1', name: 'Node Type A' });
    });

});
