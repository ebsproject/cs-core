import React from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Core, Lab } from "@ebs/styleguide";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_FORMTYPE_LIST } from "utils/apollo/gql/catalogs"

const {
  TextField,
} = Core;

const { Autocomplete } = Lab;

const FormTypeSelect = ( ) => {
  const { control, setValue, formState: { errors } } = useFormContext();
  const { loading, error, data } = useQuery(FIND_FORMTYPE_LIST);

  if (loading) return 'Loading Types...';
  if (error) return `Error! ${error.message}`;

  let formTypes = data.findCFTypeList.content;

  const renderInput = (params) => {

    return (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Link to Types"} />
            {` *`}{" "}
          </>
        }
        focused={Boolean(errors["attrType"])}
        error={Boolean(errors["attrType"])}
        helperText={errors["attrType"]?.message}
        inputProps={{
          ...params.inputProps,

        }}
      />
    )
  };

  const optionLabelReports = (option) => `${option.name}`;

  const onFormTypeChange = (e, options) => {
    e.preventDefault();
    setValue("attrType", options);
  };

  return (
    <Controller
      control={control}
      name="attrType"
      rules={{ required: "Required" }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
        isOptionEqualToValue={(option, value) => option.name === value.name}
          options={formTypes}
          value={value || null}
          onChange={onFormTypeChange}
          getOptionLabel={optionLabelReports}
          renderInput={renderInput}
        />
      )}
    />

  )
}


export default FormTypeSelect