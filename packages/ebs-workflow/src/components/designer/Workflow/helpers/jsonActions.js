import { transform } from "node-json-transform";
import { v4 as uuidv4 } from 'uuid';

export const mutationWorkflow = {
  item: {
    title: "title",
    name: "name",
    description: "description",
    help: "description",
    sortNo: "sort",
    api: "products.domain.domaininstances.0.sgContext",
    navigationName: "title",
    productId: "products.id",
    sequence: "sort",
    showMenu: "showMenu",
    system: "isSystem",
    securityDefinition: {
      programs: "programs",
      users: "users",
      roles: "roles"
    }
  },
  each: function (item, index, collection) {
    item.id = 0,
      item.icon = "na",
      item.htmlTagId = 1,
      item.navigationIcon = "na"
  }
};


export const mutationPhase = {
  item: {
    name: "data.name",
    description: "data.description",
    help: "data.description",
    sequence: "data.sequence",
    workflowId: "data.workflowId",
    workflowViewTypeId: "data.nodeViewType.id",
    designRef: "designRef",
    icon: "data.icon",
    dependOn: {
      size:"measured",
      position: "position",
      sourcePosition: "sourcePosition",
      width: "width",
      height: "height",
      edges: "edge"
    }
  },
  each: function (item, index, collection) {
    item.id = 0,
      item.htmlTagId = 1,
      item.tenantId = 1
  }
};



export const mutationStage = {
  item: {
    name: "data.name",
    description: "data.description",
    help: "data.description",
    sequence: "data.sequence",
    phaseId: "data.phaseId",
    designRef: "designRef",
    icon: "data.icon",
    workflowViewTypeId: "data.nodeViewType.id",
    dependOn: {
      size:"measured",
      position: "position",
      sourcePosition: "sourcePosition",
      width: "width",
      height: "height",
      edges: "edge"
    }
  },
  each: function (item, index, collection) {
    item.id = 0,
      item.htmlTagId = 1,
      item.tenantId = 1
      
  }
};

export const mutationStageRelationsNode = {
  item: {
    nodeIds: "nodes",
    designRef: "designRef"
  },
  each: function (item, index, collection) {
    item.id = 0,
      item.htmlTagId = 1,
      item.tenantId = 1
  }, operate: [
    {
      run: function (val) { return Number(val) }, on: "id"
    }
  ]
};


export const mutationNode = {
  item: {
    name: "data.name",
    description: "data.description",
    help: "data.description",
    processId: "data.process.id",
    define: "data.define",
    icon: "data.icon",
    designRef: "designRef",
    sequence: "data.sequence",
    nodeTypeId: "data.nodeType.id",
    dependOn: {
      size:"measured",
      position: "position",
      sourcePosition: "sourcePosition",
      width: "width",
      height: "height",
      edges: "edge"
    },
    securityDefinition: {
      programs: "data.programs",
      users: "data.users",
      roles: "data.roles"
    },
    workflowViewTypeId: "data.nodeViewType.id",
    workflowId: "data.workflowId",
  },
  each: function (item, index, collection) {
    item.id = 0,
      item.htmlTagId = 1,
      item.tenantId = 1,
      item.requireApproval = false,
      item.validationType = "javascript",
      item.requireInput = {},
      item.message = {}
  }, operate: [
    {
      run: function (val) { return Number(val) }, on: "workflowViewTypeId"
    },
    {
      run: function (val) { return Number(val) }, on: "processId"
    }
  ]
};


export const designPhase = {
  item: {
    designRef:'designRef',
    id: 'designRef',
    dbId: 'id',
    edge: "dependOn.edges",
    data: {
      dependOn: "dependOn",
      name: 'name',
      icon: "icon",
      description: 'description',
      workflowId: "workflow.id",
      nodeViewType: {
        id: "workflowViewType.id",
        name: "workflowViewType.name"
      },
      sequence: "sequence",
      mode: 'initial'
    },
    position: "dependOn.position",
    sourcePosition: "dependOn.sourcePosition",
  },
  each: function (item, index, collection) {
    item.selected = false,
      item.type = "phase"
  },
  operate: [
    {
      run: function (val) { return String(val) }, on: "id"
    }
  ]
}


export const designStage = {
  item: {
    designRef:'designRef',
    id: 'designRef',
    dbId: 'id',
    edge: "dependOn.edges",
    data: {
      dependOn: "dependOn",
      name: 'name',
      description: 'description',
      icon: "icon",
      nodeViewType: {
        id: "workflowViewType.id",
        name: "workflowViewType.name"
      },
      sequence: "sequence",
      mode: 'initial'
    },
    position: "dependOn.position",
    sourcePosition: "dependOn.sourcePosition",
    parentId: 'phase.designRef'
  },
  each: function (item, index, collection) {
    item.selected = false,
      item.type = "stage",
      item.extent = 'parent'
  },
  operate: [
    {
      run: function (val) { return String(val) }, on: "id"
    },
    {
      run: function (val) { return String(val) }, on: "parentId"
    }
  ]
}




export const designNode = {
  item: {
    designRef:'designRef',
    id: 'designRef',
    dbId: 'id',
    edge: "dependOn.edges",
    data: {
      name: 'name',
      icon: "icon",
      description: 'description',
      nodeViewType: {
        id: "workflowViewType.id",
        name: "workflowViewType.name"
      },
      nodeType: "nodeType",
      process: "process",
      roles: "securityDefinition.roles",
      users: "securityDefinition.users",
      programs: "securityDefinition.programs",
      sequence: "sequence",
      define: "define",
      dependOn: "dependOn",
      mode: 'initial',
      fields: "nodeCFs",
    },
    position: "dependOn.position",
    sourcePosition: "dependOn.sourcePosition",
    parentId: 'stageId'
  },
  each: function (item, index, collection) {
    item.selected = false,
      item.type = "process",
      item.extent = 'parent'
  },
  operate: [
    {
      run: function (val) { return String(val) }, on: "id"
    },
    {
      run: function (val) { return String(val) }, on: "parentId"
    },
    {
      run: function (ary) {
        return transform(ary, nestedCustomField);
      },
      on: "data.fields"
    }
  ]
}

var nestedCustomField = {
  item: {
    id: "id",
    attrName: "name",
    attrRequire: "required",
    attrDescription: "description",
    attrNodeField: "attributes",
    attrApiName: "apiAttributesName",
    attrType: "cfType",
    attrField: "fieldAttributes"

  }
};


export const mutationFormFields = {
  item: {
    name: "attrName",
    description: "attrDescription",
    help: "attrDescription",
    required: "attrRequire",
    fieldAttributes: "attrField",
    attributesId: "attrNodeField.id",
    apiAttributesName: "attrApiName",
    cfTypeId: "attrType.id",
    nodeId: "nodeId"
  },
  each: function (item, index, collection) {
    item.id = 0,
      item.htmlTagId = 1,
      item.tenantId = 1
  }

}

export const mapObject = {
  item: {
    value: "id",
    label: "name"
  }
}



export const edgesDefinition = {
  item: {
    id: "designRef",
    sources: "dependOn.edges"
  },
}

// const nestedMap = {
//   item: {
//     "source":
//   },
//   each: function (item, index, collection, context) {
//     return item
//   }
// };