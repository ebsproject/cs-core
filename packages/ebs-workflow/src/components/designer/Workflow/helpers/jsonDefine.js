import React, { useState, useEffect, useContext } from 'react'
import JsonEditor from 'react-json-editor-ui'
import 'react-json-editor-ui/dist/react-json-editor-ui.cjs.development.css'
import { WorkflowContext } from "components/designer/context/workflow-context";
import { Styles, Core } from "@ebs/styleguide";
import { useFormContext } from "react-hook-form";
import { nodeDefinition } from './jsonProps'
import { mapObject as mapObjectBase } from './jsonActions';
import { transform } from "node-json-transform";
import { client as graphQLClient } from 'utils/apollo';
import { FIND_TEMPLATE_LIST, FIND_EMAIL_LIST, FIND_STATUS_LIST, FIND_NODE_LIST,FIND_CONTACT_LIST } from "utils/apollo/gql/catalogs"
import { NodeContext } from 'components/designer/context/node-context';
import validateFunctions from 'validations';
import { po_client } from 'utils/axios';
import { addNameAttributeToNodes, getSgContext } from './functions';

const {
  styled
} = Styles

const {
  TextField
} = Core;


const styles = {
  root: { marginTop: 20, width: "100%" },
};
const getAllCF = (nodes) =>{
  let cfs =[];
  let formNodes = nodes.filter(a => a.process !== null).filter(item => item.process.code === "FORM")
  formNodes.forEach(n =>{
    n.nodeCFs.forEach(cf =>{
      cfs.push({label:cf.name,value:cf.name})
    })
  })
if(cfs.length === 0)
   cfs=[{label:"NoCustomFieldsError", value:"NoCustomFieldsError"}]
return cfs;

}
const functionsJS =  Object.keys(validateFunctions);
const optionMaps = async process => {
 const {data : statusType} = await graphQLClient.query({
    query: FIND_STATUS_LIST,
    variables: {
      filters: [{
        col: "workflow.id",
        mod: "EQ",
        val: process.workflowId
      }]
    }
  });
let functionObject = functionsJS.map(item=>{
  return {label:item, value:item}
})
  let response;
  const { data } = await graphQLClient.query({
    query: FIND_NODE_LIST,
    variables: {
      filters: [{
        col: "workflow.id",
        mod: "EQ",
        val: process.workflowId
      }],
      page:{size:300,number:1}
    }
  })
  const nodes = data.findNodeList.content;
  const NodeTransformation = transform(nodes, mapObjectBase);
  const cfArray = getAllCF(nodes);
  switch (process.id) {
    case "3": //print 
      const {data: _templates } = await po_client.get(`/api/FileManager/GetSystemTemplateList`);
      const templates =  _templates.result.data.filter(item => item.documentName !== "sys_connection")
      const transformation = transform(templates,{item: {value: "documentName",label: "documentName"}});
      response = {
        template: transformation,
        executeNode:NodeTransformation,
        validationFunction:functionObject,
        columnName:cfArray
      };
      break;
    case "4": //email
    const {data: _temp } = await po_client.get(`/api/FileManager/GetSystemTemplateList`);
    const _temps =  _temp.result.data.filter(item => item.documentName !== "sys_connection")
    const _transformation = transform(_temps,{item: {value: "documentName",label: "documentName"}});
    const { data } = await graphQLClient.query({
      query: FIND_CONTACT_LIST,
      variables: {
        filters:[{col:"category.name", mod:"EQ", val:"Person"}],
        page:{size:300,number:1}
      }
    });
    let emails =[];
    data.findContactList.content.map(d => {
   emails = [...emails,{label:d.person.fullName, value:d.email}]
  })
      await graphQLClient.query({
        query: FIND_EMAIL_LIST
      }).then(r => {
        const templates = r.data.findEmailTemplateList.content;
        const transformation = transform(templates, mapObjectBase);
        response = {
          template:_transformation,
          contact:emails,
          format:[{value:'pdf',label:'pdf'},{value:'csv', label:'csv'}],
          email: transformation,
          executeNode:NodeTransformation
        };
      }

      );
      break;
    case "5": //status
      await graphQLClient.query({
        query: FIND_STATUS_LIST,
        variables: {
          filters: [{
            col: "workflow.id",
            mod: "EQ",
            val: process.workflowId

          }]
        }
      }).then(r => {
        const templates = r.data.findStatusTypeList.content;
        const transformation = transform(templates, mapObjectBase);
        response = {
          status: transformation,
          actor:[{value:"Sender", label:"Sender"},{value:"Requestor", label:"Requestor"},{value:"Recipient", label:"Recipient"},{value:"SHU", label:"SHU"},],
          executeNode:NodeTransformation
        };
      }

      );
      break;
    case "8": //external components
      await import('custom-components')
        .then((registry) => {
          let comp = []
          Object.keys(registry).forEach((item) => {
            comp = [...comp, { value: item, label: item }]
          })
          let status = statusType.findStatusTypeList.content;
          const transformation = transform(status, {
            item: {
              value: "name",
              label: "name"
            }
          }
          );
          response = {
            status:transformation,
            component: comp,
            executeNode:NodeTransformation
          };

        })
      break;
      case "9": //modal popup
      await graphQLClient.query({
        query: FIND_NODE_LIST,
        variables: {
          filters: [{col: "workflow.id", mod: "EQ",val: process.workflowId },
          {col: "process.code", mod: "EQ",val: "FORM" },
        
        ],
          page:{size:300,number:1}
        }
      }).then(r => {
        const nodes = r.data.findNodeList.content;
        const transformation = transform(nodes, mapObjectBase);
        response = {
          form: transformation,
          executeNode:NodeTransformation
        };
      })
 
      break;
      case "10": //api call type
      const result = await getSgContext();
        response = {
          sgcontext: result,
          executeNode:NodeTransformation,
          method:[{label:"post",value:"post"},{label:"get", value:"get"}],
          bodyType:[{label:"none",value:"none"},{label:"form-data",value:"form-data"},{label:"binary",value:"binary"}],
          field:cfArray
        };
   
      break;
      case "11": //conditional node
        response = {
          executeNodeIfTrue: NodeTransformation,
          executeNodeIfFalse: NodeTransformation,
          executeNode:NodeTransformation,
          field:cfArray
        };
   
      break;
    default:
    response = {
      executeNode: NodeTransformation,
      node: NodeTransformation
    };
      break;
  }
  response = {...response,...{functions:functionObject}}
  return response;

}

const JsonDefine = ({ }) => {
  const { watch, setValue } = useFormContext();
  const watchNodeType = watch("process");
  const watchDefine = watch("define");

  const { workflow } = useContext(WorkflowContext);
  const { node, setNode } = useContext(NodeContext);
  const [statusType, setStatusType] = useState([])
  const [emailTemplates, setEmailTemplates] = useState([])

  const [editObject, setEditObject] = useState(null);
  const [mapObject, setMapObject] = useState({});

  function addMissingKeys(source, target) {
    for (const key in source) {
        if (source.hasOwnProperty(key) && !target.hasOwnProperty(key)) {
            target[key] = source[key];
        }
    }
    return target;
}
useEffect(() => {
  const getStatusType = async () => {
    try {
      const { data } = await graphQLClient.query({
        query: FIND_STATUS_LIST,
        variables: {
          filters: [{
            col: "workflow.id",
            mod: "EQ",
            val: workflow?.data?.id || 0
          }]
        }
      });
      setStatusType(data.findStatusTypeList.content)
    } catch (error) {
      console.error(error)
    }
  }
  getStatusType();
},[]);
useEffect(()=>{
const getTemplates = async () =>{
  try {
    const {data} = await graphQLClient.query({
      query: FIND_EMAIL_LIST
    });
    setEmailTemplates(data.findEmailTemplateList.content);
  } catch (error) {
    console.log(error);
  }
}
getTemplates();
},[]);
  useEffect(() => {

    if (watchNodeType) {
      let jsonObject = nodeDefinition.find(prop => prop.id == Number(watchNodeType.id));
      if (jsonObject) {
        watchNodeType.workflowId = workflow.data.id;
        optionMaps(watchNodeType).then((templates) => {
          setMapObject(templates)
          if (node.data.define) {
            if (Number(node.data.define.id) == Number(watchNodeType.id)) {
              let define = Object.assign({},node.data.define);
              const result = addMissingKeys(jsonObject, define);
              setEditObject(result);
            } else {
              setEditObject(jsonObject);
            }
          } else {
            setEditObject(jsonObject);
          }


        });

      } else {
        setEditObject(null)
      }
    }
  }, [watchNodeType, watch])



  useEffect(() => {
    setValue("define", editObject)
  }, [editObject])


  if (editObject == null)
    return <p>Don't exist specification</p>;
  const handleChangeData = (data) => {
    if (watchNodeType.id == 5) {
      let name = ''
      let statusId = Number(data.status)
      let status = statusType.find(item => Number(item.id) === statusId)
      if (status) {
        name = status['name'];
      }
      data = { ...data, statusName: name }
    }
    if (watchNodeType.id == 6) {
      let name = '';
      let wf = { ...workflow.data }
      wf.phases.forEach(p => {
        p.stages.forEach(s => {
          s.nodes.forEach(n => {
            if (Number(n.id) == Number(data.node)) {
              name = n['name'];
            }
          })
        })
      });
      data = { ...data, nodeName: name }
    }
    if (watchNodeType.id == 9) {
      let name = '';
      let wf = { ...workflow.data }
      wf.phases.forEach(p => {
        p.stages.forEach(s => {
          s.nodes.forEach(n => {
            if (Number(n.id) == Number(data.form)) {
              name = n['name'];
            }
          })
        })
      });
      data = { ...data, formName: name }
    }
    if (watchNodeType.id == 11) {
      let wf = { ...workflow.data }
      let nodeIfFalse= '';
      let nodeIfTrue= '';
      wf.phases.forEach(p => {
        p.stages.forEach(s => {
          s.nodes.forEach(n => {
            if (Number(n.id) == Number(data.executeNodeIfTrue)) {
              nodeIfTrue = n['name'];
            }
            if (Number(n.id) == Number(data.executeNodeIfFalse)) {
              nodeIfFalse = n['name'];
            }
          })
        })
      });
      data = { ...data, executeNodeIfTrueName: nodeIfTrue, executeNodeIfFalseName: nodeIfFalse  }
    }
    if (watchNodeType.id == 4) {
      let email = '';
      let selectedEmail = emailTemplates.find( item => Number(item.id) == Number(data.email))
      console.log(selectedEmail)
      if(selectedEmail){
        email = selectedEmail['name']
      }
      data = { ...data, emailName: email }
    }
    setEditObject(data);
  }
  return (
    <JsonEditor
      data={editObject}
      onChange={handleChangeData}
      optionsMap={mapObject}
    />

  )
}



export default JsonDefine