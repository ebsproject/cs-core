import { render, fireEvent } from "@testing-library/react";
import ProcessSelect from "./processSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Process Select component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("Error in useQuery getting the Process data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<ProcessSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<ProcessSelect />)
        expect(getByText("Loading Process...")).toBeInTheDocument();
    });

    test('Renders Process list correctly', () => {
        const mockData = { findProcessList: { content: [{ id: '1', name: 'Process A' }, { id: '2', name: 'Process B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ProcessSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Process A')).toBeInTheDocument();
        expect(getByText('Process B')).toBeInTheDocument();
    });

    test('Set the selected process', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findProcessList: { content: [{ id: '1', name: 'Process A'}]} };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ProcessSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Process A'));
        expect(mockSetValue).toHaveBeenCalledWith('process', { id: '1', name: 'Process A' });
    });

});
