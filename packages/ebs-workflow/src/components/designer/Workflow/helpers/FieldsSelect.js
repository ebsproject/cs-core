import React, { useState, useContext } from 'react';
import { useFormContext, Controller } from "react-hook-form";
import { FormattedMessage } from "react-intl";
import { Styles, Core, Lab } from "@ebs/styleguide";
import { WorkflowContext } from "components/designer/context/workflow-context";
//APOLLO
import { useQuery } from '@apollo/client';
import { FIND_PRODUCT_BY_ID } from "utils/apollo/gql/catalogs"
const {
  styled
} = Styles

const {

  TextField,

} = Core;

const { Autocomplete } = Lab;



const styles = {
  root: { marginTop: 20, width: "100%" },
};


const NodeTypeSelect = () => {
  const { control, setValue, formState: { errors } } = useFormContext();
  const { workflow } = useContext(WorkflowContext);

  const { loading, error, data } = useQuery(FIND_PRODUCT_BY_ID, {
    variables: { id: workflow.data.product.id }
  });

  if (loading) return 'Loading Fields...';
  if (error) return `Error! ${error.message}`;

  let fieldsList = data.findProduct.entityReference.attributess;

  const renderInput = (params) => {

    return (
      <TextField
        {...params}
        label={
          <>
            <FormattedMessage id={"none"} defaultMessage={"Link to Field"} />
            {` *`}{" "}
          </>
        }
        focused={Boolean(errors["attrNodeField"])}
        error={Boolean(errors["attrNodeField"])}
        helperText={errors["attrNodeField"]?.message}
        inputProps={{
          ...params.inputProps,

        }}
      />
    )

  };

  const optionLabelReports = (option) => `${option.name}`;

  const onFormTypeChange = (e, options) => {
    e.preventDefault();
    setValue("attrNodeField", options);
  };

  return (

    <Controller
      control={control}
      name="attrNodeField"
      rules={{ required: "Required" }}
      render={({ field: { value, ...field } }) => (
        <Autocomplete
          isOptionEqualToValue={(option, value) => option.name === value.name}
          options={fieldsList}
          value={value || null}
          onChange={onFormTypeChange}
          getOptionLabel={optionLabelReports}
          renderInput={renderInput}
        />
      )}
    />

  )
}


export default NodeTypeSelect