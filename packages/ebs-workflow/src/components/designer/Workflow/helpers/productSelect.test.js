import { render, fireEvent } from "@testing-library/react";
import ProductSelect from "./productSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Product Select component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("Error in useQuery getting the products data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<ProductSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<ProductSelect />)
        expect(getByText("Loading Products...")).toBeInTheDocument();
    });

    test('Renders products list correctly', () => {
        const mockData = { findProductList: { content: [{ id: '1', name: 'Product A',hasWorkflow:true, domain:{name:"CS"} }, { id: '2', name: 'Product B',hasWorkflow:true,domain:{name:"CS"} }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ProductSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Product A')).toBeInTheDocument();
        expect(getByText('Product B')).toBeInTheDocument();
    });

    test('Set the selected product', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findProductList: { content: [{ id: '1', name: 'Product A', hasWorkflow:true, domain:{name:"CS"} }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ProductSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Product A'));
        expect(mockSetValue).toHaveBeenCalledWith('products', { id: '1', name: 'Product A', hasWorkflow:true, domain:{name:"CS"}  });
    });

});
