import { render, fireEvent } from "@testing-library/react";
import RoleSelect from "./roleSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("Role Select component", () => {
  
    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() },
            watch: jest.fn((field) => {
                if (field === 'roles') {
                  return ['Admin', 'User'];
                }
                return null;
              }),
        });
    });


    test("Error in the Query", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<RoleSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<RoleSelect />)
        expect(getByText("Loading Roles...")).toBeInTheDocument();
    });

    test('renders roles list correctly', () => {
        const mockData = { findRoleList: { content: [{ id: '1', name: 'Role A' }, { id: '2', name: 'Role B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<RoleSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Role A')).toBeInTheDocument();
        expect(getByText('Role B')).toBeInTheDocument();
    });

    test('set the selected program', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
            watch: jest.fn((field) => {
                if (field === 'roles') {
                  return ['Admin', 'User'];
                }
                return null;
              }),
        });

        const mockData = { findRoleList: { content: [{ id: '1', name: 'Role A' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<RoleSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Role A'));
        expect(mockSetValue).toHaveBeenCalledWith('roles', [{ id: '1', name: 'Role A' }]);
    });

});
