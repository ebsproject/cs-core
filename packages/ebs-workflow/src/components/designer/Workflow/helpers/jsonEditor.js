import React, { useState, useEffect } from 'react'
import JsonEditor from 'react-json-editor-ui'
import 'react-json-editor-ui/dist/react-json-editor-ui.cjs.development.css'
import { FormattedMessage } from "react-intl";
import { Styles, Core } from "@ebs/styleguide";
import { useFormContext } from "react-hook-form";
import { formDefinition } from './jsonProps'
import { client as graphQLClient } from 'utils/apollo';
import { FIND_NODE_LIST } from 'utils/apollo/gql/catalogs';
import "./editor.css"

const {
  styled
} = Styles

const {
  TextField
} = Core;


const styles = {
  root: { marginTop: 20, width: "100%" },
};


const CustomTextField = ({ name, label, otherProps, rules,options,attributesName, ruleName }) => {
  const { watch, value, setValue } = useFormContext();
  const watchAttrType = watch("attrType");
  const watchAttrField = watch("attrField");
  const [editObject, setEditObject] = useState(null);
  const [customComponents, setCustomComponents] = useState([])


  useEffect(() => {

    let findProp = formDefinition.find(prop => prop.id == watchAttrType.id);
    if ("id" in watchAttrField) {
      setEditObject(watchAttrField)
    } else {
      setEditObject(findProp ? findProp : {})
    }

  }, [])

  useEffect(() => {
    setValue("attrField", editObject)
  }, [editObject])

  useEffect(() => {

    const fetchComponents = async () => {
      let comp = []
      const { data } = await graphQLClient.query({
        query: FIND_NODE_LIST,
        variables: {
          filters: [{
            col: "process.code",
            mod: "EQ",
            val: "FORM"
          }],
          page:{size:300,number:1}
        }
      });
      data.findNodeList.content.map(d => {
     comp =[...comp,{label:d.name, value:d.name}]
     })
      await import('custom-components')
        .then((registry) => {
       
          Object.keys(registry).forEach((item) => {
            comp = [...comp, { value: item, label: item }]
          })
          setCustomComponents(comp);
        })
    }
    fetchComponents();
  }, [])


  if (editObject == null)
    return <p>Something went wrong</p>;

  return (
    <div className='json-editor-container'>
    <JsonEditor
      // {...register('jsonObject')}
      data={editObject}
      onChange={data => {
        setEditObject(data);
      }}
      optionsMap={{uri:options,parentControl:attributesName,ruleName:ruleName,componentUI:customComponents}}
    />
    </div>


  )
}



export default CustomTextField