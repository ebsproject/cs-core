import { render, fireEvent } from "@testing-library/react";
import FieldSelect from "./FieldsSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';
import { WorkflowContext } from "components/designer/context/workflow-context";

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("ViewType Select component", () => {
    const mockWorkflowData = {
        data: {
          product: { id: '123' },
        },
      };
    
    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });
    const renderWithWorkflowContext = (ui, { workflowValue } = {}) => {
        return render(
          <WorkflowContext.Provider value={{ workflow: workflowValue }}>
            {ui}
          </WorkflowContext.Provider>
        );
      };

    test("Error in useQuery getting the Field data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = renderWithWorkflowContext(<FieldSelect />, { workflowValue: mockWorkflowData })
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = renderWithWorkflowContext(<FieldSelect />, { workflowValue: mockWorkflowData })
        expect(getByText("Loading Fields...")).toBeInTheDocument();
    });

    test('Renders Field list correctly', () => {
        const mockData = { findProduct:{entityReference:{attributess: [{ id: '1', name: 'Field A'}, { id: '2', name: 'Field B'}]}} };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = renderWithWorkflowContext(<FieldSelect />, { workflowValue: mockWorkflowData })
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('Field A')).toBeInTheDocument();
        expect(getByText('Field B')).toBeInTheDocument();
    });

    test('Set the selected view type', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findProduct:{entityReference:{attributess: [{ id: '1', name: 'Field A'}]}} };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = renderWithWorkflowContext(<FieldSelect />, { workflowValue: mockWorkflowData })
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('Field A'));
        expect(mockSetValue).toHaveBeenCalledWith('attrNodeField', { id: '1', name: 'Field A' });
    });

});
