import { render, fireEvent } from "@testing-library/react";
import ViewTypeSelect from "./viewTypeSelect";
import { useFormContext, Controller } from "react-hook-form";
import { useQuery } from '@apollo/client';

jest.mock("react-hook-form", () => ({
    useFormContext: jest.fn(),
    Controller: ({ render, ...props }) => {
        return render({
            field: {
                value: props.value || [],
                onChange: jest.fn(),
                onBlur: jest.fn(),
                name: props.name,
                ref: jest.fn(),
            },
        });
    },
}));
jest.mock('@apollo/client', () => ({
    ...jest.requireActual('@apollo/client'),
    useQuery: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("ViewType Select component", () => {

    beforeEach(() => {
        useFormContext.mockReturnValue({
            control: jest.fn(),
            setValue: jest.fn(),
            register: jest.fn(),
            formState: { errors: jest.fn() }
        });
    });


    test("Error in useQuery getting the ViewType data", () => {
        useQuery.mockReturnValue({ loading: false, error: true, data: undefined });
        const { getByText } = render(<ViewTypeSelect />)
        expect(getByText(/error/i)).toBeInTheDocument();
    });

    test("Loading when data is not available", () => {
        useQuery.mockReturnValue({ loading: true, error: undefined, data: undefined });
        const { getByText } = render(<ViewTypeSelect />)
        expect(getByText("Loading View Types...")).toBeInTheDocument();
    });

    test('Renders ViewType list correctly', () => {
        const mockData = { findWorkflowViewTypeList: { content: [{ id: '1', name: 'ViewType A' }, { id: '2', name: 'ViewType B' }] } };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ViewTypeSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        expect(getByText('ViewType A')).toBeInTheDocument();
        expect(getByText('ViewType B')).toBeInTheDocument();
    });

    test('Set the selected view type', () => {
        const mockSetValue = jest.fn();
        useFormContext.mockReturnValue({
            control: {},
            setValue: mockSetValue,
            formState: { errors: {} },
        });
        const mockData = { findWorkflowViewTypeList: { content: [{ id: '1', name: 'ViewType A'}]} };
        useQuery.mockReturnValue({ loading: false, error: undefined, data: mockData });
        const { getByRole, getByText } = render(<ViewTypeSelect />);
        const input = getByRole('combobox');
        fireEvent.mouseDown(input);
        fireEvent.click(getByText('ViewType A'));
        expect(mockSetValue).toHaveBeenCalledWith('nodeViewType', { id: '1', name: 'ViewType A' });
    });

});
