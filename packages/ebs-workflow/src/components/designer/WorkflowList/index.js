import React from "react";
import { EbsGrid } from "@ebs/components";
import { getCoreSystemContext } from "@ebs/layout";
const { graphqlUri } = getCoreSystemContext();
import { columns } from "./settings";
import RowActions from "./RowActions";
import Toolbar from "./Toolbar";

export default function Index() {
  return (
    <div data-testid={"WorkflowListTestId"}>
      <EbsGrid
        id="workflow"
        toolbar={true}
        toolbaractions={Toolbar}
        columns={columns}
        entity="Workflow"
        // eslint-disable-next-line no-undef
        uri={graphqlUri}
        rowactions={RowActions}
        csvfilename="workflowList"
        callstandard="graphql"
        height="85vh"
        raWidth={150}
        select="single"
      />
    </div>
  );
}
