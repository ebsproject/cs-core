import React, { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Box, Button, Tooltip, Typography } = Core;
import { useNavigate } from "react-router-dom";
import { WorkflowContext } from "components/designer/context/workflow-context";
import ImportExportButton from "./ImportExportButton";

const Toolbar = (selectedRows, refresh) => {
  const { setWorkflow } = useContext(WorkflowContext);
  const navigate = useNavigate();  
  const handleGo = () => {
    setWorkflow({
      action: "add",
      data: { id: 0 },
    });

    navigate(`/wf/designer/add`);
  };
  return (
    <div className="-mr-100">
      <Box
        component="div"
        data-testid={"RowActionsReportsTestId"}
        className="-ml-4 flex flex-auto"
      >
        <Tooltip
          arrow
          placement="top"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage
                id={"none"}
                defaultMessage={"Add New Workflow"}
              />
            </Typography>
          }
        >
          <Button
            // startIcon={<PostAdd />}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
            onClick={handleGo}
          >
            <Typography className="font-ebs">
              <FormattedMessage id="none" defaultMessage={"New Workflow"} />
            </Typography>
          </Button>
        </Tooltip>
        <ImportExportButton selectedRows={selectedRows} refresh={refresh} />
      </Box>
    </div>
  );
};

export default Toolbar;
