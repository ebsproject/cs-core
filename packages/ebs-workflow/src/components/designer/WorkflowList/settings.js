import { FormattedMessage } from "react-intl";
import { Core } from "@ebs/styleguide";
const { Typography } = Core;
export const columns = [
  {
    Header: "id",
    accessor: "id",
    hidden: true,
    filter: false,
    disableGlobalFilter: true,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Workflow" />
      </Typography>
    ),
    csvHeader: "Workflow",
    accessor: "name",
    width: 720,
  },
  {
    Header: (
      <Typography variant="h5" color="primary">
        <FormattedMessage id="none" defaultMessage="Description" />
      </Typography>
    ),
    csvHeader: "Description",
    accessor: "description",
    filter: true,
    width: 720,
  },
];
