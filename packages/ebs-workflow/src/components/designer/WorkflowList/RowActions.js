import React, { useContext, useState } from "react";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { useQuery, useMutation } from "@apollo/client";
import PropTypes from "prop-types";
import { FIND_WORKFLOW_LIST, buildDeleteMutation } from "utils/apollo/gql/workflow";
import { setWorkflowId } from "store/modules/Workflow";
import { useDispatch } from "react-redux";
const { Box, IconButton, Tooltip, Typography } = Core;
import { showMessage } from "store/modules/message";
const { Edit, ViewList, Delete, NoteAdd } = Icons;
import AlertDeletion from "components/designer/alert-deletion/alert";
import { userContext, getContext, RBAC } from "@ebs/layout";

const RowActions = React.forwardRef(({ rowData, refresh }, ref) => {
  const dispatch = useDispatch();
  const [openAlertDelete, setOpenAlertDelete] = useState(false);
  const { setWorkflow, setReload } = useContext(WorkflowContext);
  const navigate = useNavigate();
  const { userProfile } = useContext(userContext);

  const [mutateWorkflow] = useMutation(buildDeleteMutation("Workflow"), {
    variables: {
      id: rowData.id,
    },
  });

  const { loading, error, data } = useQuery(FIND_WORKFLOW_LIST, {
    variables: {
      filters: [
        {
          col: "id",
          mod: "EQ",
          val: rowData.id,
        },
      ],
    },
    fetchPolicy:'no-cache'
  });
  const onCloseAlert = () => {
    setOpenAlertDelete(false);
  };
  const onChangeAlert = (action) => {
    if (action) {
      mutateWorkflow()
        .then(() => {
          dispatch(
            showMessage({
              message: `The workflow ${rowData.name} was deleted successfully`,
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        })
        .catch((err) => {
          dispatch(
            showMessage({
              message: `${err}`,
              variant: "error",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
        });
    }
    refresh();
    setOpenAlertDelete(false);
  };

  const editHandleClick = () => {
    setWorkflow({
      action: "edit",
      data: data.findWorkflowList.content[0],
    });
    setReload(true);
    navigate(`/wf/designer/add`);
  };
  const handleGoTo = () => {
    dispatch(setWorkflowId(rowData.id));
    navigate(`/wf/designer/status`);
  };
  const handleGoToDocuments = () => {
    dispatch(setWorkflowId(rowData.id));
    navigate(`/wf/designer/documents`);
  };

  if (loading) return "Loading...";
  if (error) return `Error! ${error.message}`;
  return (
    <div className="-mr-100">
      <Box
        component="div"
        ref={ref}
        data-testid={"RowActionsReportsTestId"}
        className="-ml-4 flex flex-auto"
      >
        <RBAC
          allowedAction={"Create"}
        >
          <Tooltip
            arrow
            placement="bottom"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Edit"} />
              </Typography>
            }
          >
            <IconButton size="small" color="primary" onClick={editHandleClick}>
              <Edit />
            </IconButton>
          </Tooltip>
        </RBAC>
        <RBAC
          allowedAction={"Delete"}
        >
          <Tooltip
            arrow
            placement="bottom"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Delete"} />
              </Typography>
            }
          >
            <IconButton size="small" color="primary" onClick={() => setOpenAlertDelete(true)}>
              <Delete />
            </IconButton>
          </Tooltip>
        </RBAC>

        <RBAC
          allowedAction={"Create"}
        >
          <Tooltip
            arrow
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Workflow Status"} />
              </Typography>
            }
          >
            <IconButton
              onClick={handleGoTo}
              size="small"
              color="primary"
              className="text-green-600"
            >
              <ViewList />
            </IconButton>
          </Tooltip>
        </RBAC>

        <RBAC allowedAction={"Create"} product="Workflow Management">
          <Tooltip
            arrow
            title={
              <Typography className="font-ebs text-xl">
                <FormattedMessage id={"none"} defaultMessage={"Workflow Documents"} />
              </Typography>
            }
          >
            <IconButton
              onClick={handleGoToDocuments}
              size="small"
              color="primary"
              className="text-green-600"
            >
              <NoteAdd />
            </IconButton>
          </Tooltip>
        </RBAC>
      </Box>
      <AlertDeletion
        open={openAlertDelete}
        onClose={onCloseAlert}
        onChange={onChangeAlert}
        title={rowData.name}
        message={"if you delete the workflow, it cannot be recovered"}
      />
    </div>
  );
});
// Type and required properties
RowActions.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func,
};
// Default properties

export default RowActions;
