import { render } from "@testing-library/react";
import WorkflowList from "."

jest.mock('./Toolbar', () => () => <div>Toolbar</div>);
jest.mock('./RowActions', () => () => <div>RowActions</div>);

describe("Workflow List component", () => {

    test('Workflow List component loaded', () => {
        const { getByTestId } = render(<WorkflowList />);
        expect(getByTestId("WorkflowListTestId")).toBeInTheDocument;
    });

});
