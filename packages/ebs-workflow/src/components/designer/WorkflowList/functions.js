import { client } from "utils/apollo";
import { FIND_DOCUMENTS_BY_WF_ID, FIND_FULL_WORKFLOW_BY_ID } from "utils/apollo/gql/workflow";

export async function downloadAsJson(fileName, workflowId) {

    try {
        const {data} = await client.query({
            query: FIND_FULL_WORKFLOW_BY_ID,
            variables: { id: workflowId }

        });
        const {data : documents} = await client.query({
            query: FIND_DOCUMENTS_BY_WF_ID,
            variables: { filters: [{col: "workflow.id", val:workflowId, mod:"EQ"}] }

        });
        let responseObj = {...data.findWorkflow, documents: documents.findFileTypeList.content}
        const jsonString = JSON.stringify(responseObj, null, 2);
        const blob = new Blob([jsonString], { type: 'application/json' });
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = `${fileName}.json`;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        URL.revokeObjectURL(url);

    } catch (error) {
        return false;
    }

}