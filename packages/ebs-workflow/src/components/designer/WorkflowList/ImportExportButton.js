import React, { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { Core, DropzoneDialog, EbsDialog } from "@ebs/styleguide";
const {
  Box,
  Button,
  DialogContent,
  DialogActions,
  Menu,
  MenuItem,
  Tooltip,
  Typography,
} = Core;
import { RBAC, getCoreSystemContext, getTokenId } from "@ebs/layout";
import {
  FIND_EMAIL_LIST,
  FIND_EMAIl_TEMPLATE_LIST,
  FIND_NODE_LIST,
  FIND_NODE_LIST_DEFINE,
  FIND_TEMPLATE_LIST,
  FIND_WORKFLOW_BY_ID,
} from "utils/apollo/gql/catalogs";
import { client } from "utils/apollo";
import axios from "axios";
const { graphqlUri } = getCoreSystemContext();
import { useAlertContext } from "components/alerts/alert-context";
import { FIND_FILE_TYPE_LIST } from "utils/apollo/gql/workflow";

const ImportExportButton = React.forwardRef(
  ({ selectedRows, refresh }, ref) => {
    const [anchorEl, setAnchorEl] = useState(null);
    const [open, setOpen] = useState(false);
    const { setAlerts } = useAlertContext();
    const restUrl = graphqlUri.replace(/\/?graphql/gi, "");

    const handleClick = (e) => {
      setAnchorEl(e.currentTarget);
    };

    const handleClose = () => {
      setAnchorEl(null);
      setOpen(false);
    };

    const export_workflows = async () => {
      try {
        const allResults = [];
        const { data, loading } = await client.query({
          query: FIND_WORKFLOW_BY_ID,
          variables: {
            id: selectedRows[0].id,
          },
        });
        const { data: dataNode, loadingNode } = await client.query({
          query: FIND_NODE_LIST_DEFINE,
          variables: {
            filters: [
              { col: "process.id", mod: "EQ", val: "4" },
              { col: "workflow.id", mod: "EQ", val: selectedRows[0].id },
            ],
          },
        });
        const { data: dataDocument, loadingDocument } = await client.query({
          query: FIND_FILE_TYPE_LIST,
          variables: {
            filters: [
              { col: "workflow.id", mod: "EQ", val: selectedRows[0].id },
            ],
          },
        });
        //Email-Templeate
        if (dataNode.findNodeList.content.length > 0) {
          const dataNodeArray = dataNode.findNodeList.content.map(
            (node) => node.define.email
          );

          const arrayNodeData = [...new Set(dataNodeArray)];

          await Promise.all(
            arrayNodeData.map(async (email, index) => {
              const { data: dataEmail, loadingEmail } = await client.query({
                query: FIND_EMAIl_TEMPLATE_LIST,
                variables: {
                  filters: [{ col: "id", mod: "EQ", val: email }],
                },
              });
              allResults[index] =
                dataEmail.findEmailTemplateList.content[0] || []; // Mantiene el orden
            })
          );
        }

        const dataWorkflow = {
          workflow: data.findWorkflow,
          documents:
            dataDocument.findFileTypeList.content.length > 0
              ? dataDocument.findFileTypeList.content
              : [],
          emailTemplate: allResults,
        };

        if (data) {
          const texto = data.findWorkflow.name;
          const name = texto.replace(/ /g, "_");
          const jsonData = JSON.stringify(dataWorkflow, null, 2);
          const blob = new Blob([jsonData], { type: "application/json" });
          const url = URL.createObjectURL(blob);
          const a = document.createElement("a");
          a.href = url;
          a.download = `workflow_${name}.json`;
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);
          URL.revokeObjectURL(url);
        }
      } catch (error) {
        console.error(error);
      }
    };

    const import_workflows = async () => {
      setOpen(true);
    };

    const onSubmit = async (files) => {
      const headers = {
        Accept: "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${getTokenId()}`,
      };
      let texto = files[0].name;
      const workflowName = texto.replace(/\.json/i, "");
      if (files.length > 0) {
        const dataJson = new FormData();
        let _file = files[0];
        dataJson.append("jsonFile", _file);
        try {
          const data = await axios.request({
            url: `${restUrl}/workflow/import`,
            method: "post",
            headers: headers,
            data: dataJson,
          });
          const responseJson = data.status;
          if (responseJson === 200) {
            setAlerts((prev) => [
              ...prev,
              {
                id: "workflow-edited-success-" + workflowName,
                message: `The file ${workflowName} has been imported into this environment successfully.`,
                severity: "success",
              },
            ]);
            refresh();
            handleClose();
          }
        } catch (error) {
          handleClose();
          setAlerts((prev) => [
            ...prev,
            {
              id: "workflow-edited-error-" + workflowName,
              message: `An error occurred while trying to import the file ${workflowName} to this environment`,
              severity: "error",
            },
          ]);
          console.error(error);
        }
      }
    };

    return (
      <div>
        <RBAC allowedAction={"Import_Export"}>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Import/Export Workflow"}
                />
              </Typography>
            }
          >
            <Button
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              onClick={handleClick}
            >
              <Typography className="font-ebs">
                <FormattedMessage id="none" defaultMessage={"Import/Export"} />
              </Typography>
            </Button>
          </Tooltip>
        </RBAC>
        <Menu
          elevation={8}
          getContentAnchorEl={null}
          anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
          transformOrigin={{ vertical: "top", horizontal: "center" }}
          id={"import-export-workflow"}
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Import Workflow to this environment"}
                />
              </Typography>
            }
          >
            <MenuItem onClick={import_workflows}>
              <FormattedMessage
                id={"none"}
                defaultMessage={"Import Workflow"}
              />
            </MenuItem>
          </Tooltip>
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage
                  id={"none"}
                  defaultMessage={"Export Workflow from this environment"}
                />
              </Typography>
            }
          >
            <MenuItem
              onClick={export_workflows}
              disabled={selectedRows.length > 0 ? false : true}
            >
              <FormattedMessage
                id={"none"}
                defaultMessage={
                  selectedRows.length > 0
                    ? "Export Workflow"
                    : "Select a template first"
                }
              />
            </MenuItem>
          </Tooltip>
          <DropzoneDialog
            acceptedFiles={[".json"]}
            cancelButtonText={"cancel"}
            submitButtonText={"submit"}
            maxFileSize={50000000}
            open={open}
            onClose={handleClose}
            onSave={onSubmit}
            showPreviews={true}
            showFileNamesInPreview={true}
          />
        </Menu>
      </div>
    );
  }
);
export default ImportExportButton;
