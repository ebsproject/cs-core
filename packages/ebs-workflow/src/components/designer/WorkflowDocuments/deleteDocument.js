import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS AND ATOMS TO USE
import { Core, Icons } from "@ebs/styleguide";
const {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  IconButton,
  Typography,
  Tooltip,
} = Core;
import { FormattedMessage } from "react-intl";
import { useDispatch } from "react-redux";
import { DELETE_FILE_TYPE } from "utils/apollo/gql/workflow";
import { showMessage } from "store/modules/message";
import { client } from "utils/apollo";
const { Delete } = Icons;

const DeleteDocument = React.forwardRef(({ rowSelected, refresh }, ref) => {
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleDelete = () => {
    try {
      client
        .mutate({
          mutation: DELETE_FILE_TYPE,
          variables: { id: rowSelected.id },
        })
        .then(() => {
          dispatch(
            showMessage({
              message: "Deleted Document",
              variant: "success",
              anchorOrigin: {
                vertical: "top",
                horizontal: "right",
              },
            })
          );
          handleClose();
          refresh();
        });
    } catch (error) {
      dispatch(
        showMessage({
          message: error.toString(),
          variant: "error",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    }
  };

  return (
    <div ref={ref}>
      <Tooltip
        arrow
        title={
          <Typography className="font-ebs text-xl">
            <FormattedMessage id="none" defaultMessage="Delete Document" />
          </Typography>
        }
      >
        <IconButton onClick={handleClickOpen} aria-label="Delete Document" color="primary">
          <Delete />
        </IconButton>
      </Tooltip>
      <Dialog fullWidth keepMounted maxWidth="sm" open={open} onClose={handleClose}>
        <DialogTitle>
          <FormattedMessage id="none" defaultMessage="Delete Document" />
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <FormattedMessage
              id="none"
              defaultMessage="Are you sure to delete following Document?"
            />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <Typography variant="button" color="secondary">
              <FormattedMessage id="none" defaultMessage="Cancel" />
            </Typography>
          </Button>
          <Button onClick={handleDelete}>
            <Typography variant="button">
              <FormattedMessage id="none" defaultMessage="Confirm" />
            </Typography>
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
DeleteDocument.propTypes = {
  refresh: PropTypes.func,
  rowSelected: PropTypes.object,
};
// Default properties

export default DeleteDocument;
