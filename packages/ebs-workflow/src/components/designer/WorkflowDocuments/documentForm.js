import React, { useContext } from "react";
import { Core, Icons } from "@ebs/styleguide";
import { useForm, Controller } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import { useDispatch, useSelector } from "react-redux";
import { showMessage } from "store/modules/message";
import { WorkflowContext } from "../context/workflow-context";
import { CREATE_FILE_TYPE, MODIFY_FILE_TYPE } from "utils/apollo/gql/workflow";
import { client } from "utils/apollo";
const { Typography, Button, TextField, Grid, Box } = Core;
const { CheckCircle, Cancel } = Icons;
//MAIN FUNCTION
/**
 * @param props: component properties
 * @param ref: reference made by React.forward
 */
const DocumentForm = () => {
  const { document } = useContext(WorkflowContext);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { workflowId } = useSelector(({ wf_workflow }) => wf_workflow);

  const MutationFileType = (dataInfo, action) =>
    new Promise((resolve, reject) => {
      try {
        client
          .mutate({
            mutation: action == "add" ? CREATE_FILE_TYPE : MODIFY_FILE_TYPE,
            variables: {
              fileTypeInput: dataInfo,
            },
          })
          .then((result) => {
            resolve(result);
          })
          .catch((e) => {
            reject(e);
          });
      } catch (error) {
        reject(error);
      }
    });

  const onSubmit = (data) => {
    const dataInfo = {
      id: document.action === "edit" ? data.id : 0,
      description: data.description,
      workflowId: workflowId,
    };
    MutationFileType(dataInfo, document.action).then(() => {
      dispatch(
        showMessage({
          message: `Document was ${document.action == "edit" ? "edited" : "created"} successfully `,
          variant: "success",
          anchorOrigin: {
            vertical: "top",
            horizontal: "right",
          },
        })
      );
    });
    navigate("/wf/designer/documents");
  };
  const { control, handleSubmit, setValue } = useForm({
    defaultValues:
      document.action === "edit"
        ? {
            id: document.data.id,
            description: document.data.description,
          }
        : {
            description: "",
          },
  });
  const handleCancel = (e) => {
    e.preventDefault();
    navigate("/wf/designer/documents");
  };
  return (
    /**
     * @prop data-testid: Id to use inside contact.test.js file.
     */
    <div data-testid={"DocumentsFormTestId"}>
      <form
        onSubmit={handleSubmit(onSubmit)}
        style={{
          paddingTop: "14px",
          position: "relative",
          paddingLeft: "10px",
          paddingRight: "20px",
        }}
      >
        <Typography variant="h5" color="primary" className="flex-grow font-ebs col-span-2">
          <FormattedMessage id="none" defaultMessage="Documents" />
        </Typography>
        <Grid container justifyContent="flex-end">
          <Button
            data-testid="cancel"
            onClick={handleCancel}
            startIcon={<Cancel />}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Cancel"} />
          </Button>
          <Button
            data-testid="save"
            type="submit"
            startIcon={<CheckCircle />}
            className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
          >
            <FormattedMessage id={"none"} defaultMessage={"Save"} />
          </Button>
        </Grid>
        <Grid container spacing={2}>
          <Grid item xs={3} sm={3} md={6} lg={12} xl={12}>
            <Controller
              name="description"
              control={control}
              render={({ field }) => (
                <TextField
                  fullWidth
                  variant="outlined"
                  multiline
                  rows={5}
                  InputLabelProps={{ shrink: true }}
                  {...field}
                  required
                  label={<FormattedMessage id="none" defaultMessage="Description" />}
                  onChange={(e) => {
                    setValue("description", e.target.value);
                  }}
                />
              )}
            />
          </Grid>
        </Grid>
      </form>
    </div>
  );
};

export default DocumentForm;
