import React, { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
import { WorkflowContext, useWorkflowContext } from "components/designer/context/workflow-context";
import DeleteDocument from "../WorkflowDocuments/deleteDocument";
const { Box, IconButton, Tooltip, Typography } = Core;
const { Edit } = Icons;

const RowActions = ({ rowData, refresh }) => {

    const { setReload, setDocument } = useWorkflowContext();
    const navigate = useNavigate();

    const handleClickEdit = () => {
      setDocument({
        action: "edit",
        data: rowData
      });
      setReload(true);
      navigate(`/wf/designer/documents/add`);
    }
    return (
      <div className="-mr-100" >
        <Box
          component="div"
          data-testid={"RowActionsReportsTestId"}
          className="-ml-4 flex flex-auto"
        >
          <Tooltip
            arrow
            placement="bottom"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Edit Document"} />
              </Typography>
            }
          >
            <IconButton
            data-testid={"EditButtonTestId"}
              size="small"
              color="primary"
              onClick={handleClickEdit}
            >
              <Edit />
            </IconButton>
          </Tooltip>

          <Tooltip 
          arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Delete Document"} />
              </Typography>
            }>
            <DeleteDocument rowSelected={rowData} refresh={refresh} />
          </Tooltip>
        </Box>

      </div>
    )
  }


export default RowActions;