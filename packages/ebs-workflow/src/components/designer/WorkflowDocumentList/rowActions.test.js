import { fireEvent, render, screen } from "@testing-library/react";
import RowDocumentsActions from "./rowActions";
import { useNavigate } from "react-router-dom";
import { useWorkflowContext } from "../context/workflow-context";
import DeleteDocument from "../WorkflowDocuments/deleteDocument";

jest.mock("../context/workflow-context", () => ({
    useWorkflowContext: jest.fn(),
}));

jest.mock('react-router-dom', () => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate:  jest.fn()
}))

jest.mock("../WorkflowDocuments/deleteDocument", () => () => (<div>Delete Document</div>))


describe("Row Actions component", () => {
 
    beforeEach(() => {
        useWorkflowContext.mockReturnValue({
            setReload: jest.fn(),
            setDocument: jest.fn()
        })
    })


    const renderComponent = (props) => render(<RowDocumentsActions rowData={props.rowData} refresh={props.refresh} />)
    test("render component in the DOM", () => {
        const props = {
            rowData: { id: 1 },
            refresh: jest.fn()
        }
        renderComponent(props)
        expect(screen.getByTestId("RowActionsReportsTestId")).toBeInTheDocument()
    })
    test("click and open to edit ", () => {
        const mockNavigate = jest.fn();
        useNavigate.mockReturnValue(mockNavigate);
        const props = {
            rowData: { id: 1 },
            refresh: jest.fn()
        }
   
    
        renderComponent(props)
      const button =  screen.getByTestId("EditButtonTestId")
      fireEvent.click(button);
      expect(mockNavigate).toHaveBeenCalledWith("/wf/designer/documents/add");
     
    })
})