import React, { useEffect, useContext, useState } from "react";
import { useSelector } from "react-redux";
import { EbsGrid } from "@ebs/components";
import { FormattedMessage } from "react-intl";
import { getCoreSystemContext } from "@ebs/layout";
import { Core } from "@ebs/styleguide";
import rowActionsStatus from "./rowActions";
import toolbar from './toolbar'
const { graphqlUri } = getCoreSystemContext();
const { Typography } = Core;
//MAIN FUNCTION
/**
 * @param { }: component properties.
 * @param ref: reference made by React.forward.
 */
const WorkflowDocumentList = React.forwardRef(({ }, ref) => {
    const { workflowId } = useSelector(({ wf_workflow }) => wf_workflow);
    const columns = [
        { Header: "id", accessor: "id", hidden: true, disableGlobalFilter: true },

        {
            Header: (
                <Typography variant="h5" color="primary">
                    <FormattedMessage id="none" defaultMessage="Description" />
                </Typography>
            ),
            accessor: "description",
            csvHeader: "Description",
            width: 1000,
        },
    ];
    return (
        <EbsGrid
            id="DocumentType"
            toolbar={true}
            columns={columns}
            uri={graphqlUri}
            entity="FileType"
            defaultFilters={[{col: "workflow.id", mod: "EQ", val: Number(workflowId || 0)} ]}
            title={
                <Typography className="font-ebs text-ebs-green-default text-3xl">
                    <FormattedMessage id="none" defaultMessage="Workflow Documents" />
                </Typography>
            }
            rowactions={rowActionsStatus}
            toolbaractions={toolbar}
            csvfilename="WorkflowDocuments"
            callstandard="graphql"
            raWidth={110}
            select="multi"
            height="85vh"
        />
    )
});
// Type and required properties
WorkflowDocumentList.propTypes = {};
// Default properties


export default WorkflowDocumentList;