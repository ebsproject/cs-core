import React, { useContext } from "react";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
const { Box, Button, Tooltip, Typography } = Core;
const {  PostAdd, KeyboardArrowLeft } = Icons;
import { useNavigate } from "react-router-dom";
import { WorkflowContext, useWorkflowContext } from "components/designer/context/workflow-context";

const toolbar = React.forwardRef(
  ({ rowData, refresh }, ref) => {
    const { setDocument } = useWorkflowContext();
    const navigate = useNavigate();
    
    const handleGo = () => {
      setDocument({
        action: "add",
        data: { id: 0 }
      });
      navigate(`/wf/designer/documents/add`);
    };
    const handleBack = () => {
      navigate(`/wf/designer`);
    }
    return (
      <div className="-mr-100" >
        <Box
          component="div"
          ref={ref}
          data-testid={"RowActionsDocumentTestId"}
          className="-ml-4 flex flex-auto"
        >
          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Back to Workflow"} />
              </Typography>
            }
          >
            <Button
              data-testid={"BackButtonTestId"}              
              startIcon={<KeyboardArrowLeft />}
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              onClick={handleBack}
            >
              <Typography className="font-ebs">
                <FormattedMessage
                  id="none"
                  defaultMessage={"Back"}
                />
              </Typography>
            </Button>
          </Tooltip>

          <Tooltip
            arrow
            placement="top"
            title={
              <Typography className="font-ebs text-lg">
                <FormattedMessage id={"none"} defaultMessage={"Add New Document"} />
              </Typography>
            }
          >
            <Button
             data-testid={"NewButtonTestId"}    
              startIcon={<PostAdd />}
              className="bg-ebs-brand-default rounded-md text-white p-2 m-2 hover:bg-ebs-brand-900"
              onClick={handleGo}
            >
              <Typography className="font-ebs">
                <FormattedMessage id="none" defaultMessage={"New Document"} />
              </Typography>
            </Button>
          </Tooltip>
        </Box>
      </div>
    )
  })


export default toolbar;