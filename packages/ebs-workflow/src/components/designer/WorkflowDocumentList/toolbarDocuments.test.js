import ToolbarDocuments from "./toolbar";
import { fireEvent, getByText, render, screen } from "@testing-library/react";
import { useWorkflowContext } from "../context/workflow-context";
import { useNavigate } from "react-router-dom";
jest.mock('react-router-dom', () => ({
    ...jest.requireActual("react-router-dom"),
    useNavigate: jest.fn()
}))

jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

jest.mock("../context/workflow-context", () => ({
    useWorkflowContext: jest.fn(),
}));

describe("Toolbar documents component", () => {

    beforeEach(() => {
        useWorkflowContext.mockReturnValue({
            setReload: jest.fn(),
            setDocument: jest.fn()
        })
    })

    const renderComponent = (props) => render(<ToolbarDocuments rowData={props.rowData} refresh={props.refresh} />)
    test("component loaded in DOM", () => {
        const props = {
            rowData: { id: 1 },
            refresh: jest.fn()
        }
        renderComponent(props)
        expect(screen.getByText(/new document/i)).toBeInTheDocument()
        expect(screen.getByText(/back/i)).toBeInTheDocument()
    })
    test("click in back button", () => {
        const mockNavigate = jest.fn();
        useNavigate.mockReturnValue(mockNavigate);
        const props = {
            rowData: { id: 1 },
            refresh: jest.fn()
        }
        renderComponent(props)
       
        const button = screen.getByTestId("BackButtonTestId")
        fireEvent.click(button)
        expect(mockNavigate).toHaveBeenCalledWith("/wf/designer");
     
    })
    test("click in new button", () => {
        const mockNavigate = jest.fn();
        useNavigate.mockReturnValue(mockNavigate);
        const props = {
            rowData: { id: 1 },
            refresh: jest.fn()
        }
        renderComponent(props)
        const button = screen.getByTestId("NewButtonTestId")
        fireEvent.click(button)
        expect(mockNavigate).toHaveBeenCalledWith("/wf/designer/documents/add");
    })
})