import React, { useContext } from "react";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import { Core, Icons } from "@ebs/styleguide";
import { useNavigate } from "react-router-dom";
import { WorkflowContext } from "components/designer/context/workflow-context";
import DeleteStatus from "../WorkflowStatus/deleteStatus";
const { Box, IconButton, Tooltip, Typography } = Core;
const { Edit } = Icons;

const rowActionsStatus = React.forwardRef(({ rowData, refresh }, ref) => {
  const { setReload, setStatus } = useContext(WorkflowContext);
  const navigate = useNavigate();
  const handleClickEdit = () => {
    setStatus({
      action: "edit",
      data: rowData,
    });
    setReload(true);
    navigate(`/wf/designer/status/add`);
  };
  return (
    <div className="-mr-100">
      <Box
        component="div"
        ref={ref}
        data-testid={"RowActionsReportsTestId"}
        className="-ml-4 flex flex-auto"
      >
        <Tooltip
          arrow
          placement="bottom"
          title={
            <Typography className="font-ebs text-lg">
              <FormattedMessage id={"none"} defaultMessage={"Edit Status"} />
            </Typography>
          }
        >
          <IconButton size="small" color="primary" onClick={handleClickEdit}>
            <Edit />
          </IconButton>
        </Tooltip>

        <Tooltip>
          <DeleteStatus rowSelected={rowData} refresh={refresh} />
        </Tooltip>
      </Box>
    </div>
  );
});
// Type and required properties
rowActionsStatus.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.bool,
};
// Default properties

export default rowActionsStatus;
