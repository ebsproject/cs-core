import { createContext, useState, useContext } from 'react';
import { initialNodes, initialEdges, initialStatus } from "components/designer/Workflow/designer/initialization"
export const WorkflowContext = createContext({
  action: "unknown",
  data: { id: 0 }
});
export const WorkflowContextProvider = ({ children }) => {
  const [workflow, setWorkflow] = useState({
    action: "unknown",
    data: { id: 0 }
  });
  const [nodes, setNodes] = useState(initialNodes);
  const [edges, setEdges] = useState(initialEdges);
  const [reload, setReload] = useState(false);
  const [status, setStatus] = useState(initialStatus);
  const [document, setDocument] = useState(null);
  const [formData, setFormData] = useState(null);
  const [rowData, setRowData] = useState(null);
  const [record, setRecord] = useState(null);
  const [contactList, setContactList] = useState(null);
  return (
    <WorkflowContext.Provider
      value={
        {
          workflow, setWorkflow,
          nodes, setNodes,
          edges, setEdges,
          reload, setReload,
          status, setStatus,
          rowData, setRowData,
          formData, setFormData,
          document, setDocument,
          record, setRecord,
          contactList, setContactList
        }}>
      {children}
    </WorkflowContext.Provider>
  );
};

export function useWorkflowContext() {
  return useContext(WorkflowContext)
}