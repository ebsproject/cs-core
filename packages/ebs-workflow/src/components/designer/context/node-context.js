import { createContext, useState, useEffect } from 'react';
import PropTypes from "prop-types";
export const NodeContext = createContext(0);

const NodeContextProvider = ({ nodeSelected, children }) => {

  const [node, setNode] = useState(null);


  useEffect(() => {
    setNode(nodeSelected);
  }, [])

  return (
    <NodeContext.Provider value={{ node, setNode }}>
      {children}
    </NodeContext.Provider>
  );
};

NodeContextProvider.propTypes = {
  nodeSelected: PropTypes.object.isRequired
};
// Default properties


export default NodeContextProvider;