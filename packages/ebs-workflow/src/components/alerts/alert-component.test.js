import { render, screen, act } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import AlertMessages from "./alert-component";
import { useAlertContext } from "./alert-context";
jest.mock("./alert-context", () => ({
  useAlertContext: jest.fn(),
}));

describe("AlertMessages Component", () => {
  let alerts;
  let setAlerts;

  beforeEach(() => {
    alerts = [
      { id: 1, message: "Alert 1", severity: "error" },
      { id: 2, message: "Alert 2", severity: "warning" },
    ];
    setAlerts = jest.fn();
    useAlertContext.mockReturnValue({ alerts, setAlerts });
  });

  it("renders alerts when the alerts array is not empty", () => {
    render(<AlertMessages />);
    expect(screen.getByText("Alert 1")).toBeInTheDocument();
    expect(screen.getByText("Alert 2")).toBeInTheDocument();
  });

  it("calls setAlerts to remove the oldest alert after 8 seconds", () => {
    jest.useFakeTimers();
    render(<AlertMessages />);
    expect(setAlerts).not.toHaveBeenCalled();

    act(() => {
      jest.advanceTimersByTime(8000);
    });

    expect(setAlerts).toHaveBeenCalledWith(expect.any(Function));
  });

//   it("closes an alert when the close button is clicked", () => {
//     render(<AlertMessages />);
//     const closeButton = screen.getAllByRole("button")[0];
//     userEvent.click(closeButton);

//     expect(setAlerts).toHaveBeenCalledWith(expect.any(Function));
//   });

  it("opens Snackbar when there are alerts", () => {
    render(<AlertMessages />);
    const snackbar = screen.getByRole("presentation");
    expect(snackbar).toBeVisible();
  });
});
