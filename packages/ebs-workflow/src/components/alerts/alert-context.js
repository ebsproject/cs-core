import { createContext, useState, useContext } from 'react';

const AlertContext = createContext();

export function AlertContextProvider({ children }) {

  const [alerts, setAlerts] = useState([]);
  return (
    <AlertContext.Provider value={
      { alerts, setAlerts }
    }>
      {children}
    </AlertContext.Provider>
  );
};

export function useAlertContext() {
  return useContext(AlertContext)
}