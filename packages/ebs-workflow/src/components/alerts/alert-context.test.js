import React from 'react';
import { render, screen } from '@testing-library/react';
import { AlertContextProvider, useAlertContext } from './alert-context';
import { act, fireEvent } from '@testing-library/react';

function ContextConsumerComponent() {
  const { alerts, setAlerts } = useAlertContext();

  return (
    <div>
      <button onClick={() => setAlerts([...alerts, { id: alerts.length + 1, message: `Alert ${alerts.length + 1}` }])}>
        Add Alert
      </button>
      <div data-testid="alerts">
        {alerts.map((alert) => (
          <div key={alert.id}>{alert.message}</div>
        ))}
      </div>
    </div>
  );
}

describe('Alert Context', () => {
  test('provides default empty alerts array', () => {
    render(
      <AlertContextProvider>
        <ContextConsumerComponent />
      </AlertContextProvider>
    );
    expect(screen.getByTestId('alerts').textContent).toBe('');
  });

  test('updates alerts when setAlerts is called', () => {
    render(
      <AlertContextProvider>
        <ContextConsumerComponent />
      </AlertContextProvider>
    );
    act(() => {
      fireEvent.click(screen.getByText('Add Alert'));
    });

    expect(screen.getByText('Alert 1')).toBeInTheDocument();

    act(() => {
      fireEvent.click(screen.getByText('Add Alert'));
    });
    expect(screen.getByText('Alert 2')).toBeInTheDocument();
  });
});
