import { Core } from "@ebs/styleguide";
import { useEffect } from "react";
import { useAlertContext } from "./alert-context";
const { Alert, Snackbar } = Core;
const AlertMessages = () => {
    const { alerts, setAlerts } = useAlertContext();
    useEffect(() => {
        if (alerts.length > 0) {
            const timer = setTimeout(() => {
                setAlerts((prevItems) => prevItems.slice(1));
            }, 5000);
            return () => clearTimeout(timer);
        }
    }, [alerts]);
    const handleClose = (id) => {
        setAlerts((prevItems) => prevItems.filter((alert) => alert.id !== id));
    };

    return (
        <Snackbar
            open={alerts.length > 0}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            data-testid={"AlertComponentTestId"}
        >
            <div>
                {alerts.map((alert) => (
                    <Alert
                        key={alert.id}
                        onClose={() => handleClose(alert.id)}
                        severity={alert.severity}
                        sx={{ width: '500px', mb: 1 }}
                    >
                        {alert.message}
                    </Alert>
                ))}
            </div>
        </Snackbar>
    );
}
export default AlertMessages;