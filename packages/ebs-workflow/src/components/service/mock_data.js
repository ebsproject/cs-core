export const workflowValuesMock ={
    "workflowContacts": [
        {
            "contact": {
                "id": 77,
                "institution": {
                    "unitType": {
                        "name": "Service Unit"
                    },
                    "legalName": "CIMMYT Mexico Wheat Molecular Laboratory",
                    "commonName": "WMBL"
                }
            }
        },
        {
            "contact": {
                "id": 92,
                "institution": {
                    "unitType": {
                        "name": "Program"
                    },
                    "legalName": "Irrigated South-East Asia",
                    "commonName": "IRSEA"
                }
            }
        }
    ],
    "id": 562,
    "title": "Shipment Outgoing IRRI",
    "name": "Shipment Outgoing IRRI",
    "description": "Shipment Outgoing IRRI",
    "navigationName": "Shipment Outgoing IRRI",
    "navigationIcon": "na",
    "sortNo": 3,
    "help": "Shipment Outgoing IRRI ",
    "showMenu": true,
    "securityDefinition": {
        "roles": [
            {
                "id": "2",
                "name": "Admin",
                "isSystem": true,
                "__typename": "Role",
                "description": "admin"
            }
        ]
    },
    "phases": [
        {
            "sequence": 1,
            "id": 562,
            "icon": null,
            "description": "Creation",
            "name": "Creation",
            "dependOn": {
                "edges": [],
                "width": 800,
                "height": 400,
                "position": {
                    "x": 510.6809539537304,
                    "y": -51.151874456923224
                }
            },
            "workflow": {
                "id": 562
            },
            "workflowViewType": {
                "id": "1",
                "name": "N/A"
            },
            "stages": [
                {
                    "phase": {
                        "id": 562
                    },
                    "id": 10732,
                    "description": "Documents Stage",
                    "name": "Documents Stage",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 200,
                            "y": 196.2337989929781
                        }
                    },
                    "sequence": 3,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21026",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 10,
                                    "y": 34.4200522507731
                                }
                            },
                            "define": {
                                "id": 8,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "columns": [
                                        {
                                            "name": "",
                                            "alias": "",
                                            "hidden": false
                                        }
                                    ],
                                    "legalDocuments": {
                                        "security": {
                                            "actors": {
                                                "shu": true,
                                                "sender": false,
                                                "creator": false,
                                                "recipient": false,
                                                "requestor": false
                                            },
                                            "allowedStatus": [
                                                {
                                                    "statusId": 365
                                                },
                                                {
                                                    "statusId": "370"
                                                }
                                            ]
                                        },
                                        "documents": [
                                            {
                                                "type": "mls",
                                                "label": "Shrink Wrap",
                                                "value": "SMTA_MLS_Shrink-Wrap",
                                                "active": true,
                                                "template": "sys_legal_smta_mls"
                                            },
                                            {
                                                "type": "mls",
                                                "label": "Signed",
                                                "value": "SMTA_MLS_Signed",
                                                "active": true,
                                                "template": "sys_standard_material_transfer_agreement_signed_rel1"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "Shrink Wrap",
                                                "value": "SMTA_PUD_Shrink-Wrap",
                                                "active": true,
                                                "template": "sys_legal_smta_pud"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "Signed",
                                                "value": "SMTA_PUD_Signed",
                                                "active": true,
                                                "template": "sys_standard_material_transfer_agreement_signed_pud1"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "HRDC_OMTA",
                                                "value": "HRDC_OMTA",
                                                "active": true,
                                                "template": "sys_hrdc_omta"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "JIRCAS_OMTA",
                                                "value": "JIRCAS_OMTA",
                                                "active": true,
                                                "template": "sys_jircas_omta"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "NARVI_OMTA",
                                                "value": "NARVI_OMTA",
                                                "active": true,
                                                "template": "sys_narvi_omta"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "VRAP_OMTA",
                                                "value": "VRAP_OMTA",
                                                "active": true,
                                                "template": "sys_legal_smta_pud"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "IRRI_INGER_OMTA",
                                                "value": "IRRI_INGER_OMTA",
                                                "active": true,
                                                "template": "sys_irri_inger_omta"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "ASEAN RiceNet-OMTA",
                                                "value": "ASEAN_RICENET_OMTA",
                                                "active": true,
                                                "template": "sys_asean_omta"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "IRRI_OMTA",
                                                "value": "IRRI_OMTA",
                                                "active": true,
                                                "template": "sys_irri_omta"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "Shrink Wrap",
                                                "value": "SMTA_PUD2_Shrink-Wrap",
                                                "active": true,
                                                "template": "sys_legal_smta_pud2"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "Signed",
                                                "value": "SMTA_PUD2_Signed",
                                                "active": true,
                                                "template": "sys_standard_material_transfer_agreement_signed_pud2"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "Biological MTA",
                                                "value": "BIOLOGICAL_MTA_PUD2",
                                                "active": true,
                                                "template": "sys_biological_mta"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "Global MTA",
                                                "value": "GLOBAL_MTA_PUD2",
                                                "active": true,
                                                "template": "sys_global_mta_pud2"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "Global MTA",
                                                "value": "GLOBAL_MTA_PUD",
                                                "active": true,
                                                "template": "sys_global_mta_pud"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "IRRI CMTA",
                                                "value": "IRRI_CMTA_PUD2",
                                                "active": true,
                                                "template": "sys_irri_cmta"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "Native Traits CMTA",
                                                "value": "NATIVE_TRAITS_CMTA_PUD2",
                                                "active": true,
                                                "template": "sys_native_traits_cmta"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "HRDC CMTA",
                                                "value": "HRDC_CMTA_PUD2",
                                                "active": true,
                                                "template": "sys_hrdc_cmta"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "MTA for outgoing transgenic materials",
                                                "value": "MTA_OUTGOING_TRANSGENIC_PUD2",
                                                "active": true,
                                                "template": "sys_mta_for_outgoing_transgenic_materials"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "MTA for Golden Rice",
                                                "value": "MTA_GOLDEN_RICE_PUD2",
                                                "active": true,
                                                "template": "sys_mta_golden_rice_lead_event_to_gr_licensees"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "HRDC RETA",
                                                "value": "HRDC_RETA_PUD2",
                                                "active": true,
                                                "template": "sys_hrdc_reta_pud2"
                                            },
                                            {
                                                "type": "pud2",
                                                "label": "TLSG RETA",
                                                "value": "TLSG_RETA_PUD2",
                                                "active": true,
                                                "template": "sys_tlsg_reta_pud2"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "HRDC RETA",
                                                "value": "HRDC_RETA_PUD",
                                                "active": true,
                                                "template": "sys_hrdc_reta"
                                            },
                                            {
                                                "type": "pud1",
                                                "label": "TLSG RETA",
                                                "value": "TLSG_RETA_PUD",
                                                "active": true,
                                                "template": "sys_tlsg_reta"
                                            }
                                        ]
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "disabled": false,
                                "component": "UploadDocuments",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Documents",
                            "description": "Documents",
                            "help": "Documents",
                            "sequence": 3,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "7",
                                "name": "External",
                                "description": "External Component"
                            },
                            "process": {
                                "id": "8",
                                "isBackground": false,
                                "name": "External",
                                "code": "EXTERNAL",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21068",
                            "dependOn": {
                                "edges": [
                                    "21026"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 181.0000000000001,
                                    "y": 15
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21070",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "MOVE TO SUBMIT",
                            "description": "MOVE TO SUBMIT",
                            "help": "MOVE TO SUBMIT",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 562
                    },
                    "id": 10698,
                    "description": "Required Inputs",
                    "name": "Required Inputs",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 183.3981816542199,
                            "y": 0
                        }
                    },
                    "sequence": 1,
                    "icon": null,
                    "workflowViewType": {
                        "id": "3",
                        "name": "TabHorizontal"
                    },
                    "nodes": [
                        {
                            "id": "20992",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 0,
                                    "y": 23.352009947434
                                }
                            },
                            "define": {
                                "id": 1,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "General Information",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "General Information",
                            "description": "General Information",
                            "help": "General Information",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "2",
                                "name": "Form"
                            },
                            "nodeType": {
                                "id": "1",
                                "name": "form",
                                "description": "Form"
                            },
                            "process": {
                                "id": "1",
                                "isBackground": false,
                                "name": "Generate Form",
                                "code": "FORM",
                                "path": null
                            },
                            "nodeCFs": [
                                {
                                    "id": "770",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "senderEmail",
                                        "sort": 9,
                                        "rules": {
                                            "required": "The sender email is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Sender Email",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Sender Email",
                                            "variant": "outlined",
                                            "disabled": true,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "email",
                                            "label": [
                                                "email"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "email"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "senderId",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Sender Email",
                                    "name": "senderEmail",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "764",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 5,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "helper": {
                                            "title": "Specific Materials",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Specific Materials",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": false,
                                        "defaultOptions": [
                                            {
                                                "label": "Seeds and genetic resources",
                                                "value": 1
                                            },
                                            {
                                                "label": "GMO",
                                                "value": 2
                                            },
                                            {
                                                "label": "Non seeds",
                                                "value": 3
                                            },
                                            {
                                                "label": "Transgenic",
                                                "value": 4
                                            },
                                            {
                                                "label": "Other",
                                                "value": 5
                                            }
                                        ]
                                    },
                                    "apiAttributesName": "",
                                    "description": "Specific Materials",
                                    "name": "specificMaterials",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "763",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 4,
                                        "rules": {
                                            "required": "This field is required!"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Shipment Purpose",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "",
                                                "mod": "EQ",
                                                "val": ""
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Shipment Purpose",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        },
                                        "defaultOptions": [
                                            {
                                                "label": "Breeding",
                                                "value": 1
                                            },
                                            {
                                                "label": "Testing",
                                                "value": 2
                                            },
                                            {
                                                "label": "Research",
                                                "value": 3
                                            },
                                            {
                                                "label": "Education",
                                                "value": 4
                                            },
                                            {
                                                "label": "Commercial analysis",
                                                "value": 5
                                            },
                                            {
                                                "label": "Other",
                                                "value": "6"
                                            }
                                        ]
                                    },
                                    "apiAttributesName": "",
                                    "description": "Shipment Purpose",
                                    "name": "shipmentPurposeDd",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1003",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "securityDiveder",
                                        "sort": 1.2,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "SHIPMENT INFORMATION",
                                            "variant": "standard",
                                            "disabled": true,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": false,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Divider",
                                    "name": "securityDivider",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "797",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "requestorEmail",
                                        "sort": 15,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Requestor Email",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Requestor Email",
                                            "variant": "outlined",
                                            "disabled": true,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "email",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "email"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "requestorId",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Requestor Email",
                                    "name": "requestorEmail",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "761",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "shipmentName",
                                        "sort": 2,
                                        "rules": {
                                            "required": "The shipment Name is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "helper": {
                                            "title": "Shipment Name",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Shipment Name",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "name",
                                            "label": [
                                                "name"
                                            ],
                                            "entity": "Program",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "name"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "programId",
                                            "sequenceRules": {
                                                "ruleName": "program-code",
                                                "applyRule": true
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "requestCode",
                                    "description": "Shipment Name",
                                    "name": "requestCode",
                                    "required": true,
                                    "attributes": {
                                        "id": "2593",
                                        "description": "name",
                                        "isRequired": true,
                                        "name": "name",
                                        "sortNo": 2
                                    },
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "971",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 1.1,
                                        "rules": {
                                            "required": "The service provider is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Service Provider",
                                            "placement": "top"
                                        },
                                        "columns": [],
                                        "filters": [],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Service Provider",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": false,
                                        "stateValues": {
                                            "name": "serviceProvider"
                                        },
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        },
                                        "defaultOptions": []
                                    },
                                    "apiAttributesName": "serviceProvider",
                                    "description": "Service Provider",
                                    "name": "serviceProvider",
                                    "required": true,
                                    "attributes": {
                                        "id": "2688",
                                        "description": "modification_timestamp",
                                        "isRequired": true,
                                        "name": "modification_timestamp",
                                        "sortNo": 10
                                    },
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "872",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "submitionDate",
                                        "sort": 1,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "disabled": true,
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Submission Date",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "submitionDate",
                                    "description": "Submition Date",
                                    "name": "submitionDate",
                                    "required": false,
                                    "attributes": {
                                        "id": "2572",
                                        "description": "creation_timestamp",
                                        "isRequired": false,
                                        "name": "creation_timestamp",
                                        "sortNo": 12
                                    },
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "767",
                                    "fieldAttributes": {
                                        "id": 4,
                                        "row": false,
                                        "sort": 6,
                                        "rules": {
                                            "required": "required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "helper": {
                                            "title": "Shipment Type",
                                            "placement": "top"
                                        },
                                        "options": [
                                            {
                                                "label": "Within CGIAR",
                                                "value": "within"
                                            },
                                            {
                                                "label": "External to CGIAR",
                                                "value": "external"
                                            }
                                        ],
                                        "inputProps": {
                                            "label": "Shipment Type"
                                        },
                                        "showInGrid": false,
                                        "defaultValue": "within"
                                    },
                                    "apiAttributesName": "",
                                    "description": "Shipment Type",
                                    "name": "shipmentType",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "4",
                                        "description": "RadioGroup",
                                        "name": "radiogroup",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "768",
                                    "fieldAttributes": {
                                        "id": 4,
                                        "row": false,
                                        "sort": 7,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "helper": {
                                            "title": "Material Type",
                                            "placement": "top"
                                        },
                                        "options": [
                                            {
                                                "label": "Propagative",
                                                "value": "propagative"
                                            },
                                            {
                                                "label": "Non Propagative",
                                                "value": "non-propagative"
                                            }
                                        ],
                                        "inputProps": {
                                            "label": "Material Type"
                                        },
                                        "showInGrid": false,
                                        "defaultValue": "propagative"
                                    },
                                    "apiAttributesName": "",
                                    "description": "Material Type",
                                    "name": "materialType",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "4",
                                        "description": "RadioGroup",
                                        "name": "radiogroup",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "760",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 1,
                                        "rules": {
                                            "required": "The program is required."
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "entity": "Program",
                                        "helper": {
                                            "title": "Program",
                                            "placement": "top"
                                        },
                                        "labels": [
                                            "name"
                                        ],
                                        "columns": [],
                                        "filters": [],
                                        "apiContent": [
                                            "id",
                                            "name",
                                            "code"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Program",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        }
                                    },
                                    "apiAttributesName": "program",
                                    "description": "Program",
                                    "name": "program",
                                    "required": true,
                                    "attributes": {
                                        "id": "2662",
                                        "description": "seed_code",
                                        "isRequired": true,
                                        "name": "seed_code",
                                        "sortNo": 29
                                    },
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "795",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "shipmentInformation",
                                        "sort": 7.2,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "SHIPMENT INFORMATION",
                                            "variant": "standard",
                                            "disabled": true,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": false,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "SHIPMENT INFORMATION",
                                    "name": "shipmentInformation",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "796",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 14,
                                        "rules": {
                                            "required": "This field is required."
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Requestor Name",
                                            "placement": "top"
                                        },
                                        "labels": [
                                            "id",
                                            "person.fullName"
                                        ],
                                        "columns": [
                                            {
                                                "name": "Requestor Name",
                                                "accessor": "requestor.person.givenName"
                                            },
                                            {
                                                "name": "Requestor Last Name",
                                                "accessor": "requestor.person.familyName"
                                            },
                                            {
                                                "name": "Requestor Email",
                                                "accessor": "requestor.email"
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "category.name",
                                                "mod": "EQ",
                                                "val": "Person"
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "person.familyName",
                                            "person.givenName",
                                            "person.fullName"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Requestor Name",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        }
                                    },
                                    "apiAttributesName": "requestor",
                                    "description": "Requestor Name",
                                    "name": "requestor",
                                    "required": true,
                                    "attributes": {
                                        "id": "2642",
                                        "description": "workflow_id",
                                        "isRequired": true,
                                        "name": "workflow_id",
                                        "sortNo": 11
                                    },
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "769",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 8,
                                        "rules": {
                                            "required": "The sender name is required."
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Sender Name",
                                            "placement": "top"
                                        },
                                        "labels": [
                                            "id",
                                            "person.fullName"
                                        ],
                                        "columns": [
                                            {
                                                "name": "Sender Name",
                                                "accessor": "sender.person.givenName"
                                            },
                                            {
                                                "name": "Sender Last Name",
                                                "accessor": "sender.person.familyName"
                                            },
                                            {
                                                "name": "Sender Email",
                                                "accessor": "sender.email"
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "category.name",
                                                "mod": "EQ",
                                                "val": "Person"
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "person.familyName",
                                            "person.givenName",
                                            "person.fullName"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Sender Name",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        }
                                    },
                                    "apiAttributesName": "sender",
                                    "description": "Sender Name",
                                    "name": "sender",
                                    "required": true,
                                    "attributes": {
                                        "id": "2643",
                                        "description": "design_ref",
                                        "isRequired": true,
                                        "name": "design_ref",
                                        "sortNo": 17
                                    },
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "794",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 7.1,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Shipment Delivery",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "",
                                                "mod": "EQ",
                                                "val": ""
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Shipment Delivery",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": false,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        },
                                        "defaultOptions": [
                                            {
                                                "label": "Foreign",
                                                "value": 1
                                            },
                                            {
                                                "label": "Domestic",
                                                "value": 2
                                            }
                                        ]
                                    },
                                    "apiAttributesName": "",
                                    "description": "Shipment Delivery",
                                    "name": "shipmentDelivery",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "766",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "otherMaterials",
                                        "sort": 5.1,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "helper": {
                                            "title": "Other Materials",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Other Materials",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false,
                                            "placeholder": "Please specify if other"
                                        },
                                        "showInGrid": false,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Other Materials",
                                    "name": "otherMaterials",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "801",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "recipientEmail",
                                        "sort": 20,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Recipient Email",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Recipient Email",
                                            "variant": "outlined",
                                            "disabled": true,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "email",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "email"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "recipientId",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Recipient Email",
                                    "name": "recipientEmail",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "771",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 10,
                                        "rules": {
                                            "required": "The sender address is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Sender Address",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Sender Address",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": true,
                                            "componentUI": "AddressForm",
                                            "parentControl": "senderId"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "addresses",
                                            "label": [
                                                "streetAddress",
                                                "region",
                                                "country.name",
                                                "location",
                                                "zipCode"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "addresses.id"
                                                },
                                                {
                                                    "accessor": "addresses.streetAddress"
                                                },
                                                {
                                                    "accessor": "addresses.region"
                                                },
                                                {
                                                    "accessor": "addresses.country.name"
                                                },
                                                {
                                                    "accessor": "addresses.location"
                                                },
                                                {
                                                    "accessor": "addresses.zipCode"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "senderId"
                                        }
                                    },
                                    "apiAttributesName": "",
                                    "description": "Sender Address",
                                    "name": "senderAddress",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "798",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 16,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Requestor Address",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "",
                                                "mod": "EQ",
                                                "val": ""
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Requestor Address",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": true,
                                            "componentUI": "AddressForm",
                                            "parentControl": "requestorId"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "addresses",
                                            "label": [
                                                "streetAddress",
                                                "zipCode",
                                                "location",
                                                "country.name"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "addresses.id"
                                                },
                                                {
                                                    "accessor": "addresses.location"
                                                },
                                                {
                                                    "accessor": "addresses.zipCode"
                                                },
                                                {
                                                    "accessor": "addresses.streetAddress"
                                                },
                                                {
                                                    "accessor": "addresses.country.name"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "requestorId"
                                        }
                                    },
                                    "apiAttributesName": "",
                                    "description": "Requestor Address",
                                    "name": "requestorAddress",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "802",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "name": "recipientAddress",
                                        "sort": 21,
                                        "rules": {
                                            "required": "This field is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Recipient Address",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Recipient Address",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": true,
                                            "componentUI": "AddressForm",
                                            "parentControl": "recipientId"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "addresses",
                                            "label": [
                                                "streetAddress",
                                                "zipCode",
                                                "location",
                                                "country.name"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "addresses.id"
                                                },
                                                {
                                                    "accessor": "addresses.streetAddress"
                                                },
                                                {
                                                    "accessor": "addresses.location"
                                                },
                                                {
                                                    "accessor": "addresses.zipCode"
                                                },
                                                {
                                                    "accessor": "addresses.country.name"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "recipientId",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Recipient Address",
                                    "name": "recipientAddress",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "762",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "shuId",
                                        "sort": 3,
                                        "rules": {
                                            "required": "The SHU ID is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "helper": {
                                            "title": "SHU-ID",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "SHU-ID",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "SHU-ID",
                                    "name": "shuId",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "765",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "shipmentPurposeTx",
                                        "sort": 4.1,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "helper": {
                                            "title": "Other Purpose",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Other Purpose",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false,
                                            "placeholder": "Please specify if other"
                                        },
                                        "showInGrid": false,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.fiel"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "shipmentPurposeDd",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            },
                                            "showControlRules": {
                                                "expectedValue": "Other",
                                                "parentControl": "shipmentPurposeDd"
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Other Purpose",
                                    "name": "shipmentPurposeTx",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1493",
                                    "fieldAttributes": {
                                        "id": 6,
                                        "sort": 22.1,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "Set Recipient same as requestor",
                                            "placement": "top"
                                        },
                                        "function": "setRecipientSameAsRequestor",
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Set Requestor Information to Recipient",
                                            "disabled": false
                                        },
                                        "showInGrid": false,
                                        "defaultValue": false
                                    },
                                    "apiAttributesName": "",
                                    "description": "Set Same As Requestor",
                                    "name": "sameAsRequestor",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "6",
                                        "description": "Switch",
                                        "name": "switch",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1597",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "status",
                                        "sort": 0,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "disabled": true,
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Status",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "className": "hidden",
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "status",
                                    "description": "Status",
                                    "name": "status",
                                    "required": false,
                                    "attributes": {
                                        "id": "2582",
                                        "description": "adminContact",
                                        "isRequired": true,
                                        "name": "admin_contact",
                                        "sortNo": 8
                                    },
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1598",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "totalItems",
                                        "sort": 2.1,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "disabled": true,
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Total Items",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "className": "hidden",
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "totalItems",
                                    "description": "Total Items",
                                    "name": "totalItems",
                                    "required": false,
                                    "attributes": {
                                        "id": "2582",
                                        "description": "adminContact",
                                        "isRequired": true,
                                        "name": "admin_contact",
                                        "sortNo": 8
                                    },
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1599",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "totalWeight",
                                        "sort": 2.2,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "disabled": true,
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Total Weight",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "className": "hidden",
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "totalWeight",
                                    "description": "Total Weight",
                                    "name": "totalWeight",
                                    "required": false,
                                    "attributes": {
                                        "id": "2582",
                                        "description": "adminContact",
                                        "isRequired": true,
                                        "name": "admin_contact",
                                        "sortNo": 8
                                    },
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1600",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "totalPackages",
                                        "sort": 2.3,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "disabled": true,
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Total Packages",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "className": "hidden",
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "totalPackages",
                                    "description": "Total Packages",
                                    "name": "totalPackages",
                                    "required": false,
                                    "attributes": {
                                        "id": "2582",
                                        "description": "adminContact",
                                        "isRequired": true,
                                        "name": "admin_contact",
                                        "sortNo": 8
                                    },
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "804",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 23,
                                        "rules": {
                                            "required": "This field is required."
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "entity": "ContactType",
                                        "helper": {
                                            "title": "Recipient Type",
                                            "placement": "top"
                                        },
                                        "labels": [
                                            "name"
                                        ],
                                        "columns": [
                                            {
                                                "accessor": "label"
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "category.name",
                                                "mod": "EQ",
                                                "val": "Person"
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Recipient Type",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        },
                                        "defaultOptions": [
                                            {
                                                "label": "PRI - Private Companies",
                                                "value": 1
                                            },
                                            {
                                                "label": "AI - Academic Institutions",
                                                "value": 2
                                            },
                                            {
                                                "label": "ARI - Advance Research Institutions",
                                                "value": 3
                                            },
                                            {
                                                "label": "CGCP - CGIAR Challenge Program",
                                                "value": 4
                                            },
                                            {
                                                "label": "DO - Development Organizations",
                                                "value": 5
                                            },
                                            {
                                                "label": "FI - Financing Institutions",
                                                "value": 6
                                            },
                                            {
                                                "label": "FO - Foundations",
                                                "value": 7
                                            },
                                            {
                                                "label": " GO - Government",
                                                "value": 8
                                            },
                                            {
                                                "label": " IARC - International Agricultural Research Centers",
                                                "value": 9
                                            },
                                            {
                                                "label": "IO - International Organizations, Individual, Media",
                                                "value": 10
                                            },
                                            {
                                                "label": "Individual",
                                                "value": 11
                                            },
                                            {
                                                "label": "Media",
                                                "value": 12
                                            },
                                            {
                                                "label": "NARES - National Agriculture Research and Extension Systems",
                                                "value": 13
                                            },
                                            {
                                                "label": " NGO - Non governmental organizations",
                                                "value": 14
                                            },
                                            {
                                                "label": "SRO - Subregional organization",
                                                "value": 15
                                            },
                                            {
                                                "label": "Others",
                                                "value": 16
                                            }
                                        ]
                                    },
                                    "apiAttributesName": "",
                                    "description": "Recipient Type",
                                    "name": "recipientType",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "800",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 19,
                                        "rules": {
                                            "required": "This field is required."
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Recipient Name",
                                            "placement": "top"
                                        },
                                        "labels": [
                                            "id",
                                            "person.fullName"
                                        ],
                                        "columns": [
                                            {
                                                "name": "Recipient Name",
                                                "accessor": "recipient.person.givenName"
                                            },
                                            {
                                                "name": "Recipient Last Name",
                                                "accessor": "recipient.person.familyName"
                                            },
                                            {
                                                "name": "Recipient Email",
                                                "accessor": "recipient.email"
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "category.name",
                                                "mod": "EQ",
                                                "val": "Person"
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "person.givenName",
                                            "person.familyName",
                                            "person.fullName"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Recipient Name",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        }
                                    },
                                    "apiAttributesName": "recipient",
                                    "description": "Recipient Name",
                                    "name": "recipient",
                                    "required": true,
                                    "attributes": {
                                        "id": "2606",
                                        "description": "node_type_id",
                                        "isRequired": true,
                                        "name": "node_type_id",
                                        "sortNo": 27
                                    },
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "803",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 22,
                                        "rules": {
                                            "required": "The recipient institution is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Recipient Institution",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [],
                                        "disabled": false,
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Recipient Institution",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "",
                                            "label": [
                                                "institution.institution.legalName",
                                                "institution.institution.commonName"
                                            ],
                                            "entity": "Hierarchy",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "institution.institution.legalName"
                                                },
                                                {
                                                    "accessor": "institution.institution.commonName"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "contact.id",
                                            "customFilters": [
                                                {
                                                    "col": "institution.category.name",
                                                    "mod": "EQ",
                                                    "val": "Institution"
                                                }
                                            ],
                                            "parentControl": "recipientId"
                                        }
                                    },
                                    "apiAttributesName": "",
                                    "description": "Recipient Institution",
                                    "name": "recipientInstitution",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "799",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 18,
                                        "rules": {
                                            "required": "The requestor institution is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Requestor Institution",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [],
                                        "disabled": false,
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Requestor Institution",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "",
                                            "label": [
                                                "institution.institution.legalName",
                                                "institution.institution.commonName"
                                            ],
                                            "entity": "Hierarchy",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "institution.institution.legalName"
                                                },
                                                {
                                                    "accessor": "institution.institution.commonName"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "contact.id",
                                            "customFilters": [
                                                {
                                                    "col": "institution.category.name",
                                                    "mod": "EQ",
                                                    "val": "Institution"
                                                }
                                            ],
                                            "parentControl": "requestorId"
                                        }
                                    },
                                    "apiAttributesName": "",
                                    "description": "Requestor Institution",
                                    "name": "requestorInstitution",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "793",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 13,
                                        "rules": {
                                            "required": "The sender institution is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            2,
                                            2
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Sender Institution",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [],
                                        "disabled": false,
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Sender Institution",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "cs",
                                            "field": "",
                                            "label": [
                                                "institution.institution.legalName",
                                                "institution.institution.commonName"
                                            ],
                                            "entity": "Hierarchy",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "institution.institution.legalName"
                                                },
                                                {
                                                    "accessor": "institution.institution.commonName"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "contact.id",
                                            "customFilters": [
                                                {
                                                    "col": "institution.category.name",
                                                    "mod": "EQ",
                                                    "val": "Institution"
                                                }
                                            ],
                                            "parentControl": "senderId"
                                        }
                                    },
                                    "apiAttributesName": "",
                                    "description": "Sender Institution",
                                    "name": "senderInstitution",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                }
                            ]
                        },
                        {
                            "id": "21029",
                            "dependOn": {
                                "edges": [
                                    "20992"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 201.8344476037248,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been created, the status is Draft"
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "status": "364",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Draft",
                            "description": "Draft",
                            "help": "Draft",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21030",
                            "dependOn": {
                                "edges": [
                                    "20992"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 324.6358799985893,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "New request has been created",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been created by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Send Notification",
                            "description": "Send Notification",
                            "help": "Send Notification",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21032",
                            "dependOn": {
                                "edges": [
                                    "20992"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 111.87579472472771,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21025",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Move to Items Stage",
                            "description": "Move to Items Stage",
                            "help": "Move to Items Stage",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 562
                    },
                    "id": 10736,
                    "description": "Submitted",
                    "name": "Submitted",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 1.5178823574395892,
                            "y": 293.2128797217697
                        }
                    },
                    "sequence": 4,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21072",
                            "dependOn": {
                                "edges": [
                                    "21070"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 233.35709804663122,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "Request has been submitted",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been submitted by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "NOTIFICATION",
                            "description": "NOTIFICATION",
                            "help": "NOTIFICATION",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21132",
                            "dependOn": {
                                "edges": [
                                    "21070"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 373.28261876978536,
                                    "y": 20.647660966052626
                                }
                            },
                            "define": {},
                            "name": "SUBMITDATE",
                            "description": "SUBMITDATE",
                            "help": "SUBMITDATE",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "5",
                                "name": "submit",
                                "description": "Submit"
                            },
                            "process": {
                                "id": "1",
                                "isBackground": false,
                                "name": "Generate Form",
                                "code": "FORM",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21070",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 0,
                                    "y": 29.56689223140529
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been changed to Submitted."
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "ERROR",
                                        "functions": "validateItems",
                                        "onSuccess": "SUCCESS"
                                    }
                                },
                                "status": "365",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "customSecurityRules": [
                                    {
                                        "actor": "Sender",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Requestor",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "Recipient",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": false,
                                        "allowViewProgress": false
                                    },
                                    {
                                        "actor": "Creator",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "SHU",
                                        "allowEdit": true,
                                        "allowView": false,
                                        "allowDelete": true,
                                        "allowAddNotes": true,
                                        "allowViewProgress": false
                                    }
                                ]
                            },
                            "name": "SUBMIT",
                            "description": "SUBMIT",
                            "help": "SUBMIT",
                            "sequence": 1,
                            "icon": "Publish",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21071",
                            "dependOn": {
                                "edges": [
                                    "21070"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 124.2679251235968,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21027",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "MOVE TO PROCESS IRRI",
                            "description": "MOVE TO PROCESS IRRI",
                            "help": "MOVE TO PROCESS IRRI",
                            "sequence": 3,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21635",
                            "dependOn": {
                                "edges": [
                                    "21070"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 112.99999999999989,
                                    "y": 101
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "11",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": true,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": false,
                                    "templates": [
                                        {
                                            "format": "",
                                            "template": ""
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "Send email submitted irri",
                            "description": "Send email submitted irri",
                            "help": "Send email submitted irri",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 562
                    },
                    "id": 10731,
                    "description": "Items Stage",
                    "name": "Items Stage",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 0,
                            "y": 99.63665414197897
                        }
                    },
                    "sequence": 2,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21025",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 18.49815331776017,
                                    "y": 27.338257819306307
                                }
                            },
                            "define": {
                                "id": 8,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": "All Ok."
                                    }
                                },
                                "rules": {
                                    "columns": [
                                        {
                                            "name": "testCode",
                                            "alias": "RSHT",
                                            "hidden": false
                                        },
                                        {
                                            "name": "referenceNumber",
                                            "hidden": true
                                        },
                                        {
                                            "name": "receivedDate",
                                            "hidden": true
                                        },
                                        {
                                            "name": "id",
                                            "hidden": true
                                        },
                                        {
                                            "name": "mlsAncestors",
                                            "hidden": true
                                        },
                                        {
                                            "name": "status",
                                            "hidden": true
                                        },
                                        {
                                            "name": "isDangerous",
                                            "hidden": true
                                        }
                                    ],
                                    "bulkOptions": [
                                        "Quantities",
                                        "Sync from CB"
                                    ]
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "disabled": false,
                                "component": "Items",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Items",
                            "description": "Items",
                            "help": "Items",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "7",
                                "name": "External",
                                "description": "External Component"
                            },
                            "process": {
                                "id": "8",
                                "isBackground": false,
                                "name": "External",
                                "code": "EXTERNAL",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21066",
                            "dependOn": {
                                "edges": [
                                    "21025"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 216.51034863102143,
                                    "y": 22.142635414609003
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been changed to Created"
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "ERROR",
                                        "functions": "checkItems",
                                        "onSuccess": "ALL OK"
                                    }
                                },
                                "status": "366",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "CREATED",
                            "description": "CREATED",
                            "help": "CREATED",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21067",
                            "dependOn": {
                                "edges": [
                                    "21066"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 363.13306283245595,
                                    "y": 17.226629899540058
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21026",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "MOVE TO DOCUMENTS",
                            "description": "MOVE TO DOCUMENTS",
                            "help": "MOVE TO DOCUMENTS",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 562
                    },
                    "id": 10768,
                    "description": "Phytosanitary Review IRRI",
                    "name": "Phytosanitary Review IRRI",
                    "dependOn": {
                        "edges": [],
                        "width": 800,
                        "height": 180,
                        "position": {
                            "x": 42.12825099196152,
                            "y": 580.4177011387388
                        }
                    },
                    "sequence": 4,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21783",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 20.206065651716017,
                                    "y": 48.54199062118346
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21027",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Send to Phytosanitary Review",
                            "description": "Send to Phytosanitary Review",
                            "help": "Send to Phytosanitary Review",
                            "sequence": 1,
                            "icon": "Mail",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember",
                                                "__typename": "SecurityRule"
                                            }
                                        ],
                                        "isSystem": true,
                                        "__typename": "Role",
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21784",
                            "dependOn": {
                                "edges": [
                                    "21783"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 215.57932241313756,
                                    "y": 7.717728014319164
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "Phytosanitary Review",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "An Email notification was sent for Phytosanitary Review for [requestCode]. Report was attached.",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "PHYTOSANITARY REVIEW NOTIFICATION",
                            "description": "PHYTOSANITARY REVIEW NOTIFICATION",
                            "help": "PHYTOSANITARY REVIEW NOTIFICATION",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember",
                                                "__typename": "SecurityRule"
                                            }
                                        ],
                                        "isSystem": true,
                                        "__typename": "Role",
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21785",
                            "dependOn": {
                                "edges": [
                                    "21783"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 220.76081322089476,
                                    "y": 98.04106771946863
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "42",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": true,
                                    "templates": [
                                        {
                                            "format": "pdf",
                                            "template": "sys_data_items_irri"
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "EMAIL PHYTOSANITARY IRRI",
                            "description": "EMAIL PHYTOSANITARY IRRI",
                            "help": "EMAIL PHYTOSANITARY IRRI",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember",
                                                "__typename": "SecurityRule"
                                            }
                                        ],
                                        "isSystem": true,
                                        "__typename": "Role",
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        }
                    ]
                }
            ]
        },
        {
            "sequence": 2,
            "id": 563,
            "icon": null,
            "description": "Processing",
            "name": "Processing",
            "dependOn": {
                "edges": [
                    "562"
                ],
                "width": 800,
                "height": 400,
                "position": {
                    "x": 190.55695291191273,
                    "y": 456.32388612750333
                }
            },
            "workflow": {
                "id": 562
            },
            "workflowViewType": {
                "id": "1",
                "name": "N/A"
            },
            "stages": [
                {
                    "phase": {
                        "id": 563
                    },
                    "id": 10734,
                    "description": "Approvals",
                    "name": "Approvals",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 27.418164099041178,
                            "y": 141.77891994664208
                        }
                    },
                    "sequence": 2,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21104",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 232.2242206884,
                                    "y": 24.5
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request was rejected. All shipment items has been changed to REJECTED"
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "ERROR",
                                        "functions": "markAllItemsAsRejected",
                                        "onSuccess": "OK"
                                    }
                                },
                                "status": "400",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "REJECT",
                            "description": "REJECT",
                            "help": "REJECT",
                            "sequence": 3,
                            "icon": "Clear",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21105",
                            "dependOn": {
                                "edges": [
                                    "21104"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 349.0630800582783,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "Request has been rejected",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been rejected by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "REJECT NOT",
                            "description": "REJECT NOT",
                            "help": "REJECT NOT",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21106",
                            "dependOn": {
                                "edges": [
                                    "21102"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 500,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21027",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request is on Hold status"
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "BACK TO PROCESS",
                            "description": "BACK TO PROCESS",
                            "help": "BACK TO PROCESS",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21099",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 0,
                                    "y": 33.999999999999886
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been changed to Processing Completed."
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "validateItems",
                                        "onSuccess": ""
                                    }
                                },
                                "status": "367",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "customSecurityRules": [
                                    {
                                        "actor": "Sender",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Requestor",
                                        "allowEdit": false,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "Recipient",
                                        "allowEdit": false,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "Creator",
                                        "allowEdit": false,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "SHU",
                                        "allowView": false,
                                        "allowViewProgress": false
                                    }
                                ]
                            },
                            "name": "Approve",
                            "description": "Approve",
                            "help": "Approve",
                            "sequence": 1,
                            "icon": "CheckCircle",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21100",
                            "dependOn": {
                                "edges": [
                                    "21099"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 107.90138183599561,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "Request has been processed",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been changed to Processing Completed by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Notification Approve",
                            "description": "Notification Approve",
                            "help": "Notification Approve",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21101",
                            "dependOn": {
                                "edges": [
                                    "21099"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 126.52905331269392,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21107",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Move to Additional",
                            "description": "Move to Additional",
                            "help": "Move to Additional",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21102",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 454.40359723912,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been changed to On Hold"
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "status": "368",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "customSecurityRules": [
                                    {
                                        "actor": "Sender",
                                        "allowEdit": true,
                                        "allowView": true,
                                        "allowDelete": true,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Recipient",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "SHU",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Requestor",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    }
                                ]
                            },
                            "name": "ON HOLD",
                            "description": "ON HOLD",
                            "help": "ON HOLD",
                            "sequence": 2,
                            "icon": "Timer",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21103",
                            "dependOn": {
                                "edges": [
                                    "21104"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 346.43683670005566,
                                    "y": 0.9584714842469566
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21026",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "Please review and submit again"
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "BACK CREATED",
                            "description": "BACK CREATED",
                            "help": "BACK CREATED",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21500",
                            "dependOn": {
                                "edges": [
                                    "21101"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 245.45871533986679,
                                    "y": 16.1905434383782
                                }
                            },
                            "define": {
                                "id": 9,
                                "form": "21107",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Modal IRRI",
                            "description": "Modal popup for IRRI",
                            "help": "Modal popup for IRRI",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "9",
                                "isBackground": false,
                                "name": "Modal Popup",
                                "code": "POPUP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21502",
                            "dependOn": {
                                "edges": [
                                    "21102"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 661.1921129849724,
                                    "y": 70.29460514722814
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "The request has been changed to On-Hold",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been changed to On-Hold by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "HOLD NOTIFICATION",
                            "description": "HOLD NOTIFICATION",
                            "help": "HOLD NOTIFICATION",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21637",
                            "dependOn": {
                                "edges": [
                                    "21099"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 117.97764536972113,
                                    "y": 126.55390493500886
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "10",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": true,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": false,
                                    "templates": [
                                        {
                                            "format": "",
                                            "template": ""
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "Send email processing completed irri",
                            "description": "Send email processing completed irri",
                            "help": "Send email processing completed irri",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21638",
                            "dependOn": {
                                "edges": [
                                    "21104"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 374.13737853405996,
                                    "y": 130
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "10",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": true,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": false,
                                    "templates": [
                                        {
                                            "format": "",
                                            "template": ""
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "Send email rejected irri",
                            "description": "Send email rejected irri",
                            "help": "Send email rejected irri",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21639",
                            "dependOn": {
                                "edges": [
                                    "21102"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 663.6093639933126,
                                    "y": 130
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "10",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": true,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": false,
                                    "templates": [
                                        {
                                            "format": "",
                                            "template": ""
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "Send email on-hold irri",
                            "description": "Send email on-hold irri",
                            "help": "Send email on-hold irri",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21519",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 383.9999999999999,
                                    "y": 26
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_proforma_invoice",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Proforma Invoice",
                            "description": "Print Proforma Invoice",
                            "help": "Print Proforma Invoice",
                            "sequence": 4,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 563
                    },
                    "id": 10733,
                    "description": "Process Stage",
                    "name": "Process Stage",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 163.93678502128887,
                            "y": 4.398615785234142
                        }
                    },
                    "sequence": 1,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21074",
                            "dependOn": {
                                "edges": [
                                    "21027"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 152.27170804494926,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21099",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "MOVE TO APPROVALS",
                            "description": "MOVE TO APPROVALS",
                            "help": "MOVE TO APPROVALS",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21027",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 1.5203213077836608,
                                    "y": 30.46652523920727
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The Shipment status has changed to Processing."
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "validateRequiredDocuments",
                                        "onSuccess": ""
                                    }
                                },
                                "status": "370",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "customSecurityRules": [
                                    {
                                        "actor": "Sender",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    }
                                ]
                            },
                            "name": "Process Shipment",
                            "description": "Process Shipment",
                            "help": "Process Shipment",
                            "sequence": 1,
                            "icon": "Launch",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21075",
                            "dependOn": {
                                "edges": [
                                    "21027"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 240.03894608154286,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "Request has been sent to approval process",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been processed by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "NOTIFICATION APPROVAL",
                            "description": "NOTIFICATION APPROVAL",
                            "help": "NOTIFICATION APPROVAL",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21523",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 429.8534889233804,
                                    "y": 24.23281836932324
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_proforma_invoice",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Proforma Invoice",
                            "description": "Print Proforma Invoice",
                            "help": "Print Proforma Invoice",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21636",
                            "dependOn": {
                                "edges": [
                                    "21027"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 116.20716872212711,
                                    "y": 109.19743830413654
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "10",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": true,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": false,
                                    "templates": [
                                        {
                                            "format": "",
                                            "template": ""
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "Send email processing irri",
                            "description": "Send email processing irri",
                            "help": "Send email processing irri",
                            "sequence": 3,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21741",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 347.49208182885036,
                                    "y": 80.99417078020042
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": true,
                                "disabled": false,
                                "template": "sys_non_gm_certificate_irri",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Non GM Certificate",
                            "description": "Print Non GM Certificate",
                            "help": "Print Non GM Certificate",
                            "sequence": 3,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21742",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 469.7468665729747,
                                    "y": 6.95254058305477
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "specificMaterials",
                                            "columnValue": "GMO"
                                        }
                                    ],
                                    "validationFunction": "validateTemplateShow"
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": true,
                                "disabled": false,
                                "template": "sys_gm_certificate",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print GM Certificate",
                            "description": "Print GM Certificate",
                            "help": "Print GM Certificate",
                            "sequence": 3,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21743",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 579.2673317642889,
                                    "y": 5.525989705914981
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": true,
                                "disabled": false,
                                "template": "sys_irri_shu_service_agreement_form",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print SHU Service Agreement Form",
                            "description": "Print SHU Service Agreement Form",
                            "help": "Print SHU Service Agreement Form",
                            "sequence": 4,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21744",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 553.716055521134,
                                    "y": 90.84252060252652
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": true,
                                "disabled": false,
                                "template": "sys_phytosanitary_certificate_irri",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Phytosanitary Certificate",
                            "description": "Print Phytosanitary Certificate",
                            "help": "Print Phytosanitary Certificate",
                            "sequence": 5,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 563
                    },
                    "id": 10735,
                    "description": "Sent Stage",
                    "name": "Sent Stage",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 28.384101603669023,
                            "y": 269.1950575822077
                        }
                    },
                    "sequence": 3,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21107",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 10,
                                    "y": 33.35299238390098
                                }
                            },
                            "define": {
                                "id": 1,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "Additional Information",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Additional Information",
                            "description": "Additional Information",
                            "help": "Additional Information",
                            "sequence": 4,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "2",
                                "name": "Form"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "1",
                                "isBackground": false,
                                "name": "Generate Form",
                                "code": "FORM",
                                "path": null
                            },
                            "nodeCFs": [
                                {
                                    "id": "911",
                                    "fieldAttributes": {
                                        "id": 3,
                                        "name": "documentSignedDate",
                                        "sort": 103,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Document Signed Date",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "label": "Document Signed Date",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Document Signed Date",
                                    "name": "documentSignedDate",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "3",
                                        "description": "DatePicker",
                                        "name": "datepicker",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "904",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "",
                                        "sort": 100,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            12,
                                            12,
                                            12
                                        ],
                                        "helper": {
                                            "title": "",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "DELIVERY AND SHIPPING INFORMATION",
                                            "variant": "standard",
                                            "disabled": true,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": false,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "ADDITIONAL INFORMATION",
                                    "name": "AdditionalInfo",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "905",
                                    "fieldAttributes": {
                                        "id": 3,
                                        "name": "documentGeneratedDate",
                                        "sort": 104,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "DocumentGenerated Date",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "label": "Document Generated Date",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Document Generate Date",
                                    "name": "documentGeneratedDate",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "3",
                                        "description": "DatePicker",
                                        "name": "datepicker",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "908",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "airBillNumber",
                                        "sort": 121,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "helper": {
                                            "title": "Air Bill Number",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Air Bill Number",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "showIfValue": "By Air",
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "typeOfCuries",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Air Bill Number",
                                    "name": "airBillNumber",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "910",
                                    "fieldAttributes": {
                                        "id": 3,
                                        "name": "shippedDateV2",
                                        "sort": 105,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Shipped Date",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "label": "Shipped Date",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Shipped Date",
                                    "name": "shippedDateV2",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "3",
                                        "description": "DatePicker",
                                        "name": "datepicker",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "912",
                                    "fieldAttributes": {
                                        "id": 3,
                                        "name": "receivedDate",
                                        "sort": 101,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            3,
                                            3
                                        ],
                                        "helper": {
                                            "title": "Received Date",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "label": "Received Date",
                                            "variant": "outlined"
                                        },
                                        "showInGrid": true,
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Received Date",
                                    "name": "receivedDate",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "3",
                                        "description": "DatePicker",
                                        "name": "datepicker",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "914",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "authorizedSignatory",
                                        "sort": 104,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "helper": {
                                            "title": "IRRI Authorized Signatory",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "IRRI Authorized Signatory",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Auhtorized Signatory",
                                    "name": "authorizedSignatory",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1124",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "invoiceNumber",
                                        "sort": 110,
                                        "rules": {
                                            "required": "The invoice number is required."
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "helper": {
                                            "title": "Invoice Number",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Invoice Number",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": true,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "cs-invoice-number",
                                                "applyRule": true
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Invoice Number",
                                    "name": "invoiceNumber",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1127",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 113,
                                        "rules": {
                                            "required": "The country of origin is required"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "entity": "Country",
                                        "helper": {
                                            "title": "Country of Origin",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": "name"
                                            }
                                        ],
                                        "filters": [],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Country of Origin",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        }
                                    },
                                    "apiAttributesName": "",
                                    "description": "Country of Origin",
                                    "name": "countryOfOrigin",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1134",
                                    "fieldAttributes": {
                                        "id": 5,
                                        "sort": 119,
                                        "rules": {
                                            "required": "Please selected the type of courier"
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "entity": "Contact",
                                        "helper": {
                                            "title": "Type of Curies",
                                            "placement": "top"
                                        },
                                        "columns": [
                                            {
                                                "accessor": ""
                                            }
                                        ],
                                        "filters": [
                                            {
                                                "col": "",
                                                "mod": "EQ",
                                                "val": ""
                                            }
                                        ],
                                        "apiContent": [
                                            "id",
                                            "name"
                                        ],
                                        "inputProps": {
                                            "color": "primary",
                                            "label": "Type of Courier",
                                            "variant": "outlined"
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "person",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "category.name",
                                            "customFilters": [],
                                            "parentControl": "control name"
                                        },
                                        "defaultOptions": [
                                            {
                                                "label": "By Air",
                                                "value": 1
                                            },
                                            {
                                                "label": "By Road",
                                                "value": 2
                                            },
                                            {
                                                "label": "By Sea",
                                                "value": 3
                                            },
                                            {
                                                "label": "By Rail",
                                                "value": 4
                                            },
                                            {
                                                "label": "Picked physically by recipient",
                                                "value": 5
                                            }
                                        ]
                                    },
                                    "apiAttributesName": "",
                                    "description": "Type of Curies",
                                    "name": "typeOfCuries",
                                    "required": true,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "5",
                                        "description": "Select",
                                        "name": "select",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1237",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "unitValue",
                                        "sort": 111.2,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "helper": {
                                            "title": "Unit Value",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Unit Value",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Unit Value",
                                    "name": "unitValue",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1239",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "species",
                                        "sort": 111.4,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "helper": {
                                            "title": "Species",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Species",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Species",
                                    "name": "species",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1290",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "specificTest",
                                        "sort": 105.1,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "helper": {
                                            "title": "Specific test to be done",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Specific Test",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Specific Test",
                                    "name": "specificTest",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1291",
                                    "fieldAttributes": {
                                        "id": 4,
                                        "row": false,
                                        "sort": 105.2,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            6,
                                            6
                                        ],
                                        "helper": {
                                            "title": "Cost",
                                            "placement": "top"
                                        },
                                        "options": [
                                            {
                                                "label": "With cost",
                                                "value": "with-cost"
                                            },
                                            {
                                                "label": "Without cost",
                                                "value": "no-cost"
                                            }
                                        ],
                                        "inputProps": {
                                            "label": "Cost"
                                        },
                                        "showInGrid": true,
                                        "defaultValue": "no-cost"
                                    },
                                    "apiAttributesName": "",
                                    "description": "cost",
                                    "name": "cost",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "4",
                                        "description": "RadioGroup",
                                        "name": "radiogroup",
                                        "type": "String"
                                    }
                                },
                                {
                                    "id": "1125",
                                    "fieldAttributes": {
                                        "id": 1,
                                        "name": "valueOfProduct",
                                        "sort": 111,
                                        "rules": {
                                            "required": ""
                                        },
                                        "sizes": [
                                            12,
                                            12,
                                            6,
                                            4,
                                            4
                                        ],
                                        "helper": {
                                            "title": "Total value of the Product",
                                            "placement": "top"
                                        },
                                        "inputProps": {
                                            "rows": 1,
                                            "label": "Total value of the Product(Value only for customs purpose)",
                                            "variant": "outlined",
                                            "disabled": false,
                                            "fullWidth": true,
                                            "multiline": false,
                                            "startAdornment": "$"
                                        },
                                        "modalPopup": {
                                            "show": false,
                                            "componentUI": ""
                                        },
                                        "showInGrid": true,
                                        "defaultRules": {
                                            "uri": "",
                                            "field": "",
                                            "label": [
                                                "familyName",
                                                "givenName"
                                            ],
                                            "entity": "Contact",
                                            "apiContent": [
                                                {
                                                    "accessor": "id"
                                                },
                                                {
                                                    "accessor": "another.field"
                                                }
                                            ],
                                            "applyRules": false,
                                            "columnFilter": "id",
                                            "customFilters": [],
                                            "parentControl": "control name",
                                            "sequenceRules": {
                                                "ruleName": "",
                                                "applyRule": false
                                            }
                                        },
                                        "defaultValue": ""
                                    },
                                    "apiAttributesName": "",
                                    "description": "Value of the product",
                                    "name": "valueOfProduct",
                                    "required": false,
                                    "attributes": null,
                                    "cfType": {
                                        "id": "1",
                                        "description": "TextField",
                                        "name": "TextField",
                                        "type": "String"
                                    }
                                }
                            ]
                        },
                        {
                            "id": "21108",
                            "dependOn": {
                                "edges": [
                                    "21107"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 142.18416118339974,
                                    "y": 12.120377843594952
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been changed to Sent."
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "status": "369",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "customSecurityRules": [
                                    {
                                        "actor": "Sender",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": true,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Requestor",
                                        "allowEdit": false,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "Recipient",
                                        "allowEdit": false,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "Creator",
                                        "allowEdit": false,
                                        "allowDelete": false
                                    },
                                    {
                                        "actor": "SHU",
                                        "allowDelete": false
                                    }
                                ]
                            },
                            "name": "Change Done",
                            "description": "Change Done",
                            "help": "Change Done",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21109",
                            "dependOn": {
                                "edges": [
                                    "21107"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 279.28982908395653,
                                    "y": 45.103821289382495
                                }
                            },
                            "define": {
                                "id": 6,
                                "node": "21110",
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "action": "",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Move To Done",
                            "description": "Move To Done",
                            "help": "Move To Done",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "6",
                                "isBackground": false,
                                "name": "Move",
                                "code": "MOVE",
                                "path": "workflow/move/node"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21505",
                            "dependOn": {
                                "edges": [
                                    "21108"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 366.2671359086813,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 7,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "title": "The request has been Sent",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "message": "[date] Outgoing seed shipment [requestCode] has been changed to Sent by [currentUser]",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Sent Notification",
                            "description": "Sent Notification",
                            "help": "Sent Notification",
                            "sequence": 2,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "7",
                                "isBackground": true,
                                "name": "Send Notification",
                                "code": "NOTIFICATION",
                                "path": "workflow/send/notification"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21524",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 467.9095962937612,
                                    "y": 36.07119098726184
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_proforma_invoice",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Proforma Invoice",
                            "description": "Print Proforma Invoice",
                            "help": "Print Proforma Invoice",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21640",
                            "dependOn": {
                                "edges": [
                                    "21108"
                                ],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 289.6802486597272,
                                    "y": 113.43491774985182
                                }
                            },
                            "define": {
                                "id": 4,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "email": "10",
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "Error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "contacts": [
                                    {
                                        "contact": ""
                                    }
                                ],
                                "disabled": false,
                                "sendToSHU": true,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "attachments": {
                                    "attach": false,
                                    "templates": [
                                        {
                                            "format": "",
                                            "template": ""
                                        }
                                    ]
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "sendToSender": true,
                                "sendToRecipient": true,
                                "sendToRequestor": true
                            },
                            "name": "Send email sent irri",
                            "description": "Send email sent irri",
                            "help": "Send email sent irri",
                            "sequence": 3,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "4",
                                "name": "process",
                                "description": "Process"
                            },
                            "process": {
                                "id": "4",
                                "isBackground": false,
                                "name": "Send Email",
                                "code": "EMAIL",
                                "path": "workflow/send/email"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21745",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 534.5711239886996,
                                    "y": 71.69758907009225
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": true,
                                "disabled": false,
                                "template": "sys_inspection_or_phytosanitary_certificate",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Inspection Or Phytosanitary Certificate",
                            "description": "Print Inspection Or Phytosanitary Certificate",
                            "help": "Print Inspection Or Phytosanitary Certificate",
                            "sequence": 2,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21746",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 540.9527678328444,
                                    "y": 130
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": true,
                                "disabled": false,
                                "template": "sys_irri_transgenic_material_transfer_form",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Transgenic Material Transfer Form",
                            "description": "Print Transgenic Material Transfer Form",
                            "help": "Print Transgenic Material Transfer Form",
                            "sequence": 3,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        }
                    ]
                }
            ]
        },
        {
            "sequence": 3,
            "id": 564,
            "icon": null,
            "description": "Close",
            "name": "Close",
            "dependOn": {
                "edges": [
                    "563"
                ],
                "width": 800,
                "height": 400,
                "position": {
                    "x": 1210.9686921283705,
                    "y": 470.8050141846571
                }
            },
            "workflow": {
                "id": 562
            },
            "workflowViewType": {
                "id": "1",
                "name": "N/A"
            },
            "stages": [
                {
                    "phase": {
                        "id": 564
                    },
                    "id": 10737,
                    "description": "Close",
                    "name": "Close",
                    "dependOn": {
                        "edges": [],
                        "width": 600,
                        "height": 100,
                        "position": {
                            "x": 110.18645898775702,
                            "y": 49.2578780815719
                        }
                    },
                    "sequence": 1,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "21110",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 61.148453614317475,
                                    "y": 26.130721646651637
                                }
                            },
                            "define": {
                                "id": 5,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": true,
                                        "message": "The request has been changed to Done status."
                                    }
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "status": "452",
                                "disabled": false,
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                },
                                "customSecurityRules": [
                                    {
                                        "actor": "sender",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": false,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Requestor",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": false,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Recipient",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": false,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "SHU",
                                        "allowEdit": false,
                                        "allowView": true,
                                        "allowDelete": false,
                                        "allowAddNotes": false,
                                        "allowViewProgress": true
                                    },
                                    {
                                        "actor": "Creator",
                                        "allowEdit": false,
                                        "allowDelete": false,
                                        "allowAddNotes": false
                                    }
                                ]
                            },
                            "name": "Done",
                            "description": "Done",
                            "help": "Done",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "5",
                                "isBackground": false,
                                "name": "Change Status",
                                "code": "CHANGE",
                                "path": "workflow/change/status"
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21508",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 275.9999999999999,
                                    "y": 22
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "specificMaterials",
                                            "columnValue": "GMO"
                                        }
                                    ],
                                    "validationFunction": "validateTemplateShow"
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "error",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_package_list_irri",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Package List",
                            "description": "Print Package List",
                            "help": "Print Package List",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21521",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 373.7719759624955,
                                    "y": 27.264251502343996
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_proforma_invoice",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Proforma Invoice",
                            "description": "Print Proforma Invoice",
                            "help": "Print Proforma Invoice",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21747",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 477.13632939139666,
                                    "y": 24.473424623420897
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_inspection_or_phytosanitary_certificate",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Inspection Or Phytosanitary Certificate",
                            "description": "Print Inspection Or Phytosanitary Certificate",
                            "help": "Print Inspection Or Phytosanitary Certificate",
                            "sequence": 3,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "21748",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 480.9653156978836,
                                    "y": 83.18454798955281
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": false,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_irri_transgenic_material_transfer_form",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "Print Transgenic Material Transfer Form",
                            "description": "Print Transgenic Material Transfer Form",
                            "help": "Print Transgenic Material Transfer Form",
                            "sequence": 5,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "isSystem": true,
                                        "__typename": "Role"
                                    }
                                ]
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        }
                    ]
                },
                {
                    "phase": {
                        "id": 564
                    },
                    "id": 10971,
                    "description": "print reports",
                    "name": "print reports",
                    "dependOn": {
                        "edges": [],
                        "width": 800,
                        "height": 130,
                        "position": {
                            "x": 56,
                            "y": 218
                        }
                    },
                    "sequence": 1,
                    "icon": null,
                    "workflowViewType": {
                        "id": "1",
                        "name": "N/A"
                    },
                    "nodes": [
                        {
                            "id": "22996",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 10,
                                    "y": 50
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_jircas_omta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_jircas_omta",
                            "description": "Print JIRCAS OMTA",
                            "help": "Print JIRCAS OMTA",
                            "sequence": 1,
                            "icon": null,
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember",
                                                "__typename": "SecurityRule"
                                            }
                                        ],
                                        "isSystem": true,
                                        "__typename": "Role",
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22918",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 135.40468310548772,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_asean_omta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_asean_omta",
                            "description": "Print ASEAN OMTA",
                            "help": "Print ASEAN OMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22917",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 232.23614727554832,
                                    "y": 78.57321893542758
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_narvi_omta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_narvi_omta",
                            "description": "Print NARVI OMTA",
                            "help": "Print NARVI OMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22916",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 231.44244674956417,
                                    "y": 42.856695266143106
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_hrdc_omta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_hrdc_omta",
                            "description": "Print HRDC OMTA",
                            "help": "Print HRDC OMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22914",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 231.2840562366623,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_irri_omta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_irri_omta",
                            "description": "Print IRRI OMTA",
                            "help": "Print IRRI OMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22913",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 325.7232768899662,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_global_mta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_global_mta",
                            "description": "Print Global MTA",
                            "help": "Print Global MTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22912",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 422.94012168013205,
                                    "y": 80
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_biological_mta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_biological_mta",
                            "description": "Print Biological MTA",
                            "help": "Print Biological MTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22905",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 513.178225585159,
                                    "y": 80
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_mta_for_outgoing_transgenic_materials",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_mta_for_outgoing_transgenic_materials",
                            "description": "Print MTA for Outgoing Transgenic Materials",
                            "help": "Print MTA for Outgoing Transgenic Materials",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22904",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 513.1782255851595,
                                    "y": 44.77669662022333
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_tlsg_reta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_tlsg_reta",
                            "description": "Print TLSG RETA",
                            "help": "Print TLSG RETA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22903",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 512.3076750218638,
                                    "y": 1.2491684554170774
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_hrdc_cmta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_hrdc_cmta",
                            "description": "Print HRDC CMTA",
                            "help": "Print HRDC CMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22902",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 607.1976864211406,
                                    "y": 80
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_hrdc_reta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_hrdc_reta",
                            "description": "Print HRDC RETA",
                            "help": "Print HRDC RETA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22901",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 607.197686421141,
                                    "y": 43.436876341562765
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_native_traits_cmta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_native_traits_cmta",
                            "description": "Print Native Traits CMTA",
                            "help": "Print Native Traits CMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22900",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 606.7408435994957,
                                    "y": 1.315027959689985
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_irri_cmta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_irri_cmta",
                            "description": "Print IRRI CMTA",
                            "help": "Print IRRI CMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember",
                                                "__typename": "SecurityRule"
                                            }
                                        ],
                                        "isSystem": true,
                                        "__typename": "Role",
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22899",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 699.47604613053,
                                    "y": 71.7637640824031
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_vrap_omta",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_vrap_omta",
                            "description": "Print VRAP OMTA",
                            "help": "Print VRAP OMTA",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember",
                                                "__typename": "SecurityRule"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember",
                                                "__typename": "SecurityRule"
                                            }
                                        ],
                                        "isSystem": true,
                                        "__typename": "Role",
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22925",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 137.54718100086575,
                                    "y": 80
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_standard_material_transfer_agreement_signed",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_standard_material_transfer_agreement_signed",
                            "description": "Print Standard Material Transfer Agreement Signed",
                            "help": "Print Standard Material Transfer Agreement Signed",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22923",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 137.54718100086552,
                                    "y": 42.44786428284351
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_standard_material_transfer_agreement",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_standard_material_transfer_agreement",
                            "description": "Print Standard Material Transfer Agreement",
                            "help": "Print Standard Material Transfer Agreement",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22922",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 325.5114477389834,
                                    "y": 80
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_mta_irri_developed_seed_other_than_rice",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_mta_irri_developed_seed_other_than_rice",
                            "description": "Print MTA IRRI Developed Seed Other Than Rice",
                            "help": "Print MTA IRRI Developed Seed Other Than Rice",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22921",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 324.6723215481884,
                                    "y": 43.28699047363864
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_mta_for_samples_under_collaborative_projects",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_mta_for_samples_under_collaborative_projects",
                            "description": "Print MTA for Samples Under Collaborative Projects",
                            "help": "Print MTA for Samples Under Collaborative Projects",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22920",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 422.85008587122275,
                                    "y": 43.28699047363864
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_mta_for_outgoing_transgenic_materials(no smta)",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_mta_for_outgoing_transgenic_materials(no smta)",
                            "description": "Print MTA for Outgoing Transgenic Materials (No SMTA)",
                            "help": "Print MTA for Outgoing Transgenic Materials (No SMTA)",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        },
                        {
                            "id": "22919",
                            "dependOn": {
                                "edges": [],
                                "width": 100,
                                "height": 50,
                                "position": {
                                    "x": 417.2935059634515,
                                    "y": 0
                                }
                            },
                            "define": {
                                "id": 3,
                                "after": {
                                    "executeNode": "",
                                    "sendNotification": {
                                        "send": false,
                                        "message": ""
                                    }
                                },
                                "rules": {
                                    "parameters": [
                                        {
                                            "columnName": "",
                                            "columnValue": ""
                                        }
                                    ],
                                    "validationFunction": ""
                                },
                                "before": {
                                    "validate": {
                                        "code": "",
                                        "type": "javascript",
                                        "valid": false,
                                        "onError": "",
                                        "functions": "",
                                        "onSuccess": ""
                                    }
                                },
                                "allFlow": true,
                                "allPhase": false,
                                "disabled": false,
                                "template": "sys_mta_golden_rice_lead_event_to_gr_licensees",
                                "inputProps": {
                                    "sourceNodes": []
                                },
                                "outputProps": {
                                    "targetNodes": []
                                }
                            },
                            "name": "sys_mta_golden_rice_lead_event_to_gr_licensees",
                            "description": "Print MTA for Golden Rice Lead Event to Golden Rice Licensees",
                            "help": "Print MTA for Golden Rice Lead Event to Golden Rice Licensees",
                            "sequence": 1,
                            "icon": "PrintRounded",
                            "securityDefinition": {
                                "roles": [
                                    {
                                        "id": "2",
                                        "name": "Admin",
                                        "rules": [
                                            {
                                                "id": "1",
                                                "name": "@CreatedByMe"
                                            },
                                            {
                                                "id": "4",
                                                "name": "@SubmittedToMe"
                                            },
                                            {
                                                "id": "3",
                                                "name": "@CreateByAnyUser"
                                            },
                                            {
                                                "id": "2",
                                                "name": "@CreatedByUnitMember"
                                            },
                                            {
                                                "id": "5",
                                                "name": "@SubmittedToUnitMember"
                                            }
                                        ],
                                        "isSystem": true,
                                        "description": "admin"
                                    }
                                ],
                                "users": null,
                                "programs": null
                            },
                            "workflowViewType": {
                                "id": "1",
                                "name": "N/A"
                            },
                            "nodeType": {
                                "id": "2",
                                "name": "rowAction",
                                "description": "Row Action"
                            },
                            "process": {
                                "id": "3",
                                "isBackground": false,
                                "name": "Print Preview",
                                "code": "PP",
                                "path": null
                            },
                            "nodeCFs": []
                        }
                    ]
                }
            ]
        }
    ],
    "sequence": 3,
    "isSystem": true,
    "api": "https://smapi-dev.ebsproject.org/",
    "product": {
        "hasWorkflow": true,
        "id": "38",
        "name": "Request No-GSR",
        "entityReference": {
            "entity": "service"
        },
        "domain": {
            "domaininstances": [
                {
                    "sgContext": "https://smapi-dev.ebsproject.org/"
                }
            ]
        },
        "htmltag": {
            "id": "1",
            "tagName": "ent.Component.att.name"
        }
    }
};
 export const workflowVariablesMock={
    "uri": "https://smapi-dev.ebsproject.org/graphql",
    "entity": "Service",
    "customFields": [
        {
            "id": "770",
            "fieldAttributes": {
                "id": 1,
                "name": "senderEmail",
                "sort": 9,
                "rules": {
                    "required": "The sender email is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Sender Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Sender Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "email"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "senderId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Sender Email",
            "name": "senderEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "764",
            "fieldAttributes": {
                "id": 5,
                "sort": 5,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Specific Materials",
                    "placement": "top"
                },
                "inputProps": {
                    "color": "primary",
                    "label": "Specific Materials",
                    "variant": "outlined"
                },
                "showInGrid": false,
                "defaultOptions": [
                    {
                        "label": "Seeds and genetic resources",
                        "value": 1
                    },
                    {
                        "label": "GMO",
                        "value": 2
                    },
                    {
                        "label": "Non seeds",
                        "value": 3
                    },
                    {
                        "label": "Transgenic",
                        "value": 4
                    },
                    {
                        "label": "Other",
                        "value": 5
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Specific Materials",
            "name": "specificMaterials",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "763",
            "fieldAttributes": {
                "id": 5,
                "sort": 4,
                "rules": {
                    "required": "This field is required!"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Shipment Purpose",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Shipment Purpose",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "Breeding",
                        "value": 1
                    },
                    {
                        "label": "Testing",
                        "value": 2
                    },
                    {
                        "label": "Research",
                        "value": 3
                    },
                    {
                        "label": "Education",
                        "value": 4
                    },
                    {
                        "label": "Commercial analysis",
                        "value": 5
                    },
                    {
                        "label": "Other",
                        "value": "6"
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Shipment Purpose",
            "name": "shipmentPurposeDd",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "1003",
            "fieldAttributes": {
                "id": 1,
                "name": "securityDiveder",
                "sort": 1.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHIPMENT INFORMATION",
                    "variant": "standard",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Divider",
            "name": "securityDivider",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "797",
            "fieldAttributes": {
                "id": 1,
                "name": "requestorEmail",
                "sort": 15,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Requestor Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Requestor Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "requestorId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Requestor Email",
            "name": "requestorEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "767",
            "fieldAttributes": {
                "id": 4,
                "row": false,
                "sort": 6,
                "rules": {
                    "required": "required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Shipment Type",
                    "placement": "top"
                },
                "options": [
                    {
                        "label": "Within CGIAR",
                        "value": "within"
                    },
                    {
                        "label": "External to CGIAR",
                        "value": "external"
                    }
                ],
                "inputProps": {
                    "label": "Shipment Type"
                },
                "showInGrid": false,
                "defaultValue": "within"
            },
            "apiAttributesName": "",
            "description": "Shipment Type",
            "name": "shipmentType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "4",
                "description": "RadioGroup",
                "name": "radiogroup",
                "type": "String"
            }
        },
        {
            "id": "768",
            "fieldAttributes": {
                "id": 4,
                "row": false,
                "sort": 7,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Material Type",
                    "placement": "top"
                },
                "options": [
                    {
                        "label": "Propagative",
                        "value": "propagative"
                    },
                    {
                        "label": "Non Propagative",
                        "value": "non-propagative"
                    }
                ],
                "inputProps": {
                    "label": "Material Type"
                },
                "showInGrid": false,
                "defaultValue": "propagative"
            },
            "apiAttributesName": "",
            "description": "Material Type",
            "name": "materialType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "4",
                "description": "RadioGroup",
                "name": "radiogroup",
                "type": "String"
            }
        },
        {
            "id": "795",
            "fieldAttributes": {
                "id": 1,
                "name": "shipmentInformation",
                "sort": 7.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHIPMENT INFORMATION",
                    "variant": "standard",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "SHIPMENT INFORMATION",
            "name": "shipmentInformation",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "794",
            "fieldAttributes": {
                "id": 5,
                "sort": 7.1,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Shipment Delivery",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Shipment Delivery",
                    "variant": "outlined"
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "Foreign",
                        "value": 1
                    },
                    {
                        "label": "Domestic",
                        "value": 2
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Shipment Delivery",
            "name": "shipmentDelivery",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "766",
            "fieldAttributes": {
                "id": 1,
                "name": "otherMaterials",
                "sort": 5.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Other Materials",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Other Materials",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false,
                    "placeholder": "Please specify if other"
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Other Materials",
            "name": "otherMaterials",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "801",
            "fieldAttributes": {
                "id": 1,
                "name": "recipientEmail",
                "sort": 20,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Recipient Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Recipient Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "recipientId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Recipient Email",
            "name": "recipientEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "771",
            "fieldAttributes": {
                "id": 5,
                "sort": 10,
                "rules": {
                    "required": "The sender address is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Address",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Address",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "senderId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "region",
                        "country.name",
                        "location",
                        "zipCode"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.region"
                        },
                        {
                            "accessor": "addresses.country.name"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "senderId"
                }
            },
            "apiAttributesName": "",
            "description": "Sender Address",
            "name": "senderAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "798",
            "fieldAttributes": {
                "id": 5,
                "sort": 16,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Address",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Address",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "requestorId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "zipCode",
                        "location",
                        "country.name"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.country.name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "requestorId"
                }
            },
            "apiAttributesName": "",
            "description": "Requestor Address",
            "name": "requestorAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "802",
            "fieldAttributes": {
                "id": 5,
                "name": "recipientAddress",
                "sort": 21,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Recipient Address",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Recipient Address",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "recipientId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "zipCode",
                        "location",
                        "country.name"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        },
                        {
                            "accessor": "addresses.country.name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "recipientId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Recipient Address",
            "name": "recipientAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "762",
            "fieldAttributes": {
                "id": 1,
                "name": "shuId",
                "sort": 3,
                "rules": {
                    "required": "The SHU ID is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "SHU-ID",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHU-ID",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "SHU-ID",
            "name": "shuId",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "765",
            "fieldAttributes": {
                "id": 1,
                "name": "shipmentPurposeTx",
                "sort": 4.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Other Purpose",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Other Purpose",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false,
                    "placeholder": "Please specify if other"
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.fiel"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "shipmentPurposeDd",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    },
                    "showControlRules": {
                        "expectedValue": "Other",
                        "parentControl": "shipmentPurposeDd"
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Other Purpose",
            "name": "shipmentPurposeTx",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1493",
            "fieldAttributes": {
                "id": 6,
                "sort": 22.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "Set Recipient same as requestor",
                    "placement": "top"
                },
                "function": "setRecipientSameAsRequestor",
                "inputProps": {
                    "color": "primary",
                    "label": "Set Requestor Information to Recipient",
                    "disabled": false
                },
                "showInGrid": false,
                "defaultValue": false
            },
            "apiAttributesName": "",
            "description": "Set Same As Requestor",
            "name": "sameAsRequestor",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "6",
                "description": "Switch",
                "name": "switch",
                "type": "String"
            }
        },
        {
            "id": "804",
            "fieldAttributes": {
                "id": 5,
                "sort": 23,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "entity": "ContactType",
                "helper": {
                    "title": "Recipient Type",
                    "placement": "top"
                },
                "labels": [
                    "name"
                ],
                "columns": [
                    {
                        "accessor": "label"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Type",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "PRI - Private Companies",
                        "value": 1
                    },
                    {
                        "label": "AI - Academic Institutions",
                        "value": 2
                    },
                    {
                        "label": "ARI - Advance Research Institutions",
                        "value": 3
                    },
                    {
                        "label": "CGCP - CGIAR Challenge Program",
                        "value": 4
                    },
                    {
                        "label": "DO - Development Organizations",
                        "value": 5
                    },
                    {
                        "label": "FI - Financing Institutions",
                        "value": 6
                    },
                    {
                        "label": "FO - Foundations",
                        "value": 7
                    },
                    {
                        "label": " GO - Government",
                        "value": 8
                    },
                    {
                        "label": " IARC - International Agricultural Research Centers",
                        "value": 9
                    },
                    {
                        "label": "IO - International Organizations, Individual, Media",
                        "value": 10
                    },
                    {
                        "label": "Individual",
                        "value": 11
                    },
                    {
                        "label": "Media",
                        "value": 12
                    },
                    {
                        "label": "NARES - National Agriculture Research and Extension Systems",
                        "value": 13
                    },
                    {
                        "label": " NGO - Non governmental organizations",
                        "value": 14
                    },
                    {
                        "label": "SRO - Subregional organization",
                        "value": 15
                    },
                    {
                        "label": "Others",
                        "value": 16
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Recipient Type",
            "name": "recipientType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "803",
            "fieldAttributes": {
                "id": 5,
                "sort": 22,
                "rules": {
                    "required": "The recipient institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Recipient Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "recipientId"
                }
            },
            "apiAttributesName": "",
            "description": "Recipient Institution",
            "name": "recipientInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "799",
            "fieldAttributes": {
                "id": 5,
                "sort": 18,
                "rules": {
                    "required": "The requestor institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "requestorId"
                }
            },
            "apiAttributesName": "",
            "description": "Requestor Institution",
            "name": "requestorInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "793",
            "fieldAttributes": {
                "id": 5,
                "sort": 13,
                "rules": {
                    "required": "The sender institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "senderId"
                }
            },
            "apiAttributesName": "",
            "description": "Sender Institution",
            "name": "senderInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        }
    ],
    "allAttributes": [
        {
            "id": "770",
            "fieldAttributes": {
                "id": 1,
                "name": "senderEmail",
                "sort": 9,
                "rules": {
                    "required": "The sender email is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Sender Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Sender Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "email"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "senderId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Sender Email",
            "name": "senderEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "764",
            "fieldAttributes": {
                "id": 5,
                "sort": 5,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Specific Materials",
                    "placement": "top"
                },
                "inputProps": {
                    "color": "primary",
                    "label": "Specific Materials",
                    "variant": "outlined"
                },
                "showInGrid": false,
                "defaultOptions": [
                    {
                        "label": "Seeds and genetic resources",
                        "value": 1
                    },
                    {
                        "label": "GMO",
                        "value": 2
                    },
                    {
                        "label": "Non seeds",
                        "value": 3
                    },
                    {
                        "label": "Transgenic",
                        "value": 4
                    },
                    {
                        "label": "Other",
                        "value": 5
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Specific Materials",
            "name": "specificMaterials",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "763",
            "fieldAttributes": {
                "id": 5,
                "sort": 4,
                "rules": {
                    "required": "This field is required!"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Shipment Purpose",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Shipment Purpose",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "Breeding",
                        "value": 1
                    },
                    {
                        "label": "Testing",
                        "value": 2
                    },
                    {
                        "label": "Research",
                        "value": 3
                    },
                    {
                        "label": "Education",
                        "value": 4
                    },
                    {
                        "label": "Commercial analysis",
                        "value": 5
                    },
                    {
                        "label": "Other",
                        "value": "6"
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Shipment Purpose",
            "name": "shipmentPurposeDd",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "1003",
            "fieldAttributes": {
                "id": 1,
                "name": "securityDiveder",
                "sort": 1.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHIPMENT INFORMATION",
                    "variant": "standard",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Divider",
            "name": "securityDivider",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "797",
            "fieldAttributes": {
                "id": 1,
                "name": "requestorEmail",
                "sort": 15,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Requestor Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Requestor Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "requestorId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Requestor Email",
            "name": "requestorEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "761",
            "fieldAttributes": {
                "id": 1,
                "name": "shipmentName",
                "sort": 2,
                "rules": {
                    "required": "The shipment Name is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Shipment Name",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Shipment Name",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "name",
                    "label": [
                        "name"
                    ],
                    "entity": "Program",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "programId",
                    "sequenceRules": {
                        "ruleName": "program-code",
                        "applyRule": true
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "requestCode",
            "description": "Shipment Name",
            "name": "requestCode",
            "required": true,
            "attributes": {
                "id": "2593",
                "description": "name",
                "isRequired": true,
                "name": "name",
                "sortNo": 2
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "971",
            "fieldAttributes": {
                "id": 5,
                "sort": 1.1,
                "rules": {
                    "required": "The service provider is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Service Provider",
                    "placement": "top"
                },
                "columns": [],
                "filters": [],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Service Provider",
                    "variant": "outlined"
                },
                "showInGrid": false,
                "stateValues": {
                    "name": "serviceProvider"
                },
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": []
            },
            "apiAttributesName": "serviceProvider",
            "description": "Service Provider",
            "name": "serviceProvider",
            "required": true,
            "attributes": {
                "id": "2688",
                "description": "modification_timestamp",
                "isRequired": true,
                "name": "modification_timestamp",
                "sortNo": 10
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "872",
            "fieldAttributes": {
                "id": 1,
                "name": "submitionDate",
                "sort": 1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Submission Date",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "submitionDate",
            "description": "Submition Date",
            "name": "submitionDate",
            "required": false,
            "attributes": {
                "id": "2572",
                "description": "creation_timestamp",
                "isRequired": false,
                "name": "creation_timestamp",
                "sortNo": 12
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "767",
            "fieldAttributes": {
                "id": 4,
                "row": false,
                "sort": 6,
                "rules": {
                    "required": "required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Shipment Type",
                    "placement": "top"
                },
                "options": [
                    {
                        "label": "Within CGIAR",
                        "value": "within"
                    },
                    {
                        "label": "External to CGIAR",
                        "value": "external"
                    }
                ],
                "inputProps": {
                    "label": "Shipment Type"
                },
                "showInGrid": false,
                "defaultValue": "within"
            },
            "apiAttributesName": "",
            "description": "Shipment Type",
            "name": "shipmentType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "4",
                "description": "RadioGroup",
                "name": "radiogroup",
                "type": "String"
            }
        },
        {
            "id": "768",
            "fieldAttributes": {
                "id": 4,
                "row": false,
                "sort": 7,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Material Type",
                    "placement": "top"
                },
                "options": [
                    {
                        "label": "Propagative",
                        "value": "propagative"
                    },
                    {
                        "label": "Non Propagative",
                        "value": "non-propagative"
                    }
                ],
                "inputProps": {
                    "label": "Material Type"
                },
                "showInGrid": false,
                "defaultValue": "propagative"
            },
            "apiAttributesName": "",
            "description": "Material Type",
            "name": "materialType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "4",
                "description": "RadioGroup",
                "name": "radiogroup",
                "type": "String"
            }
        },
        {
            "id": "760",
            "fieldAttributes": {
                "id": 5,
                "sort": 1,
                "rules": {
                    "required": "The program is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "entity": "Program",
                "helper": {
                    "title": "Program",
                    "placement": "top"
                },
                "labels": [
                    "name"
                ],
                "columns": [],
                "filters": [],
                "apiContent": [
                    "id",
                    "name",
                    "code"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Program",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "program",
            "description": "Program",
            "name": "program",
            "required": true,
            "attributes": {
                "id": "2662",
                "description": "seed_code",
                "isRequired": true,
                "name": "seed_code",
                "sortNo": 29
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "795",
            "fieldAttributes": {
                "id": 1,
                "name": "shipmentInformation",
                "sort": 7.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHIPMENT INFORMATION",
                    "variant": "standard",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "SHIPMENT INFORMATION",
            "name": "shipmentInformation",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "796",
            "fieldAttributes": {
                "id": 5,
                "sort": 14,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Name",
                    "placement": "top"
                },
                "labels": [
                    "id",
                    "person.fullName"
                ],
                "columns": [
                    {
                        "name": "Requestor Name",
                        "accessor": "requestor.person.givenName"
                    },
                    {
                        "name": "Requestor Last Name",
                        "accessor": "requestor.person.familyName"
                    },
                    {
                        "name": "Requestor Email",
                        "accessor": "requestor.email"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "person.familyName",
                    "person.givenName",
                    "person.fullName"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Name",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "requestor",
            "description": "Requestor Name",
            "name": "requestor",
            "required": true,
            "attributes": {
                "id": "2642",
                "description": "workflow_id",
                "isRequired": true,
                "name": "workflow_id",
                "sortNo": 11
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "769",
            "fieldAttributes": {
                "id": 5,
                "sort": 8,
                "rules": {
                    "required": "The sender name is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Name",
                    "placement": "top"
                },
                "labels": [
                    "id",
                    "person.fullName"
                ],
                "columns": [
                    {
                        "name": "Sender Name",
                        "accessor": "sender.person.givenName"
                    },
                    {
                        "name": "Sender Last Name",
                        "accessor": "sender.person.familyName"
                    },
                    {
                        "name": "Sender Email",
                        "accessor": "sender.email"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "person.familyName",
                    "person.givenName",
                    "person.fullName"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Name",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "sender",
            "description": "Sender Name",
            "name": "sender",
            "required": true,
            "attributes": {
                "id": "2643",
                "description": "design_ref",
                "isRequired": true,
                "name": "design_ref",
                "sortNo": 17
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "794",
            "fieldAttributes": {
                "id": 5,
                "sort": 7.1,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Shipment Delivery",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Shipment Delivery",
                    "variant": "outlined"
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "Foreign",
                        "value": 1
                    },
                    {
                        "label": "Domestic",
                        "value": 2
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Shipment Delivery",
            "name": "shipmentDelivery",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "766",
            "fieldAttributes": {
                "id": 1,
                "name": "otherMaterials",
                "sort": 5.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Other Materials",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Other Materials",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false,
                    "placeholder": "Please specify if other"
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Other Materials",
            "name": "otherMaterials",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "801",
            "fieldAttributes": {
                "id": 1,
                "name": "recipientEmail",
                "sort": 20,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Recipient Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Recipient Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "recipientId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Recipient Email",
            "name": "recipientEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "771",
            "fieldAttributes": {
                "id": 5,
                "sort": 10,
                "rules": {
                    "required": "The sender address is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Address",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Address",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "senderId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "region",
                        "country.name",
                        "location",
                        "zipCode"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.region"
                        },
                        {
                            "accessor": "addresses.country.name"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "senderId"
                }
            },
            "apiAttributesName": "",
            "description": "Sender Address",
            "name": "senderAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "798",
            "fieldAttributes": {
                "id": 5,
                "sort": 16,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Address",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Address",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "requestorId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "zipCode",
                        "location",
                        "country.name"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.country.name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "requestorId"
                }
            },
            "apiAttributesName": "",
            "description": "Requestor Address",
            "name": "requestorAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "802",
            "fieldAttributes": {
                "id": 5,
                "name": "recipientAddress",
                "sort": 21,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Recipient Address",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Recipient Address",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "recipientId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "zipCode",
                        "location",
                        "country.name"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        },
                        {
                            "accessor": "addresses.country.name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "recipientId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Recipient Address",
            "name": "recipientAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "762",
            "fieldAttributes": {
                "id": 1,
                "name": "shuId",
                "sort": 3,
                "rules": {
                    "required": "The SHU ID is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "SHU-ID",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHU-ID",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "SHU-ID",
            "name": "shuId",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "765",
            "fieldAttributes": {
                "id": 1,
                "name": "shipmentPurposeTx",
                "sort": 4.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Other Purpose",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Other Purpose",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false,
                    "placeholder": "Please specify if other"
                },
                "showInGrid": false,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.fiel"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "shipmentPurposeDd",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    },
                    "showControlRules": {
                        "expectedValue": "Other",
                        "parentControl": "shipmentPurposeDd"
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "Other Purpose",
            "name": "shipmentPurposeTx",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1493",
            "fieldAttributes": {
                "id": 6,
                "sort": 22.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "Set Recipient same as requestor",
                    "placement": "top"
                },
                "function": "setRecipientSameAsRequestor",
                "inputProps": {
                    "color": "primary",
                    "label": "Set Requestor Information to Recipient",
                    "disabled": false
                },
                "showInGrid": false,
                "defaultValue": false
            },
            "apiAttributesName": "",
            "description": "Set Same As Requestor",
            "name": "sameAsRequestor",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "6",
                "description": "Switch",
                "name": "switch",
                "type": "String"
            }
        },
        {
            "id": "1597",
            "fieldAttributes": {
                "id": 1,
                "name": "status",
                "sort": 0,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Status",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "status",
            "description": "Status",
            "name": "status",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1598",
            "fieldAttributes": {
                "id": 1,
                "name": "totalItems",
                "sort": 2.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Total Items",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "totalItems",
            "description": "Total Items",
            "name": "totalItems",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1599",
            "fieldAttributes": {
                "id": 1,
                "name": "totalWeight",
                "sort": 2.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Total Weight",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "totalWeight",
            "description": "Total Weight",
            "name": "totalWeight",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1600",
            "fieldAttributes": {
                "id": 1,
                "name": "totalPackages",
                "sort": 2.3,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Total Packages",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "totalPackages",
            "description": "Total Packages",
            "name": "totalPackages",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "804",
            "fieldAttributes": {
                "id": 5,
                "sort": 23,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "entity": "ContactType",
                "helper": {
                    "title": "Recipient Type",
                    "placement": "top"
                },
                "labels": [
                    "name"
                ],
                "columns": [
                    {
                        "accessor": "label"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Type",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "PRI - Private Companies",
                        "value": 1
                    },
                    {
                        "label": "AI - Academic Institutions",
                        "value": 2
                    },
                    {
                        "label": "ARI - Advance Research Institutions",
                        "value": 3
                    },
                    {
                        "label": "CGCP - CGIAR Challenge Program",
                        "value": 4
                    },
                    {
                        "label": "DO - Development Organizations",
                        "value": 5
                    },
                    {
                        "label": "FI - Financing Institutions",
                        "value": 6
                    },
                    {
                        "label": "FO - Foundations",
                        "value": 7
                    },
                    {
                        "label": " GO - Government",
                        "value": 8
                    },
                    {
                        "label": " IARC - International Agricultural Research Centers",
                        "value": 9
                    },
                    {
                        "label": "IO - International Organizations, Individual, Media",
                        "value": 10
                    },
                    {
                        "label": "Individual",
                        "value": 11
                    },
                    {
                        "label": "Media",
                        "value": 12
                    },
                    {
                        "label": "NARES - National Agriculture Research and Extension Systems",
                        "value": 13
                    },
                    {
                        "label": " NGO - Non governmental organizations",
                        "value": 14
                    },
                    {
                        "label": "SRO - Subregional organization",
                        "value": 15
                    },
                    {
                        "label": "Others",
                        "value": 16
                    }
                ]
            },
            "apiAttributesName": "",
            "description": "Recipient Type",
            "name": "recipientType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "800",
            "fieldAttributes": {
                "id": 5,
                "sort": 19,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Recipient Name",
                    "placement": "top"
                },
                "labels": [
                    "id",
                    "person.fullName"
                ],
                "columns": [
                    {
                        "name": "Recipient Name",
                        "accessor": "recipient.person.givenName"
                    },
                    {
                        "name": "Recipient Last Name",
                        "accessor": "recipient.person.familyName"
                    },
                    {
                        "name": "Recipient Email",
                        "accessor": "recipient.email"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "person.givenName",
                    "person.familyName",
                    "person.fullName"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Name",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "recipient",
            "description": "Recipient Name",
            "name": "recipient",
            "required": true,
            "attributes": {
                "id": "2606",
                "description": "node_type_id",
                "isRequired": true,
                "name": "node_type_id",
                "sortNo": 27
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "803",
            "fieldAttributes": {
                "id": 5,
                "sort": 22,
                "rules": {
                    "required": "The recipient institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Recipient Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "recipientId"
                }
            },
            "apiAttributesName": "",
            "description": "Recipient Institution",
            "name": "recipientInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "799",
            "fieldAttributes": {
                "id": 5,
                "sort": 18,
                "rules": {
                    "required": "The requestor institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "requestorId"
                }
            },
            "apiAttributesName": "",
            "description": "Requestor Institution",
            "name": "requestorInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "793",
            "fieldAttributes": {
                "id": 5,
                "sort": 13,
                "rules": {
                    "required": "The sender institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "senderId"
                }
            },
            "apiAttributesName": "",
            "description": "Sender Institution",
            "name": "senderInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        }
    ],
    "allAttributesV2": [
        {
            "id": "770",
            "fieldAttributes": {
                "id": 1,
                "name": "senderEmail",
                "sort": 9,
                "rules": {
                    "required": "The sender email is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Sender Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Sender Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "email"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "senderId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "senderEmail",
            "description": "Sender Email",
            "name": "senderEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "763",
            "fieldAttributes": {
                "id": 5,
                "sort": 4,
                "rules": {
                    "required": "This field is required!"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Shipment Purpose",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Shipment Purpose",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "Breeding",
                        "value": 1
                    },
                    {
                        "label": "Testing",
                        "value": 2
                    },
                    {
                        "label": "Research",
                        "value": 3
                    },
                    {
                        "label": "Education",
                        "value": 4
                    },
                    {
                        "label": "Commercial analysis",
                        "value": 5
                    },
                    {
                        "label": "Other",
                        "value": "6"
                    }
                ]
            },
            "apiAttributesName": "shipmentPurposeDd",
            "description": "Shipment Purpose",
            "name": "shipmentPurposeDd",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "797",
            "fieldAttributes": {
                "id": 1,
                "name": "requestorEmail",
                "sort": 15,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Requestor Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Requestor Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "requestorId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "requestorEmail",
            "description": "Requestor Email",
            "name": "requestorEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "761",
            "fieldAttributes": {
                "id": 1,
                "name": "shipmentName",
                "sort": 2,
                "rules": {
                    "required": "The shipment Name is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "Shipment Name",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Shipment Name",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "name",
                    "label": [
                        "name"
                    ],
                    "entity": "Program",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "programId",
                    "sequenceRules": {
                        "ruleName": "program-code",
                        "applyRule": true
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "requestCode",
            "description": "Shipment Name",
            "name": "requestCode",
            "required": true,
            "attributes": {
                "id": "2593",
                "description": "name",
                "isRequired": true,
                "name": "name",
                "sortNo": 2
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "872",
            "fieldAttributes": {
                "id": 1,
                "name": "submitionDate",
                "sort": 1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Submission Date",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "submitionDate",
            "description": "Submition Date",
            "name": "submitionDate",
            "required": false,
            "attributes": {
                "id": "2572",
                "description": "creation_timestamp",
                "isRequired": false,
                "name": "creation_timestamp",
                "sortNo": 12
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "760",
            "fieldAttributes": {
                "id": 5,
                "sort": 1,
                "rules": {
                    "required": "The program is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "entity": "Program",
                "helper": {
                    "title": "Program",
                    "placement": "top"
                },
                "labels": [
                    "name"
                ],
                "columns": [],
                "filters": [],
                "apiContent": [
                    "id",
                    "name",
                    "code"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Program",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "program",
            "description": "Program",
            "name": "program",
            "required": true,
            "attributes": {
                "id": "2662",
                "description": "seed_code",
                "isRequired": true,
                "name": "seed_code",
                "sortNo": 29
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "796",
            "fieldAttributes": {
                "id": 5,
                "sort": 14,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Name",
                    "placement": "top"
                },
                "labels": [
                    "id",
                    "person.fullName"
                ],
                "columns": [
                    {
                        "name": "Requestor Name",
                        "accessor": "requestor.person.givenName"
                    },
                    {
                        "name": "Requestor Last Name",
                        "accessor": "requestor.person.familyName"
                    },
                    {
                        "name": "Requestor Email",
                        "accessor": "requestor.email"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "person.familyName",
                    "person.givenName",
                    "person.fullName"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Name",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "requestor",
            "description": "Requestor Name",
            "name": "requestor",
            "required": true,
            "attributes": {
                "id": "2642",
                "description": "workflow_id",
                "isRequired": true,
                "name": "workflow_id",
                "sortNo": 11
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "769",
            "fieldAttributes": {
                "id": 5,
                "sort": 8,
                "rules": {
                    "required": "The sender name is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Name",
                    "placement": "top"
                },
                "labels": [
                    "id",
                    "person.fullName"
                ],
                "columns": [
                    {
                        "name": "Sender Name",
                        "accessor": "sender.person.givenName"
                    },
                    {
                        "name": "Sender Last Name",
                        "accessor": "sender.person.familyName"
                    },
                    {
                        "name": "Sender Email",
                        "accessor": "sender.email"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "person.familyName",
                    "person.givenName",
                    "person.fullName"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Name",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "sender",
            "description": "Sender Name",
            "name": "sender",
            "required": true,
            "attributes": {
                "id": "2643",
                "description": "design_ref",
                "isRequired": true,
                "name": "design_ref",
                "sortNo": 17
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "801",
            "fieldAttributes": {
                "id": 1,
                "name": "recipientEmail",
                "sort": 20,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Recipient Email",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Recipient Email",
                    "variant": "outlined",
                    "disabled": true,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "email",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "email"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "recipientId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "recipientEmail",
            "description": "Recipient Email",
            "name": "recipientEmail",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "771",
            "fieldAttributes": {
                "id": 5,
                "sort": 10,
                "rules": {
                    "required": "The sender address is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Address",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Address",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "senderId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "region",
                        "country.name",
                        "location",
                        "zipCode"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.region"
                        },
                        {
                            "accessor": "addresses.country.name"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "senderId"
                }
            },
            "apiAttributesName": "senderAddress",
            "description": "Sender Address",
            "name": "senderAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "798",
            "fieldAttributes": {
                "id": 5,
                "sort": 16,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Address",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Address",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "requestorId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "zipCode",
                        "location",
                        "country.name"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.country.name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "requestorId"
                }
            },
            "apiAttributesName": "requestorAddress",
            "description": "Requestor Address",
            "name": "requestorAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "802",
            "fieldAttributes": {
                "id": 5,
                "name": "recipientAddress",
                "sort": 21,
                "rules": {
                    "required": "This field is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Recipient Address",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Recipient Address",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": true,
                    "componentUI": "AddressForm",
                    "parentControl": "recipientId"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "addresses",
                    "label": [
                        "streetAddress",
                        "zipCode",
                        "location",
                        "country.name"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "addresses.id"
                        },
                        {
                            "accessor": "addresses.streetAddress"
                        },
                        {
                            "accessor": "addresses.location"
                        },
                        {
                            "accessor": "addresses.zipCode"
                        },
                        {
                            "accessor": "addresses.country.name"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "recipientId",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "recipientAddress",
            "description": "Recipient Address",
            "name": "recipientAddress",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "762",
            "fieldAttributes": {
                "id": 1,
                "name": "shuId",
                "sort": 3,
                "rules": {
                    "required": "The SHU ID is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "SHU-ID",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHU-ID",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "shuId",
            "description": "SHU-ID",
            "name": "shuId",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1597",
            "fieldAttributes": {
                "id": 1,
                "name": "status",
                "sort": 0,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Status",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "status",
            "description": "Status",
            "name": "status",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1598",
            "fieldAttributes": {
                "id": 1,
                "name": "totalItems",
                "sort": 2.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Total Items",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "totalItems",
            "description": "Total Items",
            "name": "totalItems",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1599",
            "fieldAttributes": {
                "id": 1,
                "name": "totalWeight",
                "sort": 2.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Total Weight",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "totalWeight",
            "description": "Total Weight",
            "name": "totalWeight",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1600",
            "fieldAttributes": {
                "id": 1,
                "name": "totalPackages",
                "sort": 2.3,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    12,
                    12,
                    12
                ],
                "helper": {
                    "title": "",
                    "placement": "top"
                },
                "disabled": true,
                "inputProps": {
                    "rows": 1,
                    "label": "Total Packages",
                    "variant": "outlined",
                    "disabled": false,
                    "className": "hidden",
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "totalPackages",
            "description": "Total Packages",
            "name": "totalPackages",
            "required": false,
            "attributes": {
                "id": "2582",
                "description": "adminContact",
                "isRequired": true,
                "name": "admin_contact",
                "sortNo": 8
            },
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "804",
            "fieldAttributes": {
                "id": 5,
                "sort": 23,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "entity": "ContactType",
                "helper": {
                    "title": "Recipient Type",
                    "placement": "top"
                },
                "labels": [
                    "name"
                ],
                "columns": [
                    {
                        "accessor": "label"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Type",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "PRI - Private Companies",
                        "value": 1
                    },
                    {
                        "label": "AI - Academic Institutions",
                        "value": 2
                    },
                    {
                        "label": "ARI - Advance Research Institutions",
                        "value": 3
                    },
                    {
                        "label": "CGCP - CGIAR Challenge Program",
                        "value": 4
                    },
                    {
                        "label": "DO - Development Organizations",
                        "value": 5
                    },
                    {
                        "label": "FI - Financing Institutions",
                        "value": 6
                    },
                    {
                        "label": "FO - Foundations",
                        "value": 7
                    },
                    {
                        "label": " GO - Government",
                        "value": 8
                    },
                    {
                        "label": " IARC - International Agricultural Research Centers",
                        "value": 9
                    },
                    {
                        "label": "IO - International Organizations, Individual, Media",
                        "value": 10
                    },
                    {
                        "label": "Individual",
                        "value": 11
                    },
                    {
                        "label": "Media",
                        "value": 12
                    },
                    {
                        "label": "NARES - National Agriculture Research and Extension Systems",
                        "value": 13
                    },
                    {
                        "label": " NGO - Non governmental organizations",
                        "value": 14
                    },
                    {
                        "label": "SRO - Subregional organization",
                        "value": 15
                    },
                    {
                        "label": "Others",
                        "value": 16
                    }
                ]
            },
            "apiAttributesName": "recipientType",
            "description": "Recipient Type",
            "name": "recipientType",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "800",
            "fieldAttributes": {
                "id": 5,
                "sort": 19,
                "rules": {
                    "required": "This field is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Recipient Name",
                    "placement": "top"
                },
                "labels": [
                    "id",
                    "person.fullName"
                ],
                "columns": [
                    {
                        "name": "Recipient Name",
                        "accessor": "recipient.person.givenName"
                    },
                    {
                        "name": "Recipient Last Name",
                        "accessor": "recipient.person.familyName"
                    },
                    {
                        "name": "Recipient Email",
                        "accessor": "recipient.email"
                    }
                ],
                "filters": [
                    {
                        "col": "category.name",
                        "mod": "EQ",
                        "val": "Person"
                    }
                ],
                "apiContent": [
                    "id",
                    "person.givenName",
                    "person.familyName",
                    "person.fullName"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Name",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "recipient",
            "description": "Recipient Name",
            "name": "recipient",
            "required": true,
            "attributes": {
                "id": "2606",
                "description": "node_type_id",
                "isRequired": true,
                "name": "node_type_id",
                "sortNo": 27
            },
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "803",
            "fieldAttributes": {
                "id": 5,
                "sort": 22,
                "rules": {
                    "required": "The recipient institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Recipient Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Recipient Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "recipientId"
                }
            },
            "apiAttributesName": "recipientInstitution",
            "description": "Recipient Institution",
            "name": "recipientInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "799",
            "fieldAttributes": {
                "id": 5,
                "sort": 18,
                "rules": {
                    "required": "The requestor institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Requestor Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Requestor Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "requestorId"
                }
            },
            "apiAttributesName": "requestorInstitution",
            "description": "Requestor Institution",
            "name": "requestorInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "793",
            "fieldAttributes": {
                "id": 5,
                "sort": 13,
                "rules": {
                    "required": "The sender institution is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Sender Institution",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [],
                "disabled": false,
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Sender Institution",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "cs",
                    "field": "",
                    "label": [
                        "institution.institution.legalName",
                        "institution.institution.commonName"
                    ],
                    "entity": "Hierarchy",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "institution.institution.legalName"
                        },
                        {
                            "accessor": "institution.institution.commonName"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "contact.id",
                    "customFilters": [
                        {
                            "col": "institution.category.name",
                            "mod": "EQ",
                            "val": "Institution"
                        }
                    ],
                    "parentControl": "senderId"
                }
            },
            "apiAttributesName": "senderInstitution",
            "description": "Sender Institution",
            "name": "senderInstitution",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "911",
            "fieldAttributes": {
                "id": 3,
                "name": "documentSignedDate",
                "sort": 103,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Document Signed Date",
                    "placement": "top"
                },
                "inputProps": {
                    "label": "Document Signed Date",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultValue": ""
            },
            "apiAttributesName": "documentSignedDate",
            "description": "Document Signed Date",
            "name": "documentSignedDate",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "3",
                "description": "DatePicker",
                "name": "datepicker",
                "type": "String"
            }
        },
        {
            "id": "905",
            "fieldAttributes": {
                "id": 3,
                "name": "documentGeneratedDate",
                "sort": 104,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "DocumentGenerated Date",
                    "placement": "top"
                },
                "inputProps": {
                    "label": "Document Generated Date",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultValue": ""
            },
            "apiAttributesName": "documentGeneratedDate",
            "description": "Document Generate Date",
            "name": "documentGeneratedDate",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "3",
                "description": "DatePicker",
                "name": "datepicker",
                "type": "String"
            }
        },
        {
            "id": "908",
            "fieldAttributes": {
                "id": 1,
                "name": "airBillNumber",
                "sort": 121,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "helper": {
                    "title": "Air Bill Number",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Air Bill Number",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "showIfValue": "By Air",
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "typeOfCuries",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "airBillNumber",
            "description": "Air Bill Number",
            "name": "airBillNumber",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "910",
            "fieldAttributes": {
                "id": 3,
                "name": "shippedDateV2",
                "sort": 105,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Shipped Date",
                    "placement": "top"
                },
                "inputProps": {
                    "label": "Shipped Date",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultValue": ""
            },
            "apiAttributesName": "shippedDateV2",
            "description": "Shipped Date",
            "name": "shippedDateV2",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "3",
                "description": "DatePicker",
                "name": "datepicker",
                "type": "String"
            }
        },
        {
            "id": "912",
            "fieldAttributes": {
                "id": 3,
                "name": "receivedDate",
                "sort": 101,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    3,
                    3
                ],
                "helper": {
                    "title": "Received Date",
                    "placement": "top"
                },
                "inputProps": {
                    "label": "Received Date",
                    "variant": "outlined"
                },
                "showInGrid": true,
                "defaultValue": ""
            },
            "apiAttributesName": "receivedDate",
            "description": "Received Date",
            "name": "receivedDate",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "3",
                "description": "DatePicker",
                "name": "datepicker",
                "type": "String"
            }
        },
        {
            "id": "914",
            "fieldAttributes": {
                "id": 1,
                "name": "authorizedSignatory",
                "sort": 104,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "helper": {
                    "title": "IRRI Authorized Signatory",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "IRRI Authorized Signatory",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "authorizedSignatory",
            "description": "Auhtorized Signatory",
            "name": "authorizedSignatory",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1124",
            "fieldAttributes": {
                "id": 1,
                "name": "invoiceNumber",
                "sort": 110,
                "rules": {
                    "required": "The invoice number is required."
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Invoice Number",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Invoice Number",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": true,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "cs-invoice-number",
                        "applyRule": true
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "invoiceNumber",
            "description": "Invoice Number",
            "name": "invoiceNumber",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1127",
            "fieldAttributes": {
                "id": 5,
                "sort": 113,
                "rules": {
                    "required": "The country of origin is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "entity": "Country",
                "helper": {
                    "title": "Country of Origin",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": "name"
                    }
                ],
                "filters": [],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Country of Origin",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                }
            },
            "apiAttributesName": "countryOfOrigin",
            "description": "Country of Origin",
            "name": "countryOfOrigin",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "1134",
            "fieldAttributes": {
                "id": 5,
                "sort": 119,
                "rules": {
                    "required": "Please selected the type of courier"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "entity": "Contact",
                "helper": {
                    "title": "Type of Curies",
                    "placement": "top"
                },
                "columns": [
                    {
                        "accessor": ""
                    }
                ],
                "filters": [
                    {
                        "col": "",
                        "mod": "EQ",
                        "val": ""
                    }
                ],
                "apiContent": [
                    "id",
                    "name"
                ],
                "inputProps": {
                    "color": "primary",
                    "label": "Type of Courier",
                    "variant": "outlined"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "person",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "category.name",
                    "customFilters": [],
                    "parentControl": "control name"
                },
                "defaultOptions": [
                    {
                        "label": "By Air",
                        "value": 1
                    },
                    {
                        "label": "By Road",
                        "value": 2
                    },
                    {
                        "label": "By Sea",
                        "value": 3
                    },
                    {
                        "label": "By Rail",
                        "value": 4
                    },
                    {
                        "label": "Picked physically by recipient",
                        "value": 5
                    }
                ]
            },
            "apiAttributesName": "typeOfCuries",
            "description": "Type of Curies",
            "name": "typeOfCuries",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "5",
                "description": "Select",
                "name": "select",
                "type": "String"
            }
        },
        {
            "id": "1237",
            "fieldAttributes": {
                "id": 1,
                "name": "unitValue",
                "sort": 111.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Unit Value",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Unit Value",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "unitValue",
            "description": "Unit Value",
            "name": "unitValue",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1239",
            "fieldAttributes": {
                "id": 1,
                "name": "species",
                "sort": 111.4,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Species",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Species",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "species",
            "description": "Species",
            "name": "species",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1290",
            "fieldAttributes": {
                "id": 1,
                "name": "specificTest",
                "sort": 105.1,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "helper": {
                    "title": "Specific test to be done",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Specific Test",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "specificTest",
            "description": "Specific Test",
            "name": "specificTest",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        },
        {
            "id": "1291",
            "fieldAttributes": {
                "id": 4,
                "row": false,
                "sort": 105.2,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    6,
                    6
                ],
                "helper": {
                    "title": "Cost",
                    "placement": "top"
                },
                "options": [
                    {
                        "label": "With cost",
                        "value": "with-cost"
                    },
                    {
                        "label": "Without cost",
                        "value": "no-cost"
                    }
                ],
                "inputProps": {
                    "label": "Cost"
                },
                "showInGrid": true,
                "defaultValue": "no-cost"
            },
            "apiAttributesName": "cost",
            "description": "cost",
            "name": "cost",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "4",
                "description": "RadioGroup",
                "name": "radiogroup",
                "type": "String"
            }
        },
        {
            "id": "1125",
            "fieldAttributes": {
                "id": 1,
                "name": "valueOfProduct",
                "sort": 111,
                "rules": {
                    "required": ""
                },
                "sizes": [
                    12,
                    12,
                    6,
                    4,
                    4
                ],
                "helper": {
                    "title": "Total value of the Product",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "Total value of the Product(Value only for customs purpose)",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false,
                    "startAdornment": "$"
                },
                "modalPopup": {
                    "show": false,
                    "componentUI": ""
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "valueOfProduct",
            "description": "Value of the product",
            "name": "valueOfProduct",
            "required": false,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        }
    ],
    "serviceProvider": [
        {
            "label": "CIMMYT Mexico Wheat Molecular Laboratory",
            "value": 77
        }
    ]
};
export const rowDataMock = {
    "id": 3,
    "cost": null,
    "shuId": "4432423423432234324",
    "sender": {
        "label": "Ramirez Aldama, Juan Manuel, Nepomuceno",
        "value": 123
    },
    "status": null,
    "program": {
        "label": "Irrigated South-East Asia",
        "value": 92
    },
    "species": null,
    "statusid": null,
    "createdBy": {
        "value": 1,
        "userName": "System, Account"
    },
    "createdOn": "2024-11-08",
    "recipient": {
        "label": "Ramirez Aldama, Juan Manuel, Nepomuceno",
        "value": 123
    },
    "requestor": {
        "label": "Ramirez Aldama, Juan Manuel, Nepomuceno",
        "value": 123
    },
    "unitValue": null,
    "updatedBy": null,
    "updatedOn": null,
    "totalItems": "0",
    "wfInstance": 3,
    "description": null,
    "programCode": 101,
    "requestCode": "IRSEA-2024-11-08-0000023",
    "senderEmail": "j.ramirez@cimmyt.org",
    "totalWeight": "0",
    "byOccurrence": false,
    "materialType": "non-propagative",
    "receivedDate": null,
    "shipmentType": "external",
    "specificTest": null,
    "typeOfCuries": null,
    "airBillNumber": null,
    "invoiceNumber": null,
    "recipientType": "FI - Financing Institutions",
    "senderAddress": "Mexico-Veracruz, El Batan Km. 45, 56237 Mex., R, Mexico, Mexico, 9999",
    "shippedDateV2": null,
    "submitionDate": null,
    "totalPackages": "0",
    "AdditionalInfo": null,
    "otherMaterials": "",
    "recipientEmail": "j.ramirez@cimmyt.org",
    "requestorEmail": "j.ramirez@cimmyt.org",
    "valueOfProduct": null,
    "countryOfOrigin": null,
    "sameAsRequestor": "false",
    "securityDivider": "",
    "serviceProvider": {
        "label": "(WMBL)-CIMMYT Mexico Wheat Molecular Laboratory",
        "value": 77
    },
    "packageMaxWeight": "0",
    "recipientAddress": "Mexico-Veracruz, El Batan Km. 45, 56237 Mex., 9999, Mexico, Mexico",
    "requestorAddress": "Mexico-Veracruz, El Batan Km. 45, 56237 Mex., 9999, Mexico, Mexico",
    "shipmentDelivery": "Foreign",
    "senderInstitution": "CIMMYT, CIMMYT",
    "shipmentPurposeDd": "Commercial analysis",
    "shipmentPurposeTx": "",
    "specificMaterials": "Seeds and genetic resources",
    "documentSignedDate": null,
    "authorizedSignatory": null,
    "shipmentInformation": "",
    "recipientInstitution": "CIMMYT, CIMMYT",
    "requestorInstitution": "CIMMYT, CIMMYT",
    "documentGeneratedDate": null
}
export const responseMock ={
    formNodes: [{
      "id": "20992",
      "dependOn": {
          "edges": [],
          "width": 100,
          "height": 50,
          "position": {
              "x": 0,
              "y": 23.352009947434
          }
      },
      "define": {
          "id": 1,
          "after": {
              "executeNode": "",
              "sendNotification": {
                  "send": false,
                  "message": ""
              }
          },
          "title": "General Information",
          "before": {
              "validate": {
                  "code": "",
                  "type": "javascript",
                  "valid": false,
                  "onError": "",
                  "functions": "",
                  "onSuccess": ""
              }
          },
          "inputProps": {
              "sourceNodes": []
          },
          "outputProps": {
              "targetNodes": []
          }
      },
      "name": "General Information",
      "description": "General Information",
      "help": "General Information",
      "sequence": 1,
      "icon": null,
      "securityDefinition": {
          "roles": [
              {
                  "id": "2",
                  "name": "Admin",
                  "isSystem": true,
                  "__typename": "Role"
              }
          ]
      },
      "workflowViewType": {
          "id": "2",
          "name": "Form"
      },
      "nodeType": {
          "id": "1",
          "name": "form",
          "description": "Form"
      },
      "process": {
          "id": "1",
          "isBackground": false,
          "name": "Generate Form",
          "code": "FORM",
          "path": null
      },
      "nodeCFs": [
          {
              "id": "770",
              "fieldAttributes": {
                  "id": 1,
                  "name": "senderEmail",
                  "sort": 9,
                  "rules": {
                      "required": "The sender email is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "helper": {
                      "title": "Sender Email",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Sender Email",
                      "variant": "outlined",
                      "disabled": true,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "email",
                      "label": [
                          "email"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "email"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "senderId",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Sender Email",
              "name": "senderEmail",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "764",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 5,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "helper": {
                      "title": "Specific Materials",
                      "placement": "top"
                  },
                  "inputProps": {
                      "color": "primary",
                      "label": "Specific Materials",
                      "variant": "outlined"
                  },
                  "showInGrid": false,
                  "defaultOptions": [
                      {
                          "label": "Seeds and genetic resources",
                          "value": 1
                      },
                      {
                          "label": "GMO",
                          "value": 2
                      },
                      {
                          "label": "Non seeds",
                          "value": 3
                      },
                      {
                          "label": "Transgenic",
                          "value": 4
                      },
                      {
                          "label": "Other",
                          "value": 5
                      }
                  ]
              },
              "apiAttributesName": "",
              "description": "Specific Materials",
              "name": "specificMaterials",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "763",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 4,
                  "rules": {
                      "required": "This field is required!"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Shipment Purpose",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [
                      {
                          "col": "",
                          "mod": "EQ",
                          "val": ""
                      }
                  ],
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Shipment Purpose",
                      "variant": "outlined"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  },
                  "defaultOptions": [
                      {
                          "label": "Breeding",
                          "value": 1
                      },
                      {
                          "label": "Testing",
                          "value": 2
                      },
                      {
                          "label": "Research",
                          "value": 3
                      },
                      {
                          "label": "Education",
                          "value": 4
                      },
                      {
                          "label": "Commercial analysis",
                          "value": 5
                      },
                      {
                          "label": "Other",
                          "value": "6"
                      }
                  ]
              },
              "apiAttributesName": "",
              "description": "Shipment Purpose",
              "name": "shipmentPurposeDd",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "1003",
              "fieldAttributes": {
                  "id": 1,
                  "name": "securityDiveder",
                  "sort": 1.2,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "SHIPMENT INFORMATION",
                      "variant": "standard",
                      "disabled": true,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": false,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Divider",
              "name": "securityDivider",
              "required": false,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "797",
              "fieldAttributes": {
                  "id": 1,
                  "name": "requestorEmail",
                  "sort": 15,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "helper": {
                      "title": "Requestor Email",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Requestor Email",
                      "variant": "outlined",
                      "disabled": true,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "email",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "email"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "requestorId",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Requestor Email",
              "name": "requestorEmail",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "761",
              "fieldAttributes": {
                  "id": 1,
                  "name": "shipmentName",
                  "sort": 2,
                  "rules": {
                      "required": "The shipment Name is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "helper": {
                      "title": "Shipment Name",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Shipment Name",
                      "variant": "outlined",
                      "disabled": false,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "name",
                      "label": [
                          "name"
                      ],
                      "entity": "Program",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "name"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "programId",
                      "sequenceRules": {
                          "ruleName": "program-code",
                          "applyRule": true
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "requestCode",
              "description": "Shipment Name",
              "name": "requestCode",
              "required": true,
              "attributes": {
                  "id": "2593",
                  "description": "name",
                  "isRequired": true,
                  "name": "name",
                  "sortNo": 2
              },
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "971",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 1.1,
                  "rules": {
                      "required": "The service provider is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      4,
                      4
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Service Provider",
                      "placement": "top"
                  },
                  "columns": [],
                  "filters": [],
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Service Provider",
                      "variant": "outlined"
                  },
                  "showInGrid": false,
                  "stateValues": {
                      "name": "serviceProvider"
                  },
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  },
                  "defaultOptions": []
              },
              "apiAttributesName": "serviceProvider",
              "description": "Service Provider",
              "name": "serviceProvider",
              "required": true,
              "attributes": {
                  "id": "2688",
                  "description": "modification_timestamp",
                  "isRequired": true,
                  "name": "modification_timestamp",
                  "sortNo": 10
              },
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "872",
              "fieldAttributes": {
                  "id": 1,
                  "name": "submitionDate",
                  "sort": 1,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "disabled": true,
                  "inputProps": {
                      "rows": 1,
                      "label": "Submission Date",
                      "variant": "outlined",
                      "disabled": false,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "submitionDate",
              "description": "Submition Date",
              "name": "submitionDate",
              "required": false,
              "attributes": {
                  "id": "2572",
                  "description": "creation_timestamp",
                  "isRequired": false,
                  "name": "creation_timestamp",
                  "sortNo": 12
              },
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "767",
              "fieldAttributes": {
                  "id": 4,
                  "row": false,
                  "sort": 6,
                  "rules": {
                      "required": "required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "helper": {
                      "title": "Shipment Type",
                      "placement": "top"
                  },
                  "options": [
                      {
                          "label": "Within CGIAR",
                          "value": "within"
                      },
                      {
                          "label": "External to CGIAR",
                          "value": "external"
                      }
                  ],
                  "inputProps": {
                      "label": "Shipment Type"
                  },
                  "showInGrid": false,
                  "defaultValue": "within"
              },
              "apiAttributesName": "",
              "description": "Shipment Type",
              "name": "shipmentType",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "4",
                  "description": "RadioGroup",
                  "name": "radiogroup",
                  "type": "String"
              }
          },
          {
              "id": "768",
              "fieldAttributes": {
                  "id": 4,
                  "row": false,
                  "sort": 7,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "helper": {
                      "title": "Material Type",
                      "placement": "top"
                  },
                  "options": [
                      {
                          "label": "Propagative",
                          "value": "propagative"
                      },
                      {
                          "label": "Non Propagative",
                          "value": "non-propagative"
                      }
                  ],
                  "inputProps": {
                      "label": "Material Type"
                  },
                  "showInGrid": false,
                  "defaultValue": "propagative"
              },
              "apiAttributesName": "",
              "description": "Material Type",
              "name": "materialType",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "4",
                  "description": "RadioGroup",
                  "name": "radiogroup",
                  "type": "String"
              }
          },
          {
              "id": "760",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 1,
                  "rules": {
                      "required": "The program is required."
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      4,
                      4
                  ],
                  "entity": "Program",
                  "helper": {
                      "title": "Program",
                      "placement": "top"
                  },
                  "labels": [
                      "name"
                  ],
                  "columns": [],
                  "filters": [],
                  "apiContent": [
                      "id",
                      "name",
                      "code"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Program",
                      "variant": "outlined"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  }
              },
              "apiAttributesName": "program",
              "description": "Program",
              "name": "program",
              "required": true,
              "attributes": {
                  "id": "2662",
                  "description": "seed_code",
                  "isRequired": true,
                  "name": "seed_code",
                  "sortNo": 29
              },
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "795",
              "fieldAttributes": {
                  "id": 1,
                  "name": "shipmentInformation",
                  "sort": 7.2,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "SHIPMENT INFORMATION",
                      "variant": "standard",
                      "disabled": true,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": false,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "SHIPMENT INFORMATION",
              "name": "shipmentInformation",
              "required": false,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "796",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 14,
                  "rules": {
                      "required": "This field is required."
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Requestor Name",
                      "placement": "top"
                  },
                  "labels": [
                      "id",
                      "person.fullName"
                  ],
                  "columns": [
                      {
                          "name": "Requestor Name",
                          "accessor": "requestor.person.givenName"
                      },
                      {
                          "name": "Requestor Last Name",
                          "accessor": "requestor.person.familyName"
                      },
                      {
                          "name": "Requestor Email",
                          "accessor": "requestor.email"
                      }
                  ],
                  "filters": [
                      {
                          "col": "category.name",
                          "mod": "EQ",
                          "val": "Person"
                      }
                  ],
                  "apiContent": [
                      "id",
                      "person.familyName",
                      "person.givenName",
                      "person.fullName"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Requestor Name",
                      "variant": "outlined"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  }
              },
              "apiAttributesName": "requestor",
              "description": "Requestor Name",
              "name": "requestor",
              "required": true,
              "attributes": {
                  "id": "2642",
                  "description": "workflow_id",
                  "isRequired": true,
                  "name": "workflow_id",
                  "sortNo": 11
              },
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "769",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 8,
                  "rules": {
                      "required": "The sender name is required."
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Sender Name",
                      "placement": "top"
                  },
                  "labels": [
                      "id",
                      "person.fullName"
                  ],
                  "columns": [
                      {
                          "name": "Sender Name",
                          "accessor": "sender.person.givenName"
                      },
                      {
                          "name": "Sender Last Name",
                          "accessor": "sender.person.familyName"
                      },
                      {
                          "name": "Sender Email",
                          "accessor": "sender.email"
                      }
                  ],
                  "filters": [
                      {
                          "col": "category.name",
                          "mod": "EQ",
                          "val": "Person"
                      }
                  ],
                  "apiContent": [
                      "id",
                      "person.familyName",
                      "person.givenName",
                      "person.fullName"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Sender Name",
                      "variant": "outlined"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  }
              },
              "apiAttributesName": "sender",
              "description": "Sender Name",
              "name": "sender",
              "required": true,
              "attributes": {
                  "id": "2643",
                  "description": "design_ref",
                  "isRequired": true,
                  "name": "design_ref",
                  "sortNo": 17
              },
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "794",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 7.1,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Shipment Delivery",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [
                      {
                          "col": "",
                          "mod": "EQ",
                          "val": ""
                      }
                  ],
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Shipment Delivery",
                      "variant": "outlined"
                  },
                  "showInGrid": false,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  },
                  "defaultOptions": [
                      {
                          "label": "Foreign",
                          "value": 1
                      },
                      {
                          "label": "Domestic",
                          "value": 2
                      }
                  ]
              },
              "apiAttributesName": "",
              "description": "Shipment Delivery",
              "name": "shipmentDelivery",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "766",
              "fieldAttributes": {
                  "id": 1,
                  "name": "otherMaterials",
                  "sort": 5.1,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      4,
                      4
                  ],
                  "helper": {
                      "title": "Other Materials",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Other Materials",
                      "variant": "outlined",
                      "disabled": false,
                      "fullWidth": true,
                      "multiline": false,
                      "placeholder": "Please specify if other"
                  },
                  "showInGrid": false,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Other Materials",
              "name": "otherMaterials",
              "required": false,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "801",
              "fieldAttributes": {
                  "id": 1,
                  "name": "recipientEmail",
                  "sort": 20,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "helper": {
                      "title": "Recipient Email",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Recipient Email",
                      "variant": "outlined",
                      "disabled": true,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "email",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "email"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "recipientId",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Recipient Email",
              "name": "recipientEmail",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "771",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 10,
                  "rules": {
                      "required": "The sender address is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Sender Address",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [],
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Sender Address",
                      "variant": "outlined"
                  },
                  "modalPopup": {
                      "show": true,
                      "componentUI": "AddressForm",
                      "parentControl": "senderId"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "addresses",
                      "label": [
                          "streetAddress",
                          "region",
                          "country.name",
                          "location",
                          "zipCode"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "addresses.id"
                          },
                          {
                              "accessor": "addresses.streetAddress"
                          },
                          {
                              "accessor": "addresses.region"
                          },
                          {
                              "accessor": "addresses.country.name"
                          },
                          {
                              "accessor": "addresses.location"
                          },
                          {
                              "accessor": "addresses.zipCode"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "senderId"
                  }
              },
              "apiAttributesName": "",
              "description": "Sender Address",
              "name": "senderAddress",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "798",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 16,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Requestor Address",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [
                      {
                          "col": "",
                          "mod": "EQ",
                          "val": ""
                      }
                  ],
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Requestor Address",
                      "variant": "outlined"
                  },
                  "modalPopup": {
                      "show": true,
                      "componentUI": "AddressForm",
                      "parentControl": "requestorId"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "addresses",
                      "label": [
                          "streetAddress",
                          "zipCode",
                          "location",
                          "country.name"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "addresses.id"
                          },
                          {
                              "accessor": "addresses.location"
                          },
                          {
                              "accessor": "addresses.zipCode"
                          },
                          {
                              "accessor": "addresses.streetAddress"
                          },
                          {
                              "accessor": "addresses.country.name"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "requestorId"
                  }
              },
              "apiAttributesName": "",
              "description": "Requestor Address",
              "name": "requestorAddress",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "802",
              "fieldAttributes": {
                  "id": 5,
                  "name": "recipientAddress",
                  "sort": 21,
                  "rules": {
                      "required": "This field is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "helper": {
                      "title": "Recipient Address",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Recipient Address",
                      "variant": "outlined",
                      "disabled": false,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "modalPopup": {
                      "show": true,
                      "componentUI": "AddressForm",
                      "parentControl": "recipientId"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "addresses",
                      "label": [
                          "streetAddress",
                          "zipCode",
                          "location",
                          "country.name"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "addresses.id"
                          },
                          {
                              "accessor": "addresses.streetAddress"
                          },
                          {
                              "accessor": "addresses.location"
                          },
                          {
                              "accessor": "addresses.zipCode"
                          },
                          {
                              "accessor": "addresses.country.name"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "recipientId",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Recipient Address",
              "name": "recipientAddress",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "762",
              "fieldAttributes": {
                  "id": 1,
                  "name": "shuId",
                  "sort": 3,
                  "rules": {
                      "required": "The SHU ID is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "helper": {
                      "title": "SHU-ID",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "SHU-ID",
                      "variant": "outlined",
                      "disabled": false,
                      "fullWidth": true,
                      "multiline": false
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "SHU-ID",
              "name": "shuId",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "765",
              "fieldAttributes": {
                  "id": 1,
                  "name": "shipmentPurposeTx",
                  "sort": 4.1,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      4,
                      4
                  ],
                  "helper": {
                      "title": "Other Purpose",
                      "placement": "top"
                  },
                  "inputProps": {
                      "rows": 1,
                      "label": "Other Purpose",
                      "variant": "outlined",
                      "disabled": false,
                      "fullWidth": true,
                      "multiline": false,
                      "placeholder": "Please specify if other"
                  },
                  "showInGrid": false,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.fiel"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "shipmentPurposeDd",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      },
                      "showControlRules": {
                          "expectedValue": "Other",
                          "parentControl": "shipmentPurposeDd"
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "",
              "description": "Other Purpose",
              "name": "shipmentPurposeTx",
              "required": false,
              "attributes": null,
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "1493",
              "fieldAttributes": {
                  "id": 6,
                  "sort": 22.1,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "Set Recipient same as requestor",
                      "placement": "top"
                  },
                  "function": "setRecipientSameAsRequestor",
                  "inputProps": {
                      "color": "primary",
                      "label": "Set Requestor Information to Recipient",
                      "disabled": false
                  },
                  "showInGrid": false,
                  "defaultValue": false
              },
              "apiAttributesName": "",
              "description": "Set Same As Requestor",
              "name": "sameAsRequestor",
              "required": false,
              "attributes": null,
              "cfType": {
                  "id": "6",
                  "description": "Switch",
                  "name": "switch",
                  "type": "String"
              }
          },
          {
              "id": "1597",
              "fieldAttributes": {
                  "id": 1,
                  "name": "status",
                  "sort": 0,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "disabled": true,
                  "inputProps": {
                      "rows": 1,
                      "label": "Status",
                      "variant": "outlined",
                      "disabled": false,
                      "className": "hidden",
                      "fullWidth": true,
                      "multiline": false
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "status",
              "description": "Status",
              "name": "status",
              "required": false,
              "attributes": {
                  "id": "2582",
                  "description": "adminContact",
                  "isRequired": true,
                  "name": "admin_contact",
                  "sortNo": 8
              },
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "1598",
              "fieldAttributes": {
                  "id": 1,
                  "name": "totalItems",
                  "sort": 2.1,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "disabled": true,
                  "inputProps": {
                      "rows": 1,
                      "label": "Total Items",
                      "variant": "outlined",
                      "disabled": false,
                      "className": "hidden",
                      "fullWidth": true,
                      "multiline": false
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "totalItems",
              "description": "Total Items",
              "name": "totalItems",
              "required": false,
              "attributes": {
                  "id": "2582",
                  "description": "adminContact",
                  "isRequired": true,
                  "name": "admin_contact",
                  "sortNo": 8
              },
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "1599",
              "fieldAttributes": {
                  "id": 1,
                  "name": "totalWeight",
                  "sort": 2.2,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "disabled": true,
                  "inputProps": {
                      "rows": 1,
                      "label": "Total Weight",
                      "variant": "outlined",
                      "disabled": false,
                      "className": "hidden",
                      "fullWidth": true,
                      "multiline": false
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "totalWeight",
              "description": "Total Weight",
              "name": "totalWeight",
              "required": false,
              "attributes": {
                  "id": "2582",
                  "description": "adminContact",
                  "isRequired": true,
                  "name": "admin_contact",
                  "sortNo": 8
              },
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "1600",
              "fieldAttributes": {
                  "id": 1,
                  "name": "totalPackages",
                  "sort": 2.3,
                  "rules": {
                      "required": ""
                  },
                  "sizes": [
                      12,
                      12,
                      12,
                      12,
                      12
                  ],
                  "helper": {
                      "title": "",
                      "placement": "top"
                  },
                  "disabled": true,
                  "inputProps": {
                      "rows": 1,
                      "label": "Total Packages",
                      "variant": "outlined",
                      "disabled": false,
                      "className": "hidden",
                      "fullWidth": true,
                      "multiline": false
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "id",
                      "customFilters": [],
                      "parentControl": "control name",
                      "sequenceRules": {
                          "ruleName": "",
                          "applyRule": false
                      }
                  },
                  "defaultValue": ""
              },
              "apiAttributesName": "totalPackages",
              "description": "Total Packages",
              "name": "totalPackages",
              "required": false,
              "attributes": {
                  "id": "2582",
                  "description": "adminContact",
                  "isRequired": true,
                  "name": "admin_contact",
                  "sortNo": 8
              },
              "cfType": {
                  "id": "1",
                  "description": "TextField",
                  "name": "TextField",
                  "type": "String"
              }
          },
          {
              "id": "804",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 23,
                  "rules": {
                      "required": "This field is required."
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      6,
                      6
                  ],
                  "entity": "ContactType",
                  "helper": {
                      "title": "Recipient Type",
                      "placement": "top"
                  },
                  "labels": [
                      "name"
                  ],
                  "columns": [
                      {
                          "accessor": "label"
                      }
                  ],
                  "filters": [
                      {
                          "col": "category.name",
                          "mod": "EQ",
                          "val": "Person"
                      }
                  ],
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Recipient Type",
                      "variant": "outlined"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  },
                  "defaultOptions": [
                      {
                          "label": "PRI - Private Companies",
                          "value": 1
                      },
                      {
                          "label": "AI - Academic Institutions",
                          "value": 2
                      },
                      {
                          "label": "ARI - Advance Research Institutions",
                          "value": 3
                      },
                      {
                          "label": "CGCP - CGIAR Challenge Program",
                          "value": 4
                      },
                      {
                          "label": "DO - Development Organizations",
                          "value": 5
                      },
                      {
                          "label": "FI - Financing Institutions",
                          "value": 6
                      },
                      {
                          "label": "FO - Foundations",
                          "value": 7
                      },
                      {
                          "label": " GO - Government",
                          "value": 8
                      },
                      {
                          "label": " IARC - International Agricultural Research Centers",
                          "value": 9
                      },
                      {
                          "label": "IO - International Organizations, Individual, Media",
                          "value": 10
                      },
                      {
                          "label": "Individual",
                          "value": 11
                      },
                      {
                          "label": "Media",
                          "value": 12
                      },
                      {
                          "label": "NARES - National Agriculture Research and Extension Systems",
                          "value": 13
                      },
                      {
                          "label": " NGO - Non governmental organizations",
                          "value": 14
                      },
                      {
                          "label": "SRO - Subregional organization",
                          "value": 15
                      },
                      {
                          "label": "Others",
                          "value": 16
                      }
                  ]
              },
              "apiAttributesName": "",
              "description": "Recipient Type",
              "name": "recipientType",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "800",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 19,
                  "rules": {
                      "required": "This field is required."
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      3,
                      3
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Recipient Name",
                      "placement": "top"
                  },
                  "labels": [
                      "id",
                      "person.fullName"
                  ],
                  "columns": [
                      {
                          "name": "Recipient Name",
                          "accessor": "recipient.person.givenName"
                      },
                      {
                          "name": "Recipient Last Name",
                          "accessor": "recipient.person.familyName"
                      },
                      {
                          "name": "Recipient Email",
                          "accessor": "recipient.email"
                      }
                  ],
                  "filters": [
                      {
                          "col": "category.name",
                          "mod": "EQ",
                          "val": "Person"
                      }
                  ],
                  "apiContent": [
                      "id",
                      "person.givenName",
                      "person.familyName",
                      "person.fullName"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Recipient Name",
                      "variant": "outlined"
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "",
                      "field": "person",
                      "label": [
                          "familyName",
                          "givenName"
                      ],
                      "entity": "Contact",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "another.field"
                          }
                      ],
                      "applyRules": false,
                      "columnFilter": "category.name",
                      "customFilters": [],
                      "parentControl": "control name"
                  }
              },
              "apiAttributesName": "recipient",
              "description": "Recipient Name",
              "name": "recipient",
              "required": true,
              "attributes": {
                  "id": "2606",
                  "description": "node_type_id",
                  "isRequired": true,
                  "name": "node_type_id",
                  "sortNo": 27
              },
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "803",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 22,
                  "rules": {
                      "required": "The recipient institution is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Recipient Institution",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [],
                  "disabled": false,
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Recipient Institution",
                      "variant": "outlined"
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "",
                      "label": [
                          "institution.institution.legalName",
                          "institution.institution.commonName"
                      ],
                      "entity": "Hierarchy",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "institution.institution.legalName"
                          },
                          {
                              "accessor": "institution.institution.commonName"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "contact.id",
                      "customFilters": [
                          {
                              "col": "institution.category.name",
                              "mod": "EQ",
                              "val": "Institution"
                          }
                      ],
                      "parentControl": "recipientId"
                  }
              },
              "apiAttributesName": "",
              "description": "Recipient Institution",
              "name": "recipientInstitution",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "799",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 18,
                  "rules": {
                      "required": "The requestor institution is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Requestor Institution",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [],
                  "disabled": false,
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Requestor Institution",
                      "variant": "outlined"
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "",
                      "label": [
                          "institution.institution.legalName",
                          "institution.institution.commonName"
                      ],
                      "entity": "Hierarchy",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "institution.institution.legalName"
                          },
                          {
                              "accessor": "institution.institution.commonName"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "contact.id",
                      "customFilters": [
                          {
                              "col": "institution.category.name",
                              "mod": "EQ",
                              "val": "Institution"
                          }
                      ],
                      "parentControl": "requestorId"
                  }
              },
              "apiAttributesName": "",
              "description": "Requestor Institution",
              "name": "requestorInstitution",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          },
          {
              "id": "793",
              "fieldAttributes": {
                  "id": 5,
                  "sort": 13,
                  "rules": {
                      "required": "The sender institution is required"
                  },
                  "sizes": [
                      12,
                      12,
                      6,
                      2,
                      2
                  ],
                  "entity": "Contact",
                  "helper": {
                      "title": "Sender Institution",
                      "placement": "top"
                  },
                  "columns": [
                      {
                          "accessor": ""
                      }
                  ],
                  "filters": [],
                  "disabled": false,
                  "apiContent": [
                      "id",
                      "name"
                  ],
                  "inputProps": {
                      "color": "primary",
                      "label": "Sender Institution",
                      "variant": "outlined"
                  },
                  "modalPopup": {
                      "show": false,
                      "componentUI": ""
                  },
                  "showInGrid": true,
                  "defaultRules": {
                      "uri": "cs",
                      "field": "",
                      "label": [
                          "institution.institution.legalName",
                          "institution.institution.commonName"
                      ],
                      "entity": "Hierarchy",
                      "apiContent": [
                          {
                              "accessor": "id"
                          },
                          {
                              "accessor": "institution.institution.legalName"
                          },
                          {
                              "accessor": "institution.institution.commonName"
                          }
                      ],
                      "applyRules": true,
                      "columnFilter": "contact.id",
                      "customFilters": [
                          {
                              "col": "institution.category.name",
                              "mod": "EQ",
                              "val": "Institution"
                          }
                      ],
                      "parentControl": "senderId"
                  }
              },
              "apiAttributesName": "",
              "description": "Sender Institution",
              "name": "senderInstitution",
              "required": true,
              "attributes": null,
              "cfType": {
                  "id": "5",
                  "description": "Select",
                  "name": "select",
                  "type": "String"
              }
          }
      ]
  }],
    selectEntComp: [{
      "id": "760",
      "fieldAttributes": {
          "id": 5,
          "sort": 1,
          "rules": {
              "required": "The program is required."
          },
          "sizes": [
              12,
              12,
              6,
              4,
              4
          ],
          "entity": "Program",
          "helper": {
              "title": "Program",
              "placement": "top"
          },
          "labels": [
              "name"
          ],
          "columns": [],
          "filters": [],
          "apiContent": [
              "id",
              "name",
              "code"
          ],
          "inputProps": {
              "color": "primary",
              "label": "Program",
              "variant": "outlined"
          },
          "showInGrid": true,
          "defaultRules": {
              "uri": "",
              "field": "person",
              "label": [
                  "familyName",
                  "givenName"
              ],
              "entity": "Contact",
              "apiContent": [
                  {
                      "accessor": "id"
                  },
                  {
                      "accessor": "another.field"
                  }
              ],
              "applyRules": false,
              "columnFilter": "category.name",
              "customFilters": [],
              "parentControl": "control name"
          }
      },
      "apiAttributesName": "program",
      "description": "Program",
      "name": "program",
      "required": true,
      "attributes": {
          "id": "2662",
          "description": "seed_code",
          "isRequired": true,
          "name": "seed_code",
          "sortNo": 29
      },
      "cfType": {
          "id": "5",
          "description": "Select",
          "name": "select",
          "type": "String"
      }
  }],
    message: null,
    components:[{
        "sort": 1,
        "component": "select",
        "options": [
            {
                "label": "Irrigated South-East Asia",
                "value": 92
            }
        ],
        "helper": {
            "title": "Program",
            "placement": "top"
        },
        "name": "programId",
        "sizes": [
            12,
            12,
            6,
            4,
            4
        ],
        "defaultRules": {
            "uri": "",
            "field": "person",
            "label": [
                "familyName",
                "givenName"
            ],
            "entity": "Contact",
            "apiContent": [
                {
                    "accessor": "id"
                },
                {
                    "accessor": "another.field"
                }
            ],
            "applyRules": false,
            "columnFilter": "category.name",
            "customFilters": [],
            "parentControl": "control name"
        },
        "inputProps": {
            "color": "primary",
            "label": "Program",
            "variant": "outlined"
        },
        "rules": {
            "required": "The program is required."
        },
        "defaultValue": {
            "label": "Irrigated South-East Asia",
            "value": 92
        }
    }],
    customFieldsData:[{
        "cfId": "57",
        "eventId": "3",
        "name": "shuId",
        "cfNode": {
            "id": "762",
            "fieldAttributes": {
                "id": 1,
                "name": "shuId",
                "sort": 3,
                "rules": {
                    "required": "The SHU ID is required"
                },
                "sizes": [
                    12,
                    12,
                    6,
                    2,
                    2
                ],
                "helper": {
                    "title": "SHU-ID",
                    "placement": "top"
                },
                "inputProps": {
                    "rows": 1,
                    "label": "SHU-ID",
                    "variant": "outlined",
                    "disabled": false,
                    "fullWidth": true,
                    "multiline": false
                },
                "showInGrid": true,
                "defaultRules": {
                    "uri": "",
                    "field": "",
                    "label": [
                        "familyName",
                        "givenName"
                    ],
                    "entity": "Contact",
                    "apiContent": [
                        {
                            "accessor": "id"
                        },
                        {
                            "accessor": "another.field"
                        }
                    ],
                    "applyRules": false,
                    "columnFilter": "id",
                    "customFilters": [],
                    "parentControl": "control name",
                    "sequenceRules": {
                        "ruleName": "",
                        "applyRule": false
                    }
                },
                "defaultValue": ""
            },
            "apiAttributesName": "",
            "description": "SHU-ID",
            "name": "shuId",
            "required": true,
            "attributes": null,
            "cfType": {
                "id": "1",
                "description": "TextField",
                "name": "TextField",
                "type": "String"
            }
        }
    }]
  }