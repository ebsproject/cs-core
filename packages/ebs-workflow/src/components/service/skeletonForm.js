import React from 'react';
import { Core, Lab } from "@ebs/styleguide";
const {  Grid, Box }  = Core;
const { Skeleton }  = Lab;

const FormSkeleton = () => {
    return (
      <Box data-testid={"FormSkeletonTestId"}>
        <Grid container spacing={2}>
          {[...Array(30)].map((_, index) => (
            <Grid item xs={3} key={index}>
              <Skeleton height={65} width={300} variant="text" />
            </Grid>
          ))}
        </Grid>
      </Box>
    );
  };
  
  export default FormSkeleton;
