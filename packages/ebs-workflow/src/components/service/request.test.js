import { render, screen, waitFor, cleanup, act } from "@testing-library/react";
import Request from "./request";
import { useServiceContext } from "custom-components/context/service-context";
import { useSelector, useDispatch } from "react-redux";
import { fetchDefinition } from "./functions";
import { workflowVariablesMock, workflowValuesMock, rowDataMock, responseMock } from "./mock_data";


jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context");
jest.mock("./functions", () => ({
  fetchDefinition: jest.fn(),
}));
jest.mock("./loading-component", () => jest.fn(() => <div>Loading...</div>));

describe("Request Component", () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    jest.clearAllMocks();
    useDispatch.mockReturnValue(mockDispatch);
  });

  const renderComponent = () => render(<Request definition={{}} />);

  test("renders loading component when nodeArray or dropDown is missing", async () => {
    useServiceContext.mockReturnValue({
      mode: "view",
      setRefresh: jest.fn(),
      refresh: false,
      setLoading: jest.fn(),
      rowData: { id: 1, byOccurrence: false },
      setRequestId: jest.fn()
    });
    useSelector.mockImplementation((selectorFn) => {
      return selectorFn({
        wf_workflow: {
          workflowPhase: {id:1, name:"Phase A", sequence:1, stages: [{id:1, name:"Stage A"}] },
          workflowValues: { phases: [{id:1,name:"Phase A", sequence:1, stages:[{id:1,sequence:1, name:"stage A", nodes:[], workflowViewType:{name:"TabVertical"}}]}] },
          workflowVariables: { entity: "someEntity", customFields: [] },
        },
      });
    });
    act(() => {
      renderComponent();
    })
    await waitFor(()=>{
      expect(screen.getByText(/loading.../i)).toBeInTheDocument();
    })
  
  });
  test("renders loading component when nodeArray or dropDown is provided", async  () => {
 await fetchDefinition.mockResolvedValue(responseMock);
    useServiceContext.mockReturnValue({
      mode: "create",
      setRefresh: jest.fn(),
      refresh: false,
      setLoading: jest.fn(),
      rowData:rowDataMock,
      setRequestId: jest.fn()
    });
    useSelector.mockImplementation((selectorFn) => {
      return selectorFn({
        wf_workflow: {
          workflowPhase: {id:1, name:"Phase A", sequence:1, stages: [{id:1, name:"Stage A"}] },
          workflowValues: workflowValuesMock,
          workflowVariables:workflowVariablesMock ,
        },
      });
    });
    act(() => {
      render(<Request definition={{id:1, name:"definition A"}} />)
     
    }) //review this test
    await waitFor (()=>{
      // screen.debug()
    })
    

  });

});
