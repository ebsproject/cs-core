import FormSkeleton from "./skeletonForm";
import { render, cleanup, screen } from "@testing-library/react";
import "@testing-library/dom";

afterEach(cleanup);

test("FormSkeleton is in the DOM", () => {
    render(
                <FormSkeleton />
    );
    expect(screen.getByTestId("FormSkeletonTestId")).toBeInTheDocument();
});
