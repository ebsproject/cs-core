import React, { Fragment } from "react";
import { Core } from "@ebs/styleguide";
const { Typography, Box } = Core;
import { useSelector } from "react-redux";
//const Request = React.lazy(() => import("./request"));
import Request from "./request";
import { userContext, useUserContext } from "@ebs/layout";
import { useContext, useState, useEffect, Suspense } from "react";
import { loadDefinitions } from "hooks/utils/functions";
import { getServiceData } from "hooks/utils/functions";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { client } from "utils/apollo";
import { createDefinitionCF, createDefinitionFromEntity } from "components/runtime/DialogBox/functions";
import { FIND_CF_VALUES_BY_EVENT_ID } from "utils/apollo/gql/workflow";
import { useServiceContext } from "custom-components/context/service-context";

const Service = () => {
  const { workflowValues, workflowVariables } = useSelector(({ wf_workflow }) => wf_workflow);
  let _workflowValues = { ...workflowValues }
  const [definition, setDefinition] = useState({});
  const { requestId, setRequestId, mode, refresh, rowData, setRowData } = useServiceContext();
  const { userProfile } = useUserContext();//useContext(userContext);
  useEffect(() => {
    if (mode === 'edit') {
      const fetchDefinition = async () => {
        let definition = { components: [] };
        try {
          const { data } = await client.query({
            query: FIND_CF_VALUES_BY_EVENT_ID,
            variables: {
              page: { number: 1, size: 300 },
              filters: [{ col: "event.recordId", mod: "EQ", val: Number(rowData.id || 0) }],
              sort: [{ col: "nodeCF.id", mod: "ASC" }],
            },
            fetchPolicy: "no-cache",
          });
          const cfValues = data.findCFValueList.content;
          if (cfValues.length > 0) {
            definition = await createDefinitionCF(cfValues);
          }
          let result = await createDefinitionFromEntity(workflowVariables, rowData);
          result.components.forEach((item) => {
            definition.components.push(item);
          });
          definition.components = definition.components.sort((a, b) => {
            return a.sort - b.sort;
          });
          setDefinition(definition)
        } catch (error) {
          console.log(error)
        }

      }
      fetchDefinition();
    }
    return () => setRequestId(null);
  }, [refresh]);

  useEffect(() => {
    if (requestId) {
      const fetchData = async () => {
        const defaultFilters = [
          { col: "workflowId", mod: "EQ", val: _workflowValues.id },
          { col: "userId", mod: "EQ", val: userProfile?.dbId },
          { col: "id", mod: "EQ", val: requestId },
        ];
        let row_data = await getServiceData(defaultFilters);
        let definition = await loadDefinitions(row_data, workflowVariables);
        setDefinition(definition);
        setRowData(row_data);
      }
      fetchData();
    }
    return () => setRequestId(null);
  }, [requestId, refresh]);

  return (
    <Fragment>
      <Typography variant="h4">{`${mode === "edit" ? "Edit" : "Create"} ${_workflowValues.navigationName
        } request`}</Typography>
      <Box>&nbsp;</Box>
        <Request
          definition={definition}
        />
    </Fragment>
  );
};
export default Service;
