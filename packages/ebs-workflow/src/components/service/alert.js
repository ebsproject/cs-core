import { Core, Icons } from "@ebs/styleguide";
import PropTypes from "prop-types";
const { Dialog, DialogContent, Typography, Icon, Image } = Core;
const { Warning } = Icons;
import loader from "assets/images/time_loader.gif"
import { useServiceContext } from "custom-components/context/service-context";

const RequestMessage = ({ mode }) => {
  const { blockButtons } = useServiceContext();
  return (
      <Dialog open={blockButtons} maxWidth={"md"} data-testid={"RequestMessageTestId"}>
        <DialogContent sx={{ padding: 2 }}>
          <Typography variant="title" color="primary">
            <Icon>
              <Warning />
            </Icon>
            {`Please wait. Your request is being ${mode ? mode === "create" ? "created" : "processed" : "processed"}.`}
          </Typography>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "100%"
            }}>
            <img src={loader} alt="loading..." />
          </div>

        </DialogContent>
      </Dialog>
  );
};
// Type and required properties
RequestMessage.propTypes = {
  mode: PropTypes.string,
  open: PropTypes.bool,
};
// Default properties

export default RequestMessage;
