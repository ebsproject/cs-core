import NodeTabs from "./nodeTabs";
import { render, cleanup, screen, act, waitFor } from "@testing-library/react";
import "@testing-library/dom";
import { useSelector, useDispatch } from "react-redux";
import { useServiceContext } from "custom-components/context/service-context";
import { responseMock, workflowValuesMock, workflowVariablesMock } from "./mock_data";
import { checkNodeStatus } from "./functions";
import { checkPermissions } from "validations";

jest.mock("./formTab", () => jest.fn(() => <div>Form Tab Component</div>));
jest.mock("./accessDenied", () => jest.fn(() => <div>Access Denied Component</div>));
jest.mock("components/runtime/GenerateForm/generateForm", () => jest.fn(() => <div>Generate Form Component</div>));

jest.mock("./functions", () => ({
    checkNodeStatus: jest.fn(),
  }));
jest.mock("validations", () => ({
    checkPermissions: jest.fn(),
  }));
jest.mock("react-redux", () => ({
    useSelector: jest.fn(),
    useDispatch: jest.fn(),
}));
jest.mock("custom-components/context/service-context", () => ({
    useServiceContext: jest.fn(),
}));
jest.mock('react-intl', () => ({
    FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("NodeTabs component", () => {

    beforeEach(() => {
        jest.clearAllMocks();
        useServiceContext.mockReturnValue({
            mode: "edit",
            setBlockButtons: jest.fn(),
            rowData: { id: 1, byOccurrence: false }
        });
        useSelector.mockImplementation((selectorFn) => {
            return selectorFn({
                wf_workflow: {
                    workflowValues: workflowValuesMock,
                    workflowVariables: workflowVariablesMock,
                    workflowPhase: { id: 1, name: "test phase", sequence: 1, stages: [{ id: 1, name: "", sequence: 1, nodes: [{ id: 1, name: "", sort: 1 }] }] },
                },
                wf_user: {
                    userPermissions: { id: 1, name: "User Permissions" }
                }
            });
        });
    })

    const renderComponent = (props) => render(
        <NodeTabs
            selectEntityComponents={props.selectEntityComponentsMock}
            buttonRef={props.buttonRefMock}
            exitForm={props.exitFormMock}
            nodeArray={props.nodeArrayMock}
            tabMode={props.tabModeMock} />)

    test("node tabs loaded with loading message", async () => {
       await checkNodeStatus.mockReturnValue(false);
       checkPermissions.mockReturnValue(true);
        const props = {
        selectEntityComponentsMock : responseMock.selectEntComp,
         buttonRefMock :jest.fn(),
         exitFormMock :jest.fn(),
         nodeArrayMock : responseMock.formNodes,
         tabModeMock :"Vertical",}

        act(() => {
            renderComponent(props);
        });
        await waitFor(()=>{
            expect(screen.getByText(/Building the workflow components, please wait..../i)).toBeInTheDocument();
        })
     
    })

    test("node tabs loaded with access denied", async () => {
        await checkNodeStatus.mockReturnValue(false);
        checkPermissions.mockReturnValue(false);
         const props = {
         selectEntityComponentsMock : responseMock.selectEntComp,
          buttonRefMock :jest.fn(),
          exitFormMock :jest.fn(),
          nodeArrayMock : responseMock.formNodes,
          tabModeMock :"Vertical",}
 
         act(() => {
             renderComponent(props);
         });
         await waitFor(()=>{ //TODO:: check this test
            screen.debug();
           //  expect(screen.getByText(/Building the workflow components, please wait..../i)).toBeInTheDocument();
         })
      
     })

});
