import LoadingComponent from "./loading-component";
import { render, screen } from "@testing-library/react";
import "@testing-library/dom";



describe("Loading Component", () => {
  const messageMock = "Test message"
  test("loading component in DOM", () => {
    render(<LoadingComponent message={messageMock} />);
      expect(screen.getByTestId("LoadingComponentTestId")).toBeInTheDocument();
  })


});
