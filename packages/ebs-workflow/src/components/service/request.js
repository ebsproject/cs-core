import { Core } from "@ebs/styleguide";
import PropTypes from "prop-types";
const { Typography } = Core;
import { useSelector } from "react-redux";
import { useEffect, useRef, useState, lazy } from "react";
import StepperBar from "components/runtime/StepperBar/StepperBar";
import { useDispatch } from "react-redux";
import RequestMessage from "./alert";
import { fetchDefinition } from "./functions";
import NodeTabs from "./nodeTabs";
import loader from "assets/images/time_loader.gif";
import { useServiceContext } from "custom-components/context/service-context";
import { setWorkflowPhase } from "store/modules/Workflow";
import LoadingComponent from "./loading-component";

const Request = ({ definition }) => {
  const { workflowValues, workflowVariables } = useSelector(
    ({ wf_workflow }) => wf_workflow
  );
  const { mode, setRefresh, refresh, setLoading, rowData, setRequestId, setDefinition } = useServiceContext();
  const buttonRef = useRef(null);
  const dispatch = useDispatch();
  const [dropDown, setDropDown] = useState(null);
  const [tabMode, setTabMode] = useState("horizontal");
  const [nodes, setNodes] = useState(null);
  const [nodeArray, setNodeArray] = useState([]);
  const [errorMessage, setErrorMessage] = useState(null);
  //TODO::: fix the set of the workflow phase or check if its save to remove it
  useEffect(() => { 
    const orderedPhases = workflowValues.phases.filter(item => Number(item.sequence) == 1)
    dispatch(setWorkflowPhase(orderedPhases[0]));
  }, []);

  useEffect(() => {
    let wv = { ...workflowValues }
    let _phases = [...wv.phases];
    let _stages = []// [..._phases[0].stages];

    _phases.forEach(p => {
      p.stages.forEach(s => {
        _stages = [..._stages, s]
      })
    })
      let _nodes = [];
      _stages
        .sort((a, b) => {
          return Number(a.sequence) - Number(b.sequence);
        })
        .forEach((element) => {
          element.workflowViewType.name === "TabVertical" ? setTabMode("vertical") : null;
          element.nodes.forEach((nodeItem) => {
            if (!nodeItem.name.includes('process-'))
              _nodes = [..._nodes, nodeItem];
          });
        });
      setNodes(_nodes);
  }, [mode, rowData]);

  useEffect(() => {
    let mounted = true;
    const getDefinition = async () => {
      const cc = await import("custom-components");
      if (nodes && nodes.length > 0) {
        let _nodes = [...nodes];
        const isByOccurrenceRequest = rowData && rowData["byOccurrence"];
        if (isByOccurrenceRequest)
          _nodes = _nodes.filter(item =>item.name.toLowerCase() !== 'items');
        else
          _nodes = _nodes.filter(item => item.name.toLowerCase() !== 'occurrences');
        const { formNodes, selectEntComp, message } = await fetchDefinition(_nodes, mounted, mode, workflowVariables, cc, rowData);
        setDropDown(selectEntComp);
        setNodeArray(formNodes);
        if (message) setErrorMessage(message);
      }
    }
    getDefinition();
    return () => mounted = false;
  }, [nodes, workflowVariables]);

  const exitForm = () => {
    setLoading(false);
  };
  const refreshAction = () => {
    setTimeout(async () => {
      setLoading(false);
      setRequestId(Number(rowData?.id));
      setRefresh(!refresh);
      setDefinition(null);
    }, 3500);
  }

  if (!dropDown) return <LoadingComponent message={"Loading nodes..."} />;
  if ((mode === "view" || mode === "edit") && (!rowData || Object.keys(definition).length === 0)) return <LoadingComponent message={"Loading form definition..."}/>;

  return (
    <div data-testid={"RequestTestId"}>
      <RequestMessage mode={mode} />
      <StepperBar buttonRef={buttonRef} phases={[...workflowValues.phases]} refreshAction={refreshAction} />
      {nodeArray.length > 0 ? (
        <NodeTabs selectEntityComponents={dropDown} exitForm={exitForm} nodeArray={nodeArray} tabMode={tabMode} buttonRef={buttonRef} />
      ) : errorMessage ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            height: "20vh",
          }}
        >
          <Typography variant="h5" color="error">
            {errorMessage}
          </Typography>
        </div>
      ) : (
        <>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              height: "20vh",
            }}
          >
            <Typography variant="title" color="primary">
              {"Building the workflow components, please wait...."}
            </Typography>
          </div>
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <img src={loader} alt="Loading..." />
          </div>
        </>
      )}
    </div>
  )
};
// Type and required properties
Request.propTypes = {
  mode: PropTypes.string,
  rowData: PropTypes.object,
  definition: PropTypes.object,
};
// Default properties

export default Request;