import AccessDenied from "./accessDenied";
import { render, screen } from "@testing-library/react";
import "@testing-library/dom";

const message ="Some message";

describe("AccessDenied Component", () => {
  test("should render the component with the correct message provided", () => {
    render(<AccessDenied message={message}/>);
    expect(screen.getByTestId("AccessDeniedTestId")).toBeInTheDocument();
    expect(screen.getByText(message)).toBeInTheDocument();
  });

  test("should render the component with the default message if not provided", () => {
    render(<AccessDenied />);
    expect(screen.getByTestId("AccessDeniedTestId")).toBeInTheDocument();
    expect(screen.getByText(/Sorry, you do not have access to this content./i)).toBeInTheDocument();
  });

});
