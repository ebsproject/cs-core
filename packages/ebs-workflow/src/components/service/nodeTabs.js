import { Core, EbsTabsLayout, Icons } from "@ebs/styleguide";
import AccessDenied from "./accessDenied";
import FormTab from "./formTab";
import { useEffect, useState } from "react";
import { FormattedMessage } from "react-intl";
import { useSelector } from "react-redux";
const { Paper, IconButton, Typography } = Core;
const { CheckCircle } = Icons;
import { checkPermissions } from "validations";
import { checkNodeStatus } from "./functions";
import GenerateForm from "components/runtime/GenerateForm/generateForm";
import loader from "assets/images/time_loader.gif";
import { useServiceContext } from "custom-components/context/service-context";


const NodeTabs = ({ selectEntityComponents, exitForm, nodeArray, tabMode, buttonRef }) => {
    const [currentTabIndex, setCurrentTabIndex] = useState(0);
    const { mode, rowData, setBlockButtons } = useServiceContext();
    const [tabNodes, setTabNodes] = useState([]);
    const { workflowVariables } = useSelector(
        ({ wf_workflow }) => wf_workflow
    );
    const entity = workflowVariables.entity
    const { userPermissions } = useSelector(({ wf_user }) => wf_user);
    const [customComponents, setCustomComponents] = useState(null);
    useEffect(() => {
        const fetchCustomComponents = async () => {
            const customComp = await import("custom-components");
            setCustomComponents(customComp);
        };
        fetchCustomComponents();
    },[]);
    useEffect(() => {
        if (rowData && (mode === "edit" || mode === "view")) {
            const transformData = async () => {
                let flag = false;
                let newArray = [];
                let formNodes = [...nodeArray].sort((a, b) => {
                    return a.sequence - b.sequence;
                })
                const isByOccurrenceRequest = rowData && rowData["byOccurrence"];
                if (isByOccurrenceRequest)
                    formNodes = formNodes.filter(item => Number(item.id) !== 20665);
                else
                    formNodes = formNodes.filter(item => Number(item.id) !== 21890);
                formNodes.map(array => array);
                for (let i = 0; i < formNodes.length; i++) {
                    const completed = await checkNodeStatus(formNodes[i].id, rowData.wfInstance);
                    if (!completed && !flag) {
                        setCurrentTabIndex(i)
                        flag = true;
                    }
                    let obj = { ...formNodes[i], ...{ completed: completed } }
                    newArray = [...newArray, obj]
                }
                setTabNodes(newArray);
                setBlockButtons(false);
            }
            transformData();
        } else {
            setTabNodes(nodeArray)
        }

    }, [rowData, nodeArray]);

    const selectComponent = (node, _buttonRef, index) => {
        let disabled = false;
        let securityDefinition = node.securityDefinition;
        let access = checkPermissions(userPermissions, securityDefinition);
        if ((node.nodeType.name !== "form" && mode === "create") || (index > 0 && mode === "create")) {
            return <AccessDenied message={"Please input all the basic information before continuing in this tab."} />
        }
        if (!access) {
            return <AccessDenied />
        }
        switch (node.nodeType.name) {
            case "form":
                return (
                    <FormTab selectEntityComponents={selectEntityComponents} node={node} exitForm={exitForm} _buttonRef={_buttonRef} disabled={disabled} />
                );
            case "External": {
                const ExternalComponent = customComponents[node.define.component];
                return ExternalComponent ? (
                    <ExternalComponent id={rowData.id} entity={entity} node={node} />
                ) : (
                    <h1>Please check the custom components definition for this workflow</h1>
                );
            }
            default:
                if (node.process.code === "FORM") {
                    return (
                        <>
                            <GenerateForm _nodeId={Number(node.id)} buttonRef={buttonRef} />
                        </>
                    );
                }
        }
    };
    const loadingComponent = () => {
        return (
            <>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                        height: "20vh",
                    }}
                >
                    <Typography variant="title" color="primary">
                        {"Building the workflow components, please wait...."}
                    </Typography>
                </div>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                    }}
                >
                    <img src={loader} alt="Loading..." />
                </div>
            </>
        )
    }   
if ((!customComponents && (mode === "edit" || mode === "view"))) return loadingComponent();
    return (
        <div data-testid={"NodeTabsTestId"}>
            <EbsTabsLayout
                index={currentTabIndex}
                orientation={tabMode}
                tabs={tabNodes.map((item, index) => {
                    return {
                        label: (
                            <>
                                <FormattedMessage id={"none"} defaultMessage={item.name} />
                                {" "}
                                {item.completed && (
                                    <IconButton color={"primary"} size={"medium"}>
                                        <CheckCircle />
                                    </IconButton>
                                )}
                            </>
                        ),
                        component: (
                            <Paper elevation={5} style={{ padding: 40 }}>
                                {selectComponent(item, buttonRef, index)}
                            </Paper>
                        ),
                    };
                })}
            />
        </div>
    )
}
export default NodeTabs;