import React from "react";
import { act, render, screen, waitFor } from "@testing-library/react";
import { useSelector } from "react-redux";
import { client } from "utils/apollo";
import Service from "./service";
import { useServiceContext } from "custom-components/context/service-context";
import { responseMock, rowDataMock, workflowValuesMock, workflowVariablesMock } from "./mock_data";
import { useUserContext } from "@ebs/layout";
import { getServiceData, loadDefinitions } from "hooks/utils/functions";
import { createDefinitionCF, createDefinitionFromEntity } from "components/runtime/DialogBox/functions";

jest.mock("components/runtime/DialogBox/functions", () => ({
  createDefinitionCF: jest.fn(),
  createDefinitionFromEntity: jest.fn()
}))

jest.mock("utils/apollo", () => ({
  client: {
    query: jest.fn(),
  },
}));

jest.mock("@ebs/layout", () => ({
  useUserContext: jest.fn(),
}));

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));

jest.mock("components/runtime/DialogBox/functions", () => ({
  createDefinitionCF: jest.fn().mockResolvedValue({ components: [] }),
  createDefinitionFromEntity: jest.fn().mockResolvedValue({ components: [] }),
}));
jest.mock("hooks/utils/functions", () => ({
  getServiceData: jest.fn().mockResolvedValue({ id: 1 }),
  loadDefinitions: jest.fn().mockResolvedValue({}),
}));

jest.mock("components/service/request", () => () => <div data-testid={"RequestTestId"}>Request Component</div>);

describe("Service Component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
    useSelector.mockImplementation((selectorFn) => {
      return selectorFn({
        wf_workflow: {
          workflowValues: workflowValuesMock,
          workflowVariables: workflowVariablesMock,
        },
      });
    });
    useUserContext.mockReturnValue({
      userProfile: { dbId: 123, userName: "j.ramirez@cgiar.org" },
    });
    useServiceContext.mockReturnValue({
      requestId: 23,
      setRequestId: jest.fn(),
      mode: "edit",
      refresh: false,
      rowData: rowDataMock,
      setRowData: jest.fn(),
    });
  });
  const renderComponent = () => render(<Service />);

  test("renders Service component in 'edit' mode", async () => {
    await client.query.mockResolvedValue({ data: { findCFValueList: { content: [] } } });
    await createDefinitionCF.mockReturnValue({ components: [] });
    await createDefinitionFromEntity.mockReturnValue({ components: [] });
    await getServiceData.mockReturnValue(rowDataMock);
    await loadDefinitions.mockReturnValue({ components: responseMock.components, customFieldsData: responseMock.customFieldsData });

    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      expect(screen.getByText(/edit/i)).toBeInTheDocument();
    });
  });

  test("renders Service component in 'create' mode", async () => {
    useServiceContext.mockReturnValueOnce({
      requestId: 23,
      setRequestId: jest.fn(),
      mode: "create",
      refresh: false,
      rowData: rowDataMock,
      setRowData: jest.fn(),
    });
    await client.query.mockResolvedValue({ data: { findCFValueList: { content: [] } } });
    await createDefinitionCF.mockReturnValue({ components: [] });
    await createDefinitionFromEntity.mockReturnValue({ components: [] });
    await getServiceData.mockReturnValue(rowDataMock);
    await loadDefinitions.mockReturnValue({ components: responseMock.components, customFieldsData: responseMock.customFieldsData });

    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      expect(screen.getByText(/create/i)).toBeInTheDocument();
    });
  });
  test("renders Service component in 'edit' mode and definition set", async () => {
    await client.query.mockResolvedValue({ data: { findCFValueList: { content: [{id:1, name:"test"}] } } });
    await createDefinitionCF.mockReturnValue({ components: [] });
    await createDefinitionFromEntity.mockReturnValue({ components: [{id:1, name:"test A", sort:1},{id:2, name:"test B", sort:2}] });
    await getServiceData.mockReturnValue(rowDataMock);
    await loadDefinitions.mockReturnValue({ components: responseMock.components, customFieldsData: responseMock.customFieldsData });

    act(() => {
      renderComponent();
    })
    await waitFor(() => {
      expect(screen.getByText(/edit/i)).toBeInTheDocument();
    });
  });
  //TODO :: review the error scenario
  // test("renders Service component  with error in query", async () => {
  //   await client.query.mockResolvedValue({ error: { message: "Error"} });
  //   act(() => {
  //     renderComponent();
  //   })
  //   await waitFor(() => {
  //     screen.debug();
     
  //   });
  // });

});
