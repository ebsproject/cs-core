import { Core } from "@ebs/styleguide";
const { Typography } = Core;
import loader from "assets/images/time_loader.gif";


const LoadingComponent = ({ message }) => {
    return (
        <div data-testid={"LoadingComponentTestId"}>
            <div
                style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    height: "20vh",
                }}
            >
                <Typography variant="title" color="primary">
                    {"Building the workflow components, please wait...."}
                </Typography>
            </div>
            <div
                style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <img src={loader} alt="Please wait..." />
            </div>
            <div
                style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                }}
            >
                <Typography variant="title" color="primary">
                    {message}
                </Typography>

            </div>
        </div>
    )
}
export default LoadingComponent;