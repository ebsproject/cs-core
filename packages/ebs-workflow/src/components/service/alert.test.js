import { render, screen } from "@testing-library/react";
import { useServiceContext } from "custom-components/context/service-context";
import RequestMessage from "./alert";

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));

jest.mock("assets/images/time_loader.gif", () => "loader-image");

describe("RequestMessage Component", () => {
  it("should render the dialog with the correct message for 'create' mode", () => {
    useServiceContext.mockReturnValue({ blockButtons: true });
    render(<RequestMessage mode="create" />);
    expect(screen.getByTestId("RequestMessageTestId")).toBeInTheDocument();
    expect(screen.getByText(/Your request is being created/i)).toBeInTheDocument();
    expect(screen.getByAltText("loading...")).toBeInTheDocument();
  });

  it("should render the dialog with the correct message for 'processed' mode", () => {
    useServiceContext.mockReturnValue({ blockButtons: true });
    render(<RequestMessage mode="processed" />);
    expect(screen.getByText(/Your request is being processed/i)).toBeInTheDocument();
  });

  it("should render the default message if mode is undefined", () => {
    useServiceContext.mockReturnValue({ blockButtons: true });
    render(<RequestMessage />);
    expect(screen.getByText(/Your request is being processed/i)).toBeInTheDocument();
  });

  it("Test should not render the dialog if blockButtons is false state", () => {
    useServiceContext.mockReturnValue({ blockButtons: false });
    render(<RequestMessage mode="create" />);
    expect(screen.queryByTestId("RequestMessageTestId")).not.toBeInTheDocument();
  });
});
