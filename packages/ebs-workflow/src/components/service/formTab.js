import { Core } from "@ebs/styleguide";
const { Button, Grid,Box } = Core;
import { FormattedMessage } from "react-intl";
import { EbsForm } from "@ebs/components";
import { useEffect, useState, useContext } from "react";
import { useUserContext } from "@ebs/layout";
import { WorkflowContext } from "components/designer/context/workflow-context";
import { loadDefinitions, getServiceData } from "hooks/utils/functions";
import { useDispatch, useSelector } from "react-redux";
import FormSkeleton from "./skeletonForm";
import { useServiceContext } from "custom-components/context/service-context";
import { getNodesToExecute, serviceMutation } from "./functions";
import { showMessage } from "store/modules/message";
import { sendUpdateEmail } from "./functions";
import { useExecuteProcess } from "hooks/useExecuteProcess";


const FormTab = ({selectEntityComponents, exitForm, node, _buttonRef, disabled }) => {
  const { executeProcess } = useExecuteProcess();
  const { workflowValues, workflowVariables, workflowPhase} = useSelector(({ wf_workflow }) => wf_workflow);
  const { userProfile } = useUserContext() || {};
  const {setRequestId, setMode, setRefresh, refresh, mode, setLoading, rowData, setBlockButtons} = useServiceContext();
  const [definition, setDefinition] = useState(null)
  const entity = workflowVariables.entity;
  const dispatch = useDispatch();

useEffect(()=>{
  if(node.formDefinition)
    setDefinition(node.formDefinition)
return ()=> setDefinition(null)
},[])
  useEffect(() => {
   // setDefinition(null);
    if (rowData && (mode === "edit" || mode === "view")) {
      const fetchData = async () => {
        let _workflowValues = {...workflowValues}
        const defaultFilters = [
          { col: "workflowId", mod: "EQ", val: _workflowValues?.id },
          { col: "userId", mod: "EQ", val: userProfile?.dbId },
          { col: "id", mod: "EQ", val: rowData.id },
        ];
        let row_data = await getServiceData(defaultFilters);
        let definition = await loadDefinitions(row_data, workflowVariables, node);
        setDefinition(definition);
      }
      fetchData();
    } 
    // else {
    //   console.log("Setting node info", node)
    //    setDefinition(node.formDefinition) }
return () => setDefinition(null)
  }, [rowData,refresh]);

  const sendEmailAndNotificationWhenUpdate = async (data) => {
    if (!data) return;
    await sendUpdateEmail(data, userProfile);
  }

  const submitAction = async (_data) => {
    let _workflowValues = {...workflowValues}
    let stages = [];
    _workflowValues.phases.forEach(p => {
      p.stages.forEach(s => {
        stages = [...stages, s]
      })
    })
    let form_data = Object.assign({}, _data)
    let data = {..._data};
    setBlockButtons(true);
    data.id = mode === "create" ? 0 : rowData.id;
    let selEntComp = selectEntityComponents;
    if (mode === "create") {
      data.customFields = [...workflowVariables.customFields];
    } else {
      let cfFields = [];
      if(definition['customFieldsData']){
        definition.customFieldsData.forEach((cf) => {
          cfFields = [...cfFields, cf.cfNode];
        });
        data.customFields = cfFields;
      }
      else {
        let cfs = workflowVariables.allAttributesV2 //.filter(item => Number(item.node.id) === Number(node.id));
        cfs.forEach((cf) => {
          cfFields = [...cfFields, cf];
        });
        data.customFields = cfFields;
      }
      // data = { //TODO Adding by default all the required fields for the main Entity, Service for the moment, review for implement different entities
      //   ...data,
      //   recipientId: rowData['recipient'] || 0,
      //   requestorId: rowData['requestor'] || 0  ,
      //   senderId: rowData['sender'] || 0 ,
      //   programId: rowData['program'] || 0 ,
      //   requestCode: rowData['requestCode'] || '',
      //   workflowInstance : rowData['wfInstance'] || 0 
      //  // serviceProviderId: rowData['serviceProvider'] || 0
      // }

      let _submitionDate = rowData['submitionDate'];
      if(_submitionDate)
         data = {...data,submitionDate : _submitionDate}
    }
    data.workflowId = _workflowValues.id;
    data.stages = [...stages];
    data.entity = entity;
    const { result: createdObj } = await serviceMutation(
      data,
      selEntComp,
      definition.customFieldsData,
      node
    );
    if (!createdObj) {
      dispatch(
        showMessage({
          variant: "error",
          message: createdObj.error,
          anchorOrigin: { vertical: "top", horizontal: "right" },
        })
      );
      exitForm();
      return;
    }
   if (mode === "create" || node['completed'] === false) {
  if(!createdObj['workflowInstanceId'])
        createdObj.workflowInstanceId = rowData['wfInstance']
      // let stages = [...workflowPhase.stages];
      let edgesNodes = getNodesToExecute(node, stages);
      let nodes = edgesNodes.sort((a, b) => {
        return a.sequence - b.sequence;
      });
      for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];
        await executeProcess({ node, createdObj, form_data });
      }
   }
    if (mode === "create") {
      setTimeout(() => {
        setRequestId(Number(createdObj?.id))
        setMode("edit");
      }, 3500);
    } else {
      refreshAction();
      await sendEmailAndNotificationWhenUpdate(data);
    }
  };
  const refreshAction = () => {
    setTimeout(() => {
      setRequestId(Number(rowData?.id));
      setRefresh(!refresh)
      exitForm();
      setDefinition(null)
    }, 3500);
  }

  if (!definition) return <FormSkeleton />;
  return (
    <div data-testid={"FormTabTestId"}>
        <Box sx={{width:"100%"}} >
      <EbsForm
        definition={() => definition}
        onSubmit={submitAction}
        actionPlacement="top"
      >
        {mode !== "view" && (
          <Grid container justifyContent={"flex-end"}>
            <Grid item>
              <Button
              data-testid={"button-submit-test-id"}
                  ref={(mode === "create" || !node.completed) ? _buttonRef : null}
                  sx={{ display: (mode === "create" || !node.completed) ? "none" : "block" }}
                disabled={disabled }
                type="submit"
              >
                <FormattedMessage id="none" defaultMessage="Save" />
              </Button>
            </Grid>

          </Grid>
        )}
      </EbsForm>
    </Box>
    </div>

    )
}
export default FormTab;