import { client } from "utils/apollo";
import {
  CREATE_CF_VALUES_BULK,
  FIND_EVENT_LIST,
  MODIFY_CF_VALUES_BULK,
  buildCreateMutation,
  buildModifyMutation,
} from "utils/apollo/gql/workflow";
import { CREATE_WORKFLOW_INSTANCE, CREATE_EVENT } from "utils/apollo/gql/workflow";
import { getContext } from "@ebs/layout";
import { buildQuery } from "utils/apollo/query";
import { getUserProfile } from "@ebs/layout";
import { showMessage } from "store/modules/message";
import { getCurrentStage } from "components/runtime/StepperBar/functions";
import { axiosClient } from "utils/axios/axios";
import validateFunctions from "validations";
export const serviceMutation = async (data, selEntComp, customFieldsData, node) => {

  try {
    let cf = [...data.customFields];
    let file;
    let fileAttribute = cf.find(item => item.cfType.name === 'file' )
    if(fileAttribute){
      file = cf[fileAttribute.fieldAttributes.name]
      delete data[fileAttribute.fieldAttributes.name]
    }
    const entity = data.entity;
    const stages = data.stages;
    const wfi = data?.workflowInstance;
    delete data.workflowInstance
    delete data.stages;
    delete data.entity;
    let input = Object.assign({}, extractCFs(data, selEntComp));
    if(!input["serviceProviderId"]){
      input = {...input, serviceProviderId : input["requestorId"] || 0}
    }
    if(!input["senderId"]){
      input = {...input, senderId : input["requestorId"] || 0}
    }
    if(!input["recipientId"]){
      input = {...input, recipientId : input["requestorId"] || 0}
    }
    if(input["requestorId"] === null){
      input = {...input, requestorId : input["senderId"] || 0}
    }
    const customFields = input.customFields;
    delete input.customFields;
    const mutation =
      Number(data.id) === 0 ? buildCreateMutation(entity) : buildModifyMutation(entity);
    input.id = data.id;
    input.tenantId = getContext().tenantId;

    if (Number(data.id) === 0) {
      const resp = await createWorkflowInstance(input.workflowId);
      const wfi_id = resp.createWorkflowInstance.id;
      input.workflowInstanceId = wfi_id;
      delete input.workflowId;
      const { data: response } = await client.mutate({
        mutation: mutation,
        variables: { [`${entity.toLowerCase()}`]: input },
      });
      const id = response[`create${entity}`].id;
      const eventId = await createEvent(id, wfi_id, stages);
      createCustomFieldValues(customFields, eventId);
      let createdObj = { id: id, workflowInstanceId: wfi_id };
      return { result: createdObj };
    } else {
      delete input.workflowId;
      let finalObject = {};
      let keys = Object.keys(input)
      keys.forEach(key => {
        if (typeof input[key] !== 'object') {
          finalObject = { ...finalObject, [key]: input[key] }
        }else{
          finalObject = {...finalObject, [key] : Number(input[key].value)}
        }
      })
      await client.mutate({
        mutation: mutation,
        variables: { [`${entity.toLowerCase()}`]: finalObject },
      });
      // if(!node.completed){
      //   const eventIdCF = await createEventCF(data.id, wfi || 0, stages, node);
      //   createCustomFieldValues(customFields, eventIdCF);
      // }
      // else
      await modifyCustomFieldValues(customFields, customFieldsData);
      let createdObj = { id: data.id, workflowInstanceId: wfi };
      return { result: createdObj };
    }
  } catch (error) {
    console.log(error);
    return { result: false, error: error };
  }
};

export const createEvent = async (id, wfi_id, stages, nd_st) => {
  let EventInput = {
    id: 0,
    recordId: id,
    workflowInstanceId: wfi_id,
    stageId: nd_st ? nd_st.st : stages.filter((item) => item.sequence === 1)[0].id,
    nodeId: nd_st
      ? nd_st.nd
      : stages
        .filter((item) => item.sequence === 1)[0]
        .nodes.filter((item) => item.nodeType.name === "form")[0].id,
    description: nd_st ? "" : stages[0].description,
    tenantId: getContext().tenantId,
  };
  try {
    const { data } = await client.mutate({
      mutation: CREATE_EVENT,
      variables: { event: EventInput },
    });
    return data.createEvent.id;
  } catch (error) {
    console.log(error);
  }
};
export const createEventCF = async (id, wfi_id, stages, node) => {
  let stageId = 0;
  let stageDescription =''
  stages.forEach(stage =>{
    stage.nodes.forEach(nd =>{
       if(Number(nd.id) === Number(node.id)){
        stageDescription = stage.description
        stageId = Number(stage.id)
       }
    })
  })

  console.log(id, wfi_id, stages, node.id, stageId)
  let EventInput = {
    id: 0,
    recordId: id,
    workflowInstanceId: wfi_id,
    stageId: stageId,
    nodeId: Number(node.id),
    description: stageDescription,
    tenantId: 1,
  };
  try {
    const { data } = await client.mutate({
      mutation: CREATE_EVENT,
      variables: { event: EventInput },
    });
    return data.createEvent.id;
  } catch (error) {
    console.log(error);
  }
};

const createWorkflowInstance = async (wfId) => {
  let date = new Date();
  let WorkflowInstanceInput = {
    id: 0,
    initiated: date.toISOString().split("T")[0],
    workflowId: wfId,
  };
  try {
    const { data } = await client.mutate({
      mutation: CREATE_WORKFLOW_INSTANCE,
      variables: { workflowInstance: WorkflowInstanceInput },
    });
    if (data) return data;
  } catch (error) {
    console.log(error);
  }
};
function checkLabelForSelectComponents(cf) {
  if (cf.value && "label" in cf.value)
    return cf.value["label"];
  else return "None"

}
function checkValueForSelectComponents(cf) {
  if (cf.value && "value" in cf.value)
    return cf.value["value"];
  else return 0

}
export const createCustomFieldValues = async (customFields, eventId) => {
  let input = [];
  for (let i = 0; i < customFields.length; i++) {
    const cf = customFields[i];
    const cfValueInput = {
      id: 0,
      textValue: cf.cfType.description.toLowerCase() === "select" ? checkLabelForSelectComponents(cf) : cf.value,
      codeValue: cf.cfType.description.toLowerCase() === "select" ? checkValueForSelectComponents(cf) : 0,
      numValue: 0,
      flagValue:
        cf.cfType.description.toLowerCase() === "checkbox" ||
          cf.cfType.description.toLowerCase() === "switch"
          ? cf.value
          : null,
      nodeCFId: cf.id,
      eventId: eventId,
      dateValue: cf.cfType.description.toLowerCase() === "datepicker" ? cf.value : null,
      tenantId: getContext().tenantId,
    };
    input = [...input, cfValueInput];
  }
  try {
    const { errors } = await client.mutate({
      mutation: CREATE_CF_VALUES_BULK,
      variables: { cfValues: input },
    });

    if (errors) return `Error! ${errors}`;
  } catch (error) {
    console.log(error);
  }
};
const modifyCustomFieldValues = async (customFields, customFieldsData) => {
  let input = [];
  for (let i = 0; i < customFields.length; i++) {
    const cf = customFields[i];
    let obj = customFieldsData.find((item) => item.name === cf.name);
    if (obj) {
      const cfValueInput = {
        id: obj.cfId,
        textValue: cf.cfType.description.toLowerCase() === "select" ? checkLabelForSelectComponents(cf) : cf.value,
        codeValue: cf.cfType.description.toLowerCase() === "select" ? checkValueForSelectComponents(cf) : 0,
        numValue: 0,
        flagValue:
          cf.cfType.description.toLowerCase() === "checkbox" ||
            cf.cfType.description.toLowerCase() === "switch"
            ? cf.value
            : null,
        nodeCFId: cf.id,
        eventId: obj.eventId,
        dateValue: cf.cfType.description.toLowerCase() === "datepicker" ? cf.value : null,
        tenantId: getContext().tenantId,
      };
      input = [...input, cfValueInput];
    }
  }
  try {
    const { errors } = await client.mutate({
      mutation: MODIFY_CF_VALUES_BULK,
      variables: { cfValues: input },
    });

    if (errors) return `Error! ${errors}`;
  } catch (error) {
    console.log(error);
  }
};

export function extractCFs(input, selEntComp) {
  let _input = { ...input };
  let objKeys = Object.keys(_input);
  for (let i = 0; i < _input.customFields.length; i++) {
    let cf = { ..._input.customFields[i] };
    if (objKeys.includes(cf.name)) {
      cf = { ...cf, ...{ value: _input[`${cf.name}`] } };
      _input.customFields[i] = cf
      delete _input[`${cf.name}`];
    }

  }
  for (let i = 0; i < selEntComp.length; i++) {
    let item = selEntComp[i];
    const controllerName = `${item.apiAttributesName}Id`;
    if (objKeys.includes(controllerName)) {
      let value = _input[`${controllerName}`];
      _input = { ..._input, ...{ [`${controllerName}`]: value ? value["value"] : null } };                   //input[`${controllerName}`] = value["value"];
    }
  }

  return _input;
}

export function createComponent(item, customComponents) {
  let date = new Date();
  switch (item.cfType.description.toLowerCase()) {
    case "textfield": {
      let textObj = {
        component: "textfield",
        name: item.attributes !== null ? item.apiAttributesName : item.name,
        sizes: item.fieldAttributes.sizes,
        helper: item.fieldAttributes.helper,
        inputProps: item.fieldAttributes.inputProps
          ? item.fieldAttributes.inputProps
          : {
            "data-testid": item.name,
            label: item.fieldAttributes.inputProps.label,
            multiline: true,
            rows: 1,
          },
        defaultValue: item.fieldAttributes.defaultValue,
        defaultRules: item.fieldAttributes.defaultRules,
      };
      if (item.required) textObj.rules = item.fieldAttributes.rules;
      return textObj;
    }
    case "datepicker": {
      let dateObj = {
        component: "datepicker",
        name: item.attributes !== null ? item.apiAttributesName : item.name,
        sizes: item.fieldAttributes.sizes,
        inputProps: item.fieldAttributes.inputProps
          ? item.fieldAttributes.inputProps
          : {
            "data-testid": item.name,
            label: item.fieldAttributes.inputProps.label,
          },
        defaultValue: null, //date.toISOString().split("T")[0],
      };
      if (item.required) dateObj.rules = item.fieldAttributes.rules;
      return dateObj;
    }
    case "checkbox": {
      let chkObj = {
        sizes: item.fieldAttributes.sizes,
        component: "checkbox",
        name: item.attributes !== null ? item.apiAttributesName : item.name,
        helper: item.fieldAttributes.helper,
        inputProps: item.fieldAttributes.inputProps,
        defaultValue: item.fieldAttributes.checked,
        defaultRules: item.fieldAttributes.defaultRules,
      };
      if (item.required) chkObj.rules = item.fieldAttributes.rules;
      return chkObj;
    }
    case "file":
      return {
        sizes: item.fieldAttributes.sizes,
        component: "File",
        name: item.fieldAttributes.name,
        label: item.fieldAttributes.customProps.label,
        customProps: {
          button: { color: "primary", size: "large" },
          input: {
            acceptedFiles: item.fieldAttributes.customProps.acceptedFiles,
            cancelButtonText: item.fieldAttributes.customProps.cancelButtonText,
            submitButtonText: item.fieldAttributes.customProps.submitButtonText,
            maxFileSize: 5000000,
            showPreviews: true,
            showFileNamesInPreview: true,
            isMulti: false,
          },
        },
        helper: item.fieldAttributes.helper ,
        defaultRules: item.fieldAttributes.defaultRules,
        rules:item.fieldAttributes.rules
      };
    case "radiogroup": {
      let radioObj = {
        sizes: item.fieldAttributes.sizes,
        component: "radio",
        name: item.name,
        label: item.fieldAttributes.inputProps.label,
        row: item.fieldAttributes.row,
        helper: item.fieldAttributes.helper,
        options: item.fieldAttributes.options,
        defaultValue: item.fieldAttributes.defaultValue.length > 0 ? item.fieldAttributes.defaultValue : null,
      };
      if (item.required) radioObj.rules = item.fieldAttributes.rules;
      return radioObj;
    }
    case "switch":
      return {
        sizes: item.fieldAttributes.sizes,
        component: "switch",
        name: item.name,
        label: item.fieldAttributes.inputProps.label,
        defaultValue: item.fieldAttributes.defaultValue,
        inputProps: item.fieldAttributes.inputProps,
        helper: item.fieldAttributes.helper,
        defaultRules: item.fieldAttributes.defaultRules,
        onChange: item.fieldAttributes.function ? (e, { setValue, getValues }) => validateFunctions[item.fieldAttributes.function](e, { setValue, getValues }) : ()=>{},
      };
      case "modal": //TODO: REPLACE THIS MODAL COMPONENT WITH THE NEW ONE DIALOG
        const Component = item.Component;
        return {
          sizes: item.sizes,
          component: "modal",
          parentControl: item.parentControl,
          defaultRules: {},
          uiComponent: Component,
          name: `modal.${item.parentControl}`,
          label: "",
          inputProps: {variant:"contained", className:'bg-ebs-brand-default hover:bg-ebs-brand-900 text-white p-2 m-2 rounded-md'},
          helper:{title:"Click here to open de Modal", placement:"top"},
        };
    case "dialog":
      const _Component = customComponents[item.fieldAttributes.componentUI]
      return {
        defaultRules: item.fieldAttributes.defaultRules,
        sizes: item.fieldAttributes.sizes,
        component: "dialog",
        parentControl: item.fieldAttributes.defaultRules.parentControl,
        uiComponent: _Component,
        name: `dialog.${item.fieldAttributes.name}`,
        label: item.fieldAttributes.label || "",
        inputProps: { variant: "contained", className: 'bg-ebs-brand-default hover:bg-ebs-brand-900 text-white p-2 m-2 rounded-md' },
        helper: item.fieldAttributes.helper,
      };
  }
}

export async function createComponentFromAPI(item) {
  try {
    let options = [];
    if (item.fieldAttributes.defaultOptions) {
      options = item.fieldAttributes.defaultOptions;
    } else {
      if (item.fieldAttributes.defaultRules.applyRules) {
        options = [];
      } else {
      //  options = await getOptions(item);
      }
    }
    let obj = {
      component: "select",
      name: item.attributes !== null ? `${item.apiAttributesName}Id` : item.name,
      sizes: item.fieldAttributes.sizes,
      options: options,
      inputProps: item.fieldAttributes.inputProps
        ? item.fieldAttributes.inputProps
        : {
          "data-testid": item.name,
          label: item.attributes !== null ? item.description : item.name,
          variant: "outlined",
        },
      helper: item.fieldAttributes.helper,
      defaultRules: {...item.fieldAttributes.defaultRules,fieldAttributes: item.fieldAttributes},
    };
    if (item.required) obj.rules = item.fieldAttributes.rules;
    return obj;
  } catch (error) {
    return { fail: true, error: error };
  }
}
async function getAllPages(variables, QUERY, entity) {

  const { data } = await client.query({
    query: QUERY,
    variables: variables,
  });
  return data[`find${entity}List`].content;
}
function getNestedProperty(obj, path) {
  return path.split('.').reduce((acc, key) => acc && acc[key], obj);
}
export async function getOptions(item) {
  let userProfile = getUserProfile();
  try {
    if (item.fieldAttributes.stateValues) {
      return [];
    }
    const entity = item.fieldAttributes.entity;
    const apiContent = item.fieldAttributes.apiContent;
    const QUERY = buildQuery(entity, apiContent);
    let variables = { page: { number: 1, size: 300 } };
    if (item.fieldAttributes.filters) {
      variables.filters = item.fieldAttributes.filters;
    }
    const { data } = await client.query({
      query: QUERY,
      variables: variables,
    });

    let result = [...data[`find${entity}List`].content];
    let totalPages = data[`find${entity}List`].totalPages;

    if (totalPages > 1) {
      for (let i = 2; i <= totalPages; i++) {
        variables.page = { number: i, size: 300 };
        result = [...result, ...await getAllPages(variables, QUERY, entity)];
      }
    }
    const _labels = item.fieldAttributes["labels"] ?  item.fieldAttributes["labels"] : [ "name" ];
    entity === "Program" ? (result = userProfile.permissions.memberOf.programs) : null; //TODO review to get from a dynamic rest API
    const formattedRes = result.map((data) => {
      let label= "";
      label = _labels.map(i => {
        return getNestedProperty(data, i)
      }).join("-");
      return { label: label, value: data.id };
    });
    return formattedRes;
  } catch (error) {
    console.log(item.fieldAttributes)
    console.error(error);
    return [];
  }
}

export function getNodesToExecute(node, stages) {
  let nodes = [];
  stages.forEach((item) => {
    item.nodes.forEach((nodeChild) => {
      if (nodeChild.dependOn.edges.includes(node.designRef)) {
        nodes = [...nodes, nodeChild];
      }
    });
  });
  return nodes;
}

export async function checkNodeStatus(id, wi) {
  try {
    const { data } = await client.query({
      query: FIND_EVENT_LIST,
      variables: {
        filters: [
          { col: "node.id", val: Number(id), mod: "EQ" },
          { col: "workflowInstance.id", val: Number(wi), mod: "EQ" },
        ],
      },
      fetchPolicy: "no-cache",
    });
    let response = [...data.findEventList.content];
    if (response.length > 0) {
      if (response[0].completed === null) return false;
      else return true;
    }
    return false;
  } catch (error) {
    console.error(error);
    return false;
  }
}
export function addModalComponent(ebsComponents, Component, parentControl) {

  let b = {
    cfType: { description: "modal" },
    sizes: [0, 0, 0, 0, 0],
    Component: Component,
    parentControl: parentControl
  }
  ebsComponents.push(createComponent(b))
  return;
}

export const fetchDefinition = async (nodes, mounted, mode, workflowVariables, customComponents, rowData) => {
  let flag = false;
  let message= null;
  let formNodes = [];
  let selectEntComp = [];
  let _nodes = [...nodes];
  let node = _nodes
    .sort((a, b) => {
      return Number(a.sequence) - Number(b.sequence);
    })
    .filter((item) => item.nodeType.name === "form" || item.nodeType.name === "External");
  for (let i = 0; i < node.length; i++) {
    let ebsComponents = [];
    let components = [...node[i].nodeCFs].sort((a, b) => {
      return Number(a.fieldAttributes.sort) - Number(b.fieldAttributes.sort);
    });
    let obj = {}
    if (mode === "create") {
      for (let i = 0; i < components.length; i++) {
        if (components[i].fieldAttributes.disabled) continue;
        if (components[i].cfType.description.toLowerCase() === "select") {
          if (components[i].attributes) selectEntComp.push(components[i]);
          let apiComp = await createComponentFromAPI(components[i]);
          if (components[i].fieldAttributes.stateValues) {
            let options = workflowVariables[components[i].fieldAttributes.stateValues.name];
            apiComp.options = options ? options : [];
          }
          if (apiComp.fail) {
             message = `Please check the ${components[i].name
              } field definition in Designer, ${apiComp.error.toString()}`;
              let resp = {
                selectEntComp: [],
                 message:message,
                 formNodes:[]
               }
               return resp;
          }
          ebsComponents.push(apiComp);
          let c = components[i];
          if (c.fieldAttributes.modalPopup?.show && customComponents) {
            const Component = customComponents[c.fieldAttributes.modalPopup?.componentUI];
            const parentControl = c.fieldAttributes.modalPopup?.parentControl;
            addModalComponent(ebsComponents, Component, parentControl);
          }
        } else {
          let c = components[i];
          let comp = createComponent(c, customComponents);
          ebsComponents = [...ebsComponents, comp];
          if (c.fieldAttributes.modalPopup?.show && customComponents) {
            const Component = customComponents[c.fieldAttributes.modalPopup?.componentUI];
            const parentControl = c.fieldAttributes.modalPopup?.parentControl;
            addModalComponent(ebsComponents, Component, parentControl);
          }
        }
      }
      obj = { ...node[i], ...{ formDefinition: { components: ebsComponents, name: node[i].name } } }
    } else {
      obj = { ...node[i] }
      for (let i = 0; i < components.length; i++) {
        if (components[i].cfType.description.toLowerCase() === "select") {
          if (components[i].attributes) selectEntComp.push(components[i]);
        }
      }
    }
    formNodes.push(obj);
  }
  if (mounted) {
  const { newNodes } =  await getAdditionalForms(rowData, mode);
 newNodes.forEach(node =>{
  let validateNode = formNodes.find( f => f.id === node.id)
  if(!validateNode)
    formNodes.push(node)
 })
  //formNodes = [...formNodes, ...newNodes]
   let resp = {
    selectEntComp: selectEntComp,
     message:message,
     formNodes:formNodes
   }
   return resp;
  }

};

const getAdditionalForms = async (rowData, mode) => {
  let newNodes = [];
  if (rowData && (mode === "edit" || mode === "view")) {
    const result = await getCurrentStage(rowData);
    const nodes = result.stage.nodes;
    for (let n = 0; n < nodes.length; n++) {
      const _node = nodes[n];
      if (_node['process'] && _node.process.code === "FORM" && _node.nodeType.name !== "submit") {
        newNodes = [...newNodes, _node];
      }
    }
   
  } 
return {newNodes: newNodes}
};
export const sendUpdateEmail = async (data, userProfile) => {
  let date = new Date();
  let rowData = Object.assign({}, data);
  let recipientEmail = rowData.customFields.find(item => item.name === "recipientEmail");
  let requestorEmail = rowData.customFields.find(item => item.name === "requestorEmail");
  if(!recipientEmail || !requestorEmail) return;
  const contactEmails = `${recipientEmail.value}, ${requestorEmail.value}`
  rowData = { ...rowData, currentUser: userProfile.full_name, date: date.toISOString().split("T")[0] }
  let payload = {
    to: contactEmails,
    from: "no-reply@ebsproject.org",
    subject: '',
    body: '',
    template: {
      id: 21,
      data: rowData
    }
  }
  const formData = new FormData();
  formData.append("payload", JSON.stringify(payload));
  try {
    await axiosClient.post(`send/email`, formData);
  } catch (error) {
    console.error(error)
  }

}