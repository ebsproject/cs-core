import { render, screen, fireEvent, waitFor, act } from "@testing-library/react";
import { useSelector, useDispatch } from "react-redux";
import { useUserContext } from "@ebs/layout";
import { useServiceContext } from "custom-components/context/service-context";
import { useExecuteProcess } from "hooks/useExecuteProcess";
import FormTab from "./formTab";
import { loadDefinitions, getServiceData } from "hooks/utils/functions";
import React from "react";
import { IntlProvider } from "react-intl";

jest.mock("react-redux", () => ({
  useSelector: jest.fn(),
  useDispatch: jest.fn(),
}));

jest.mock("@ebs/layout", () => ({
  useUserContext: jest.fn(),
  getCoreSystemContext: jest.fn(() => {
    return {
      markerDbUrl: "https://test-org.org",
      graphqlUri: "https://csapi-dev.ebsproject.org/graphql",
      printoutUri: "https://csps-dev.ebsproject.org",
      fileAPIUrl: "https://fileapi-dev.ebsproject.org",
      cbGraphqlUri: "https://cbgraphql-dev.ebsproject.org/graphql"
    }
  }),
  getDomainContext: jest.fn((instance) => {
    return { sgContext: "" };
  })
}));

jest.mock("custom-components/context/service-context", () => ({
  useServiceContext: jest.fn(),
}));

jest.mock("hooks/useExecuteProcess", () => ({
  useExecuteProcess: jest.fn(),
}));

jest.mock("hooks/utils/functions", () => ({
  loadDefinitions: jest.fn(),
  getServiceData: jest.fn(),
}));

jest.mock("./skeletonForm", () => jest.fn(() => <div>Skeleton Form</div>));

jest.mock('react-intl', () => ({
  FormattedMessage: ({ id, defaultMessage }) => <span>{defaultMessage}</span>,
}));

describe("FormTab component", () => {
  const mockDispatch = jest.fn();

  beforeEach(() => {
    useDispatch.mockReturnValue(mockDispatch);
    useUserContext.mockReturnValue({ userProfile: { dbId: 1 } });

    useSelector.mockImplementation((selectorFn) => {
      return selectorFn({
        wf_workflow: {
          workflowValues: {},
          workflowVariables: { entity: "someEntity", customFields: [] },
          workflowPhase: { stages: [] },
        },
      });
    });


    useExecuteProcess.mockReturnValue({
      executeProcess: jest.fn(),
    });

    // Mocking getServiceData and loadDefinitions
    loadDefinitions.mockResolvedValue({
      formFields: [{ fieldName: "exampleField", fieldType: "text" }],
    });

    getServiceData.mockResolvedValue({
      data: [{ id: 1, name: "Test Data" }],
    });

  });

const renderComponent  = (props) => render(
  <FormTab selectEntityComponents={props.selectEntityComponents}
   exitForm={props.exitForm} node={props.node}
    _buttonRef={props._buttonRef} disabled={props.disabled} />
)

  test("renders the form when definition is available", async () => {
    const props = {
      disabled:false,
      _buttonRef:null,
      exitForm: jest.fn(),
      selectEntityComponents:[],
      node:{
      formDefinition: {
        components: [{id:1, name:"name"}]
      }
    }}
    useServiceContext.mockReturnValue({
      setRequestId: jest.fn(),
      setMode: jest.fn(),
      setRefresh: jest.fn(),
      setBlockButtons: jest.fn(),
      setLoading: jest.fn(),
      rowData: { id: 1, submitionDate: "2023-10-01", status: "Draft" },
      mode: "edit",
      refresh: false,
      setDefinition: jest.fn(),
      definition:{formFields: [{ fieldName: "exampleField", fieldType: "text" }],}
    });
      renderComponent(props)
      expect(screen.getByTestId("FormTabTestId")).toBeInTheDocument();
  });

  test("renders the form skeleton while loading definition", () => {
    useServiceContext.mockReturnValue({
      setRequestId: jest.fn(),
      setMode: jest.fn(),
      setRefresh: jest.fn(),
      setBlockButtons: jest.fn(),
      setLoading: jest.fn(),
      rowData: { id: 1, submitionDate: "2023-10-01" },
      mode: "create",
      refresh: false,
      setDefinition: jest.fn(),
      definition: null
    });
    const props = {
      node:{
      formDefinition: null
    }
    }
    const { getByText } = renderComponent(props)
    expect(getByText("Skeleton Form")).toBeInTheDocument();
  });

  // test("disables the button when disabled prop is true", () => {
  //   const node = {
  //     formDefinition: {
  //       components: []
  //     }
  //   }
  //   const { getByTestId } = render(
  //     <IntlProvider locale="en">
  //       <FormTab selectEntityComponents={[]} exitForm={jest.fn()} node={node} _buttonRef={null} disabled={true} />
  //     </IntlProvider>

  //   );
  //   const button = getByTestId("button-submit-test-id");
  //   expect(button).toBeDisabled();
  // });

//   test("calls submitAction on form submit", async () => {
//     const mockSubmit = jest.fn();
//     const mockExitForm = jest.fn();
//     const props = {
//       disabled:false,
//       _buttonRef:null,
//       exitForm: jest.fn(),
//       selectEntityComponents:[],
//       node:{
//       formDefinition: {
//         components: [{id:1, name:"name"}]
//       }
//     }}
//     useServiceContext.mockReturnValue({
//       setRequestId: jest.fn(),
//       setMode: jest.fn(),
//       setRefresh: jest.fn(),
//       setBlockButtons: jest.fn(),
//       setLoading: jest.fn(),
//       rowData: { id: 1, submitionDate: "2023-10-01", status: "Draft" },
//       mode: "edit",
//       refresh: false,
//       setDefinition: jest.fn(),
//       definition:{formFields: [{ fieldName: "exampleField", fieldType: "text" }],}
//     });
//       renderComponent(props)
// screen.debug()
//     const submitButton = screen.getByTestId("button-submit-test-id");
//     fireEvent.click(submitButton);
//     await waitFor(() => {
//       expect(mockSubmit).toHaveBeenCalled();
//     });
//   });
});
