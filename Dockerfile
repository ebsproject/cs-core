FROM nginx:stable-alpine


RUN apk add jq bash npm ncurses


COPY ./packages/ebs-root /usr/code
COPY ./run.sh .
#COPY ./update_host_index.sh /usr/code 
COPY ./nginx.conf /etc/nginx/nginx.conf

# variables of security configuration
ENV REACT_APP_AUTH_CONFIG_AUTH_URL=https://sg-sso.ebsproject.org:8243/authorize
ENV REACT_APP_AUTH_CONFIG_TOKEN_URL=REACT_APP_AUTH_CONFIG_TOKEN_URL=https://sg-sso.ebsproject.org:8243/token
ENV REACT_APP_AUTH_CONFIG_CALLBACK_URL=https://cs-sdx.ebsproject.org/callback
ENV REACT_APP_AUTH_CONFIG_CLIENT_ID=tE3GUz8FnG_s6fxrqFRpOrGY8m4a
ENV REACT_APP_AUTH_CONFIG_CLIENT_SECRET=Nm1fqpUPYq3ZcWZ00Y4ddAgRvB0a
ENV REACT_APP_FILEAPI_URI=https://fileapi-dev.ebsproject.org

# variables of CS backend
ENV REACT_APP_CSAPI_URI_GRAPHQL=https://cs-sdx.ebsproject.org/graphql
ENV REACT_APP_CSAPI_URI_REST=https://cs-sdx.ebsproject.org/df
ENV REACT_APP_PRINTOUT_ENDPOINT=https://ps-sdx.ebsproject.org/
ENV REACT_APP_MARKERDB_ENDPOINT=https://smmarkerdbapi-dev.ebsproject.org
ENV REACT_APP_FILEAPI_URI=https://fileapi-sdx.ebsproject.org/
ENV REACT_APP_ACTUAL_VERSION=23.06.31

#variable define the version of the product
ENV REACT_APP_ACTUAL_VERSION=${libVersion}
ENV NODE_OPTIONS=--openssl-legacy-provider
#variables use to replace the tag #{HOST_CS_SHAREDLIB}# in index.ejs
ENV HOST_URL=https://bucketebriones.s3.us-west-2.amazonaws.com




RUN chmod a+x ./run.sh
#RUN chmod a+x ./update_host_importmap.sh
#RUN chmod a+x ./usr/code/update_host_index.sh
RUN chown nginx:nginx -R /usr/share/nginx/html

EXPOSE 80

#ENTRYPOINT ["nginx", "-g", "daemon off;"]
CMD ["./run.sh"]